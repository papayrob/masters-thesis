# ProperRaytracer project

Git repository for the Master's thesis project: **Efficient ray tracing algorithms exploiting kd-trees on a GPU**

Author: Robert Papay

## Structure

- ProperRaytracer contains a CMake project with all source code.
- ProperRaytracer/Scripts cotains additional Python scripts used for testing
- ProperRaytracer/Assets contains sample assets that can be used when testing the built application
- Documents contains the semestral project and thesis texts and latex source code of the thesis
- Media contains media related to the application

## Images

### Application UI

![Application UI](/Media/application-ui.png)

### Conference scene render

![Conference scene render](/Media/ConferenceStatic.png)
