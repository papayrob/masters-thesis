"""
 * File name: benchmarker.py
 * Description: Python script for benchmarking ProperRaytracer.
 * Creates statistics files based on input config files for input scenes.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
"""

import subprocess
from pathlib import Path
from shutil import copyfile, move as movefile

gen_file_names_only = False
frame_count = 20

def benchmark(runs : int, parent_path : Path, scene : str, app_config : str) -> str:
    """
    Benchamrks ProperRaytracer in parent_path on scene with app_config runs times.
    """

    stats_path = parent_path / "Tables" / f"{Path(scene).stem}_{Path(app_config).stem}.stats"
    global frame_count

    # run the program a few times
    args = [str((parent_path / "ProperRaytracer.exe")), scene, app_config, '-t', '--mode=rt', '--no-gui', f'--stop-frame={frame_count}', '--save-frame=10', '--log=benchmark.log']
    if not gen_file_names_only:
        for _ in range(runs):
            run_result = subprocess.run(args, cwd=parent_path)
            run_result.check_returncode()
    
        copyfile(parent_path / "Tables" / "benchmark.stats", stats_path)
    
    for file in (parent_path / "Assets").iterdir():
        if file.is_file() and file.suffix == '.png':
            image_path = stats_path.with_stem(f"{file.stem}_{Path(app_config).stem}").with_suffix('.png')
            movefile(file, image_path)

    return stats_path


def reset_stats_file(parent_path : Path):
    """Resets benchmark.stats file"""

    if not gen_file_names_only:
        with open(parent_path / "Tables" / "benchmark.stats", 'w') as f:
            f.write('{"info": {},"data": {}}')

def benchmark_configs(parent_path : Path, scene : str, configs : list[Path]) -> list[Path]:
    """
    Benchamrks ProperRaytracer in parent_path on scene with configs.

    Assumes configs are relative to Assets folder or absolute.
    """

    stat_files = []

    for config in configs:
        reset_stats_file(parent_path)
        stat_files.append(benchmark(2, parent_path, scene, str(config.relative_to(parent_path / "Assets"))))
    
    return stat_files


def benchmark_configs_path(parent_path : Path, scene : str, configs_path : Path):
    """
    Benchamrks ProperRaytracer in parent_path on scene with all configs from configs_path.
    """
    configs = []
    if configs_path.is_dir():
        for config in configs_path.iterdir():
            configs.append(config)
    return benchmark_configs(parent_path, scene, configs)

release_path = Path("C:/Users/Robert/Documents/UniProjects/master/ProperRaytracer/out/build/x64-release/")
E_STATIC = 0
E_DYNAMIC = 1

def benchmark_scene(scene : str, scene_type):
    """Benchmarks a static scene, expects benchmarks configs to be located in Assets/StaticBenchmarks"""
    if scene_type == E_STATIC:
        return benchmark_configs_path(release_path, scene, release_path / "Assets" / "StaticBenchmarks")
    else:
        return benchmark_configs_path(release_path, scene, release_path / "Assets" / "DynamicBenchmarks")

def get_benchmark_stats_file_names(scene : str, scene_type):
    global gen_file_names_only
    gen_file_names_only = True
    names = benchmark_scene(scene, scene_type)
    gen_file_names_only = False
    return names

static_scenes = [
    "BistroStatic.scene",
    "Buddha.scene",
    "Conference.scene",
    "FairyForestStatic.scene",
    "FieldStatic.scene",
    "Powerplant.scene",
    "SanMiguel.scene",
    "SibenikStatic.scene",
    "Sponza.scene",
    "StreetStatic.scene"
]

dynamic_scenes = [
    "Bistro.scene",
    "FairyForest.scene",
    "Field.scene",
    "Sibenik1.scene",
    "Sibenik2.scene",
    "Sibenik3.scene",
    "Street.scene"
]

if __name__ == "__main__":
    #benchmark_scene("SibenikStatic.scene", E_STATIC)

    for scene in static_scenes:
        benchmark_scene(scene, E_STATIC)

    for scene in dynamic_scenes:
        benchmark_scene(scene, E_DYNAMIC)
