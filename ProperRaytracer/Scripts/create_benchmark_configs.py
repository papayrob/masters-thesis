"""
 * File name: create_benchmark_configs.py
 * Description: Python script for automatic creation of benchmark configuration json files.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
"""

import pyjson5
import json
from pathlib import Path

class Iterator:
    def __init__(self, key : str, values : list):
        self.key = key
        self.values = values
        self.idx = 0
    
    @property
    def value(self):
        return self.values[self.idx]
    
    def __iadd__(self, other):
        if isinstance(other, int):
            self.idx += other
        else:
            raise ValueError('Add must be with int')
        return self
    
    def __bool__(self):
        return self.idx < len(self.values)
    
    def reset(self):
        self.idx = 0

def create(template : str, name_template : str):
    with open(template, 'r', encoding="utf-8") as file:
        j = pyjson5.decode_io(file)
    
    params = []
    out = {}
    for key, values in j.items():
        params.append(Iterator(key, values))
        out[key] = None
    
    out_dir = Path('Generated')
    if not out_dir.exists():
        out_dir.mkdir()

    idx = 0
    n = 0
    while True:
        it : Iterator = params[idx]
        # reset iterator if at end
        if it:
            # insert current value
            out[it.key] = it.value

            # increase iterator
            it += 1

            # next iterator
            idx += 1
            if idx == len(params):
                # save json
                with open(out_dir / name_template.format(n), 'w', encoding="utf-8") as file:
                    json.dump(out, file, indent=2)
                idx -= 1
                n += 1
        else:
            it.reset()
            idx -= 1

            # if the first iterator is at end, end loop
            if idx < 0:
                break


if __name__ == "__main__":
    create("bm_static_cpu_template.config", "bm_static_cpu_{:02d}.config")
    create("bm_static_gpu_template.config", "bm_static_gpu_{:02d}.config")
    create("bm_static_gpu_stack_template.config", "bm_static_gpu_stack_{:02d}.config")
    create("bm_static_gpu_stackless_template.config", "bm_static_gpu_stackless_{:02d}.config")
    create("bm_static_gpu_shortstack_template.config", "bm_static_gpu_shortstack_{:02d}.config")

    create("bm_dynamic_cpu_template.config", "bm_dynamic_cpu_{:02d}.config")
    create("bm_dynamic_gpu_template.config", "bm_dynamic_gpu_{:02d}.config")
    create("bm_merge_template.config", "bm_merge_{:02d}.config")
