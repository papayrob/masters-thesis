"""
 * File name: table_maker.py
 * Description: Python script for creating tables from statistics files.
 * Creates benchmark statistics tables from stat files created by benchmarker.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
"""

import TableUtil
import benchmarker
from pathlib import Path
from quantiphy import Quantity, UnitConversion
from texttable import Texttable
import latextable
import matplotlib.pyplot as plt
import numpy as np

def to_herz(s):
    return 1.0/s

def from_herz(hz):
    return 1.0/hz

herz_conversion = UnitConversion('s', 'Hz', to_herz, from_herz)

def make_time_column(getter, name : str, source : str = 'ns', target : str = 'ms') -> TableUtil.Column:
    return TableUtil.Column(
        TableUtil.ConvertGetter(getter, lambda x : Quantity(x, source).scale(target).real if x is not None else None),
        name
    )

column_dict = { key: TableUtil.Column(key, value) for key,value in {
    'AverageIntersectionsPerQuery': '$N_{IT}$',
    'AverageTraversalStepsPerQuery': '$N_{TR}$',
    'QueryCount': '$N_Q$',
    'Cost': '$C$',
    'InnerNodes': '$N_I$',
    'LeafNodes': '$N_L$',
    'EmptyNodes': '$N_E$',
    'AveragePrimitivesInLeaf': '$N_{AP}$',
    'MaxPrimitivesInLeaf': '$N_{MP}$',
    'AverageDepth': '$D_{AVG}$',
    'MaxDepth': '$D_{MAX}$',
    'Bins': 'Bins',
    'Chunks': 'Chunks',
    'Groups': 'Groups',
    'Subgroups': 'Subgroups',
    'maxDepth_k1': '$k_{D,1}$',
    'maxDepth_k2': '$k_{D,2}$',
    'traversalCost': '$C_{T}$',
    'intersectionCost': '$C_{I}$',
    'primitiveLimit': '$P_{MAX}$',
    'StaticPrimitiveCount': '$N_{S}$',
    'DynamicPrimitiveCount': '$N_{D}$',
    'PrimitiveMemPoolAllocs': '$M_{P,A}$',
    'PrimitiveMemPoolSplits': '$M_{P,S}$',
    'PrimitiveMemPoolEmpty': '$M_{P,E}$',
    'DataMemPoolAllocs': '$M_{D,A}$',
    'DataMemPoolSplits': '$M_{D,S}$',
    'DataMemPoolEmpty': '$M_{D,E}$'
}.items() }

column_dict.update({
    'Nodes': TableUtil.Column(TableUtil.AddingGetter(['InnerNodes','LeafNodes'], TableUtil.take_average), '$N_n$'),
    'InnerToLeafRatio': TableUtil.Column(TableUtil.RatioGetter('InnerNodes','LeafNodes', TableUtil.take_average), '$R_{I2L}$'),
    'EmptyRatio': TableUtil.Column(TableUtil.RatioGetter('LeafNodes', 'EmptyNodes', TableUtil.take_average), '$R_E$'),
    'Performance': TableUtil.Column(
        TableUtil.ConvertGetter(TableUtil.ValueGetter('AverageQueryTime', TableUtil.take_median), lambda x : Quantity(x, 'ns').scale('MHz').real if x is not None else None),
        'PERF (MR/s)'
    ),
    'StaticBuildTime': make_time_column(TableUtil.ValueGetter('StaticBuildTime', TableUtil.take_median), '$T_{B,S}$ (ms)'),
    'DynamicBuildTime': make_time_column(TableUtil.ValueGetter('DynamicBuildTime', TableUtil.take_median), '$T_{B,D}$ (ms)'),
    'TaskPoolTime': make_time_column(TableUtil.ValueGetter('TaskPoolTime', TableUtil.take_median), '$T_{TP}$ (ms)'),
    'AverageQueryTime': make_time_column(TableUtil.ValueGetter('AverageQueryTime', TableUtil.take_median), '$T_Q$ (ns)', target='ns'),
    'BuildTime': make_time_column(TableUtil.AddingGetter(['StaticBuildTime','DynamicBuildTime'], TableUtil.take_median), '$T_B$ (ms)'),
    'FrameTime': make_time_column(
        TableUtil.ChainGetter(
            TableUtil.ValueGetter('DynamicBuildTime', TableUtil.take_median),
            lambda v1,v2 : v1 + v2 if v1 is not None and v2 is not None else None,
            TableUtil.MultGetter(['AverageQueryTime', 'QueryCount'], TableUtil.take_median)
        ),
        '$T_F$ (s)', target='s'),
    'PrimitiveCount': TableUtil.Column(TableUtil.AddingGetter(['StaticPrimitiveCount','DynamicPrimitiveCount']), '$N$'),
    'Retrievals': TableUtil.Column(
        TableUtil.ConvertGetter(TableUtil.ValueGetter('Retrievals', TableUtil.take_average), lambda x : x / 1000000 if x is not None else None),
        '$N_R$ $(\\cdot 10^{-6})$'
    ),
    'SkippedRetrievals': TableUtil.Column(
        TableUtil.ConvertGetter(TableUtil.ValueGetter('SkippedRetrievals', TableUtil.take_average), lambda x : x / 1000000 if x is not None else None),
        '$N_{SR}$ $(\\cdot 10^{-6})$'
    ),
    'MemoryConsumption': TableUtil.Column(
        TableUtil.ConvertGetter(TableUtil.ValueGetter('MemoryConsumption', TableUtil.take_average), lambda x : x / 1000000 if x is not None else None),
        '$M$ (MB)'
    ),
})

graph_dict = { key: TableUtil.Column(key, value) for key,value in {
    'QueryCountPerThread': 'Query count',
    'AverageIntersectionsPerQueryPerThread': 'Average intersections per query',
    'AverageTraversalStepsPerQueryPerThread': 'Average traversal steps per query'
}.items() }


def format_table(table : Texttable, formats : list[TableUtil.Formatter]) -> Texttable:
    return table.set_max_width(0).set_cols_align(['l'] + ['r' for _ in range(len(formats))]).set_cols_dtype(['t'] + formats)

def plot_bars(fig, ax, data, labels : list[str], colors : list[str], width : float, ylabel : str, title : str, scenes : list[str], filepath : str):
    # Ensure the input data are of the same length
    if len(data) == 0 or any([(len(d) != len(data[0])) for d in data]) or len(data[0]) != len(scenes) or len(data) != len(colors) or len(data) != len(labels):
        raise ValueError("Wrong input")

    # Create an array for the x positions of groups
    indices = np.arange(len(data[0]))

    # Plot bars for data
    width = width / len(data)

    for i, (d, label, color) in enumerate(zip(data, labels, colors)):
        offset = (i - (len(data) - 1) / 2) * width
        ax.bar(indices + offset, d, width, label=label, color=color)

    # Add labels and title
    ax.set_xlabel('Scene')
    ax.set_ylabel(ylabel)
    ax.set_title(title)
    
    # Add x-axis ticks
    ax.set_xticks(indices, labels=[scene[0:2] for scene in scenes])

    # Add legend
    ax.legend()

    # Show grid
    ax.grid(axis='y', linestyle='--', alpha=0.7)

    # Display the plot
    fig.set_size_inches(plt.rcParams['figure.figsize'][0], 1.5 * plt.rcParams['figure.figsize'][1])
    fig.tight_layout()
    fig.show()
    fig.savefig(filepath, format='svg')

write_tables : bool = True
write_graphs : bool = True
display_graphs : bool = False

bar_colors = ['blue','orange','green', 'red', 'purple']

if __name__ == "__main__":
    # Create columns and formats for tables
    def make_columns_from_keys(keys : list[str]):
        return [column_dict[key] for key in keys]

    build_info_table_columns = make_columns_from_keys([
        'Nodes', 'InnerToLeafRatio', 'EmptyRatio', 'AveragePrimitivesInLeaf', 'MaxPrimitivesInLeaf', 'AverageDepth', 'Cost'
    ])
    build_info_table_formats = [
        TableUtil.Formatter('i'), TableUtil.Formatter(), TableUtil.Formatter(), TableUtil.Formatter(), TableUtil.Formatter('i'), TableUtil.Formatter(), TableUtil.Formatter()
    ]

    build_speed_table_columns = make_columns_from_keys([
        'BuildTime', 'TaskPoolTime', 'Retrievals', 'SkippedRetrievals', 'MemoryConsumption'
    ])
    build_speed_table_formats = [
        TableUtil.Formatter(), TableUtil.Formatter(), TableUtil.Formatter('f', 3), TableUtil.Formatter('f', 3), TableUtil.Formatter('f', 3)
    ]

    performance_table_columns = make_columns_from_keys([
        "Performance", "AverageQueryTime", 'AverageIntersectionsPerQuery', 'AverageTraversalStepsPerQuery', "FrameTime"
    ])
    performance_table_formats = [
        TableUtil.Formatter(), TableUtil.Formatter(), TableUtil.Formatter(), TableUtil.Formatter(), TableUtil.Formatter()
    ]

    traversal_table_columns = make_columns_from_keys([
        "Performance", "AverageQueryTime", 'AverageIntersectionsPerQuery', 'AverageTraversalStepsPerQuery'
    ])
    traversal_table_formats = [
        TableUtil.Formatter(), TableUtil.Formatter(), TableUtil.Formatter(), TableUtil.Formatter()
    ]

    info_table_columns = make_columns_from_keys(['StaticPrimitiveCount', 'DynamicPrimitiveCount', 'QueryCount', 'MaxDepth'])
    info_table_formats = [TableUtil.Formatter(), TableUtil.Formatter(), TableUtil.Formatter(), TableUtil.Formatter()]

    # Init arrays for graphs
    exact_cost : list[float] = []
    approx_cost : list[float] = []
    large_build_time_cpu : list[float] = []
    large_build_time_gpu : list[float] = []
    small_build_time_cpu : list[float] = []
    small_build_time_gpu : list[float] = []
    whole_time : list[float] = []
    task_time : list[float] = []
    cpu_mem : list[float] = []
    gpu_mem : list[float] = []
    exact_perf : list[float] = []
    approx_perf : list[float] = []
    stack_traversal : list[float] = []
    shortstack_traversal : list[float] = []
    stackless_traversal : list[float] = []
    large_scenes : list[str] = []
    small_scenes : list[str] = []
    cpu_single_b : list[float] = []
    cpu_double_b : list[float] = []
    cpu_merge_b : list[float] = []
    gpu_single_b : list[float] = []
    gpu_double_b : list[float] = []
    cpu_single_p : list[float] = []
    cpu_double_p : list[float] = []
    cpu_merge_p : list[float] = []
    gpu_single_p : list[float] = []
    gpu_double_p : list[float] = []
    cpu_single_f : list[float] = []
    cpu_double_f : list[float] = []
    cpu_merge_f : list[float] = []
    gpu_single_f : list[float] = []
    gpu_double_f : list[float] = []

    # Create tables for static scenes
    with open(benchmarker.release_path / "Tables" / "latex" / 'static.txt', 'w') as latex_file:
        for scene in benchmarker.static_scenes:
            with open(benchmarker.release_path / "Tables" / "text" / f'{Path(scene).stem}.txt', 'w') as text_file:
                # Create rows
                files = benchmarker.get_benchmark_stats_file_names(scene, benchmarker.E_STATIC)
                build_rows : list[TableUtil.Row] = []
                traversal_rows : list[TableUtil.Row] = []
                for file in files:
                    if file.exists():
                        if file.name.find('cpu_00') >= 0:
                            build_rows.append(TableUtil.Row(file, 'CPU exact', 0))
                        elif file.name.find('cpu_01') >= 0:
                            build_rows.append(TableUtil.Row(file, 'CPU approx', 1))
                        elif file.name.find('gpu_00') >= 0:
                            build_rows.append(TableUtil.Row(file, 'GPU approx', 2))
                        elif file.name.find('_stack_00') >= 0:
                            traversal_rows.append(TableUtil.Row(file, 'Stack', 0))
                        elif file.name.find('stackless_00') >= 0:
                            traversal_rows.append(TableUtil.Row(file, 'Restart', 1))
                        elif file.name.find('stackless_01') >= 0:
                            traversal_rows.append(TableUtil.Row(file, 'PushDown', 2))
                        elif file.name.find('shortstack_00') >= 0:
                            traversal_rows.append(TableUtil.Row(file, 'RestartShortStack (4)', 3))
                        elif file.name.find('shortstack_01') >= 0:
                            traversal_rows.append(TableUtil.Row(file, 'RestartShortStack (8)', 4))
                        elif file.name.find('shortstack_02') >= 0:
                            traversal_rows.append(TableUtil.Row(file, 'PushDownShortStack (4)', 5))
                        elif file.name.find('shortstack_03') >= 0:
                            traversal_rows.append(TableUtil.Row(file, 'PushDownShortStack (8)', 6))
                
                build_rows = sorted(build_rows, key=lambda row : row.order)
                traversal_rows = sorted(traversal_rows, key=lambda row : row.order)
                
                # Write tables
                if write_tables:
                    build_info_table = TableUtil.make_table("\\textbf{Config}", build_rows, build_info_table_columns, lambda x : format_table(x, build_info_table_formats))
                    build_speed_table = TableUtil.make_table("\\textbf{Config}", build_rows, build_speed_table_columns, lambda x : format_table(x, build_speed_table_formats))
                    performance_table = TableUtil.make_table("\\textbf{Config}", build_rows, performance_table_columns, lambda x : format_table(x, performance_table_formats))
                    traversal_table = TableUtil.make_table("\\textbf{Config}", traversal_rows, traversal_table_columns, lambda x : format_table(x, traversal_table_formats))

                    text_file.write(TableUtil.make_table("\\textbf{Config}", build_rows, info_table_columns, lambda x : format_table(x, info_table_formats)).draw())
                    text_file.write('\n\n')
                    text_file.write(build_info_table.draw())
                    text_file.write('\n\n')
                    text_file.write(build_speed_table.draw())
                    text_file.write('\n\n')
                    text_file.write(performance_table.draw())
                    text_file.write('\n\n')
                    text_file.write(traversal_table.draw())
                    text_file.write('\n\n')

                    scene_name = Path(scene).stem.replace('Static', '')
                    latex_file.write(f"\\allbold{{{scene_name} ($N_S = {int(info_table_columns[1].get_value_from_file(build_rows[2].path))}, N_Q: {int(info_table_columns[2].get_value_from_file(build_rows[2].path))}, D_{{MAX}} = {int(info_table_columns[3].get_value_from_file(build_rows[2].path))}$)}}")
                    latex_file.write('\n\n')
                    latex_file.write(latextable.draw_latex(build_info_table, f'{scene_name} scene k-d tree statistics', None, False, f'TAB:{scene_name}-1', position='H'))
                    latex_file.write('\n\n')
                    latex_file.write(latextable.draw_latex(build_speed_table, f'{scene_name} scene k-d tree build statistics', None, False, f'TAB:{scene_name}-2', position='H'))
                    latex_file.write('\n\n')
                    latex_file.write(latextable.draw_latex(performance_table, f'{scene_name} scene ray tracing statistics', None, False, f'TAB:{scene_name}-3', position='H'))
                    latex_file.write('\n\n')
                    latex_file.write(latextable.draw_latex(traversal_table, f'{scene_name} traversal algorithm statistics', None, False, f'TAB:{scene_name}-4', position='H'))
                    latex_file.write('\n\n')
                    latex_file.write('\\newpage\n\n')

                # Append to graph arrays
                if write_graphs:
                    exact_cost.append(column_dict['Cost'].get_value_from_file(build_rows[0].path))
                    approx_cost.append(column_dict['Cost'].get_value_from_file(build_rows[1].path))
                    exact_perf.append(column_dict['Performance'].get_value_from_file(build_rows[0].path))
                    approx_perf.append(column_dict['Performance'].get_value_from_file(build_rows[1].path))
                    if int(info_table_columns[1].get_value_from_file(build_rows[2].path)) >= 1_000_000:
                        large_build_time_cpu.append(column_dict['DynamicBuildTime'].get_value_from_file(build_rows[1].path))
                        large_build_time_gpu.append(column_dict['DynamicBuildTime'].get_value_from_file(build_rows[2].path))
                        large_scenes.append(scene)
                    else:
                        small_build_time_cpu.append(column_dict['DynamicBuildTime'].get_value_from_file(build_rows[1].path))
                        small_build_time_gpu.append(column_dict['DynamicBuildTime'].get_value_from_file(build_rows[2].path))
                        small_scenes.append(scene)
                    cpu_mem.append(column_dict['MemoryConsumption'].get_value_from_file(build_rows[1].path))
                    gpu_mem.append(column_dict['MemoryConsumption'].get_value_from_file(build_rows[2].path))
                    whole_time.append(column_dict['DynamicBuildTime'].get_value_from_file(build_rows[2].path))
                    task_time.append(column_dict['TaskPoolTime'].get_value_from_file(build_rows[2].path))

                    stack_traversal.append(column_dict['Performance'].get_value_from_file(traversal_rows[0].path))
                    stackless_traversal.append(column_dict['Performance'].get_value_from_file(traversal_rows[2].path))
                    shortstack_traversal.append(column_dict['Performance'].get_value_from_file(traversal_rows[5].path))

    # Create graphs
    if write_graphs:
        fpath_template = str(benchmarker.release_path / "Tables" / "latex" / '{}.svg')
        fig, ax = plt.subplots(1,1)
        plot_bars(fig, ax, [exact_cost, approx_cost], ['Exact alg. cost', 'Approx. alg. cost'], bar_colors[0:2], 0.6, 'Cost [-]', '', benchmarker.static_scenes, fpath_template.format('cost'))
        if display_graphs:
            plt.show()
        plt.close()
        fig, ax = plt.subplots(1,1)
        plot_bars(fig, ax, [exact_perf, approx_perf], ['Exact alg. performance', 'Approx. alg. performance'], bar_colors[0:2], 0.6, 'PERF [MR/s]', '', benchmarker.static_scenes, fpath_template.format('performance'))
        if display_graphs:
            plt.show()
        plt.close()
        fig, ax = plt.subplots(1,1)
        plot_bars(fig, ax, [large_build_time_cpu, large_build_time_gpu], ['CPU alg. build time', 'GPU alg. build time'], bar_colors[0:2], 0.6, 'Time [ms]', '', large_scenes, fpath_template.format('large-build'))
        if display_graphs:
            plt.show()
        plt.close()
        fig, ax = plt.subplots(1,1)
        plot_bars(fig, ax, [small_build_time_cpu, small_build_time_gpu], ['CPU alg. build time', 'GPU alg. build time'], bar_colors[0:2], 0.6, 'Time [ms]', '', small_scenes, fpath_template.format('small-build'))
        if display_graphs:
            plt.show()
        plt.close()
        fig, ax = plt.subplots(1,1)
        plot_bars(fig, ax, [whole_time, task_time], ['Total build time', 'Task pool time'], bar_colors[0:2], 0.6, 'Time [ms]', '', benchmarker.static_scenes, fpath_template.format('whole-vs-task'))
        if display_graphs:
            plt.show()
        plt.close()
        fig, ax = plt.subplots(1,1)
        plot_bars(fig, ax, [cpu_mem, gpu_mem], ['CPU alg. memory', 'GPU alg. memory'], bar_colors[0:2], 0.6, 'Memory [MB]', '', benchmarker.static_scenes, fpath_template.format('memory'))
        if display_graphs:
            plt.show()
        plt.close()
        fig, ax = plt.subplots(1,1)
        plot_bars(fig, ax, [stack_traversal, stackless_traversal, shortstack_traversal], ['Stack traversal', 'Stackless traversal', 'Short-stack traversal'], bar_colors[0:3], 0.6, 'PERF [MR/s]', '', benchmarker.static_scenes, fpath_template.format('traversal'))
        if display_graphs:
            plt.show()
        plt.close()
    
    # Modify columns and formats for dynamic scenes
    build_speed_table_columns = make_columns_from_keys([
        'StaticBuildTime', 'DynamicBuildTime', 'TaskPoolTime'#, 'Retrievals', 'SkippedRetrievals'
    ])
    build_speed_table_formats = [
        TableUtil.Formatter(), TableUtil.Formatter(), TableUtil.Formatter()#, TableUtil.Formatter('f', 3), TableUtil.Formatter('f', 3)
    ]
    
    # Create tables for dynamic scenes
    with open(benchmarker.release_path / "Tables" / "latex" / 'dynamic.txt', 'w') as latex_file:
        i = 0
        for scene in benchmarker.dynamic_scenes:
            with open(benchmarker.release_path / "Tables" / "text" / f'{Path(scene).stem}.txt', 'w') as text_file:
                # Create rows
                files = benchmarker.get_benchmark_stats_file_names(scene, benchmarker.E_DYNAMIC)
                build_rows : list[TableUtil.Row] = []
                for file in files:
                    if file.exists():
                        if file.name.find('cpu_00') >= 0:
                            build_rows.append(TableUtil.Row(file, 'CPU single', 0))
                        elif file.name.find('cpu_01') >= 0:
                            build_rows.append(TableUtil.Row(file, 'CPU double', 1))
                        elif file.name.find('merge_00') >= 0:
                            build_rows.append(TableUtil.Row(file, 'CPU merge', 2))
                        elif file.name.find('gpu_00') >= 0:
                            build_rows.append(TableUtil.Row(file, 'GPU single', 3))
                        elif file.name.find('gpu_01') >= 0:
                            build_rows.append(TableUtil.Row(file, 'GPU double', 4))
                
                build_rows = sorted(build_rows, key=lambda row : row.order)
                
                # Write tables
                if write_tables:
                    build_speed_table = TableUtil.make_table("\\textbf{Config}", build_rows, build_speed_table_columns, lambda x : format_table(x, build_speed_table_formats))
                    performance_table = TableUtil.make_table("\\textbf{Config}", build_rows, performance_table_columns, lambda x : format_table(x, performance_table_formats))

                    text_file.write(TableUtil.make_table("\\textbf{Config}", build_rows, info_table_columns, lambda x : format_table(x, info_table_formats)).draw())
                    text_file.write('\n\n')
                    text_file.write(build_speed_table.draw())
                    text_file.write('\n\n')
                    text_file.write(performance_table.draw())
                    text_file.write('\n\n')

                    scene_name = Path(scene).stem
                    latex_file.write(f"\\allbold{{{scene_name} ($N_S = {int(info_table_columns[0].get_value_from_file(build_rows[4].path))}, N_D = {int(info_table_columns[1].get_value_from_file(build_rows[4].path))}, N_Q: {int(info_table_columns[2].get_value_from_file(build_rows[4].path))}$)}}")
                    latex_file.write('\n\n')
                    latex_file.write(latextable.draw_latex(build_speed_table, f'{scene_name} scene dynamic k-d tree build statistics', None, False, f'TAB:{scene_name}-d1', position='H'))
                    latex_file.write('\n\n')
                    latex_file.write(latextable.draw_latex(performance_table, f'{scene_name} scene dynamic ray tracing statistics', None, False, f'TAB:{scene_name}-d2', position='H'))
                    latex_file.write('\n\n')
                    if i % 2 == 0:
                        #latex_file.write('\\vspace{0.15cm}\n\\noindent\\rule{\\textwidth}{1.0pt}\n\\vspace{0.15cm}\n\n')
                        latex_file.write('\\noindent\\rule{\\textwidth}{1.0pt}\n\n')
                    else:
                        latex_file.write('\\newpage\n\n')
                
                # Append to graph arrays
                if write_graphs:
                    cpu_single_b.append(column_dict['DynamicBuildTime'].get_value_from_file(build_rows[0].path))
                    cpu_double_b.append(column_dict['DynamicBuildTime'].get_value_from_file(build_rows[1].path))
                    cpu_merge_b.append(column_dict['DynamicBuildTime'].get_value_from_file(build_rows[2].path))
                    gpu_single_b.append(column_dict['DynamicBuildTime'].get_value_from_file(build_rows[3].path))
                    gpu_double_b.append(column_dict['DynamicBuildTime'].get_value_from_file(build_rows[4].path))
                    cpu_single_p.append(column_dict['Performance'].get_value_from_file(build_rows[0].path))
                    cpu_double_p.append(column_dict['Performance'].get_value_from_file(build_rows[1].path))
                    cpu_merge_p.append(column_dict['Performance'].get_value_from_file(build_rows[2].path))
                    gpu_single_p.append(column_dict['Performance'].get_value_from_file(build_rows[3].path))
                    gpu_double_p.append(column_dict['Performance'].get_value_from_file(build_rows[4].path))
                    cpu_single_f.append(column_dict['FrameTime'].get_value_from_file(build_rows[0].path))
                    cpu_double_f.append(column_dict['FrameTime'].get_value_from_file(build_rows[1].path))
                    cpu_merge_f.append(column_dict['FrameTime'].get_value_from_file(build_rows[2].path))
                    gpu_single_f.append(column_dict['FrameTime'].get_value_from_file(build_rows[3].path))
                    gpu_double_f.append(column_dict['FrameTime'].get_value_from_file(build_rows[4].path))

                i += 1
    
    # Create graphs
    dyn_colors = ['blue','navy','green', 'orange', 'chocolate']
    if write_graphs:
        fpath_template = str(benchmarker.release_path / "Tables" / "latex" / '{}.svg')
        fig, ax = plt.subplots(1,1)
        plot_bars(fig, ax, [cpu_double_b, cpu_merge_b, gpu_double_b], ['CPU double', 'CPU merge', 'GPU double'], [dyn_colors[0], dyn_colors[2], dyn_colors[3]], 0.6, 'Time [ms]', '', benchmarker.dynamic_scenes, fpath_template.format('dynamic-build'))
        if display_graphs:
            plt.show()
        plt.close()
        fig, ax = plt.subplots(1,1)
        plot_bars(fig, ax, [cpu_single_p, cpu_double_p, cpu_merge_p, gpu_single_p, gpu_double_p], ['CPU single', 'CPU dobule', 'CPU merge', 'GPU single', 'GPU double'], dyn_colors, 0.8, 'PERF [MR/s]', '', benchmarker.dynamic_scenes, fpath_template.format('dynamic-performance'))
        if display_graphs:
            plt.show()
        plt.close()
        fig, ax = plt.subplots(1,1)
        plot_bars(fig, ax, [cpu_single_f, cpu_double_f, cpu_merge_f, gpu_single_f, gpu_double_f], ['CPU single', 'CPU dobule', 'CPU merge', 'GPU single', 'GPU double'], dyn_colors, 0.8, 'Time [s]', '', benchmarker.dynamic_scenes, fpath_template.format('dynamic-frame'))
        if display_graphs:
            plt.show()
        plt.close()
