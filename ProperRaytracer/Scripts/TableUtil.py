"""
 * File name: TableUtil.py
 * Description: Python script for creating tables from statistics in json files.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
"""

from texttable import Texttable
from pathlib import Path
import json
from typing import Any

class Formatter:
    def __init__(self, value_type : str = 'a', precision : int = 2):
        self.value_type = value_type
        self.precision = precision
    
    def __call__(self, value):
        if value is None:
            return "---"
        
        value_type = ''
        if self.value_type == 'a':
            if isinstance(value, int):
                value_type = 'i'
            elif isinstance(value, float):
                value_type = 'f'
        else:
            value_type = self.value_type
        
        if value_type == 'i':
            return str(int(value))
        if value_type == 'f':
            return "{:.{prec}f}".format(value, prec=self.precision)
        
        raise RuntimeError()


def take_median(l : list | Any | None):
    if isinstance(l, list):
        return sorted(l)[len(l) // 2]
    
    return l

def take_average(l : list | Any | None):
    if isinstance(l, list):
        return (sum(l) / len(l))
    
    return l

def take_first(l : list | Any):
    if isinstance(l, list):
        return l[0]

    return l


class ValueGetter:
    def __init__(self, key : str, take_method = take_average):
        self.key = key
        self.take_method = take_method
    
    def __call__(self, j):
        return self.take_method(j.get(self.key))

class AddingGetter:
    def __init__(self, keys : list[str], take_method = take_average):
        self.keys = keys
        self.take_method = take_method
    
    def __call__(self, j):
        values = [self.take_method(j.get(key)) for key in self.keys]
        if all(value is None for value in values):
            return None
        
        return sum([value if value is not None else 0 for value in values])

class RatioGetter:
    def __init__(self, key1 : str, key2 : str, take_method = take_average):
        self.keys = key1, key2
        self.take_method = take_method
    
    def __call__(self, j):
        if (self.keys[0] not in j or self.keys[1] not in j):
            return None
        return self.take_method(j[self.keys[0]]) / self.take_method(j[self.keys[1]])

class MultGetter:
    def __init__(self, keys : list[str], take_method = take_average):
        self.keys = keys
        self.take_method = take_method
    
    def __call__(self, j):
        values = [self.take_method(j.get(key)) for key in self.keys]
        if any(value is None for value in values):
            return None
        res = 1
        for value in values:
            res *= value
        return res

class ChainGetter:
    def __init__(self, getter1, op, getter2):
        self.getter1 = getter1
        self.op = op
        self.getter2 = getter2
    
    def __call__(self, j):
        return self.op(self.getter1(j), self.getter2(j))

class ConvertGetter:
    def __init__(self, getter, op):
        self.getter = getter
        self.op = op
    
    def __call__(self, j):
        return self.op(self.getter(j))
    
class ConstantGetter:
    def __init__(self, value):
        self.value = value
    
    def __call__(self, _):
        return self.value

class Row:
    """
    Class for storing the name of a table row and the path to the config file it represents.
    """
    def __init__(self, path : Path, name : str, order : int = -1):
        self.path = path
        self.name = name
        self.order = order

class Column:
    """
    Class for fetching statistics using value_getter.
    """

    def __init__(self, value_getter : str | Any, name : str):
        if isinstance(value_getter, str):
            self.value_getter = ValueGetter(value_getter)
        else:
            self.value_getter = value_getter
        
        self.name = name

    def get_value_from_file(self, path : Path):
        with open(path, 'r', encoding="utf-8") as file:
            j = json.load(file)
        data_json : dict = j['data']
        data_json.update(j['info'])
        return self.get_value(data_json)
    
    def get_value(self, json):
        return self.value_getter(json)

def make_table_rows(first_col_name : str, rows : list[Row], columns : list[Column]) -> list[list]:
    """
    Creates a list of table rows where the first column represents names of configurations,
    the rows have a statistics file for its configuration attached,
    and the rest of the columns represent statistics to be retrieved from each file.
    """
    table_rows = []
    # add first header row
    table_rows.append([first_col_name] + [f'\\multicolumn{{1}}{{c|}}{{{column.name}}}' for column in columns])

    # add rows with data
    for row in rows:
        # read json
        with open(row.path, 'r', encoding="utf-8") as file:
            j = json.load(file)
        
        table_row = [row.name]
        data_json : dict = j['data']
        data_json.update(j['info'])
        for column in columns:
            table_row.append(column.get_value(data_json))
        
        table_rows.append(table_row)
    
    return table_rows

def make_table(first_col_name : str, rows : list[Row], columns : list[Column], format = None) -> Texttable:
    """
    Creates a table where the first column represents names of configurations,
    the rows have a statistics file for its configuration attached,
    and the rest of the columns represent statistics to be retrieved from each file.
    """
    table = Texttable() if format is None else format(Texttable())
    table.add_rows(make_table_rows(first_col_name, rows, columns))
    return table

if __name__ == "__main__":
    pass
