/**
 * File name: glfwInclude.hpp
 * Description: Customized glfw include file. Contains the required macros in addition to the header file.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2023 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#define VKFW_NO_INCLUDE_VULKAN
#define VKFW_NO_INCLUDE_VULKAN_HPP
//#define VKFW_INCLUDE_GL
//#include "glbinding/gl/gl.h"
#include "Cvelth/vkfw.hpp"
