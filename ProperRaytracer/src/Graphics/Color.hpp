/**
 * File name: Color.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <type_traits>

#include "Geometry/Vector.hpp"
#include "Utilities/Utilities.hpp"

namespace ProperRT
{
    /// @brief Color vector with 4 components, using cfloat.
    using Color = Vector<4, cfloat>;

    /// @brief Color vector using four 32 bit floating point components.
    using Color32f = Vector<4, float>;

    /// @brief Color vector using four 64 bit floating point components.
    using Color64f = Vector<4, double>;

    /// @brief Color vector using four 8 bit integer components, fitting in 32 bits.
    using Color32i = Vector<4, uint8>;

    /// @brief Color vector using four 16 bit integer components, fitting in 64 bits.
    using Color64i = Vector<4, uint16>;

    template<class T>
    concept ColorVector = (T::DIMENSION == 4 && IsAnyOf<typename T::DataType, float, double, uint8, uint16>);

    /// @brief Color with integer components.
    template<class T>
    concept IntegerColor = std::is_integral_v<typename T::DataType>;

    /// @brief Color with floating point components
    template<class T>
    concept RealColor = std::is_floating_point_v<typename T::DataType>;

    /// @brief Converts an input color with integer components to a color with floating point components.
    /// @details The input color is normalized into the <0,1> range by dividing the input by int_max.
    /// @tparam TTarget Type of the target color, with floating point components
    /// @param input Input color with integer components
    /// @return Color converted from <0, int_max> to <0, 1>
    template<RealColor TTarget>
    inline
    TTarget NormalizedConvert(IntegerColor auto input) {
        using TSource = std::remove_cvref_t<decltype(input)>;
        using TSourceData = typename TSource::DataType;
        using TTargetData = typename TTarget::DataType;
        return input.Transform([] (TSourceData component) { return IntegerToRealNormalized<TTargetData, TSourceData>(component); });
    }

    /// @brief Converts an input color with floating point components to a color with integer components.
    /// @details The input color is clamped to and converted from the <0,1> range by multiplying the input by int_max.
    /// @tparam TTarget Type of the target color, with integer components
    /// @param input Input color with floating point components
    /// @return Color converted from <0, 1> to <0, int_max>
    template<IntegerColor TTarget>
    inline
    TTarget NormalizedConvert(RealColor auto input) {
        using TSource = std::remove_cvref_t<decltype(input)>;
        using TSourceData = typename TSource::DataType;
        using TTargetData = typename TTarget::DataType;
        return input.Clamp(TSourceData{ 0 }, TSourceData{ 1 }).Transform([] (TSourceData component) { return RealToIntegerNormalized<TTargetData, TSourceData>(component); });
    }

    /// @brief Converts an input color with integer components of type TSource to a color with integer components of type TTarget.
    /// @details The input color converted from <0,TSource::int_max> to <0,TTarget::int_max>, while keeping ratios.
    /// @tparam TTarget Type of the target color
    /// @param input Input color
    /// @return Color converted from <0,TSource::int_max> to <0,TTarget::int_max>
    template<IntegerColor TTarget>
    inline
    TTarget NormalizedConvert(IntegerColor auto input) {
        using TSource = std::remove_cvref_t<decltype(input)>;
        using TSourceData = typename TSource::DataType;
        using TTargetData = typename TTarget::DataType;
        return input.Transform([] (TSourceData component) { return RealToIntegerNormalized<TTargetData, double>(IntegerToRealNormalized<double, TSourceData>(component)); });
    }

    /// @brief Converts floating point colors with a static cast.
    template<RealColor TTarget>
    inline
    TTarget NormalizedConvert(RealColor auto const& input) {
        return TTarget{ input };
    }
}
