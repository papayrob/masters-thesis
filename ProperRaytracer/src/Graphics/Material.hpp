/**
 * File name: Material.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "Color.hpp"

namespace ProperRT
{
    /// @brief Class for storing information about a mesh material.
    class Material
    {
    public:
        /// @brief Diffuse color of the material.
        Color32f colorDiffuse{ 1.f };
        //Color32f colorSpecular{ 0.0f };
    };
}
