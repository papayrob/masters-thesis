/**
 * File name: ImageBuffer.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2022 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <vector>
#include <algorithm>
#include <thread>
#include <atomic>
#include <mutex>
#include <queue>

#include "Color.hpp"

namespace ProperRT
{
    class ImageWriter;

    /// @brief Class for storing and manipulating a buffer used for rendering, e.g. color or depth buffer.
    /// @details Modifying the size of the underlying buffer leads to undefined behavior. Create a new instance to resize the buffer.
    /// @tparam TData Type stored in the buffer
    template <typename TData>
    class ImageBuffer
    {
        template<typename TOther>
        friend class ImageBuffer;

        friend class ImageWriter;

    public:
        using SizeType = int32;

    public:
        std::vector<TData> buffer{};

        /// @brief Value with which the buffer is cleared.
        TData clearValue{};

    public:
        /// @brief Constructs the image buffer with dimensions @p width x @p height.
        /// @param width Width of the buffer
        /// @param height Height of the buffer
        ImageBuffer(SizeType width, SizeType height, TData clearValue_ = TData{})
            : buffer{ width * height, clearValue_ }, clearValue{ clearValue_ }, dimensions{ width, height } {}

        /// @brief Constructs the image buffer with @p dimensions_.
        /// @param dimensions_ Width and height of the buffer
        explicit
        ImageBuffer(Vector<2, SizeType> dimensions_, TData clearValue_ = TData{})
            : buffer(dimensions_.ElementProduct(), clearValue_), clearValue{ clearValue_ }, dimensions(dimensions_) {}

        /// @brief Constructs a new buffer with elements from @p other converted using @p converter.
        /// @tparam TOther Type stored in @p other
        /// @tparam FConverter Converter functor type
        /// @param other Buffer to copy and convert from
        /// @param converter Functor that converts type stored in @p other to the type stored in this buffer
        template<class TOther, class FConverter>
            requires requires (FConverter converter, TOther elem) { { converter(elem) } -> std::same_as<TData>; }
        ImageBuffer(ImageBuffer<TOther> const& other, FConverter&& converter)
            : buffer{}, clearValue{ converter(other.clearValue) }, dimensions{ other.dimensions }
        {
            buffer.reserve(dimensions.ElementProduct());
            for (auto const& elem : other.buffer) {
                buffer.push_back(converter(elem));
            }
        }

        /// @brief Returns reference to pixel value at (u,v) with no bounds checking.
        /// @param u Width coordinate
        /// @param v Height coordinate
        /// @return Reference to pixel value at (u,v)
        TData& operator()(SizeType u, SizeType v) { return buffer[Coord2Direct(u, v, dimensions.x())]; }

        /// @brief Returns reference to pixel value at (u,v) with no bounds checking.
        /// @param u Width coordinate
        /// @param v Height coordinate
        /// @return Const reference to pixel value at (u,v)
        TData const& operator()(SizeType u, SizeType v) const { return buffer[Coord2Direct(u, v, dimensions.x())]; }

        /// @brief Returns reference to pixel value at (u,v) with bounds checking.
        /// @param u Width coordinate
        /// @param v Height coordinate
        /// @return Reference to pixel value at (u,v)
        TData& At(SizeType u, SizeType v) {
            if (u != ClampWidth(u)) throw std::out_of_range{ "u out of range" };
            if (v != ClampWidth(v)) throw std::out_of_range{ "v out of range" };
            return buffer[Coord2Direct(u, v, dimensions.x())];
        }

        /// @brief Returns reference to pixel value at (u,v) with bounds checking.
        /// @param u Width coordinate
        /// @param v Height coordinate
        /// @return Const reference to pixel value at (u,v)
        TData const& At(SizeType u, SizeType v) const {
            if (u != ClampWidth(u)) throw std::out_of_range{ "u out of range" };
            if (v != ClampHeight(v)) throw std::out_of_range{ "v out of range" };
            return buffer[Coord2Direct(u, v, dimensions.x())];
        }

        /// @brief Clamps width coordinate to <0, width>.
        /// @param u Width coordinate to be clamped
        /// @return Clamped width coordinate
        SizeType ClampWidth(SizeType u) const {
            if (u < 0) return 0;
            if (static_cast<SizeType>(u) >= dimensions.x()) return dimensions.x() - 1;
            return static_cast<SizeType>(u);
        }

        /// @brief Clamps height coordinate to <0, height>.
        /// @param u Height coordinate to be clamped
        /// @return Clamped height coordinate
        SizeType ClampHeight(SizeType v) const {
            if (v < 0) return 0;
            if (static_cast<SizeType>(v) >= dimensions.y()) return dimensions.y() - 1;
            return static_cast<SizeType>(v);
        }

        /// @brief Fills the buffer with @p fillWith.
        void Fill(TData const& fillWith) { std::fill(buffer.begin(), buffer.end(), fillWith); }

        /// @brief Fills the buffer with clear value.
        void Clear() { Fill(clearValue); }

        /// @brief Returns the width of the buffer.
        /// @return Width of the buffer
        Vector<2, SizeType> Dimensions() const { return dimensions; }

        /// @brief Flips the image along the horizontal axis.
        void FlipAlongU() {
            SizeType width = dimensions.x();
            SizeType height = dimensions.y();
            SizeType half = height / 2;
            for (SizeType i = 0; i < half; ++i) {
                std::swap_ranges(buffer.begin() + (width * i), buffer.begin() + (width * (i + 1)), buffer.begin() + (width * (height - i - 1)));
            }
        }

    private:
        struct WriteData
        {
            std::unique_ptr<ImageBuffer> buffer;
            std::string filePath;
            bool flipOnSave;
        };

        static inline
        std::mutex saveMutex{};

        static inline
        std::queue<WriteData> saveQueue{};

        Vector<2, SizeType> dimensions;

        // Maps 2D coords to 1D buffer coords
        static constexpr
        auto Coord2Direct(SizeType u, SizeType v, SizeType width) { return v * width + u; }

        static
        int EmptyQueue(ImageWriter* writer) requires ColorVector<TData>;
    };

    /// @brief Writes images to disk in a separate thread and guarantees thread-safety.
    class ImageWriter
    {
        template<typename T>
        friend class ImageBuffer;
        
    public:
        /// @brief Singleton accessor.
        /// @return ImageWriter singleton instance
        static
        ImageWriter* Instance() { return (instance == nullptr) ? (instance = std::unique_ptr<ImageWriter>(new ImageWriter)).get() : instance.get(); }

        /// @brief Stops the worker thread, blocks
        static
        void StopWorkerThread();

        /// @brief Pushes a write of @p imageBuffer to the queue, which is processed by another thread.
        /// @tparam TColor Color type of the buffer
        /// @param imageBuffer Image buffer to write
        /// @param filePath Path with the extension (relative path based in Assets)
        /// @param flipVerticallyOnWrite If true, the image will be flipped vertically when writing
        template<ColorVector TColor>
        void PushWrite(ImageBuffer<TColor> imageBuffer, std::string filePath, bool flipVerticallyOnWrite = false);

    private:
        static inline
        std::unique_ptr<ImageWriter> instance{};

        std::atomic_int n{ 0 };
        std::jthread consumer{};

        static
        void Run();

        ImageWriter();

        template<ColorVector TColor>
        void SaveToDisk(std::string const& filePathNoExt, ImageBuffer<TColor> const& imageBuffer);

        std::string FullPath(std::filesystem::path const& relativePath);
    };
}
