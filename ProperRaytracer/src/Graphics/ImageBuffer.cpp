/**
 * File name: ImageBuffer.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "ImageBuffer.hpp"

#include "stb_image_write.h"
#include "SceneDefinition/AssetManager.hpp"
#include "Utilities/Logger.hpp"

namespace ProperRT
{
    template <typename TData>
    int ImageBuffer<TData>::EmptyQueue(ImageWriter *writer) requires ColorVector<TData> {
        std::size_t size;
        {
            std::lock_guard lockGuard{ saveMutex };
            size = saveQueue.size();
        }
        int processed{ 0 };

        while (size > 0) {
            // Retrieve item
            WriteData item;
            {
                std::lock_guard lockGuard{ saveMutex };
                item = std::move(saveQueue.front());
                saveQueue.pop();
                size = saveQueue.size();
            }

            // Process item
            stbi_flip_vertically_on_write(item.flipOnSave);
            writer->SaveToDisk(item.filePath, *item.buffer);

            ++processed;
        }

        return processed;
    }

    void ImageWriter::StopWorkerThread() {
        if (instance != nullptr) {
            // Wait for worker to finish (cannot wait() because worker does not notify)
            while (instance->n.load() > 0) {}
            // Send signal
            instance->n.fetch_sub(1);
            instance->n.notify_one();
            instance = nullptr;
        }
    }

    void ImageWriter::Run() {
        ImageWriter* instance = Instance();

        while (true) {
            instance->n.wait(0);

            // Stop execution
            if (instance->n.load() < 0) {
                return;
            }

            // Empty queues
            int processed{ 0 };
            do {
                processed += ImageBuffer<Vector<4, float>>::EmptyQueue(instance);
                processed += ImageBuffer<Vector<4, double>>::EmptyQueue(instance);
                processed += ImageBuffer<Vector<4, uint8>>::EmptyQueue(instance);
                processed += ImageBuffer<Vector<4, uint16>>::EmptyQueue(instance);
            } while (instance->n.fetch_sub(processed) - processed > 0);
        }
    }

    ImageWriter::ImageWriter()
        : consumer{ ImageWriter::Run }
    {
    }

    template<ColorVector TColor>
    void ImageWriter::PushWrite(ImageBuffer<TColor> imageBuffer, std::string filePath, bool flipVerticallyOnWrite) {
        {
            std::lock_guard lockGuard{ imageBuffer.saveMutex };
            imageBuffer.saveQueue.push(typename ImageBuffer<TColor>::WriteData{ std::make_unique<ImageBuffer<TColor>>(std::move(imageBuffer)), std::move(filePath), flipVerticallyOnWrite });
        }
        ++n;
        n.notify_one();
    }

    template
    void ImageWriter::PushWrite(ImageBuffer<Color32f>, std::string, bool);

    template
    void ImageWriter::PushWrite(ImageBuffer<Color64f>, std::string, bool);

    template
    void ImageWriter::PushWrite(ImageBuffer<Color32i>, std::string, bool);

    template
    void ImageWriter::PushWrite(ImageBuffer<Color64i>, std::string, bool);

    template<ColorVector TColor>
    void ImageWriter::SaveToDisk(std::string const& filePath, ImageBuffer<TColor> const& imageBuffer) {
        if constexpr (std::same_as<typename TColor::DataType, float>) {
            std::filesystem::path path{ filePath };
            if (path.has_extension() && path.extension() == ".hdr") {
                stbi_write_hdr(
                    FullPath(path).c_str(),
                    imageBuffer.Dimensions().x(), imageBuffer.Dimensions().y(),
                    4, reinterpret_cast<float const*>(imageBuffer.buffer.data())
                );
                return;
            }
        }
        if constexpr (std::same_as<typename TColor::DataType, double>) {
            std::filesystem::path path{ filePath };
            if (path.has_extension() && path.extension() == ".hdr") {
                SaveToDisk(filePath, ImageBuffer<Color32i>(imageBuffer, [] (TColor const& c) {
                    return NormalizedConvert<Color32i>(c);
                }));
                return;
            }
        }

        SaveToDisk(filePath, ImageBuffer<Color32i>(imageBuffer, [] (TColor const& c) {
            return NormalizedConvert<Color32i>(c);
        }));
    }

    template
    void ImageWriter::SaveToDisk(std::string const&, ImageBuffer<Color32f> const&);

    template
    void ImageWriter::SaveToDisk(std::string const&, ImageBuffer<Color64f> const&);

    template<>
    void ImageWriter::SaveToDisk<Color32i>(std::string const& filePath, ImageBuffer<Color32i> const& imageBuffer) {
        std::filesystem::path path{ filePath };
        if (!path.has_extension()) {
            //TODO throw "Cannot deduce image type without extension"
        }

        auto ext = path.extension();
        auto fullPath = FullPath(path);
        if (ext == ".png") {
            stbi_write_png(
                fullPath.c_str(),
                imageBuffer.Dimensions().x(), imageBuffer.Dimensions().y(),
                4, imageBuffer.buffer.data(),
                imageBuffer.Dimensions().x() * sizeof(Color32i)
            );
        }
        else if (ext == ".jpg") {
            stbi_write_jpg(
                fullPath.c_str(),
                imageBuffer.Dimensions().x(), imageBuffer.Dimensions().y(),
                4, imageBuffer.buffer.data(),
                5
            );
        }
        else if (ext == ".bmp") {
            stbi_write_bmp(
                fullPath.c_str(),
                imageBuffer.Dimensions().x(), imageBuffer.Dimensions().y(),
                4, imageBuffer.buffer.data()
            );
        }
        else if (ext == ".tga") {
            stbi_write_tga(
                fullPath.c_str(),
                imageBuffer.Dimensions().x(), imageBuffer.Dimensions().y(),
                4, imageBuffer.buffer.data()
            );
        }

        //TODO throw "Invalid image file extension"
    }

    template
    void ImageWriter::SaveToDisk(std::string const&, ImageBuffer<Color64i> const&);

    std::string ImageWriter::FullPath(std::filesystem::path const& relativePath) {
        return (AssetManager::assetRootPath / relativePath).string();
    }

}
