/**
 * File name: KDTGPURaytracer.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "IRaytracer.hpp"
#include "KDTree/KDTreeGPUBase.hpp"
#include "SceneDefinition/CompRegistry.hpp"
#include "SceneDefinition/Components/Renderer.hpp"
#include "Utilities/TimerGPU.hpp"
#include "Compute/InteropTexture.hpp"

namespace ProperRT
{
    /// @brief Raytracer for k-d trees that executes on the GPU.
    class KDTGPURaytracer : public IRaytracer
    {
    public:
        KDTGPURaytracer();

        virtual
        ~KDTGPURaytracer();

        virtual
        void Init() override;

        virtual
        void BuildStatic() override;

        virtual
        void BuildDynamic() override;

        virtual
        void RaytraceScene() override;

        virtual
        ImageBuffer<Color32f> const& GetRender() override;

        virtual
        OpenGL::Texture const& GetRenderTexture() override;

        virtual
        void UpdateOpenGLResources() override;

        virtual
        void RegisterRenderer(CompPtr<Components::Renderer> renderer) override;

        virtual
        INavigator* GetNavigator() override {
            return (tree == nullptr ? nullptr : tree->TopNavigator());
        }

        virtual
        IStructureValidator* GetValidator() override {
            return (tree == nullptr ? nullptr : tree->Validator());
        }

        virtual
        std::optional<RayHit> CastRay(Ray const& ray, cfloat maxDist = std::numeric_limits<cfloat>::infinity(), TraversalStatistics* traversalStats = nullptr) const override;

        virtual
        bool CastShadowRay(Ray const& ray, cfloat maxDist, TraversalStatistics* traversalStats = nullptr) const override;

        virtual
        void SetRenderOrigin(IRaytracer::RenderOrigin origin) override { renderOrigin = origin; }

        virtual
        IAccelerationStructure* GetAccelerationStructure() override { return tree.get(); }

        virtual
        void WaitForFinish() override;

    private:
        struct GPUData;
    
    private:
        CompRegistry<Components::Renderer> renderers{};
        std::unique_ptr<KDTreeGPUBase> tree{};

        ImageBuffer<Color32f> colorBufferCPU{ 0, 0 };
        InteropTexture gpuTexture{};
        std::unique_ptr<GPUData> gpuData;
        bool renderOnCPU{ true };

        IRaytracer::RenderOrigin renderOrigin{};

        TimerGPU buildTimer;
        TimerGPU raytraceTimer;

        std::unique_ptr<KDTreeGPUBase> CreateTree() const;
    };
}
