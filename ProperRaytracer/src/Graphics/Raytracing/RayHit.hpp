/**
 * File name: RayHit.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "Geometry/Primitive.hpp"
#include "Geometry/Ray.hpp"
#include "SceneDefinition/Components/Renderer.hpp"

namespace ProperRT
{
    /// @brief Class for storing info about a ray hit.
    struct RayHit
    {
        static inline constexpr
        cfloat SHIFT_EPSILON = 0.000001_cf;

        /// @brief Standard ctor.
        /// @param ray_ Ray
        /// @param tHit_ Hit distance
        /// @param ic_ Coordinates of the intersection on the hit primitive
        /// @param primitive_ Hit primitive
        /// @param renderer_ Renderer that renders the primitive
        /// @param submeshIdx_ Submesh where the primitive is located in
        RayHit(Ray const& ray_, cfloat tHit_ , IntersectionCoordinates const& ic_, Primitive const& primitive_, CompPtr<Components::Renderer> renderer_, int32 submeshIdx_)
            : ray{ ray_ }, tHit{ tHit_ * (1._cf - SHIFT_EPSILON) }, uv{ /*TODO*/ }, point{ ray.origin + tHit * ray.direction },
              normal{ GetNormal(primitive_, ic_) }, primitive{ primitive_ }, renderer{ std::move(renderer_) }, submeshIdx{ submeshIdx_ }
        {}
        
        /// @brief Ray.
        Ray ray;
        /// @brief Hit distance.
        cfloat tHit;
        /// @brief UV coordinates of the hit used for texturing.
        Vector2cf uv;
        /// @brief Hit point.
        Vector3cf point;
        /// @brief Normal at the hit point.
        Vector3cf normal;
        /// @brief Hit primitive.
        Primitive primitive;
        /// @brief Renderer that renders the hit primitive.
        CompPtr<Components::Renderer> renderer;
        /// @brief Submesh where the primitive is located in.
        int32 submeshIdx;

        /// @brief Returns true if the ray hit something.
        operator bool() const { return tHit != std::numeric_limits<cfloat>::infinity(); }

        /// @brief Returns the material of the primitive.
        Material const& GetMaterial() const {
            return renderer->GetMesh()->Materials()[submeshIdx];
        }
    };
}
