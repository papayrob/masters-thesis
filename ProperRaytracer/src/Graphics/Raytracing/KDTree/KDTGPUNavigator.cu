/**
 * File name: KDTGPUNavigator.cu
 * Description: See KDTreeGPU header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "KDTreeGPU.hpp"

#include <glbinding/gl/gl.h>
#include "KDTGPUBuilderBase.cuh"
#include "KDTreeValidatorCommon.hpp"
#include "Graphics/Raytracing/GPUMeshManager.cuh"
#include "SceneDefinition/Components/Camera.hpp"
#include "SceneDefinition/Components/Transform.hpp"

namespace ProperRT
{
    KDTreeGPU::TreeNavigator::TreeNavigator(KDTreeGPU* tree_, bool selectedStatic_)
        : Base{ tree_ }, selectedStatic{selectedStatic_}
    {
    }

    void KDTreeGPU::TreeNavigator::StartNavigating() {
        if (selectedStatic) {
            dynamic_cast<KDTreeGPU*>(tree)->staticBuilder->Nodes();
        }
        else {
            dynamic_cast<KDTreeGPU*>(tree)->dynamicBuilder->Nodes();
        }
        Base::StartNavigating();
    }

    bool KDTreeGPU::TreeNavigator::CanNavigateInto() {
        return node->PrimitiveCount() > 0;
    }

    IMeshManager const& KDTreeGPU::TreeNavigator::GetMeshManager() {
        if (selectedStatic) {
            return *dynamic_cast<KDTreeGPU*>(tree)->staticMeshManager.get();
        }
        else {
            return *dynamic_cast<KDTreeGPU*>(tree)->dynamicMeshManager.get();
        }
    }

    std::unique_ptr<BinaryTreeNodeNavigator> KDTreeGPU::TreeNavigator::CreateNodeNavigator() {
        return std::make_unique<KDTreeGPU::NodeNavigator>(this);
    }

    KDTreeGPU::NodeNavigator::NodeNavigator(KDTreeGPU::TreeNavigator* parentNavigator_)
        : Base{ parentNavigator_ }
    {}

    KDTreeGPU::ChooseTreeNavigator::ChooseTreeNavigator(KDTreeGPU* tree_)
        : tree{tree_}
    {
        boundingBoxMesh = std::make_shared<OrientedBoundingBoxMesh>(std::vector<OrientedBoundingBox>{OrientedBoundingBox{}, OrientedBoundingBox{}});
        boundingBoxMesh->SetMaterial(Material{.colorDiffuse{NormalizedConvert<Color32f>(KDTreeNavigator::boundingBoxColor)}});
    }

    void KDTreeGPU::ChooseTreeNavigator::StartNavigating() {
        if (tree->GetStaticMeshManager() != nullptr) {
            boundingBoxMesh->Boxes()[0] = tree->GetStaticMeshManager()->SceneBoundingBox();
        }
        else {
            boundingBoxMesh->Boxes()[0] = OrientedBoundingBox{};
        }

        boundingBoxMesh->Boxes()[1] = tree->GetDynamicMeshManager()->SceneBoundingBox();
        boundingBoxMesh->SyncToGPU();
    }

    void KDTreeGPU::ChooseTreeNavigator::Navigate() {
        ImGui::RadioButton("Static tree", &radioValue, 0);
        ImGui::RadioButton("Dynamic tree", &radioValue, 1);
    }

    void KDTreeGPU::ChooseTreeNavigator::NavigateOut() {
        boundingBoxMesh->Boxes()[0] = OrientedBoundingBox{};
        boundingBoxMesh->Boxes()[1] = OrientedBoundingBox{};
    }

    bool KDTreeGPU::ChooseTreeNavigator::CanNavigateInto() {
        if (radioValue == 0) {
            return (tree->GetStaticBuilder() != nullptr && tree->GetStaticBuilder()->IsBuilt());
        }
        else if (radioValue == 1) {
            return tree->GetDynamicBuilder()->IsBuilt();
        }
        else {
            return false;
        }
    }

    INavigator* KDTreeGPU::ChooseTreeNavigator::NavigateInto() {
        if (radioValue == 0) {
            tree->staticNavigator.StartNavigating();
            return &tree->staticNavigator;
        }
        else if (radioValue == 1) {
            tree->dynamicNavigator.StartNavigating();
            return &tree->dynamicNavigator;
        }
        else {
            return nullptr;
        }
    }

    void KDTreeGPU::ChooseTreeNavigator::Render() {
        gl::GLenum oldMode[2];
        gl::glGetIntegerv(gl::GLenum::GL_POLYGON_MODE, &oldMode[0]);
        gl::glPolygonMode(gl::GLenum::GL_FRONT_AND_BACK, gl::GLenum::GL_LINE);

        auto mat{Matrix4x4f::Identity()};
        auto camMats = Components::Camera::MainCamera()->GetCameraMatrices();

        boundingBoxMesh->Rasterize(mat, camMats);

        gl::glPolygonMode(gl::GLenum::GL_FRONT_AND_BACK, oldMode[0]);
    }

    std::optional<std::string> KDTreeGPU::ChooseTreeNavigator::Validate() {
        auto& stats = tree->GetStatistics();
        stats.ResetBuildStats();

        std::vector<int32> depths{};
        std::vector<int32> primitiveCounts{};
        
        std::optional<std::string> ret;
        if (tree->GetStaticBuilder() != nullptr && tree->GetStaticMeshManager() != nullptr) {
            auto sret = KDTreeValidateCommon(*tree, tree->GetStaticBuilder()->Root(), *tree->GetStaticMeshManager(), *tree->GetStaticBuilder(), tree->staticBuilder->Nodes(), depths, primitiveCounts);
            if (sret.has_value()) {
                ret = "Static: " + sret.value();
            }
        }

        if (tree->GetDynamicBuilder() != nullptr && tree->GetDynamicMeshManager() != nullptr) {
            auto dret = KDTreeValidateCommon(*tree, tree->GetDynamicBuilder()->Root(), *tree->GetDynamicMeshManager(), *tree->GetDynamicBuilder(), tree->dynamicBuilder->Nodes(), depths, primitiveCounts);
            if (dret.has_value()) {
                if (ret.has_value()) {
                    ret = ret.value() + "\nDynamic: " + dret.value();
                }
                else {
                    ret = "Dynamic: " + dret.value();
                }
            }
        }

        stats.avgPrimitivesInLeaf = static_cast<float>(std::reduce(primitiveCounts.begin(), primitiveCounts.end())) / static_cast<float>(primitiveCounts.size());
        stats.avgDepth = static_cast<float>(std::reduce(depths.begin(), depths.end())) / static_cast<float>(depths.size());

        auto& appStats = RTApp::Instance()->statistics;
        appStats.AddData("MemoryConsumption", stats.memoryConsumption);
        appStats.AddData("Cost", stats.cost);
        appStats.AddData("InnerNodes", stats.innerNodes);
        appStats.AddData("LeafNodes", stats.leafNodes);
        appStats.AddData("EmptyNodes", stats.emptyNodes);
        appStats.AddData("AveragePrimitivesInLeaf", stats.avgPrimitivesInLeaf);
        appStats.AddData("MaxPrimitivesInLeaf", stats.maxPrimitivesInLeaf);
        appStats.AddData("AverageDepth", stats.avgDepth);
        appStats.AddData("MaxDepth", stats.maxDepth);

        return ret;
    }
}
