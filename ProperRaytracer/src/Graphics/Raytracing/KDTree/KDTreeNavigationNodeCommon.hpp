/**
 * File name: KDTreeNavigationNodeCommon.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "KDTreeNavigator.hpp"

namespace ProperRT
{
    /// @brief Common implementation class for the specialization of k-d tree navigation node.
    /// @tparam TBuilder Tree builder type
    template<class TBuilder>
    class KDTreeNavigationNodeCommon : public KDTreeNavigationNode
    {
    public:
        using Node = KDTNode;

    public:
        /// @brief Standard ctor.
        /// @param builder_ Builder the tree was built with
        /// @param meshManager_ Mesh manager the tree was built over
        /// @param node Starting node
        /// @param nodeBoundingBox Bounding box of the node
        /// @param meshTreeNode_ Mesh tree node corresponding to @p node
        /// @param keepHistory_ If true, keeps navigation history, if false, cannot navigate up
        KDTreeNavigationNodeCommon(TBuilder& builder_, IMeshManager& meshManager_, Node const* node, AABB const& nodeBoundingBox, BinaryTreeMesh::Node* meshTreeNode_, bool keepHistory_ = true)
            : builder{ builder_ }, meshManager{meshManager_}, meshTreeNode{ meshTreeNode_ }, keepHistory{ keepHistory_ }
        {
            nodeStack.push(StackItem{ node, nodeBoundingBox });
        }

        virtual
        AABB BoundingBox() const override { return nodeStack.top().boundingBox; }

        virtual
        bool IsLeaf() const override { return nodeStack.top().node->IsLeaf(); }

        virtual
        bool CanGoUp() override { return nodeStack.size() > 1; }
        
        virtual
        void GoUp() override {
            nodeStack.pop();
            if (meshTreeNode != nullptr) {
                meshTreeNode = meshTreeNode->Parent();
            }
        }

        virtual
        bool CanGoLeft() override { return nodeStack.top().node->IsInner(); }

        virtual
        void GoLeft() override {
            StackItem top = nodeStack.top();

            StackItem item;
            item.node = builder.GetLeftChild(top.node->inner);

            AABB otherBB;
            top.boundingBox.SplitCoord(SplitAxis(), SplitCoordinate(), item.boundingBox, otherBB);

            nodeStack.push(item);

            if (meshTreeNode != nullptr) {
                meshTreeNode = meshTreeNode->Left();
            }

            if constexpr (std::same_as<std::vector<PUID>, decltype(builder.LeafPrimitives(nodeStack.top().node->leaf))>) {
                if (nodeStack.top().node->IsLeaf()) {
                    primitivesCache = builder.LeafPrimitives(nodeStack.top().node->leaf);
                }
            }
        }

        virtual
        bool CanGoRight() override { return nodeStack.top().node->IsInner(); }

        virtual
        void GoRight() override {
            StackItem top = nodeStack.top();

            StackItem item;
            item.node = builder.GetRightChild(top.node->inner);

            AABB otherBB;
            top.boundingBox.SplitCoord(SplitAxis(), SplitCoordinate(), otherBB, item.boundingBox);

            nodeStack.push(item);

            if (meshTreeNode != nullptr) {
                meshTreeNode = meshTreeNode->Right();
            }

            if constexpr (std::same_as<std::vector<PUID>, decltype(builder.LeafPrimitives(nodeStack.top().node->leaf))>) {
                if (nodeStack.top().node->IsLeaf()) {
                    primitivesCache = builder.LeafPrimitives(nodeStack.top().node->leaf);
                }
            }
        }

        virtual
        int32 PrimitiveCount() override {
            if (meshTreeNode != nullptr) {
                return StdSize<int32>(meshTreeNode->GetPrimitives());
            }
            else {
                return 0;
            }
        }

        virtual
        Primitive GetPrimitive(int32 primitiveIdx) override {
            if (meshTreeNode != nullptr) {
                return meshManager.GetPrimitive(meshTreeNode->GetPrimitives()[primitiveIdx]);
            }
            else {
                return Primitive{};
            }
        }

        virtual
        Axis3D SplitAxis() const override { return ToAxis3D(nodeStack.top().node->inner.tag); }
        
        virtual
        float SplitCoordinate() const override { return (nodeStack.top().node->IsLeaf() ? 0.0f : nodeStack.top().node->inner.splitValue); }

        virtual
        std::span<PUID const> LeafPrimitives() const override {
            if constexpr (std::same_as<std::vector<PUID>, decltype(builder.LeafPrimitives(nodeStack.top().node->leaf))>) {
                return (
                    nodeStack.top().node->IsLeaf() ?
                    std::span<PUID const>{primitivesCache} :
                    std::span<PUID const>{}
                );
            }
            else {
                return (
                    nodeStack.top().node->IsLeaf() ?
                    builder.LeafPrimitives(nodeStack.top().node->leaf) :
                    std::span<PUID const>{}
                );
            }
        }

        virtual
        std::unique_ptr<KDTreeNavigationNode> NoParentCopy() const override {
            return std::make_unique<KDTreeNavigationNodeCommon>(builder, meshManager, nodeStack.top().node, nodeStack.top().boundingBox, meshTreeNode, false);
        }

    private:
        struct StackItem
        {
            Node const* node;
            AABB boundingBox;
        };

        TBuilder& builder;
        IMeshManager& meshManager;
        std::stack<StackItem> nodeStack{};
        BinaryTreeMesh::Node* meshTreeNode{};
        std::vector<PUID> primitivesCache{};
        bool keepHistory;
    };
}
