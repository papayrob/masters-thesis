/**
 * File name: KDTEventsCPUBuilder.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "KDTEventsCPUBuilder.hpp"

namespace ProperRT
{
    KDTEventsCPUBuilder::~KDTEventsCPUBuilder() {
        Reset();
    }

    void KDTEventsCPUBuilder::Build(IMeshManager& meshManager, std::vector<PUID> primitives_, std::vector<AABB> primitiveBBs_, KDTreeParams const &parameters) {
        if (primitives_.size() != primitiveBBs_.size() || primitives_.size() <= 0) {
            //TODO throw
            assert(false);
        }
        
        Reset();

        // Init
        std::stack<BuildItem> recursion;
        nodes.push_back(Node{});

        if (primitives_.size() == 0) {
            nodes.back().leaf = KDTCPUBuilderBase::LeafNode{ KDTNodeType::Leaf, 0, nullptr };
            isBuilt = true;
        }

        int32 primitiveN_ = StdSize<int32>(primitives_);
        recursion.push(BuildItem{
            .node{ 0 },
            .boundingBox{ meshManager.SceneBoundingBox() },
            .primitives{std::move(primitives_)},
            .primitiveBBs{std::move(primitiveBBs_)},
            .depth{ 0 }
        });

        CreateAndSortEvents(recursion.top());

        int32 maxDepth = MaxDepth(primitiveN_, parameters);

        while (!recursion.empty()) {
            // Init
            BuildItem item = std::move(recursion.top());
            recursion.pop();

            BestSplit bestSplit{};

            if (StdSize<int32>(item.primitives) > parameters.primitiveLimit && item.depth <= maxDepth) {
                bestSplit = FindBestPlane(item, parameters);
            }

            // Inner node
            if (bestSplit.plane.axis != Axis3D::None) {
                auto leftIdx = StdSize<int32>(nodes);
                
                nodes.push_back(Node{});
                recursion.push(BuildItem{ .node{ leftIdx }, .depth{ item.depth + 1 } });
                BuildItem& leftItem = recursion.top();

                nodes.push_back(Node{});
                recursion.push(BuildItem{ .node{ leftIdx + 1 }, .depth{ item.depth + 1 } });
                BuildItem& rightItem = recursion.top();

                item.boundingBox.SplitCoord(bestSplit.plane.axis, bestSplit.plane.coordinate, leftItem.boundingBox, rightItem.boundingBox);
                StepItem stepItem{item.primitives.size()};

                // Distribute
                ClassifyAndDistribute(item, stepItem, leftItem, rightItem, bestSplit, meshManager);
                DistributeAndSortEvents(item, stepItem, leftItem, rightItem, bestSplit);

                nodes[item.node].inner = KDTCPUBuilderBase::InnerNode{
                    .tag{ ToNodeType(bestSplit.plane.axis) },
                    .splitValue{ bestSplit.plane.coordinate },
                    .leftChildIdx{ leftIdx },
                    .rightChildIdx{ leftIdx + 1 }
                };
            }
            // Leaf node
            else {
                int32 primitiveN = StdSize<int32>(item.primitives);
                PUID* primitives = ((primitiveN > 0) ? new PUID[primitiveN] : nullptr);
                nodes[item.node].leaf = KDTCPUBuilderBase::LeafNode{ KDTNodeType::Leaf, primitiveN, primitives };
                std::copy(item.primitives.begin(), item.primitives.end(), primitives);
            }
        }

        isBuilt = true;
        Logger::LogInfo("Node count: " + std::to_string(nodes.size()));
    }

    void KDTEventsCPUBuilder::CreateAndSortEvents(BuildItem& item) {
        // Add start and end events for each axis
        for (int axis = 0; axis < 3; ++axis) {
            int32 primitiveN = StdSize<int32>(item.primitives);
            item.events[axis].reserve(primitiveN);
            for (int32 primitiveI = 0; primitiveI < primitiveN; ++primitiveI) {
                assert(item.primitiveBBs[primitiveI].minBounds[axis] != item.primitiveBBs[primitiveI].maxBounds[axis]);

                item.events[axis].push_back(Event{SplitEventType::Start, primitiveI});
                item.events[axis].push_back(Event{SplitEventType::End, primitiveI});
            }

            // Sort events
            std::sort(item.events[axis].begin(), item.events[axis].end(), EventLess{item.primitiveBBs, axis});
        }
    }

    KDTEventsCPUBuilder::BestSplit KDTEventsCPUBuilder::FindBestPlane(BuildItem const& item, KDTreeParams const& parameters) {
        BestSplit bestSplit{};

        for (int axis = 0; axis < 3; ++axis) {
            int32 leftPrimitiveN{ 0 }, rightPrimitiveN{ StdSize<int32>(item.primitives) };

            Axis3D otherAxis1{ (axis + 1) % 3 }, otherAxis2{ (axis + 2) % 3 };

            cfloat parentSurface{ item.boundingBox.SurfaceArea() };
            PartialSurfaceArea partialSurface{ item.boundingBox.SideLength(otherAxis1), item.boundingBox.SideLength(otherAxis2) };
            
            for (auto eventIt = item.events[axis].begin(), eventEnd = item.events[axis].end(); eventIt != eventEnd;) {
                float currentCoordinate = eventIt->Coordinate(item.primitiveBBs, axis);

                // Count number of starting and ending events at current coordinate
                int32 ending{ 0 }, starting{ 0 };
                while (eventIt != eventEnd && eventIt->Coordinate(item.primitiveBBs, axis) == currentCoordinate) {
                    switch (eventIt->type)
                    {
                    case SplitEventType::Start:
                        ++starting;
                        break;
                    case SplitEventType::End:
                        ++ending;
                        break;
                    }

                    ++eventIt;
                }

                // Remove ending primitives from right
                rightPrimitiveN -= ending;

                // Update cost (don't count box edges)
                assert(rightPrimitiveN >= 0 && leftPrimitiveN >= 0 && leftPrimitiveN + rightPrimitiveN >= StdSize<int32>(item.primitives));
                if (currentCoordinate != item.boundingBox.minBounds[axis] && currentCoordinate != item.boundingBox.maxBounds[axis]) {
                    cfloat cost = ComputeSAH(
                        partialSurface(currentCoordinate - item.boundingBox.minBounds[axis]),
                        partialSurface(item.boundingBox.maxBounds[axis] - currentCoordinate),
                        parentSurface,
                        leftPrimitiveN,
                        rightPrimitiveN,
                        parameters.traversalCost,
                        parameters.intersectionCost
                    );

                    assert(cost >= 0.0_cf);

                    if (cost < bestSplit.minCost) {
                        bestSplit.plane.axis = static_cast<Axis3D>(axis);
                        bestSplit.plane.coordinate = currentCoordinate;
                        bestSplit.minCost = cost;
                    }
                }

                // Add starting primitives to left
                leftPrimitiveN += starting;
            }
        }

        if (bestSplit.minCost > parameters.intersectionCost * StdSize<cfloat>(item.primitives)) {
            bestSplit.plane.axis = Axis3D::None;
        }

        return bestSplit;
    }

    void KDTEventsCPUBuilder::ClassifyAndDistribute(BuildItem const& item, StepItem& stepItem, BuildItem& left, BuildItem& right, BestSplit const& bestSplit, IMeshManager& meshManager) {
        for (int32 primitiveIdx = 0, primitiveN = StdSize<int32>(item.primitives); primitiveIdx < primitiveN; ++primitiveIdx) {
            bool belongsLeft = (item.primitiveBBs[primitiveIdx].minBounds[ToUnderlying(bestSplit.plane.axis)] < bestSplit.plane.coordinate) ? true : false;
            stepItem.classificationLeft[primitiveIdx] = (belongsLeft ? StdSize<int32>(left.primitives) : -1);
            bool belongsRight = (bestSplit.plane.coordinate < item.primitiveBBs[primitiveIdx].maxBounds[ToUnderlying(bestSplit.plane.axis)]) ? true : false;
            stepItem.classificationRight[primitiveIdx] = (belongsRight ? StdSize<int32>(right.primitives) : -1);

            if (belongsLeft && belongsRight) {
                left.primitives.push_back(item.primitives[primitiveIdx]);
                right.primitives.push_back(item.primitives[primitiveIdx]);

                constexpr bool doSplitClip{PRT_SPLIT_CLIP};
                if constexpr (doSplitClip) {
                    auto [leftBB, rightBB] = SplitClipBB(meshManager.GetPrimitive(item.primitives[primitiveIdx]), item.boundingBox, ToUnderlying(bestSplit.plane.axis), bestSplit.plane.coordinate);
                    if (!leftBB.IsValid() || !rightBB.IsValid()) {
                        if (!leftBB.IsValid()) {
                            leftBB = item.primitiveBBs[primitiveIdx];
                            leftBB.maxBounds[ToUnderlying(bestSplit.plane.axis)] = bestSplit.plane.coordinate;
                        }
                        if (!rightBB.IsValid()) {
                            rightBB = item.primitiveBBs[primitiveIdx];
                            rightBB.minBounds[ToUnderlying(bestSplit.plane.axis)] = bestSplit.plane.coordinate;
                        }
                    }

                    left.primitiveBBs.push_back(leftBB);
                    right.primitiveBBs.push_back(rightBB);
                }
                else {
                    AABB leftBB{ item.primitiveBBs[primitiveIdx] }, rightBB{ leftBB };
                    leftBB.maxBounds[ToUnderlying(bestSplit.plane.axis)] = bestSplit.plane.coordinate;
                    rightBB.minBounds[ToUnderlying(bestSplit.plane.axis)] = bestSplit.plane.coordinate;

                    left.primitiveBBs.push_back(leftBB);
                    right.primitiveBBs.push_back(rightBB);
                }
            }
            else if (belongsLeft) {
                left.primitives.push_back(item.primitives[primitiveIdx]);
                left.primitiveBBs.push_back(item.primitiveBBs[primitiveIdx]);
            }
            else {
                right.primitives.push_back(item.primitives[primitiveIdx]);
                right.primitiveBBs.push_back(item.primitiveBBs[primitiveIdx]);
            }
        }
    }

    void KDTEventsCPUBuilder::DistributeAndSortEvents(BuildItem const& item, StepItem const& stepItem, BuildItem& left, BuildItem& right, BestSplit const& bestSplit) {
        left.ReserveEvents();
        right.ReserveEvents();
        constexpr bool doSplitClip{PRT_SPLIT_CLIP};

        for (int axis = 0; axis < 3; ++axis) {
            // With split clipping
            if constexpr (doSplitClip) {
                std::vector<Event> leftUnsorted;
                std::vector<Event> rightUnsorted;

                for (int32 eventIdx = 0, eventN = StdSize<int32>(item.events[axis]); eventIdx < eventN; ++eventIdx) {
                    Event const& event = item.events[axis][eventIdx];
                    // Clipped primitive
                    if (stepItem.classificationLeft[event.primitiveIdx] >= 0 && stepItem.classificationRight[event.primitiveIdx] >= 0) {
                        // if (bestSplit.plane.axis == static_cast<Axis3D>(axis)) {
                        //     left.events[axis].push_back(Event{event.type, stepItem.classificationLeft[event.primitiveIdx]});
                        //     right.events[axis].push_back(Event{event.type, stepItem.classificationRight[event.primitiveIdx]});
                        // }
                        // else {
                        //     leftUnsorted.push_back(Event{event.type, stepItem.classificationLeft[event.primitiveIdx]});
                        //     rightUnsorted.push_back(Event{event.type, stepItem.classificationRight[event.primitiveIdx]});
                        // }
                        // Must sort all axes because of float computation imprecisions
                        leftUnsorted.push_back(Event{event.type, stepItem.classificationLeft[event.primitiveIdx]});
                        rightUnsorted.push_back(Event{event.type, stepItem.classificationRight[event.primitiveIdx]});
                    }
                    // Non-clipped primitives
                    else if (stepItem.classificationLeft[event.primitiveIdx] >= 0) {
                        left.events[axis].push_back(Event{event.type, stepItem.classificationLeft[event.primitiveIdx]});
                    }
                    else {
                        right.events[axis].push_back(Event{event.type, stepItem.classificationRight[event.primitiveIdx]});
                    }
                }

                // Sort events of clipped primitives
                std::sort(leftUnsorted.begin(), leftUnsorted.end(), EventLess{left.primitiveBBs, axis});
                // Insert at the end of left events
                std::copy(leftUnsorted.begin(), leftUnsorted.end(), std::back_inserter(left.events[axis]));
                // Merge two sorted ranges
                std::inplace_merge(left.events[axis].begin(), left.events[axis].end() - leftUnsorted.size(), left.events[axis].end(), EventLess{left.primitiveBBs, axis});

                // Same with right
                std::sort(rightUnsorted.begin(), rightUnsorted.end(), EventLess{right.primitiveBBs, axis});
                std::copy(rightUnsorted.begin(), rightUnsorted.end(), std::back_inserter(right.events[axis]));
                std::inplace_merge(right.events[axis].begin(), right.events[axis].end() - rightUnsorted.size(), right.events[axis].end(), EventLess{right.primitiveBBs, axis});
            }
            else {
                for (int32 eventIdx = 0, eventN = StdSize<int32>(item.events[axis]); eventIdx < eventN; ++eventIdx) {
                    Event const& event = item.events[axis][eventIdx];

                    // No split clipping
                    if (stepItem.classificationLeft[event.primitiveIdx] >= 0) {
                        left.events[axis].push_back(Event{event.type, stepItem.classificationLeft[event.primitiveIdx]});
                    }
                    if (stepItem.classificationRight[event.primitiveIdx] >= 0) {
                        right.events[axis].push_back(Event{event.type, stepItem.classificationRight[event.primitiveIdx]});
                    }
                }
            }
            assert(std::is_sorted(left.events[axis].begin(), left.events[axis].end(), EventLess{left.primitiveBBs, axis}));
            assert(std::is_sorted(right.events[axis].begin(), right.events[axis].end(), EventLess{right.primitiveBBs, axis}));
        }

        assert(item.events[0].size() == item.events[1].size() && item.events[1].size() == item.events[2].size());
    }

    int64 KDTEventsCPUBuilder::AllocatedMemory() const {
        std::size_t memory{0};
        for (KDTNode const& node : nodes) {
            if (node.IsLeaf()) {
                memory += node.leaf.primitiveN * sizeof(PUID);
            }
        }
        return static_cast<int64>(std::span{nodes}.size_bytes() + memory);
    }

    void KDTEventsCPUBuilder::Reset()
    {
        isBuilt = false;
        
        for (auto const& node : nodes) {
            if (node.IsLeaf() && node.leaf.primitives != nullptr) {
                delete[] node.leaf.primitives;
            }
        }
        nodes.clear();
    }
}