/**
 * File name: KDTreeGPU.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "KDTreeGPUBase.hpp"
#include "KDTreeNavigator.hpp"

namespace ProperRT
{
    /// @brief GPU implementation of a k-d tree.
    class KDTreeGPU : public KDTreeGPUBase, public NavigatableBinaryTree
    {
    public:
        using InnerNode = KDTInnerNode;
        using LeafNode = KDTLeafNode;
        using Node = KDTNode;

        class NodeNavigator;

        /// @brief Tree navigator implementation.
        class TreeNavigator : public KDTreeNavigator
        {
        public:
            friend KDTreeGPU;
            using Base = KDTreeNavigator;
        public:
            explicit
            TreeNavigator(KDTreeGPU* tree_, bool selectedStatic_);

            virtual
            void StartNavigating() override;

            virtual
            bool CanNavigateInto() override;

        protected:
            bool selectedStatic;

            virtual
            IMeshManager const& GetMeshManager() override;

            virtual
            std::unique_ptr<BinaryTreeNodeNavigator> CreateNodeNavigator() override;
        };

        /// @brief Node navigator implementation.
        class NodeNavigator : public KDTreeNodeNavigator
        {
        public:
            using Base = KDTreeNodeNavigator;
        public:
            explicit
            NodeNavigator(TreeNavigator* parentNavigator_);
        };

        /// @brief Navigator for choosing between navigating the static and dynamic trees.
        class ChooseTreeNavigator : public INavigator, public IStructureValidator
        {
        public:
            explicit
            ChooseTreeNavigator(KDTreeGPU* tree_);

            virtual
            void StartNavigating() override;

            virtual
            void Navigate() override;

            virtual
            void NavigateOut() override;

            virtual
            bool CanNavigateInto() override;

            virtual
            INavigator* NavigateInto() override;

            virtual
            void Render() override;

            virtual
            std::optional<std::string> Validate() override;

            bool ChoseStatic() const { return radioValue == 0; }

        protected:
            KDTreeGPU* tree;
            std::shared_ptr<OrientedBoundingBoxMesh> boundingBoxMesh;
            int radioValue{0};
        };

    public:
        /// @brief Tree parameters.
        KDTreeParams parameters;

        /// @brief Default ctor.
        KDTreeGPU();

        /// @brief Dtor.
        ~KDTreeGPU();

        virtual
        void BuildStatic(std::vector<CompPtr<Components::Renderer>>&& renderers) override;

        virtual
        void BuildDynamic(std::vector<CompPtr<Components::Renderer>>&& renderers) override;

        virtual
        bool IsBuilt() const override;

        virtual
        INavigator* TopNavigator() override;

        virtual
        IStructureValidator* Validator() override;

        virtual
        std::optional<RayHit> Traverse(Ray const& ray, cfloat maxDist = std::numeric_limits<cfloat>::infinity(), TraversalStatistics* traversalStats = nullptr) override;

        virtual
        bool TraverseShadow(Ray const& ray, cfloat maxDist, TraversalStatistics* traversalStats = nullptr) override;

        virtual
        AccStructStats const& GetStatistics() const override { return stats; }

        virtual
        AccStructStats& GetStatistics() override { return stats; }

        virtual
        std::unique_ptr<BinaryTreeNavigationNode> NavigationRoot();

        /// @brief Returns the static mesh manager.
        GPUMeshManager const* GetStaticMeshManager() const;

        /// @brief Returns the static mesh builder.
        KDTGPUBuilderBase const* GetStaticBuilder() const;

        /// @brief Returns the dynamic mesh manager.
        GPUMeshManager const* GetDynamicMeshManager() const;

        /// @brief Returns the dynamic mesh builder.
        KDTGPUBuilderBase const* GetDynamicBuilder() const;

        virtual
        KDTGPUTraverser GetStaticTraverser() const override;

        virtual
        KDTGPUTraverser GetDynamicTraverser() const override;

        virtual
        UniqueMemoryPool& DataMemoryPool() override;

    private:
        struct ComputeData;
        
        std::vector<CompPtr<Components::Renderer>> renderers{};
        int32 staticRendererCount{};

        std::unique_ptr<ComputeData> computeData;

        std::unique_ptr<GPUMeshManager> staticMeshManager{};
        std::unique_ptr<KDTGPUBuilderBase> staticBuilder{};

        std::unique_ptr<GPUMeshManager> dynamicMeshManager{};
        std::unique_ptr<KDTGPUBuilderBase> dynamicBuilder{};

        ChooseTreeNavigator topNavigator;
        TreeNavigator staticNavigator;
        TreeNavigator dynamicNavigator;

        AccelerationStructureStatistics stats{};

        KDTGPUTraverser Traverser(GPUMeshManager const& meshManager, KDTGPUBuilderBase const& builder) const;
    };
}