/**
 * File name: KDTEventsCPUBuilder.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "KDTCPUBuilderBase.hpp"
#include "KDTreeCPU.hpp"

namespace ProperRT
{
    /// @brief Class for a k-d tree builder using exact split selection with events.
    class KDTEventsCPUBuilder : public KDTCPUBuilderBase
    {
    public:
        using Node = KDTCPUBuilderBase::Node;

    public:
        virtual
        ~KDTEventsCPUBuilder();

        virtual
        void Build(IMeshManager& meshManager, std::vector<PUID> primitives, std::vector<AABB> primitiveBBs, KDTreeParams const& parameters) override;

        virtual
        bool IsBuilt() const override { return isBuilt; }

        virtual
        std::span<Node> Nodes() override { return nodes; }

        virtual
        std::vector<Node> const& Nodes() const override { return nodes; }

        virtual
        int32 RootIdx() const override { return 0; }

        virtual
        int32 MaxDepth(int32 primitiveN, KDTreeParams const& parameters) const override {
            return static_cast<int32>(parameters.maxDepth_k1 + parameters.maxDepth_k2 * std::log2(static_cast<cfloat>(primitiveN)));
        }

        virtual
        int64 AllocatedMemory() const override;

        virtual
        void Reset() override;

    private:
        struct Event
        {
            SplitEventType type;
            int32 primitiveIdx;

            bool operator==(Event const&) const noexcept = default;

            float Coordinate(std::span<AABB const> primitiveBBs, int axis) const {
                return primitiveBBs[primitiveIdx].GetBounds(static_cast<AABB::BoundsEnum>(type))[axis];
            }
        };

        struct EventLess
        {
            std::span<AABB const> primitiveBBs;
            int axis;

            bool operator()(Event const& left, Event const& right) {
                return left.Coordinate(primitiveBBs, axis) < right.Coordinate(primitiveBBs, axis);
            }
        };

        struct BuildItem
        {
            int32 node{ -1 };
            AABB boundingBox{};
            std::vector<PUID> primitives;
            std::vector<AABB> primitiveBBs;
            std::array<std::vector<Event>, 3> events;
            int32 depth{ -1 };

            void ReserveEvents() {
                for (int axis = 0; axis < 3; ++axis) {
                    events[axis].reserve(2 * primitives.size());
                }
            }
        };

        struct StepItem
        {
            std::vector<int32> classificationLeft;
            std::vector<int32> classificationRight;

            StepItem(std::size_t primitiveN)
                : classificationLeft(primitiveN), classificationRight(primitiveN)
            {}
        };

        struct BestSplit
        {
            SplitPlane plane{ Axis3D::None };
            cfloat minCost{ std::numeric_limits<cfloat>::infinity() };
        };

    private:
        std::vector<Node> nodes;

        bool isBuilt{ false };

        void CreateAndSortEvents(BuildItem& item);

        BestSplit FindBestPlane(BuildItem const& item, KDTreeParams const& parameters);

        void ClassifyAndDistribute(BuildItem const& item, StepItem& stepItem, BuildItem& left, BuildItem& right, BestSplit const& bestSplit, IMeshManager& meshManager);

        void DistributeAndSortEvents(BuildItem const& item, StepItem const& stepItem, BuildItem& left, BuildItem& right, BestSplit const& bestSplit);
    };
}
