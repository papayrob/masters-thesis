/**
 * File name: KDTGPUBuilderBase.cuh
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "Graphics/Raytracing/GPUMeshManager.cuh"
#include "KDTreeGPU.hpp"

namespace ProperRT
{
    /// @brief Base class for GPU k-d tree builder classes.
    class KDTGPUBuilderBase
    {
    public:
        /// @brief Dtor.
        virtual
        ~KDTGPUBuilderBase() = default;

        /// @brief Builds the tree.
        /// @param meshManager Mesh manager to build the tree over
        /// @param primitives Scene primitives
        /// @param primitiveBBs Primitive bounding boxes
        /// @param parameters Parameters
        virtual
        void Build(GPUMeshManager& meshManager, DeviceUniquePointer<PUID[]> primitives, DeviceUniquePointer<AABB[]> primitiveBBs, KDTreeParams const& parameters) = 0;

        /// @brief Returns true if the tree is built.
        virtual
        bool IsBuilt() const = 0;

        /// @brief Returns the nodes on the GPU.
        virtual
        DevicePointer<KDTNode[]> NodesGPU() const = 0;
        // TODO add const into DevicePointer template

        /// @brief Returns the nodes on the CPU.
        virtual
        std::span<KDTNode> Nodes() = 0;

        /// @brief Returns the nodes on the CPU.
        virtual
        std::vector<KDTNode> const& Nodes() const = 0;

        /// @brief Returns the index of the root node.
        virtual
        int32 RootIdx() const = 0;

        /// @brief Returns the root node.
        virtual
        KDTNode* Root() {
            return (IsBuilt() ? &Nodes()[RootIdx()] : nullptr);
        }

        /// @brief Returns the root node.
        virtual
        KDTNode const* Root() const {
            return (IsBuilt() ? &Nodes()[RootIdx()] : nullptr);
        }

        /// @brief Returns the primitives of @p leaf.
        /// @param leaf Leaf node
        virtual
        std::vector<PUID> LeafPrimitives(KDTLeafNode const& leaf) const = 0;

        /// @brief Returns the left child of @p innerNode.
        /// @param innerNode Inner node
        virtual
        KDTNode* GetLeftChild(KDTInnerNode& innerNode) {
            if (innerNode.leftChildIdx < 0) {
                return nullptr;
            }
            return &(Nodes()[innerNode.leftChildIdx]);
        }

        /// @brief Returns the left child of @p innerNode.
        /// @param innerNode Inner node
        virtual
        KDTNode const* GetLeftChild(KDTInnerNode const& innerNode) const {
            if (innerNode.leftChildIdx < 0) {
                return nullptr;
            }
            return &(Nodes()[innerNode.leftChildIdx]);
        }

        /// @brief Returns the right child of @p innerNode.
        /// @param innerNode Inner node
        virtual
        KDTNode* GetRightChild(KDTInnerNode& innerNode) {
            if (innerNode.rightChildIdx < 0) {
                return nullptr;
            }
            return &(Nodes()[innerNode.rightChildIdx]);
        }

        /// @brief Returns the right child of @p innerNode.
        /// @param innerNode Inner node
        virtual
        KDTNode const* GetRightChild(KDTInnerNode const& innerNode) const {
            if (innerNode.rightChildIdx < 0) {
                return nullptr;
            }
            return &(Nodes()[innerNode.rightChildIdx]);
        }

        /// @brief Returns the child of @p innerNode on @p side.
        /// @param side Side of the child (left/right)
        /// @param innerNode Inner node
        virtual
        KDTNode* GetChild(Side side, KDTInnerNode& innerNode) {
            if (side == Side::Left) {
                return GetLeftChild(innerNode);
            }
            else {
                return GetRightChild(innerNode);
            }
        }

        /// @brief Returns the child of @p innerNode on @p side.
        /// @param side Side of the child (left/right)
        /// @param innerNode Inner node
        virtual
        KDTNode const* GetChild(Side side, KDTInnerNode const& innerNode) const {
            if (side == Side::Left) {
                return GetLeftChild(innerNode);
            }
            else {
                return GetRightChild(innerNode);
            }
        }

        /// @brief Returns the max depth based on @p primitiveN and @p parameters.
        /// @param primitiveN Number of primitives
        /// @param parameters Parameters
        virtual
        int32 MaxDepth(int32 primitiveN, KDTreeParams const& parameters) const = 0;

        /// @brief Calculates the total amount of allocated memory and returns it.
        virtual
        int64 AllocatedMemory() const = 0;

        /// @brief Resets the builder.
        virtual
        void Reset() = 0;
    };
}