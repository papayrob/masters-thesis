/**
 * File name: KDTreeNavigator.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "Graphics/Raytracing/BinaryTreeNavigator.hpp"
#include "KDTreeCommon.hpp"
#include "Graphics/Raytracing/PUID.hpp"
#include "Graphics/Raytracing/BinaryTreeMesh.hpp"

namespace ProperRT
{
    /// @brief Specilazied navigation node class for k-d trees.
    class KDTreeNavigationNode : public BinaryTreeNavigationNode
    {
    public:
        /// @brief Returns the split axis of the node, Axis3D::None if leaf.
        virtual
        Axis3D SplitAxis() const = 0;
        
        /// @brief Returns the split coordinate of the node, NaN if leaf.
        virtual
        float SplitCoordinate() const = 0;

        /// @brief Returns the leaf primitives.
        virtual
        std::span<PUID const> LeafPrimitives() const = 0;

        /// @brief Copies the nav node without a parent path (for saving memory).
        virtual
        std::unique_ptr<KDTreeNavigationNode> NoParentCopy() const = 0;
    };

    class KDTreeNodeNavigator;

    /// @brief Specilazied tree navigator class for k-d trees.
    class KDTreeNavigator : public BinaryTreeNavigator
    {
    public:
        friend KDTreeNodeNavigator;
        using Base = BinaryTreeNavigator;

        inline static
        Color splitPlaneColor = Color{ 1.0, 1.0, 0.0, 1.0 };

    public:
        /// @brief Standard ctor.
        /// @param tree_ Pointer to a navigatable tree instance
        explicit
        KDTreeNavigator(NavigatableBinaryTree* tree_);

        virtual
        void StartNavigating() override;

        virtual
        void NavigateOut() override;

        virtual
        void Render() override;

    protected:
        std::deque<SplitPlane> splitChain;
        std::unique_ptr<BinaryTreeMesh> primitiveMeshTree;
        BinaryTreeMesh::Node* primitiveMeshTreeNode;

        std::shared_ptr<OrientedBoundingBoxMesh> splitPlane;

        virtual
        void NavigateUp() override;

        virtual
        void NavigateLeft() override;

        virtual
        void NavigateRight() override;

        virtual
        void NavigateSwitch() override;
        
        /// @brief Creates a mesh according to the tree structure.
        virtual
        std::unique_ptr<BinaryTreeMesh> CreateBinaryTreeMesh();

        /// @brief Returns the mesh manager instance the tree was built over.
        virtual
        IMeshManager const& GetMeshManager() = 0;

        virtual
        void UpdateMeshes() override;

        virtual
        std::string CreateNavigationChainString() const override;

        virtual
        void SetButtons() override;

        virtual
        std::unique_ptr<BinaryTreeNodeNavigator> CreateNodeNavigator() override;
    };

    /// @brief Specilazied tree node navigator class for k-d trees.
    class KDTreeNodeNavigator : public BinaryTreeNodeNavigator
    {
    public:
        using Base = BinaryTreeNodeNavigator;

    public:
        /// @brief Standard ctor.
        /// @param tree_ Pointer to a parent tree navigator instance
        explicit
        KDTreeNodeNavigator(KDTreeNavigator* parentNavigator_);

        virtual
        void StartNavigating() override;

        virtual
        void Render() override;

    protected:
        SplitPlane split;

        std::shared_ptr<OrientedBoundingBoxMesh> primitiveSplitBoxes;
        std::shared_ptr<OrientedBoundingBoxMesh> splitPlane;

        bool opaqueSplit{ false };
        bool opaqueBoxes{ false };

        virtual
        void UpdateMeshes() override;
    };
}