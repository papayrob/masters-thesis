/**
 * File name: KDTreeCPU.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "KDTreeCPU.hpp"

#include "KDTEventsCPUBuilder.hpp"
#include "KDTBinsCPUBuilder.hpp"
#include "KDTCPUTraverser.hpp"
#include "KDTreeNavigationNodeCommon.hpp"
#include "RTApp.hpp"

namespace ProperRT
{
    namespace
    {
        TraversalStatistics traversalStatsDummy;
    }

    KDTreeCPU::KDTreeCPU()
        : topNavigator{ this }, staticNavigator{ this, true }, dynamicNavigator{ this, false }
    {
        auto const& sceneConfig = Scene::GetActiveScene()->Config();
        if (std::string value; sceneConfig.TryGetValue("staticBuilder", value)) {
            if (value == "Events") {
                staticBuilder = std::make_unique<KDTEventsCPUBuilder>();
            }
            else if (value == "Bins") {
                staticBuilder = std::make_unique<KDTBinsCPUBuilder>();
            }
            else {
                staticBuilder = nullptr;
            }

            if (staticBuilder != nullptr) {
                staticMeshManager = std::make_unique<CPUMeshManager>();
            }
        }

        if (std::string value; sceneConfig.TryGetValue("dynamicBuilder", value)) {
            if (value == "Events") {
                dynamicBuilder = std::make_unique<KDTEventsCPUBuilder>();
            }
            else if (value == "Bins") {
                dynamicBuilder = std::make_unique<KDTBinsCPUBuilder>();
            }
            else {
                dynamicBuilder = nullptr;
            }

            dynamicMeshManager = std::make_unique<CPUMeshManager>();
        }

        if (dynamicBuilder == nullptr) {
            //TODO own exception (ConfigException)
            throw std::runtime_error("Cannot run without a dynamic tree builder");
        }
    }

    KDTreeCPU::~KDTreeCPU() = default;

    void KDTreeCPU::BuildStatic(std::vector<CompPtr<Components::Renderer>>&& renderers_) {
        if (staticMeshManager != nullptr && staticBuilder != nullptr) {
            renderers.clear();
            staticRendererCount = -1;
            
            auto&& [primitives, primitiveBBs] = staticMeshManager->CreateInstances(renderers_);
            RTApp::Instance()->statistics.AddInfo("StaticPrimitiveCount", StdSize<int64>(primitives));
            staticBuilder->Build(*staticMeshManager, std::move(primitives), std::move(primitiveBBs), parameters);
        }
        else {
            renderers = std::move(renderers_);
            staticRendererCount = StdSize<int32>(renderers);
            RTApp::Instance()->statistics.AddInfo("StaticPrimitiveCount", 0);
        }
    }

    void KDTreeCPU::BuildDynamic(std::vector<CompPtr<Components::Renderer>>&& renderers_) {
        std::span<CompPtr<Components::Renderer>> rendererSpan;
        if (staticRendererCount < 0) {
            rendererSpan = std::span<CompPtr<Components::Renderer>>{renderers_};
        }
        else {
            renderers.resize(staticRendererCount + renderers_.size());
            std::move(renderers_.begin(), renderers_.end(), renderers.begin() + staticRendererCount);
            rendererSpan = std::span<CompPtr<Components::Renderer>>{renderers};
        }

        auto&& [primitives, primitiveBBs] = dynamicMeshManager->CreateInstances(rendererSpan);
        RTApp::Instance()->statistics.AddInfo("DynamicPrimitiveCount", StdSize<int64>(primitives));
        dynamicBuilder->Build(*dynamicMeshManager, std::move(primitives), std::move(primitiveBBs), parameters);
    }

    bool KDTreeCPU::IsBuilt() const {
        return ((staticBuilder == nullptr ? true : staticBuilder->IsBuilt()) && dynamicBuilder->IsBuilt());
    }

    INavigator* KDTreeCPU::TopNavigator() {
        if ((staticBuilder != nullptr && staticBuilder->IsBuilt()) || dynamicBuilder->IsBuilt()) {
            return &topNavigator;
        }

        return nullptr;
    }

    IStructureValidator *KDTreeCPU::Validator() {
        if ((staticBuilder != nullptr && staticBuilder->IsBuilt()) || dynamicBuilder->IsBuilt()) {
            return &topNavigator;
        }

        return nullptr;
    }

    std::optional<RayHit> KDTreeCPU::Traverse(Ray const& ray, cfloat maxDist, TraversalStatistics* traversalStats) {
        if (traversalStats == nullptr) {
            traversalStats = &traversalStatsDummy;
        }

        ++(traversalStats->queryCount);
        std::optional<RayHit> hitStatic{};
        auto hitDynamic = Traverser(*dynamicMeshManager, *dynamicBuilder).Traverse(ray, maxDist, *traversalStats);

        if (staticBuilder != nullptr) {
            hitStatic = Traverser(*staticMeshManager, *staticBuilder).Traverse(ray, maxDist, *traversalStats);
        }
        
        // Combine static and dynamic traversals
        if (hitStatic.has_value() && hitDynamic.has_value()) {
            return (hitStatic->tHit < hitDynamic->tHit ? hitStatic : hitDynamic);
        }
        else if (hitStatic.has_value()) {
            return hitStatic;
        }
        else {
            return hitDynamic;
        }
    }
    
    bool KDTreeCPU::TraverseShadow(Ray const& ray, cfloat maxDist, TraversalStatistics* traversalStats) {
        if (traversalStats == nullptr) {
            traversalStats = &traversalStatsDummy;
        }

        ++(traversalStats->queryCount);
        if (staticBuilder == nullptr) {
            return Traverser(*dynamicMeshManager, *dynamicBuilder).TraverseShadow(ray, maxDist, *traversalStats);
        }
        else {
            // Combine static and dynamic traversals
            auto hitStatic = Traverser(*staticMeshManager, *staticBuilder).TraverseShadow(ray, maxDist, *traversalStats);
            auto hitDynamic = Traverser(*dynamicMeshManager, *dynamicBuilder).TraverseShadow(ray, maxDist, *traversalStats);
            return (hitStatic || hitDynamic);
        }
    }

    std::unique_ptr<BinaryTreeNavigationNode> KDTreeCPU::NavigationRoot() {
        using NavigationNode = KDTreeNavigationNodeCommon<KDTCPUBuilderBase>;
        if (topNavigator.ChoseStatic()) {
            return std::unique_ptr<BinaryTreeNavigationNode>(new NavigationNode(
                *staticBuilder,
                *staticMeshManager,
                staticBuilder->Root(),
                staticMeshManager->SceneBoundingBox(),
                staticNavigator.primitiveMeshTree ? staticNavigator.primitiveMeshTree->Root() : nullptr
            ));
        }
        else {
            return std::unique_ptr<BinaryTreeNavigationNode>(new NavigationNode(
                *dynamicBuilder,
                *dynamicMeshManager,
                dynamicBuilder->Root(),
                dynamicMeshManager->SceneBoundingBox(),
                dynamicNavigator.primitiveMeshTree ? dynamicNavigator.primitiveMeshTree->Root() : nullptr
            ));
        }
    }

    KDTCPUTraverser KDTreeCPU::Traverser(CPUMeshManager const& meshManager, KDTCPUBuilderBase const& builder) const {
        return KDTCPUTraverser{ meshManager, builder.Nodes(), builder.RootIdx(), builder.MaxDepth(meshManager.PrimitiveCount(), parameters) };
    }
    
    CPUMeshManager const* KDTreeCPU::GetStaticMeshManager() const {
        return staticMeshManager.get();
    }

    KDTCPUBuilderBase const* KDTreeCPU::GetStaticBuilder() const {
        return staticBuilder.get();
    }

    CPUMeshManager const* KDTreeCPU::GetDynamicMeshManager() const {
        return dynamicMeshManager.get();
    }

    KDTCPUBuilderBase const* KDTreeCPU::GetDynamicBuilder() const {
        return dynamicBuilder.get();
    }
}
