/**
 * File name: KDTBinsGPUBuilder.cuh
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <optional>
#include <cuda/std/functional>
#include <cub/warp/warp_merge_sort.cuh>

#include "KDTGPUBuilderBase.cuh"
#include "Compute/DeviceLinkedList.cuh"
#include "Compute/ParallelScan.cuh"

namespace ProperRT
{
    /// @brief Class for a k-d tree builder using min-max binning on the GPU.
    class KDTBinsGPUBuilder : public KDTGPUBuilderBase
    {
    public:
        using PrimitiveCountType = int32;
        using PrimitiveCountAtomic = cuda::atomic<PrimitiveCountType, cuda::thread_scope_device>;
        using ParallelScan = ParallelScan<PrimitiveCountType, 0, cuda::std::plus<PrimitiveCountType>>;

    public:
        /// @brief The number of bins used for binning.
        constexpr static inline
        int32 binN = PRT_KDTREE_BINS;

        /// @brief Split strategy used for the MemoryPool.
        constexpr static inline
        MemoryPool::SplitStrategy splitStrategy = MemoryPool::SplitStrategy::Proportional;

        static_assert(binN % Compute::SUBGROUP_SIZE == 0);

        /// @brief For unused exact split selection algorithm
        constexpr static inline
        int32 LOCAL_COMPUTATION_THRESHOLD = binN;
        static_assert(LOCAL_COMPUTATION_THRESHOLD % Compute::SUBGROUP_SIZE == 0);
        /// @brief For unused exact split selection algorithm
        constexpr static inline
        int16 LOCAL_COMPUTATION_EVENT_COUNT = 2 * LOCAL_COMPUTATION_THRESHOLD;

        /// @brief For unused exact split selection algorithm
        constexpr static inline
        int16 LOCAL_PRIMITIVES_PER_THREAD = LOCAL_COMPUTATION_THRESHOLD / Compute::SUBGROUP_SIZE;
        /// @brief For unused exact split selection algorithm
        constexpr static inline
        int16 EVENTS_PER_THREAD = LOCAL_PRIMITIVES_PER_THREAD * 2;

        /// @brief Special pointer class for build nodes.
        struct NodeIdxPointer
        {
            int32 listIdx;
            int32 offset;
        };

        /// @brief Build version of KDTInnerNode.
        struct InnerNode
        {
            KDTNodeType tag;
            float splitValue;
            NodeIdxPointer leftChild;
            NodeIdxPointer rightChild;
        };

        /// @brief Build version of KDTLeafNode.
        struct LeafNode
        {
            KDTNodeType tag;
            int32 primitiveN;
            PUID* primitives;
        };

        /// @brief Build version of KDTNode.
        struct Node
        {
            union {
                InnerNode inner;
                LeafNode leaf;
            };

            __host__ __device__
            KDTNodeType Tag() const { return inner.tag; }

            __host__ __device__
            bool IsLeaf() const { return Tag() == KDTNodeType::Leaf; }

            __host__ __device__
            bool IsInner() const { return !IsLeaf(); }
        };

        #if PRT_BUILD_STATUS
        struct BuildStatusAtomic
        {
            cuda::atomic<int32, cuda::thread_scope_system> tasks{0};
            cuda::atomic<int32, cuda::thread_scope_system> workingSubgroups{0};
            cuda::atomic<int32, cuda::thread_scope_system> depth{0};
            cuda::atomic<int32, cuda::thread_scope_system> nodes{0};
        };
        struct BuildStatus
        {
            int32 tasks{0};
            int32 workingSubgroups{0};
            int32 depth{0};
            int32 nodes{0};
        };
        #endif

    public:
        /// @brief Multiplier used for allocations using MemoryPool.
        int32 memPoolMultiplier{4};

        #if PRT_BUILD_STATUS
        BuildStatus* buildStatusHost;
        DeviceUniquePointer<BuildStatusAtomic> buildStatusAtomic;
        cudaEvent_t stop;
        #endif

        /// @brief Standard ctor.
        /// @param dataMemPool_ Memory pool used for data
        KDTBinsGPUBuilder(UniqueMemoryPool& dataMemPool_);

        /// @brief Dtor.
        ~KDTBinsGPUBuilder();

        virtual
        void Build(GPUMeshManager& meshManager, DeviceUniquePointer<PUID[]> primitives, DeviceUniquePointer<AABB[]> primitiveBBs, KDTreeParams const& parameters) override;

        virtual
        bool IsBuilt() const override { return isBuilt; }

        virtual
        DevicePointer<KDTreeGPU::Node[]> NodesGPU() const override { return nodes.Get(); }

        virtual
        std::span<KDTNode> Nodes() override {
            if (nodesCPU.empty()) {
                nodesCPU = nodes.Download();
            }
            return nodesCPU;
        }

        virtual
        std::vector<KDTNode> const& Nodes() const override {
            if (nodesCPU.empty()) {
                nodesCPU = nodes.Download();
            }
            return nodesCPU;
        }

        virtual
        int32 RootIdx() const override { return 0; }

        virtual
        KDTNode* Root() override {
            if (nodesCPU.empty()) {
                nodesCPU = nodes.Download();
            }
            if (nodesCPU.empty()) {
                return nullptr;
            }
            
            return &nodesCPU[0];
        }

        virtual
        KDTNode const* Root() const override {
            if (nodesCPU.empty()) {
                nodesCPU = nodes.Download();
            }
            if (nodesCPU.empty()) {
                return nullptr;
            }
            return &nodesCPU[0];
        }

        virtual
        std::vector<PUID> LeafPrimitives(KDTLeafNode const& leaf) const override {
            return DevicePointer<PUID[]>{ leaf.primitives, leaf.primitiveN }.Download();
        }

        virtual
        int32 MaxDepth(int32 primitiveN, KDTreeParams const& parameters) const override { return static_cast<int32>(parameters.maxDepth_k1 + parameters.maxDepth_k2 * std::log2(static_cast<cfloat>(primitiveN))); }

        virtual
        int64 AllocatedMemory() const override;

        virtual
        void Reset() override;

    public:
        /// @brief Enum for task phases.
        enum class Phases : int8
        {
            Binning = 0, Classification, ClassifyUpSweep, ClassifyDownSweep, Distribution, MakeLeaf
        };

        /// @brief Class representing a mask with 3 booleans.
        struct EnabledAxes {
        public:
            constexpr inline static
            uint8 X_MASK = 0b00000001;

            constexpr inline static
            uint8 Y_MASK = 0b00000010;

            constexpr inline static
            uint8 Z_MASK = 0b00000100;

            constexpr inline static
            uint8 ALL_ENABLED = 0b00000111;

        public:
            uint8 flags;

            __host__ __device__
            bool IsEnabled(Axis3D axis) {
                switch (axis) {
                case Axis3D::X: {
                    return flags & X_MASK;
                }
                case Axis3D::Y: {
                    return flags & Y_MASK;
                }
                case Axis3D::Z: {
                    return flags & Z_MASK;
                }
                }
                return false;
            }

            __host__ __device__
            void DisableAxis(Axis3D axis) {
                switch (axis) {
                case Axis3D::X: {
                    flags &= ~X_MASK;
                }
                case Axis3D::Y: {
                    flags &= ~Y_MASK;
                }
                case Axis3D::Z: {
                    flags &= ~Z_MASK;
                }
                }
            }

            __host__ __device__
            void EnableAll() {
                flags = ALL_ENABLED;
            }
        };

        /// @brief Class with custom task data.
        struct BuildTaskData
        {
            /// @brief Allocated memory for node primitives.
            MemoryPool::AllocatedMemory<PUID> primitivesStorage;
            /// @brief Allocated memory for left node primitives.
            MemoryPool::AllocatedMemory<PUID> primitivesLeftStorage;
            /// @brief Allocated memory for right node primitives.
            MemoryPool::AllocatedMemory<PUID> primitivesRightStorage;

            /// @brief Allocated memory for primitive bounding boxes.
            MemoryPool::AllocatedMemory<AABB> bbsStorage;
            /// @brief Allocated memory for left node's primitive bounding boxes.
            MemoryPool::AllocatedMemory<AABB> bbsLeftStorage;
            /// @brief Allocated memory for right node's primitive bounding boxes.
            MemoryPool::AllocatedMemory<AABB> bbsRightStorage;

            /// @brief Allocated memory for classification.
            MemoryPool::AllocatedMemory<PrimitiveCountType> classificationStorage;

            /// @brief Pointer to the node belonging to the task.
            Node* node;

            /// @brief Current array stride used for scanning.
            PrimitiveCountType arrayStride;
            /// @brief Total step number used for scanning.
            int32 stepN;

            /// @brief Min-bins.
            PrimitiveCountAtomic minBins[3][binN];
            /// @brief Max-bins.
            PrimitiveCountAtomic maxBins[3][binN];

            /// @brief Number of primitives in the associated node.
            PrimitiveCountType primitiveN;
            /// @brief Number of primitives in the left child of the associated node.
            PrimitiveCountType primitiveLeftN;
            /// @brief Number of primitives in the right child of the associated node.
            PrimitiveCountType primitiveRightN;

            /// @brief Bounding box of the associated node.
            AABB boundingBox;
            /// @brief Depth of the associated node.
            int32 depth;

            /// @brief Split coordinate of the associated node.
            sfloat splitCoord;
            /// @brief Split axis of the associated node.
            Axis3D splitAxis;
            
            /// @brief Binning is not done on disabled axes.
            EnabledAxes enabledAxes;

            /// @brief Returns the left classification.
            __device__
            PrimitiveCountType* ClassificationLeft() const { return classificationStorage.data; }
            
            /// @brief Returns the right classification.
            __device__
            PrimitiveCountType* ClassificationRight() const { return ClassificationLeft() + primitiveN; }

            /// @brief Returns the primitive references data.
            __device__ __host__
            PUID* Primitives() const { return primitivesStorage.data; }

            /// @brief Returns the primitive bounding boxes.
            __device__ __host__
            AABB* PrimitiveBBs() const { return bbsStorage.data; }

            /// @brief Returns the left node's primitive references data.
            __device__ __host__
            PUID* PrimitivesLeft() const { return primitivesLeftStorage.data; }
            
            /// @brief Returns the right node's primitive references data.
            __device__ __host__
            PUID* PrimitivesRight() const { return primitivesRightStorage.data; }

            /// @brief Returns the left node's primitive bounding boxes.
            __device__ __host__
            AABB* PrimitiveBBsLeft() const { return bbsLeftStorage.data; }
            
            /// @brief Returns the right node's primitive bounding boxes.
            __device__ __host__
            AABB* PrimitiveBBsRight() const { return bbsRightStorage.data; }

            /// @brief Passes storage down a level.
            __device__ __host__
            void PassDownStorage(MemoryPool::AllocatedMemory<PUID> const& primStorage, MemoryPool::AllocatedMemory<AABB> const& bbStorage) {
                primitivesLeftStorage = primStorage;
                bbsLeftStorage = bbStorage;
            }
        };

        /// @brief Same as BuildTaskData, but used for a shared copy of the task.
        struct SharedBuildTaskData
        {
            MemoryPool::AllocatedMemory<PUID> primitivesStorage;
            MemoryPool::AllocatedMemory<PUID> primitivesLeftStorage;
            MemoryPool::AllocatedMemory<PUID> primitivesRightStorage;

            MemoryPool::AllocatedMemory<AABB> bbsStorage;
            MemoryPool::AllocatedMemory<AABB> bbsLeftStorage;
            MemoryPool::AllocatedMemory<AABB> bbsRightStorage;

            MemoryPool::AllocatedMemory<PrimitiveCountType> classificationStorage;

            Node* node;

            PrimitiveCountType arrayStride;
            int32 stepN;

            PrimitiveCountType minBins[3][binN];
            PrimitiveCountType maxBins[3][binN];

            PrimitiveCountType primitiveN;
            PrimitiveCountType primitiveLeftN;
            PrimitiveCountType primitiveRightN;

            AABB boundingBox;
            int32 depth;

            sfloat splitCoord;
            Axis3D splitAxis;
            
            EnabledAxes enabledAxes;

            __device__
            PrimitiveCountType* ClassificationLeft() const { return classificationStorage.data; }
            
            __device__
            PrimitiveCountType* ClassificationRight() const { return ClassificationLeft() + primitiveN; }

            __device__ __host__
            PUID* Primitives() const { return primitivesStorage.data; }

            __device__ __host__
            AABB* PrimitiveBBs() const { return bbsStorage.data; }

            __device__ __host__
            PUID* PrimitivesLeft() const { return primitivesLeftStorage.data; }
            
            __device__ __host__
            PUID* PrimitivesRight() const { return primitivesRightStorage.data; }

            __device__ __host__
            AABB* PrimitiveBBsLeft() const { return bbsLeftStorage.data; }
            
            __device__ __host__
            AABB* PrimitiveBBsRight() const { return bbsRightStorage.data; }

            __device__ __host__
            void PassDownStorage(MemoryPool::AllocatedMemory<PUID> const& primStorage, MemoryPool::AllocatedMemory<AABB> const& bbStorage) {
                primitivesLeftStorage = primStorage;
                bbsLeftStorage = bbStorage;
            }
        };

        //Unused:
        using SharedGroupTask = typename GPUTaskPoolProcessor<Phases, SharedBuildTaskData, NoData, ParallelScan>::GroupTask;

        struct Event
        {
            SplitEventType type;
            int16 primitiveIdx;
            float coordinate;

            bool operator<(Event const& other) const {
                return coordinate < other.coordinate;
            }
        };

        using SubgroupSort = cub::WarpMergeSort<Event, EVENTS_PER_THREAD, Compute::SUBGROUP_SIZE>;

        struct BestSplit
        {
            SplitPlane plane{ Axis3D::None };
            cfloat minCost{ std::numeric_limits<cfloat>::infinity() };
        };

        constexpr static inline int SORT_STORAGE_SIZE = sizeof(SubgroupSort::TempStorage);
        constexpr static inline int TASK_SIZE = sizeof(SharedGroupTask);

        /// @brief Custom shared data.
        struct SharedCustomData
        {
            SharedGroupTask taskCopy;
        };

    public:
        /// @brief Task processor class.
        class Processor : public GPUTaskPoolProcessor<Phases, BuildTaskData, SharedCustomData, Processor>
        {
        public:
            friend KDTBinsGPUBuilder;
            
            using Base = GPUTaskPoolProcessor<Phases, BuildTaskData, SharedCustomData, Processor>;
            using SharedParameters = Base::SharedParameters;
            using GroupTask = Base::GroupTask;

            constexpr static inline int x = sizeof(GroupTask);

        public:
            cfloat traversalCost;
            cfloat intersectionCost;
            int32 primitiveLimit;
            int32 maxDepth;

            DevicePointer<DeviceLinkedListHead<Node>[]> nodeLists;
            DevicePointer<int32[]> nodeListInfos;

            DevicePointer<GPUMeshManagement::Instance[]> instances;

            DevicePointer<MemoryPool> primitivePool;
            DevicePointer<MemoryPool> dataPool;
            int32 allocationMultiplier;

            #if PRT_BUILD_STATUS
            DevicePointer<BuildStatus> buildStatusHost;
            DevicePointer<BuildStatusAtomic> buildStatusAtomic;
            #endif

            #if PRT_TIME_KERNELS
            DeviceTimer binTimer;
            DeviceTimer classificationTimer;
            DeviceTimer distributionTimer;
            DeviceTimer leafTimer;
            DeviceTimer lockTimer;
            DeviceTimer subTimer;
            #endif

        public:
            __host__
            void RunKernel(KDTBinsGPUBuilder& builder, DevicePointer<PUID[]> primitives, DevicePointer<AABB[]> primitiveBBs, AABB const& sceneBoundingBox, Compute::TaskPoolProcessorParams params);
            
        public:
            __device__
            void ProcessTask();

            __device__
            void TaskPostProcess();

            __device__
            void Start();

            __device__
            void Finish();

        private:
            __device__
            void Bin();

            __device__
            void BinPostProcess();

            __device__
            void Classify();

            __device__
            void ClassificationScanPostProcess();

            __device__
            void DistributeChunks();

            __device__
            bool Distribute(int32 gid, PrimitiveCountType* classification, PUID* primitives, PUID* childPrimitives);

            __device__
            void DistributionPostProcess();

            __device__
            Primitive GetPrimitive(PUID puid) {
                return instances[puid.instanceID].GetPrimitive(puid.primitiveOffset);
            }

            __device__
            void CopySharedTaskToGlobal(GroupTask* globalTask);

            __device__
            void LoadSelectedTaskToShared();

            __device__
            bool LeafCondition(int32 childPrimitiveN, int32 depth) { return (childPrimitiveN <= primitiveLimit || depth > maxDepth); }

            template<class T>
            struct MinOperator
            {
                __device__
                T operator()(T lhs, T rhs) const { return min(lhs, rhs); }
            };

            __device__
            BestSplit FindBestPlaneExact();
        };

    private:
        class LinearComputation
        {
        public:
            __device__ static
            int32 GlobalArrayIndex(int32 laneIdx, int32 workChunkIdx) {
                return workChunkIdx * Compute::SUBGROUP_SIZE + laneIdx;
            }

            __host__ __device__ static
            int32 StepCount() {
                return 1;
            }

            __host__ __device__ static
            int32 WorkChunkCount(int32 arraySize) {
                return static_cast<int32>(IntDivRoundUp(static_cast<uint32>(arraySize), static_cast<uint32>(Compute::SUBGROUP_SIZE)));
            }
        };

        struct TaskPoolMemPoolDeleter
        {
            std::shared_ptr<int> allocatedPoolN{ std::make_shared<int>(0) };

            constexpr
            TaskPoolMemPoolDeleter() noexcept = default;

            void operator()(Processor::Base::TaskPool* pointer) const {
                if (pointer != nullptr) {
                    auto allocated = DeviceUniquePointer<int>::Allocate();
                    Kernels::GetTaskPoolCount<<<1,1>>>(pointer, allocated.Get());
                    #ifdef PRT_DEBUG
                    Compute::CudaCheckError(cudaDeviceSynchronize());
                    #endif
                    *allocatedPoolN = allocated.Download();
                }
            }
        };

    private:
        Processor processor{};
        Processor::Base::PersistentState<TaskPoolMemPoolDeleter> taskPoolState{};

        DeviceUniquePointer<KDTreeGPU::Node[]> nodes{};
        std::vector<DeviceLinkedListUniqueHead<Node, DeviceLinkedListDeleters::MemPoolDeleter<Node>>> nodeLists{};
        DeviceUniquePointer<DeviceLinkedListHead<Node>[]> nodeListsGPU;
        DeviceUniquePointer<int32[]> nodeListInfosGPU;

        UniqueMemoryPool primitiveMemPool;
        UniqueMemoryPool& dataMemPool;

        Compute::TaskPoolProcessorParams processorParams;

        mutable std::vector<KDTreeGPU::Node> nodesCPU{};

        bool isBuilt{ false };

        #if PRT_TIME_KERNELS
        DeviceUniqueTimer binTimer{};
        DeviceUniqueTimer classificationTimer{};
        DeviceUniqueTimer distributionTimer{};
        DeviceUniqueTimer leafTimer{};
        DeviceUniqueTimer lockTimer{};
        DeviceUniqueTimer subTimer{};
        #endif

        void DistributeNodes();

        int NodeListSize() { return processorParams.groupN *  processorParams.subgroupsPerGroupN; }
    };
}
