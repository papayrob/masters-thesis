/**
 * File name: KDTCPUTraverser.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "KDTCPUTraverser.hpp"

namespace ProperRT
{
    KDTCPUTraverser::KDTCPUTraverser(IMeshManager const& meshManager_, std::span<KDTNode const> nodes_, int32 rootIdx_, int32 stackSize_)
        : meshManager{ meshManager_ }, nodes{ nodes_ }, rootIdx{ rootIdx_ }, stackSize{ stackSize_ }
    {
    }

    std::optional<RayHit> KDTCPUTraverser::Traverse(Ray const& ray, cfloat maxDist, TraversalStatistics& traversalStats) {
        if (nodes.empty()) {
            return {};
        }

        HitInfo hitInfo = TraverseInternal<RayType::Normal>(ray, maxDist, traversalStats);
        if (hitInfo.tHit < maxDist) {
            return RayHit{
                ray,
                hitInfo.tHit,
                hitInfo.ic,
                meshManager.GetPrimitive(hitInfo.hitPrimitiveID),
                meshManager.GetRenderer(hitInfo.hitPrimitiveID),
                meshManager.GetSubmeshIdx(hitInfo.hitPrimitiveID)
            };
        }
        else {
            return {};
        }
    }

    bool KDTCPUTraverser::TraverseShadow(Ray const& ray, cfloat maxDist, TraversalStatistics& traversalStats) {
        if (nodes.empty()) {
            return false;
        }

        return (TraverseInternal<RayType::Shadow>(ray, maxDist, traversalStats).tHit < maxDist);
    }

    template<KDTCPUTraverser::RayType VRayType>
    void KDTCPUTraverser::LeafIntersect(LeafNode const& leaf, Ray const& ray, TraversalStatistics& traversalStats, cfloat& tHit, IntersectionCoordinates& ic, PUID& hitPrimitiveID) {
        // Iterate over all leaf primitives
        for (int32 i = 0; i < leaf.primitiveN; ++i) {
            ++(traversalStats.intersections);

            // Intersect with the ray, intersection counts only if it is closer than previous one
            cfloat tPrimitive;
            IntersectionCoordinates icPrimitive;
            if (Intersect(ray, meshManager.GetPrimitive(leaf.primitives[i]), tPrimitive, icPrimitive) && tPrimitive < tHit) {
                tHit = tPrimitive;
                ic = icPrimitive;
                hitPrimitiveID = leaf.primitives[i];

                // Return on first hit
                if constexpr (VRayType == RayType::Shadow) {
                    return;
                }
            }
        }
    }

    template<KDTCPUTraverser::RayType VRayType>
    KDTCPUTraverser::HitInfo KDTCPUTraverser::TraverseInternal(Ray const &ray, cfloat maxDist, TraversalStatistics& traversalStats) {
        cfloat tMin, tMax;
        HitInfo hitInfo;
        if constexpr (VRayType == RayType::Shadow) {
            hitInfo.tHit = maxDist;
        }
        else {
            hitInfo.tHit = std::numeric_limits<cfloat>::infinity();
        }

        if (meshManager.SceneBoundingBox().Intersect(ray, tMin, tMax)) {
            // Scene is too far away
            //TODO more with maxDist
            if (maxDist < tMin) {
                return hitInfo;
            }
            
            Node const* node = &nodes[rootIdx];

            // Init stack
            std::stack<StackItem> stack;
            stack.push(StackItem{ node, tMin, tMax });

            while (!stack.empty()) {
                tMin = stack.top().tMin;
                tMax = stack.top().tMax;
                node = stack.top().node;
                stack.pop();

                while (node->IsInner()) {
                    ++(traversalStats.traversalSteps);
                    auto axis = ToUnderlying(node->inner.tag);
                    if (std::abs(ray.direction[axis]) < ZERO_COMP_EPSILON) {
                        // Ray origin based
                        // Traverse left cell
                        if (ray.origin[axis] < node->inner.splitValue) {
                            node = &nodes[node->inner.leftChildIdx];
                        }
                        // Traverse right cell
                        else {
                            node = &nodes[node->inner.rightChildIdx];
                        }
                    }
                    else {
                        cfloat tSplit = (node->inner.splitValue - ray.origin[axis]) / ray.direction[axis];

                        int32 first, second;
                        // Ray direction based classification
                        if (ray.direction[axis] > 0._cf) {
                            first = node->inner.leftChildIdx;
                            second = node->inner.rightChildIdx;
                        }
                        else {
                            first = node->inner.rightChildIdx;
                            second = node->inner.leftChildIdx;
                        }

                        // Traverse only first cell
                        if (tMax <= tSplit) {
                            node = &nodes[first];
                        }
                        // Traverse only second cell
                        else if (tSplit <= tMin) {
                            node = &nodes[second];
                        }
                        // Traverse both cells, first and after returning from DFS second
                        else {
                            // Push
                            stack.push(StackItem{ &nodes[second], tSplit, tMax });
                            node = &nodes[first];
                            tMax = tSplit;
                        }
                    }
                }

                ++(traversalStats.traversalSteps);

                // Node is a leaf, so test for intersections with primitives
                LeafIntersect<VRayType>(node->leaf, ray, traversalStats, hitInfo.tHit, hitInfo.ic, hitInfo.hitPrimitiveID);

                // Early exit
                if constexpr (VRayType == RayType::Shadow) {
                    // Return on first hit closer than the light
                    if (hitInfo.tHit < maxDist) {
                        return hitInfo;
                    }
                }
                else {
                    // Hit is inside the cell, there cannot be a closer one
                    //TODO check if correct
                    if (hitInfo.tHit < tMax) {
                        return hitInfo;
                    }
                }
            }
        }

        return hitInfo;
    }
}
