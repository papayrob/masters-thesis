/**
 * File name: KDTCPU2GPUBuilder.cuh
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <optional>

#include "KDTGPUBuilderBase.cuh"
#include "KDTCPUBuilderBase.hpp"
#include "Compute/DeviceLinkedList.cuh"
#include "Compute/MemoryPool.cuh"
#include "Compute/ParallelScan.cuh"

namespace ProperRT
{
    /// @brief Class for wrapping a CPU builder as a GPU builder.
    class KDTCPU2GPUBuilder : public KDTGPUBuilderBase
    {
    public:
        /// @brief Standard ctor.
        /// @param builder_ CPU k-d tre builder instance
        KDTCPU2GPUBuilder(std::unique_ptr<KDTCPUBuilderBase>&& builder_)
            : builder{std::move(builder_)}
        {}

        /// @brief Dtor.
        ~KDTCPU2GPUBuilder() {
            Reset();
        }

        virtual
        void Build(GPUMeshManager& meshManager, DeviceUniquePointer<PUID[]> primitives, DeviceUniquePointer<AABB[]> primitiveBBs, KDTreeParams const& parameters) override {
            Reset();
            builder->Build(meshManager, primitives.Download(), primitiveBBs.Download(), parameters);

            // Count primitive references
            std::span<KDTNode> nodesCPU = builder->Nodes();
            int32 leafReferencesSize = 0;
            for (KDTNode const& nodeCPU : nodesCPU) {
                if (nodeCPU.IsLeaf()) {
                    leafReferencesSize += nodeCPU.leaf.primitiveN;
                }
            }

            // Allocate room for references
            leafReferences = DeviceUniquePointer<PUID[]>::Allocate(leafReferencesSize);
            // Copy references to GPU and change leaf pointers
            PUID* pointer = leafReferences.Get().data;
            for (KDTNode& nodeCPU : nodesCPU) {
                if (nodeCPU.IsLeaf() && nodeCPU.leaf.primitiveN > 0) {
                    KDTLeafNode& leaf = nodeCPU.leaf;
                    DevicePointer<PUID[]>{pointer, leaf.primitiveN}.Upload(std::span<PUID>(leaf.primitives, static_cast<std::size_t>(leaf.primitiveN)));
                    delete[] leaf.primitives;
                    leaf.primitives = pointer;
                    pointer += nodeCPU.leaf.primitiveN;
                }
            }

            assert(pointer == leafReferences.Get().data + leafReferences.Count());

            // Copy nodes with changed leaf pointers to GPU
            nodes = DeviceUniquePointer<KDTNode[]>::AllocateAndUpload(nodesCPU);

            isBuilt = true;
        }

        virtual
        bool IsBuilt() const override { return isBuilt; }

        virtual
        DevicePointer<KDTreeGPU::Node[]> NodesGPU() const override {
            return nodes.Get();
        }

        virtual
        std::span<KDTNode> Nodes() override {
            return builder->Nodes();
        }

        virtual
        std::vector<KDTNode> const& Nodes() const override {
            return static_cast<KDTCPUBuilderBase const*>(builder.get())->Nodes();
        }

        virtual
        int32 RootIdx() const override { return 0; }

        virtual
        KDTNode* Root() override {
            return &Nodes()[0];
        }

        virtual
        KDTNode const* Root() const override {
            return &Nodes()[0];
        }

        virtual
        std::vector<PUID> LeafPrimitives(KDTLeafNode const& leaf) const override {
            return DevicePointer<PUID[]>{ leaf.primitives, leaf.primitiveN }.Download();
        }

        virtual
        int32 MaxDepth(int32 primitiveN, KDTreeParams const& parameters) const override { return static_cast<int32>(parameters.maxDepth_k1 + parameters.maxDepth_k2 * std::log2(static_cast<cfloat>(primitiveN))); }

        virtual
        int64 AllocatedMemory() const override {
            return builder->AllocatedMemory() + nodes.Bytes() + leafReferences.Bytes();
        }

        virtual
        void Reset() override {
            isBuilt = false;
            // Null leaf pointers so delete is not called on wrong pointers
            for (KDTNode& nodeCPU : builder->Nodes()) {
                if (nodeCPU.IsLeaf()) {
                    nodeCPU.leaf.primitives = nullptr;
                }
            }
            builder->Reset();
            nodes = nullptr;
            leafReferences = nullptr;
        }

    private:
        std::unique_ptr<KDTCPUBuilderBase> builder{};
        DeviceUniquePointer<KDTNode[]> nodes{};
        DeviceUniquePointer<PUID[]> leafReferences{};

        bool isBuilt{ false };
    };
}
