/**
 * File name: KDMergeTreeCPU.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "KDTreeNavigator.hpp"
#include "Graphics/Raytracing/CPUMeshManager.hpp"
#include "Utilities/ObjectFactory.hpp"
#include "KDTCPUBuilderBase.hpp"

namespace ProperRT
{
    class KDTCPUTraverser;
    class KDTCPUMergeBuilder;

    /// @brief CPU implementation of a k-d merge tree.
    class KDMergeTreeCPU : public IAccelerationStructure, public IStructureValidator//, public NavigatableBinaryTree
    {
    public:
        using Node = KDTNode;
        using InnerNode = KDTInnerNode;
        using LeafNode = KDTLeafNode;

    public:
        KDTreeParams parameters;

        /// @brief Default ctor.
        KDMergeTreeCPU();

        /// @brief Dtor.
        ~KDMergeTreeCPU();
        virtual
        void BuildStatic(std::vector<CompPtr<Components::Renderer>>&& renderers) override;

        virtual
        void BuildDynamic(std::vector<CompPtr<Components::Renderer>>&& renderers) override;

        virtual
        bool IsBuilt() const override;

        virtual
        INavigator* TopNavigator() override;

        virtual
        IStructureValidator* Validator() override;

        virtual
        std::optional<RayHit> Traverse(Ray const& ray, cfloat maxDist = std::numeric_limits<cfloat>::infinity(), TraversalStatistics* traversalStats = nullptr) override;

        virtual
        bool TraverseShadow(Ray const& ray, cfloat maxDist, TraversalStatistics* traversalStats = nullptr) override;

        virtual
        AccStructStats const& GetStatistics() const override { return stats; }

        virtual
        AccStructStats& GetStatistics() override { return stats; }

        //virtual
        //std::unique_ptr<BinaryTreeNavigationNode> NavigationRoot();

        virtual
        std::optional<std::string> Validate() override;

    private:
        constexpr static inline
        int staticKey = 0;

        constexpr static inline
        int dynamicKey = 1;

        ObjectFactory<int, KDTCPUBuilderBase> builderFactory{};

        std::unique_ptr<KDTCPUMergeBuilder> mergeBuilder{};

        AccelerationStructureStatistics stats{};
    };
}
