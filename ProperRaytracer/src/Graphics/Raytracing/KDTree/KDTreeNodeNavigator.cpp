/**
 * File name: KDTreeNodeNavigator.cpp
 * Description: See KDTreeNavigator header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "KDTreeNavigator.hpp"

#include "glbinding/gl/gl.h"

#include "SceneDefinition/Components/Camera.hpp"

namespace ProperRT
{
    KDTreeNodeNavigator::KDTreeNodeNavigator(KDTreeNavigator* parentNavigator_)
        : Base{ parentNavigator_ }
    {
        primitiveSplitBoxes = std::make_shared<OrientedBoundingBoxMesh>(std::vector<OrientedBoundingBox>{ OrientedBoundingBox{}, OrientedBoundingBox{} });
        primitiveSplitBoxes->SetMaterial(Material{.colorDiffuse{NormalizedConvert<Color32f>(KDTreeNavigator::boundingBoxColor)}});
    }

    void KDTreeNodeNavigator::StartNavigating() {
        auto* kdNavNode = dynamic_cast<KDTreeNavigationNode*>(dynamic_cast<KDTreeNavigator*>(parentNavigator)->node.get());
        split = SplitPlane{ kdNavNode->SplitAxis(), kdNavNode->SplitCoordinate() };
        splitPlane = dynamic_cast<KDTreeNavigator*>(parentNavigator)->splitPlane;

        Base::StartNavigating();
    }

    void KDTreeNodeNavigator::Render() {
        gl::GLenum oldMode[2];
        gl::glGetIntegerv(gl::GLenum::GL_POLYGON_MODE, &oldMode[0]);

        gl::glPolygonMode(gl::GLenum::GL_FRONT_AND_BACK, gl::GLenum::GL_LINE);

        auto matrices = Components::Camera::MainCamera()->GetCameraMatrices();

        // Render primitives
        auto primitiveMeshTreeNode = dynamic_cast<KDTreeNavigator*>(parentNavigator)->primitiveMeshTreeNode;
        primitiveMeshTreeNode->Render(matrices);

        // Render node
        Matrix4x4f modelMat{ Matrix4x4f::Identity() };

        nodeBoundingBox->Rasterize(modelMat, matrices);

        gl::glPolygonMode(gl::GLenum::GL_FRONT_AND_BACK, (opaqueSplit ? gl::GLenum::GL_FILL : gl::GLenum::GL_LINE));
        splitPlane->Rasterize(modelMat, matrices);

        // Render selected
        gl::glPolygonMode(gl::GLenum::GL_FRONT_AND_BACK, gl::GLenum::GL_FILL);
        selectedPrimitive->Rasterize(modelMat, matrices);

        gl::glPolygonMode(gl::GLenum::GL_FRONT_AND_BACK, (opaqueBoxes ? gl::GLenum::GL_FILL : gl::GLenum::GL_LINE));
        primitiveSplitBoxes->Rasterize(modelMat, matrices);

        gl::glPolygonMode(gl::GLenum::GL_FRONT_AND_BACK, oldMode[0]);
    }

    void KDTreeNodeNavigator::UpdateMeshes() {
        Base::UpdateMeshes();

        auto* node = dynamic_cast<KDTreeNavigator*>(parentNavigator)->node.get();

        auto splitBBs = (split.axis == Axis3D::None) ?
            SplitClipBB(node->GetPrimitive(primitiveIdx), node->BoundingBox(), 0, node->BoundingBox().minBounds[0])
            :
            SplitClipBB(node->GetPrimitive(primitiveIdx), node->BoundingBox(), static_cast<int>(split.axis), split.coordinate);
        
        if (splitBBs.first.IsValid()) {
            primitiveSplitBoxes->Boxes()[0] = splitBBs.first;
        }
        else {
            primitiveSplitBoxes->Boxes()[0] = OrientedBoundingBox{};
        }

        if (splitBBs.second.IsValid()) {
            primitiveSplitBoxes->Boxes()[1] = splitBBs.second;
        }
        else {
            primitiveSplitBoxes->Boxes()[1] = OrientedBoundingBox{};
        }

        primitiveSplitBoxes->SyncToGPU();
    }
}
