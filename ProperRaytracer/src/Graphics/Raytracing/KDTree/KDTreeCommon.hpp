/**
 * File name: KDTreeCommon.hpp
 * Description: Contains common definitions for k-d tree related classes.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "Geometry/Vector.hpp"
#include "Graphics/Raytracing/IAccelerationStructure.hpp"
#include "Graphics/Raytracing/PUID.hpp"

namespace ProperRT
{
    /// @brief Evaluates the surface area heuristic and returns the result.
    /// @param leftSurface Surface area of the left node
    /// @param rightSurface Surface area of the right node
    /// @param parentSurface Surface area of the parent node
    /// @param leftPrimitiveN Number of primitives to the left of the split
    /// @param rightPrimitiveN Number of primitives to the right of the split
    /// @param traversalCost Traversal cost
    /// @param intersectionCost Intersection cost
    /// @return traversalCost + intersectionCost * (leftSurface / parentSurface * leftPrimitiveN + rightSurface / parentSurface * rightPrimitiveN)
    CUDA_FUNCTION inline
    cfloat ComputeSAH(
        cfloat leftSurface,
        cfloat rightSurface,
        cfloat parentSurface,
        int32 leftPrimitiveN,
        int32 rightPrimitiveN,
        cfloat traversalCost,
        cfloat intersectionCost
    ) {
        return traversalCost + intersectionCost * (leftSurface / parentSurface * leftPrimitiveN + rightSurface / parentSurface * rightPrimitiveN);
    }

    /// @brief Class for storing parameters for k-d trees.
    struct KDTreeParams
    {
        cfloat maxDepth_k1{};
        cfloat maxDepth_k2{};
        cfloat traversalCost{};
        cfloat intersectionCost{};
        int32 primitiveLimit{};
    };

    /// @brief Class for representing a (axis-aligned) splitting plane.
    struct SplitPlane
    {
        /// @brief Axis of the plane.
        Axis3D axis;
        /// @brief Coordinate of the plane.
        float coordinate;
    };

    /// @brief A common function to create KDTreeParams from the scene config.
    KDTreeParams CreateKDTreeParameters();

    /// @brief Enum for k-d tree node types.
    enum class KDTNodeType : int8 {
        InnerX = 0, InnerY, InnerZ, Leaf
    };

    struct KDTNode;

    /// @brief k-d tree inner node representation.
    struct KDTInnerNode
    {
        /// @brief Union tag (also includes information on split axis).
        KDTNodeType tag;
        /// @brief Split coordinate value.
        float splitValue;
        /// @brief Index of the right child.
        int32 leftChildIdx;
        /// @brief Index of the left child.
        int32 rightChildIdx;
    };

    /// @brief k-d tree leaf node representation.
    struct KDTLeafNode
    {
        /// @brief Union tag.
        KDTNodeType tag;
        /// @brief Number of primitive references in the leaf.
        int32 primitiveN;
        /// @brief Primitive reference array.
        PUID* primitives;
    };

    /// @brief k-d tree node union.
    struct KDTNode
    {
        union {
            /// @brief Inner node, only access if IsInner is true or if assigning.
            KDTInnerNode inner;
            /// @brief Leaf node, only access if IsLeaf is true or if assigning.
            KDTLeafNode leaf;
        };

        /// @brief Common union tag.
        CUDA_FUNCTION constexpr
        KDTNodeType Tag() const { return inner.tag; }

        /// @brief Returns true if the node is a leaf node.
        CUDA_FUNCTION constexpr
        bool IsLeaf() const { return Tag() == KDTNodeType::Leaf; }

        /// @brief Returns true if the node is an inner node.
        CUDA_FUNCTION constexpr
        bool IsInner() const { return !IsLeaf(); }
    };

    /// @brief Casts Axis3D to KDTNodeType.
    /// @param axis Axis value
    CUDA_FUNCTION inline
    KDTNodeType ToNodeType(Axis3D axis) {
        return static_cast<KDTNodeType>(ToUnderlying(axis));
    }

    /// @brief Casts KDTNodeType to Axis3D.
    /// @param tag Node tag
    CUDA_FUNCTION inline
    Axis3D ToAxis3D(KDTNodeType tag) {
        return static_cast<Axis3D>(ToUnderlying(tag));
    }

    /// @brief Enum for event types.
    enum class SplitEventType : int8 {
        Start = 0, End
    };

    /// @brief Class for pre-computing partial terms of the surface area of a box.
    /// @details Assuming only one side changes values and the others stay the same,
    /// using this class uses only one addition and multiplication to compute the area
    /// from the pre-computed terms.
    struct PartialSurfaceArea
    {
        /// @brief Pre-computed term.
        cfloat staticTerm, dynamicTerm;

        /// @brief Constructs the class and precomputes terms for a box with 2 set side lengths.
        /// @param side1 Length of one side
        /// @param side2 Length of a second side
        CUDA_FUNCTION
        PartialSurfaceArea(cfloat side1, cfloat side2)
            :
            staticTerm(2._cf * side1 * side2),
            dynamicTerm(2._cf * side1 + 2._cf * side2)
        {}
        
        /// @brief Computes the surface area based on the third side @p side3 and returns it.
        /// @param side3 Length of the last side
        CUDA_FUNCTION
        cfloat operator()(cfloat side3) const { return staticTerm + side3 * dynamicTerm; }
    };
}
