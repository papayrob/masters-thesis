/**
 * File name: KDTGPUTraverser.cuh
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <cuda/std/optional>

#include "KDTreeGPU.hpp"
#include "Graphics/Raytracing/GPUMeshManager.cuh"
#include "Compute/MemoryPool.cuh"
#include "KDTCPUTraverser.hpp"

namespace ProperRT
{
    class KDTGPUTraverser
    {
    public:
        using Node = KDTreeGPU::Node;
        using InnerNode = KDTreeGPU::InnerNode;
        using LeafNode = KDTreeGPU::LeafNode;

        /// @brief Class for storing recursion info in a stack.
        struct StackItem
        {
            Node* node;
            cfloat tMin, tMax;
        };

        /// @brief Class for storing a ray hit on the GPU.
        struct RayHitGPU
        {
            Ray ray;
            cfloat tHit;
            IntersectionCoordinates ic;
            Vector2cf uv;
            Vector3cf point;
            Vector3cf normal;
            Primitive primitive;
            PUID primitiveID;
            GPUMeshManagement::Instance* instance;

            __device__
            RayHitGPU(Ray const& ray_, cfloat tHit_ , IntersectionCoordinates const& ic_, Primitive const& primitive_, PUID primitiveID_, DevicePointer<GPUMeshManagement::Instance[]> instances)
                : ray{ ray_ }, tHit{ tHit_ * (1._cf - RayHit::SHIFT_EPSILON) }, ic{ ic_ }, uv{ /*TODO*/ }, point{ ray_.origin + tHit * ray_.direction },
                normal{ GetNormal(primitive_, ic_) }, primitive{ primitive_ }, primitiveID{ primitiveID_ }, instance{ &instances[primitiveID_.instanceID] }
            {}

            __host__
            RayHit ToRayHitCPU(GPUMeshManager const& meshManager) {
                return RayHit{
                    ray,
                    tHit,
                    ic,
                    primitive,
                    meshManager.GetRenderer(primitiveID),
                    instance->submeshIdx
                };
            }
        };

        /// @brief Enum for the restart strategy for the stackless and short-stack travrsal algs.
        enum class RestartStrategy : int8 {
            Restart, PushDown
        };

    public:
        /// @brief Instances to retrieve primitives from PUID.
        DevicePointer<GPUMeshManagement::Instance[]> instances;
        /// @brief Tree nodes.
        DevicePointer<Node[]> nodes;
        /// @brief Scene bounding box.
        AABB sceneBoundingBox;
        /// @brief Index of the root node.
        int32 rootIdx;

        /// @brief Traverses the nodes using a stack-based traversal algorithm.
        /// @param ray Ray to traverse with
        /// @param maxDist Search only for intersections closer then this distance
        /// @param stack Stack used in the traversal
        /// @param stackSize The capacity of the stack
        /// @param memPool Memory pool to allocate new room for the stack in
        /// @param traversalStats Statistics counters
        /// @return Optional with a ray hit structure, empty if no intersection was found, otherwise contains the closest hit
        __device__
        cuda::std::optional<RayHitGPU> Traverse(Ray const& ray, cfloat maxDist, StackItem*& stack, int32& stackSize, DevicePointer<MemoryPool> memPool, TraversalStatistics& traversalStats) const {
            if (nodes == nullptr) {
                return {};
            }

            assert((instances != nullptr || (nodes[rootIdx].IsLeaf() && nodes[rootIdx].leaf.primitives == nullptr && nodes[rootIdx].leaf.primitiveN == 0)) && stack != nullptr && stackSize > 0);

            HitInfo hitInfo = TraverseInternal<RayType::Normal>(ray, maxDist, stack, stackSize, memPool, traversalStats);
            if (hitInfo.tHit < maxDist) {
                return RayHitGPU{
                    ray,
                    hitInfo.tHit,
                    hitInfo.ic,
                    GPUMeshManager::GetPrimitive(instances, hitInfo.hitPrimitiveID),
                    hitInfo.hitPrimitiveID,
                    instances
                };
            }
            else {
                return {};
            }
        }

        /// @brief Traverses the nodes using a stackless traversal algorithm.
        /// @tparam VRestartStrat Restart strategy
        /// @param ray Ray to traverse with
        /// @param maxDist Search only for intersections closer then this distance
        /// @param traversalStats Statistics counters
        /// @return Optional with a ray hit structure, empty if no intersection was found, otherwise contains the closest hit
        template<RestartStrategy VRestartStrat>
        __device__
        cuda::std::optional<RayHitGPU> Traverse(Ray const& ray, cfloat maxDist, TraversalStatistics& traversalStats) const {
            if (nodes == nullptr) {
                return {};
            }

            assert(instances != nullptr || (nodes[rootIdx].IsLeaf() && nodes[rootIdx].leaf.primitives == nullptr && nodes[rootIdx].leaf.primitiveN == 0));

            HitInfo hitInfo = TraverseInternal<RayType::Normal, VRestartStrat>(ray, maxDist, traversalStats);
            if (hitInfo.tHit < maxDist) {
                return RayHitGPU{
                    ray,
                    hitInfo.tHit,
                    hitInfo.ic,
                    GPUMeshManager::GetPrimitive(instances, hitInfo.hitPrimitiveID),
                    hitInfo.hitPrimitiveID,
                    instances
                };
            }
            else {
                return {};
            }
        }

        /// @brief Traverses the nodes using a short-stack traversal algorithm.
        /// @tparam VRestartStrat Restart strategy
        /// @param ray Ray to traverse with
        /// @param maxDist Search only for intersections closer then this distance
        /// @param stack Stack used in the traversal
        /// @param stackSize The capacity of the stack
        /// @param traversalStats Statistics counters
        /// @return Optional with a ray hit structure, empty if no intersection was found, otherwise contains the closest hit
        template<RestartStrategy VRestartStrat>
        __device__
        cuda::std::optional<RayHitGPU> Traverse(Ray const& ray, cfloat maxDist, StackItem* stack, int8 stackSize, TraversalStatistics& traversalStats) const {
            if (nodes == nullptr) {
                return {};
            }

            assert((instances != nullptr || (nodes[rootIdx].IsLeaf() && nodes[rootIdx].leaf.primitives == nullptr && nodes[rootIdx].leaf.primitiveN == 0)) && stack != nullptr && stackSize > 0);

            HitInfo hitInfo = TraverseInternal<RayType::Normal, VRestartStrat>(ray, maxDist, stack, stackSize, traversalStats);
            if (hitInfo.tHit < maxDist) {
                return RayHitGPU{
                    ray,
                    hitInfo.tHit,
                    hitInfo.ic,
                    GPUMeshManager::GetPrimitive(instances, hitInfo.hitPrimitiveID),
                    hitInfo.hitPrimitiveID,
                    instances
                };
            }
            else {
                return {};
            }
        }
        
        /// @brief Traverses the nodes using a stack-based traversal algorithm for shadow rays.
        /// @param ray Ray to traverse with
        /// @param maxDist Search only for intersections closer then this distance
        /// @param stack Stack used in the traversal
        /// @param stackSize The capacity of the stack
        /// @param memPool Memory pool to allocate new room for the stack in
        /// @param traversalStats Statistics counters
        /// @return True if any intersection was found
        __device__
        bool TraverseShadow(Ray const& ray, cfloat maxDist, StackItem*& stack, int32 stackSize, DevicePointer<MemoryPool> memPool, TraversalStatistics& traversalStats) const {
            if (nodes == nullptr) {
                return false;
            }

            assert(((instances != nullptr || (nodes[rootIdx].IsLeaf() && nodes[rootIdx].leaf.primitives == nullptr && nodes[rootIdx].leaf.primitiveN == 0)) || (nodes[rootIdx].IsLeaf() && nodes[rootIdx].leaf.primitives == nullptr && nodes[rootIdx].leaf.primitiveN == 0)) && stack != nullptr && stackSize > 0);

            return (TraverseInternal<RayType::Shadow>(ray, maxDist, stack, stackSize, memPool, traversalStats).tHit < maxDist);
        }

        /// @brief Traverses the nodes using a stackless traversal algorithm for shadow rays.
        /// @tparam VRestartStrat Restart strategy
        /// @param ray Ray to traverse with
        /// @param maxDist Search only for intersections closer then this distance
        /// @param traversalStats Statistics counters
        /// @return True if any intersection was found
        template<RestartStrategy VRestartStrat>
        __device__
        bool TraverseShadow(Ray const& ray, cfloat maxDist, TraversalStatistics& traversalStats) const {
            if (nodes == nullptr) {
                return false;
            }

            assert(instances != nullptr);

            return (TraverseInternal<RayType::Shadow, VRestartStrat>(ray, maxDist, traversalStats).tHit < maxDist);
        }

        /// @brief Traverses the nodes using a short-stack traversal algorithm for shadow rays.
        /// @tparam VRestartStrat Restart strategy
        /// @param ray Ray to traverse with
        /// @param maxDist Search only for intersections closer then this distance
        /// @param stack Stack used in the traversal
        /// @param stackSize The capacity of the stack
        /// @param traversalStats Statistics counters
        /// @return True if any intersection was found
        template<RestartStrategy VRestartStrat>
        __device__
        bool TraverseShadow(Ray const& ray, cfloat maxDist, StackItem* stack, int8 stackSize, TraversalStatistics& traversalStats) const {
            if (nodes == nullptr) {
                return false;
            }

            assert((instances != nullptr || (nodes[rootIdx].IsLeaf() && nodes[rootIdx].leaf.primitives == nullptr && nodes[rootIdx].leaf.primitiveN == 0)) && stack != nullptr && stackSize > 0);

            return (TraverseInternal<RayType::Shadow, VRestartStrat>(ray, maxDist, stack, stackSize, traversalStats).tHit < maxDist);
        }

    private:

        struct HitInfo
        {
            cfloat tHit;
            IntersectionCoordinates ic;
            PUID hitPrimitiveID;
        };

        enum class RayType {
            Normal, Shadow
        };

    private:

        template<RayType VRayType>
        __device__
        void LeafIntersect(LeafNode const& leaf, Ray const& ray, TraversalStatistics& traversalStats, cfloat& tHit, IntersectionCoordinates& ic, PUID& hitPrimitiveID) const {
            // Iterate over all leaf primitives
            int32 primitiveN = leaf.primitiveN;
            PUID* primitives = leaf.primitives;
            for (int32 i = 0; i < primitiveN; ++i) {
                ++(traversalStats.intersections);

                // Intersect with the ray, intersection counts only if it is closer than previous one
                cfloat tPrimitive;
                IntersectionCoordinates icPrimitive;
                if (Intersect(ray, GPUMeshManager::GetPrimitive(instances, primitives[i]), tPrimitive, icPrimitive) && tPrimitive < tHit) {
                    tHit = tPrimitive;
                    ic = icPrimitive;
                    hitPrimitiveID = primitives[i];

                    // Return on first hit
                    if constexpr (VRayType == RayType::Shadow) {
                        return;
                    }
                }
            }
        }

        template<RayType VRayType>
        __device__
        HitInfo TraverseInternal(Ray const& ray, cfloat maxDist, StackItem*& stack, int32& stackSize, DevicePointer<MemoryPool> memPool, TraversalStatistics& traversalStats) const {
            cfloat tMin, tMax;
            HitInfo hitInfo;
            if constexpr (VRayType == RayType::Shadow) {
                hitInfo.tHit = maxDist;
            }
            else {
                hitInfo.tHit = cuda::std::numeric_limits<cfloat>::infinity();
            }

            if (sceneBoundingBox.Intersect(ray, tMin, tMax)) {
                // Scene is too far away
                //TODO more with maxDist
                if (maxDist < tMin) {
                    return hitInfo;
                }
                
                Node* node = &nodes[rootIdx];

                // Init stack
                stack[0] = StackItem{ node, tMin, tMax };
                int stackTop = 1;

                while (stackTop > 0) {
                    // Pop
                    tMin = stack[--stackTop].tMin;
                    tMax = stack[stackTop].tMax;
                    node = stack[stackTop].node;

                    while (node->IsInner()) {
                        ++(traversalStats.traversalSteps);
                        auto axis = ToUnderlying(node->inner.tag);
                        if (abs(ray.direction[axis]) < ZERO_COMP_EPSILON) {
                            // Ray origin based
                            // Traverse left cell
                            if (ray.origin[axis] < node->inner.splitValue) {
                                node = &nodes[node->inner.leftChildIdx];
                            }
                            // Traverse right cell
                            else {
                                node = &nodes[node->inner.rightChildIdx];
                            }
                        }
                        else {
                            cfloat tSplit = (node->inner.splitValue - ray.origin[axis]) / ray.direction[axis];

                            int32 first, second;
                            // Ray direction based classification
                            if (ray.direction[axis] > 0._cf) {
                                first = node->inner.leftChildIdx;
                                second = node->inner.rightChildIdx;
                            }
                            else {
                                first = node->inner.rightChildIdx;
                                second = node->inner.leftChildIdx;
                            }

                            // Traverse only first cell
                            if (tMax <= tSplit) {
                                node = &nodes[first];
                            }
                            // Traverse only second cell
                            else if (tSplit <= tMin) {
                                node = &nodes[second];
                            }
                            // Traverse both cells, second after returning from DFS
                            else {
                                // Push
                                if (stackTop == stackSize) {
                                    StackItem* oldStack = stack;
                                    stack = memPool->Allocate<StackItem>(1, stackSize * 2).data;
                                    for (int i = 0; i < stackSize; ++i) {
                                        stack[i] = oldStack[i];
                                    }
                                    stackSize *= 2;
                                }

                                stack[stackTop++] = StackItem{ &nodes[second], tSplit, tMax };
                                node = &nodes[first];
                                tMax = tSplit;
                            }
                        }
                    }

                    ++(traversalStats.traversalSteps);

                    // Node is a leaf, so test for intersections with primitives
                    LeafIntersect<VRayType>(node->leaf, ray, traversalStats, hitInfo.tHit, hitInfo.ic, hitInfo.hitPrimitiveID);

                    // Early exit
                    if constexpr (VRayType == RayType::Shadow) {
                        // Return on first hit closer than the light
                        if (hitInfo.tHit < maxDist) {
                            return hitInfo;
                        }
                    }
                    else {
                        // Hit is inside the cell, there cannot be a closer one
                        if (hitInfo.tHit < tMax) {
                            return hitInfo;
                        }
                    }
                }
            }

            return hitInfo;
        }
        
        // Restart without stack
        template<RayType VRayType, RestartStrategy VRestartStrat>
            requires (VRestartStrat == RestartStrategy::Restart)
        __device__
        HitInfo TraverseInternal(Ray const& ray, cfloat maxDist, TraversalStatistics& traversalStats) const {
            cfloat tMin, tMax, sceneMax;
            HitInfo hitInfo;
            if constexpr (VRayType == RayType::Shadow) {
                hitInfo.tHit = maxDist;
            }
            else {
                hitInfo.tHit = cuda::std::numeric_limits<cfloat>::infinity();
            }

            if (sceneBoundingBox.Intersect(ray, tMin, sceneMax)) {
                // Scene is too far away
                //TODO more with maxDist
                if (maxDist < tMin) {
                    return hitInfo;
                }

                tMax = tMin;

                while (tMax < sceneMax) {
                    // Restart
                    Node* node = &nodes[rootIdx];
                    tMin = tMax;
                    tMax = sceneMax;

                    while (node->IsInner()) {
                        ++(traversalStats.traversalSteps);
                        auto axis = ToUnderlying(node->inner.tag);
                        if (abs(ray.direction[axis]) < ZERO_COMP_EPSILON) {
                            // Ray origin based
                            // Traverse left cell
                            if (ray.origin[axis] < node->inner.splitValue) {
                                node = &nodes[node->inner.leftChildIdx];
                            }
                            // Traverse right cell
                            else {
                                node = &nodes[node->inner.rightChildIdx];
                            }
                        }
                        else {
                            cfloat tSplit = (node->inner.splitValue - ray.origin[axis]) / ray.direction[axis];

                            int32 first, second;
                            // Ray direction based classification
                            if (ray.direction[axis] > 0._cf) {
                                first = node->inner.leftChildIdx;
                                second = node->inner.rightChildIdx;
                            }
                            else {
                                first = node->inner.rightChildIdx;
                                second = node->inner.leftChildIdx;
                            }

                            // Traverse only first cell
                            if (tMax <= tSplit) {
                                node = &nodes[first];
                            }
                            // Traverse only second cell
                            else if (tSplit <= tMin) {
                                node = &nodes[second];
                            }
                            // Traverse first cell, second will be traversed after restart
                            else {
                                node = &nodes[first];
                                tMax = tSplit;
                            }
                        }
                    }

                    ++(traversalStats.traversalSteps);

                    // Node is a leaf, so test for intersections with primitives
                    LeafIntersect<VRayType>(node->leaf, ray, traversalStats, hitInfo.tHit, hitInfo.ic, hitInfo.hitPrimitiveID);

                    // Early exit
                    if constexpr (VRayType == RayType::Shadow) {
                        // Return on first hit closer than the light
                        if (hitInfo.tHit < maxDist) {
                            return hitInfo;
                        }
                    }
                    else {
                        // Hit is inside the cell, there cannot be a closer one
                        if (hitInfo.tHit < tMax) {
                            return hitInfo;
                        }
                    }
                }
            }

            return hitInfo;
        }

        // Push-down without stack
        template<RayType VRayType, RestartStrategy VRestartStrat>
            requires (VRestartStrat == RestartStrategy::PushDown)
        __device__
        HitInfo TraverseInternal(Ray const& ray, cfloat maxDist, TraversalStatistics& traversalStats) const {
            cfloat tMin, tMax, sceneMax;
            HitInfo hitInfo;
            if constexpr (VRayType == RayType::Shadow) {
                hitInfo.tHit = maxDist;
            }
            else {
                hitInfo.tHit = cuda::std::numeric_limits<cfloat>::infinity();
            }

            if (sceneBoundingBox.Intersect(ray, tMin, sceneMax)) {
                // Scene is too far away
                //TODO more with maxDist
                if (maxDist < tMin) {
                    return hitInfo;
                }

                Node* pushedRoot = &nodes[rootIdx];
                tMax = tMin;

                while (tMax < sceneMax) {
                    // Restart
                    Node* node = pushedRoot;
                    tMin = tMax;
                    tMax = sceneMax;
                    bool pushDown = true;

                    while (node->IsInner()) {
                        ++(traversalStats.traversalSteps);
                        auto axis = ToUnderlying(node->inner.tag);
                        if (abs(ray.direction[axis]) < ZERO_COMP_EPSILON) {
                            // Ray origin based
                            // Traverse left cell
                            if (ray.origin[axis] < node->inner.splitValue) {
                                node = &nodes[node->inner.leftChildIdx];
                            }
                            // Traverse right cell
                            else {
                                node = &nodes[node->inner.rightChildIdx];
                            }
                        }
                        else {
                            cfloat tSplit = (node->inner.splitValue - ray.origin[axis]) / ray.direction[axis];

                            int32 first, second;
                            // Ray direction based classification
                            if (ray.direction[axis] > 0._cf) {
                                first = node->inner.leftChildIdx;
                                second = node->inner.rightChildIdx;
                            }
                            else {
                                first = node->inner.rightChildIdx;
                                second = node->inner.leftChildIdx;
                            }

                            // Traverse only first cell
                            if (tMax <= tSplit) {
                                node = &nodes[first];
                            }
                            // Traverse only second cell
                            else if (tSplit <= tMin) {
                                node = &nodes[second];
                            }
                            // Traverse first cell, second will be traversed after restart
                            else {
                                // Disable push down to traverse both nodes
                                pushDown = false;
                                node = &nodes[first];
                                tMax = tSplit;
                            }

                        }

                        if (pushDown) {
                            pushedRoot = node;
                        }
                    }

                    ++(traversalStats.traversalSteps);

                    // Node is a leaf, so test for intersections with primitives
                    LeafIntersect<VRayType>(node->leaf, ray, traversalStats, hitInfo.tHit, hitInfo.ic, hitInfo.hitPrimitiveID);

                    // Early exit
                    if constexpr (VRayType == RayType::Shadow) {
                        // Return on first hit closer than the light
                        if (hitInfo.tHit < maxDist) {
                            return hitInfo;
                        }
                    }
                    else {
                        // Hit is inside the cell, there cannot be a closer one
                        if (hitInfo.tHit < tMax) {
                            return hitInfo;
                        }
                    }
                }
            }

            return hitInfo;
        }

        // Short-stack with restart
        template<RayType VRayType, RestartStrategy VRestartStrat>
            requires (VRestartStrat == RestartStrategy::Restart)
        __device__
        HitInfo TraverseInternal(Ray const& ray, cfloat maxDist, StackItem* stack, int8 stackSize, TraversalStatistics& traversalStats) const {
            cfloat tMin, tMax, sceneMax;
            HitInfo hitInfo;
            if constexpr (VRayType == RayType::Shadow) {
                hitInfo.tHit = maxDist;
            }
            else {
                hitInfo.tHit = cuda::std::numeric_limits<cfloat>::infinity();
            }

            if (sceneBoundingBox.Intersect(ray, tMin, sceneMax)) {
                // Scene is too far away
                //TODO more with maxDist
                if (maxDist < tMin) {
                    return hitInfo;
                }

                tMax = tMin;
                int8 stackFullness = 0, stackTop;

                while (tMax < sceneMax) {
                    Node* node;

                    // Restart
                    if (stackFullness == 0) {
                        node = &nodes[rootIdx];
                        tMin = tMax;
                        tMax = sceneMax;
                        stackTop = 0;
                    }
                    // Pop
                    else {
                        --stackTop;
                        --stackFullness;
                        
                        // Loop
                        if (stackTop < 0) {
                            stackTop = stackSize - 1;
                        }

                        tMin = stack[stackTop].tMin;
                        tMax = stack[stackTop].tMax;
                        node = stack[stackTop].node;
                    }

                    while (node->IsInner()) {
                        ++(traversalStats.traversalSteps);
                        auto axis = ToUnderlying(node->inner.tag);
                        if (abs(ray.direction[axis]) < ZERO_COMP_EPSILON) {
                            // Ray origin based
                            // Traverse left cell
                            if (ray.origin[axis] < node->inner.splitValue) {
                                node = &nodes[node->inner.leftChildIdx];
                            }
                            // Traverse right cell
                            else {
                                node = &nodes[node->inner.rightChildIdx];
                            }
                        }
                        else {
                            cfloat tSplit = (node->inner.splitValue - ray.origin[axis]) / ray.direction[axis];

                            int32 first, second;
                            // Ray direction based classification
                            if (ray.direction[axis] > 0._cf) {
                                first = node->inner.leftChildIdx;
                                second = node->inner.rightChildIdx;
                            }
                            else {
                                first = node->inner.rightChildIdx;
                                second = node->inner.leftChildIdx;
                            }

                            // Traverse only first cell
                            if (tMax <= tSplit) {
                                node = &nodes[first];
                            }
                            // Traverse only second cell
                            else if (tSplit <= tMin) {
                                node = &nodes[second];
                            }
                            // Traverse first cell, second will be traversed after restart or through the short stack
                            else {
                                // Push
                                stack[stackTop++] = StackItem{ &nodes[second], tSplit, tMax };
                                node = &nodes[first];
                                tMax = tSplit;

                                if (stackFullness < stackSize) {
                                    ++stackFullness;
                                }

                                // Loop
                                if (stackTop == stackSize) {
                                    stackTop = 0;
                                }
                            }
                        }
                    }

                    ++(traversalStats.traversalSteps);

                    // Node is a leaf, so test for intersections with primitives
                    LeafIntersect<VRayType>(node->leaf, ray, traversalStats, hitInfo.tHit, hitInfo.ic, hitInfo.hitPrimitiveID);

                    // Early exit
                    if constexpr (VRayType == RayType::Shadow) {
                        // Return on first hit closer than the light
                        if (hitInfo.tHit < maxDist) {
                            return hitInfo;
                        }
                    }
                    else {
                        // Hit is inside the cell, there cannot be a closer one
                        if (hitInfo.tHit < tMax) {
                            return hitInfo;
                        }
                    }
                }
            }

            return hitInfo;
        }

        // Short-stack with push-down
        template<RayType VRayType, RestartStrategy VRestartStrat>
            requires (VRestartStrat == RestartStrategy::PushDown)
        __device__
        HitInfo TraverseInternal(Ray const& ray, cfloat maxDist, StackItem* stack, int8 stackSize, TraversalStatistics& traversalStats) const {
            cfloat tMin, tMax, sceneMax;
            HitInfo hitInfo;
            if constexpr (VRayType == RayType::Shadow) {
                hitInfo.tHit = maxDist;
            }
            else {
                hitInfo.tHit = cuda::std::numeric_limits<cfloat>::infinity();
            }

            if (sceneBoundingBox.Intersect(ray, tMin, sceneMax)) {
                // Scene is too far away
                //TODO more with maxDist
                if (maxDist < tMin) {
                    return hitInfo;
                }

                tMax = tMin;
                Node* pushedRoot = &nodes[rootIdx];
                int8 stackFullness = 0, stackTop;

                while (tMax < sceneMax) {
                    Node* node;
                    bool pushDown;

                    // Restart
                    if (stackFullness == 0) {
                        node = pushedRoot;
                        tMin = tMax;
                        tMax = sceneMax;
                        stackTop = 0;
                        pushDown = true;
                    }
                    // Pop
                    else {
                        --stackTop;
                        --stackFullness;
                        
                        // Loop
                        if (stackTop < 0) {
                            stackTop = stackSize - 1;
                        }

                        tMin = stack[stackTop].tMin;
                        tMax = stack[stackTop].tMax;
                        node = stack[stackTop].node;
                        // Push down can only be enabled till the first stack push
                        pushDown = false;
                    }

                    while (node->IsInner()) {
                        ++(traversalStats.traversalSteps);
                        auto axis = ToUnderlying(node->inner.tag);
                        if (abs(ray.direction[axis]) < ZERO_COMP_EPSILON) {
                            // Ray origin based
                            // Traverse left cell
                            if (ray.origin[axis] < node->inner.splitValue) {
                                node = &nodes[node->inner.leftChildIdx];
                            }
                            // Traverse right cell
                            else {
                                node = &nodes[node->inner.rightChildIdx];
                            }
                        }
                        else {
                            cfloat tSplit = (node->inner.splitValue - ray.origin[axis]) / ray.direction[axis];

                            int32 first, second;
                            // Ray direction based classification
                            if (ray.direction[axis] > 0._cf) {
                                first = node->inner.leftChildIdx;
                                second = node->inner.rightChildIdx;
                            }
                            else {
                                first = node->inner.rightChildIdx;
                                second = node->inner.leftChildIdx;
                            }

                            // Traverse only first cell
                            if (tMax <= tSplit) {
                                node = &nodes[first];
                            }
                            // Traverse only second cell
                            else if (tSplit <= tMin) {
                                node = &nodes[second];
                            }
                            // Traverse first cell, second will be traversed after restart or through the short stack
                            else {
                                // Push
                                stack[stackTop++] = StackItem{ &nodes[second], tSplit, tMax };
                                node = &nodes[first];
                                tMax = tSplit;
                                // Disable push down
                                pushDown = false;

                                if (stackFullness < stackSize) {
                                    ++stackFullness;
                                }

                                // Loop
                                if (stackTop == stackSize) {
                                    stackTop = 0;
                                }
                            }
                        }

                        if (pushDown) {
                            pushedRoot = node;
                        }
                    }

                    ++(traversalStats.traversalSteps);

                    // Node is a leaf, so test for intersections with primitives
                    LeafIntersect<VRayType>(node->leaf, ray, traversalStats, hitInfo.tHit, hitInfo.ic, hitInfo.hitPrimitiveID);

                    // Early exit
                    if constexpr (VRayType == RayType::Shadow) {
                        // Return on first hit closer than the light
                        if (hitInfo.tHit < maxDist) {
                            return hitInfo;
                        }
                    }
                    else {
                        // Hit is inside the cell, there cannot be a closer one
                        if (hitInfo.tHit < tMax) {
                            return hitInfo;
                        }
                    }
                }
            }

            return hitInfo;
        }
    };
}
