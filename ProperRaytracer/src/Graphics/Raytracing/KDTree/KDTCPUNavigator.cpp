/**
 * File name: KDTCPUNavigator.cpp
 * Description: See KDTreeCPU header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "KDTreeCPU.hpp"

#include <glbinding/gl/gl.h>
#include "KDTreeValidatorCommon.hpp"
#include "KDTCPUBuilderBase.hpp"
#include "SceneDefinition/Components/Camera.hpp"
#include "SceneDefinition/Components/Transform.hpp"

namespace ProperRT
{
    KDTreeCPU::TreeNavigator::TreeNavigator(KDTreeCPU* tree_, bool selectedStatic_)
        : Base{ tree_ }, selectedStatic{selectedStatic_}
    {
    }

    bool KDTreeCPU::TreeNavigator::CanNavigateInto() {
        return node->PrimitiveCount() > 0;
    }

    IMeshManager const& KDTreeCPU::TreeNavigator::GetMeshManager() {
        if (selectedStatic) {
            return *dynamic_cast<KDTreeCPU*>(tree)->staticMeshManager.get();
        }
        else {
            return *dynamic_cast<KDTreeCPU*>(tree)->dynamicMeshManager.get();
        }
    }

    std::unique_ptr<BinaryTreeNodeNavigator> KDTreeCPU::TreeNavigator::CreateNodeNavigator() {
        return std::make_unique<KDTreeCPU::NodeNavigator>(this);
    }

    KDTreeCPU::NodeNavigator::NodeNavigator(KDTreeCPU::TreeNavigator *parentNavigator_)
        : Base{ parentNavigator_ }
    {
    }

    KDTreeCPU::ChooseTreeNavigator::ChooseTreeNavigator(KDTreeCPU* tree_)
        : tree{tree_}
    {
        boundingBoxMesh = std::make_shared<OrientedBoundingBoxMesh>(std::vector<OrientedBoundingBox>{OrientedBoundingBox{}, OrientedBoundingBox{}});
        boundingBoxMesh->SetMaterial(Material{.colorDiffuse{NormalizedConvert<Color32f>(KDTreeNavigator::boundingBoxColor)}});
    }

    void KDTreeCPU::ChooseTreeNavigator::StartNavigating() {
        if (tree->GetStaticMeshManager() != nullptr) {
            boundingBoxMesh->Boxes()[0] = tree->GetStaticMeshManager()->SceneBoundingBox();
        }
        else {
            boundingBoxMesh->Boxes()[0] = OrientedBoundingBox{};
        }

        boundingBoxMesh->Boxes()[1] = tree->GetDynamicMeshManager()->SceneBoundingBox();
        boundingBoxMesh->SyncToGPU();
    }

    void KDTreeCPU::ChooseTreeNavigator::Navigate() {
        ImGui::RadioButton("Static tree", &radioValue, 0);
        ImGui::RadioButton("Dynamic tree", &radioValue, 1);
    }

    void KDTreeCPU::ChooseTreeNavigator::NavigateOut() {
        boundingBoxMesh->Boxes()[0] = OrientedBoundingBox{};
        boundingBoxMesh->Boxes()[1] = OrientedBoundingBox{};
    }

    bool KDTreeCPU::ChooseTreeNavigator::CanNavigateInto() {
        if (radioValue == 0) {
            return (tree->GetStaticBuilder() != nullptr && tree->GetStaticBuilder()->IsBuilt());
        }
        else if (radioValue == 1) {
            return tree->GetDynamicBuilder()->IsBuilt();
        }
        else {
            return false;
        }
    }

    INavigator* KDTreeCPU::ChooseTreeNavigator::NavigateInto() {
        if (radioValue == 0) {
            tree->staticNavigator.StartNavigating();
            return &tree->staticNavigator;
        }
        else if (radioValue == 1) {
            tree->dynamicNavigator.StartNavigating();
            return &tree->dynamicNavigator;
        }
        else {
            return nullptr;
        }
    }

    void KDTreeCPU::ChooseTreeNavigator::Render() {
        gl::GLenum oldMode[2];
        gl::glGetIntegerv(gl::GLenum::GL_POLYGON_MODE, &oldMode[0]);
        gl::glPolygonMode(gl::GLenum::GL_FRONT_AND_BACK, gl::GLenum::GL_LINE);

        auto mat{Matrix4x4f::Identity()};
        auto camMats = Components::Camera::MainCamera()->GetCameraMatrices();

        boundingBoxMesh->Rasterize(mat, camMats);

        gl::glPolygonMode(gl::GLenum::GL_FRONT_AND_BACK, oldMode[0]);
    }

    std::optional<std::string> KDTreeCPU::ChooseTreeNavigator::Validate() {
        auto& stats = tree->GetStatistics();
        stats.ResetBuildStats();

        std::vector<int32> depths{};
        std::vector<int32> primitiveCounts{};
        
        std::optional<std::string> ret;
        if (tree->GetStaticBuilder() != nullptr && tree->GetStaticMeshManager() != nullptr) {
            auto sret = KDTreeValidateCommon(*tree, tree->GetStaticBuilder()->Root(), *tree->GetStaticMeshManager(), *tree->GetStaticBuilder(), tree->GetStaticBuilder()->Nodes(), depths, primitiveCounts);
            if (sret.has_value()) {
                ret = "Static: " + sret.value();
            }
        }

        if (tree->GetDynamicBuilder() != nullptr && tree->GetDynamicMeshManager() != nullptr) {
            auto dret = KDTreeValidateCommon(*tree, tree->GetDynamicBuilder()->Root(), *tree->GetDynamicMeshManager(), *tree->GetDynamicBuilder(), tree->GetDynamicBuilder()->Nodes(), depths, primitiveCounts);
            if (dret.has_value()) {
                if (ret.has_value()) {
                    ret = ret.value() + "\nDynamic: " + dret.value();
                }
                else {
                    ret = "Dynamic: " + dret.value();
                }
            }
        }

        stats.avgPrimitivesInLeaf = static_cast<float>(std::reduce(primitiveCounts.begin(), primitiveCounts.end())) / static_cast<float>(primitiveCounts.size());
        stats.avgDepth = static_cast<float>(std::reduce(depths.begin(), depths.end())) / static_cast<float>(depths.size());

        auto& appStats = RTApp::Instance()->statistics;
        appStats.AddData("MemoryConsumption", stats.memoryConsumption);
        appStats.AddData("Cost", stats.cost);
        appStats.AddData("InnerNodes", stats.innerNodes);
        appStats.AddData("LeafNodes", stats.leafNodes);
        appStats.AddData("EmptyNodes", stats.emptyNodes);
        appStats.AddData("AveragePrimitivesInLeaf", stats.avgPrimitivesInLeaf);
        appStats.AddData("MaxPrimitivesInLeaf", stats.maxPrimitivesInLeaf);
        appStats.AddData("AverageDepth", stats.avgDepth);
        appStats.AddData("MaxDepth", stats.maxDepth);

        return ret;
    }
}
