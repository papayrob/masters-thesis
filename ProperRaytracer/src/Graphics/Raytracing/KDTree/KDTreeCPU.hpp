/**
 * File name: KDTreeCPU.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "KDTreeNavigator.hpp"
#include "Graphics/Raytracing/CPUMeshManager.hpp"

namespace ProperRT
{
    class KDTCPUBuilderBase;
    class KDTCPUTraverser;

    /// @brief CPU implementation of a k-d tree.
    class KDTreeCPU : public IAccelerationStructure, public NavigatableBinaryTree
    {
    public:
        using Node = KDTNode;
        using InnerNode = KDTInnerNode;
        using LeafNode = KDTLeafNode;

        class NodeNavigator;

        /// @brief Tree navigator implementation.
        class TreeNavigator : public KDTreeNavigator
        {
        public:
            friend KDTreeCPU;
            using Base = KDTreeNavigator;

        public:
            explicit
            TreeNavigator(KDTreeCPU* tree_, bool selectedStatic_);

            virtual
            bool CanNavigateInto() override;

        protected:
            bool selectedStatic;

            virtual
            IMeshManager const& GetMeshManager() override;

            virtual
            std::unique_ptr<BinaryTreeNodeNavigator> CreateNodeNavigator() override;
        };

        /// @brief Node navigator implementation.
        class NodeNavigator : public KDTreeNodeNavigator
        {
        public:
            using Base = KDTreeNodeNavigator;
        public:
            explicit
            NodeNavigator(TreeNavigator* parentNavigator_);
        };

        /// @brief Navigator for choosing between navigating the static and dynamic trees.
        class ChooseTreeNavigator : public INavigator, public IStructureValidator
        {
        public:
            explicit
            ChooseTreeNavigator(KDTreeCPU* tree_);

            virtual
            void StartNavigating() override;

            virtual
            void Navigate() override;

            virtual
            void NavigateOut() override;

            virtual
            bool CanNavigateInto() override;

            virtual
            INavigator* NavigateInto() override;

            virtual
            void Render() override;

            virtual
            std::optional<std::string> Validate() override;

            bool ChoseStatic() const { return radioValue == 0; }

        protected:
            KDTreeCPU* tree;
            std::shared_ptr<OrientedBoundingBoxMesh> boundingBoxMesh;
            int radioValue{0};
        };
        
    public:
        /// @brief Tree parameters.
        KDTreeParams parameters;

        /// @brief Default ctor.
        KDTreeCPU();

        /// @brief Dtor.
        ~KDTreeCPU();

        virtual
        void BuildStatic(std::vector<CompPtr<Components::Renderer>>&& renderers) override;

        virtual
        void BuildDynamic(std::vector<CompPtr<Components::Renderer>>&& renderers) override;

        virtual
        bool IsBuilt() const override;

        virtual
        INavigator* TopNavigator() override;

        virtual
        IStructureValidator* Validator() override;

        virtual
        std::optional<RayHit> Traverse(Ray const& ray, cfloat maxDist = std::numeric_limits<cfloat>::infinity(), TraversalStatistics* traversalStats = nullptr) override;

        virtual
        bool TraverseShadow(Ray const& ray, cfloat maxDist, TraversalStatistics* traversalStats = nullptr) override;

        virtual
        AccStructStats const& GetStatistics() const override { return stats; }

        virtual
        AccStructStats& GetStatistics() override { return stats; }

        virtual
        std::unique_ptr<BinaryTreeNavigationNode> NavigationRoot();

        /// @brief Returns the static mesh manager.
        CPUMeshManager const* GetStaticMeshManager() const;

        /// @brief Returns the static mesh builder.
        KDTCPUBuilderBase const* GetStaticBuilder() const;

        /// @brief Returns the dynamic mesh manager.
        CPUMeshManager const* GetDynamicMeshManager() const;

        /// @brief Returns the dynamic mesh builder.
        KDTCPUBuilderBase const* GetDynamicBuilder() const;

    private:
        std::vector<CompPtr<Components::Renderer>> renderers{};
        int32 staticRendererCount{};

        std::unique_ptr<CPUMeshManager> staticMeshManager{};
        std::unique_ptr<KDTCPUBuilderBase> staticBuilder{};

        std::unique_ptr<CPUMeshManager> dynamicMeshManager{};
        std::unique_ptr<KDTCPUBuilderBase> dynamicBuilder{};

        ChooseTreeNavigator topNavigator;
        TreeNavigator staticNavigator;
        TreeNavigator dynamicNavigator;

        AccelerationStructureStatistics stats{};

        KDTCPUTraverser Traverser(CPUMeshManager const& meshManager, KDTCPUBuilderBase const& builder) const;
    };
}