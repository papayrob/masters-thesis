/**
 * File name: KDTreeValidatorCommon.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "KDTreeCommon.hpp"
#include "RTApp.hpp"
#include "Graphics/Raytracing/IMeshManager.hpp"

namespace ProperRT
{
    /// @brief Common function for validation of standard k-d trees.
    /// @tparam TTree Tree class type
    /// @tparam TBuilder Tree builder type
    /// @param tree Instance of the tree to validate
    /// @param root Root node
    /// @param meshManager Mesh manager the tree was built over
    /// @param builder Builder the tree was built with
    /// @param nodes Nodes of the tree
    /// @param[inout] depths Vector with depths of leaves
    /// @param[inout] primitiveCounts Vector with primitive counts of leaves
    template<class TTree, class TBuilder>
    std::optional<std::string> KDTreeValidateCommon(
        TTree& tree,
        typename TTree::Node const* root,
        IMeshManager const& meshManager,
        TBuilder const& builder,
        std::span<typename TTree::Node const> nodes,
        std::vector<int32>& depths,
        std::vector<int32>& primitiveCounts
    ) {
        using Node = typename TTree::Node;

        struct StackItem
        {
            Node const* node;
            AABB boundingBox;
            int32 depth;
            double multiplier;
            std::vector<Side> traversalChain;
        };

        auto const ToString = [] (std::vector<Side> const& v) -> std::string {
            std::stringstream ss{};
            if (v.size() > 0) {
                ss << '\n' << (v.front() == Side::Left ? 'L' : 'R');
            }
            for (auto it = v.begin() + 1; it != v.end(); ++it) {
                ss << ',' << (*it == Side::Left ? 'L' : 'R');
            }
            return ss.str();
        };

        auto& stats = tree.GetStatistics();
        stats.memoryConsumption += builder.AllocatedMemory();

        std::stack<StackItem> stack{};
        stack.push(StackItem{ root, meshManager.SceneBoundingBox(), 0, 1.0, {} });

        std::unordered_set<int32> seenNodeIdxs{};

        while (!stack.empty()) {
            StackItem item = std::move(stack.top());
            stack.pop();

            stats.cost += item.multiplier * tree.parameters.traversalCost;

            if (item.node->IsInner()) {
                auto const& node = item.node->inner;

                ++stats.innerNodes;

                if (seenNodeIdxs.contains(node.leftChildIdx) || seenNodeIdxs.contains(node.rightChildIdx)) {
                    return "Recursive child reference" + ToString(item.traversalChain);
                }

                seenNodeIdxs.insert(node.leftChildIdx);
                seenNodeIdxs.insert(node.rightChildIdx);

                AABB leftBB, rightBB;
                
                try
                {
                    item.boundingBox.SplitCoord(ToAxis3D(node.tag), node.splitValue, leftBB, rightBB);
                }
                catch(const std::invalid_argument& e) //TODO custom exception
                {
                    return e.what() + ToString(item.traversalChain);
                }

                item.traversalChain.push_back(Side::Left);
                stack.push(StackItem{ &nodes[node.leftChildIdx], leftBB, item.depth + 1, item.multiplier * (leftBB.SurfaceArea() / item.boundingBox.SurfaceArea()), item.traversalChain });
                item.traversalChain.back() = Side::Right;
                stack.push(StackItem{ &nodes[node.rightChildIdx], rightBB, item.depth + 1, item.multiplier * (rightBB.SurfaceArea() / item.boundingBox.SurfaceArea()), item.traversalChain });
            }
            else {
                auto const& leaf = item.node->leaf;

                ++stats.leafNodes;
                stats.maxDepth = std::max(item.depth, stats.maxDepth);
                depths.push_back(item.depth);

                try
                {
                    auto primitives = builder.LeafPrimitives(leaf);
                    int32 primitiveN = StdSize<int32>(primitives);
                    if (primitiveN == 0) {
                        ++stats.emptyNodes;
                    }
                    primitiveCounts.push_back(primitiveN);
                    stats.maxPrimitivesInLeaf = std::max(primitiveN, stats.maxPrimitivesInLeaf);

                    stats.cost += item.multiplier * primitiveN * tree.parameters.intersectionCost;

                    constexpr sfloat tolerance = 1.0e-3;
                    constexpr Vector3sf toleranceVec{tolerance};
                    item.boundingBox.ExpandProportional(tolerance);
                    item.boundingBox.ExpandBy(toleranceVec);

                    for (int32 primitiveIdx = 0, primitiveN = StdSize<int32>(primitives); primitiveIdx < primitiveN; ++primitiveIdx) {
                        Primitive primitive = meshManager.GetPrimitive(primitives[primitiveIdx]);
                        AABB primitiveBB = GetAABB(primitive);
                        if (!primitiveBB.Intersects(item.boundingBox)) {
                            return "Primitive " + std::to_string(primitiveIdx) + " is outside leaf bounding box" + ToString(item.traversalChain);
                        }
                    }
                }
                catch(const std::out_of_range& e)
                {
                    return e.what() + ToString(item.traversalChain);
                }
            }
        }

        return {};
    }
}
