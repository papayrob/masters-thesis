/**
 * File name: KDTCPUMergeBuilder.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <algorithm>

#include "KDTreeCPU.hpp"
#include "KDTCPUBuilderBase.hpp"

namespace ProperRT
{
    /// @brief Class for a k-d tree builder that merges a static tree with dynamic trees.
    class KDTCPUMergeBuilder : public IMeshManager
    {
    public:
        using Node = KDTreeCPU::Node;
        using InnerNode = KDTreeCPU::InnerNode;
        using LeafNode = KDTreeCPU::LeafNode;

    public:
        /// @brief Dtor.
        virtual
        ~KDTCPUMergeBuilder() {
            DestroyLeaves(0);
        }

        /// @brief Initializes the builder with an empty static tree.
        void InitStaticEmpty() {
            // Reset old
            DestroyLeaves(0);
            RestoreStaticTree();
            instances.clear();
            nodes.clear();
            trashedNodes = {};

            // Init with empty leaf
            sceneBoundingBox = staticTree.boundingBox = AABB::InfinitelySmall();
            treeRootIdx = staticTree.root = AddLeafNode(0, nullptr);
            staticPrimitiveN = primitiveN = 0;

            staticInstancesCount = 0;
            staticNodesCount = 1;
        }

        /// @brief Initializes the builder with a static tree.
        /// @param staticMeshManager Mesh manager the static tree was built over
        /// @param staticBuilder Builder the static tree was built with
        void InitStatic(CPUMeshManager const& staticMeshManager, KDTCPUBuilderBase& staticBuilder) {
            if (!staticBuilder.IsBuilt()) {
                //TODO throw
                assert(false);
            }

            // Reset old
            DestroyLeaves(0);
            RestoreStaticTree();
            instances.clear();
            nodes.clear();
            trashedNodes = {};

            // Init with the static tree
            sceneBoundingBox = staticTree.boundingBox = staticMeshManager.SceneBoundingBox();
            treeRootIdx = staticTree.root = CopyNodes(staticMeshManager, staticBuilder);
            staticPrimitiveN = primitiveN = staticMeshManager.PrimitiveCount();

            staticInstancesCount = StdSize<int32>(instances);
            staticNodesCount = StdSize<int32>(nodes);
        }

        /// @brief Resets to the initial state with only static tree.
        void ResetToStaticState() {
            DestroyLeaves(staticNodesCount);
            RestoreStaticTree();
            instances.resize(staticInstancesCount);
            nodes.resize(staticNodesCount);
            trashedNodes = {};
            sceneBoundingBox = staticTree.boundingBox;
            treeRootIdx = staticTree.root;
            primitiveN = staticPrimitiveN;
        }

        /// @brief Inserts a dynamic tree, after inserting all trees, call BuildLeaves.
        /// @param dynamicMeshManager Mesh manager the dynamic tree was built over
        /// @param dynamicBuilder Builder the dynamic tree was built with
        void InsertDynamic(CPUMeshManager const& dynamicMeshManager, KDTCPUBuilderBase& dynamicBuilder) {
            if (staticTree == nullptr || !dynamicBuilder.IsBuilt() || dynamicBuilder.Nodes().size() == 0) {
                //TODO throw
                assert(false);
            }

            // Skip empty trees
            if (dynamicBuilder.Root()->IsLeaf() && dynamicBuilder.Root()->leaf.primitiveN == 0) {
                return;
            }

            // Insert dynamic tree at static leaves
            PartitionTree dynamicTree = PartitionTree{CopyNodes(dynamicMeshManager, dynamicBuilder), dynamicMeshManager.SceneBoundingBox()};
            sceneBoundingBox.Include(dynamicTree.boundingBox);
            PartitionTreeWithTree(dynamicTree, staticTree, -1, Side::None);
            primitiveN += dynamicMeshManager.PrimitiveCount();
        }

        /// @brief Build sub-trees from static leaves with split dynamic trees. Must be called for the build process to finish.
        void BuildLeaves() {
            for (auto& [leafIdx, data] : treesAtLeaves) {
                AABB boundingBox{ data.leafBoundingBox };
                for (PartitionTree const& tree : data.trees) {
                    boundingBox.Include(tree.boundingBox);
                }

                // Add static tree leaf
                if (data.leafBoundingBox.IsValid() && nodes[leafIdx].leaf.primitiveN > 0) {
                    nodes[leafIdx].leaf.primitives = new PUID[nodes[leafIdx].leaf.primitiveN];
                    std::copy(data.backup.primitives, data.backup.primitives + data.backup.primitiveN, nodes[leafIdx].leaf.primitives);
                    data.trees.push_back(PartitionTree{leafIdx, data.leafBoundingBox});
                }

                ParentsChildReference(data.parent, data.parentSide) = PartitionTrees(std::move(data.trees), boundingBox, false);
            }
        }

        /// @brief Returns true if the tree is built.
        bool IsBuilt() const {
            return (treeRootIdx >= 0);
        }

        /// @brief Returns tree nodes.
        std::span<Node> Nodes() {
            return nodes;
        }

        /// @brief Returns tree nodes.
        std::vector<Node> const& Nodes() const {
            return nodes;
        }

        /// @brief Returns the index of the root node.
        int32 RootIdx() const {
            return treeRootIdx;
        }

        /// @brief Returns the primitives of @p leaf.
        /// @param leaf Leaf node
        std::span<PUID> LeafPrimitives(LeafNode const& leaf) {
            return std::span<PUID>{leaf.primitives, static_cast<std::size_t>(leaf.primitiveN)};
        }

        /// @brief Returns the primitives of @p leaf.
        /// @param leaf Leaf node
        std::span<PUID const> LeafPrimitives(LeafNode const& leaf) const {
            return std::span<PUID>{leaf.primitives, static_cast<std::size_t>(leaf.primitiveN)};
        }

        virtual
        Primitive GetPrimitive(PUID puid) const override {
            return instances.at(puid.instanceID).GetPrimitive(puid.primitiveOffset);
        }

        virtual
        CompPtr<Components::Renderer> GetRenderer(PUID puid) const override {
            return instances.at(puid.instanceID).renderer;
        }

        virtual
        int32 GetSubmeshIdx(PUID puid) const override {
            return instances.at(puid.instanceID).renderer->GetSubmeshInfo(instances.at(puid.instanceID).submeshIdxInRenderer).submeshIdx;
        }

        virtual
        int32 PrimitiveCount() const override {
            return primitiveN;
        }

        virtual
        int64 AllocatedMemory() const override {
            std::size_t memory{0};
            for (auto const& [leafIdx, data] : treesAtLeaves) {
                memory += sizeof(LeafMergeTrees) + std::span{data.trees}.size_bytes() + data.backup.primitiveN * sizeof(PUID);
            }
            for (KDTNode const& node : nodes) {
                if (node.IsLeaf()) {
                    memory += node.leaf.primitiveN * sizeof(PUID);
                }
            }
            return static_cast<int64>(std::span{instances}.size_bytes() + std::span{nodes}.size_bytes() + memory);
        }

        virtual
        AABB const& SceneBoundingBox() const override {
            return sceneBoundingBox;
        }

        /// @brief Returns the mesh manager instances.
        std::span<CPUMeshManager::MeshInstance> Instances() {
            return instances;
        }

    private:
        struct Event;
        
        struct Partition
        {
            int32 minus;
            int32 plus;
        };

        struct PartitionTree
        {
            int32 root{-1};
            AABB boundingBox;

            bool operator==(std::nullptr_t) {
                return root < 0;
            }
        };

        struct Event
        {
            enum class Type : int8 { Start = 0, End };

            Type type;
            float coordinate;

            bool operator==(Event const&) const noexcept = default;

            std::partial_ordering operator<=>(Event const& other) const noexcept {
                using T = std::underlying_type_t<Type>;
                auto left = ToUnderlying(type);
                auto right = ToUnderlying(other.type);
                return std::tie(coordinate, left) <=> std::tie(other.coordinate, right);
            }
        };

        using EventLists = std::array<std::vector<Event>, 3>;

        struct BestSplit
        {
            SplitPlane split{ Axis3D::None, 0.f };
            int32 minCost{ std::numeric_limits<int32>::max() };
            bool clean{true};
        };

        struct LeafMergeTrees
        {
            AABB leafBoundingBox;
            int32 parent;
            Side parentSide;
            LeafNode backup;
            std::vector<PartitionTree> trees;
        };

        std::vector<CPUMeshManager::MeshInstance> instances{};
        std::vector<Node> nodes{};
        std::stack<int32> trashedNodes{};

        AABB sceneBoundingBox{};
        int32 primitiveN{0};
        int32 treeRootIdx{-1};
        
        PartitionTree staticTree{};
        std::unordered_map<int32, LeafMergeTrees> treesAtLeaves{};
        int32 staticInstancesCount{0};
        int32 staticNodesCount{0};
        int32 staticPrimitiveN{0};

        int32 CopyNodes(CPUMeshManager const& meshManager, KDTCPUBuilderBase& builder) {
            // Copy over instances
            int32 instanceOffset = StdSize<int32>(instances);
            instances.insert(instances.end(), meshManager.Instances().begin(), meshManager.Instances().end());

            // Copy over nodes
            int32 nodeOffset = StdSize<int32>(nodes);
            std::span<KDTNode> treeNodes = builder.Nodes();
            int treeNodeN = StdSize<int>(treeNodes);
            for (int treeNodeI = 0; treeNodeI < treeNodeN; ++treeNodeI) {
                nodes.push_back(treeNodes[treeNodeI]);

                // Offset PUID
                if (nodes.back().IsLeaf()) {
                    LeafNode const& leaf = nodes.back().leaf;
                    for (int32 primitiveI = 0; primitiveI < leaf.primitiveN; ++primitiveI) {
                        leaf.primitives[primitiveI].instanceID += instanceOffset;
                    }

                    // Prevent old builder from deleting primitives array
                    treeNodes[treeNodeI].leaf.primitives = nullptr;
                }
                // Offset child indices
                else {
                    InnerNode& inner = nodes.back().inner;
                    inner.leftChildIdx += nodeOffset;
                    inner.rightChildIdx += nodeOffset;
                }
            }

            return builder.RootIdx() + nodeOffset;
        }

        int32 AddEmptyNode() {
            int32 index;
            if (!trashedNodes.empty()) {
                index = trashedNodes.top();
                trashedNodes.pop();
            }
            else {
                index = StdSize<int32>(nodes);
                nodes.push_back(Node{});
            }
            return index;
        }

        int32 AddLeafNode(int32 primitiveN, PUID* primitives) {
            int32 index = AddEmptyNode();
            nodes[index].leaf = LeafNode{KDTNodeType::Leaf, primitiveN, primitives};
            return index;
        }

        int32 AddInnerNode(Axis3D axis, float splitValue, int32 left, int32 right) {
            int32 index = AddEmptyNode();
            nodes[index].inner = InnerNode{ToNodeType(axis), splitValue, left, right};
            return index;
        }

        Primitive GetPrimitive(PUID const& puid) {
            return instances[puid.instanceID].GetPrimitive(puid.primitiveOffset);
        }

        void PartitionPrimitives(int32 nodeIdx, PUID* primitives, int32 primitiveN, LeafNode& leftLeaf, LeafNode& rightLeaf, SplitPlane const& plane) {
            // Zero primitves, both leaves will be empty
            if (primitiveN <= 0) {
                leftLeaf.primitives = nullptr;
                rightLeaf.primitives = nullptr;
                leftLeaf.primitiveN = 0;
                rightLeaf.primitiveN = 0;
                return;
            }

            // Count number of primitives that will go into the left and right leaves
            leftLeaf.primitiveN = 0;
            rightLeaf.primitiveN = 0;

            for (int32 i = 0; i < primitiveN; ++i) {
                AABB bb = GetAABB(GetPrimitive(primitives[i]));
                if (bb.maxBounds[ToUnderlying(plane.axis)] <= plane.coordinate) {
                    ++leftLeaf.primitiveN;
                }
                else if (plane.coordinate <= bb.minBounds[ToUnderlying(plane.axis)]) {
                    ++rightLeaf.primitiveN;
                }
                else {
                    ++leftLeaf.primitiveN;
                    ++rightLeaf.primitiveN;
                }
            }

            // Distribute primitives
            if (leftLeaf.primitiveN > 0 && rightLeaf.primitiveN > 0) {
                leftLeaf.primitives = new PUID[leftLeaf.primitiveN];
                rightLeaf.primitives = new PUID[rightLeaf.primitiveN];
                leftLeaf.primitiveN = 0;
                rightLeaf.primitiveN = 0;

                for (int32 i = 0; i < primitiveN; ++i) {
                    AABB bb = GetAABB(GetPrimitive(primitives[i]));
                    if (bb.maxBounds[ToUnderlying(plane.axis)] <= plane.coordinate) {
                        leftLeaf.primitives[leftLeaf.primitiveN++] = primitives[i];
                    }
                    else if (plane.coordinate <= bb.minBounds[ToUnderlying(plane.axis)]) {
                        rightLeaf.primitives[rightLeaf.primitiveN++] = primitives[i];
                    }
                    else {
                        leftLeaf.primitives[leftLeaf.primitiveN++] = primitives[i];
                        rightLeaf.primitives[rightLeaf.primitiveN++] = primitives[i];
                    }
                }

                delete[] primitives;
            }
            // Right leaf is empty, give all to left
            else if (leftLeaf.primitiveN > 0) {
                assert(leftLeaf.primitiveN == primitiveN);
                leftLeaf.primitives = primitives;
                rightLeaf.primitives = nullptr;
            }
            // Left leaf is empty, give all to right
            else {
                assert(rightLeaf.primitiveN == primitiveN);
                leftLeaf.primitives = nullptr;
                rightLeaf.primitives = primitives;
            }
        }

        Partition PartitionTreeWithPlane(int32 root, SplitPlane const& plane) {
            Partition partition;
            // Leaf node, split primitives
            if (nodes[root].IsLeaf()) {
                partition.minus = root;
                partition.plus = AddLeafNode(0, nullptr);
                PartitionPrimitives(root, nodes[root].leaf.primitives, nodes[root].leaf.primitiveN, nodes[partition.minus].leaf, nodes[partition.plus].leaf, plane);
                return partition;
            }

            // Inner node
            InnerNode innerNode = nodes[root].inner;
            if (ToAxis3D(innerNode.tag) != plane.axis) {
                // Create new nodes with partitions as chlidren
                Partition leftPartition = PartitionTreeWithPlane(innerNode.leftChildIdx, plane);
                Partition rightPartition = PartitionTreeWithPlane(innerNode.rightChildIdx, plane);

                // Equal to partition.minus = MakeInnerNode(ToAxis3D(innerNode.tag), innerNode.splitValue, leftPartition.minus, rightPartition.minus)
                partition.minus = root;
                nodes[partition.minus].inner.leftChildIdx = leftPartition.minus;
                nodes[partition.minus].inner.rightChildIdx = rightPartition.minus;

                partition.plus = AddInnerNode(ToAxis3D(innerNode.tag), innerNode.splitValue, leftPartition.plus, rightPartition.plus);
            }
            else {
                if (plane.coordinate < innerNode.splitValue) {
                    Partition childPartition = PartitionTreeWithPlane(innerNode.leftChildIdx, plane);
                    // Left partition is left child partition
                    partition.minus = childPartition.minus;

                    // Move root to right partition
                    partition.plus = root;
                    // Right child is the same, set new left child
                    nodes[partition.plus].inner.leftChildIdx = childPartition.plus;
                }
                else if (innerNode.splitValue < plane.coordinate) {
                    Partition childPartition = PartitionTreeWithPlane(innerNode.rightChildIdx, plane);
                    // Move root to left partition
                    partition.minus = root;
                    // Left child is the same, set new right child
                    // same variable: nodes[partition.minus].inner.leftChildIdx = innerNode.leftChildIdx;
                    nodes[partition.minus].inner.rightChildIdx = childPartition.minus;

                    // Right partition is right child partition
                    partition.plus = childPartition.plus;
                }
                else {
                    // No need to partition
                    partition.minus = innerNode.leftChildIdx;
                    partition.plus = innerNode.rightChildIdx;
                    trashedNodes.push(root);
                }
            }

            return partition;
        }

        int32 PartitionTreeSplitCost(int32 left, int32 right, int32 overlapping) {
            return (2 * overlapping * (1 + std::abs(left - right)) + overlapping);
        }

        BestSplit FindBestPlane(EventLists const& events, AABB const& boundingBox) {
            BestSplit bestSplit{};

            for (int axis = 0; axis < 3; ++axis) {
                int32 leftTreeN{ 0 }, rightTreeN{ StdSize<int32>(events[axis]) / 2 }, overlappingN{ 0 };

                auto eventEnd = events[axis].end();
                for (auto eventIt = events[axis].begin(); eventIt != eventEnd;) {
                    auto currentCoordinate = eventIt->coordinate;

                    // Count number of starting, ending and planar events at current coordinate
                    int32 ending{ 0 }, starting{ 0 };
                    while (eventIt != eventEnd && eventIt->coordinate == currentCoordinate) {
                        switch (eventIt->type)
                        {
                        case Event::Type::Start: {
                            ++starting;
                            break;
                        }
                        case Event::Type::End: {
                            ++ending;
                            break;
                        }
                        default:
                            break;
                        }

                        ++eventIt;
                    }

                    // Remove ending primitives from right
                    rightTreeN -= ending;
                    overlappingN -= ending;

                    // Update cost (don't count box edges)
                    assert(overlappingN >= 0);
                    assert(rightTreeN >= 0 && leftTreeN >= 0);
                    if (currentCoordinate != boundingBox.minBounds[axis] && currentCoordinate != boundingBox.maxBounds[axis]) {
                        int32 cost = PartitionTreeSplitCost(leftTreeN, rightTreeN, overlappingN);

                        if (cost < bestSplit.minCost) {
                            bestSplit.split = SplitPlane{ static_cast<Axis3D>(axis), currentCoordinate };
                            bestSplit.minCost = cost;
                            bestSplit.clean = (overlappingN == 0);
                        }
                    }

                    // Add starting primitives to left
                    leftTreeN += starting;
                    overlappingN += starting;
                }
            }

            return bestSplit;
        }

        bool Dissolve(std::vector<PartitionTree>& trees) {
            bool allLeaves = true;
            for (int i = 0, size = StdSize<int>(trees); i < size; ++i) {
                if (nodes[trees[i].root].IsInner()) {
                    auto const& inner = nodes[trees[i].root].inner;
                    trashedNodes.push(trees[i].root);
                    
                    trees.push_back(PartitionTree{inner.rightChildIdx, AABB{}});
                    trees[i].boundingBox.SplitCoord(ToAxis3D(inner.tag), inner.splitValue, trees[i].boundingBox, trees.back().boundingBox);
                    trees[i].root = inner.leftChildIdx;

                    allLeaves = false;
                }
            }

            return allLeaves;
        }

        int32 MakeTreeFromLeaves(std::vector<PartitionTree> trees) {
            // Merge leaves into single leaf
            int32 primitiveN = std::transform_reduce(trees.begin(), trees.end(), int32{0}, std::plus<int32>{}, [this] (PartitionTree const& tree) {
                return nodes[tree.root].leaf.primitiveN;
            });

            PUID* primitives = new PUID[primitiveN];
            
            primitiveN = 0;
            for (auto const& tree : trees) {
                LeafNode& leaf = nodes[tree.root].leaf;
                if (leaf.primitives != nullptr) {
                    assert(leaf.primitiveN > 0);
                    std::copy(leaf.primitives, leaf.primitives + leaf.primitiveN, primitives + primitiveN);
                    primitiveN += leaf.primitiveN;

                    delete[] leaf.primitives;
                    leaf.primitiveN = 0;
                    leaf.primitives = nullptr;
                }

                trashedNodes.push(tree.root);
            }

            return AddLeafNode(primitiveN, primitives);
        }

        int32 PartitionTrees(std::vector<PartitionTree> trees, AABB boundingBox, bool comingFromDissolve) {
            // Return empty node
            if (trees.size() == 0) {
                return AddLeafNode(0, nullptr);
            }

            // Return the tree
            if (trees.size() == 1 && boundingBox.IsTightlyBounding(trees[0].boundingBox)) {
                return trees[0].root;
            }

            EventLists events{};
            for (PartitionTree const& tree : trees) {
                for (int axis = 0; axis < 3; ++axis) {
                    events[axis].push_back(Event{Event::Type::Start, tree.boundingBox.minBounds[axis]});
                    events[axis].push_back(Event{Event::Type::End, tree.boundingBox.maxBounds[axis]});
                }
            }

            for (int axis = 0; axis < 3; ++axis) {
                std::sort(events[axis].begin(), events[axis].end());
            }

            auto bestSplit = FindBestPlane(events, boundingBox);

            // Try dissolving when the found cut cuts trees, or when all trees are touching the bounding box
            if ((!bestSplit.clean && !comingFromDissolve) || bestSplit.split.axis == Axis3D::None) {
                bool allLeaves = Dissolve(trees);
                if (allLeaves) {
                    return MakeTreeFromLeaves(trees);
                }
                else {
                    return PartitionTrees(trees, boundingBox, true);
                }
            }

            // Split trees
            std::vector<PartitionTree> leftTrees{}, rightTrees{};
            for (auto& tree : trees) {
                if (tree.boundingBox.maxBounds[ToUnderlying(bestSplit.split.axis)] <= bestSplit.split.coordinate) {
                    leftTrees.push_back(tree);
                }
                else if (bestSplit.split.coordinate <= tree.boundingBox.minBounds[ToUnderlying(bestSplit.split.axis)]) {
                    rightTrees.push_back(tree);
                }
                else {
                    Partition partition = PartitionTreeWithPlane(tree.root, bestSplit.split);
                    leftTrees.push_back(PartitionTree{ .root = partition.minus });
                    rightTrees.push_back(PartitionTree{ .root = partition.plus });
                    tree.boundingBox.SplitCoord(bestSplit.split.axis, bestSplit.split.coordinate, leftTrees.back().boundingBox, rightTrees.back().boundingBox);
                }
            }

            // Partition left and right subtree
            AABB leftBB, rightBB;
            boundingBox.SplitCoord(bestSplit.split.axis, bestSplit.split.coordinate, leftBB, rightBB);
            int32 left = PartitionTrees(leftTrees, leftBB, false);
            int32 right = PartitionTrees(rightTrees, rightBB, false);
            return AddInnerNode(bestSplit.split.axis, bestSplit.split.coordinate, left, right);
        }

        void PartitionTreeWithTree(PartitionTree dynamicSplitTree, PartitionTree staticSubTree, int32 staticSubTreeParent, Side splitSide) {
            if (nodes[staticSubTree.root].IsInner()) {
                InnerNode inner = nodes[staticSubTree.root].inner;
                SplitPlane split = SplitPlane{ToAxis3D(inner.tag), inner.splitValue};
                staticSubTreeParent = staticSubTree.root;

                // Merge into left child
                if (dynamicSplitTree.boundingBox.maxBounds[ToUnderlying(split.axis)] <= split.coordinate) {
                    staticSubTree.root = inner.leftChildIdx;
                    staticSubTree.boundingBox.maxBounds[ToUnderlying(split.axis)] = split.coordinate;
                    PartitionTreeWithTree(dynamicSplitTree, staticSubTree, staticSubTreeParent, Side::Left);
                }
                // Merge into right child
                else if (split.coordinate <= dynamicSplitTree.boundingBox.minBounds[ToUnderlying(split.axis)]) {
                    staticSubTree.root = inner.rightChildIdx;
                    staticSubTree.boundingBox.minBounds[ToUnderlying(split.axis)] = split.coordinate;
                    PartitionTreeWithTree(dynamicSplitTree, staticSubTree, staticSubTreeParent, Side::Right);
                }
                // Split and then merge into both children
                else {
                    Partition partition = PartitionTreeWithPlane(dynamicSplitTree.root, split);

                    dynamicSplitTree.root = partition.minus;
                    PartitionTree rightDynamic{ .root{partition.plus} };

                    staticSubTree.root = inner.leftChildIdx;
                    PartitionTree rightStatic{ .root{inner.rightChildIdx} };

                    dynamicSplitTree.boundingBox.SplitCoord(split.axis, split.coordinate, dynamicSplitTree.boundingBox, rightDynamic.boundingBox);
                    staticSubTree.boundingBox.SplitCoord(split.axis, split.coordinate, staticSubTree.boundingBox, rightStatic.boundingBox);

                    PartitionTreeWithTree(dynamicSplitTree, staticSubTree, staticSubTreeParent, Side::Left);
                    PartitionTreeWithTree(rightDynamic, rightStatic, staticSubTreeParent, Side::Right);
                }
            }
            else {
                if (treesAtLeaves.contains(staticSubTree.root)) {
                    treesAtLeaves[staticSubTree.root].trees.push_back(dynamicSplitTree);
                }
                else {
                    treesAtLeaves.insert({staticSubTree.root, LeafMergeTrees{staticSubTree.boundingBox, staticSubTreeParent, splitSide, nodes[staticSubTree.root].leaf, {dynamicSplitTree}}});
                }
            }
        }

        int32& ParentsChildReference(int32 parentIdx, Side parentSide) {
            if (parentIdx < 0) {
                return treeRootIdx;
            }

            if (parentSide == Side::Left) {
                return nodes[parentIdx].inner.leftChildIdx;
            }
            else {
                return nodes[parentIdx].inner.rightChildIdx;
            }
        }

        void RestoreStaticTree() {
            for (auto& [leafIdx, data] : treesAtLeaves) {
                ParentsChildReference(data.parent, data.parentSide) = leafIdx;
                if (nodes[leafIdx].IsLeaf()) {
                    delete[] nodes[leafIdx].leaf.primitives;
                }
                nodes[leafIdx].leaf = data.backup;
            }

            treesAtLeaves.clear();
        }

        void DestroyLeaves(int32 staticNodesCount) {
            for (int32 nodeIdx = staticNodesCount, nodeN = StdSize<int32>(nodes); nodeIdx < nodeN; ++nodeIdx) {
                if (nodes[nodeIdx].IsLeaf()) {
                    LeafNode& leaf = nodes[nodeIdx].leaf;
                    delete[] leaf.primitives;
                    leaf.primitives = nullptr;
                    leaf.primitiveN = 0;
                }
            }
        }
    };
}
