/**
 * File name: KDTreeCPU.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "KDMergeTreeCPU.hpp"

#include "KDTEventsCPUBuilder.hpp"
#include "KDTBinsCPUBuilder.hpp"
#include "KDTCPUTraverser.hpp"
#include "KDTreeNavigationNodeCommon.hpp"
#include "KDTCPUMergeBuilder.hpp"
#include "RTApp.hpp"
#include "KDTreeValidatorCommon.hpp"

namespace ProperRT
{
    namespace
    {
        TraversalStatistics traversalStatsDummy;
    }

    KDMergeTreeCPU::KDMergeTreeCPU()
    {
        auto const& sceneConfig = Scene::GetActiveScene()->Config();
        bool success = false;
        if (std::string value; (success = sceneConfig.TryGetValue("staticBuilder", value))) {
            if (value == "Events") {
                builderFactory.Register<KDTEventsCPUBuilder>(staticKey);
            }
            else if (value == "Bins") {
                builderFactory.Register<KDTBinsCPUBuilder>(staticKey);
            }
            else {
                success = false;
            }
        }

        if (!success) {
            //TODO own exception (ConfigException)
            throw std::runtime_error("Cannot run merging tree without a static tree builder");
        }

        if (std::string value; (success = sceneConfig.TryGetValue("dynamicBuilder", value))) {
            if (value == "Events") {
                builderFactory.Register<KDTEventsCPUBuilder>(dynamicKey);
            }
            else if (value == "Bins") {
                builderFactory.Register<KDTBinsCPUBuilder>(dynamicKey);
            }
            else {
                success = false;
            }
        }

        if (!success) {
            //TODO own exception (ConfigException)
            throw std::runtime_error("Cannot run without a dynamic tree builder");
        }

        mergeBuilder = std::make_unique<KDTCPUMergeBuilder>();
    }

    KDMergeTreeCPU::~KDMergeTreeCPU() = default;

    void KDMergeTreeCPU::BuildStatic(std::vector<CompPtr<Components::Renderer>>&& renderers_) {
        CPUMeshManager staticMeshManager{};
        std::unique_ptr<KDTCPUBuilderBase> staticBuilder{ builderFactory.Create(staticKey) };

        auto&& [primitives, primitiveBBs] = staticMeshManager.CreateInstances(renderers_);
        RTApp::Instance()->statistics.AddInfo("StaticPrimitiveCount", StdSize<int64>(primitives));

        if (primitives.size() > 0) {
            staticBuilder->Build(staticMeshManager, std::move(primitives), std::move(primitiveBBs), parameters);
            mergeBuilder->InitStatic(staticMeshManager, *staticBuilder);
        }
        else {
            mergeBuilder->InitStaticEmpty();
        }
    }

    void KDMergeTreeCPU::BuildDynamic(std::vector<CompPtr<Components::Renderer>>&& renderers_) {
        CPUMeshManager dynamicMeshManager{};
        std::unique_ptr<KDTCPUBuilderBase> dynamicBuilder{builderFactory.Create(dynamicKey)};

        mergeBuilder->ResetToStaticState();

        int64 dynamicPrimitiveCount = 0;
        for (int32 i = 0, size = StdSize<int32>(renderers_); i < size; ++i) {
            auto&& [primitives, primitiveBBs] = dynamicMeshManager.CreateInstances(std::span{renderers_}.subspan(i, 1));
            dynamicPrimitiveCount += StdSize<int64>(primitives);
            dynamicBuilder->Build(dynamicMeshManager, std::move(primitives), std::move(primitiveBBs), parameters);

            mergeBuilder->InsertDynamic(dynamicMeshManager, *dynamicBuilder);
        }

        RTApp::Instance()->statistics.AddInfo("DynamicPrimitiveCount", dynamicPrimitiveCount);

        mergeBuilder->BuildLeaves();
    }

    bool KDMergeTreeCPU::IsBuilt() const {
        return mergeBuilder->IsBuilt();
    }

    INavigator* KDMergeTreeCPU::TopNavigator() {
        return nullptr;
    }

    IStructureValidator* KDMergeTreeCPU::Validator() {
        if (mergeBuilder->IsBuilt()) {
            return this;
        }
        return nullptr;
    }

    std::optional<RayHit> KDMergeTreeCPU::Traverse(Ray const& ray, cfloat maxDist, TraversalStatistics* traversalStats) {
        if (traversalStats == nullptr) {
            traversalStats = &traversalStatsDummy;
        }

        ++(traversalStats->queryCount);
        return KDTCPUTraverser{ *mergeBuilder, mergeBuilder->Nodes(), mergeBuilder->RootIdx(), 32 }.Traverse(ray, maxDist, *traversalStats);
    }
    
    bool KDMergeTreeCPU::TraverseShadow(Ray const& ray, cfloat maxDist, TraversalStatistics* traversalStats) {
        if (traversalStats == nullptr) {
            traversalStats = &traversalStatsDummy;
        }

        ++(traversalStats->queryCount);
        return KDTCPUTraverser{ *mergeBuilder, mergeBuilder->Nodes(), mergeBuilder->RootIdx(), 32 }.TraverseShadow(ray, maxDist, *traversalStats);
    }

    std::optional<std::string> KDMergeTreeCPU::Validate() {
        stats.ResetBuildStats();
        
        std::vector<int32> depths{};
        std::vector<int32> primitiveCounts{};

        std::optional<std::string> ret;
        ret = KDTreeValidateCommon(*this, &mergeBuilder->Nodes()[mergeBuilder->RootIdx()], *mergeBuilder, *mergeBuilder, mergeBuilder->Nodes(), depths, primitiveCounts);

        stats.avgPrimitivesInLeaf = static_cast<float>(std::reduce(primitiveCounts.begin(), primitiveCounts.end())) / static_cast<float>(primitiveCounts.size());
        stats.avgDepth = static_cast<float>(std::reduce(depths.begin(), depths.end())) / static_cast<float>(depths.size());
        
        auto& appStats = RTApp::Instance()->statistics;
        appStats.AddData("MemoryConsumption", stats.memoryConsumption);
        appStats.AddData("Cost", stats.cost);
        appStats.AddData("InnerNodes", stats.innerNodes);
        appStats.AddData("LeafNodes", stats.leafNodes);
        appStats.AddData("EmptyNodes", stats.emptyNodes);
        appStats.AddData("AveragePrimitivesInLeaf", stats.avgPrimitivesInLeaf);
        appStats.AddData("MaxPrimitivesInLeaf", stats.maxPrimitivesInLeaf);
        appStats.AddData("AverageDepth", stats.avgDepth);
        appStats.AddData("MaxDepth", stats.maxDepth);

        return ret;
    }
}
