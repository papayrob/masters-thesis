/**
 * File name: KDTreeCPU.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "KDMergeTreeGPU.hpp"

#include "KDTEventsCPUBuilder.hpp"
#include "KDTBinsCPUBuilder.hpp"
#include "KDTCPUTraverser.hpp"
#include "KDTreeNavigationNodeCommon.hpp"
#include "KDTCPUMergeBuilder.hpp"
#include "RTApp.hpp"
#include "KDTreeValidatorCommon.hpp"
#include "Graphics/Raytracing/GPUMeshManager.cuh"
#include "KDTGPUTraverser.cuh"
#include "KDTGPUBuilderBase.cuh"

namespace ProperRT
{
    namespace
    {
        TraversalStatistics traversalStatsDummy;

        class GPUBuilder
        {
        public:
            DeviceUniquePointer<KDTNode[]> nodes{};
            DeviceUniquePointer<PUID[]> leafReferences{};
            bool isBuilt{false};

            void Build(GPUMeshManager& meshManager, KDTCPUMergeBuilder& mergeBuilder) {
                Reset();

                // Count primitive references
                std::vector<KDTNode> nodesCPU = static_cast<KDTCPUMergeBuilder const&>(mergeBuilder).Nodes();
                int32 leafReferencesSize = 0;
                for (KDTNode const& nodeCPU : nodesCPU) {
                    if (nodeCPU.IsLeaf()) {
                        leafReferencesSize += nodeCPU.leaf.primitiveN;
                    }
                }

                // Allocate room for references
                leafReferences = DeviceUniquePointer<PUID[]>::Allocate(leafReferencesSize);
                // Copy references to GPU and change leaf pointers
                PUID* pointer = leafReferences.Get().data;
                for (KDTNode& nodeCPU : nodesCPU) {
                    if (nodeCPU.IsLeaf() && nodeCPU.leaf.primitiveN > 0) {
                        KDTLeafNode& leaf = nodeCPU.leaf;
                        DevicePointer<PUID[]>{pointer, leaf.primitiveN}.Upload(std::span<PUID>(leaf.primitives, static_cast<std::size_t>(leaf.primitiveN)));
                        leaf.primitives = pointer;
                        pointer += nodeCPU.leaf.primitiveN;
                    }
                }

                assert(pointer == leafReferences.Get().data + leafReferences.Count());

                // Copy nodes with changed leaf pointers to GPU
                nodes = DeviceUniquePointer<KDTNode[]>::AllocateAndUpload(nodesCPU);

                isBuilt = true;
            }

            virtual
            bool IsBuilt() const { return isBuilt; }

            virtual
            int64 AllocatedMemory() const {
                return nodes.Bytes() + leafReferences.Bytes();
            }

            virtual
            void Reset() {
                nodes = nullptr;
                leafReferences = nullptr;
                isBuilt = false;
            }
        };
    }

    struct KDMergeTreeGPU::ComputeData
    {
        UniqueMemoryPool memoryPool;
        GPUBuilder builder{};

        ComputeData(MemoryPool::SizeType memPoolCapacity)
            : memoryPool{memPoolCapacity}
        {}
    };

    KDMergeTreeGPU::KDMergeTreeGPU()
        : computeData{std::make_unique<ComputeData>(
            ToBytesPrefixAs<MemoryPool::SizeType>(PrefixedValue{MetricPrefix::Giga, Scene::GetActiveScene()->Config().GetValue<double>("dataMemoryPoolCapacity", 4.)}, MetricPrefix::None)
          )},
          gpuMeshManager{std::make_unique<GPUMeshManager>()}
    {
        auto const& sceneConfig = Scene::GetActiveScene()->Config();
        bool success = false;
        if (std::string value; (success = sceneConfig.TryGetValue("staticBuilder", value))) {
            if (value == "Events") {
                builderFactory.Register<KDTEventsCPUBuilder>(staticKey);
            }
            else if (value == "Bins") {
                builderFactory.Register<KDTBinsCPUBuilder>(staticKey);
            }
            else {
                success = false;
            }
        }

        if (!success) {
            //TODO own exception (ConfigException)
            throw std::runtime_error("Cannot run merging tree without a static tree builder");
        }

        if (std::string value; (success = sceneConfig.TryGetValue("dynamicBuilder", value))) {
            if (value == "Events") {
                builderFactory.Register<KDTEventsCPUBuilder>(dynamicKey);
            }
            else if (value == "Bins") {
                builderFactory.Register<KDTBinsCPUBuilder>(dynamicKey);
            }
            else {
                success = false;
            }
        }

        if (!success) {
            //TODO own exception (ConfigException)
            throw std::runtime_error("Cannot run without a dynamic tree builder");
        }

        mergeBuilder = std::make_unique<KDTCPUMergeBuilder>();
    }

    KDMergeTreeGPU::~KDMergeTreeGPU() = default;

    void KDMergeTreeGPU::BuildStatic(std::vector<CompPtr<Components::Renderer>>&& renderers_) {
        CPUMeshManager staticMeshManager{};
        std::unique_ptr<KDTCPUBuilderBase> staticBuilder{ builderFactory.Create(staticKey) };

        auto&& [primitives, primitiveBBs] = staticMeshManager.CreateInstances(renderers_);
        RTApp::Instance()->statistics.AddInfo("StaticPrimitiveCount", StdSize<int64>(primitives));

        if (primitives.size() > 0) {
            staticBuilder->Build(staticMeshManager, std::move(primitives), std::move(primitiveBBs), parameters);
            mergeBuilder->InitStatic(staticMeshManager, *staticBuilder);
        }
        else {
            mergeBuilder->InitStaticEmpty();
        }
    }

    void KDMergeTreeGPU::BuildDynamic(std::vector<CompPtr<Components::Renderer>>&& renderers_) {
        // Build trees
        CPUMeshManager dynamicMeshManager{};
        std::unique_ptr<KDTCPUBuilderBase> dynamicBuilder{builderFactory.Create(dynamicKey)};

        mergeBuilder->ResetToStaticState();

        int64 dynamicPrimitiveCount = 0;
        for (int32 i = 0, size = StdSize<int32>(renderers_); i < size; ++i) {
            auto&& [primitives, primitiveBBs] = dynamicMeshManager.CreateInstances(std::span{renderers_}.subspan(i, 1));
            dynamicPrimitiveCount += StdSize<int64>(primitives);
            dynamicBuilder->Build(dynamicMeshManager, std::move(primitives), std::move(primitiveBBs), parameters);

            mergeBuilder->InsertDynamic(dynamicMeshManager, *dynamicBuilder);
        }

        RTApp::Instance()->statistics.AddInfo("DynamicPrimitiveCount", dynamicPrimitiveCount);

        // Merge
        mergeBuilder->BuildLeaves();

        // Move to GPU
        gpuMeshManager->FromCPUMeshInstances(*mergeBuilder, mergeBuilder->Instances());

        computeData->builder.Build(*gpuMeshManager, *mergeBuilder);
    }

    bool KDMergeTreeGPU::IsBuilt() const {
        return mergeBuilder->IsBuilt();
    }

    INavigator* KDMergeTreeGPU::TopNavigator() {
        return nullptr;
    }

    IStructureValidator* KDMergeTreeGPU::Validator() {
        if (mergeBuilder->IsBuilt()) {
            return this;
        }
        return nullptr;
    }

    std::optional<RayHit> KDMergeTreeGPU::Traverse(Ray const& ray, cfloat maxDist, TraversalStatistics* traversalStats) {
        if (traversalStats == nullptr) {
            traversalStats = &traversalStatsDummy;
        }

        ++(traversalStats->queryCount);
        return KDTCPUTraverser{ *mergeBuilder, mergeBuilder->Nodes(), mergeBuilder->RootIdx(), 32 }.Traverse(ray, maxDist, *traversalStats);
    }
    
    bool KDMergeTreeGPU::TraverseShadow(Ray const& ray, cfloat maxDist, TraversalStatistics* traversalStats) {
        if (traversalStats == nullptr) {
            traversalStats = &traversalStatsDummy;
        }

        ++(traversalStats->queryCount);
        return KDTCPUTraverser{ *mergeBuilder, mergeBuilder->Nodes(), mergeBuilder->RootIdx(), 32 }.TraverseShadow(ray, maxDist, *traversalStats);
    }

    std::optional<std::string> KDMergeTreeGPU::Validate() {
        stats.ResetBuildStats();
        
        std::vector<int32> depths{};
        std::vector<int32> primitiveCounts{};

        std::optional<std::string> ret;
        ret = KDTreeValidateCommon(*this, &mergeBuilder->Nodes()[mergeBuilder->RootIdx()], *mergeBuilder, *mergeBuilder, mergeBuilder->Nodes(), depths, primitiveCounts);

        stats.memoryConsumption += computeData->builder.AllocatedMemory();
        stats.avgPrimitivesInLeaf = static_cast<float>(std::reduce(primitiveCounts.begin(), primitiveCounts.end())) / static_cast<float>(primitiveCounts.size());
        stats.avgDepth = static_cast<float>(std::reduce(depths.begin(), depths.end())) / static_cast<float>(depths.size());
        
        auto& appStats = RTApp::Instance()->statistics;
        appStats.AddData("MemoryConsumption", stats.memoryConsumption);
        appStats.AddData("Cost", stats.cost);
        appStats.AddData("InnerNodes", stats.innerNodes);
        appStats.AddData("LeafNodes", stats.leafNodes);
        appStats.AddData("EmptyNodes", stats.emptyNodes);
        appStats.AddData("AveragePrimitivesInLeaf", stats.avgPrimitivesInLeaf);
        appStats.AddData("MaxPrimitivesInLeaf", stats.maxPrimitivesInLeaf);
        appStats.AddData("AverageDepth", stats.avgDepth);
        appStats.AddData("MaxDepth", stats.maxDepth);

        return ret;
    }

    KDTGPUTraverser KDMergeTreeGPU::GetStaticTraverser() const {
        return KDTGPUTraverser{ .instances{nullptr}, .nodes{nullptr}, .rootIdx{-1} };
    }

    KDTGPUTraverser KDMergeTreeGPU::GetDynamicTraverser() const {
        return KDTGPUTraverser{
            gpuMeshManager->Instances(),
            computeData->builder.nodes.Get(),
            gpuMeshManager->SceneBoundingBox(),
            mergeBuilder->RootIdx()
        };
    }

    UniqueMemoryPool& KDMergeTreeGPU::DataMemoryPool() {
        return computeData->memoryPool;
    }
}
