/**
 * File name: KDTBinsCPUBuilder.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "KDTCPUBuilderBase.hpp"
#include "KDTreeCPU.hpp"

namespace ProperRT
{
    /// @brief Class for a k-d tree builder using min-max binning on the CPU.
    class KDTBinsCPUBuilder : public KDTCPUBuilderBase
    {
    public:
        using Node = KDTCPUBuilderBase::Node;

        /// @brief The number of bins used for binning.
        static inline constexpr
        int32 binN = PRT_KDTREE_BINS;

    public:
        virtual
        ~KDTBinsCPUBuilder();

        virtual
        void Build(IMeshManager& meshManager, std::vector<PUID> primitives, std::vector<AABB> primitiveBBs, KDTreeParams const& parameters) override;

        virtual
        bool IsBuilt() const override { return isBuilt; }

        virtual
        std::span<Node> Nodes() override { return nodes; }

        virtual
        std::vector<Node> const& Nodes() const override { return nodes; }

        virtual
        int32 RootIdx() const override { return 0; }

        virtual
        Node* Root() { return (IsBuilt() ? &nodes[0] : nullptr); }

        virtual
        Node const* Root() const { return (IsBuilt() ? &nodes[0] : nullptr); }

        virtual
        int32 MaxDepth(int32 primitiveN, KDTreeParams const& parameters) const override {
            return static_cast<int32>(parameters.maxDepth_k1 + parameters.maxDepth_k2 * std::log2(static_cast<cfloat>(primitiveN)));
        }

        virtual
        int64 AllocatedMemory() const override;

        virtual
        void Reset() override;

    private:
        struct Event
        {
            SplitEventType type;
            int16 primitiveIdx;

            float Coordinate(std::span<AABB const> primitiveBBs, int axis) const {
                return primitiveBBs[primitiveIdx].GetBounds(static_cast<AABB::BoundsEnum>(type))[axis];
            }
        };

        struct BuildItem
        {
            int32 node{ -1 };
            AABB boundingBox{};
            std::vector<PUID> primitives{};
            std::vector<AABB> primitiveBBs{};
            union {
                struct
                {
                    int32 minBins[3][binN];
                    int32 maxBins[3][binN];
                };
                struct
                {
                    Event events[3][binN * 2];
                };
            };
            int32 depth{ -1 };
        };

        struct BestSplit
        {
            SplitPlane plane{ Axis3D::None };
            cfloat minCost{ std::numeric_limits<cfloat>::infinity() };
        };

    private:
        std::vector<Node> nodes;

        bool isBuilt{ false };

        void Bin(BuildItem& item);

        void CreateEvents(BuildItem& item);

        BestSplit FindBestPlane(BuildItem& item, KDTreeParams const& parameters);

        BestSplit FindBestPlaneExact(BuildItem& item, KDTreeParams const& parameters);

        void Distribute(BestSplit const& split, BuildItem const& item, BuildItem& left, BuildItem& right, IMeshManager& meshManager);

        bool SplitExact(BuildItem const& item) {
            return (StdSize<int32>(item.primitives) <= -1);
        }
    };
}
