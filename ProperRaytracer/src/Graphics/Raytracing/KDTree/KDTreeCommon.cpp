/**
 * File name: KDTreeCommon.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "KDTreeCommon.hpp"

#include "SceneDefinition/Scene.hpp"
#include "RTApp.hpp"

namespace ProperRT
{
    KDTreeParams CreateKDTreeParameters() {
        auto const& sceneConfig = Scene::GetActiveScene()->Config();
        KDTreeParams params{
            .maxDepth_k1 = sceneConfig.GetValue("maxDepth_k1", 1.2_cf),
            .maxDepth_k2 = sceneConfig.GetValue("maxDepth_k2", 2.0_cf),
            .traversalCost = sceneConfig.GetValue("traversalCost", 1.0_cf),
            .intersectionCost = sceneConfig.GetValue("intersectionCost", 1.0_cf),
            .primitiveLimit = sceneConfig.GetValue("primitiveLimit", 2)
        };

        auto& stats = RTApp::Instance()->statistics;
        stats.AddInfo("maxDepth_k1", params.maxDepth_k1);
        stats.AddInfo("maxDepth_k2", params.maxDepth_k2);
        stats.AddInfo("traversalCost", params.traversalCost);
        stats.AddInfo("intersectionCost", params.intersectionCost);
        stats.AddInfo("primitiveLimit", params.primitiveLimit);

        return params;
    }
}
