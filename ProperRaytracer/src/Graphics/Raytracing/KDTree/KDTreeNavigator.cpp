/**
 * File name: KDTreeNavigator.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "KDTreeNavigator.hpp"

#include <stdexcept>
#include <glbinding/gl/gl.h>

#include "InputSystem/Input.hpp"
#include "SceneDefinition/Components/Camera.hpp"

namespace ProperRT
{
    KDTreeNavigator::KDTreeNavigator(NavigatableBinaryTree* tree_)
        : Base{ tree_ }
    {
    }

    void KDTreeNavigator::StartNavigating() {
        // Create meshes
        primitiveMeshTree = CreateBinaryTreeMesh();
        primitiveMeshTree->CreateMeshes(GetMeshManager());
        primitiveMeshTreeNode = primitiveMeshTree->Root();

        if (splitPlane == nullptr) {
            splitPlane = std::make_shared<OrientedBoundingBoxMesh>(std::vector<OrientedBoundingBox>{ OrientedBoundingBox{} });
            splitPlane->SetMaterial(Material{.colorDiffuse{NormalizedConvert<Color32f>(splitPlaneColor)}});
        }

        Base::StartNavigating();

        if (dynamic_cast<KDTreeNavigationNode*>(node.get()) == nullptr) {
            throw std::runtime_error{"k-d tree navigator navigation node is not of KDTreeNavigationNode type"}; //TODO
        }
    }

    void KDTreeNavigator::NavigateOut() {
        splitChain.clear();

        splitPlane = nullptr;
        primitiveMeshTreeNode = nullptr;
        primitiveMeshTree = nullptr;

        Base::NavigateOut();
    }

    void KDTreeNavigator::Render() {
        gl::GLenum oldMode[2];
        gl::glGetIntegerv(gl::GLenum::GL_POLYGON_MODE, &oldMode[0]);

        gl::glPolygonMode(gl::GLenum::GL_FRONT_AND_BACK, gl::GLenum::GL_LINE);

        auto matrices = Components::Camera::MainCamera()->GetCameraMatrices();

        // Render primitives
        primitiveMeshTreeNode->Render(matrices);

        // Render node
        Matrix4x4f modelMat{ Matrix4x4f::Identity() };

        nodeBoundingBox->Rasterize(modelMat, matrices);
        splitPlane->Rasterize(modelMat, matrices);

        gl::glPolygonMode(gl::GLenum::GL_FRONT_AND_BACK, oldMode[0]);
    }

    void KDTreeNavigator::NavigateUp() {
        splitChain.pop_back();

        primitiveMeshTreeNode = primitiveMeshTreeNode->Parent();

        Base::NavigateUp();
    }

    void KDTreeNavigator::NavigateLeft() {
        KDTreeNavigationNode* kdnode = dynamic_cast<KDTreeNavigationNode*>(node.get());
        splitChain.push_back(SplitPlane{ kdnode->SplitAxis(), kdnode->SplitCoordinate() });

        primitiveMeshTreeNode = primitiveMeshTreeNode->Left();

        Base::NavigateLeft();
    }

    void KDTreeNavigator::NavigateRight() {
        KDTreeNavigationNode* kdnode = dynamic_cast<KDTreeNavigationNode*>(node.get());
        splitChain.push_back(SplitPlane{ kdnode->SplitAxis(), kdnode->SplitCoordinate() });

        primitiveMeshTreeNode = primitiveMeshTreeNode->Right();

        Base::NavigateRight();
    }

    void KDTreeNavigator::NavigateSwitch() {
        Base::NavigateSwitch();

        KDTreeNavigationNode* kdnode = dynamic_cast<KDTreeNavigationNode*>(node.get());
        splitChain.back() = SplitPlane{ kdnode->SplitAxis(), kdnode->SplitCoordinate() };

        primitiveMeshTreeNode = primitiveMeshTreeNode->Parent();
        if (navigationChain.back() == Side::Left) {
            primitiveMeshTreeNode = primitiveMeshTreeNode->Left();
        }
        else {
            primitiveMeshTreeNode = primitiveMeshTreeNode->Right();
        }
    }

    std::unique_ptr<BinaryTreeMesh> KDTreeNavigator::CreateBinaryTreeMesh() {
        struct StackItem
        {
            BinaryTreeMesh::Node* meshTreeNode;
            std::unique_ptr<KDTreeNavigationNode> navigationNode;
        };

        std::unique_ptr<BinaryTreeMesh> meshTree = std::make_unique<BinaryTreeMesh>(4);

        std::stack<StackItem> stack{};
        stack.push(StackItem{ meshTree->Root(), dynamic_cast<KDTreeNavigationNode*>(tree->NavigationRoot().get())->NoParentCopy() });

        while (!stack.empty()) {
            StackItem item = std::move(stack.top());
            stack.pop();

            if (item.navigationNode->IsLeaf()) {
                auto p = item.navigationNode->LeafPrimitives();
                item.meshTreeNode->SetPrimitives(p.begin(), p.end());
            }
            else {
                auto left = item.navigationNode->NoParentCopy();
                left->GoLeft();
                stack.push(StackItem{ item.meshTreeNode->CreateLeft(), std::move(left) });
                item.navigationNode->GoRight();
                stack.push(StackItem{ item.meshTreeNode->CreateRight(), std::move(item.navigationNode) });
            }
        }

        return meshTree;
    }

    void KDTreeNavigator::UpdateMeshes() {
        // Update split
        if (node->IsLeaf()) {
            splitPlane->Boxes()[0] = AABB{};
        }
        else {
            AABB plane = node->BoundingBox();
            KDTreeNavigationNode* kdnode = dynamic_cast<KDTreeNavigationNode*>(node.get());
            plane.minBounds[ToUnderlying(kdnode->SplitAxis())] = plane.maxBounds[ToUnderlying(kdnode->SplitAxis())] = kdnode->SplitCoordinate();

            splitPlane->Boxes()[0] = plane;
        }
        splitPlane->SyncToGPU();

        Base::UpdateMeshes();
    }

    std::string KDTreeNavigator::CreateNavigationChainString() const {
        std::stringstream ss;
        auto splitIt = splitChain.begin();
        for (auto it = navigationChain.begin(); it != navigationChain.end(); ++it, ++splitIt) {
            ss << (*it == Side::Left ? "Left" : "Right");
            ss << ',';
            ss << ((splitIt->axis == Axis3D::X) ? 'X' : ((splitIt->axis == Axis3D::Y) ? 'Y' : 'Z'));

            if (it != navigationChain.end() - 1) ss << " -> ";
        }

        return ss.str();
    }

    void KDTreeNavigator::SetButtons() {
        disableUp = !node->CanGoUp();
        disableLeft = !node->CanGoLeft();
        disableRight = !node->CanGoRight();
        disableNeigh = disableUp;
    }

    std::unique_ptr<BinaryTreeNodeNavigator> KDTreeNavigator::CreateNodeNavigator() {
        return std::make_unique<KDTreeNodeNavigator>(this);
    }
}
