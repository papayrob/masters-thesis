/**
 * File name: KDTBinsCPUBuilder.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "KDTBinsCPUBuilder.hpp"

namespace ProperRT
{
    KDTBinsCPUBuilder::~KDTBinsCPUBuilder() {
        Reset();
    }

    void KDTBinsCPUBuilder::Build(IMeshManager& meshManager, std::vector<PUID> primitives_, std::vector<AABB> primitiveBBs, KDTreeParams const &parameters) {
        if (primitives_.size() != primitiveBBs.size() || primitives_.size() < 0) {
            //TODO throw
            assert(false);
        }
        
        Reset();

        // Init
        std::stack<BuildItem> recursion;
        nodes.push_back(Node{});

        if (primitives_.size() == 0) {
            nodes.back().leaf = KDTCPUBuilderBase::LeafNode{ KDTNodeType::Leaf, 0, nullptr };
            isBuilt = true;
        }

        int32 maxDepth = MaxDepth(StdSize<int32>(primitives_), parameters);

        recursion.push(BuildItem{
            .node{ 0 },
            .boundingBox{ meshManager.SceneBoundingBox() },
            .primitives{ std::move(primitives_) },
            .primitiveBBs{ std::move(primitiveBBs) },
            .depth{ 0 } }
        );

        if (SplitExact(recursion.top())) {
            CreateEvents(recursion.top());
        }

        while (!recursion.empty()) {
            // Pop
            BuildItem item = std::move(recursion.top());
            recursion.pop();

            if (!SplitExact(item)) {
                Bin(item);
            }

            BestSplit bestSplit{};

            if (StdSize<int32>(item.primitives) > parameters.primitiveLimit && item.depth <= maxDepth) {//TODO LeafCondition
                if (SplitExact(item)) {
                    bestSplit = FindBestPlaneExact(item, parameters);
                }
                else {
                    bestSplit = FindBestPlane(item, parameters);
                }
            }

            // Inner node
            if (bestSplit.plane.axis != Axis3D::None) {
                auto leftIdx = StdSize<int32>(nodes);
                nodes.push_back(Node{});
                recursion.push(BuildItem{ .node{ leftIdx }, .boundingBox{}, .depth{ item.depth + 1 } });
                BuildItem& leftItem = recursion.top();

                nodes.push_back(Node{});
                recursion.push(BuildItem{ .node{ leftIdx + 1 }, .boundingBox{}, .depth{ item.depth + 1 } });
                BuildItem& rightItem = recursion.top();

                item.boundingBox.SplitCoord(bestSplit.plane.axis, bestSplit.plane.coordinate, leftItem.boundingBox, rightItem.boundingBox);

                // Distribute primitives
                Distribute(bestSplit, item, leftItem, rightItem, meshManager);

                nodes[item.node].inner = KDTCPUBuilderBase::InnerNode{
                    .tag{ ToNodeType(bestSplit.plane.axis) },
                    .splitValue{ bestSplit.plane.coordinate },
                    .leftChildIdx{ leftIdx },
                    .rightChildIdx{ leftIdx + 1 }
                };
            }
            // Leaf node
            else {
                int32 primitiveN = StdSize<int32>(item.primitives);
                PUID* primitives = ((primitiveN > 0) ? new PUID[primitiveN] : nullptr);

                nodes[item.node].leaf = KDTCPUBuilderBase::LeafNode{ KDTNodeType::Leaf, primitiveN, primitives };

                std::copy(item.primitives.begin(), item.primitives.end(), primitives);
            }
        }

        isBuilt = true;
        Logger::LogInfo("Node count: " + std::to_string(nodes.size()));
    }

    void KDTBinsCPUBuilder::Bin(BuildItem& item) {
        // Clear bins
        for (int axis = 0; axis < 3; ++axis) {
            for (int bin = 0; bin < binN; ++bin) {
                item.minBins[axis][bin] = 0;
                item.maxBins[axis][bin] = 0;
            }
        }

        Vector3sf binSizes = (item.boundingBox.maxBounds - item.boundingBox.minBounds) / static_cast<sfloat>(binN);

        // Bin primitives based on bbs
        for (AABB const& primitiveBB : item.primitiveBBs) {
            Vector3i bids = static_cast<Vector3i>(((primitiveBB.minBounds - item.boundingBox.minBounds) / binSizes)/*.Floored() not needed for float -> int*/);
            bids.Clamp(0, binN - 1);

            for (int axis = 0; axis < 3; ++axis) {
                //TODO enabled axes
                ++(item.minBins[axis][bids[axis]]);
            }

            bids = static_cast<Vector3i>(((primitiveBB.maxBounds - item.boundingBox.minBounds) / binSizes).Ceiled()) - 1;
            bids.Clamp(0, binN - 1);
            for (int axis = 0; axis < 3; ++axis) {
                //TODO enabled axes
                ++(item.maxBins[axis][bids[axis]]);
            }
        }
    }

    void KDTBinsCPUBuilder::CreateEvents(BuildItem& item) {
        int16 eventIdx = 0;
        for (int16 primitiveIdx = 0, primitiveN = StdSize<int16>(item.primitives); primitiveIdx < primitiveN; ++primitiveIdx) {
            for (int axis = 0; axis < 3; ++axis) {
                item.events[axis][eventIdx] = Event{
                    SplitEventType::Start,
                    primitiveIdx
                };
                item.events[axis][eventIdx + 1] = Event{
                    SplitEventType::End,
                    primitiveIdx
                };
            }
            eventIdx += 2;
        }
    }

    KDTBinsCPUBuilder::BestSplit KDTBinsCPUBuilder::FindBestPlane(BuildItem& item, KDTreeParams const& parameters) {
        BestSplit bestSplit{};
        Vector3cf sideLengths = item.boundingBox.SideLengths();
        Vector3cf binSizes = (sideLengths) / static_cast<sfloat>(binN);
        cfloat parentSurface{ item.boundingBox.SurfaceArea() };

        for (int axis = 0; axis < 3; ++axis) {
            // Exclusive scan min bins
            std::exclusive_scan(std::begin(item.minBins[axis]), std::end(item.minBins[axis]), std::begin(item.minBins[axis]), int32{0});
            std::inclusive_scan(std::rbegin(item.maxBins[axis]), std::rend(item.maxBins[axis]), std::rbegin(item.maxBins[axis]));
            
            int otherAxis1{ (axis + 1) % 3 }, otherAxis2{ (axis + 2) % 3 };

            PartialSurfaceArea partialSurface{ sideLengths[otherAxis1], sideLengths[otherAxis2] };
            
            // Compute SAH for all bins and save min cost (except for first bin, bcs zero volume)
            for (int bin = 1; bin < binN; ++bin) {
                cfloat sideLeft = bin * binSizes[axis];
                cfloat sideRight = (binN - bin) * binSizes[axis];
                cfloat cost = ComputeSAH(
                    partialSurface(sideLeft),
                    partialSurface(sideRight),
                    parentSurface,
                    item.minBins[axis][bin],
                    item.maxBins[axis][bin],
                    parameters.traversalCost,
                    parameters.intersectionCost
                );

                if (cost < bestSplit.minCost) {
                    bestSplit.plane.axis = static_cast<Axis3D>(axis);
                    bestSplit.plane.coordinate = item.boundingBox.minBounds[axis] + bin * binSizes[axis];
                    bestSplit.minCost = cost;
                }
            }
        }

        if (bestSplit.minCost > parameters.intersectionCost * StdSize<int32>(item.primitives)) {
            bestSplit.plane.axis = Axis3D::None;
        }

        return bestSplit;
    }

    KDTBinsCPUBuilder::BestSplit KDTBinsCPUBuilder::FindBestPlaneExact(BuildItem& item, KDTreeParams const& parameters) {
        BestSplit bestSplit{};

        int16 primitiveN = StdSize<int16>(item.primitives);
        cfloat parentSurface{ item.boundingBox.SurfaceArea() };

        for (int axis = 0; axis < 3; ++axis) {
            std::sort(&item.events[axis][0], &item.events[axis][2 * primitiveN],
                [&item, axis] (Event const& left, Event const& right) {
                    return left.Coordinate(item.primitiveBBs, axis) < right.Coordinate(item.primitiveBBs, axis);
                }
            );

            int16 leftPrimitiveN{ 0 }, rightPrimitiveN{ primitiveN };

            Axis3D otherAxis1{ (axis + 1) % 3 }, otherAxis2{ (axis + 2) % 3 };

            PartialSurfaceArea leftPartial{ item.boundingBox.SideLength(otherAxis1), item.boundingBox.SideLength(otherAxis2) };
            PartialSurfaceArea rightPartial{ item.boundingBox.SideLength(otherAxis1), item.boundingBox.SideLength(otherAxis2) };
            
            for (int16 eventIdx = 0; eventIdx < 2 * primitiveN;) {
                float currentCoordinate = item.events[axis][eventIdx].Coordinate(item.primitiveBBs, axis);

                // Count number of starting and ending events at current coordinate
                int32 ending{ 0 }, starting{ 0 };
                while (eventIdx < 2 * primitiveN && item.events[axis][eventIdx].Coordinate(item.primitiveBBs, axis) == currentCoordinate) {
                    switch (item.events[axis][eventIdx].type)
                    {
                    case SplitEventType::Start:
                        ++starting;
                        break;
                    case SplitEventType::End:
                        ++ending;
                        break;
                    }

                    ++eventIdx;
                }

                // Remove ending primitives from right
                rightPrimitiveN -= ending;

                // Update cost (don't count box edges)
                assert(rightPrimitiveN >= 0 && leftPrimitiveN >= 0 && leftPrimitiveN + rightPrimitiveN >= primitiveN);
                if (currentCoordinate != item.boundingBox.minBounds[axis] && currentCoordinate != item.boundingBox.maxBounds[axis]) {
                    auto cost = ComputeSAH(
                        leftPartial(currentCoordinate - item.boundingBox.minBounds[axis]),
                        rightPartial(item.boundingBox.maxBounds[axis] - currentCoordinate),
                        parentSurface,
                        leftPrimitiveN,
                        rightPrimitiveN,
                        parameters.traversalCost,
                        parameters.intersectionCost
                    );

                    if (cost < bestSplit.minCost) {
                        bestSplit.plane.axis = static_cast<Axis3D>(axis);
                        bestSplit.plane.coordinate = currentCoordinate;
                        bestSplit.minCost = cost;
                    }
                }

                // Add starting primitives to left
                leftPrimitiveN += starting;
            }
        }

        if (bestSplit.minCost > parameters.intersectionCost * primitiveN) {
            bestSplit.plane.axis = Axis3D::None;
        }

        return bestSplit;
    }

    void KDTBinsCPUBuilder::Distribute(BestSplit const& split, BuildItem const& item, BuildItem& left, BuildItem& right, IMeshManager& meshManager) {
        bool exact = SplitExact(item);
        for (int32 i = 0, n = StdSize<int32>(item.primitives); i < n; ++i) {
            if (item.primitiveBBs[i].maxBounds[ToUnderlying(split.plane.axis)] <= split.plane.coordinate) {
                left.primitives.push_back(item.primitives[i]);
                left.primitiveBBs.push_back(item.primitiveBBs[i]);
                
                if (exact) {
                    int eventIdx = (StdSize<int>(left.primitives) - 1) * 2;
                    for (int axis = 0; axis < 3; ++axis) {
                        int16 primitiveIdx = StdSize<int16>(left.primitives) - 1;
                        left.events[axis][eventIdx] = Event{
                            SplitEventType::Start,
                            primitiveIdx
                        };
                        left.events[axis][eventIdx + 1] = Event{
                            SplitEventType::End,
                            primitiveIdx
                        };
                    }
                }
            }
            else if (split.plane.coordinate <= item.primitiveBBs[i].minBounds[ToUnderlying(split.plane.axis)]) {
                right.primitives.push_back(item.primitives[i]);
                right.primitiveBBs.push_back(item.primitiveBBs[i]);

                if (exact) {
                    int eventIdx = (StdSize<int>(right.primitives) - 1) * 2;
                    for (int axis = 0; axis < 3; ++axis) {
                        int16 primitiveIdx = StdSize<int16>(right.primitives) - 1;
                        right.events[axis][eventIdx] = Event{
                            SplitEventType::Start,
                            primitiveIdx
                        };
                        right.events[axis][eventIdx + 1] = Event{
                            SplitEventType::End,
                            primitiveIdx
                        };
                    }
                }
            }
            else {
                left.primitives.push_back(item.primitives[i]);
                right.primitives.push_back(item.primitives[i]);

                constexpr bool doSplitClip{PRT_SPLIT_CLIP};
                if constexpr (doSplitClip) {
                    auto [leftBB, rightBB] = SplitClipBB(meshManager.GetPrimitive(item.primitives[i]), item.boundingBox, ToUnderlying(split.plane.axis), split.plane.coordinate);
                    if (!leftBB.IsValid() || !rightBB.IsValid()) {
                        if (!leftBB.IsValid()) {
                            leftBB = item.primitiveBBs[i];
                            leftBB.maxBounds[ToUnderlying(split.plane.axis)] = split.plane.coordinate;
                        }
                        if (!rightBB.IsValid()) {
                            rightBB = item.primitiveBBs[i];
                            rightBB.minBounds[ToUnderlying(split.plane.axis)] = split.plane.coordinate;
                        }
                    }

                    left.primitiveBBs.push_back(leftBB);
                    right.primitiveBBs.push_back(rightBB);
                }
                else {
                    AABB leftBB{ item.primitiveBBs[i] }, rightBB{ leftBB };
                    leftBB.maxBounds[ToUnderlying(split.plane.axis)] = split.plane.coordinate;
                    rightBB.minBounds[ToUnderlying(split.plane.axis)] = split.plane.coordinate;

                    left.primitiveBBs.push_back(leftBB);
                    right.primitiveBBs.push_back(rightBB);
                }

                if (exact) {
                    {
                    int eventIdx = (StdSize<int>(left.primitives) - 1) * 2;
                    for (int axis = 0; axis < 3; ++axis) {
                        int16 primitiveIdx = StdSize<int16>(left.primitives) - 1;
                        left.events[axis][eventIdx] = Event{
                            SplitEventType::Start,
                            primitiveIdx
                        };
                        left.events[axis][eventIdx + 1] = Event{
                            SplitEventType::End,
                            primitiveIdx
                        };
                    }
                    }
                    {
                    int eventIdx = (StdSize<int>(right.primitives) - 1) * 2;
                    for (int axis = 0; axis < 3; ++axis) {
                        int16 primitiveIdx = StdSize<int16>(right.primitives) - 1;
                        right.events[axis][eventIdx] = Event{
                            SplitEventType::Start,
                            primitiveIdx
                        };
                        right.events[axis][eventIdx + 1] = Event{
                            SplitEventType::End,
                            primitiveIdx
                        };
                    }
                    }
                }
            }
        }

        if (!exact) {
            if (SplitExact(left)) {
                CreateEvents(left);
            }
            if (SplitExact(right)) {
                CreateEvents(right);
            }
        }
    }

    int64 KDTBinsCPUBuilder::AllocatedMemory() const {
        std::size_t memory{0};
        for (KDTNode const& node : nodes) {
            if (node.IsLeaf()) {
                memory += node.leaf.primitiveN * sizeof(PUID);
            }
        }
        return static_cast<int64>(std::span{nodes}.size_bytes() + memory);
    }

    void KDTBinsCPUBuilder::Reset()
    {
        isBuilt = false;
        
        for (auto const& node : nodes) {
            if (node.IsLeaf() && node.leaf.primitives != nullptr) {
                delete[] node.leaf.primitives;
            }
        }
        nodes.clear();
    }
}
