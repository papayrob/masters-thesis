/**
 * File name: KDTCPUBuilderBase.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "KDTreeCommon.hpp"
#include "Graphics/Raytracing/CPUMeshManager.hpp"

namespace ProperRT
{
    class KDTCPUBuilderBase
    {
    public:
        using Node = KDTNode;
        using LeafNode = KDTLeafNode;
        using InnerNode = KDTInnerNode;

        /// @brief Dtor.
        virtual
        ~KDTCPUBuilderBase() = default;
        
        /// @brief Builds the tree.
        /// @param meshManager Mesh manager to build the tree over
        /// @param primitives Scene primitives
        /// @param primitiveBBs Primitive bounding boxes
        /// @param parameters Parameters
        virtual
        void Build(IMeshManager& meshManager, std::vector<PUID> primitives, std::vector<AABB> primitiveBBs, KDTreeParams const& parameters) = 0;

        /// @brief Returns true if the tree is built.
        virtual
        bool IsBuilt() const = 0;

        /// @brief Returns the nodes on the GPU.
        virtual
        std::span<Node> Nodes() = 0;

        /// @brief Returns the nodes on the GPU.
        virtual
        std::vector<Node> const& Nodes() const = 0;

        /// @brief Returns the index of the root node.
        virtual
        int32 RootIdx() const = 0;

        /// @brief Returns the root node.
        virtual
        Node* Root() {
            return (IsBuilt() ? &Nodes()[RootIdx()] : nullptr);
        }

        /// @brief Returns the root node.
        virtual
        Node const* Root() const {
            return (IsBuilt() ? &Nodes()[RootIdx()] : nullptr);
        }

        /// @brief Returns the primitives of @p leaf.
        /// @param leaf Leaf node
        virtual
        std::span<PUID> LeafPrimitives(LeafNode const& leaf) {
            return std::span<PUID>{leaf.primitives, static_cast<std::size_t>(leaf.primitiveN)};
        }

        virtual
        std::span<PUID const> LeafPrimitives(LeafNode const& leaf) const {
            return std::span<PUID>{leaf.primitives, static_cast<std::size_t>(leaf.primitiveN)};
        }

        /// @brief Returns the left child of @p innerNode.
        /// @param innerNode Inner node
        virtual
        Node* GetLeftChild(InnerNode& innerNode) {
            if (innerNode.leftChildIdx < 0) {
                return nullptr;
            }
            return &(Nodes()[innerNode.leftChildIdx]);
        }

        /// @brief Returns the left child of @p innerNode.
        /// @param innerNode Inner node
        virtual
        Node const* GetLeftChild(InnerNode const& innerNode) const {
            if (innerNode.leftChildIdx < 0) {
                return nullptr;
            }
            return &(Nodes()[innerNode.leftChildIdx]);
        }

        /// @brief Returns the right child of @p innerNode.
        /// @param innerNode Inner node
        virtual
        Node* GetRightChild(InnerNode& innerNode) {
            if (innerNode.rightChildIdx < 0) {
                return nullptr;
            }
            return &(Nodes()[innerNode.rightChildIdx]);
        }

        /// @brief Returns the right child of @p innerNode.
        /// @param innerNode Inner node
        virtual
        Node const* GetRightChild(InnerNode const& innerNode) const {
            if (innerNode.rightChildIdx < 0) {
                return nullptr;
            }
            return &(Nodes()[innerNode.rightChildIdx]);
        }

        /// @brief Returns the child of @p innerNode on @p side.
        /// @param side Side of the child (left/right)
        /// @param innerNode Inner node
         virtual
        Node* GetChild(Side side, InnerNode& innerNode) {
            if (side == Side::Left) {
                return GetLeftChild(innerNode);
            }
            else {
                return GetRightChild(innerNode);
            }
        }

        /// @brief Returns the child of @p innerNode on @p side.
        /// @param side Side of the child (left/right)
        /// @param innerNode Inner node
        virtual
        Node const* GetChild(Side side, InnerNode const& innerNode) const {
            if (side == Side::Left) {
                return GetLeftChild(innerNode);
            }
            else {
                return GetRightChild(innerNode);
            }
        }

        /// @brief Returns the max depth based on @p primitiveN and @p parameters.
        /// @param primitiveN Number of primitives
        /// @param parameters Parameters
        virtual
        int32 MaxDepth(int32 primitiveN, KDTreeParams const& parameters) const = 0;

        /// @brief Calculates the total amount of allocated memory and returns it.
        virtual
        int64 AllocatedMemory() const = 0;

        /// @brief Resets the builder.
        virtual
        void Reset() = 0;
    };
}