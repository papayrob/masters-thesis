/**
 * File name: KDTreeGPU.cu
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "KDTreeGPU.hpp"

#include "Graphics/Raytracing/GPUMeshManager.cuh"
#include "KDTBinsGPUBuilder.cuh"
#include "KDTCPU2GPUBuilder.cuh"
#include "KDTEventsCPUBuilder.hpp"
#include "KDTBinsCPUBuilder.hpp"
#include "KDTGPUTraverser.cuh"
#include "KDTreeNavigationNodeCommon.hpp"
#include "RTApp.hpp"

namespace ProperRT
{
    namespace
    {
        TraversalStatistics traversalStatsDummy;

        std::unique_ptr<KDTGPUBuilderBase> CreateBuilder(std::string const& value, SceneConfig const& sceneConfig, UniqueMemoryPool& memoryPool) {
            if (sceneConfig.GetValue<bool>("buildOnCPU", false)) {
                if (value == "Events") {
                    return std::make_unique<KDTCPU2GPUBuilder>(std::make_unique<KDTEventsCPUBuilder>());
                }
                else if (value == "Bins") {
                    return std::make_unique<KDTCPU2GPUBuilder>(std::make_unique<KDTBinsCPUBuilder>());
                }
            }
            else {
                if (value == "Events") {
                }
                else if (value == "Bins") {
                    return std::make_unique<KDTBinsGPUBuilder>(memoryPool);
                }
            }
            return nullptr;
        }
    }

    struct KDTreeGPU::ComputeData
    {
        UniqueMemoryPool memoryPool;

        ComputeData(MemoryPool::SizeType memPoolCapacity)
            : memoryPool{memPoolCapacity}
        {}
    };
    
    KDTreeGPU::KDTreeGPU()
        : topNavigator{ this }, staticNavigator{ this, true }, dynamicNavigator{ this, false }
    {
        auto const& sceneConfig = Scene::GetActiveScene()->Config();

        computeData = std::make_unique<ComputeData>(
            ToBytesPrefixAs<MemoryPool::SizeType>(PrefixedValue{MetricPrefix::Giga, sceneConfig.GetValue<double>("dataMemoryPoolCapacity", 4.)}, MetricPrefix::None)
        );

        if (std::string value; sceneConfig.TryGetValue("staticBuilder", value)) {
            staticBuilder = CreateBuilder(value, sceneConfig, computeData->memoryPool);
            
            if (staticBuilder != nullptr) {
                staticMeshManager = std::make_unique<GPUMeshManager>();
            }
        }

        if (std::string value; sceneConfig.TryGetValue("dynamicBuilder", value)) {
            dynamicBuilder = CreateBuilder(value, sceneConfig, computeData->memoryPool);
            dynamicMeshManager = std::make_unique<GPUMeshManager>();
        }

        if (dynamicBuilder == nullptr) {
            //TODO own exception (ConfigException)
            throw std::runtime_error("Cannot run without a dynamic tree builder");
        }
    }

    KDTreeGPU::~KDTreeGPU() = default;

    void KDTreeGPU::BuildStatic(std::vector<CompPtr<Components::Renderer>>&& renderers_) {
        if (staticMeshManager != nullptr && staticBuilder != nullptr) {
            renderers.clear();
            staticRendererCount = -1;

            auto&& [primitives, primitiveBBs] = staticMeshManager->CreateInstances(renderers_);
            RTApp::Instance()->statistics.AddInfo("StaticPrimitiveCount", primitives.Count());

            computeData->memoryPool.GetCPU().Reset();
            computeData->memoryPool.SyncCPU2GPU();

            staticBuilder->Build(*staticMeshManager, std::move(primitives), std::move(primitiveBBs), parameters);
        }
        else {
            renderers = std::move(renderers_);
            staticRendererCount = StdSize<int32>(renderers);
            RTApp::Instance()->statistics.AddInfo("StaticPrimitiveCount", 0);
        }
    }

    void KDTreeGPU::BuildDynamic(std::vector<CompPtr<Components::Renderer>>&& renderers_) {
        std::span<CompPtr<Components::Renderer>> rendererSpan;
        if (staticRendererCount < 0) {
            rendererSpan = std::span<CompPtr<Components::Renderer>>{renderers_};
        }
        else {
            renderers.resize(staticRendererCount + renderers_.size());
            std::move(renderers_.begin(), renderers_.end(), renderers.begin() + staticRendererCount);
            rendererSpan = std::span<CompPtr<Components::Renderer>>{renderers};
        }

        auto&& [primitives, primitiveBBs] = dynamicMeshManager->CreateInstances(rendererSpan);
        RTApp::Instance()->statistics.AddInfo("DynamicPrimitiveCount", primitives.Count());

        Compute::CudaCheckError(cudaDeviceSynchronize());
        computeData->memoryPool.GetCPU().Reset();
        computeData->memoryPool.SyncCPU2GPU();

        dynamicBuilder->Build(*dynamicMeshManager, std::move(primitives), std::move(primitiveBBs), parameters);

#ifdef PRT_DEBUG
        Compute::CudaCheckError(cudaDeviceSynchronize());
#endif
    }

    bool KDTreeGPU::IsBuilt() const {
        return ((staticBuilder == nullptr ? true : staticBuilder->IsBuilt()) && dynamicBuilder->IsBuilt());
    }

    INavigator* KDTreeGPU::TopNavigator() {
        if ((staticBuilder != nullptr && staticBuilder->IsBuilt()) || dynamicBuilder->IsBuilt()) {
            return &topNavigator;
        }

        return nullptr;
    }

    IStructureValidator* KDTreeGPU::Validator() {
        if ((staticBuilder != nullptr && staticBuilder->IsBuilt()) || dynamicBuilder->IsBuilt()) {
            return &topNavigator;
        }

        return nullptr;
    }

    namespace Kernels
    {
        __global__
        void Traverse(Ray ray, cfloat maxDist, KDTGPUTraverser traverser, int32 stackSize, DevicePointer<cuda::std::optional<KDTGPUTraverser::RayHitGPU>> rayHit, DevicePointer<TraversalStatistics> traversalStats) {
            extern __shared__ KDTGPUTraverser::StackItem stack[];
            *rayHit = traverser.Traverse<KDTGPUTraverser::RestartStrategy::Restart>(ray, maxDist, stack, stackSize, *traversalStats);
        }

        __global__
        void TraverseShadow(Ray ray, cfloat maxDist, KDTGPUTraverser traverser, int32 stackSize, DevicePointer<bool> hit, DevicePointer<TraversalStatistics> traversalStats) {
            extern __shared__ KDTGPUTraverser::StackItem stack[];
            *hit = traverser.TraverseShadow<KDTGPUTraverser::RestartStrategy::Restart>(ray, maxDist, stack, stackSize, *traversalStats);
        }
    }

    std::optional<RayHit> KDTreeGPU::Traverse(Ray const& ray, cfloat maxDist, TraversalStatistics* traversalStats) {
        if (!IsBuilt()) {
            return {}; //TODO mby throw
        }

        if (traversalStats == nullptr) {
            traversalStats = &traversalStatsDummy;
        }

        ++(traversalStats->queryCount);

        // Allocate for gpu traversal
        auto hitGPU = DeviceUniquePointer<cuda::std::optional<KDTGPUTraverser::RayHitGPU>>::Allocate();
        auto traversalStatsGPU = DeviceUniquePointer<TraversalStatistics>::AllocateAndUpload(*traversalStats);

        // Create empty hits
        cuda::std::optional<KDTGPUTraverser::RayHitGPU> hitStatic{};
        cuda::std::optional<KDTGPUTraverser::RayHitGPU> hitDynamic{};

        // Traverse dynamic tree
        auto traverser = Traverser(*dynamicMeshManager, *dynamicBuilder);

        auto const& sceneConfig = Scene::GetActiveScene()->Config();
        int32 stackSize = sceneConfig.GetValue<int32>("stackSize", 8);

        Kernels::Traverse<<<1, 1, stackSize * sizeof(KDTGPUTraverser::StackItem)>>>(ray, maxDist, traverser, stackSize, hitGPU.Get(), traversalStatsGPU.Get());
        #ifdef PRT_DEBUG
        Compute::CudaCheckError(cudaDeviceSynchronize());
        #endif

        hitDynamic = hitGPU.Download();

        // Traverse static tree if it exists
        if (staticBuilder != nullptr) {
            traverser = Traverser(*staticMeshManager, *staticBuilder);
            Kernels::Traverse<<<1, 1, stackSize * sizeof(KDTGPUTraverser::StackItem)>>>(ray, maxDist, traverser, stackSize, hitGPU.Get(), traversalStatsGPU.Get());
            #ifdef PRT_DEBUG
            Compute::CudaCheckError(cudaDeviceSynchronize());
            #endif

            hitStatic = hitGPU.Download();
        }

        *traversalStats = traversalStatsGPU.Download();

        // Combine static and dynamic traversals
        if (hitStatic.has_value() && hitDynamic.has_value()) {
            return (hitStatic->tHit < hitDynamic->tHit ?
                hitStatic->ToRayHitCPU(*staticMeshManager)
                :
                hitDynamic->ToRayHitCPU(*dynamicMeshManager)
            );
        }
        else if (hitStatic.has_value()) {
            return hitStatic->ToRayHitCPU(*staticMeshManager);
        }
        else {
            return hitDynamic->ToRayHitCPU(*dynamicMeshManager);
        }
    }

    bool KDTreeGPU::TraverseShadow(Ray const& ray, cfloat maxDist, TraversalStatistics* traversalStats) {
        if (!IsBuilt()) {
            return false; //TODO mby throw
        }

        if (traversalStats == nullptr) {
            traversalStats = &traversalStatsDummy;
        }

        ++(traversalStats->queryCount);

        // Allocate for gpu traversal
        auto hitGPU = DeviceUniquePointer<bool>::Allocate();
        auto traversalStatsGPU = DeviceUniquePointer<TraversalStatistics>::AllocateAndUpload(*traversalStats);

        // Create empty hits
        bool hitStatic{false};
        bool hitDynamic{false};

        auto const& sceneConfig = Scene::GetActiveScene()->Config();
        int32 stackSize = sceneConfig.GetValue<int32>("stackSize", 8);

        // Traverse dynamic tree
        auto traverser = Traverser(*dynamicMeshManager, *dynamicBuilder);
        Kernels::TraverseShadow<<<1, 1, stackSize * sizeof(KDTGPUTraverser::StackItem)>>>(ray, maxDist, traverser, stackSize, hitGPU.Get(), traversalStatsGPU.Get());
        #ifdef PRT_DEBUG
        Compute::CudaCheckError(cudaDeviceSynchronize());
        #endif

        hitDynamic = hitGPU.Download();

        // Traverse static tree if it exists
        if (staticBuilder != nullptr) {
            traverser = Traverser(*staticMeshManager, *staticBuilder);
            Kernels::TraverseShadow<<<1, 1, stackSize * sizeof(KDTGPUTraverser::StackItem)>>>(ray, maxDist, traverser, stackSize, hitGPU.Get(), traversalStatsGPU.Get());
            #ifdef PRT_DEBUG
            Compute::CudaCheckError(cudaDeviceSynchronize());
            #endif

            hitStatic = hitGPU.Download();
        }

        *traversalStats = traversalStatsGPU.Download();

        // Combine static and dynamic traversals
        return (hitStatic || hitDynamic);
    }

    std::unique_ptr<BinaryTreeNavigationNode> KDTreeGPU::NavigationRoot() {
        using NavigationNode = KDTreeNavigationNodeCommon<KDTGPUBuilderBase>;

        if (topNavigator.ChoseStatic()) {
            return std::unique_ptr<BinaryTreeNavigationNode>(new NavigationNode(
                *staticBuilder,
                *staticMeshManager,
                staticBuilder->Root(),
                staticMeshManager->SceneBoundingBox(),
                staticNavigator.primitiveMeshTree ? staticNavigator.primitiveMeshTree->Root() : nullptr
            ));
        }
        else {
            return std::unique_ptr<BinaryTreeNavigationNode>(new NavigationNode(
                *dynamicBuilder,
                *dynamicMeshManager,
                dynamicBuilder->Root(),
                dynamicMeshManager->SceneBoundingBox(),
                dynamicNavigator.primitiveMeshTree ? dynamicNavigator.primitiveMeshTree->Root() : nullptr
            ));
        }
    }

    KDTGPUTraverser KDTreeGPU::Traverser(GPUMeshManager const& meshManager, KDTGPUBuilderBase const& builder) const {
        return KDTGPUTraverser{
            meshManager.Instances(),
            builder.NodesGPU(),
            meshManager.SceneBoundingBox(),
            builder.RootIdx()
        };
    }

    GPUMeshManager const* KDTreeGPU::GetStaticMeshManager() const {
        return staticMeshManager.get();
    }

    KDTGPUBuilderBase const* KDTreeGPU::GetStaticBuilder() const {
        return staticBuilder.get();
    }

    GPUMeshManager const* KDTreeGPU::GetDynamicMeshManager() const {
        return dynamicMeshManager.get();
    }

    KDTGPUBuilderBase const* KDTreeGPU::GetDynamicBuilder() const {
        return dynamicBuilder.get();
    }

    KDTGPUTraverser KDTreeGPU::GetStaticTraverser() const {
        if (staticMeshManager == nullptr) {
            return KDTGPUTraverser{ .instances{nullptr}, .nodes{nullptr}, .rootIdx{-1} };
        }
        else {
            return Traverser(*staticMeshManager, *staticBuilder);
        }
    }

    KDTGPUTraverser KDTreeGPU::GetDynamicTraverser() const {
        return Traverser(*dynamicMeshManager, *dynamicBuilder);
    }

    UniqueMemoryPool& KDTreeGPU::DataMemoryPool() {
        return computeData->memoryPool;
    }
}
