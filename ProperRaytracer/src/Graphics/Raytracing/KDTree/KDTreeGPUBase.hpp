/**
 * File name: KDTreeGPUBase.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "Graphics/Raytracing/IAccelerationStructure.hpp"

namespace ProperRT
{
    class GPUMeshManager;
    class KDTGPUBuilderBase;
    class KDTGPUTraverser;
    class UniqueMemoryPool;

    /// @brief Base class for k-d trees usable by KDTGPURaytracer.
    class KDTreeGPUBase : public IAccelerationStructure
    {
    public:
        /// @brief Virtual default dtor.
        virtual
        ~KDTreeGPUBase() = default;

        /// @brief Returns the traverser for the static tree, can be empty.
        virtual
        KDTGPUTraverser GetStaticTraverser() const = 0;

        /// @brief Returns the traverser for the dynamic tree.
        virtual
        KDTGPUTraverser GetDynamicTraverser() const = 0;

        /// @brief Returns the data memory pool
        virtual
        UniqueMemoryPool& DataMemoryPool() = 0;
    };
}
