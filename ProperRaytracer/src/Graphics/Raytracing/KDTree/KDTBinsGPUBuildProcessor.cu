/**
 * File name: KDTBinsGPUBuildProcessor.cu
 * Description: See KDTBinsBuilder.cuh header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "KDTBinsGPUBuilder.cuh"
#include <cub/warp/warp_scan.cuh>

#define PRINT_STAGE 0
#define PRINT_POST_STAGE 0
#define TIME_PROCESSING 0
#define TIME_WORK_CHUNKS 1

namespace ProperRT
{
    __host__
    void KDTBinsGPUBuilder::Processor::RunKernel(KDTBinsGPUBuilder& builder, DevicePointer<PUID[]> primitives, DevicePointer<AABB[]> primitiveBBs, AABB const& sceneBoundingBox, Compute::TaskPoolProcessorParams processorParams) {
        // Create initial task for root node
        GroupTask initialTasks[] { GroupTask{} };
        initialTasks[0].phase = Phases::Binning;
        initialTasks[0].step = 0;
        initialTasks[0].workChunksLeft = LinearComputation::WorkChunkCount(primitives.count);

        allocationMultiplier = builder.memPoolMultiplier;
        initialTasks[0].customData.primitivesStorage = builder.primitiveMemPool.GetCPU().Allocate<PUID>(allocationMultiplier, primitives.count);
        
        auto allocatedSplit = builder.dataMemPool.AllocateOnDevice<AABB, AABB, PrimitiveCountType>(
            allocationMultiplier,
            primitives.count,
            primitives.count,
            2 * primitives.count
        ).Split();

        initialTasks[0].customData.bbsStorage = cuda::std::get<0>(allocatedSplit);
        initialTasks[0].customData.classificationStorage = cuda::std::get<2>(allocatedSplit);

        initialTasks[0].customData.PassDownStorage(
            builder.primitiveMemPool.GetCPU().Allocate<PUID>(allocationMultiplier, primitives.count),
            cuda::std::get<1>(allocatedSplit)
        );

        builder.primitiveMemPool.SyncCPU2GPU();

        primitives.HostMemCopyTo(DevicePointer<PUID[]>{ initialTasks[0].customData.Primitives(), primitives.count });
        primitiveBBs.HostMemCopyTo(DevicePointer<AABB[]>{ initialTasks[0].customData.PrimitiveBBs(), primitives.count });

        for (int axis = 0; axis < 3; ++axis) {
            for (int i = 0; i < binN; ++i) {
                initialTasks[0].customData.minBins[axis][i] = 0;
                initialTasks[0].customData.maxBins[axis][i] = 0;
            }
        }

        initialTasks[0].customData.primitiveN = primitives.count;
        initialTasks[0].customData.boundingBox = sceneBoundingBox;
        initialTasks[0].customData.depth = 0;

        initialTasks[0].customData.splitCoord = 0;
        initialTasks[0].customData.splitAxis = Axis3D::None;

        initialTasks[0].customData.enabledAxes.EnableAll();

        int32 const taskHeader[] { Base::TASK_LOCKED };

        Base::RunKernel<TaskPoolMemPoolDeleter>(builder.taskPoolState, initialTasks, taskHeader, processorParams);
    }

    __device__
    void KDTBinsGPUBuilder::Processor::Start() {
        auto tid = Compute::Grid1DHelpers::GlobalThreadIdx();

        #if PRT_TIME_KERNELS
        binTimer.Reset(tid);
        classificationTimer.Reset(tid);
        distributionTimer.Reset(tid);
        leafTimer.Reset(tid);
        lockTimer.Reset(tid);
        subTimer.Reset(tid);
        #endif

        if (tid == 0) {
            assert(taskPool->taskHeaders[0].load(cuda::memory_order_relaxed) == TASK_LOCKED);
            nodeListInfos[0] = 1;
            taskPool->tasks[0].customData.node = nodeLists[0].PushBack(KDTBinsGPUBuilder::Node{}, dataPool);
            taskPool->taskHeaders[0].store(*reinterpret_cast<int32*>(&taskPool->tasks[0].workChunksLeft), cuda::memory_order_release);
        }
    }

    __device__
    void KDTBinsGPUBuilder::Processor::Finish() {
        #if PRT_BUILD_STATUS
        if (Compute::Grid1DHelpers::GlobalThreadIdx() == 0) {
            while (workingSubgroups->load() != 0) {
            }
            BuildStatus status;
            status.depth = buildStatusAtomic->depth.load(cuda::memory_order_relaxed);
            status.nodes = buildStatusAtomic->nodes.load(cuda::memory_order_relaxed);
            status.tasks = buildStatusAtomic->tasks.load(cuda::memory_order_relaxed);
            status.workingSubgroups = workingSubgroups->load(cuda::memory_order_relaxed);
            *buildStatusHost = status;
        }
        #endif
    }

    __device__
    void KDTBinsGPUBuilder::Processor::Bin() {
        #if PRT_TIME_KERNELS && TIME_PROCESSING
        __threadfence_block();
        lockTimer.Start(Compute::Grid1DHelpers::GlobalThreadIdx());
        __threadfence_block();
        #endif

        // Compute bin sizes
        auto& taskCopyData = sharedParam->customData.taskCopy.customData;
        Vector3cf binSizes = taskCopyData.boundingBox.SideLengths() / static_cast<cfloat>(binN);

#if PRT_BIN_LOCALLY
        // Clear local bins
        for (int axis = 0; axis < 3; ++axis) {
            for (int i = Compute::Grid1DHelpers::LaneIdx(); i < binN; i += Compute::SUBGROUP_SIZE) {
                taskCopyData.minBins[axis][i] = 0;
                taskCopyData.maxBins[axis][i] = 0;
            }
        }
#else
        PrimitiveCountAtomic (*minBinGlobal)[binN] = &Base::SelectedTask()->customData.minBins[0]/*[axis]*/;
        PrimitiveCountAtomic (*maxBinGlobal)[binN] = &Base::SelectedTask()->customData.maxBins[0]/*[axis]*/;
#endif

        for (int workChunkIdx = Base::sharedParam->taskInfo.workChunkFirst, workChunkLast = workChunkIdx + Base::sharedParam->taskInfo.workChunkN; workChunkIdx < workChunkLast; ++workChunkIdx) {
            // Compute bucket id of the primitive
            auto gid = LinearComputation::GlobalArrayIndex(Compute::Grid1DHelpers::LaneIdx(), workChunkIdx);
            auto activeMask = __ballot_sync(Compute::FULL_MASK, gid < taskCopyData.primitiveN);
            if (gid < taskCopyData.primitiveN) {
                AABB primitiveBB = taskCopyData.PrimitiveBBs()[gid];

                Vector3i bids = static_cast<Vector3i>((static_cast<Vector3cf>(primitiveBB.minBounds - taskCopyData.boundingBox.minBounds) / binSizes)/*.Floored() not needed for float -> int*/);
                bids.Clamp(0, binN - 1);

                for (int axis = 0; axis < 3; ++axis) {
                    if (taskCopyData.enabledAxes.IsEnabled(static_cast<Axis3D>(axis))) {
                        #if PRT_BIN_LOCALLY
                        for (int bucket = 0; bucket < binN; ++bucket) {
                            PrimitiveCountType bucketed = __popc(__ballot_sync(activeMask, bids[axis] == bucket));
                            if (Compute::Grid1DHelpers::LaneIdx() == 0) {
                                taskCopyData.minBins[axis][bucket] += bucketed;
                            }
                        }
                        #else
                        minBinGlobal[axis][bids[axis]].fetch_add(1, cuda::memory_order_relaxed);
                        #endif
                    }
                }

                bids = static_cast<Vector3i>((static_cast<Vector3cf>(primitiveBB.maxBounds - taskCopyData.boundingBox.minBounds) / binSizes).Ceiled()) - 1;
                bids.Clamp(0, binN - 1);
                for (int axis = 0; axis < 3; ++axis) {
                    if (taskCopyData.enabledAxes.IsEnabled(static_cast<Axis3D>(axis))) {
                        #if PRT_BIN_LOCALLY
                        for (int bucket = 0; bucket < binN; ++bucket) {
                            PrimitiveCountType bucketed = __popc(__ballot_sync(activeMask, bids[axis] == bucket));
                            if (Compute::Grid1DHelpers::LaneIdx() == 0) {
                                taskCopyData.maxBins[axis][bucket] += bucketed;
                            }
                        }
                        #else
                        maxBinGlobal[axis][bids[axis]].fetch_add(1, cuda::memory_order_relaxed);
                        #endif
                    }
                }
            }
        }

        #if PRT_TIME_KERNELS && TIME_PROCESSING
        __threadfence_block();
        #if TIME_WORK_CHUNKS
        lockTimer.Stop(Compute::Grid1DHelpers::GlobalThreadIdx(), Base::sharedParam->taskInfo.workChunkN);
        #else
        lockTimer.Stop(Compute::Grid1DHelpers::GlobalThreadIdx());
        #endif
        __threadfence_block();
        #endif
        
#if PRT_BIN_LOCALLY
        for (int axis = 0; axis < 3; ++axis) {
            PrimitiveCountAtomic* minBinGlobal = Base::SelectedTask()->customData.minBins[axis];
            PrimitiveCountAtomic* maxBinGlobal = Base::SelectedTask()->customData.maxBins[axis];
            for (int i = 0; i < binN; i += Compute::SUBGROUP_SIZE) {
                int tid = i + Compute::Grid1DHelpers::LaneIdx();
                if (taskCopyData.minBins[axis][tid] > 0) {
                    minBinGlobal[tid].fetch_add(taskCopyData.minBins[axis][tid], cuda::memory_order_relaxed);
                }
                if (taskCopyData.maxBins[axis][tid] > 0) {
                    maxBinGlobal[tid].fetch_add(taskCopyData.maxBins[axis][tid], cuda::memory_order_relaxed);
                }
            }
        }
#endif
    }

    __device__
    void KDTBinsGPUBuilder::Processor::BinPostProcess() {
        // Scan bins and find min cost

        // Copy global bins to shared
        auto laneIdx = Compute::Grid1DHelpers::LaneIdx();
        auto& taskCopyData = sharedParam->customData.taskCopy.customData;
        PrimitiveCountType* binsGlobal = reinterpret_cast<PrimitiveCountType*>(&Base::SelectedTask()->customData.minBins[0][0]);
        for (int axis = 0; axis < 3; ++axis) {
            for (int i = laneIdx; i < binN; i += Compute::SUBGROUP_SIZE) {
                taskCopyData.minBins[axis][i] = binsGlobal[axis * binN + i];
            }
        }
        binsGlobal = reinterpret_cast<PrimitiveCountType*>(&Base::SelectedTask()->customData.maxBins[0][0]);
        for (int axis = 0; axis < 3; ++axis) {
            for (int i = laneIdx; i < binN; i += Compute::SUBGROUP_SIZE) {
                taskCopyData.maxBins[axis][i] = binsGlobal[axis * binN + i];
            }
        }

        cfloat minCost = cuda::std::numeric_limits<cfloat>::infinity();
        sfloat minCoord = 0._sf;
        Axis3D minAxis = Axis3D::None;
        cfloat parentSurface = taskCopyData.boundingBox.SurfaceArea();

        #if PRT_TIME_KERNELS && !TIME_PROCESSING
        __threadfence_block();
        subTimer.Start(Compute::Grid1DHelpers::GlobalThreadIdx());
        __threadfence_block();
        #endif

        for (int axis = 0; axis < 3; ++axis) {
            // Skip disabled axis
            if (!taskCopyData.enabledAxes.IsEnabled(static_cast<Axis3D>(axis))) {
                continue;
            }

            cfloat sideLength = taskCopyData.boundingBox.SideLength(static_cast<Axis3D>(axis));
            cfloat binSize = (sideLength) / static_cast<cfloat>(binN);

            Axis3D otherAxis1{ (axis + 1) % 3 }, otherAxis2{ (axis + 2) % 3 };
            PartialSurfaceArea partialSurface{ taskCopyData.boundingBox.SideLength(otherAxis1), taskCopyData.boundingBox.SideLength(otherAxis2) };

            if constexpr (binN == 32) {
                PrimitiveCountType maxSum = taskCopyData.primitiveN;
                assert(maxSum == Compute::SubgroupReduceLocal(taskCopyData.maxBins[axis][laneIdx], laneIdx, cuda::std::plus<PrimitiveCountType>{}));

                // Inclusive scan min bin part
                PrimitiveCountType value = Compute::SubgroupScanLocal(taskCopyData.minBins[axis][laneIdx], laneIdx, cuda::std::plus<PrimitiveCountType>{});

                // Shift right as we need exclusive scan
                PrimitiveCountType minValue = __shfl_up_sync(Compute::FULL_MASK, value, 1);
                if (laneIdx == 0) {
                    minValue = 0;
                }

                // Similarly with max bin
                value = Compute::SubgroupScanLocal(taskCopyData.maxBins[axis][laneIdx], laneIdx, cuda::std::plus<PrimitiveCountType>{});
                // To create reversed inclusive scan, we need to subtract exclusive scan values from total sum
                PrimitiveCountType maxValue = __shfl_up_sync(Compute::FULL_MASK, value, 1);
                if (laneIdx == 0) {
                    maxValue = 0;
                }
                maxValue = maxSum - maxValue;

                // Compute cost and take the minimum
                cfloat cost = ComputeSAH(
                    partialSurface(laneIdx * binSize),
                    partialSurface((binN - laneIdx) * binSize),
                    parentSurface,
                    minValue,
                    maxValue,
                    traversalCost,
                    intersectionCost
                );

                assert(cost > 0._cf);

                if (cost < minCost && laneIdx > 0) {
                    minCost = cost;
                    minCoord = taskCopyData.boundingBox.minBounds[axis] + laneIdx * binSize;
                    minAxis = static_cast<Axis3D>(axis);
                }
            }
            else {
                PrimitiveCountType minSum{};
                PrimitiveCountType maxPartialSum{};
                PrimitiveCountType maxSum = taskCopyData.primitiveN;
                assert(maxSum == Compute::SubgroupReduceArray(taskCopyData.maxBins[axis], PrimitiveCountType{ 0 }, binN, laneIdx, cuda::std::plus<PrimitiveCountType>{}));
                for (int binIdx = laneIdx; binIdx < binN; binIdx += Compute::SUBGROUP_SIZE) {
                    // Inclusive scan min bin part
                    PrimitiveCountType value = Compute::SubgroupScanLocal(taskCopyData.minBins[axis][binIdx], laneIdx, cuda::std::plus<PrimitiveCountType>{});

                    // Add previous sum
                    value += minSum;

                    // Shift right as we need exclusive scan
                    PrimitiveCountType minValue = __shfl_up_sync(Compute::FULL_MASK, value, 1);
                    if (laneIdx == 0) {
                        minValue = minSum;
                    }

                    // Distribute sum of this part
                    minSum = __shfl_sync(Compute::FULL_MASK, value, Compute::SUBGROUP_SIZE - 1);


                    // Similarly with max bin
                    value = Compute::SubgroupScanLocal(taskCopyData.maxBins[axis][binIdx], laneIdx, cuda::std::plus<PrimitiveCountType>{});
                    value += maxPartialSum;
                    // To create reversed inclusive scan, we need to subtract exclusive scan values from total sum
                    PrimitiveCountType maxValue = __shfl_up_sync(Compute::FULL_MASK, value, 1);
                    if (laneIdx == 0) {
                        maxValue = maxPartialSum;
                    }
                    maxValue = maxSum - maxValue;
                    maxPartialSum = __shfl_sync(Compute::FULL_MASK, value, Compute::SUBGROUP_SIZE - 1);

                    // Compute cost and take the minimum
                    cfloat cost = ComputeSAH(
                        partialSurface(laneIdx * binSize),
                        partialSurface((binN - laneIdx) * binSize),
                        parentSurface,
                        minValue,
                        maxValue,
                        traversalCost,
                        intersectionCost
                    );

                    assert(cost > 0._cf);

                    if (cost < minCost && binIdx > 0) {
                        minCost = cost;
                        minCoord = taskCopyData.boundingBox.minBounds[axis] + binIdx * binSize;
                        minAxis = static_cast<Axis3D>(axis);
                    }
                }
            }
        }

        // Compute min cost across all subgroups
        cfloat minCostGlobal = Compute::SubgroupReduceLocal(minCost, laneIdx, MinOperator<cfloat>{});
        auto leaderIdx = __ffs(__ballot_sync(Compute::FULL_MASK, minCostGlobal == minCost)) - 1;

        cuda::atomic_thread_fence(cuda::memory_order_release, cuda::thread_scope_device);

        #if PRT_TIME_KERNELS && !TIME_PROCESSING
        __threadfence_block();
        subTimer.Stop(Compute::Grid1DHelpers::GlobalThreadIdx());
        __threadfence_block();
        #endif

        if (laneIdx == leaderIdx) {
            int32 newWorkChunkCount = -1;
            auto& taskData = Base::SelectedTask()->customData;
            // Make this node a leaf instead
            if (minCost > intersectionCost * taskCopyData.primitiveN) {
                taskData.splitAxis = taskCopyData.splitAxis = Axis3D::None;

                Base::SelectedTask()->phase = Base::sharedParam->customData.taskCopy.phase = Phases::MakeLeaf;

                newWorkChunkCount = 1;
            }
            // Move onto classification
            else {
                taskData.splitCoord = taskCopyData.splitCoord = minCoord;
                taskData.splitAxis = taskCopyData.splitAxis = minAxis;

                taskData.arrayStride = taskCopyData.arrayStride = 1;
                taskData.stepN = taskCopyData.stepN = ParallelScan::StepCount(taskCopyData.primitiveN);
                
                Base::SelectedTask()->phase = Base::sharedParam->customData.taskCopy.phase = Phases::Classification;

                newWorkChunkCount = LinearComputation::WorkChunkCount(taskCopyData.primitiveN);
            }

            Base::UpdateTask(newWorkChunkCount);
        }
    }

    __device__
    void KDTBinsGPUBuilder::Processor::Classify() {
        auto& taskCopyData = sharedParam->customData.taskCopy.customData;

        for (int workChunkIdx = Base::sharedParam->taskInfo.workChunkFirst, workChunkLast = workChunkIdx + Base::sharedParam->taskInfo.workChunkN; workChunkIdx < workChunkLast; ++workChunkIdx) {
            int32 gid = LinearComputation::GlobalArrayIndex(Compute::Grid1DHelpers::LaneIdx(), workChunkIdx);
            if (gid < taskCopyData.primitiveN) {
                AABB primitiveBB = taskCopyData.PrimitiveBBs()[gid];
                taskCopyData.ClassificationLeft()[gid] = (primitiveBB.minBounds[ToUnderlying(taskCopyData.splitAxis)] < taskCopyData.splitCoord) ? 1 : 0;
                taskCopyData.ClassificationRight()[gid] = (taskCopyData.splitCoord < primitiveBB.maxBounds[ToUnderlying(taskCopyData.splitAxis)]) ? 1 : 0;
            }
        }
    }

    __device__
    void KDTBinsGPUBuilder::Processor::ClassificationScanPostProcess() {
        auto& taskCopy = sharedParam->customData.taskCopy;

        if (taskCopy.phase == Phases::ClassifyUpSweep) {
            // Increase step
            ++taskCopy.step;
            if (taskCopy.step < taskCopy.customData.stepN){
                taskCopy.customData.arrayStride *= Compute::SUBGROUP_SIZE;
            }
            else {
                // Move onto down sweep phase
                Base::SelectedTask()->phase = taskCopy.phase = Phases::ClassifyDownSweep;
                taskCopy.step -= 2;
                taskCopy.customData.arrayStride /= Compute::SUBGROUP_SIZE;
            }
        }
        else {
            // Decrease step
            --taskCopy.step;
            if (taskCopy.step >= 0) {
                taskCopy.customData.arrayStride /= Compute::SUBGROUP_SIZE;
            }
        }

        // Next step
        if (taskCopy.step >= 0) {
            // Write local changes to global memory
            Base::SelectedTask()->step = taskCopy.step;
            Base::SelectedTask()->customData.arrayStride = taskCopy.customData.arrayStride;

            // Compute and store new work chunk count for the next step
            Base::UpdateTask(ParallelScan::WorkChunkCount(taskCopy.customData.primitiveN, taskCopy.customData.arrayStride));
        }
        // Next phase
        else {
            // Save primitive counts
            Base::SelectedTask()->customData.primitiveLeftN = taskCopy.customData.primitiveLeftN = taskCopy.customData.ClassificationLeft()[taskCopy.customData.primitiveN - 1];
            Base::SelectedTask()->customData.primitiveRightN = taskCopy.customData.primitiveRightN = taskCopy.customData.ClassificationRight()[taskCopy.customData.primitiveN - 1];

            // Allocate child storage
            // Prevent zero length allocation by adding 1
            taskCopy.customData.primitivesRightStorage = primitivePool->SplitOrAllocateNew<splitStrategy, PUID>(
                taskCopy.customData.primitivesLeftStorage,
                allocationMultiplier,
                taskCopy.customData.primitiveLeftN + 1,
                taskCopy.customData.primitiveRightN + 1
            );
            taskCopy.customData.bbsRightStorage = dataPool->SplitOrAllocateNew<splitStrategy, AABB>(
                taskCopy.customData.bbsLeftStorage,
                allocationMultiplier,
                taskCopy.customData.primitiveLeftN + 1,
                taskCopy.customData.primitiveRightN + 1
            );
            Base::SelectedTask()->customData.primitivesLeftStorage = taskCopy.customData.primitivesLeftStorage;
            Base::SelectedTask()->customData.primitivesRightStorage = taskCopy.customData.primitivesRightStorage;
            Base::SelectedTask()->customData.bbsLeftStorage = taskCopy.customData.bbsLeftStorage;
            Base::SelectedTask()->customData.bbsRightStorage = taskCopy.customData.bbsRightStorage;

            // Update phase
            Base::SelectedTask()->phase = Phases::Distribution;

            Base::UpdateTask(LinearComputation::WorkChunkCount(taskCopy.customData.primitiveN));
        }
    }

    __device__
    void KDTBinsGPUBuilder::Processor::DistributeChunks() {
        auto& taskCopyData = sharedParam->customData.taskCopy.customData;

        for (int workChunkIdx = Base::sharedParam->taskInfo.workChunkFirst, workChunkLast = workChunkIdx + Base::sharedParam->taskInfo.workChunkN; workChunkIdx < workChunkLast; ++workChunkIdx) {
            int32 gid = LinearComputation::GlobalArrayIndex(Compute::Grid1DHelpers::LaneIdx(), workChunkIdx);;
            if (gid < taskCopyData.primitiveN) {
                bool distLeft = Distribute(gid, taskCopyData.ClassificationLeft(), taskCopyData.Primitives(), taskCopyData.PrimitivesLeft());
                bool distRight = Distribute(gid, taskCopyData.ClassificationRight(), taskCopyData.Primitives(), taskCopyData.PrimitivesRight());

                if (distLeft && distRight) {
                    #if PRT_SPLIT_CLIP
                    auto [leftBB, rightBB] = SplitClipBB(GetPrimitive(taskCopyData.Primitives()[gid]), taskCopyData.boundingBox, ToUnderlying(taskCopyData.splitAxis), taskCopyData.splitCoord);
                    if (!leftBB.IsValid() || !rightBB.IsValid()) {
                        if (!leftBB.IsValid()) {
                            leftBB = taskCopyData.PrimitiveBBs()[gid];
                            leftBB.maxBounds[ToUnderlying(taskCopyData.splitAxis)] = taskCopyData.splitCoord;
                        }
                        if (!rightBB.IsValid()) {
                            rightBB = taskCopyData.PrimitiveBBs()[gid];
                            rightBB.minBounds[ToUnderlying(taskCopyData.splitAxis)] = taskCopyData.splitCoord;
                        }
                    }
                    #else
                    AABB leftBB{ taskCopyData.PrimitiveBBs()[gid] }, rightBB{ leftBB };
                    leftBB.maxBounds[ToUnderlying(taskCopyData.splitAxis)] = taskCopyData.splitCoord;
                    rightBB.minBounds[ToUnderlying(taskCopyData.splitAxis)] = taskCopyData.splitCoord;
                    #endif

                    taskCopyData.PrimitiveBBsLeft()[taskCopyData.ClassificationLeft()[gid] - 1] = leftBB;
                    taskCopyData.PrimitiveBBsRight()[taskCopyData.ClassificationRight()[gid] - 1] = rightBB;
                }
                else if (distLeft) {
                    taskCopyData.PrimitiveBBsLeft()[taskCopyData.ClassificationLeft()[gid] - 1] = taskCopyData.PrimitiveBBs()[gid];
                }
                else {
                    taskCopyData.PrimitiveBBsRight()[taskCopyData.ClassificationRight()[gid] - 1] = taskCopyData.PrimitiveBBs()[gid];
                }
            }
        }
    }

    __device__
    bool KDTBinsGPUBuilder::Processor::Distribute(int32 gid, PrimitiveCountType* classification, PUID* primitives, PUID* childPrimitives) {
        auto classif = classification[gid];
        if (gid == 0) {
            if (classif == 1) {
                childPrimitives[0] = primitives[gid];
                return true;
            }
        }
        else if (classif != classification[gid - 1]) {
            childPrimitives[classif - 1] = primitives[gid];
            return true;
        }
        return false;
    }

    __device__
    void KDTBinsGPUBuilder::Processor::DistributionPostProcess() {
        #if PRT_TIME_KERNELS && !TIME_PROCESSING
        __threadfence_block();
        lockTimer.Start(Compute::Grid1DHelpers::GlobalThreadIdx());
        __threadfence_block();
        #endif

        auto [storeTaskPool, storeIndex] = Base::LockTask(dataPool);

        #if PRT_TIME_KERNELS && !TIME_PROCESSING
        __threadfence_block();
        lockTimer.Stop(Compute::Grid1DHelpers::GlobalThreadIdx());
        __threadfence_block();
        #endif

        #if PRT_TIME_KERNELS && !TIME_PROCESSING
        distributionTimer.Start(Compute::Grid1DHelpers::GlobalThreadIdx());
        __threadfence_block();
        #endif

        // Initialize bins back to zero
        auto& taskCopyData = sharedParam->customData.taskCopy.customData;
        for (int axis = 0; axis < 3; ++axis) {
            for (int i = Compute::Grid1DHelpers::LaneIdx(); i < binN; i += Compute::SUBGROUP_SIZE) {
                taskCopyData.minBins[axis][i] = 0;
                taskCopyData.maxBins[axis][i] = 0;
            }
        }

        PrimitiveCountType primitiveLeftN;
        MemoryPool::AllocatedMemory<PUID> primitivesPassedDownLeft;
        MemoryPool::AllocatedMemory<PUID> primitivesLeftStorage;
        MemoryPool::AllocatedMemory<AABB> bbsPassedDownLeft;
        MemoryPool::AllocatedMemory<AABB> bbsLeftStorage;
        MemoryPool::AllocatedMemory<PrimitiveCountType> classLeftStorage;
        Node* leftChild;
        AABB leftBB;
        int32 childWorkChunkCount;

        if (Compute::Grid1DHelpers::LaneIdx() == 0) {
            taskCopyData.depth += 1;

            // Create nodes
            auto subgroupIdx = Compute::Grid1DHelpers::GlobalSubgroupIdx();

            auto nodeCount = nodeListInfos[subgroupIdx];
            nodeListInfos[subgroupIdx] += 2;

            taskCopyData.node->inner = InnerNode{
                ToNodeType(taskCopyData.splitAxis),
                taskCopyData.splitCoord,
                NodeIdxPointer{
                    subgroupIdx,
                    nodeCount
                },
                NodeIdxPointer{
                    subgroupIdx,
                    nodeCount + 1
                }
            };

            leftChild = nodeLists[subgroupIdx].ReserveBack(dataPool);
            taskCopyData.node = nodeLists[subgroupIdx].ReserveBack(dataPool);

            // Create right task in local task copy and save left task values
            // Split node bounding box
            taskCopyData.boundingBox.SplitCoord(taskCopyData.splitAxis, taskCopyData.splitCoord, leftBB, taskCopyData.boundingBox);

            // Create storage
            if (LeafCondition(taskCopyData.primitiveLeftN, taskCopyData.depth)) {
                // Left does not need storage, right keeps all
                primitivesPassedDownLeft.data = nullptr;
                primitivesPassedDownLeft.size = 0;
                bbsPassedDownLeft.data = nullptr;
                bbsPassedDownLeft.size = 0;
                classLeftStorage.data = nullptr;
                classLeftStorage.size = 0;
            }
            else if (LeafCondition(taskCopyData.primitiveRightN, taskCopyData.depth)) {
                // Right does not need storage, left keeps all
                primitivesPassedDownLeft = taskCopyData.primitivesStorage;
                bbsPassedDownLeft = taskCopyData.bbsStorage;
                classLeftStorage = taskCopyData.classificationStorage;

                taskCopyData.primitivesStorage.data = nullptr;
                taskCopyData.primitivesStorage.size = 0;
                taskCopyData.bbsStorage.data = nullptr;
                taskCopyData.bbsStorage.size = 0;
                taskCopyData.classificationStorage.data = nullptr;
                taskCopyData.classificationStorage.size = 0;
            }
            else {
                // Allocate/split memory for passing down
                primitivesPassedDownLeft = primitivePool->SplitOrAllocateNew<splitStrategy, PUID>(
                    taskCopyData.primitivesStorage,
                    allocationMultiplier,
                    taskCopyData.primitiveRightN,
                    taskCopyData.primitiveLeftN
                );

                bbsPassedDownLeft = dataPool->SplitOrAllocateNew<splitStrategy, AABB>(
                    taskCopyData.bbsStorage,
                    allocationMultiplier,
                    taskCopyData.primitiveRightN,
                    taskCopyData.primitiveLeftN
                );

                classLeftStorage = dataPool->SplitOrAllocateNew<splitStrategy, PrimitiveCountType>(
                    taskCopyData.classificationStorage,
                    allocationMultiplier,
                    2 * taskCopyData.primitiveRightN,
                    2 * taskCopyData.primitiveLeftN
                );
            }

            // save left
            primitivesLeftStorage = taskCopyData.primitivesLeftStorage;
            bbsLeftStorage = taskCopyData.bbsLeftStorage;

            // pass down to left
            taskCopyData.PassDownStorage(taskCopyData.primitivesStorage, taskCopyData.bbsStorage);
            // set right child's storage as main
            taskCopyData.primitivesStorage = taskCopyData.primitivesRightStorage;
            taskCopyData.bbsStorage = taskCopyData.bbsRightStorage;

            // Primitive counts
            primitiveLeftN = taskCopyData.primitiveLeftN;
            taskCopyData.primitiveN = taskCopyData.primitiveRightN;

            // Set phase and chunks
            if (LeafCondition(taskCopyData.primitiveN, taskCopyData.depth)) {
                sharedParam->customData.taskCopy.phase = Phases::MakeLeaf;
                childWorkChunkCount = 1;
            }
            else {
                sharedParam->customData.taskCopy.phase = Phases::Binning;
                childWorkChunkCount = LinearComputation::WorkChunkCount(taskCopyData.primitiveN);

                taskCopyData.enabledAxes.EnableAll();

                // Disable flat axes
                for (int axis = 0; axis < 3; ++axis) {
                    cfloat binSize = taskCopyData.boundingBox.SideLength(static_cast<Axis3D>(axis)) / static_cast<cfloat>(binN);
                    if (binSize < ZERO_COMP_EPSILON) {
                        taskCopyData.enabledAxes.DisableAxis(static_cast<Axis3D>(axis));
                    }
                }
            }

            *reinterpret_cast<int32*>(&sharedParam->customData.taskCopy.workChunksLeft) = childWorkChunkCount;
            sharedParam->customData.taskCopy.step = 0;
        }

        // Copy right task saved in local copy to locked global task
        CopySharedTaskToGlobal(&storeTaskPool->tasks[storeIndex]);

        if (Compute::Grid1DHelpers::LaneIdx() == 0) {
            // Commit right task
            storeTaskPool->taskHeaders[storeIndex].store(childWorkChunkCount, cuda::memory_order_release);

            // Create left task in local task copy
            taskCopyData.primitivesStorage = primitivesLeftStorage;
            taskCopyData.bbsStorage = bbsLeftStorage;
            taskCopyData.PassDownStorage(primitivesPassedDownLeft, bbsPassedDownLeft);
            taskCopyData.classificationStorage = classLeftStorage;

            taskCopyData.node = leftChild;
            taskCopyData.primitiveN = primitiveLeftN;
            taskCopyData.boundingBox = leftBB;

            if (LeafCondition(taskCopyData.primitiveN, taskCopyData.depth)) {
                sharedParam->customData.taskCopy.phase = Phases::MakeLeaf;
                childWorkChunkCount = 1;
            }
            else {
                sharedParam->customData.taskCopy.phase = Phases::Binning;
                childWorkChunkCount = LinearComputation::WorkChunkCount(taskCopyData.primitiveN);

                taskCopyData.enabledAxes.EnableAll();

                // Disable flat axes
                for (int axis = 0; axis < 3; ++axis) {
                    cfloat binSize = taskCopyData.boundingBox.SideLength(static_cast<Axis3D>(axis)) / static_cast<cfloat>(binN);
                    if (binSize < ZERO_COMP_EPSILON) {
                        taskCopyData.enabledAxes.DisableAxis(static_cast<Axis3D>(axis));
                    }
                }
            }

            *reinterpret_cast<int32*>(&sharedParam->customData.taskCopy.workChunksLeft) = childWorkChunkCount;
            sharedParam->customData.taskCopy.step = 0;
        }

        // Copy left to global
        CopySharedTaskToGlobal(Base::SelectedTask());

        if (Compute::Grid1DHelpers::LaneIdx() == 0) {
            // Commit left
            Base::UpdateTask(childWorkChunkCount);
        }

        #if PRT_BUILD_STATUS
        if (Compute::Grid1DHelpers::LaneIdx() == 0) {
            buildStatusAtomic->tasks.fetch_add(1, cuda::memory_order_relaxed);
            buildStatusAtomic->depth.fetch_max(taskCopyData.depth, cuda::memory_order_relaxed);
            buildStatusAtomic->nodes.fetch_add(2, cuda::memory_order_relaxed);
        }
        #endif

        #if PRT_TIME_KERNELS && !TIME_PROCESSING
        __threadfence_block();
        distributionTimer.Stop(Compute::Grid1DHelpers::GlobalThreadIdx());
        #endif
    }

    __device__
    void KDTBinsGPUBuilder::Processor::CopySharedTaskToGlobal(GroupTask* globalTask) {
        __syncwarp();
        Compute::SubgroupMemCopyStruct(&(Base::sharedParam->customData.taskCopy), globalTask);
        __syncwarp();
        cuda::atomic_thread_fence(cuda::memory_order_release, cuda::thread_scope_device);
    }

    __device__
    void KDTBinsGPUBuilder::Processor::LoadSelectedTaskToShared() {
        cuda::atomic_thread_fence(cuda::memory_order_acquire, cuda::thread_scope_device);
        __syncwarp();
        Compute::SubgroupMemCopyStruct(Base::SelectedTask(), &(Base::sharedParam->customData.taskCopy));
        __syncwarp();
    }

    __device__
    KDTBinsGPUBuilder::BestSplit KDTBinsGPUBuilder::Processor::FindBestPlaneExact() {
        BestSplit bestSplit{};

        struct {
            int16 primitiveN;
            AABB* primitiveBBs;
            AABB boundingBox;
            union {
                typename SubgroupSort::TempStorage sortStorage;
                struct {
                    int16 scanStorage[LOCAL_COMPUTATION_EVENT_COUNT];
                    float coordinates[LOCAL_COMPUTATION_EVENT_COUNT];
                };
            };
        } item;

        struct EventCompare
        {
            AABB* primitiveBBs;
        };

        int16 laneIdx = Compute::Grid1DHelpers::LaneIdx<int16>();
        cfloat parentSurface{ item.boundingBox.SurfaceArea() };
        Event threadEvents[EVENTS_PER_THREAD];

        for (int axis = 0; axis < 3; ++axis) {
            // Create events
            for (int16 localIdx = 0; localIdx < LOCAL_PRIMITIVES_PER_THREAD; ++localIdx) {
                int16 primitiveIdx = laneIdx * LOCAL_PRIMITIVES_PER_THREAD + localIdx;
                threadEvents[2 * localIdx] = Event{
                    SplitEventType::Start,
                    primitiveIdx,
                    (primitiveIdx < item.primitiveN) ? (item.primitiveBBs[primitiveIdx].minBounds[axis]) : (cuda::std::numeric_limits<float>::infinity())
                };
                threadEvents[2 * localIdx + 1] = Event{
                    SplitEventType::End,
                    primitiveIdx,
                    (primitiveIdx < item.primitiveN) ? (item.primitiveBBs[primitiveIdx].maxBounds[axis]) : (cuda::std::numeric_limits<float>::infinity())
                };
            }

            // Sort events
            SubgroupSort{item.sortStorage}.Sort(threadEvents, [] (Event left, Event right) { return left.coordinate < right.coordinate; });
            __syncwarp();

            // Transform data to shared
            for (int16 localIdx = 0; localIdx < EVENTS_PER_THREAD; ++localIdx) {
                int16 eventIdx = laneIdx * EVENTS_PER_THREAD + localIdx;
                item.scanStorage[eventIdx] = ((threadEvents[localIdx].type == SplitEventType::Start) ? 1 : 0);
            }

            // Scan events
            Compute::SubgroupScanShared<int16,int16>(item.scanStorage, 0, item.primitiveN, laneIdx, cuda::std::plus<int16>{});

            Axis3D otherAxis1{ (axis + 1) % 3 }, otherAxis2{ (axis + 2) % 3 };
            PartialSurfaceArea leftPartial{ item.boundingBox.SideLength(otherAxis1), item.boundingBox.SideLength(otherAxis2) };
            PartialSurfaceArea rightPartial{ item.boundingBox.SideLength(otherAxis1), item.boundingBox.SideLength(otherAxis2) };
            
            // Compute costs in subgroup-sized chunks
            for (int i = laneIdx; i < 2 * item.primitiveN; i += Compute::SUBGROUP_SIZE) {
                if (i == 0 || item.coordinates[i] != item.coordinates[i-1]) {
                    // Make value exclusive
                    int16 leftPrimitiveN = ((i == 0) ? 0 : item.scanStorage[i - 1]);
                    // Get to correct right value
                    while (i + 1 < 2 * item.primitiveN && item.coordinates[i] == item.coordinates[i+1]) {
                        ++i;
                    }
                    // Compute right
                    int16 rightPrimitiveN = item.primitiveN - 1 - i + item.scanStorage[i];

                    // Compute cost and save if min
                    auto cost = ComputeSAH(
                        leftPartial(item.coordinates[i] - item.boundingBox.minBounds[axis]),
                        rightPartial(item.boundingBox.maxBounds[axis] - item.coordinates[i]),
                        parentSurface,
                        leftPrimitiveN,
                        rightPrimitiveN,
                        traversalCost,
                        intersectionCost
                    );

                    if (cost < bestSplit.minCost) {
                        bestSplit.plane.axis = static_cast<Axis3D>(axis);
                        bestSplit.plane.coordinate = item.coordinates[i];
                        bestSplit.minCost = cost;
                    }
                }
            }
        }

        return bestSplit;
    }

    __device__
    void KDTBinsGPUBuilder::Processor::ProcessTask() {
        #if PRT_TIME_KERNELS && TIME_PROCESSING
        auto tid = Compute::Grid1DHelpers::GlobalThreadIdx();
        #endif

        LoadSelectedTaskToShared();

        // Process based on phase
        switch (Base::sharedParam->customData.taskCopy.phase)
        {
        case Phases::Binning: {
            #if PRINT_STAGE
            if (Compute::Grid1DHelpers::LaneIdx() == 0) {
                printf("%d: Binning\n", Compute::Grid1DHelpers::GlobalSubgroupIdx());
            }
            #endif
            #if PRT_TIME_KERNELS && TIME_PROCESSING
            binTimer.Start(tid);
            __threadfence_block();
            #endif

            Bin();

            #if PRT_TIME_KERNELS && TIME_PROCESSING
            __threadfence_block();
            #if TIME_WORK_CHUNKS
            binTimer.Stop(tid, Base::sharedParam->taskInfo.workChunkN);
            #else
            binTimer.Stop(tid);
            #endif
            #endif
            #if PRINT_STAGE
            if (Compute::Grid1DHelpers::LaneIdx() == 0) {
                printf("%d: Binned\n", Compute::Grid1DHelpers::GlobalSubgroupIdx());
            }
            #endif

            break;
        }
        case Phases::Classification: {
            #if PRINT_STAGE
            if (Compute::Grid1DHelpers::LaneIdx() == 0) {
                printf("%d: Classifying\n", Compute::Grid1DHelpers::GlobalSubgroupIdx());
            }
            #endif
            #if PRT_TIME_KERNELS && TIME_PROCESSING
            classificationTimer.Start(tid);
            __threadfence_block();
            #endif

            Classify();

            #if PRT_TIME_KERNELS && TIME_PROCESSING
            __threadfence_block();
            #if TIME_WORK_CHUNKS
            classificationTimer.Stop(tid, Base::sharedParam->taskInfo.workChunkN);
            #else
            classificationTimer.Stop(tid);
            #endif
            #endif
            #if PRINT_STAGE
            if (Compute::Grid1DHelpers::LaneIdx() == 0) {
                printf("%d: Classified\n", Compute::Grid1DHelpers::GlobalSubgroupIdx());
            }
            #endif

            break;
        }
        case Phases::ClassifyUpSweep: {
            #if PRINT_STAGE
            if (Compute::Grid1DHelpers::LaneIdx() == 0) {
                printf("%d: Upsweeping\n", Compute::Grid1DHelpers::GlobalSubgroupIdx());
            }
            #endif
            #if PRT_TIME_KERNELS && TIME_PROCESSING
            classificationTimer.Start(tid);
            __threadfence_block();
            #endif

            auto& taskCopyData = sharedParam->customData.taskCopy.customData;

            ParallelScan::ProcessUpSweep(
                taskCopyData.ClassificationLeft(),
                taskCopyData.primitiveN,
                Base::sharedParam->taskInfo.workChunkFirst,
                Base::sharedParam->taskInfo.workChunkFirst + Base::sharedParam->taskInfo.workChunkN,
                taskCopyData.arrayStride,
                Compute::Grid1DHelpers::LaneIdx()
            );
            ParallelScan::ProcessUpSweep(
                taskCopyData.ClassificationRight(),
                taskCopyData.primitiveN,
                Base::sharedParam->taskInfo.workChunkFirst,
                Base::sharedParam->taskInfo.workChunkFirst + Base::sharedParam->taskInfo.workChunkN,
                taskCopyData.arrayStride,
                Compute::Grid1DHelpers::LaneIdx()
            );

            #if PRT_TIME_KERNELS && TIME_PROCESSING
            __threadfence_block();
            #if TIME_WORK_CHUNKS
            classificationTimer.Stop(tid, Base::sharedParam->taskInfo.workChunkN);
            #else
            classificationTimer.Stop(tid);
            #endif
            #endif
            #if PRINT_STAGE
            if (Compute::Grid1DHelpers::LaneIdx() == 0) {
                printf("%d: Upsweeped\n", Compute::Grid1DHelpers::GlobalSubgroupIdx());
            }
            #endif

            break;
        }
        case Phases::ClassifyDownSweep: {
            #if PRINT_STAGE
            if (Compute::Grid1DHelpers::LaneIdx() == 0) {
                printf("%d: Downsweeping\n", Compute::Grid1DHelpers::GlobalSubgroupIdx());
            }
            #endif
            #if PRT_TIME_KERNELS && TIME_PROCESSING
            classificationTimer.Start(tid);
            __threadfence_block();
            #endif

            auto& taskCopyData = sharedParam->customData.taskCopy.customData;

            ParallelScan::ProcessDownSweep(
                taskCopyData.ClassificationLeft(),
                taskCopyData.primitiveN,
                Base::sharedParam->taskInfo.workChunkFirst,
                Base::sharedParam->taskInfo.workChunkFirst + Base::sharedParam->taskInfo.workChunkN,
                taskCopyData.arrayStride,
                Compute::Grid1DHelpers::LaneIdx()
            );
            ParallelScan::ProcessDownSweep(
                taskCopyData.ClassificationRight(),
                taskCopyData.primitiveN,
                Base::sharedParam->taskInfo.workChunkFirst,
                Base::sharedParam->taskInfo.workChunkFirst + Base::sharedParam->taskInfo.workChunkN,
                taskCopyData.arrayStride,
                Compute::Grid1DHelpers::LaneIdx()
            );

            #if PRT_TIME_KERNELS && TIME_PROCESSING
            __threadfence_block();
            #if TIME_WORK_CHUNKS
            classificationTimer.Stop(tid, Base::sharedParam->taskInfo.workChunkN);
            #else
            classificationTimer.Stop(tid);
            #endif
            #endif
            #if PRINT_STAGE
            if (Compute::Grid1DHelpers::LaneIdx() == 0) {
                printf("%d: Downsweeped\n", Compute::Grid1DHelpers::GlobalSubgroupIdx());
            }
            #endif

            break;
        }
        case Phases::Distribution: {
            #if PRINT_STAGE
            if (Compute::Grid1DHelpers::LaneIdx() == 0) {
                printf("%d: Distributing\n", Compute::Grid1DHelpers::GlobalSubgroupIdx());
            }
            #endif
            #if PRT_TIME_KERNELS && TIME_PROCESSING
            distributionTimer.Start(tid);
            __threadfence_block();
            #endif

            DistributeChunks();

            #if PRT_TIME_KERNELS && TIME_PROCESSING
            __threadfence_block();
            #if TIME_WORK_CHUNKS
            distributionTimer.Stop(tid, Base::sharedParam->taskInfo.workChunkN);
            #else
            distributionTimer.Stop(tid);
            #endif
            #endif
            #if PRINT_STAGE
            if (Compute::Grid1DHelpers::LaneIdx() == 0) {
                printf("%d: Distributed\n", Compute::Grid1DHelpers::GlobalSubgroupIdx());
            }
            #endif

            break;
        }
        case Phases::MakeLeaf: {
            #if PRINT_STAGE
            if (Compute::Grid1DHelpers::LaneIdx() == 0) {
                printf("%d: Making leaf\n", Compute::Grid1DHelpers::GlobalSubgroupIdx());
            }
            #endif
            // Empty
        }
        default:
            break;
        }
    }

    __device__
    void KDTBinsGPUBuilder::Processor::TaskPostProcess() {
        #if PRT_TIME_KERNELS && !TIME_PROCESSING
        auto tid = Compute::Grid1DHelpers::GlobalThreadIdx();
        #endif

        cuda::atomic_thread_fence(cuda::memory_order_seq_cst);
        #if PRT_BUILD_STATUS
        if (Compute::Grid1DHelpers::GlobalThreadIdx() == 0) {
            //buildStatusAtomic->workingSubgroups.exchange(workingSubgroups->load(cuda::memory_order_relaxed), cuda::memory_order_relaxed);
            BuildStatus status;
            status.depth = buildStatusAtomic->depth.load(cuda::memory_order_relaxed);
            status.nodes = buildStatusAtomic->nodes.load(cuda::memory_order_relaxed);
            status.tasks = buildStatusAtomic->tasks.load(cuda::memory_order_relaxed);
            status.workingSubgroups = workingSubgroups->load(cuda::memory_order_relaxed);
            *buildStatusHost = status;
        }
        #endif

        // Process based on phase
        switch (Base::sharedParam->customData.taskCopy.phase)
        {
        case Phases::Binning: {
            #if PRINT_POST_STAGE
            if (Compute::Grid1DHelpers::LaneIdx() == 0) {
                printf("%d: Binning\n", Compute::Grid1DHelpers::GlobalSubgroupIdx());
            }
            #endif
            #if PRT_TIME_KERNELS && !TIME_PROCESSING
            binTimer.Start(tid);
            __threadfence_block();
            #endif

            BinPostProcess();

            #if PRT_TIME_KERNELS && !TIME_PROCESSING
            __threadfence_block();
            binTimer.Stop(tid);
            #endif
            break;
        }
        case Phases::Classification: {
            #if PRINT_POST_STAGE
            if (Compute::Grid1DHelpers::LaneIdx() == 0) {
                printf("%d: Classification\n", Compute::Grid1DHelpers::GlobalSubgroupIdx());
            }
            #endif
            #if PRT_TIME_KERNELS && !TIME_PROCESSING
            classificationTimer.Start(tid);
            __threadfence_block();
            #endif

            if (Compute::Grid1DHelpers::LaneIdx() == 0) {
                Base::sharedParam->customData.taskCopy.phase = Phases::ClassifyUpSweep;
                Base::SelectedTask()->phase = Phases::ClassifyUpSweep;
                Base::UpdateTask(ParallelScan::WorkChunkCount(Base::sharedParam->customData.taskCopy.customData.primitiveN, 1));
            }

            #if PRT_TIME_KERNELS && !TIME_PROCESSING
            __threadfence_block();
            classificationTimer.Stop(tid);
            #endif

            break;
        }
        case Phases::ClassifyUpSweep:
        case Phases::ClassifyDownSweep: {
            #if PRINT_POST_STAGE
            if (Compute::Grid1DHelpers::LaneIdx() == 0) {
                printf("%d: Classify sweep\n", Compute::Grid1DHelpers::GlobalSubgroupIdx());
            }
            #endif
            #if PRT_TIME_KERNELS && !TIME_PROCESSING
            classificationTimer.Start(tid);
            __threadfence_block();
            #endif

            if (Compute::Grid1DHelpers::LaneIdx() == 0) {
                ClassificationScanPostProcess();
            }

            #if PRT_TIME_KERNELS && !TIME_PROCESSING
            __threadfence_block();
            classificationTimer.Stop(tid);
            #endif

            break;
        }
        case Phases::Distribution: {
            #if PRINT_POST_STAGE
            if (Compute::Grid1DHelpers::LaneIdx() == 0) {
                printf("%d: Distributing\n", Compute::Grid1DHelpers::GlobalSubgroupIdx());
            }
            #endif
            DistributionPostProcess();
            break;
        }
        case Phases::MakeLeaf: {
            #if PRINT_POST_STAGE
            if (Compute::Grid1DHelpers::LaneIdx() == 0) {
                printf("%d: Make leaf\n", Compute::Grid1DHelpers::GlobalSubgroupIdx());
            }
            #endif
            #if PRT_TIME_KERNELS && !TIME_PROCESSING
            leafTimer.Start(tid);
            __threadfence_block();
            #endif

            if (Compute::Grid1DHelpers::LaneIdx() == 0) {
                Base::sharedParam->customData.taskCopy.customData.node->leaf = LeafNode{
                    KDTNodeType::Leaf,
                    Base::sharedParam->customData.taskCopy.customData.primitiveN,
                    Base::sharedParam->customData.taskCopy.customData.Primitives()
                };

                #if PRT_COUNT_MEMPOOL_ALLOCATIONS
                primitivePool->usedSpace.fetch_add(Base::sharedParam->customData.taskCopy.customData.primitiveN * sizeof(PUID), cuda::memory_order_relaxed);
                #endif

                #if PRT_BUILD_STATUS
                if (Compute::Grid1DHelpers::LaneIdx() == 0) {
                    buildStatusAtomic->tasks.fetch_sub(1, cuda::memory_order_relaxed);
                }
                #endif

                Base::UnlockTask();
            }

            #if PRT_TIME_KERNELS && !TIME_PROCESSING
            __threadfence_block();
            leafTimer.Stop(tid);
            #endif
        }
        default:
            break;
        }
    }
}