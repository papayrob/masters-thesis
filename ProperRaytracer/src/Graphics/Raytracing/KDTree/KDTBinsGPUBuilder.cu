/**
 * File name: KDTBinsGPUBuilder.cu
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "KDTBinsGPUBuilder.cuh"

#include <thrust/scan.h>
#include <thrust/device_ptr.h>
#include <thrust/execution_policy.h>

#include "Geometry/Primitive.hpp"
#include "RTApp.hpp"

//#define WHICH_TIME GetAverageTimeString GetAccumulatedString GetElapsedString
#define WHICH_TIME GetAverageTimeString

namespace ProperRT
{
    KDTBinsGPUBuilder::KDTBinsGPUBuilder(UniqueMemoryPool& dataMemPool_)
        : primitiveMemPool{ToBytesPrefixAs<MemoryPool::SizeType>(PrefixedValue{MetricPrefix::Giga, Scene::GetActiveScene()->Config().GetValue<double>("primitiveMemoryPoolCapacity", 1.)}, MetricPrefix::None)},
          dataMemPool{dataMemPool_},
          processorParams{
            Scene::GetActiveScene()->Config().GetValue("chunks", 32),
            Scene::GetActiveScene()->Config().GetValue("poolMult", 8),
            0,
            Scene::GetActiveScene()->Config().GetValue("kdTreeSubgroupCount", 8)
          }
    {
        if (int32 value; Scene::GetActiveScene()->Config().TryGetValue("kdTreeGroupCount", value)) {
            processorParams.groupN = value;
        }
        else {
            int devID;
            Compute::CudaCheckError(cudaGetDevice(&devID));
            int sm;
            Compute::CudaCheckError(cudaDeviceGetAttribute(&sm, cudaDeviceAttr::cudaDevAttrMultiProcessorCount, devID));
            processorParams.groupN = 4 * sm;
        }

        auto& stats = RTApp::Instance()->statistics;
        stats.AddInfo("Bins", binN);
        stats.AddInfo("Chunks", processorParams.chunksToRetrieve);
        stats.AddInfo("Groups", processorParams.groupN);
        stats.AddInfo("Subgroups", processorParams.subgroupsPerGroupN);
        
        nodeListsGPU = DeviceUniquePointer<DeviceLinkedListHead<Node>[]>::Allocate(NodeListSize());
        nodeListInfosGPU = DeviceUniquePointer<int32[]>::Allocate(NodeListSize());

        processor.nodeLists = nodeListsGPU.Get();
        processor.nodeListInfos = nodeListInfosGPU.Get();

        processor.primitivePool = primitiveMemPool.GetGPU();
        processor.dataPool = dataMemPool.GetGPU();

        // Change cuda limits
		size_t limit;
        cudaDeviceGetLimit(&limit, cudaLimit::cudaLimitStackSize);
        Logger::LogInfo(std::string{ "CUDA original stack size: " } + std::to_string(limit));
        cudaDeviceSetLimit(cudaLimit::cudaLimitStackSize, ProperRT::ToBytesPrefixAs<MemoryPool::SizeType>(PrefixedValue{MetricPrefix::Kilo, 4}, MetricPrefix::None));
        cudaDeviceGetLimit(&limit, cudaLimit::cudaLimitStackSize);
        Logger::LogInfo(std::string{ "CUDA new stack size: " } + std::to_string(limit));

        cudaDeviceGetLimit(&limit, cudaLimit::cudaLimitMallocHeapSize);
        Logger::LogInfo(std::string{ "CUDA original heap size: " } + std::to_string(limit));
        cudaDeviceSetLimit(cudaLimit::cudaLimitMallocHeapSize, ProperRT::ToBytesPrefixAs<MemoryPool::SizeType>(PrefixedValue{MetricPrefix::Mega, 50.0}, MetricPrefix::None));
        cudaDeviceGetLimit(&limit, cudaLimit::cudaLimitMallocHeapSize);
        Logger::LogInfo(std::string{ "CUDA new heap size: " } + std::to_string(limit));

        #if PRT_BUILD_STATUS
        cudaHostAlloc(&buildStatusHost, sizeof(BuildStatus), cudaHostAllocMapped);
        cudaHostGetDevicePointer(&processor.buildStatusHost.data, buildStatusHost, 0);
        buildStatusAtomic = DeviceUniquePointer<BuildStatusAtomic>::Allocate();
        processor.buildStatusAtomic = buildStatusAtomic.Get();
        cudaEventCreate(&stop);
        #endif
    }

    KDTBinsGPUBuilder::~KDTBinsGPUBuilder() {
        taskPoolState.taskPool = nullptr;
        Logger::LogTest(std::string{ "Allocated task pools: " } + std::to_string(*taskPoolState.taskPoolDeleter.allocatedPoolN));
    }

    void KDTBinsGPUBuilder::Build(GPUMeshManager& meshManager, DeviceUniquePointer<PUID[]> primitives, DeviceUniquePointer<AABB[]> primitiveBBs, KDTreeParams const& parameters) {
        Reset();
        if (primitives.Count() == 0) {
            nodes = DeviceUniquePointer<KDTreeGPU::Node[]>::AllocateAndUpload(std::vector<KDTreeGPU::Node>{KDTreeGPU::Node{.leaf{ KDTNodeType::Leaf, 0, nullptr }}});
            isBuilt = true;
            return;
        }

        processor.traversalCost = parameters.traversalCost;
        processor.intersectionCost = parameters.intersectionCost;
        processor.primitiveLimit = parameters.primitiveLimit;
        processor.maxDepth = MaxDepth(primitives.Count(), parameters);

        nodeLists.clear();
        int size = NodeListSize();
        nodeLists.reserve(size);
        for (int i = 0; i < size; ++i) {
            nodeLists.push_back(DeviceLinkedListUniqueHead<Node, DeviceLinkedListDeleters::MemPoolDeleter<Node>>{ Compute::SUBGROUP_SIZE * 64 }); //TODO magic number
        }
 
        nodeListsGPU.Upload(std::vector<DeviceLinkedListHead<Node>>(nodeLists.begin(), nodeLists.end()));
        nodeListInfosGPU.Upload(std::vector<int32>(size, 0));

        processor.instances = meshManager.Instances();

        primitiveMemPool.GetCPU().Reset();

        #if PRT_TIME_KERNELS
        binTimer.SetThreadCount(processorParams.groupN * processorParams.subgroupsPerGroupN * Compute::SUBGROUP_SIZE);
        classificationTimer.SetThreadCount(processorParams.groupN * processorParams.subgroupsPerGroupN * Compute::SUBGROUP_SIZE);
        distributionTimer.SetThreadCount(processorParams.groupN * processorParams.subgroupsPerGroupN * Compute::SUBGROUP_SIZE);
        leafTimer.SetThreadCount(processorParams.groupN * processorParams.subgroupsPerGroupN * Compute::SUBGROUP_SIZE);
        lockTimer.SetThreadCount(processorParams.groupN * processorParams.subgroupsPerGroupN * Compute::SUBGROUP_SIZE);
        subTimer.SetThreadCount(processorParams.groupN * processorParams.subgroupsPerGroupN * Compute::SUBGROUP_SIZE);
        processor.binTimer = binTimer.Get();
        processor.classificationTimer = classificationTimer.Get();
        processor.distributionTimer = distributionTimer.Get();
        processor.leafTimer = leafTimer.Get();
        processor.lockTimer = lockTimer.Get();
        processor.subTimer = subTimer.Get();
        #endif

        #if PRT_BUILD_STATUS
        buildStatusHost->depth = 0;
        buildStatusHost->nodes = 0;
        buildStatusHost->tasks = 0;
        buildStatusHost->workingSubgroups = 0;
        buildStatusAtomic.Upload(BuildStatusAtomic{});
        #endif

        processor.RunKernel(*this, primitives.Get(), primitiveBBs.Get(), meshManager.SceneBoundingBox(), processorParams);

        #if PRT_BUILD_STATUS
        cudaEventRecord(stop);
        #endif

        DistributeNodes();
    }

    int64 KDTBinsGPUBuilder::AllocatedMemory() const {
        auto nodeListsCPU = nodeListsGPU.Download();

        return
            taskPoolState.AllocatedMemory(processor.taskPoolSize) +
            nodes.Bytes() +
            std::span{nodeLists}.size_bytes() +
            std::transform_reduce(
                nodeListsCPU.begin(), nodeListsCPU.end(), int64{0},
                std::plus<int64>{}, [] (DeviceLinkedListHead<Node> const& head) { return head.TotalSize() * static_cast<int64>(sizeof(Node)); }
            ) +
            nodeListsGPU.Bytes() +
            nodeListInfosGPU.Bytes() +
            primitiveMemPool.GetGPU().Download().AllocatedSize();

    }

    void KDTBinsGPUBuilder::Reset() {
        nodesCPU.clear();
    }

    namespace
    {
        __device__
        KDTreeGPU::Node BuildNodeToTreeNode(KDTBinsGPUBuilder::Node buildNode, DevicePointer<int32[]> listIdxOffsets) {
            KDTreeGPU::Node treeNode{};
            if (buildNode.IsInner()) {
                treeNode.inner = KDTreeGPU::InnerNode{
                    buildNode.Tag(),
                    buildNode.inner.splitValue,
                    listIdxOffsets[buildNode.inner.leftChild.listIdx] + buildNode.inner.leftChild.offset,
                    listIdxOffsets[buildNode.inner.rightChild.listIdx] + buildNode.inner.rightChild.offset
                };
            }
            else {
                treeNode.leaf = KDTreeGPU::LeafNode{
                    buildNode.Tag(),
                    buildNode.leaf.primitiveN,
                    buildNode.leaf.primitives
                };
            }
            return treeNode;
        }
    }

    namespace Kernels
    {
        __global__
        void DistributeNodes(DevicePointer<DeviceLinkedListHead<KDTBinsGPUBuilder::Node>[]> buildNodes, DevicePointer<int32[]> startIndeces, DevicePointer<KDTreeGPU::Node[]> treeNodes) {
            auto subgroupIdx = Compute::Grid1DHelpers::GlobalSubgroupIdx();
            auto laneIdx = Compute::Grid1DHelpers::LaneIdx();
            auto insertIdx = startIndeces[subgroupIdx];
            
            // Copy nodes from first list (capacity initialized with SUBGROUP_SIZE)
            // Go through the first list, assuming the capacity is a multiple of subgroup size
            auto listSize = buildNodes[subgroupIdx].first->count;
            auto listData = buildNodes[subgroupIdx].first->data;
            for (int32 readIdx = 0; readIdx < listSize; readIdx += Compute::SUBGROUP_SIZE, insertIdx += Compute::SUBGROUP_SIZE) {
                // Last list's count might not be a multiple of subgroup size
                if (readIdx + laneIdx < listSize) {
                    treeNodes[insertIdx + laneIdx] = BuildNodeToTreeNode(listData[readIdx + laneIdx], startIndeces);
                }
            }

            // Start iterating from second list
            DeviceLinkedList<KDTBinsGPUBuilder::Node>* list = buildNodes[subgroupIdx].first->next;
            // Go through all lists
            while (list != nullptr) {
                listSize = list->count;
                listData = list->data;
                // Go through the current list, assuming the capacity is a multiple of subgroup size
                for (int32 readIdx = 0; readIdx < listSize; readIdx += Compute::SUBGROUP_SIZE, insertIdx += Compute::SUBGROUP_SIZE) {
                    // Last list's count might not be a multiple of subgroup size
                    if (readIdx + laneIdx < listSize) {
                        treeNodes[insertIdx + laneIdx] = BuildNodeToTreeNode(listData[readIdx + laneIdx], startIndeces);
                    }
                }

                DeviceLinkedList<KDTBinsGPUBuilder::Node>* next = list->next;
                // if (Compute::Grid1DHelpers::LaneIdx() == 0) {
                //     delete list->data;
                //     delete list;
                // }
                list = next;
            }

            buildNodes[subgroupIdx].first->next = nullptr;
            buildNodes[subgroupIdx].first->count = 0;
            buildNodes[subgroupIdx].last = buildNodes[subgroupIdx].first;
        }
    }

    void KDTBinsGPUBuilder::DistributeNodes() {
        auto thrustDevPtr = thrust::device_pointer_cast(processor.nodeListInfos.data);
        Compute::CudaCheckError(cudaDeviceSynchronize());

        Logger::LogTest(std::string{ "k-d task pool finished in " } + taskPoolState.runTimer.GetElapsedString(Timer::Precision::Milliseconds));
        RTApp::Instance()->statistics.AddData("TaskPoolTime", taskPoolState.runTimer.Elapsed());

        #if PRT_COUNT_MEMPOOL_ALLOCATIONS
        MemoryPool memPool = primitiveMemPool.GetGPU().Download();
        int64 emptySpace = memPool.AllocatedSize() - memPool.usedSpace.load(cuda::memory_order_relaxed);
        Logger::LogTest(std::string{ "k-d gpu primitive memory pool allocations: " } + std::to_string(memPool.allocations.load(cuda::memory_order_relaxed)));
        Logger::LogTest(std::string{ "k-d gpu primitive memory pool splits: " } + std::to_string(memPool.splits.load(cuda::memory_order_relaxed)));
        Logger::LogTest(std::string{ "k-d gpu primitive memory pool empty space: " } + std::to_string(emptySpace));
        RTApp::Instance()->statistics.AddData("PrimitiveMemPoolAllocs", memPool.allocations.load(cuda::memory_order_relaxed));
        RTApp::Instance()->statistics.AddData("PrimitiveMemPoolSplits", memPool.splits.load(cuda::memory_order_relaxed));
        RTApp::Instance()->statistics.AddData("PrimitiveMemPoolEmpty", emptySpace);

        // memPool = dataMemoryPoolGPU.Download();
        // Logger::LogTest(std::string{ "k-d gpu data memory pool allocations: " } + std::to_string(memPool.allocations.load(cuda::memory_order_relaxed)));
        // Logger::LogTest(std::string{ "k-d gpu data memory pool splits: " } + std::to_string(memPool.splits.load(cuda::memory_order_relaxed)));
        // Logger::LogTest(std::string{ "k-d gpu data memory pool empty space: " } + std::to_string(memPool.emptySpace.load(cuda::memory_order_relaxed)));
        // RTApp::Instance()->statistics.AddData("DataMemPoolAllocs", memPool.allocations.load(cuda::memory_order_relaxed));
        // RTApp::Instance()->statistics.AddData("DataMemPoolSplits", memPool.splits.load(cuda::memory_order_relaxed));
        // RTApp::Instance()->statistics.AddData("DataMemPoolEmpty", memPool.emptySpace.load(cuda::memory_order_relaxed));
        #endif

        // Download last node count as it gets lost in exclusive scan
        int32 nodeN = thrustDevPtr[processor.nodeListInfos.count - 1];// DevicePointer<int32>{ processor.nodeListInfos.data + processor.nodeListInfos.count - 1 }.Download();

        // Exclusive scan -> start indices
        thrust::exclusive_scan(thrust::device, thrustDevPtr, thrustDevPtr + processor.nodeListInfos.count, thrustDevPtr, 0);

        // Add the sum of the rest
        nodeN += thrustDevPtr[processor.nodeListInfos.count - 1];

        Logger::LogInfo("Node count: " + std::to_string(nodeN));
        
        nodes = DeviceUniquePointer<KDTreeGPU::Node[]>::Allocate(nodeN);
        Kernels::DistributeNodes<<<processorParams.groupN, processorParams.subgroupsPerGroupN * Compute::SUBGROUP_SIZE>>>(processor.nodeLists, processor.nodeListInfos, nodes.Get());

        #ifdef PRT_DEBUG
        Compute::CudaCheckError(cudaDeviceSynchronize());
        #endif
        Processor::Base::AtomicInt n;
        taskPoolState.totalRetrievals.Download(&n);
        Logger::LogTest(std::string{ "Total number of retrievals: " } + std::to_string(n.load()));
        RTApp::Instance()->statistics.AddData("Retrievals", n.load());
        taskPoolState.totalSkippedRetrievals.Download(&n);
        Logger::LogTest(std::string{ "Total number of skipped retrievals: " } + std::to_string(n.load()));
        RTApp::Instance()->statistics.AddData("SkippedRetrievals", n.load());

        isBuilt = true;
    }
}
