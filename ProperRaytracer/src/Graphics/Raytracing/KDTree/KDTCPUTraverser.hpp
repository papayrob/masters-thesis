/**
 * File name: KDTCPUTraverser.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "KDTreeCPU.hpp"
#include "KDTCPUBuilderBase.hpp"

namespace ProperRT
{
    class KDTCPUTraverser
    {
    public:
        using Node = KDTreeCPU::Node;
        using InnerNode = KDTreeCPU::InnerNode;
        using LeafNode = KDTreeCPU::LeafNode;

    public:
        /// @brief Standard ctor.
        /// @param meshManager Mesh manager the tree was built over
        /// @param nodes Tree nodes
        /// @param rootIdx Index of the root node
        /// @param stackSize Stack capacity (unused)
        explicit
        KDTCPUTraverser(IMeshManager const& meshManager, std::span<KDTNode const> nodes, int32 rootIdx, int32 stackSize);

        /// @brief Traverses the nodes using a stack-based traversal algorithm.
        /// @param ray Ray to traverse with
        /// @param maxDist Search only for intersections closer then this distance
        /// @param traversalStats Statistics counters
        /// @return Optional with a ray hit structure, empty if no intersection was found, otherwise contains the closest hit
        std::optional<RayHit> Traverse(Ray const& ray, cfloat maxDist, TraversalStatistics& traversalStats);

        /// @brief Traverses the nodes using a stack-based traversal algorithm for shadow rays.
        /// @param ray Ray to traverse with
        /// @param maxDist Search only for intersections closer then this distance
        /// @param traversalStats Statistics counters
        /// @return True if any intersection was found
        bool TraverseShadow(Ray const& ray, cfloat maxDist, TraversalStatistics& traversalStats);

    private:
        enum class RayType {
            Normal, Shadow
        };

        struct StackItem
        {
            Node const* node;
            cfloat tMin, tMax;
        };

        struct HitInfo
        {
            cfloat tHit;
            IntersectionCoordinates ic;
            PUID hitPrimitiveID;
        };

    private:
        IMeshManager const& meshManager;
        std::span<Node const> nodes;
        int32 rootIdx;
        int32 stackSize;

        template<RayType VRayType>
        void LeafIntersect(LeafNode const& leaf, Ray const& ray, TraversalStatistics& traversalStats, cfloat& tHit, IntersectionCoordinates& ic, PUID& hitPrimitiveID);

        template<RayType VRayType>
        HitInfo TraverseInternal(Ray const &ray, cfloat maxDist, TraversalStatistics& traversalStats);
    };
}
