/**
 * File name: KDTGPURaytracer.cu
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "KDTGPURaytracer.hpp"

#include "PrimaryRayGenerator.hpp"
#include "KDTree/KDTGPUBuilderBase.cuh"
#include "KDTree/KDTGPUTraverser.cuh"
#include "RTApp.hpp"
#include "KDTree/KDTreeGPU.hpp"
#include "KDTree/KDMergeTreeGPU.hpp"

namespace ProperRT
{
    struct KDTGPURaytracer::GPUData
    {
        DeviceUniquePointer<TraversalStatistics[]> traversalStats{};
    };

    struct RaytracingData
    {
        cudaSurfaceObject_t colorBuffer;
        Color32f clearValue;
        PrimaryRayGenerator primaryRayGen;
    };

    struct ShadingData
    {
        DevicePointer<Components::Light::GPUData[]> lights;
    };

    __device__
    cuda::std::optional<KDTGPUTraverser::RayHitGPU> TraverseBoth(
        KDTGPUTraverser const& staticTraverser, KDTGPUTraverser const& dynamicTraverser, KDTGPUTraverser::StackItem* stack, int32 stackSize, DevicePointer<MemoryPool> memPool,
        Ray const& ray, cfloat maxDist, TraversalStatistics& traversalStats
    ) {
        ++(traversalStats.queryCount);
        auto staticHit = staticTraverser.Traverse(ray, maxDist, stack, stackSize, memPool, traversalStats);
        auto dynamicHit = dynamicTraverser.Traverse(ray, maxDist, stack, stackSize, memPool, traversalStats);

        if (!staticHit.has_value()) {
            return dynamicHit;
        }
        else if (staticHit.has_value() && dynamicHit.has_value()) {
            return (staticHit->tHit < dynamicHit->tHit ? staticHit : dynamicHit);
        }
        else {
            return staticHit;
        }
    }

    template<KDTGPUTraverser::RestartStrategy VRestartStrat>
    __device__
    cuda::std::optional<KDTGPUTraverser::RayHitGPU> TraverseBoth(
        KDTGPUTraverser const& staticTraverser, KDTGPUTraverser const& dynamicTraverser,
        Ray const& ray, cfloat maxDist, TraversalStatistics& traversalStats
    ) {
        ++(traversalStats.queryCount);
        auto staticHit = staticTraverser.Traverse<VRestartStrat>(ray, maxDist, traversalStats);
        auto dynamicHit = dynamicTraverser.Traverse<VRestartStrat>(ray, maxDist, traversalStats);

        if (!staticHit.has_value()) {
            return dynamicHit;
        }
        else if (staticHit.has_value() && dynamicHit.has_value()) {
            return (staticHit->tHit < dynamicHit->tHit ? staticHit : dynamicHit);
        }
        else {
            return staticHit;
        }
    }

    template<KDTGPUTraverser::RestartStrategy VRestartStrat>
    __device__
    cuda::std::optional<KDTGPUTraverser::RayHitGPU> TraverseBoth(
        KDTGPUTraverser const& staticTraverser, KDTGPUTraverser const& dynamicTraverser, KDTGPUTraverser::StackItem* stack, int8 stackSize,
        Ray const& ray, cfloat maxDist, TraversalStatistics& traversalStats
    ) {
        ++(traversalStats.queryCount);
        auto staticHit = staticTraverser.Traverse<VRestartStrat>(ray, maxDist, stack, stackSize, traversalStats);
        auto dynamicHit = dynamicTraverser.Traverse<VRestartStrat>(ray, maxDist, stack, stackSize, traversalStats);

        if (!staticHit.has_value()) {
            return dynamicHit;
        }
        else if (staticHit.has_value() && dynamicHit.has_value()) {
            return (staticHit->tHit < dynamicHit->tHit ? staticHit : dynamicHit);
        }
        else {
            return staticHit;
        }
    }

    __device__
    bool TraverseShadowBoth(
        KDTGPUTraverser const& staticTraverser, KDTGPUTraverser const& dynamicTraverser, KDTGPUTraverser::StackItem* stack, int32 stackSize, DevicePointer<MemoryPool> memPool,
        Ray const& ray, cfloat maxDist, TraversalStatistics& traversalStats
    ) {
        ++(traversalStats.queryCount);
        auto staticHit = staticTraverser.TraverseShadow(ray, maxDist, stack, stackSize, memPool, traversalStats);
        auto dynamicHit = dynamicTraverser.TraverseShadow(ray, maxDist, stack, stackSize, memPool, traversalStats);
        return (staticHit || dynamicHit);
    }

    template<KDTGPUTraverser::RestartStrategy VRestartStrat>
    __device__
    bool TraverseShadowBoth(
        KDTGPUTraverser const& staticTraverser, KDTGPUTraverser const& dynamicTraverser,
        Ray const& ray, cfloat maxDist, TraversalStatistics& traversalStats
    ) {
        ++(traversalStats.queryCount);
        auto staticHit = staticTraverser.TraverseShadow<VRestartStrat>(ray, maxDist, traversalStats);
        auto dynamicHit = dynamicTraverser.TraverseShadow<VRestartStrat>(ray, maxDist, traversalStats);
        return (staticHit || dynamicHit);
    }

    template<KDTGPUTraverser::RestartStrategy VRestartStrat>
    __device__
    bool TraverseShadowBoth(
        KDTGPUTraverser const& staticTraverser, KDTGPUTraverser const& dynamicTraverser, KDTGPUTraverser::StackItem* stack, int8 stackSize,
        Ray const& ray, cfloat maxDist, TraversalStatistics& traversalStats
    ) {
        ++(traversalStats.queryCount);
        auto staticHit = staticTraverser.TraverseShadow<VRestartStrat>(ray, maxDist, stack, stackSize, traversalStats);
        auto dynamicHit = dynamicTraverser.TraverseShadow<VRestartStrat>(ray, maxDist, stack, stackSize, traversalStats);
        return (staticHit || dynamicHit);
    }

    __device__
    Color ComputeLitSurfaceColor(Vector3cf const& lightDir, Vector3cf const& normal, Vector3cf const& viewerDir, Material const& material, Components::Light::GPUData const& light, cfloat distanceIntensity) {
        Vector3cf reflection = Vector3cf::Reflect(-lightDir, normal);
        return
            // Diffuse
            material.colorDiffuse.a() * light.color.a() * distanceIntensity *
            abs(Vector3cf::Dot(normal, lightDir)) *
            Color{ light.color * material.colorDiffuse };
            // +
            // // Specular
            // pow(max(0.0_cf, Vector3cf::Dot(eyeDir, reflectDir)), mat.colorSpecular.a() + 0.001f) *
            // Color{ shadingData.lights[i].color * mat.colorSpecular };
    }

    __device__
    Color Shade(
        Ray const& incomingRay,
        KDTGPUTraverser::RayHitGPU const& hitInfo,
        KDTGPUTraverser const& staticTraverser,
        KDTGPUTraverser const& dynamicTraverser,
        KDTGPUTraverser::StackItem*& stack,
        int32& stackSize,
        DevicePointer<MemoryPool> memPool,
        ShadingData const& shadingData,
        TraversalStatistics& traversalStats
    ) {
        Color color{ 0.0, 0.0, 0.0, 1.0/* ambient */ };

        // Sum phong model calculations for each light
        for (int i = 0; i < shadingData.lights.count; ++i) {
            auto directionData = shadingData.lights[i].DirectToLight(hitInfo.point);
            if (!directionData.IsOutOfRange()) {
                Ray shadowRay{ hitInfo.point, directionData.direction };

                // Cast a shadow ray, if the light is not occluded compute phong lighting
                if (!TraverseShadowBoth(staticTraverser, dynamicTraverser, stack, stackSize, memPool, shadowRay, directionData.distance, traversalStats)) {
                    color += ComputeLitSurfaceColor(
                        shadowRay.direction,
                        hitInfo.normal,
                        (incomingRay.origin - hitInfo.point).Normalized(),
                        hitInfo.instance->material,
                        shadingData.lights[i],
                        directionData.distanceIntensity
                    );
                }
            }
        }

        return color;
    }

    template<KDTGPUTraverser::RestartStrategy VRestartStrat>
    __device__
    Color Shade(
        Ray const& incomingRay,
        KDTGPUTraverser::RayHitGPU const& hitInfo,
        KDTGPUTraverser const& staticTraverser,
        KDTGPUTraverser const& dynamicTraverser,
        KDTGPUTraverser::StackItem* const stack,
        int8 stackSize,
        ShadingData const& shadingData,
        TraversalStatistics& traversalStats
    ) {
        Color color{ 0.0, 0.0, 0.0, 1.0/* ambient */ };
        
        // Sum phong model calculations for each light
        for (int i = 0; i < shadingData.lights.count; ++i) {
            auto directionData = shadingData.lights[i].DirectToLight(hitInfo.point);
            if (!directionData.IsOutOfRange()) {
                Ray shadowRay{ hitInfo.point, directionData.direction };

                // Cast a shadow ray, if the light is not occluded compute phong lighting
                if ((stackSize == 0 ?
                    !TraverseShadowBoth<VRestartStrat>(staticTraverser, dynamicTraverser, shadowRay, directionData.distance, traversalStats)
                    :
                    !TraverseShadowBoth<VRestartStrat>(staticTraverser, dynamicTraverser, stack, stackSize, shadowRay, directionData.distance, traversalStats)
                )) {
                    color += ComputeLitSurfaceColor(
                        shadowRay.direction,
                        hitInfo.normal,
                        (incomingRay.origin - hitInfo.point).Normalized(),
                        hitInfo.instance->material,
                        shadingData.lights[i],
                        directionData.distanceIntensity
                    );
                }
            }
        }

        return color;
    }

    namespace Kernels
    {
        __global__
        void RaytraceScreen(
            RaytracingData const rtData,
            KDTGPUTraverser const staticTraverser,
            KDTGPUTraverser const dynamicTraverser,
            DevicePointer<KDTGPUTraverser::StackItem[]> const stack,
            DevicePointer<MemoryPool> memPool,
            ShadingData const shadingData,
            DevicePointer<TraversalStatistics[]> traversalStatsArray)
        {
            int32 stackSize = static_cast<int32>(stack.count / Compute::Grid2DHelpers::ThreadsInGrid().ElementProduct());
            KDTGPUTraverser::StackItem* threadStack = stack.data + Compute::Grid2DHelpers::GlobalThreadIdx1D() * stackSize;

            auto bufferIdx = Compute::Grid2DHelpers::GlobalThreadIdx2D();
            auto gridSize = Compute::Grid2DHelpers::ThreadsInGrid();
            auto rayIt = PrimaryRayGenerator::BlockIterator{ &rtData.primaryRayGen, gridSize, bufferIdx };
            auto rayItEnd = rtData.primaryRayGen.end();
            TraversalStatistics traversalStats{};

            while (rayIt < rayItEnd) {
                Ray primaryRay = *rayIt;
                // Init with background color
                Color color = Color{ rtData.clearValue };

                // Intersect primary ray with scene
                if (auto hitOptional = TraverseBoth(staticTraverser, dynamicTraverser, threadStack, stackSize, memPool, primaryRay, cuda::std::numeric_limits<cfloat>::infinity(), traversalStats); hitOptional.has_value()) {
                    //color = threadColor;
                    color = Shade(primaryRay, hitOptional.value(), staticTraverser, dynamicTraverser, threadStack, stackSize, memPool, shadingData, traversalStats);
                }
                
                // Save color to output buffer
                bufferIdx = rayIt.GetScreenCoordinates();
                float4 colorOgl = make_float4(
                    static_cast<float>(color.r()),
                    static_cast<float>(color.g()),
                    static_cast<float>(color.b()),
                    static_cast<float>(color.a())
                );
                surf2Dwrite(colorOgl, rtData.colorBuffer, sizeof(float4) * bufferIdx.x(), bufferIdx.y());
                ++rayIt;
            }

            traversalStatsArray[Compute::Grid2DHelpers::GlobalThreadIdx1D()] = traversalStats;
        }

        template<KDTGPUTraverser::RestartStrategy VRestartStrat>
        __global__
        void RaytraceScreen(
            RaytracingData const rtData,
            KDTGPUTraverser const staticTraverser,
            KDTGPUTraverser const dynamicTraverser,
            uint8 const stackSize,
            ShadingData const shadingData,
            DevicePointer<TraversalStatistics[]> traversalStatsArray)
        {
            extern __shared__ KDTGPUTraverser::StackItem stack_shared[];
            KDTGPUTraverser::StackItem* stack = &stack_shared[stackSize * Compute::Grid2DHelpers::LocalThreadIdx1D()];

            auto bufferIdx = Compute::Grid2DHelpers::GlobalThreadIdx2D();
            auto gridSize = Compute::Grid2DHelpers::ThreadsInGrid();
            auto rayIt = PrimaryRayGenerator::BlockIterator{ &rtData.primaryRayGen, gridSize, bufferIdx };
            auto rayItEnd = rtData.primaryRayGen.end();
            TraversalStatistics traversalStats{};

            while (rayIt < rayItEnd) {
                Ray primaryRay = *rayIt;
                // Init with background color
                Color color = Color{ rtData.clearValue };

                // Intersect primary ray with scene
                if (stackSize == 0) {
                    if (auto hitOptional = TraverseBoth<VRestartStrat>(staticTraverser, dynamicTraverser, primaryRay, cuda::std::numeric_limits<cfloat>::infinity(), traversalStats); hitOptional.has_value()) {
                        color = Shade<VRestartStrat>(primaryRay, hitOptional.value(), staticTraverser, dynamicTraverser, stack, stackSize, shadingData, traversalStats);
                    }
                }
                else {
                    if (auto hitOptional = TraverseBoth<VRestartStrat>(staticTraverser, dynamicTraverser, stack, stackSize, primaryRay, cuda::std::numeric_limits<cfloat>::infinity(), traversalStats); hitOptional.has_value()) {
                        color = Shade<VRestartStrat>(primaryRay, hitOptional.value(), staticTraverser, dynamicTraverser, stack, stackSize, shadingData, traversalStats);
                    }
                }
                
                // Save color to output buffer
                bufferIdx = rayIt.GetScreenCoordinates();
                float4 colorOgl = make_float4(
                    static_cast<float>(color.r()),
                    static_cast<float>(color.g()),
                    static_cast<float>(color.b()),
                    static_cast<float>(color.a())
                );
                surf2Dwrite(colorOgl, rtData.colorBuffer, sizeof(float4) * bufferIdx.x(), bufferIdx.y());
                ++rayIt;
            }

            traversalStatsArray[Compute::Grid2DHelpers::GlobalThreadIdx1D()] = traversalStats;
        }
    }

    KDTGPURaytracer::KDTGPURaytracer()
        : gpuData{ std::make_unique<KDTGPURaytracer::GPUData>() },
          buildTimer{}, raytraceTimer{}
    {
    }

    KDTGPURaytracer::~KDTGPURaytracer() {
        Logger::LogTest(std::string{ "Average k-d tree build time: " } + buildTimer.GetAverageTimeString(Timer::Precision::Milliseconds));
        Logger::LogTest(std::string{ "Average scene render time: " } + raytraceTimer.GetAverageTimeString(Timer::Precision::Milliseconds));
    }

    void KDTGPURaytracer::Init() {
        tree = CreateTree();
    }

    void KDTGPURaytracer::BuildStatic() {
        auto renderersRange = renderers.GetItems() | std::views::filter([] (CompPtr<Components::Renderer> const& r) {
            return r->GetComponent<Components::Transform>()->IsStatic();
        });
        std::vector<CompPtr<Components::Renderer>> renderersVec(renderersRange.begin(), renderersRange.end());

        buildTimer.Start();
        tree->BuildStatic(std::move(renderersVec));
        buildTimer.Stop();

        tree->GetStatistics().structureBuildTime = buildTimer.Elapsed();
        Logger::LogTest(std::string{ "Static k-d tree built in " } + buildTimer.GetElapsedString(Timer::Precision::Milliseconds));
        RTApp::Instance()->statistics.AddData("StaticBuildTime", buildTimer.Elapsed());

        buildTimer.Reset();
    }

    void KDTGPURaytracer::BuildDynamic() {
        auto renderersRange = renderers.GetItems() | std::views::filter([] (CompPtr<Components::Renderer> const& r) {
            return (r->doRender && !r->GetComponent<Components::Transform>()->IsStatic());
        });
        std::vector<CompPtr<Components::Renderer>> renderersVec(renderersRange.begin(), renderersRange.end());

        buildTimer.Start();
        tree->BuildDynamic(std::move(renderersVec));
        buildTimer.Stop();

        Logger::LogTest(std::string{ "Dynamic k-d tree built in " } + buildTimer.GetElapsedString(Timer::Precision::Milliseconds));
        tree->GetStatistics().structureBuildTime = buildTimer.Elapsed();
        RTApp::Instance()->statistics.AddData("DynamicBuildTime", buildTimer.Elapsed());
    }

    void KDTGPURaytracer::RaytraceScene() {
        using namespace Components;

        if (colorBufferCPU.Dimensions() != Camera::MainCamera()->Viewport()) {
            colorBufferCPU = ImageBuffer<Color32f>{ Camera::MainCamera()->Viewport(), Camera::MainCamera()->backgroundColor };
            gpuTexture.Resize(Camera::MainCamera()->Viewport());
        }

        GPUMeshManager::UploadLights();

        gpuTexture.Map();

        RaytracingData rtData{
            gpuTexture.GetSurfaceObject(),
            Camera::MainCamera()->backgroundColor,
            PrimaryRayGenerator{ Camera::MainCamera(), (renderOrigin == RenderOrigin::BottomLeft) }
        };

        KDTGPUTraverser staticTraverser = tree->GetStaticTraverser();

        KDTGPUTraverser dynamicTraverser = tree->GetDynamicTraverser();

        ShadingData shadingData{
            GPUMeshManager::Lights()
        };

        auto const& sceneConfig = Scene::GetActiveScene()->Config();

        int32 threadsPerAxis = sceneConfig.GetValue("raytracingThreadsPerAxis", 8);
        int32 groupsPerAxis;
        if (!sceneConfig.TryGetValue("raytracingGroupsPerAxis", groupsPerAxis)) {
            int devID;
            Compute::CudaCheckError(cudaGetDevice(&devID));
            int sm;
            Compute::CudaCheckError(cudaDeviceGetAttribute(&sm, cudaDeviceAttr::cudaDevAttrMultiProcessorCount, devID));
            groupsPerAxis = static_cast<int32>(std::sqrt(4 * sm));
        }

        dim3 groupSize{ static_cast<uint32>(threadsPerAxis), static_cast<uint32>(threadsPerAxis) };
        dim3 gridSize{ static_cast<uint32>(groupsPerAxis), static_cast<uint32>(groupsPerAxis) };
        int64 threadN = groupsPerAxis * groupsPerAxis * threadsPerAxis * threadsPerAxis;

        if (gpuData->traversalStats.Count() != threadN) {
            gpuData->traversalStats = DeviceUniquePointer<TraversalStatistics[]>::AllocateAndSet(threadN, 0);
        }

        if (std::string traversal = sceneConfig.GetValue<std::string>("traversal", "Stack"); traversal == "Stack") {
            int32 stackSize = sceneConfig.GetValue<int32>("stackSize", 32) * threadN;
            UniqueMemoryPool& memoryPool = tree->DataMemoryPool();
            memoryPool.GetCPU().Reset();
            auto* stack = memoryPool.GetCPU().Allocate<KDTGPUTraverser::StackItem>(1, stackSize).data;
            memoryPool.SyncCPU2GPU();

            raytraceTimer.Start();
            Kernels::RaytraceScreen<<<gridSize, groupSize>>>(rtData, staticTraverser, dynamicTraverser, DevicePointer<KDTGPUTraverser::StackItem[]>{ stack, stackSize }, memoryPool.GetGPU(), shadingData, gpuData->traversalStats.Get());
            raytraceTimer.Stop();
        }
        else {
            int8 stackSize;
            KDTGPUTraverser::RestartStrategy restartStrat;

            if (traversal == "Restart") {
                stackSize = 0;
                restartStrat = KDTGPUTraverser::RestartStrategy::Restart;
            }
            else if (traversal == "PushDown") {
                stackSize = 0;
                restartStrat = KDTGPUTraverser::RestartStrategy::PushDown;
            }
            else if (traversal == "RestartShortStack") {
                stackSize = sceneConfig.GetValue<int32>("stackSize", 8);
                restartStrat = KDTGPUTraverser::RestartStrategy::Restart;
            }
            else if (traversal == "PushDownShortStack") {
                stackSize = sceneConfig.GetValue<int32>("stackSize", 8);
                restartStrat = KDTGPUTraverser::RestartStrategy::PushDown;
            }

            auto sharedMem = stackSize * sizeof(KDTGPUTraverser::StackItem) * threadsPerAxis * threadsPerAxis;
            raytraceTimer.Start();
            if (restartStrat == KDTGPUTraverser::RestartStrategy::Restart) {
                Kernels::RaytraceScreen<KDTGPUTraverser::RestartStrategy::Restart><<<gridSize, groupSize, sharedMem>>>(rtData, staticTraverser, dynamicTraverser, stackSize, shadingData, gpuData->traversalStats.Get());
            }
            else {
                Kernels::RaytraceScreen<KDTGPUTraverser::RestartStrategy::PushDown><<<gridSize, groupSize, sharedMem>>>(rtData, staticTraverser, dynamicTraverser, stackSize, shadingData, gpuData->traversalStats.Get());
            }
            raytraceTimer.Stop();
        }

        renderOnCPU = false;

#ifdef PRT_DEBUG
        Compute::CudaCheckError(cudaDeviceSynchronize());
#endif
    }

    ImageBuffer<Color32f> const& KDTGPURaytracer::GetRender() {
        WaitForFinish();

        if (!renderOnCPU) {
            gpuTexture.Download(colorBufferCPU);
            renderOnCPU = true;
        }

        return colorBufferCPU;
    }

    OpenGL::Texture const& KDTGPURaytracer::GetRenderTexture() {
        WaitForFinish();

        return gpuTexture.GetTexture();
    }

    void KDTGPURaytracer::UpdateOpenGLResources() {
        using namespace Components;
        if (colorBufferCPU.Dimensions() != Camera::MainCamera()->Viewport()) {
            colorBufferCPU = ImageBuffer<Color32f>{ Camera::MainCamera()->Viewport(), Camera::MainCamera()->backgroundColor };
            gpuTexture.Resize(Camera::MainCamera()->Viewport());
        }
        gpuTexture.Map();
    }

    std::optional<RayHit> KDTGPURaytracer::CastRay(Ray const &ray, cfloat maxDist, TraversalStatistics* traversalStats) const {
        return tree->Traverse(ray, maxDist, traversalStats);
    }

    bool KDTGPURaytracer::CastShadowRay(Ray const& ray, cfloat maxDist, TraversalStatistics* traversalStats) const {
        return tree->TraverseShadow(ray, maxDist, traversalStats);
    }

    void KDTGPURaytracer::RegisterRenderer(CompPtr<Components::Renderer> renderer) {
        renderers.Register(renderer);
    }

    std::unique_ptr<KDTreeGPUBase> KDTGPURaytracer::CreateTree() const {
        auto const& sceneConfig = Scene::GetActiveScene()->Config();
        if (std::string structureStr = sceneConfig.GetValue<std::string>("structure", "KDTreeGPU"); structureStr == "KDTreeGPU") {
            auto tree = std::make_unique<KDTreeGPU>();
            tree->parameters = CreateKDTreeParameters();

            return tree;
        }
        else if (structureStr == "KDMergeTreeGPU") {
            auto tree = std::make_unique<KDMergeTreeGPU>();
            tree->parameters = CreateKDTreeParameters();

            return tree;
        }
        else {
            auto tree = std::make_unique<KDTreeGPU>();
            tree->parameters = CreateKDTreeParameters();

            return tree;
        }
    }
    
    void KDTGPURaytracer::WaitForFinish() {
        if (gpuTexture.IsMapped()) {
            Compute::CudaCheckError(cudaDeviceSynchronize());
            gpuTexture.Unmap();

            Logger::LogTest(std::string{ "Scene rendered in " } + raytraceTimer.GetElapsedString(Timer::Precision::Milliseconds));

            auto& stats = tree->GetStatistics();

            auto threadTraversalStats = gpuData->traversalStats.Download();
            auto traversalStats = std::reduce(threadTraversalStats.begin(), threadTraversalStats.end());

            stats.queryCount = traversalStats.queryCount;
            stats.avgIntersectionsPerQuery = static_cast<float>(traversalStats.intersections) / static_cast<float>(stats.queryCount);
            stats.avgTraversalStepsPerQuery = static_cast<float>(traversalStats.traversalSteps) / static_cast<float>(stats.queryCount);

            using DurType = decltype(stats.averageQueryTime);
            stats.averageQueryTime = std::chrono::duration_cast<DurType>(raytraceTimer.Elapsed()) / static_cast<double>(stats.queryCount);
            stats.performance = (1.0 / std::chrono::duration_cast<Timer::DurationReal>(stats.averageQueryTime).count());

            auto& appStats = RTApp::Instance()->statistics;
            appStats.AddData("AverageQueryTime", stats.averageQueryTime);
            appStats.AddData("AverageIntersectionsPerQuery", stats.avgIntersectionsPerQuery);
            appStats.AddData("AverageTraversalStepsPerQuery", stats.avgTraversalStepsPerQuery);
            appStats.AddData("QueryCount", stats.queryCount);

            #if PRT_SAVE_PER_THREAD_STATS
            std::vector<int64> threadQueryCount(threadTraversalStats.size());
            std::vector<double> threadIntersections(threadTraversalStats.size()), threadTraversalSteps(threadTraversalStats.size());
            int n = StdSize<int>(threadTraversalStats);
            for (int i = 0; i < n; ++i) {
                threadQueryCount[i] = threadTraversalStats[i].queryCount;
                threadIntersections[i] = static_cast<double>(threadTraversalStats[i].intersections) / static_cast<double>(threadTraversalStats[i].queryCount);
                threadTraversalSteps[i] = static_cast<double>(threadTraversalStats[i].traversalSteps) / static_cast<double>(threadTraversalStats[i].queryCount);
            }
            appStats.AddData("QueryCountPerThread", std::move(threadQueryCount));
            appStats.AddData("AverageIntersectionsPerQueryPerThread", std::move(threadIntersections));
            appStats.AddData("AverageTraversalStepsPerQueryPerThread", std::move(threadTraversalSteps));
            #endif
        }
    }
}
