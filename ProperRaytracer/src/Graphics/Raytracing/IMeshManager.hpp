/**
 * File name: IMeshManager.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <vector>

#include "Geometry/Primitive.hpp"
#include "Graphics/Raytracing/PUID.hpp"
#include "SceneDefinition/Components/Renderer.hpp"

namespace ProperRT
{
    class IMeshManager
    {
    public:
        /// @brief Returns the primitive from its reference.
        /// @param puid ID of the primitive to retrieve
        virtual
        Primitive GetPrimitive(PUID puid) const = 0;

        /// @brief Returns the renderer based on a primtive reference.
        /// @param puid ID of the primitive
        virtual
        CompPtr<Components::Renderer> GetRenderer(PUID puid) const = 0;

        /// @brief Returns the submesh based on a primtive reference.
        /// @param puid ID of the primitive
        virtual
        int32 GetSubmeshIdx(PUID puid) const = 0;

        /// @brief Returns the total primitive count the manager is managing.
        virtual
        int32 PrimitiveCount() const = 0;

        /// @brief Returns the total amount of memory used by this manager.
        virtual
        int64 AllocatedMemory() const = 0;

        /// @brief Returns the bounding box of the scene managed by this manager.
        virtual
        AABB const& SceneBoundingBox() const = 0;
    };
}
