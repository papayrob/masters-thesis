/**
 * File name: CPUMeshManager.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "CPUMeshManager.hpp"

namespace ProperRT
{
    std::pair<std::vector<PUID>, std::vector<AABB>> CPUMeshManager::CreateInstances(std::span<CompPtr<Components::Renderer>> renderers) {
        instances.clear();
        sceneBoundingBox = AABB::InfinitelySmall();

        std::vector<PUID> primitives{};
        std::vector<AABB> primitiveBBs{};

        // Each renderer
        int32 instanceI = 0;
        for (CompPtr<Components::Renderer> renderer : renderers) {
            auto submeshCount = renderer->GetSubmeshCount();
            
            // Each submesh in renderer
            for (int submeshI = 0; submeshI < submeshCount; ++submeshI, ++instanceI) {
                // Create instance
                instances.push_back(MeshInstance{ renderer, submeshI });
                auto& instance = instances.back();

                // Create primitive unique IDs
                auto info = renderer->GetSubmeshInfo(submeshI);
                for (int32 primitiveI = 0; primitiveI < info.submeshSize; ++primitiveI) {
                    primitives.push_back(PUID{ instanceI, primitiveI });

                    // Compute and include primitive bounding box in the scene bounding box
                    AABB primitiveBB = GetAABB(renderer->GetPrimitive(submeshI, primitiveI));
                    primitiveBBs.push_back(primitiveBB);
                    sceneBoundingBox.Include(primitiveBB);
                }
            }
        }

        primitiveN = StdSize<int32>(primitives);
        if (primitiveN == 0) {
            sceneBoundingBox = AABB{};
        }

        return std::make_pair<std::vector<PUID>, std::vector<AABB>>(std::move(primitives), std::move(primitiveBBs));
    }

}