/**
 * File name: GPUMeshManager.cuh
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "CPUMeshManager.hpp"
#include "Compute/ComputeUtilities.cuh"
#include "Geometry/MorphMesh.hpp"
#include "SceneDefinition/Components/Renderer.hpp"
#include "SceneDefinition/Components/Light.hpp"

namespace ProperRT
{
    namespace GPUMeshManagement
    {
        template<class TPrimitive>
        struct MeshData
        {
            DevicePointer<TPrimitive[]> primitives;
        };

        template<class TPrimitive>
        struct MorphMeshData
        {
            DevicePointer<TPrimitive[]> primitives;
            MorphMesh::MeshBlendInfo blendInfo;
            int32 singleMeshVertexN;
        };

        template<class TIndexedPrimitive>
        struct IndexedMeshData
        {
            DevicePointer<typename TIndexedPrimitive::VertexType[]> vertices;
            DevicePointer<TIndexedPrimitive[]> primitives;
        };

        template<class TIndexedPrimitive>
        struct IndexedMorphMeshData
        {
            DevicePointer<typename TIndexedPrimitive::VertexType[]> vertices;
            DevicePointer<TIndexedPrimitive[]> primitives;
            MorphMesh::MeshBlendInfo blendInfo;
            int32 singleMeshVertexN;
        };

        template<class TPrimitiveList, class TIndexedPrimitiveList>
        struct MeshDataVariant {};

        template<class... TsPrimitive, class... TsIndexedPrimitive>
        struct MeshDataVariant<TypeList<TsPrimitive...>, TypeList<TsIndexedPrimitive...>>
        {
            using Type = cuda::std::variant<
                MeshData<TsPrimitive> ...,
                MorphMeshData<TsPrimitive> ...,
                IndexedMeshData<TsIndexedPrimitive> ...,
                IndexedMorphMeshData<TsIndexedPrimitive> ...
            >;
        };

        using MeshDataVariant_t = typename MeshDataVariant<PrimitiveTypes, IndexedPrimitiveTypes>::Type;

        struct Instance
        {
            Matrix4x4cf transform;
            MeshDataVariant_t meshData;
            Material material;
            int32 submeshIdx;

            struct PrimitiveGetter
            {
                Instance const& instance;
                int32 idx;

                template<class TPrimitive>
                __device__
                Primitive operator()(MeshData<TPrimitive> const& mesh) const {
                    return mesh.primitives[idx].Transform(instance.transform);
                }

                template<class TPrimitive>
                __device__
                Primitive operator()(MorphMeshData<TPrimitive> const& mesh) const {
                    return TPrimitive::LERP(
                        mesh.primitives[mesh.blendInfo.FirstIndex() * mesh.singleMeshVertexN + idx],
                        mesh.primitives[mesh.blendInfo.SecondIndex() * mesh.singleMeshVertexN + idx],
                        mesh.blendInfo.blendAlpha
                    ).Transform(instance.transform);
                }

                template<class TIndexedPrimitive>
                __device__
                Primitive operator()(IndexedMeshData<TIndexedPrimitive> const& mesh) const {
                    return mesh.primitives[idx].DeIndex(mesh.vertices).Transform(instance.transform);
                }

                template<class TIndexedPrimitive>
                __device__
                Primitive operator()(IndexedMorphMeshData<TIndexedPrimitive> const& mesh) const {
                    return ProperRT::LERP(
                        mesh.primitives[idx].Offset(mesh.blendInfo.FirstIndex() * mesh.singleMeshVertexN).DeIndex(mesh.vertices),
                        mesh.primitives[idx].Offset(mesh.blendInfo.SecondIndex() * mesh.singleMeshVertexN).DeIndex(mesh.vertices),
                        mesh.blendInfo.blendAlpha
                    ).Transform(instance.transform);
                }
            };

            __device__
            Primitive GetPrimitive(int32 idx) const {
                return cuda::std::visit(PrimitiveGetter{ *this, idx }, meshData);
            }
        };

        namespace Kernels
        {
            __global__
            void InstanceGetPrimitive(Instance instance, int32 idx, DevicePointer<Primitive> primitive);
        }
    }
    

    /// @brief Mesh manager for the CPU.
    class GPUMeshManager : public IMeshManager
    {
    public:
        /// @brief Uploads lights to the GPU.
        static
        void UploadLights();

        /// @brief Returns pointer to uploaded lights.
        static
        DevicePointer<Components::Light::GPUData[]> Lights() { return lightsGPU.Get(); }

    public:
        /// @brief Creates instances from @p renderers.
        /// @param renderers List of renderers to build instances over
        /// @return Primitive references and their bounding boxes from all renderers
        std::pair<DeviceUniquePointer<PUID[]>, DeviceUniquePointer<AABB[]>> CreateInstances(std::span<CompPtr<Components::Renderer>> renderers);

        /// @brief Initializes mesh instances from another, CPU mesh manager.
        /// @param meshManager Other mesh manager
        /// @param instances CPU mesh instances of the other mesh manager
        void FromCPUMeshInstances(IMeshManager& meshManager, std::span<CPUMeshManager::MeshInstance> instances);

        /// @brief Returns pointer to the manager instances on the GPU.
        DevicePointer<GPUMeshManagement::Instance[]> Instances() const { return instancesGPU.Get(); }

        virtual
        Primitive GetPrimitive(PUID puid) const override {
            return instanceRenderers[puid.instanceID].renderer->GetPrimitive(instanceRenderers[puid.instanceID].submeshIdxInRenderer, puid.primitiveOffset);
        }

        /// @brief GetPrimitive method for device code.
        __device__ static
        Primitive GetPrimitive(DevicePointer<GPUMeshManagement::Instance[]> instances, PUID puid) {
            return instances[puid.instanceID].GetPrimitive(puid.primitiveOffset);
        }

        virtual
        CompPtr<Components::Renderer> GetRenderer(PUID puid) const override {
            return instanceRenderers[puid.instanceID].renderer;
        }

        virtual
        int32 GetSubmeshIdx(PUID puid) const override {
            return instanceRenderers[puid.instanceID].renderer->GetSubmeshInfo(instanceRenderers[puid.instanceID].submeshIdxInRenderer).submeshIdx;
        }

        virtual
        int32 PrimitiveCount() const override {
            return primitiveN;
        }

        virtual
        int64 AllocatedMemory() const override;

        virtual
        AABB const& SceneBoundingBox() const override { return sceneBoundingBox; };

    private:
        struct InstanceRendererSubmesh
        {
            CompPtr<Components::Renderer> renderer;
            int32 submeshIdxInRenderer;
        };

        static inline
        DeviceUniquePointer<Components::Light::GPUData[]> lightsGPU{};
    
    private:
        std::vector<InstanceRendererSubmesh> instanceRenderers;
        std::vector<GPUMeshManagement::Instance> instancesCPU;
        DeviceUniquePointer<GPUMeshManagement::Instance[]> instancesGPU;
        int32 primitiveN;
        AABB sceneBoundingBox;
    };
}
