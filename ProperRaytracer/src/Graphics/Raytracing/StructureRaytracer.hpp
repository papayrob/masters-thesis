/**
 * File name: StructureRaytracer.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <vector>
#include <memory>
#include <thread>

#include "IRaytracer.hpp"
#include "IAccelerationStructure.hpp"
#include "SceneDefinition/CompRegistry.hpp"
#include "SceneDefinition/Components/Renderer.hpp"

namespace ProperRT
{
    /// @brief Raytracer that uses any acceleration structure that implements IAccelerationStructure.
    class StructureRaytracer : public IRaytracer
    {
    public:
        StructureRaytracer(int threadN);

        virtual
        ~StructureRaytracer();

        virtual
        void Init() override;

        virtual
        void BuildStatic() override;

        virtual
        void BuildDynamic() override;

        virtual
        void RaytraceScene() override;

        virtual
        ImageBuffer<Color32f> const& GetRender() override;

        virtual
        OpenGL::Texture const& GetRenderTexture() override;

        virtual
        void UpdateOpenGLResources() override;

        virtual
        void RegisterRenderer(CompPtr<Components::Renderer> renderer) override;

        virtual
        INavigator* GetNavigator() override {
            return (accStructure == nullptr ? nullptr : accStructure->TopNavigator());
        }

        virtual
        IStructureValidator* GetValidator() override {
            return (accStructure == nullptr ? nullptr : accStructure->Validator());
        }

        virtual
        std::optional<RayHit> CastRay(Ray const& ray, cfloat maxDist = std::numeric_limits<cfloat>::infinity(), TraversalStatistics* traversalStats = nullptr) const override;

        virtual
        bool CastShadowRay(Ray const& ray, cfloat maxDist, TraversalStatistics* traversalStats = nullptr) const override;

        // Simple Phong primary ray shading with shadow rays
        Color Shade(Ray const& incomingRay, RayHit const& hitInfo, TraversalStatistics* traversalStats = nullptr) const;

        virtual
        void SetRenderOrigin(IRaytracer::RenderOrigin origin) override { renderOrigin = origin; }

        virtual
        IAccelerationStructure* GetAccelerationStructure() override { return accStructure.get(); }

        virtual
        void WaitForFinish() override;

    private:
        CompRegistry<Components::Renderer> renderers{};
        std::unique_ptr<IAccelerationStructure> accStructure{};
        ImageBuffer<Color32f> colorBuffer{ 0, 0 };
        OpenGL::Texture gpuTexture{};
        bool renderOnGPU{ true };

        int threadN;
        std::unique_ptr<std::thread[]> threads;
        std::atomic_int runningThreadN{};

        IRaytracer::RenderOrigin renderOrigin{};

        Timer buildTimer{};
        Timer raytraceTimer{};

        std::atomic_int traversalSteps;
        std::atomic_int intersections;
        std::atomic_int rayCount;

        std::unique_ptr<IAccelerationStructure> CreateStructure() const;
    };
}
