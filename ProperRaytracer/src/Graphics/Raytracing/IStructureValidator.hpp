/**
 * File name: IStructureValidator.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <optional>
#include <string>

namespace ProperRT
{
    /// @brief Interface for a class that can validate an acceleration data structure.
    class IStructureValidator
    {
    public:
        /// @brief Validates the data structure and returns an empty optional if no errors ocurred, otherwise returns the error message.
        virtual
        std::optional<std::string> Validate() = 0;
    };
}