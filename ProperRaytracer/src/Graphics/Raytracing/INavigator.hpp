/**
 * File name: INavigator.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

namespace ProperRT
{
    /// @brief Interface for accelerating structure navigators.
    /// @details Classes implementing this interface can be used as structure navigators,
    /// which means they can be attached to the StructureNavigator component and used to
    /// view and debug structures visually, in engine.
    class INavigator
    {
    public:
        /// @brief Allocates resources and resets navigation.
        /// @details Guaranteed to be called at least once before any other methods,
        /// and if called again, should reset navigation to the default state.
        virtual
        void StartNavigating() = 0;

        /// @brief Display navigation UI and process inputs.
        virtual
        void Navigate() = 0;

        /// @brief Called when navigating out.
        virtual
        void NavigateOut() = 0;

        /// @brief Returns true if this navigator has a sub-navigator
        /// it can navigate into, and the sub-navigator is available.
        /// @details For example: For a tree navigator with a node sub-navigator,
        /// return true if the node is not empty.
        virtual
        bool CanNavigateInto() = 0;

        /// @brief Navigate into a sub-navigator, called only if @ref INavigator::CanNavigateInto()
        /// returns true.
        /// @return Pointer to the sub-navigator, cannot be nullptr if CanNavigateInto is true and allocation and de-allocation
        /// must be handled by the navigator
        virtual
        INavigator* NavigateInto() = 0;

        /// @brief Render visualization.
        virtual
        void Render() = 0;
    };
}