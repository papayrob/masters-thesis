/**
 * File name: CPUMeshManager.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <span>

#include "IMeshManager.hpp"

namespace ProperRT
{
    /// @brief Mesh manager for the CPU.
    class CPUMeshManager : public IMeshManager
    {
    public:
        /// @brief Mesh instance, corresponding to a renderer and one submesh of the submeshes it is rendering.
        struct MeshInstance
        {
            /// @brief Renderer this instance referes to.
            CompPtr<Components::Renderer> renderer;
            /// @brief Index of the submesh in the renderer this instance referes to.
            int32 submeshIdxInRenderer;
            //std::vector<Triangle> triangles;

            /// @brief Returns the primitive at @p idx.
            /// @param idx Index of the primitive in the submesh
            Primitive GetPrimitive(int32 idx) const {
                return renderer->GetPrimitive(submeshIdxInRenderer, idx);
            }
        };

    public:
        /// @brief Creates instances from @p renderers.
        /// @param renderers List of renderers to build instances over
        /// @return Primitive references and their bounding boxes from all renderers
        std::pair<std::vector<PUID>, std::vector<AABB>> CreateInstances(std::span<CompPtr<Components::Renderer>> renderers);

        /// @brief Returns the manager instances.
        std::span<MeshInstance> Instances() { return instances; }

        /// @brief Returns the manager instances.
        std::vector<MeshInstance> const& Instances() const { return instances; }

        virtual
        Primitive GetPrimitive(PUID puid) const override {
            return instances[puid.instanceID].GetPrimitive(puid.primitiveOffset);
        }

        virtual
        CompPtr<Components::Renderer> GetRenderer(PUID puid) const override {
            return instances[puid.instanceID].renderer;
        }

        virtual
        int32 GetSubmeshIdx(PUID puid) const override {
            return instances[puid.instanceID].renderer->GetSubmeshInfo(instances[puid.instanceID].submeshIdxInRenderer).submeshIdx;
        }

        virtual
        int32 PrimitiveCount() const override {
            return primitiveN;
        }

        virtual
        int64 AllocatedMemory() const override { return static_cast<int64>(std::span{instances}.size_bytes()); }

        virtual
        AABB const& SceneBoundingBox() const override { return sceneBoundingBox; }

    private:
        std::vector<MeshInstance> instances;
        int32 primitiveN;
        AABB sceneBoundingBox;
    };
}
