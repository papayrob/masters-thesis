/**
 * File name: BinaryTreeNavigator.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <stack>

#include "Geometry/AABB.hpp"
#include "INavigator.hpp"
#include "Geometry/OrientedBoundingBoxMesh.hpp"

namespace ProperRT
{
    /// @brief Class for representing a navigation node of a binary tree, used for navigating the tree.
    class BinaryTreeNavigationNode
    {
    public:
        /// @brief Returns the node bounding box.
        virtual
        AABB BoundingBox() const = 0;

        /// @brief Returns true if the node is a leaf.
        virtual
        bool IsLeaf() const = 0;

        /// @brief Returns true if the node has a parent.
        virtual
        bool CanGoUp() = 0;
        
        /// @brief Navigates to the parent.
        virtual
        void GoUp() = 0;

        /// @brief Returns true if the node has a left child.
        virtual
        bool CanGoLeft() = 0;

        /// @brief Navigates to the left child.
        virtual
        void GoLeft() = 0;

        /// @brief Returns true if the node has a left child.
        virtual
        bool CanGoRight() = 0;

        /// @brief Navigates to the right child.
        virtual
        void GoRight() = 0;

        /// @brief Number of primitives in the current node.
        virtual
        int32 PrimitiveCount() = 0;

        /// @brief Returns the primitive at @p primitiveIdx.
        /// @param primitiveIdx Index of the primitive to retrieve
        virtual
        Primitive GetPrimitive(int32 primitiveIdx) = 0;
    };

    /// @brief Interface for a navigatable binary tree.
    class NavigatableBinaryTree
    {
    public:
        virtual
        std::unique_ptr<BinaryTreeNavigationNode> NavigationRoot() = 0;
    };

    class BinaryTreeNodeNavigator;

    /// @brief Class for representing a navigator of a binary tree, implementing some common functionality.
    class BinaryTreeNavigator : public INavigator
    {
    public:
        friend BinaryTreeNodeNavigator;

        /// @brief Color used to render the tree/node bounding box.
        inline static
        Color boundingBoxColor = Color{ 1.0, 0.0, 1.0, 1.0 };

    public:
        /// @brief Standard ctor.
        /// @param tree Navigatable binary tree instance
        explicit
        BinaryTreeNavigator(NavigatableBinaryTree* tree);

        virtual
        void StartNavigating() override;

        virtual
        void Navigate() override;

        virtual
        void NavigateOut() override;

        virtual
        INavigator* NavigateInto() override;

    protected:
        NavigatableBinaryTree* tree;

        std::unique_ptr<BinaryTreeNavigationNode> node;
        std::deque<Side> navigationChain;
        std::string navigationChainString;

        std::shared_ptr<OrientedBoundingBoxMesh> nodeBoundingBox;

        std::unique_ptr<BinaryTreeNodeNavigator> nodeNavigator;

        bool disableUp{ true };
        bool disableLeft{ true };
        bool disableRight{ true };
        bool disableNeigh{ true };

        /// @brief Navigate to the parent of the current node.
        virtual
        void NavigateUp();

        /// @brief Navigate to the left child of the current node.
        virtual
        void NavigateLeft();

        /// @brief Navigate to the right child of the current node.
        virtual
        void NavigateRight();

        /// @brief Navigate to the neighbour of the current node.
        virtual
        void NavigateSwitch();

        /// @brief Update meshes needed for rendering.
        virtual
        void UpdateMeshes();

        /// @brief Create a navigation chain string from the current navigation chain.
        virtual
        std::string CreateNavigationChainString() const = 0;

        /// @brief Set navigation buttons.
        virtual
        void SetButtons() = 0;

        /// @brief Creates the node navigator.
        virtual
        std::unique_ptr<BinaryTreeNodeNavigator> CreateNodeNavigator() = 0;
    };

    /// @brief Class for representing a navigator of a binary tree node, implementing some common functionality.
    class BinaryTreeNodeNavigator : public INavigator
    {
    public:
        /// @brief Color of the selected primitive.
        inline static
        Color selectedColor{ 1., 0., 0., 1. };

    public:
        /// @brief Standard ctor.
        /// @param parentNavigator Parent tree navigator
        explicit
        BinaryTreeNodeNavigator(BinaryTreeNavigator* parentNavigator);

        virtual
        void StartNavigating() override;

        virtual
        void Navigate() override;

        virtual
        void NavigateOut() override;

        virtual
        bool CanNavigateInto() override { return false; }

        virtual
        INavigator* NavigateInto() override { return nullptr; }

    protected:
        BinaryTreeNavigator* parentNavigator;

        std::shared_ptr<OrientedBoundingBoxMesh> nodeBoundingBox;
        std::shared_ptr<Mesh> selectedPrimitive;
        int32 primitiveIdx{};

        bool opaqueBoxes{ false };

        /// @brief Update meshes needed for rendering.
        virtual
        void UpdateMeshes();
    };
}
