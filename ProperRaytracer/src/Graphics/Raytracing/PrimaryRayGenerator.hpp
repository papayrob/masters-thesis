/**
 * File name: PrimaryRayGenerator.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "Geometry/Vector.hpp"
#include "Geometry/Ray.hpp"
#include "SceneDefinition/Components/Transform.hpp"
#include "SceneDefinition/Components/Camera.hpp"

namespace ProperRT
{
    /// @brief Class for generating primary rays.
    class PrimaryRayGenerator
    {
    public:
        using SizeType = int32;

        /// @brief Ray iterator.
        class Iterator
        {
        public:
            CUDA_FUNCTION
            Iterator(PrimaryRayGenerator const* owner_, SizeType i_ = 0, SizeType j_ = 0)
                : owner(owner_), i(i_), j(j_), shift(owner->GetShift(i_, j_))
            {
                Wrap();
                Clamp();
            }

            CUDA_FUNCTION
            Iterator& operator+=(SizeType rhs) {
                i += rhs;
                if (Wrap()) {
                    shift = owner->GetShift(i, j);
                }
                else {
                    shift += static_cast<cfloat>(rhs) * owner->rightShift;
                }
                Clamp();
                return *this;
            }

            CUDA_FUNCTION
            friend Iterator operator+(Iterator lhs, SizeType rhs) {
                lhs += rhs;
                return lhs;
            }

            CUDA_FUNCTION
            Iterator& operator++() {
                if (j < owner->viewport.y()) {
                    ++i;
                    if (i == owner->viewport.x()) {
                        i = 0;
                        ++j;
                        shift = owner->GetShift(0, j);
                    }
                    else {
                        shift += owner->rightShift;
                    }
                }
                return *this;
            }

            CUDA_FUNCTION
            Ray operator*() const {
                return {
                    owner->origin,
                    (owner->forward + shift).Normalized()
                };
            }

            CUDA_FUNCTION
            friend auto operator==(Iterator const& lhs, Iterator const& rhs) {
                return (lhs.owner == rhs.owner && lhs.i == rhs.i && lhs.j == rhs.j);
            }

            CUDA_FUNCTION
            friend auto operator<(Iterator const& lhs, Iterator const& rhs) {
                return (lhs.j < rhs.j || (lhs.j == rhs.j && lhs.i < rhs.i));
            }

            CUDA_FUNCTION
            friend auto operator<=>(Iterator const& lhs, Iterator const& rhs) {
                auto jOrder = lhs.j <=> rhs.j;
                if (jOrder == std::strong_ordering::equal) {
                    return lhs.i <=> rhs.i;
                }
                else {
                    return jOrder;
                }
            }

            CUDA_FUNCTION
            Vector<2, SizeType> GetScreenCoordinates() const { return { i, j }; }

            CUDA_FUNCTION
            bool Wrap() {
                // https://www.desmos.com/calculator/1pl0sarbmx
                if (i < 0) {
                    auto w = owner->viewport.x();
                    auto change = ((-i - 1) / w + 1);
                    j = j - change;
                    i = w * change + i;
                    return true;
                }
                else if (i >= owner->viewport.x()) {
                    auto w = owner->viewport.x();
                    j += i / w;
                    i = i % w;
                    return true;
                }
                return false;
            }

            CUDA_FUNCTION
            void Clamp() {
                if (j < 0) {
                    i = owner->viewport.x();
                    j = -1;
                }
                if (j >= owner->viewport.y()) {
                    i = 0;
                    j = owner->viewport.y();
                }
            }

        protected:
            PrimaryRayGenerator const* owner;
            SizeType i, j;
            Vector3cf shift;
        };

        /// @brief Ray iterator for a 2D block.
        class BlockIterator : public Iterator
        {
        public:
            CUDA_FUNCTION
            BlockIterator(PrimaryRayGenerator const* owner_, Vector<2, SizeType> blockSize_, Vector<2, SizeType> startCoords = { 0, 0 })
                : Iterator(owner_, startCoords.x(), startCoords.y()), blockSize{ blockSize_ }
            {}

            CUDA_FUNCTION
            BlockIterator& operator+=(SizeType rhs) {
                SizeType jOld = j;
                i += rhs * blockSize.x();
                if (Wrap()) {
                    j = jOld + (blockSize.y() * (j - jOld));
                    shift = owner->GetShift(i, j);
                }
                else {
                    shift += static_cast<cfloat>(rhs * blockSize.x()) * owner->rightShift;
                }
                
                Clamp();
                return *this;
            }

            CUDA_FUNCTION
            friend BlockIterator operator+(BlockIterator lhs, SizeType rhs) {
                lhs += rhs;
                return lhs;
            }

            CUDA_FUNCTION
            BlockIterator& operator++() {
                return operator+=(1);
            }

            CUDA_FUNCTION
            Ray operator*() const {
                return Iterator::operator*();
            }

        protected:
            using Iterator::owner;
            using Iterator::i;
            using Iterator::j;
            Vector<2, SizeType> blockSize;
        };

        /// @brief Class representing a pixel row, used to generate RowIterators.
        class Row
        {
        public:
            class Iterator
            {
            public:
                CUDA_FUNCTION
                Iterator(PrimaryRayGenerator const* owner_, SizeType i_ = 0, SizeType j_ = 0)
                    : owner{ owner_} , i{ i_ }, j{ j_ }, shift(owner->GetShift(i_, j_))
                {
                    Clamp();
                }

                CUDA_FUNCTION
                Iterator& operator+=(SizeType rhs) {
                    i += rhs;
                    shift += static_cast<cfloat>(rhs) * owner->rightShift;
                    Clamp();
                    return *this;
                }

                CUDA_FUNCTION
                friend Iterator operator+(Iterator lhs, SizeType rhs) {
                    lhs += rhs;
                    return lhs;
                }

                CUDA_FUNCTION
                Iterator& operator++() {
                    ++i;
                    shift += owner->rightShift;
                    if (i > owner->viewport.x()) i = owner->viewport.x();
                    return *this;
                }

                CUDA_FUNCTION
                Ray operator*() const {
                    return {
                        owner->origin,
                        (owner->forward + shift).Normalized()
                    };
                }

                CUDA_FUNCTION
                friend auto operator==(Iterator const& lhs, Iterator const& rhs) {
                    return (lhs.owner == rhs.owner && lhs.i == rhs.i && lhs.j == rhs.j);
                }

                CUDA_FUNCTION
                friend auto operator<(Iterator const& lhs, Iterator const& rhs) {
                    return lhs.i < rhs.i;
                }

                friend auto operator<=>(Iterator const& lhs, Iterator const& rhs) {
                    return lhs.i <=> rhs.i;
                }

                CUDA_FUNCTION
                void Clamp() {
                    if (i < 0) i = -1;
                    if (i > owner->viewport.x()) i = owner->viewport.x();
                }

            protected:
                PrimaryRayGenerator const* owner;
                SizeType i, j;
                Vector3cf shift;
            };
        public:
            CUDA_FUNCTION
            Row(PrimaryRayGenerator const* owner_, SizeType row_) : owner{ owner_ }, row{ row_ } {}

            CUDA_FUNCTION
            Iterator begin() { return Iterator{ owner, 0, row }; }

            CUDA_FUNCTION
            Iterator end() { return Iterator{ owner, owner->viewport.x(), row }; }

        private:
            PrimaryRayGenerator const* owner;
            SizeType row;
        };

        /// @brief Ray iterator for a row.
        class RowIterator
        {
        public:
            CUDA_FUNCTION
            RowIterator(PrimaryRayGenerator const* owner_, SizeType row_ = 0)
                : owner{ owner_ } , row{ row_ }
            {
                Clamp();
            }

            CUDA_FUNCTION
            RowIterator& operator+=(SizeType rhs) {
                row += rhs;
                Clamp();
                return *this;
            }

            CUDA_FUNCTION
            friend RowIterator operator+(RowIterator lhs, SizeType rhs) {
                lhs += rhs;
                return lhs;
            }

            CUDA_FUNCTION
            RowIterator& operator++() {
                ++row;
                if (row > owner->viewport.y()) row = owner->viewport.y();
                return *this;
            }

            CUDA_FUNCTION
            Row operator*() const {
                return Row{
                    owner,
                    row
                };
            }

            CUDA_FUNCTION
            friend auto operator==(RowIterator const& lhs, RowIterator const& rhs) {
                return (lhs.owner == rhs.owner && lhs.row == rhs.row);
            }

            CUDA_FUNCTION
            friend auto operator<(RowIterator const& lhs, RowIterator const& rhs) {
                return lhs.row < rhs.row;
            }

            friend auto operator<=>(RowIterator const& lhs, RowIterator const& rhs) {
                return lhs.row <=> rhs.row;
            }

            CUDA_FUNCTION
            void Clamp() {
                if (row < 0) row = -1;
                if (row > owner->viewport.y()) row = owner->viewport.y();
            }

        protected:
            PrimaryRayGenerator const* owner;
            SizeType row;
        };

    public:
        /// @brief Standard ctor, constructs the generator to generate rays from @p camera
        /// @param camera Camera to generate the rays from
        /// @param flipY If set to true, begins ray generation at the bottom-left corner instead of the top-left corner
        PrimaryRayGenerator(CompPtr<Components::Camera const> const& camera, bool flipY = false) {
            auto camTransform = camera->GetComponent<Components::Transform>();
            origin = camTransform->GetPositionWorld();
            forward = camTransform->ForwardWorld();

            // Save viewport and compute the screen center
            viewport = camera->Viewport();
            auto fviewport = static_cast<Vector2cf>(viewport);
            center = 0.5_cf * fviewport;

            // Compute vertical and horizontal fov
            cfloat verticalFOV = camera->GetFOV();
            cfloat horizontalFOV = 2._cf * std::atan(std::tan(verticalFOV * 0.5_cf) * camera->GetAspectRatio());

            // Compute width and height of the image plane in world coordinates 1 unit away from the pinhole
            cfloat height = 2._cf * std::tan(verticalFOV * 0.5_cf);
            cfloat width = 2._cf * std::tan(horizontalFOV * 0.5_cf);

            // Compute the size of a pixel in world coordinates and multiply it by world direction
            rightShift = (width / fviewport.x()) * camTransform->RightWorld();
            downShift = (flipY ? 1._cf : -1._cf) * (height / fviewport.y()) * camTransform->UpWorld();
        }

        /// @brief Generates a ray for a pixel at (i,j)
        /// @param i Width coordinate of the pixel through which to shoot the primary ray
        /// @param j Height coordinate of the pixel through which to shoot the primary ray
        /// @return Ray for pixel at (i,j)
        CUDA_FUNCTION
        Ray GetRay(SizeType i, SizeType j) const {
            return {
                origin,
                (forward + GetShift(i,j)).Normalized()
            };
        }

        /// @brief Returns the horizontal shift between two neighbouring rays.
        /// @return Horizontal shift between two neighbouring rays
        CUDA_FUNCTION
        Vector3cf const& GetRightShit() const { return rightShift; }

        /// @brief Returns the vertical shift between two neighbouring rays.
        /// @return Vertical shift between two neighbouring rays
        CUDA_FUNCTION
        Vector3cf const& GetUpShit() const { return downShift; }

        CUDA_FUNCTION
        Iterator begin() const { return Iterator{ this, 0, 0 }; }

        CUDA_FUNCTION
        Iterator end() const { return Iterator{ this, 0, viewport.y() }; }

        CUDA_FUNCTION
        int32 RowCount() const { return viewport.y(); }

        CUDA_FUNCTION
        RowIterator Rows() const { return RowIterator(this, 0); }

    private:
        Vector3cf origin;
        Vector3cf forward;

        Vector2i viewport;

        Vector2cf center;
        Vector3cf rightShift;
        Vector3cf downShift;

        CUDA_FUNCTION
        Vector3cf GetShift(SizeType i, SizeType j) const {
            // Compute distance in pixels and multiply by pixel-sized directions
            return (0.5_cf + static_cast<cfloat>(i) - center.x()) * rightShift +
                   (0.5_cf + static_cast<cfloat>(j) - center.y()) * downShift;
        }
    };
}
