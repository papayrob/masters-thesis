/**
 * File name: BinaryMeshTree.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <concepts>
#include <unordered_set>

#include "Geometry/TriangleMesh.hpp"
#include "Geometry/OrientedBoundingBoxMesh.hpp"
#include "IMeshManager.hpp"

namespace ProperRT
{
    /// @brief Class for creating and rendering a mesh of a binary tree structure.
    class BinaryTreeMesh //MeshTree
    {
    public:
        /// @brief Class representing a mesh tree node, used for rendering the mesh for the associated node in the real tree.
        class Node
        {
        public:
            friend BinaryTreeMesh;
        public:
            /// @brief Default ctor.
            Node() noexcept = default;
            
            /// @brief Constructs the node relative to a parent node.
            /// @param parent_ Parent node
            explicit
            Node(Node* parent_) noexcept : parent{ parent_ } {}

            /// @brief Creates a left child.
            Node* CreateLeft() {
                left = std::make_unique<Node>(this);
                return left.get();
            }

            /// @brief Creates a right child.
            Node* CreateRight() {
                right = std::make_unique<Node>(this);
                return right.get();
            }

            /// @brief Sets primitive references of the current node.
            template<std::forward_iterator TIt>
                requires requires (TIt it) { {*it} -> std::convertible_to<PUID>; }
            void SetPrimitives(TIt first, TIt last) {
                primitives.clear();
                std::copy(first, last, std::back_inserter(primitives));
            }

            /// @brief Sets primitive references of the current node.
            void SetPrimitives(std::vector<PUID>&& primitives_) {
                primitives = std::move(primitives_);
            }

            /// @brief Renders the mesh associated with this node.
            void Render(RasterizationMatrices const& rasterizationMatrices);

            /// @brief Returns the parent node.
            Node* Parent() { return parent; }

            /// @brief Returns the left child.
            Node* Left() { return left.get(); }

            /// @brief Returns the right child.
            Node* Right() { return right.get(); }

            /// @brief Returns the primitive references associated with this node.
            std::span<PUID const> GetPrimitives() const { return primitives; }

        private:
            Node* parent{nullptr};
            std::unique_ptr<Node> left{};
            std::unique_ptr<Node> right{};

            //std::vector<int32> submeshes;
            std::shared_ptr<TriangleMesh> triangleMesh{};
            std::shared_ptr<OrientedBoundingBoxMesh> boxMesh{};
            bool isAnchor{ false };

            std::vector<PUID> primitives{};
        };

        /// @brief Color used to render the primitives.
        inline static
        Color primitiveColor = Color{ 0.0, 1.0, 0.1, 1.0 };

    public:
        /// @brief Meshes are created for nodes in multiples of this value.
        int meshDepth;

        /// @brief Standard ctor.
        /// @param meshDepth_ At which depths to create meshes
        BinaryTreeMesh(int meshDepth_) : meshDepth{ meshDepth_ } {}

        /// @brief Returns the root node.
        Node* Root() { return (root == nullptr ? (root = std::make_unique<Node>()).get() : root.get()); }

        /// @brief Creates meshes from the initialized structure.
        void CreateMeshes(IMeshManager const& meshManager);

    private:
        std::unique_ptr<Node> root{nullptr};
        IMeshManager const* meshManager{nullptr};

        //std::shared_ptr<TriangleMesh> triangleMesh;
        //std::shared_ptr<OrientedBoundingBoxMesh> boxMesh;

        std::unordered_set<PUID, PUID::Hash> ProcessNode(Node* node, int depth);
    };
}
