/**
 * File name: IRaytracer.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <optional>

#include "Graphics/OpenGL/Texture.hpp"
#include "SceneDefinition/Components/Renderer.hpp"
#include "RayHit.hpp"
#include "Utilities/Timer.hpp"

namespace ProperRT
{
    class INavigator;
    class IStructureValidator;
    class IAccelerationStructure;
    class TraversalStatistics;

    /// @brief Interface for raytracers.
    /// @details Implementing this interface allows the class to be used
    /// as a raytracer.
    class IRaytracer
    {
    public:
        enum class RenderOrigin {
            TopLeft, BottomLeft
        };

        inline static
        bool countTraversalStatistics{ true };
    public:
        /// @brief Initializes the raytracer.
        /// @details Guaranteed to be called once before any other method.
        virtual
        void Init() = 0;

        /// @brief Build the static part of the scene.
        /// @details Called once when loading a scene. Static game objects
        /// are guaranteed to not move, but can be disabled. To check if a renderer's
        /// game object is static, use Transform::IsStatic(). Use this method if you
        /// want to build a separate structure for or capitalize on static geometry.
        virtual
        void BuildStatic() = 0;

        /// @brief Build the dynamic part of the scene.
        /// @details Called every frame before rendering.
        virtual
        void BuildDynamic() = 0;

        /// @brief Render the scene.
        /// @details In OpenGL mode, raytracing 
        virtual
        void RaytraceScene() = 0;

        /// @brief Returns the rendered image.
        virtual
        ImageBuffer<Color32f> const& GetRender() = 0;

        /// @brief Returns the rendered image in an OpenGL texture.
        virtual
        OpenGL::Texture const& GetRenderTexture() = 0;

        /// @brief Call from the main thread before calling any other method in another thread each frame.
        virtual
        void UpdateOpenGLResources() {}

        /// @brief Registers @p renderer to be rendered every frame by this renderer.
        /// @details Called by renderers on Awake. The renderer is responsible for
        /// filtering out renderers that have been destroyed or disabled.
        /// @param renderer Renderer to register
        virtual
        void RegisterRenderer(CompPtr<Components::Renderer> renderer) = 0;

        /// @brief Returns a pointer to the top-level navigator, or nullptr if
        /// there is no navigator available.
        virtual
        INavigator* GetNavigator() { return nullptr; }

        /// @brief Returns a pointer to the structure validator, or nullptr if
        /// there is no navigator available.
        virtual
        IStructureValidator* GetValidator() { return nullptr; }

        /// @brief Casts @p ray and returns the result.
        /// @param ray Ray to cast
        /// @param maxDist Length of the ray
        /// @param traversalStats Traversal statistics, may be null
        /// @return Returns an empty optional if the ray did not hit any primitives, otherwise returns a ray hit class with informatino about the closes hit primitive
        virtual
        std::optional<RayHit> CastRay(Ray const& ray, cfloat maxDist = std::numeric_limits<cfloat>::infinity(), TraversalStatistics* traversalStats = nullptr) const = 0;

        /// @brief Casts a shadow @p ray and returns the result.
        /// @param ray Ray to cast
        /// @param maxDist Length of the ray
        /// @param traversalStats Traversal statistics, may be null
        /// @return Returns true if the ray hit any primitives
        virtual
        bool CastShadowRay(Ray const& ray, cfloat maxDist, TraversalStatistics* traversalStats = nullptr) const = 0;

        /// @brief Set in which corner the rays should originate from.
        virtual
        void SetRenderOrigin(RenderOrigin origin) = 0;

        /// @brief Returns a pointer to the acceleration structure this raytracer uses,
        /// or nullptr if it does not use one.
        virtual
        IAccelerationStructure* GetAccelerationStructure() { return nullptr; }

        /// @brief Wait until rendering is finished.
        virtual
        void WaitForFinish() = 0;

        /// @brief Default virtual dtor.
        virtual
        ~IRaytracer() = default;
    };
}
