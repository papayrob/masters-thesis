/**
 * File name: NoAccelStruct.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "NoAccelStruct.hpp"

namespace ProperRT
{
    void NoAccelStruct::BuildStatic(std::vector<CompPtr<Components::Renderer>>&& renderers_) {
        renderers = std::move(renderers_);
        staticRendererCount = StdSize<int32>(renderers);
    }

    void NoAccelStruct::BuildDynamic(std::vector<CompPtr<Components::Renderer>>&& renderers_) {
        renderers.resize(staticRendererCount);
        std::move(renderers_.begin(), renderers_.end(), std::back_inserter(renderers));
    }

    std::optional<RayHit> NoAccelStruct::Traverse(Ray const& ray, cfloat maxDist, TraversalStatistics* traversalStats) {
        //TOOD compute stats
        return TraverseInternal(ray, maxDist, false).second;
    }

    bool NoAccelStruct::TraverseShadow(Ray const& ray, cfloat maxDist, TraversalStatistics* traversalStats) {
        //TOOD compute stats
        return TraverseInternal(ray, maxDist, true).first;
    }

    std::pair<bool, std::optional<RayHit>> NoAccelStruct::TraverseInternal(Ray const& ray, cfloat maxDist, bool shadow) {
        struct HitInfo
        {
            Primitive primitive;
            cfloat t;
            IntersectionCoordinates ic;
            CompPtr<Components::Renderer> renderer;
            int32 submeshIdx;
        };

        HitInfo hit;
        hit.t = std::numeric_limits<cfloat>::infinity();

        // Intersect with scene
		for (CompPtr<Components::Renderer> const& renderer : renderers) {
			int submeshN = renderer->GetSubmeshCount();
			for (int submeshI = 0; submeshI < submeshN; ++submeshI) {
				auto submeshInfo = renderer->GetSubmeshInfo(submeshI);
				for (int primitiveIdx = 0; primitiveIdx < submeshInfo.submeshSize; ++primitiveIdx) {
					Primitive primitive = renderer->GetPrimitive(submeshI, primitiveIdx);
					cfloat t;
					IntersectionCoordinates ic;
					if (Intersect(ray, primitive, t, ic)) {
                        // Closest hit
                        if (shadow && t < maxDist) {
                            return std::make_pair<bool, std::optional<RayHit>>(true, {});
                        }
                        else if (t < hit.t && t <= maxDist) {
                            hit = HitInfo{ primitive, t, ic, renderer, submeshInfo.submeshIdx };
                        }
					}
				}
			}
		}

        if (hit.t < std::numeric_limits<cfloat>::infinity()) {
            return std::make_pair<bool, std::optional<RayHit>>(true, RayHit{ ray, hit.t, hit.ic, hit.primitive, hit.renderer, hit.submeshIdx });
        }
        else {
            return std::make_pair<bool, std::optional<RayHit>>(false, {});
        }
    }
}
