/**
 * File name: StructureRaytracer.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "StructureRaytracer.hpp"

#include "SceneDefinition/Components/Renderer.hpp"
#include "SceneDefinition/Components/Camera.hpp"
#include "SceneDefinition/Components/Light.hpp"
#include "KDTree/KDTreeCPU.hpp"
#include "KDTree/KDTreeGPU.hpp"
#include "KDTree/KDMergeTreeCPU.hpp"
#include "NoAccelStruct.hpp"
#include "PrimaryRayGenerator.hpp"
#include "RayHit.hpp"
#include "Utilities/Logger.hpp"
#include "RTApp.hpp"

namespace ProperRT
{
    namespace
    {
        Color ComputeLitSurfaceColor(Vector3cf const& lightDir, Vector3cf const& normal, Vector3cf const& viewerDir, Material const& material, Components::Light const& light, cfloat distanceIntensity) {
            Vector3cf reflection = Vector3cf::Reflect(-lightDir, normal);
            return
                // Diffuse
                material.colorDiffuse.a() * light.color.a() * distanceIntensity *
                std::abs(Vector3cf::Dot(normal, lightDir)) *
                Color{ light.color * material.colorDiffuse };
                // +
                // // Specular
                // pow(max(0.0_cf, Vector3cf::Dot(eyeDir, reflectDir)), mat.colorSpecular.a() + 0.001f) *
                // Color{ shadingData.lights[i].color * mat.colorSpecular };
        }
    }

    StructureRaytracer::StructureRaytracer(int threadN_)
        : threadN{ threadN_ }, threads{ std::make_unique<std::thread[]>(threadN) }
    {
    }

    StructureRaytracer::~StructureRaytracer() {
        Logger::LogTest(std::string{ "Average accel. structure build time: " } + buildTimer.GetAverageTimeString(Timer::Precision::Milliseconds));
        Logger::LogTest(std::string{ "Average scene render time: " } + raytraceTimer.GetAverageTimeString(Timer::Precision::Milliseconds));
    }

    void StructureRaytracer::Init() {
        accStructure = CreateStructure();
    }

    void StructureRaytracer::BuildStatic() {
        auto renderersRange = renderers.GetItems() | std::views::filter([] (CompPtr<Components::Renderer> const& r) {
            return r->GetComponent<Components::Transform>()->IsStatic();
        });
        std::vector<CompPtr<Components::Renderer>> renderersVec(renderersRange.begin(), renderersRange.end());

        buildTimer.Start();
        accStructure->BuildStatic(std::move(renderersVec));
        buildTimer.Stop();

        accStructure->GetStatistics().structureBuildTime = buildTimer.Elapsed();
        Logger::LogTest(std::string{ "Static accel. structure built in " } + buildTimer.GetElapsedString(Timer::Precision::Milliseconds));
        RTApp::Instance()->statistics.AddData("StaticBuildTime", buildTimer.Elapsed());

        buildTimer.Reset();
    }

    void StructureRaytracer::BuildDynamic() {
        auto renderersRange = renderers.GetItems() | std::views::filter([] (CompPtr<Components::Renderer> const& r) {
            return (r->doRender && !r->GetComponent<Components::Transform>()->IsStatic());
        });
        std::vector<CompPtr<Components::Renderer>> renderersVec(renderersRange.begin(), renderersRange.end());

        buildTimer.Start();
        accStructure->BuildDynamic(std::move(renderersVec));
        buildTimer.Stop();

        Logger::LogTest(std::string{ "Dynamic accel. structure built in " } + buildTimer.GetElapsedString(Timer::Precision::Milliseconds));
        accStructure->GetStatistics().structureBuildTime = buildTimer.Elapsed();
        RTApp::Instance()->statistics.AddData("DynamicBuildTime", buildTimer.Elapsed());
    }

    void StructureRaytracer::RaytraceScene() {
        using namespace Components;

        if (colorBuffer.Dimensions() != Camera::MainCamera()->Viewport()) {
            colorBuffer = ImageBuffer<Color32f>{ Camera::MainCamera()->Viewport(), Camera::MainCamera()->backgroundColor };
            if (RTApp::ShowGUI()) {
                gpuTexture = OpenGL::Texture{ Camera::MainCamera()->Viewport(), gl::GLenum::GL_RGBA32F };
            }
        }

        raytraceTimer.Start();

        traversalSteps.store(0);
        intersections.store(0);
        rayCount.store(0);

        // Spawn raytracing threads
        int remainder = Camera::MainCamera()->Viewport().y() % threadN;
        runningThreadN = threadN;
        for (int threadIdx = 0; threadIdx < threadN; ++threadIdx) {
            threads[threadIdx] = std::thread{[this, threadIdx, remainder] () {
                PrimaryRayGenerator rayGen{ Camera::MainCamera(), (renderOrigin == RenderOrigin::BottomLeft) };

                int32 chunk = rayGen.RowCount() / threadN;
                // Distribute remainder between threads, adding one to work chunks in the first x threads, x=remainder
                int32 offset;
                if (threadIdx < remainder) {
                    ++chunk;
                    offset = threadIdx * chunk;
                }
                else {
                    offset = remainder * (chunk + 1) + (threadIdx - remainder) * chunk;
                }

                // Row iterators
                auto rowIt = rayGen.Rows() + offset;
                auto rowEnd = rowIt + chunk;

                // Output buffer iterator
                auto cbufferIt = colorBuffer.buffer.begin() + offset * colorBuffer.Dimensions().x();
                TraversalStatistics traversalStats{};

                while (rowIt != rowEnd) {
                    // Trace primary ray
                    for (Ray primaryRay : *rowIt) {
                        // Init with background color
                        Color color = Color{ colorBuffer.clearValue };

                        // Intersect with scene
                        if (auto hitOptional = accStructure->Traverse(primaryRay, std::numeric_limits<cfloat>::infinity(), &traversalStats);
                            hitOptional.has_value()
                        ) {
                            color = Shade(primaryRay, hitOptional.value(), &traversalStats);
                        }

                        // Save color to output buffer
                        color.a() = 1.0f;
                        *cbufferIt = Color32f{ color }.Clamped(0.0f, 1.0f);
                        ++cbufferIt;
                    }

                    ++rowIt;
                }

                if (IRaytracer::countTraversalStatistics) {
                    traversalSteps.fetch_add(traversalStats.traversalSteps);
                    intersections.fetch_add(traversalStats.intersections);
                    rayCount.fetch_add(traversalStats.queryCount);
                }

                if (runningThreadN.fetch_sub(1) == 1) {
                    raytraceTimer.Stop();
                }
            }};
        }

        renderOnGPU = false;
    }

    ImageBuffer<Color32f> const& StructureRaytracer::GetRender() {
        WaitForFinish();

        return colorBuffer;
    }

    OpenGL::Texture const& StructureRaytracer::GetRenderTexture() {
        WaitForFinish();

        if (!renderOnGPU) {
            gpuTexture.Upload(colorBuffer);
            renderOnGPU = true;
        }

        return gpuTexture;
    }

    void StructureRaytracer::UpdateOpenGLResources() {
        using namespace Components;
        if (colorBuffer.Dimensions() != Camera::MainCamera()->Viewport()) {
            colorBuffer = ImageBuffer<Color32f>{ Camera::MainCamera()->Viewport(), Camera::MainCamera()->backgroundColor };
            if (RTApp::ShowGUI()) {
                gpuTexture = OpenGL::Texture{ Camera::MainCamera()->Viewport(), gl::GLenum::GL_RGBA32F };
            }
        }
    }

    std::optional<RayHit> StructureRaytracer::CastRay(Ray const &ray, cfloat maxDist, TraversalStatistics* traversalStats) const {
        return accStructure->Traverse(ray, maxDist, traversalStats);
    }

    bool StructureRaytracer::CastShadowRay(Ray const &ray, cfloat maxDist, TraversalStatistics* traversalStats) const {
        return accStructure->TraverseShadow(ray, maxDist, traversalStats);
    }

    void StructureRaytracer::RegisterRenderer(CompPtr<Components::Renderer> renderer) {
        renderers.Register(renderer);
    }

    Color StructureRaytracer::Shade(Ray const& incomingRay, RayHit const& hitInfo, TraversalStatistics* traversalStats) const {
        Color color{ /*Components::Light::ambientColor*/ };
        
        // Sum phong model calculations for each light
        for (auto const& light : Components::Light::lightRegistry.GetItems()) {
            auto directionData = light->DirectToLight(hitInfo.point);
            if (!directionData.IsOutOfRange()) {
                Ray shadowRay{ hitInfo.point, directionData.direction };

                // Cast a shadow ray, if the light is not occluded compute phong lighting
                if (!CastShadowRay(shadowRay, directionData.distance, traversalStats)) {
                    color += ComputeLitSurfaceColor(
                        shadowRay.direction,
                        hitInfo.normal,
                        (incomingRay.origin - hitInfo.point).Normalized(),
                        hitInfo.GetMaterial(),
                        *light,
                        directionData.distanceIntensity
                    );
                }
            }
        }

        return color;
    }

    void StructureRaytracer::WaitForFinish() {
        // Wait for the raytracing threads to finish
        if (threads[0].joinable()) {
            for (int threadIdx = 0; threadIdx < threadN; ++threadIdx) {
                threads[threadIdx].join();
            }

            Logger::LogTest(std::string{ "Scene rendered in " } + raytraceTimer.GetElapsedString(Timer::Precision::Milliseconds));

            auto& stats = accStructure->GetStatistics();

            stats.queryCount = rayCount.load();
            stats.avgIntersectionsPerQuery = static_cast<float>(intersections.load()) / static_cast<float>(stats.queryCount);
            stats.avgTraversalStepsPerQuery = static_cast<float>(traversalSteps.load()) / static_cast<float>(stats.queryCount);

            using DurType = decltype(stats.averageQueryTime);
            stats.averageQueryTime = std::chrono::duration_cast<DurType>(raytraceTimer.Elapsed()) / static_cast<double>(stats.queryCount);
            stats.performance = (1.0 / std::chrono::duration_cast<Timer::DurationReal>(stats.averageQueryTime).count());

            auto& appStats = RTApp::Instance()->statistics;
            appStats.AddData("AverageQueryTime", stats.averageQueryTime);
            appStats.AddData("AverageIntersectionsPerQuery", stats.avgIntersectionsPerQuery);
            appStats.AddData("AverageTraversalStepsPerQuery", stats.avgTraversalStepsPerQuery);
            appStats.AddData("QueryCount", stats.queryCount);
        }
    }

    std::unique_ptr<IAccelerationStructure> StructureRaytracer::CreateStructure() const {
        auto const& sceneConfig = Scene::GetActiveScene()->Config();
        if (std::string structureStr = sceneConfig.GetValue<std::string>("structure", "KDTreeCPU"); structureStr == "KDTreeCPU") {
            auto tree = std::make_unique<KDTreeCPU>();
            tree->parameters = CreateKDTreeParameters();

            return tree;
        }
        else if (structureStr == "KDTreeGPU") {
            auto tree = std::make_unique<KDTreeGPU>();
            tree->parameters = CreateKDTreeParameters();

            return tree;
        }
        else if (structureStr == "KDMergeTreeCPU") {
            auto tree = std::make_unique<KDMergeTreeCPU>();
            tree->parameters = CreateKDTreeParameters();

            return tree;
        }
        else {
            auto tree = std::make_unique<NoAccelStruct>();
            return tree;
        }
    }
}
