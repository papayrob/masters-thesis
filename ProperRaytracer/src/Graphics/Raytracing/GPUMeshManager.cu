/**
 * File name: GPUMeshManager.cu
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "GPUMeshManager.cuh"

namespace ProperRT
{
    namespace GPUMeshManagement::Kernels
    {
        __global__
        void InstanceGetPrimitive(Instance instance, int32 idx, DevicePointer<Primitive> primitive) {
            *primitive = instance.GetPrimitive(idx);
        }
    }

    __host__
    void GPUMeshManager::UploadLights() {
        std::vector<Components::Light::GPUData> lightsCPU;
        for (auto const& light : Components::Light::lightRegistry.GetItems()) {
            lightsCPU.push_back(light->GetGPUData());
        }

        if (StdSize<int64>(lightsCPU) == lightsGPU.Count()) {
            lightsGPU.Upload(lightsCPU);
        }
        else {
            lightsGPU = DeviceUniquePointer<Components::Light::GPUData[]>::AllocateAndUpload(lightsCPU);
        }
    }
    
    __host__
    std::pair<DeviceUniquePointer<PUID[]>, DeviceUniquePointer<AABB[]>> GPUMeshManager::CreateInstances(std::span<CompPtr<Components::Renderer>> renderers) {
        auto submeshN = std::reduce(renderers.begin(), renderers.end(), int32{ 0 }, [] (int32 sum, CompPtr<Components::Renderer> const& renderer) { return sum + renderer->GetSubmeshCount(); }); //TODO change to transform_reduce
        instanceRenderers.clear();
        instancesCPU.resize(submeshN);
        std::vector<PUID> primitivesCPU{};
        std::vector<AABB> primitiveBBsCPU{};
        sceneBoundingBox = AABB::InfinitelySmall();

        submeshN = 0;
        int32 instanceI = 0;
        for (CompPtr<Components::Renderer> renderer : renderers) {
            auto submeshCount = renderer->GetSubmeshCount();
            renderer->CreateSubmeshInstances(std::span(instancesCPU).subspan(submeshN, submeshCount));
            submeshN += submeshCount;
            for (int submeshI = 0; submeshI < submeshCount; ++submeshI, ++instanceI) {
                auto info = renderer->GetSubmeshInfo(submeshI);
                for (int32 primitiveI = 0; primitiveI < info.submeshSize; ++primitiveI) {
                    primitivesCPU.push_back(PUID{ instanceI, primitiveI });

                    // Compute and include primitive bounding box in the scene bounding box
                    AABB primitiveBB = GetAABB(renderer->GetPrimitive(submeshI, primitiveI));
                    primitiveBBsCPU.push_back(primitiveBB);
                    sceneBoundingBox.Include(primitiveBB);
                }
                
                instanceRenderers.push_back({renderer, submeshI});
            }
        }

        DeviceUniquePointer<PUID[]> primitivesGPU{nullptr};
        DeviceUniquePointer<AABB[]> primitiveBBsGPU{nullptr};

        if (instancesCPU.size() > 0) {
            instancesGPU = DeviceUniquePointer<GPUMeshManagement::Instance[]>::AllocateAndUpload(instancesCPU);

            if (primitivesCPU.size() > 0) {

                primitivesGPU = DeviceUniquePointer<PUID[]>::AllocateAndUpload(primitivesCPU);
                primitiveBBsGPU = DeviceUniquePointer<AABB[]>::AllocateAndUpload(primitiveBBsCPU);

                primitiveN = StdSize<int32>(primitivesCPU);
            }
            else {
                primitiveN = 0;
                sceneBoundingBox = AABB{};
            }
        }
        else {
            instancesGPU = nullptr;
            sceneBoundingBox = AABB{};
        }

        return std::make_pair(std::move(primitivesGPU), std::move(primitiveBBsGPU));
    }

    void GPUMeshManager::FromCPUMeshInstances(IMeshManager& meshManager, std::span<CPUMeshManager::MeshInstance> cpuInstances) {
        instanceRenderers.clear();
        instancesCPU.resize(cpuInstances.size());
        sceneBoundingBox = meshManager.SceneBoundingBox();

        int32 submeshN = 0;
        int32 instanceI = 0;
        CompPtr<Components::Renderer> currentRenderer{nullptr};
        for (CPUMeshManager::MeshInstance const& cpuInstance : cpuInstances) {
            if (cpuInstance.renderer != currentRenderer) {
                currentRenderer = cpuInstance.renderer;
                auto submeshCount = currentRenderer->GetSubmeshCount();
                currentRenderer->CreateSubmeshInstances(std::span(instancesCPU).subspan(submeshN, submeshCount));
                submeshN += submeshCount;

                for (int submeshI = 0; submeshI < submeshCount; ++submeshI, ++instanceI) {
                    instanceRenderers.push_back({currentRenderer, submeshI});
                }
            }
        }

        if (instancesCPU.size() > 0) {
            instancesGPU = DeviceUniquePointer<GPUMeshManagement::Instance[]>::AllocateAndUpload(instancesCPU);
            primitiveN = meshManager.PrimitiveCount();
        }
        else {
            instancesGPU = nullptr;
            sceneBoundingBox = AABB{};
        }
    }

    __host__
    int64 GPUMeshManager::AllocatedMemory() const {
        return
            std::span{instanceRenderers}.size_bytes() +
            std::span{instancesCPU}.size_bytes() +
            instancesGPU.Bytes() +
            lightsGPU.Bytes();
    }
}
