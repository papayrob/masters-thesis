/**
 * File name: PUID.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <utility>
#include "ProperRT.hpp"

namespace ProperRT
{
    /// @brief Primitive Unique IDentifier class.
    /// @details Consists of an instance ID (groups primitives with the same type, transform, material, etc.)
    /// and a primitive offset inside the instance. Unique to a mesh manager it was created by.
    struct PUID
    {
        /// @brief Hash for the PUID type.
        struct Hash
        {
            /// @brief Hashes the PUID as a 64-bit unsigned integer.
            auto operator()(PUID const& puid) const noexcept {
                return std::hash<uint64>{}((static_cast<uint64>(puid.instanceID) << 32) | static_cast<uint64>(puid.primitiveOffset));
            }
        };

        /// @brief Index of the instance (groups primitives with the same type, transform, material, etc.).
        int32 instanceID;
        /// @brief Index of the primitive inside the instance group.
        int32 primitiveOffset;

        auto operator<=>(PUID const&) const noexcept = default;
        bool operator==(PUID const& other) const noexcept = default;
    };
}