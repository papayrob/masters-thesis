/**
 * File name: NoAccelStruct.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "IAccelerationStructure.hpp"

namespace ProperRT
{
    /// @brief Special brute-force implementation of the acceleration structure interface that does not use any data structure, used for testing.
    class NoAccelStruct : public IAccelerationStructure
    {
    public:
        virtual
        void BuildStatic(std::vector<CompPtr<Components::Renderer>>&& renderers) override;

        virtual
        void BuildDynamic(std::vector<CompPtr<Components::Renderer>>&& renderers) override;

        virtual
        bool IsBuilt() const override { return true; }

        virtual
        std::optional<RayHit> Traverse(Ray const& ray, cfloat maxDist = std::numeric_limits<cfloat>::infinity(), TraversalStatistics* traversalStats = nullptr) override;

        virtual
        bool TraverseShadow(Ray const& ray, cfloat maxDist, TraversalStatistics* traversalStats = nullptr) override;

        virtual
        AccStructStats const& GetStatistics() const override { return stats; };

        virtual
        AccStructStats& GetStatistics() override { return stats; }

    private:
        std::vector<CompPtr<Components::Renderer>> renderers;
        int32 staticRendererCount{};
        AccStructStats stats;

        std::pair<bool, std::optional<RayHit>> TraverseInternal(Ray const& ray, cfloat maxDist, bool shadow);
    };
}
