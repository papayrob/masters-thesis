/**
 * File name: BinaryTreeMesh.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "BinaryTreeMesh.hpp"

namespace ProperRT
{
    void BinaryTreeMesh::CreateMeshes(IMeshManager const& meshManager_) {
        meshManager = &meshManager_;
        ProcessNode(Root(), 0);
    }

    std::unordered_set<PUID, PUID::Hash> BinaryTreeMesh::ProcessNode(Node* node, int depth) {
        struct PrimitivePusher
        {
            std::vector<Triangle>& triangles;
            std::vector<OrientedBoundingBox>& boxes;

            void operator()(Triangle const& tri) const {
                triangles.push_back(tri);
            }

            void operator()(OrientedBoundingBox const& box) const {
                boxes.push_back(box);
            }
        };

        std::unordered_set<PUID, PUID::Hash> puids;

        if (node->left != nullptr) {
            auto left = ProcessNode(node->left.get(), depth + 1);
            puids.insert(left.begin(), left.end());
        }

        if (node->right != nullptr) {
            auto right = ProcessNode(node->right.get(), depth + 1);
            puids.insert(right.begin(), right.end());
        }

        puids.insert(node->primitives.begin(), node->primitives.end());
        node->primitives = std::vector<PUID>(puids.begin(), puids.end());

        if (depth % meshDepth == 0 || (node->left == nullptr && node->right == nullptr)) {
            std::vector<Triangle> triangles;
            std::vector<OrientedBoundingBox> boxes;
            PrimitivePusher pusher{ triangles, boxes };

            for (auto puid : puids) {
                cuda::std::visit(pusher, meshManager->GetPrimitive(puid));
            }

            if (!triangles.empty()) {
                node->triangleMesh = std::make_shared<TriangleMesh>();
                node->triangleMesh->SetTriangles(triangles);
                node->triangleMesh->SetMaterial(Material{.colorDiffuse{NormalizedConvert<Color32f>(primitiveColor)}});
            }

            if (!boxes.empty()) {
                node->boxMesh = std::make_shared<OrientedBoundingBoxMesh>(boxes);
                node->boxMesh->SetMaterial(Material{.colorDiffuse{NormalizedConvert<Color32f>(primitiveColor)}});
            }

            node->isAnchor = true;
        }

        return puids;
    }
    
    void BinaryTreeMesh::Node::Render(RasterizationMatrices const& rasterizationMatrices) {
        // End recursion and render
        if (isAnchor) {
            if (triangleMesh != nullptr) {
                triangleMesh->Rasterize(Matrix4x4f::Identity(), rasterizationMatrices);
            }

            if (boxMesh != nullptr) {
                boxMesh->Rasterize(Matrix4x4f::Identity(), rasterizationMatrices);
            }
        }
        // Continue recursion until anchor node with meshes is found
        else {
            if (left != nullptr) {
                left->Render(rasterizationMatrices);
            }

            if (right != nullptr) {
                right->Render(rasterizationMatrices);
            }
        }
    }
}