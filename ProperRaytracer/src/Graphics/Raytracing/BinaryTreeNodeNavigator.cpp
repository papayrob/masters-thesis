/**
 * File name: BinaryTreeNodeNavigator.cpp
 * Description: See BinaryTreeNavigator header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "BinaryTreeNavigator.hpp"

#include "InputSystem/Input.hpp"
#include "SceneDefinition/Components/Camera.hpp"
#include "Geometry/TriangleMesh.hpp"

namespace ProperRT
{
    BinaryTreeNodeNavigator::BinaryTreeNodeNavigator(BinaryTreeNavigator *parentNavigator_)
        : parentNavigator{ parentNavigator_ }
    {
    }

    void BinaryTreeNodeNavigator::StartNavigating() {
        if (parentNavigator->node->PrimitiveCount() < 1) {
            throw std::runtime_error{"Cannot navigate node with no primitives"};
        }
        
        primitiveIdx = std::clamp(primitiveIdx, 0, parentNavigator->node->PrimitiveCount() - 1);
        nodeBoundingBox = parentNavigator->nodeBoundingBox;
        UpdateMeshes();
    }

    void BinaryTreeNodeNavigator::Navigate() {
        if (primitiveIdx > 0) {
            if (ImGui::Button("<-") || Input::GetKeyState(vkfw::Key::eLeft).HasReleased()) {
                --primitiveIdx;
                UpdateMeshes();
            }
        }
        else {
            ImGui::BeginDisabled();
            ImGui::Button("<-");
            ImGui::EndDisabled();
        }

        ImGui::SameLine();

        if (ImGui::SliderInt("##Primitive slider", &primitiveIdx, 0, parentNavigator->node->PrimitiveCount() - 1, "%d", ImGuiSliderFlags_AlwaysClamp)) {
            UpdateMeshes();
        }

        ImGui::SameLine();

        if (primitiveIdx < parentNavigator->node->PrimitiveCount() - 1) {
            if (ImGui::Button("->") || Input::GetKeyState(vkfw::Key::eRight).HasReleased()) {
                ++primitiveIdx;
                UpdateMeshes();
            }
        }
        else {
            ImGui::BeginDisabled();
            ImGui::Button("->");
            ImGui::EndDisabled();
        }
    }

    void BinaryTreeNodeNavigator::NavigateOut() {
    }

    void BinaryTreeNodeNavigator::UpdateMeshes() {
        auto primitive = parentNavigator->node->GetPrimitive(primitiveIdx);

        struct MeshMaker
        {
            std::shared_ptr<Mesh>& mesh;

            void operator()(Triangle const& triangle) {
                std::shared_ptr<TriangleMesh> m = std::dynamic_pointer_cast<TriangleMesh>(mesh);
                if (m == nullptr) {
                    m = std::make_shared<TriangleMesh>();
                    m->SetTriangles(std::vector<Triangle>{ triangle });
                    m->SetMaterial(Material{.colorDiffuse{NormalizedConvert<Color32f>(selectedColor)}});
                }
                else {
                    m->Triangles()[0] = triangle;
                }

                mesh = std::move(m);
            }

            void operator()(OrientedBoundingBox const& boundingBox) {
                std::shared_ptr<OrientedBoundingBoxMesh> m = std::dynamic_pointer_cast<OrientedBoundingBoxMesh>(mesh);
                if (m == nullptr) {
                    m = std::make_shared<OrientedBoundingBoxMesh>(std::vector<OrientedBoundingBox>{ boundingBox });
                    m->SetMaterial(Material{.colorDiffuse{NormalizedConvert<Color32f>(selectedColor)}});
                }

                m->Boxes()[0] = boundingBox;

                mesh = std::move(m);
            }
        };

        CuSTD::visit(MeshMaker{ selectedPrimitive }, primitive);
        selectedPrimitive->SyncToGPU();
    }
}
