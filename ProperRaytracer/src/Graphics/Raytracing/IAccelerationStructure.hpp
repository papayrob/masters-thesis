/**
 * File name: IAccelerationStructure.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "SceneDefinition/Components/Renderer.hpp"
#include "INavigator.hpp"
#include "IStructureValidator.hpp"
#include "RayHit.hpp"
#include "Utilities/Timer.hpp"

namespace ProperRT
{
    /// @brief Class for storing statistics of an acceleration structure.
    struct AccelerationStructureStatistics
    {
        int64 memoryConsumption{ -1 };
        double performance{ -1. };
        double cost{ -1. };

        Timer::Clock::duration structureBuildTime{ -1 };
        Timer::DurationNanoReal averageQueryTime{ -1 };

        float avgIntersectionsPerQuery{ -1.f };
        float avgTraversalStepsPerQuery{ -1.f };
        int32 queryCount{ -1 };

        int32 innerNodes{ -1 };
        int32 leafNodes{ -1 };
        int32 emptyNodes{ -1 };
        float avgPrimitivesInLeaf{ -1.f };
        int32 maxPrimitivesInLeaf{ -1 };
        float avgDepth{ -1.f };
        int32 maxDepth{ -1 };

        /// @brief Resets the build related statistics.
        void ResetBuildStats() {
            memoryConsumption = 0;
            cost = 0.0;
            structureBuildTime = Timer::Clock::duration{0};
            innerNodes = 0;
            leafNodes = 0;
            emptyNodes = 0;
            avgPrimitivesInLeaf = 0.f;
            maxPrimitivesInLeaf = 0;
            avgDepth = 0.f;
            maxDepth = 0;
        }

        /// @brief Resets the ray tracing related statistics.
        void ResetRaytracingStats() {
            performance = 0.0;
            averageQueryTime = Timer::DurationNanoReal{0};
            avgIntersectionsPerQuery = 0.f;
            avgTraversalStepsPerQuery = 0.f;
            queryCount = 0;
        }
    };

    /// @brief Short alias for AccelerationStructureStatistics.
    using AccStructStats = AccelerationStructureStatistics;

    /// @brief Class for storing traversal statistics.
    struct TraversalStatistics
    {
        int64 traversalSteps{};
        int64 intersections{};
        int64 queryCount{};

        friend CUDA_FUNCTION
        TraversalStatistics operator+(TraversalStatistics const& lhs, TraversalStatistics const& rhs) {
            return TraversalStatistics{
                lhs.traversalSteps + rhs.traversalSteps,
                lhs.intersections + rhs.intersections,
                lhs.queryCount + rhs.queryCount
            };
        }
    };

    /// @brief Interface class for acceleration structures, used by StructureRaytracer.
    class IAccelerationStructure
    {
    public:
        virtual
        ~IAccelerationStructure() = default;
        
        /// @brief Builds the structure for static (non-moving) models on scene switch. Might not be used.
        /// @param renderers List of static active renderers in the current scene
        virtual
        void BuildStatic(std::vector<CompPtr<Components::Renderer>>&& renderers) = 0;

        /// @brief Builds the structure for dynamic (moving/morphing) models each frame. If static building is not used, might also be used universally.
        /// @param renderers List of active renderers in the current scene, including static ones
        virtual
        void BuildDynamic(std::vector<CompPtr<Components::Renderer>>&& renderers) = 0;

        /// @brief Returns true if the structure is built and ready to be used, false otherwise.
        virtual
        bool IsBuilt() const = 0;

        /// @brief Returns the pointer to the top-most navigator, or null if there is no navigator implemented.
        virtual
        INavigator* TopNavigator() { return nullptr; }

        /// @brief Returns the pointer to the structure validator, or null if there is no navigator implemented.
        virtual
        IStructureValidator* Validator() { return nullptr; }

        /// @brief Traverses the acceleration data structure with @p ray and returns the result.
        /// @param ray Ray to traverse with
        /// @param maxDist Length of the ray
        /// @param traversalStats Traversal statistics, may be null
        /// @return Returns an empty optional if the ray did not hit any primitives, otherwise returns a ray hit class with informatino about the closes hit primitive
        virtual
        std::optional<RayHit> Traverse(Ray const& ray, cfloat maxDist = std::numeric_limits<cfloat>::infinity(), TraversalStatistics* traversalStats = nullptr) = 0;

        /// @brief Traverses the acceleration data structure with @p ray for shadow rays and returns the result.
        /// @param ray Ray to traverse with
        /// @param maxDist Length of the ray
        /// @param traversalStats Traversal statistics, may be null
        /// @return Returns true if the ray hit any primitives
        virtual
        bool TraverseShadow(Ray const& ray, cfloat maxDist, TraversalStatistics* traversalStats = nullptr) = 0;

        /// @brief Returns the instance of traversal statistics.
        virtual
        AccStructStats const& GetStatistics() const = 0;

        /// @brief Returns the instance of traversal statistics.
        virtual
        AccStructStats& GetStatistics() = 0;
    };
}
