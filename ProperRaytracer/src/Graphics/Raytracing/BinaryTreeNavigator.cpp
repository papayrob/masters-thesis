/**
 * File name: BinaryTreeNavigator.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "BinaryTreeNavigator.hpp"

#include "InputSystem/Input.hpp"
#include "SceneDefinition/Components/Camera.hpp"

namespace ProperRT
{
    BinaryTreeNavigator::BinaryTreeNavigator(NavigatableBinaryTree* tree_)
        : tree{ tree_ }
    {
    }

    void BinaryTreeNavigator::StartNavigating() {
        navigationChain.clear();
        node = tree->NavigationRoot();
        nodeBoundingBox = std::make_shared<OrientedBoundingBoxMesh>(std::vector<OrientedBoundingBox>{ node->BoundingBox() });
        nodeBoundingBox->SetMaterial(Material{.colorDiffuse{NormalizedConvert<Color32f>(boundingBoxColor)}});
        UpdateMeshes();
    }

    void BinaryTreeNavigator::Navigate() {
        SetButtons();

        // Get clicked button
        enum class Button { None, Up, Left, Right, Neigh };
        Button b = Button::None;

        ImGui::BeginTable("Table", 3, ImGuiTableFlags_SizingFixedFit);

        ImGui::TableNextRow();
        ImGui::TableSetColumnIndex(1);
        if (!disableUp) {
            if (ImGui::Button("U") || Input::GetKeyState(vkfw::Key::eUp).HasReleased()) {
                b = Button::Up;
            }
        }
        else {
            ImGui::BeginDisabled();
            ImGui::Button("U");
            ImGui::EndDisabled();
        }

        ImGui::TableNextRow();
        ImGui::TableNextColumn();
        if (!disableLeft) {
            if (ImGui::Button("L") || Input::GetKeyState(vkfw::Key::eLeft).HasReleased()) {
                b = Button::Left;
            }
        }
        else {
            ImGui::BeginDisabled();
            ImGui::Button("L");
            ImGui::EndDisabled();
        }

        ImGui::TableNextColumn();

        if (!disableNeigh) {
            if (ImGui::Button("S") || Input::GetKeyState(vkfw::Key::eDown).HasReleased()) {
                b = Button::Neigh;
            }
        }
        else {
            ImGui::BeginDisabled();
            ImGui::Button("S");
            ImGui::EndDisabled();
        }

        ImGui::TableNextColumn();

        if (!disableRight) {
            if (ImGui::Button("R") || Input::GetKeyState(vkfw::Key::eRight).HasReleased()) {
                b = Button::Right;
            }
        }
        else {
            ImGui::BeginDisabled();
            ImGui::Button("R");
            ImGui::EndDisabled();
        }

        ImGui::EndTable();

        // Navigate according to button
        switch (b)
        {
        case Button::Up: {
            NavigateUp();
            break;
        }
        case Button::Neigh: {
            NavigateSwitch();
            break;
        }
        case Button::Left: {
            NavigateLeft();
            break;
        }
        case Button::Right: {
            NavigateRight();
            break;
        }
        default:
            break;
        }

        if (b != Button::None) {
            navigationChainString = CreateNavigationChainString();
            UpdateMeshes();
        }

        // Display navigation chain
        ImGui::Text("Navigation history:");
        ImGui::TextWrapped(navigationChainString.c_str());

        SetButtons();
    }

    void BinaryTreeNavigator::NavigateOut() {
        navigationChain.clear();
    }

    INavigator* BinaryTreeNavigator::NavigateInto() {
        if (nodeNavigator == nullptr) {
            nodeNavigator = CreateNodeNavigator();
        }

        nodeNavigator->StartNavigating();

        return nodeNavigator.get();
    }

    void BinaryTreeNavigator::NavigateUp() {
        node->GoUp();
        navigationChain.pop_back();
    }

    void BinaryTreeNavigator::NavigateLeft() {
        node->GoLeft();
        navigationChain.push_back(Side::Left);
    }

    void BinaryTreeNavigator::NavigateRight() {
        node->GoRight();
        navigationChain.push_back(Side::Right);
    }

    void BinaryTreeNavigator::NavigateSwitch() {
        node->GoUp();
        navigationChain.back() = static_cast<Side>((ToUnderlying(navigationChain.back()) + 1) % 2);
        switch (navigationChain.back())
        {
        case Side::Left:
            node->GoLeft();
            break;
        case Side::Right:
            node->GoRight();
            break;
        default:
            break;
        }
    }

    void BinaryTreeNavigator::UpdateMeshes() {
        nodeBoundingBox->Boxes()[0] = node->BoundingBox();
        nodeBoundingBox->SyncToGPU();
    }

    std::string BinaryTreeNavigator::CreateNavigationChainString() const {
        std::stringstream ss;
        for (auto it = navigationChain.begin(); it != navigationChain.end(); ++it) {
            ss << (*it == Side::Left ? 'L' : 'R');

            if (it != navigationChain.end() - 1) ss << " -> ";
        }

        return ss.str();
    }
}
