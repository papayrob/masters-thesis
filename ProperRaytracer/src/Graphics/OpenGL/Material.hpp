/**
 * File name: Material.hpp
 * Description: Unused and not implemented.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "glbinding/gl/types.h"

#include "Shader.hpp"

namespace ProperRT::OpenGL
{
    class Material
    {
    public:
        enum class SSBOBindings {
            VertexPositions = 0, VertexAttributes = 1, Indices = 2
        };

    public:
        Material(Shader shader);

        gl::GLint GetUniformLocation(std::string const& uniformName);

        gl::GLint GetAttributeLocation(std::string const& attribName);

    private:
        Shader shader{};
        //Texture textures;
    };
}
