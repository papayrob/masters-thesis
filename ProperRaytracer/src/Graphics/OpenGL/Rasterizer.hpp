/**
 * File name: Rasterizer.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2023 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <memory>
#include <map>
#include "glbinding/gl/types.h"

#include "Geometry/GenericMesh.hpp"
#include "SceneDefinition/Components/Renderer.hpp"

namespace ProperRT
{
    /// @brief Renderer based on OpenGL. Renders the scene without shading, only using the depth buffer.
    class Rasterizer
    {
    public:
        /// @brief Default ctor.
        Rasterizer();

        ~Rasterizer();

        /// @brief Registers @p renderer to be rasterized by this rasterizer every frame.
        /// @param renderer Renderer to register
        void RegisterRenderer(CompPtr<Components::Renderer> renderer);

        /// @brief Render the currently active scene using OpenGL.
        void Render();

        /// @brief Returns the OpenGL shader storage buffer with uploaded lights.
        gl::GLuint LightBuffer() { return lightBuffer; }

    private:
        std::map<std::weak_ptr<GenericMesh>, std::vector<CompPtr<Components::Renderer>>, std::owner_less<>> meshMap;
        int lightBufferSize{0};
        gl::GLuint lightBuffer{0};

        void UploadLights();
    };
}
