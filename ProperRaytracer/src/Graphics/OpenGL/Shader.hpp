/**
 * File name: Shader.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <string_view>
#include <optional>
#include "glbinding/gl/types.h"

#include "SceneDefinition/AssetID.hpp"

namespace ProperRT::OpenGL
{
    /// @brief Class for storing and manipulating OpenGL shader programs.
    class Shader
    {
    public:
        /// @brief Class for passing shader sources to the Shader constructor.
        struct Sources
        {
            std::string_view vertexSource{};
            std::optional<std::string_view> geometrySource{};
            std::optional<std::string_view> tessellationControlSource{};
            std::optional<std::string_view> tessellationEvalSource{};
            std::optional<std::string_view> fragmentSource{};
        };

    public:
        /// @brief Loads shader source from @p assetID.
        /// @param assetID Asset id of the shader
        /// @return Source of the shader in a string
        static
        std::string LoadShaderSource(AssetID const& assetID);

    public:
        /// @brief Default ctor.
        Shader() noexcept = default;

        /// @brief Creates a shader from @p sources.
        /// @param sources Pass all shader sources that you want compiled and linked into the shader program
        Shader(Sources const& sources);

        ~Shader();

        Shader(Shader const&) = delete;
        Shader& operator=(Shader const&) = delete;

        Shader(Shader&& other) noexcept;
        Shader& operator=(Shader&& other) noexcept;

        /// @brief Binds the shader.
        void Use() const noexcept;

        /// @brief Returns the location of a uniform with @p name.
        /// @param name Name of the uniform to retrieve the location of
        gl::GLuint GetUniformLocation(std::string const& name) const;

        /// @brief Returns the program identifier of this shader for binding.
        gl::GLuint Program() const noexcept { return program; }

    private:
        gl::GLuint program{ 0 };
    };
}