/**
 * File name: Material.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "Material.hpp"

#include "glbinding/gl/gl.h"

namespace ProperRT::OpenGL
{
    Material::Material(Shader shader_)
        : shader{ std::move(shader_) }
    {}

    gl::GLint Material::GetUniformLocation(std::string const& uniformName) {
        return gl::glGetUniformLocation(shader.Program(), uniformName.c_str());
    }

    gl::GLint Material::GetAttributeLocation(std::string const& attribName) {
        return gl::glGetAttribLocation(shader.Program(), attribName.c_str());
    }
}
