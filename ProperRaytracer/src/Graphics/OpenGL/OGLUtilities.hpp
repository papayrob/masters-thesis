/**
 * File name: OGLUtilities.hpp
 * Description: Utility methods for OpenGL related code.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <span>
#include "glbinding/gl/gl.h"

namespace ProperRT
{
    /// @brief Creates an OpenGL named buffer and uploads @p data.
    /// @tparam T Data type
    /// @param data Data to be uploaded, also determines the size of the created buffer
    /// @return Handle of the created buffer
    template<class T>
    inline
    gl::GLuint CreateOGLBuffer(std::span<T const> data) {
        gl::GLuint buffer;
        gl::glCreateBuffers(1, &buffer);
        gl::glNamedBufferStorage(buffer, data.size_bytes(), data.data(), gl::BufferStorageMask::GL_DYNAMIC_STORAGE_BIT);
        return buffer;
    }

    /// @brief Creates an OpenGL named buffer and uploads @p data.
    /// @tparam T Data type
    /// @param data Data to be uploaded, also determines the size of the created buffer
    /// @return Handle of the created buffer
    template<class T>
    inline
    gl::GLuint CreateOGLBuffer(std::span<T> data) {
        return CreateOGLBuffer<T const>(std::span<T const>{ data });
    }

    /// @brief Template metaprogramming class for getting OpenGL type enum from c++ types.
    template<typename T>
    struct GLType {};
    
    template<>
    struct GLType<float>
    {
        inline static constexpr gl::GLenum TYPE = gl::GLenum::GL_FLOAT;
    };

    template<>
    struct GLType<double>
    {
        inline static constexpr gl::GLenum TYPE = gl::GLenum::GL_DOUBLE;
    };

    template<>
    struct GLType<int8>
    {
        inline static constexpr gl::GLenum TYPE = gl::GLenum::GL_BYTE;
    };

    template<>
    struct GLType<uint8>
    {
        inline static constexpr gl::GLenum TYPE = gl::GLenum::GL_UNSIGNED_BYTE;
    };

    template<>
    struct GLType<int16>
    {
        inline static constexpr gl::GLenum TYPE = gl::GLenum::GL_SHORT;
    };

    template<>
    struct GLType<uint16>
    {
        inline static constexpr gl::GLenum TYPE = gl::GLenum::GL_UNSIGNED_SHORT;
    };

    template<>
    struct GLType<int32>
    {
        inline static constexpr gl::GLenum TYPE = gl::GLenum::GL_INT;
    };

    template<>
    struct GLType<uint32>
    {
        inline static constexpr gl::GLenum TYPE = gl::GLenum::GL_UNSIGNED_INT;
    };
}