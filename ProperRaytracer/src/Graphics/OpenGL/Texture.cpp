/**
 * File name: Texture.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "Texture.hpp"

#include "OGLUtilities.hpp"
#include "Shader.hpp"

namespace{

char const* vertexShader = R"(
    #version 460 core

    smooth out vec2 uv;

    void main() {
        vec4 vertices[3] = vec4[3](vec4(-1.0, -1.0, 0.5, 1.0), vec4(3.0, -1.0, 0.5, 1.0), vec4(-1.0, 3.0, 0.5, 1.0));
        vec2 uvs[3] = vec2[3](vec2(0.0, 0.0), vec2(2.0, 0.0), vec2(0.0, 2.0));
        gl_Position = vertices[gl_VertexID];
        uv = uvs[gl_VertexID];
    }
)";

char const* fragmentShader = R"(
    #version 460 core

    layout(binding=0) uniform sampler2D textureSampler;

    in vec2 uv;

    out vec4 color;

    void main() {
        color = texture(textureSampler, uv);
    }
)";

}


namespace ProperRT::OpenGL
{
    void Texture::BuildShaders() {
        // Create shader
        shader = OpenGL::Shader{ OpenGL::Shader::Sources{ .vertexSource{ vertexShader }, .fragmentSource{ fragmentShader } } };
        // Create an empty VAO for glDrawArrays call
        gl::glCreateVertexArrays(1, &emptyVAO);
    }

    Texture::Texture() noexcept = default;

    Texture::~Texture() {
        if (handle != 0) {
            gl::glDeleteTextures(1, &handle);
            handle = 0;
        }
    }

    Texture::Texture(std::string_view filename) {
        //TODO
    }

    template<class TData>
    Texture::Texture(ImageBuffer<Vector<4, TData>> const& buffer)
        : dimensions{ buffer.Dimensions() }
    {
        Create(gl::GLenum::GL_RGBA, gl::GLenum::GL_RGBA, GLType<TData>::TYPE, reinterpret_cast<const void*>(buffer.buffer.data()));
    }

    template
    Texture::Texture(ImageBuffer<Vector<4, float>> const&);

    template
    Texture::Texture(ImageBuffer<Vector<4, uint8>> const&);

    template
    Texture::Texture(ImageBuffer<Vector<4, uint16>> const&);

    Texture::Texture(Vector2i dimensions_, gl::GLenum internalFormat)
        : dimensions{ dimensions_ }
    {
        gl::glGenTextures(1, &handle);
        gl::glBindTexture(gl::GLenum::GL_TEXTURE_2D, handle);

        gl::glTexStorage2D(gl::GLenum::GL_TEXTURE_2D, 1, internalFormat, static_cast<gl::GLsizei>(dimensions.x()), static_cast<gl::GLsizei>(dimensions.y()));

        gl::glTexParameteri(gl::GLenum::GL_TEXTURE_2D, gl::GLenum::GL_TEXTURE_MIN_FILTER, gl::GLenum::GL_LINEAR);
        gl::glTexParameteri(gl::GLenum::GL_TEXTURE_2D, gl::GLenum::GL_TEXTURE_MAG_FILTER, gl::GLenum::GL_LINEAR);
        gl::glTexParameteri(gl::GLenum::GL_TEXTURE_2D, gl::GLenum::GL_TEXTURE_WRAP_S, gl::GLenum::GL_CLAMP_TO_EDGE);
        gl::glTexParameteri(gl::GLenum::GL_TEXTURE_2D, gl::GLenum::GL_TEXTURE_WRAP_T, gl::GLenum::GL_CLAMP_TO_EDGE);

        gl::glBindTexture(gl::GLenum::GL_TEXTURE_2D, 0);
    }

    Texture::Texture(Texture &&other) noexcept
        : handle(other.handle), dimensions(other.dimensions)
    {
        other.handle = 0;
        other.dimensions = { 0, 0 };
    }

    Texture& Texture::operator=(Texture&& other) noexcept {
        if (this != &other) {
            gl::glDeleteTextures(1, &handle);

            handle = other.handle;
            dimensions = other.dimensions;

            other.handle = 0;
        }

        return *this;
    }

    void Texture::Bind(gl::GLuint unit) const {
        gl::glBindTextureUnit(unit, handle);
    }

    void Texture::RenderToFramebuffer() const {
        gl::glBindVertexArray(emptyVAO);
        shader.Use();
        Bind(0);

        gl::glDrawArrays(gl::GLenum::GL_TRIANGLES, 0, 3);
    }

    template
    void Texture::Upload(ImageBuffer<Vector<4, float>> const&) const;

    template
    void Texture::Upload(ImageBuffer<Vector<4, uint8>> const&) const;

    template
    void Texture::Upload(ImageBuffer<Vector<4, uint16>> const&) const;

    template<class TData>
    void Texture::Upload(ImageBuffer<Vector<4, TData>> const& buffer) const {
        gl::glTextureSubImage2D(
            handle, 0,
            0, 0, static_cast<gl::GLsizei>(dimensions.x()), static_cast<gl::GLsizei>(dimensions.y()),
            gl::GLenum::GL_RGBA, GLType<TData>::TYPE, buffer.buffer.data()
        );
    }

    template
    ImageBuffer<Vector<4, float>> Texture::Download() const;

    template
    ImageBuffer<Vector<4, uint8>> Texture::Download() const;
    
    template
    ImageBuffer<Vector<4, uint16>> Texture::Download() const;

    template<class TData>
    ImageBuffer<Vector<4, TData>> Texture::Download() const {
        ImageBuffer<Vector<4, TData>> cpuBuffer{ dimensions };
        Download(cpuBuffer);
        return cpuBuffer;
    }

    template
    void Texture::Download(ImageBuffer<Vector<4, float>>&) const;

    template
    void Texture::Download(ImageBuffer<Vector<4, uint8>>&) const;
    
    template
    void Texture::Download(ImageBuffer<Vector<4, uint16>>&) const;

    template<class TData>
    void Texture::Download(ImageBuffer<Vector<4, TData>>& buffer) const {
        if (buffer.Dimensions() != dimensions) {
            //TODO throw
        }

        gl::glGetTextureImage(handle, 0, gl::GLenum::GL_RGBA, GLType<TData>::TYPE, buffer.buffer.size() * sizeof(Vector<4, TData>), buffer.buffer.data());
    }

    void Texture::Create(gl::GLenum internalFormat, gl::GLenum inputFormat, gl::GLenum inputType, void const* data) {
        gl::glGenTextures(1, &handle);
        gl::glBindTexture(gl::GLenum::GL_TEXTURE_2D, handle);

        gl::glTexParameteri(gl::GLenum::GL_TEXTURE_2D, gl::GLenum::GL_TEXTURE_MIN_FILTER, gl::GLenum::GL_LINEAR);
        gl::glTexParameteri(gl::GLenum::GL_TEXTURE_2D, gl::GLenum::GL_TEXTURE_MAG_FILTER, gl::GLenum::GL_LINEAR);
        gl::glTexParameteri(gl::GLenum::GL_TEXTURE_2D, gl::GLenum::GL_TEXTURE_WRAP_S, gl::GLenum::GL_CLAMP_TO_EDGE);
        gl::glTexParameteri(gl::GLenum::GL_TEXTURE_2D, gl::GLenum::GL_TEXTURE_WRAP_T, gl::GLenum::GL_CLAMP_TO_EDGE);

        gl::glTexImage2D(gl::GLenum::GL_TEXTURE_2D, 0, internalFormat,
            static_cast<gl::GLsizei>(dimensions.x()), static_cast<gl::GLsizei>(dimensions.y()),
            0, inputFormat, inputType, data);
        
        gl::glBindTexture(gl::GLenum::GL_TEXTURE_2D, 0);
    }
}
