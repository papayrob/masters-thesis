/**
 * File name: Shader.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "Shader.hpp"

#include <stdexcept>
#include <fstream>
#include <sstream>
#include "glbinding/gl/gl.h"

namespace ProperRT::OpenGL
{
    namespace
    {
        template<gl::GLenum E>
        gl::GLuint BuildShader(std::string_view const& source, gl::GLuint program) {
            gl::GLuint shader = gl::glCreateShader(E);
            auto const str = source.data();
            gl::glShaderSource(shader, 1, &str, nullptr);
            gl::glCompileShader(shader);

            // Check for shader compile errors
            gl::GLboolean success = 0;
            gl::glGetShaderiv(shader, gl::GLenum::GL_COMPILE_STATUS, &success);
            if (success == gl::GL_FALSE) {
                gl::GLint logLength = 0;
                gl::glGetShaderiv(shader, gl::GLenum::GL_INFO_LOG_LENGTH, &logLength);

                std::vector<gl::GLchar> infoLog(logLength);
                gl::glGetShaderInfoLog(shader, logLength, nullptr, infoLog.data());

                // Prevent leak
                gl::glDeleteShader(shader);

                throw std::runtime_error("Shader compilation failed: " + std::string(infoLog.data()));
            }

            gl::glAttachShader(program, shader);
            return shader;
        }

        template<gl::GLenum E>
        gl::GLuint BuildShader(std::optional<std::string_view> const& source, gl::GLuint program) {
            gl::GLuint shader{ 0 };
            if (source.has_value()) {
                shader = BuildShader<E>(source.value(), program);
            }
            return shader;
        }
    }

    std::string Shader::LoadShaderSource(AssetID const &assetID) {
        std::ifstream file{assetID.GetSystemPath()};
        if (file) {
            std::stringstream text;
            text << file.rdbuf();
            return text.str();
        }
        else {
            //TODO exception
            assert(false);
            return {};
        }
    }

    Shader::Shader(Sources const &sources)
    {
        program = gl::glCreateProgram();

        auto vertex = BuildShader<gl::GLenum::GL_VERTEX_SHADER>(sources.vertexSource, program);
        auto geometry = BuildShader<gl::GLenum::GL_GEOMETRY_SHADER>(sources.geometrySource, program);
        auto tessControl = BuildShader<gl::GLenum::GL_TESS_CONTROL_SHADER>(sources.tessellationControlSource, program);
        auto tessEval = BuildShader<gl::GLenum::GL_TESS_EVALUATION_SHADER>(sources.tessellationEvalSource, program);
        auto fragment = BuildShader<gl::GLenum::GL_FRAGMENT_SHADER>(sources.fragmentSource, program);

        gl::glLinkProgram(program);

        gl::GLboolean linkSuccess = 0;
        glGetProgramiv(program, gl::GLenum::GL_LINK_STATUS, &linkSuccess);
        if (linkSuccess == gl::GL_FALSE) {
            gl::GLint logLength = 0;
            glGetProgramiv(program, gl::GLenum::GL_INFO_LOG_LENGTH, &logLength);

            std::vector<gl::GLchar> infoLog(logLength);
            gl::glGetProgramInfoLog(program, logLength, nullptr, infoLog.data());
            
            // Prevent leaks
            gl::glDeleteProgram(program);
            program = 0;
            gl::glDeleteShader(vertex);
            gl::glDeleteShader(geometry);
            gl::glDeleteShader(tessControl);
            gl::glDeleteShader(tessEval);
            gl::glDeleteShader(fragment);

            throw std::runtime_error("Shader compilation failed: " + std::string(infoLog.data()));
        }

        // Delete the shaders as they're linked into our program now and no longer necessary
        //TODO detach shaders
        gl::glDeleteShader(vertex);
        gl::glDeleteShader(geometry);
        gl::glDeleteShader(tessControl);
        gl::glDeleteShader(tessEval);
        gl::glDeleteShader(fragment);
    }

    Shader::~Shader() {
        if (program > 0) {
            gl::glDeleteProgram(program);
        }
    }
    
    Shader::Shader(Shader&& other) noexcept
        : program{ other.program }
    {
		other.program = 0;
    }

    Shader& Shader::operator=(Shader&& other) noexcept {
        if (this != &other) {
            if (program != 0) {
				gl::glDeleteProgram(program);
			}

			program = other.program;
			other.program = 0;
		}
        
        return *this;
    }

    void Shader::Use() const noexcept {
        gl::glUseProgram(program);
    }
    
    gl::GLuint Shader::GetUniformLocation(std::string const &name) const {
        return gl::glGetUniformLocation(program, name.c_str());
    }
}
