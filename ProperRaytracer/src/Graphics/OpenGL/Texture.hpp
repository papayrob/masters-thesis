/**
 * File name: Texture.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "glbinding/gl/types.h"
#include "glbinding/gl/enum.h"

#include "Graphics/ImageBuffer.hpp"
#include "Graphics/Color.hpp"
#include "Shader.hpp"

namespace ProperRT::OpenGL
{
    /// @brief Class for storing and manipulating OpenGL textures.
    struct Texture
    {
    public:
        static
        void BuildShaders();

    public:
        /// @brief Default ctor, no texture object is created.
        Texture() noexcept;

        ~Texture();

        /// @brief Unimplemented.
        explicit
        Texture(std::string_view filename);

        /// @brief Creates a texture from @p buffer.
        /// @details Template instantiated for common types.
        /// @tparam TData Color component data type
        /// @param buffer Image buffer with color data
        template<class TData>
        explicit
        Texture(ImageBuffer<Vector<4, TData>> const& buffer);

        /// @brief Creates a texture with given dimensions and format.
        /// @param dimensions Desired dimensions of the texture
        /// @param internalFormat Desired OpenGL format of the texture
        explicit
        Texture(Vector2i dimensions, gl::GLenum internalFormat);

        Texture(Texture const& other) = delete;
        Texture& operator=(Texture const& other) = delete;

        Texture(Texture&& other) noexcept;
        Texture& operator=(Texture&& other) noexcept;

        /// @brief Returns true if this instance contains a valid texture object, false otherwise.
        bool IsValid() const noexcept { return handle > 0; }

        /// @copydoc Texture::IsValid()
        operator bool() const noexcept { return IsValid(); }

        /// @brief Returns the OpenGL texture object handle.
        gl::GLuint Handle() const noexcept { return handle; }

        /// @brief Binds the texture to @p unit.
        /// @param unit Unit to bind to
        void Bind(gl::GLuint unit) const;

        /// @brief Returns the dimensions/size of the texture.
        Vector2i Dimensions() const noexcept { return dimensions; }

        /// @brief Renders the texture to the current framebuffer.
        void RenderToFramebuffer() const;

        /// @brief Uploads image data to the texture from @p buffer.
        /// @tparam TData Data type
        /// @param buffer Color data
        template<class TData>
        void Upload(ImageBuffer<Vector<4, TData>> const& buffer) const;

        /// @brief Downloads and returns the texture data from the GPU.
        template<class TData>
        ImageBuffer<Vector<4, TData>> Download() const;

        /// @brief Downloads and returns the texture data from the GPU to @p buffer.
        /// @param buffer Buffer to download to
        template<class TData>
        void Download(ImageBuffer<Vector<4, TData>>& buffer) const;

    private:
        static inline
        gl::GLuint emptyVAO{};

        static inline
        OpenGL::Shader shader{};

    private:
        gl::GLuint handle{};
        Vector2i dimensions{};

        void Create(gl::GLenum internalFormat, gl::GLenum inputFormat, gl::GLenum inputType, void const* data);
    };
}
