﻿/**
 * File name: Rasterizer.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2023 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "Rasterizer.hpp"

#include <array>
#include "glbinding/gl/gl.h"
#include "glbinding-aux/logging.h"

#include "SceneDefinition/Components/Camera.hpp"
#include "OGLUtilities.hpp"
#include "SceneDefinition/Components/Light.hpp"

namespace {

void callback(gl::GLenum, gl::GLenum, gl::GLuint, gl::GLenum severity, gl::GLsizei, const gl::GLchar* message, const void*) {
    if (severity == gl::GL_DEBUG_SEVERITY_NOTIFICATION) {
        return;
    }
    std::cout << message << '\n';
}

}

namespace ProperRT
{
    Rasterizer::Rasterizer() {
#ifdef PRT_DEBUG
        gl::glEnable(gl::GLenum::GL_DEBUG_OUTPUT);
        gl::glEnable(gl::GLenum::GL_DEBUG_OUTPUT_SYNCHRONOUS);
        gl::glDebugMessageCallback(callback, nullptr);
#endif

    }

    Rasterizer::~Rasterizer() {
        gl::glDeleteBuffers(1, &lightBuffer);
    }

    void Rasterizer::RegisterRenderer(CompPtr<Components::Renderer> renderer) {
        meshMap[std::weak_ptr{ renderer->GetMesh() }].push_back(std::move(renderer));
    }

    void Rasterizer::Render() {
        UploadLights();

        // Prepare matrices
        auto matrices = Components::Camera::MainCamera()->GetCameraMatrices();
        
        auto meshIt = meshMap.begin();
        auto meshMapEnd = meshMap.end();
        while (meshIt != meshMapEnd) {
            // Remove unloaded mesh
            if (meshIt->first.expired()) {
                meshIt = meshMap.erase(meshIt);
            }
            // Draw all renderers
            else {
                auto const& [meshWeak, renderers] = *meshIt;
                auto mesh = meshWeak.lock();
                mesh->GLBind();

                for (auto const& renderer : renderers) {
                    if (renderer->doRender && renderer->GetGameObject()->GetScene() == Scene::GetActiveScene()) {
                        renderer->Rasterize(matrices, false);
                    }
                }

                ++meshIt;
            }
        }
    }

    void Rasterizer::UploadLights() {
        std::vector<Components::Light::GPUData> lightsCPU;
        for (auto const& light : Components::Light::lightRegistry.GetItems()) {
            lightsCPU.push_back(light->GetGPUData());
        }

        if (lightBufferSize != StdSize<int>(lightsCPU)) {
            lightBufferSize = StdSize<int>(lightsCPU);
            lightBuffer = CreateOGLBuffer<Components::Light::GPUData>(lightsCPU);
            gl::glBindBufferBase(gl::GL_SHADER_STORAGE_BUFFER, 0, lightBuffer);
        }
        else {
            gl::glNamedBufferSubData(lightBuffer, 0, std::span(lightsCPU).size_bytes(), lightsCPU.data());
        }
    }
}
