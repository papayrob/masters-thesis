/**
 * File name: RTApp.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2022 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "RTApp.hpp"

#include <functional>
#include <fstream>
#include <filesystem>
#include <format>

#include "Graphics/glfwInclude.hpp"
#include "stb_image_write.h"
#include "glbinding/gl/gl.h"
#include "glbinding/glbinding.h"
#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

#include "ProperRT.hpp"
#include "Utilities/Serializers.hpp"
#include "InputSystem/Input.hpp"
#include "SceneDefinition/Components/Camera.hpp"
#include "Utilities/Logger.hpp"
#include "Graphics/Raytracing/StructureRaytracer.hpp"
#include "Graphics/Raytracing/KDTGPURaytracer.hpp"
#include "SceneDefinition/Scene.hpp"
#include "Utilities/AdvancedUtilities.hpp"

namespace ProperRT
{
	struct RTApp::GLFWData
	{
		vkfw::UniqueInstance glfwInstance{};
        vkfw::UniqueWindow window{};
	};

	RTApp::RTApp() {
		// Init window
		if (showGUI) {
			glfw = std::make_unique<GLFWData>();
			glfw->glfwInstance = vkfw::initUnique();

			vkfw::WindowHints hints;
			hints.clientAPI = vkfw::ClientAPI::eOpenGL;

			using Camera = Components::Camera;
			glfw->window = vkfw::createWindowUnique(500, 500, APP_NAME, hints);
			glfw->window->makeContextCurrent();

			glbinding::initialize(nullptr);

			// Callbacks
			glfw->window->callbacks()->on_framebuffer_resize = Camera::WindowSizeChangeCallback;

			glfw->window->callbacks()->on_key = Input::KeyCallback;
			glfw->window->callbacks()->on_mouse_button = Input::MouseButtonCallback;
			glfw->window->callbacks()->on_cursor_move = Input::MousePositionCallback;

			// OpenGL settings
			if (renderingMode == RenderingMode::OpenGL) {
				gl::glEnable(gl::GLenum::GL_DEPTH_TEST);
			}
			else {
				gl::glDisable(gl::GLenum::GL_DEPTH_TEST);
			}

			gl::glDisable(gl::GLenum::GL_CULL_FACE);
			OpenGL::Texture::BuildShaders();

			// Setup ImGui
			IMGUI_CHECKVERSION();
			ImGui::CreateContext();
			ImGuiIO& io = ImGui::GetIO();
			io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
			io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;
			ImGui::GetStyle().ScaleAllSizes(appConfig.GetValue<float>("uiScale", 1.0f));

			// Setup Platform/Renderer backends
			ImGui_ImplGlfw_InitForOpenGL(static_cast<GLFWwindow*>(glfw->window.get()), true);
			ImGui_ImplOpenGL3_Init(nullptr);
		}

		if (stopFrame < 0) {
			stopFrame = (showGUI ? std::numeric_limits<int64>::max() - 10 : 1);
		}

		// Renderers
		if (auto raytracerStr = appConfig.GetValue<std::string>("raytracer", "StructureRaytracer"); raytracerStr == "StructureRaytracer") {
			raytracer = std::make_unique<StructureRaytracer>(appConfig.GetValue<int>("raytracingThreads", 16));
		}
		else if (raytracerStr == "KDTGPURaytracer") {
			raytracer = std::make_unique<KDTGPURaytracer>();
		}

		if (renderingMode == RenderingMode::OpenGL) {
			rasterizer = std::make_unique<Rasterizer>();
			frameTracer = std::make_unique<FrameTracer>(raytracer.get(), runFrameTracerInSeparateThread);
			frameTracer->stage = FrameTracerStage::Constructed;

			raytracer->SetRenderOrigin(IRaytracer::RenderOrigin::TopLeft);
		}
		else {
			raytracer->SetRenderOrigin(IRaytracer::RenderOrigin::BottomLeft);
		}
	}

	RTApp::~RTApp() {
		if (showGUI) {
			ImGui_ImplOpenGL3_Shutdown();
			ImGui_ImplGlfw_Shutdown();
			ImGui::DestroyContext();
		}
    }

    void RTApp::EnterMainLoop() {
		inMainLoop = true;

		Scene::LoadScene(AssetID{ Scene::startingSceneName }, true, false);
		raytracer->Init();

		if (renderingMode == RenderingMode::OpenGL) {
			OpenGLMainLoop();
		}
		else {
			RaytracingMainLoop();
		}

		OnExit();
		inMainLoop = false;
	}

    vkfw::UniqueWindow const& RTApp::GetMainWindow() {
		return glfw->window;
    }

    void RTApp::OpenGLMainLoop() {
		lastTimePoint = std::chrono::steady_clock::now();
		while (!ShouldExit()) {
			bool snapshotted = (FrameTracerStage::Ready <= frameTracer->stage);
			// Init updater
			if (!snapshotted) {
				Scene::Updater::SwitchScenes();
			}
			Scene::Updater updater(Scene::GetActiveScene());
			updater.snapshotted = snapshotted;

			// Pre render
			updater.PreRenderUpdate();


			// Render
			glfw->window->makeContextCurrent();

			ImGui_ImplOpenGL3_NewFrame();
			ImGui_ImplGlfw_NewFrame();
			ImGui::NewFrame();

			updater.UIUpdate();

			RaytraceFrame();

			auto const& bgclr = Components::Camera::MainCamera()->backgroundColor;
			gl::glClearColor(bgclr.x(), bgclr.y(), bgclr.z(), bgclr.w());
			gl::glClear(gl::ClearBufferMask::GL_COLOR_BUFFER_BIT | gl::ClearBufferMask::GL_DEPTH_BUFFER_BIT);

			rasterizer->Render();
			++frame;


			// Post render
			updater.PostRenderUpdate();


			// Render UI
			ImGui::Render();
			ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
			glfw->window->swapBuffers();

			Input::ResetKeyStates();
			vkfw::pollEvents();

			auto now = std::chrono::steady_clock::now();
			timeDelta = std::chrono::duration<cfloat>(now - lastTimePoint).count();
			lastTimePoint = now;
		}
    }

    void RTApp::RaytracingMainLoop() {
		lastTimePoint = std::chrono::steady_clock::now();
		while (!ShouldExit()) {
			// Init updater
			Scene::Updater::SwitchScenes();
			Scene::Updater updater(Scene::GetActiveScene());

			// Pre render
			updater.PreRenderUpdate();


			// Render
			if (showGUI) {
				glfw->window->makeContextCurrent();
			}

			raytracer->BuildDynamic();

			if (showGUI) {
				ImGui_ImplOpenGL3_NewFrame();
				ImGui_ImplGlfw_NewFrame();
				ImGui::NewFrame();
			}

			updater.UIUpdate();

			raytracer->RaytraceScene();
			if (showGUI) {
				raytracer->GetRenderTexture().RenderToFramebuffer();
			}
			else {
				raytracer->WaitForFinish();
			}


			// Post render
			updater.PostRenderUpdate();

			if (saveFrame > 0 && (frame % saveFrame) == 0) {
				ImageWriter::Instance()->PushWrite(raytracer->GetRender(), std::format("{0}_frame_{1:03}.png", Scene::GetActiveScene()->Name(), frame), true);
			}


			if (showGUI) {
				// Render UI
				ImGui::Render();
				ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
				glfw->window->swapBuffers();

				Input::ResetKeyStates();
				vkfw::pollEvents();
			}

			++frame;
			auto now = std::chrono::steady_clock::now();
			timeDelta = std::chrono::duration<cfloat>(now - lastTimePoint).count();
			lastTimePoint = now;
		}
    }

    void RTApp::RaytraceFrame() {
		ImGui::Begin("Raytracing controls");

		if (!frameTracer->enabled) {
			ImGui::Text("Frame tracer is disabled");
			ImGui::End();
			return;
		}

		if (frameTracer->running.load()) {
			// Frame tracer running, cannot do anything
			ImGui::BeginDisabled();

			ImGui::Button("Resume scene");
			ImGui::Separator();
			ImGui::Button("Build static");
			ImGui::Button("Build dynamic");
			ImGui::Button("Raytrace");

			ImGui::EndDisabled();
		}
		else {
			// Frame tracer not running
			// Wait for user to close rendered image, otherwise display menu
			if (frameTracer->renderOpen) {
				ImGui::BeginDisabled();

				ImGui::Button("Resume scene");
				ImGui::Separator();
				ImGui::Button("Build static");
				ImGui::Button("Build dynamic");
				ImGui::Button("Raytrace");

				ImGui::EndDisabled();
				
				// Show rendered image in a window
				ImGui::Begin("Raytraced frame", &(frameTracer->renderOpen));
				if (ImGui::Button("Save render")) {
					ImageWriter::Instance()->PushWrite(frameTracer->raytracer->GetRender(), "render.png", false);
				}
				auto region = ImGui::GetContentRegionAvail();
				auto const& texture = frameTracer->raytracer->GetRenderTexture();
				ImGui::Image((void*)(intptr_t)texture.Handle(), FitInsideKeepRatio(texture.Dimensions(), region));
				ImGui::End();

			}
			else {
				switch (frameTracer->stage)
				{
				case FrameTracerStage::Default:
					// Wait for construction
					break;

				case FrameTracerStage::Constructed: {
					// Wait for user to snapshot the frame
					if (ImGui::Button("Snapshot frame")) {
						frameTracer->stage = FrameTracerStage::Ready;
					}

					break;
				}
				default: {
					// Frametracer ready to run, wait for user input
					FrameTracerStage stopStage = FrameTracerStage::Default;

					// Reset
					if (ImGui::Button("Resume scene")) {
						frameTracer = std::make_unique<FrameTracer>(raytracer.get(), runFrameTracerInSeparateThread);
						frameTracer->stage = FrameTracerStage::Constructed;

						ImGui::Separator();

						ImGui::BeginDisabled();

						ImGui::Button("Build static");
						ImGui::Button("Build dynamic");
						ImGui::Button("Raytrace");
						
						ImGui::EndDisabled();
					}
					else {
						ImGui::Separator();

						if (ImGui::Button("Build static")) {
							stopStage = FrameTracerStage::StaticBuilt;
						}
						
						if (ImGui::Button("Build dynamic")) {
							stopStage = FrameTracerStage::DynamicBuilt;
						}

						if (ImGui::Button("Raytrace")) {
							stopStage = FrameTracerStage::Traced;
						}

						frameTracer->Run(stopStage);

					}
				}
				}
			}
		}

		ImGui::End();
    }

    bool RTApp::ShouldExit() {
		if (showGUI) {
        	return (glfw->window->shouldClose() || frame >= stopFrame);
		}
		else {
			return (frame >= stopFrame);
		}
    }

    void RTApp::OnExit() {
		ImageWriter::StopWorkerThread();

		// Save stats
		std::string statsFileName{};
		if (!appConfig.TryGetValue("statsFileName", statsFileName)) {
			auto now = std::chrono::system_clock::now();
			auto date = std::chrono::year_month_day{std::chrono::floor<std::chrono::days>(now)};
			auto time = std::chrono::hh_mm_ss{std::chrono::floor<std::chrono::seconds>(now - std::chrono::floor<std::chrono::days>(now))};
			statsFileName = std::format(
				"{:%y}-{:%m}-{:%d}_{:%H}-{:%M}-{:%S}.stats",
				date.year(), date.month(), date.day(),
				time.hours(), time.minutes(), time.seconds()
			);
		}
		statistics.SaveToDisk(statsFileName);
    }

    void RTApp::SetFrameTracerEnabled(bool newValue) {
		if (Instance()->frameTracer != nullptr) {
			Instance()->frameTracer->enabled = newValue;
		}
    }

    void RTApp::InitializeStatic(RenderingMode renderingMode_, bool showGUI_, int stopFrame_, int saveFrame_, AssetID&& appConfigID) {
		if (instance == nullptr) {
			renderingMode = renderingMode_;
			showGUI = showGUI_;
			stopFrame = stopFrame_;
			saveFrame = saveFrame_;

			// Load config file
			if (appConfigID.IsValid()) {
				std::ifstream fileStream{ appConfigID.GetSystemPath() };
				if (fileStream.is_open()) {
					appConfig = SceneConfig{ nlohmann::json::parse(fileStream, nullptr, true, true) };
				}
				else {
					Logger::LogWarning("Could not open " + appConfigID.GetAssetPath().string());
					appConfig = SceneConfig{ nlohmann::json{} };
				}
			}
			else {
				Logger::LogWarning("Non-existent app config path " + appConfigID.GetAssetPath().string());
				appConfig = SceneConfig{ nlohmann::json{} };
			}
			//TODO try catch
		}
    }

    RTApp::FrameTracer::FrameTracer(IRaytracer *raytracer_, bool runFrameTracerInSeparateThread_)
		: raytracer{ raytracer_ }, runFrameTracerInSeparateThread{ runFrameTracerInSeparateThread_ }
	{
    }

    void RTApp::FrameTracer::Run(Stage stopStage) {
		using StageIntType = std::underlying_type_t<Stage>;
		if (running.load() || stopStage < Stage::StaticBuilt) {
			return;
		}
		
		running = true;

		if (stopStage == Stage::Traced) {
			renderOpen = true;
		}

		auto frameTracerLambda = [this, stopStage]() {
			// Rebuild static or build it if not already built
			if (
				stopStage == Stage::StaticBuilt ||
				stage.load() < Stage::StaticBuilt
			) {
				raytracer->BuildStatic();
				stage = Stage::StaticBuilt;
			}

			// Rebuild dynamic or build if not already built
			if (
				stopStage == Stage::DynamicBuilt ||
				(Stage::DynamicBuilt < stopStage && stage.load() < Stage::DynamicBuilt)
			) {
				raytracer->BuildDynamic();
				stage = Stage::DynamicBuilt;
			}

			if (stopStage == Stage::Traced) {
				raytracer->RaytraceScene();
				stage = Stage::Traced;
			}

			running = false;
		};

		if (runFrameTracerInSeparateThread) {
			raytracer->UpdateOpenGLResources();
			thread = std::jthread(frameTracerLambda);
		}
		else {
			frameTracerLambda();
		}
    }
}
