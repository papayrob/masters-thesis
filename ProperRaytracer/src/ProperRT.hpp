/**
 * File name: ProperRT.hpp
 * Description: Top-most namespace header file.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2022 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */

#pragma once

#include <cstdint>
#include <iostream>
#include <string>
#include <filesystem>
#include <cuda_runtime_api.h>
#include <cuda/std/bit>
#include <bit>

#include "Macros.hpp"

#define CUDA_FUNCTION __host__ __device__
#define CuSTD cuda::std

namespace ProperRT
{
	/// @brief Storage floating point type
	using sfloat = float;
	/// @brief Compute floating point type
	using cfloat = float;

	using int8 = int8_t;
	using int16 = int16_t;
	using int32 = int32_t;
	using int64 = int64_t;

	using uint8 = uint8_t;
	using uint16 = uint16_t;
	using uint32 = uint32_t;
	using uint64 = uint64_t;

	CUDA_FUNCTION inline constexpr
	sfloat operator ""_sf(long double x) { return sfloat(x); }

	CUDA_FUNCTION inline constexpr
	cfloat operator ""_cf(long double x) { return cfloat(x); }

	inline constexpr
	double ZERO_COMP_EPSILON{ 0.000000001 };

	using namespace std::string_literals;

	/// @brief Enum for differentiating axes.
	enum class Axis3D : int8_t
	{
		X = 0, Y = 1, Z = 2, None
	};

	/// @brief Enum for differentiating sides.
	enum class Side : int8_t
	{
		Left = 0, Right = 1, None
	};

	/// @brief Enum for allowing safe or unsafe method calls.
	enum class Safety
	{
		Safe, Unsafe
	};

	/// @brief Enum for differentiating threaded execution.
	enum class Execution
	{
		Sequential, Parallel
	};

	enum class GPUMemoryLocation
	{
		Local, Shared, Global, Unified
	};

	struct NoData {};
}
