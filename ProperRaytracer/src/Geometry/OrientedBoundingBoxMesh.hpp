/**
 * File name: OrientedBoundingBoxMesh.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <vector>
#include "glbinding/gl/types.h"

#include "Mesh.hpp"
#include "OrientedBoundingBox.hpp"
#include "Graphics/Color.hpp"

namespace ProperRT
{
    /// @brief Class for representing oriented bounding box meshes.
    class OrientedBoundingBoxMesh : public Mesh
    {
    public:
        OrientedBoundingBoxMesh(std::vector<OrientedBoundingBox> boxes_);

        /// @brief Default ctor.
        virtual
        ~OrientedBoundingBoxMesh();

        OrientedBoundingBoxMesh(OrientedBoundingBoxMesh const&) = delete;
        OrientedBoundingBoxMesh& operator=(OrientedBoundingBoxMesh const&) = delete;

        OrientedBoundingBoxMesh(OrientedBoundingBoxMesh&&);
        OrientedBoundingBoxMesh& operator=(OrientedBoundingBoxMesh&&);

        /// @brief Mesh boxes
        std::vector<OrientedBoundingBox> const& Boxes() const { return boxes; }

        /// @brief Mesh boxes
        std::span<OrientedBoundingBox> Boxes() { return boxes; }

        /// @brief Sets the material of this mesh
        /// @param newValue New material value
        void SetMaterial(Material const& newValue) {
            if (materials.empty()) {
                materials.push_back(newValue);
            }
            else {
                materials[0] = newValue;
            }
        }

        virtual
        Primitive GetPrimitive(int32_t submeshIdx, int32_t primitiveIdx) const override;

        virtual
        Primitive GetPrimitive(int32_t submeshIdx, int32_t primitiveIdx, Matrix4x4cf const& transform) const override;

        virtual
        int32_t GetPrimitiveCount(int32_t submeshIdx) const noexcept override { return StdSize<int32>(boxes); }

        virtual
        std::vector<int32_t> GetPrimitiveCounts() const noexcept override { return std::vector<int32_t>{ StdSize<int32>(boxes) }; }

        virtual
        void FillSubmeshInstance(GPUMeshManagement::Instance& instance, int32_t submeshIdx) override;

        virtual
        void SyncToGPU() override;

        virtual
        void ReleaseCPUResources() override;

        virtual
        void ReleaseGPUResources() override;

        virtual
        void GLBind() const override;

        virtual
        void Rasterize(Matrix4x4f const& modelMat, RasterizationMatrices const& matrices, bool bind = true) override;

        virtual
        void Rasterize(Matrix4x4f const& modelMat, RasterizationMatrices const& matrices, std::span<int32_t const> submeshIndeces, bool bind = true) override;
    
    private:
        // Pimpl
        struct ComputeStorage;

    private:
        std::vector<OrientedBoundingBox> boxes;

        gl::GLuint VAO{};
        gl::GLuint vertexBuffer{};

        std::unique_ptr<ComputeStorage> computeStorage;
    };
}
