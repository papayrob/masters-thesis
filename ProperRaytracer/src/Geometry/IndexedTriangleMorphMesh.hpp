/**
 * File name: IndexedTriangleMorphMesh.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "MorphMesh.hpp"
#include "IndexedTriangleMesh.hpp"

namespace ProperRT
{
    /// @brief Class for representing indexed triangle morph target meshes.
    class IndexedTriangleMorphMesh : public MorphMesh
    {
    public:
        /// @brief Creates a morph target mesh from @p meshes.
        /// @details All data is copied, so changes to input meshes will not
        /// be visible in this mesh. All input meshes must have the same amount of attributes.
        /// @param meshes A list of key frame meshes between which he mesh will be morphed
        IndexedTriangleMorphMesh(std::vector<std::shared_ptr<IndexedTriangleMesh>> meshes);

        virtual
        ~IndexedTriangleMorphMesh() noexcept;

        IndexedTriangleMorphMesh(IndexedTriangleMorphMesh const&) = delete;
        IndexedTriangleMorphMesh& operator=(IndexedTriangleMorphMesh const&) = delete;

        IndexedTriangleMorphMesh(IndexedTriangleMorphMesh&&);
        IndexedTriangleMorphMesh& operator=(IndexedTriangleMorphMesh&&);

        /// @brief Mesh vertices.
        std::vector<Vector3sf> const& Vertices() const { return vertices; }
        /// @brief Mesh indeces.
        std::vector<IndexedTriangle> const& Indeces() const { return indeces; }

        virtual
        int32_t MeshCount() const override { return meshN; }

        virtual
        Primitive GetPrimitive(int32_t submeshIdx, int32_t primitiveIdx, MeshBlendInfo blendInfo) const override;

        virtual
        Primitive GetPrimitive(int32_t submeshIdx, int32_t primitiveIdx, MeshBlendInfo blendInfo, Matrix4x4cf const& transform) const override;

        virtual
        int32_t GetPrimitiveCount(int32_t submeshIdx) const noexcept override { return StdSize<int32_t>(submeshes.at(submeshIdx)); }

        virtual
        std::vector<int32_t> GetPrimitiveCounts() const noexcept override;

        virtual
        void SyncToGPU() override;

        virtual
        void ReleaseCPUResources() override;

        virtual
        void ReleaseGPUResources() override;

        virtual
        void GLBind() const override;

        virtual
        void FillSubmeshInstance(GPUMeshManagement::Instance& instance, MeshBlendInfo blendInfo, int32_t submeshIdx) override;

        virtual
        void Rasterize(Matrix4x4f const& modelMat, RasterizationMatrices const& matrices, MeshBlendInfo blendInfo, bool bind = true) override;

        virtual
        void Rasterize(Matrix4x4f const& modelMat, RasterizationMatrices const& matrices, std::span<int32_t const> submeshIndeces, MeshBlendInfo blendInfo, bool bind = true) override;

    private:
        // Pimpl
        struct ComputeStorage;

    private:
        std::vector<Vector3sf> vertices;

        std::vector<IndexedTriangle> indeces;
        std::vector<std::vector_span<IndexedTriangle const>> submeshes;

        int32_t meshN;
        int32_t singleMeshVertexN;

        gl::GLuint VAO{};

        gl::GLuint vertexBuffer{};

        gl::GLuint indexBuffer{};

        std::unique_ptr<ComputeStorage> computeStorage;
    };
}
