/**
 * File name: GenericMesh.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <vector>
#include <span>

#include "Utilities/StrippedType.hpp"
#include "SceneDefinition/Asset.hpp"
#include "Graphics/Material.hpp"

namespace ProperRT
{
    namespace GPUMeshManagement
    {
        struct Instance;
    }

    /// @brief Base class for meshes.
    class GenericMesh : public Asset
    {
    public:
        /// @brief Returns the primitive count for a specific submesh.
        /// @param submeshIdx Index of the queried submesh
        virtual
        int32 GetPrimitiveCount(int32 submeshIdx) const noexcept = 0;

        /// @brief Returns the primitive counts for all submeshes.
        virtual
        std::vector<int32> GetPrimitiveCounts() const noexcept = 0;

        /// @brief Uploads mesh data to the GPU.
        virtual
        void SyncToGPU() = 0;

        /// @brief Releases resources allocated on the CPU.
        /// @details Use this method when you upload the mesh data
        /// to the GPU and do not need the data on the CPU anymore.
        virtual
        void ReleaseCPUResources() = 0;

        /// @brief Releases resources allocated on the GPU.
        virtual
        void ReleaseGPUResources() = 0;
        
        /// @brief Binds OpenGL resources.
        /// @details This method can be used to save binding the same
        /// resources multiple times when rendering the same mesh
        /// multiple times in a row. Do not call when in Raytracing rendering mode.
        virtual
        void GLBind() const = 0;

        /// @brief Returns the span of materials of this mesh, one per submesh.
        virtual
        std::span<Material const> Materials() const { return materials; }

    protected:
        /// @brief List of materials, one per submesh.
        std::vector<Material> materials;
    };
}
