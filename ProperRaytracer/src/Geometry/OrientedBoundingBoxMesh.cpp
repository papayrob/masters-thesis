/**
 * File name: OrientedBoundingBoxMesh.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "OrientedBoundingBoxMesh.hpp"

#include "glbinding/gl/gl.h"

#include "RTApp.hpp"
#include "Graphics/OpenGL/OGLUtilities.hpp"
#include "SceneDefinition/Scene.hpp"
#include "SceneDefinition/Components/Camera.hpp"
#include "SceneDefinition/Components/Transform.hpp"
#include "Graphics/OpenGL/Shader.hpp"
#include "Graphics/Raytracing/GPUMeshManager.cuh"

namespace
{

ProperRT::OpenGL::Shader shader;

}

namespace ProperRT
{
    namespace
    {
        std::vector<Vector<8, OrientedBoundingBox::VertexType>> ToBufferData(std::span<OrientedBoundingBox const> boxes) {
            std::vector<Vector<8, OrientedBoundingBox::VertexType>> bufferData;
            for (auto const& box : boxes) {
                auto points = box.AllPoints();
                bufferData.push_back(
                    { points[0], points[4], points[1], points[5], points[2], points[6], points[3], points[7] }
                );
            }
            return bufferData;
        }
    }

    struct OrientedBoundingBoxMesh::ComputeStorage
    {
        DeviceUniquePointer<OrientedBoundingBox[]> boxes;
    };
    
    OrientedBoundingBoxMesh::OrientedBoundingBoxMesh(std::vector<OrientedBoundingBox> boxes_)
        : boxes{ std::move(boxes_) }
    {
        if (RTApp::GetRenderingMode() == RTApp::RenderingMode::OpenGL && shader.Program() == 0) {
            shader = OpenGL::Shader{OpenGL::Shader::Sources{
                .vertexSource{ OpenGL::Shader::LoadShaderSource(AssetID{"Shaders/OrientedBBox.vs"}) },
                .geometrySource{ OpenGL::Shader::LoadShaderSource(AssetID{"Shaders/OrientedBBox.gs"}) },
                .fragmentSource{ OpenGL::Shader::LoadShaderSource(AssetID{"Shaders/OrientedBBox.fs"}) }
            }};
        }
    }

    OrientedBoundingBoxMesh::~OrientedBoundingBoxMesh() {
        ReleaseGPUResources();
    }

    OrientedBoundingBoxMesh::OrientedBoundingBoxMesh(OrientedBoundingBoxMesh&&) = default;

    OrientedBoundingBoxMesh& OrientedBoundingBoxMesh::operator=(OrientedBoundingBoxMesh&&) = default;

    Primitive OrientedBoundingBoxMesh::GetPrimitive(int32_t submeshIdx, int32_t primitiveIdx) const {
        return boxes.at(primitiveIdx);
    }

    Primitive OrientedBoundingBoxMesh::GetPrimitive(int32_t submeshIdx, int32_t primitiveIdx, Matrix4x4cf const& transform) const {
        return boxes.at(primitiveIdx).Transform(transform);
    }

    void OrientedBoundingBoxMesh::FillSubmeshInstance(GPUMeshManagement::Instance &instance, int32_t submeshIdx) {
        instance.meshData = GPUMeshManagement::MeshData<OrientedBoundingBox>{
            computeStorage->boxes.Get()
        };
    }

    void OrientedBoundingBoxMesh::SyncToGPU() {
        if (VAO == 0) {
            if (RTApp::GetRenderingMode() == RTApp::RenderingMode::OpenGL) {
                auto bufferData = ToBufferData(boxes);
                
                // Create VAO
                gl::glCreateVertexArrays(1, &VAO);

                // Create buffers
                vertexBuffer = CreateOGLBuffer(std::span(bufferData));

                // Enable attributes
                gl::glVertexArrayVertexBuffer(VAO, 0, vertexBuffer, 0, 2 * sizeof(OrientedBoundingBox::VertexType));

                gl::glEnableVertexArrayAttrib(VAO, 0);
                gl::glEnableVertexArrayAttrib(VAO, 1);

                gl::glVertexArrayAttribFormat(VAO, 0, 3, gl::GLenum::GL_FLOAT, gl::GL_FALSE, 0);
                gl::glVertexArrayAttribFormat(VAO, 1, 3, gl::GLenum::GL_FLOAT, gl::GL_FALSE, sizeof(OrientedBoundingBox::VertexType));

                gl::glVertexArrayAttribBinding(VAO, 0, 0);
                gl::glVertexArrayAttribBinding(VAO, 1, 0);
            }

            computeStorage = std::make_unique<ComputeStorage>();
            computeStorage->boxes = DeviceUniquePointer<OrientedBoundingBox[]>::AllocateAndUpload(boxes);
        }
        else {
            if (RTApp::GetRenderingMode() == RTApp::RenderingMode::OpenGL) {
                auto bufferData = ToBufferData(boxes);
                gl::glNamedBufferSubData(vertexBuffer, 0, std::span(bufferData).size_bytes(), bufferData.data());
            }

            computeStorage->boxes.Upload(boxes);
        }
    }

    void OrientedBoundingBoxMesh::ReleaseCPUResources() {
        boxes.clear();
    }

    void OrientedBoundingBoxMesh::ReleaseGPUResources() {
        if (VAO != 0) {
            if (RTApp::GetRenderingMode() == RTApp::RenderingMode::OpenGL) {
                gl::glDeleteBuffers(1, &vertexBuffer);
                gl::glDeleteVertexArrays(1, &VAO);
            }
            computeStorage = nullptr;
            VAO = 0;
        }
    }

    void OrientedBoundingBoxMesh::GLBind() const {
        gl::glBindVertexArray(VAO);
        shader.Use();
    }

    void OrientedBoundingBoxMesh::Rasterize(Matrix4x4f const& modelMat, RasterizationMatrices const &matrices, bool bind) {
        if (VAO == 0) {
            SyncToGPU();
        }
        if (bind) {
            GLBind();
        }

        auto PVM = matrices.PV * modelMat;
        gl::glUniformMatrix4fv(0, 1, gl::GL_FALSE, &PVM[0][0]);
        gl::glUniform4fv(1, 1, materials.at(0).colorDiffuse.elements);
        //gl::glUniform4fv(2, 1, materials.at(0).colorSpecular.elements);

        gl::glDrawArrays(
            gl::GLenum::GL_LINES_ADJACENCY,
            0,
            4 * boxes.size()
        );
    }

    void OrientedBoundingBoxMesh::Rasterize(Matrix4x4f const& modelMat, RasterizationMatrices const &matrices, std::span<int32_t const> submeshIndeces, bool bind) {
        Rasterize(modelMat, matrices, bind);
    }
}
