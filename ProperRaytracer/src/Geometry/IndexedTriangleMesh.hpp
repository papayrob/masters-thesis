/**
 * File name: IndexedTriangleMesh.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <vector>
#include "glbinding/gl/types.h"

#include "Utilities/vector_span.hpp"
#include "Mesh.hpp"
#include "IndexedTriangle.hpp"
#include "Graphics/Color.hpp"

namespace ProperRT
{
    /// @brief Class for representing indexed triangle meshes.
    class IndexedTriangleMesh : public Mesh
    {
    public:
        using VertexType = Vector3sf;
    public:
        /// @brief Default ctor.
        IndexedTriangleMesh() noexcept;

        virtual
        ~IndexedTriangleMesh() noexcept;

        IndexedTriangleMesh(IndexedTriangleMesh const&) = delete;
        IndexedTriangleMesh& operator=(IndexedTriangleMesh const&) = delete;

        IndexedTriangleMesh(IndexedTriangleMesh&&);
        IndexedTriangleMesh& operator=(IndexedTriangleMesh&&);

        /// @brief Sets the vertex array.
        /// @details Cannot set vertices after allocating resources on the GPU.
        /// To change values use the @ref Vertices() method, or release GPU resources
        /// to make this method available again.
        void SetVertices(std::vector<VertexType> newValue) {
            if (VAO != 0) {
                throw std::runtime_error("Cannot modify mesh after creating GPU resources");
            }
            vertices = std::move(newValue);
        }

        /// @brief Mesh vertices.
        std::vector<VertexType> const& Vertices() const { return vertices; }

        /// @brief Mesh indeces.
        std::vector<IndexedTriangle> const& Indeces() const { return indeces; }

        /// @brief List of submeshes (each corresponds to a subspan of indeces and a material).
        auto const& Submeshes() const { return submeshes; }

        /// @brief Mesh vertices.
        std::span<VertexType> Vertices() { return vertices; }

        /// @brief Mesh indeces.
        std::span<IndexedTriangle> Indeces() { return indeces; }

        /// @brief Adds a submesh (list of indeces).
        /// @param subIndeces List of integers, 3 consecutive oens define a triangle.
        /// @return Index of the submesh
        std::size_t AddSubmesh(std::span<int32_t const> subIndeces, Material material);

        /// @brief Adds a submesh. The indeces will be appended to existing ones.
        /// @param subIndeces List of indexed triangles.
        /// @return Index of the added submesh
        std::size_t AddSubmesh(std::span<IndexedTriangle const> subIndeces, Material material);

        /// @brief Adds a submesh. @p subIndeces must be a subspan of this mesh's index array.
        /// @param subIndeces Span of existing indeces.
        /// @return Index of the added submesh
        std::size_t AddSubmesh(std::vector_span<IndexedTriangle const> subIndeces, Material material);

        virtual
        Primitive GetPrimitive(int32_t submeshIdx, int32_t primitiveIdx) const override;

        virtual
        Primitive GetPrimitive(int32_t submeshIdx, int32_t primitiveIdx, Matrix4x4cf const& transform) const override;

        virtual
        int32_t GetPrimitiveCount(int32_t submeshIdx) const noexcept override { return StdSize<int32_t>(submeshes.at(submeshIdx)); }

        virtual
        std::vector<int32_t> GetPrimitiveCounts() const noexcept override;

        virtual
        void FillSubmeshInstance(GPUMeshManagement::Instance& instance, int32_t submeshIdx) override;

        virtual
        void SyncToGPU() override;

        virtual
        void ReleaseCPUResources() override;

        virtual
        void ReleaseGPUResources() override;

        virtual
        void GLBind() const override;

        virtual
        void Rasterize(Matrix4x4f const& modelMat, RasterizationMatrices const& matrices, bool bind = true) override;

        virtual
        void Rasterize(Matrix4x4f const& modelMat, RasterizationMatrices const& matrices, std::span<int32_t const> submeshIndeces, bool bind = true) override;

    private:
        // Pimpl
        struct ComputeStorage;

    private:
        std::vector<VertexType> vertices;
        //std::vector<Vector2sf> uvs;

        std::vector<IndexedTriangle> indeces;
        std::vector<std::vector_span<IndexedTriangle const>> submeshes;

        gl::GLuint VAO{};
        
        gl::GLuint vertexBuffer{};
        gl::GLuint colorBuffer{};

        gl::GLuint indexBuffer{};

        std::unique_ptr<ComputeStorage> computeStorage;
    };
}
