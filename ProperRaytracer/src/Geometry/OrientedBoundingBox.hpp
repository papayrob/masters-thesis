/**
 * File name: OrientedBoundingBox.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <limits>

#include "AABB.hpp"

namespace ProperRT
{
    /// @brief Struct for representing box an oriented rectangular cuboid.
    struct OrientedBoundingBox
    {
        using VertexType = Vector3sf;

        using IntersectionCoordinates = AABB::IntersectionCoordinates;

        /// @brief Corner point
        VertexType corner{};
        /// @brief Vectors representing the direction and length of the cuboid's sides.
        /// @details Must be orthogonal.
        Vector<3, VertexType> sideVectors{};

        /// @brief Default ctor.
        OrientedBoundingBox() noexcept = default;

        /// @brief Standard ctor.
        CUDA_FUNCTION
        OrientedBoundingBox(VertexType corner_, Vector<3, VertexType> sideVectors_) noexcept
            : corner{ corner_ }, sideVectors{ sideVectors_ }
        {}

        /// @brief Linearly interpolates between @p source and @p target according to @p alpha.
        /// @param source Value to interpolate from
        /// @param target Value to interpolate to
        /// @param alpha Interpolant
        /// @return Interpolated value
        CUDA_FUNCTION static
        OrientedBoundingBox LERP(OrientedBoundingBox const& source, OrientedBoundingBox const& target, sfloat alpha) {
            OrientedBoundingBox box;
            // Lerp the corner point
            box.corner = ::ProperRT::LERP(source.corner, target.corner, alpha);

            //TODO
            // Lerp the rotation
            //auto sourceRotation = Quaternion{ Matrix3x3::Columns(normalize(source.sideVectors)) };
            //auto targetRotation = Quaternion{ Matrix3x3::Columns(normalize(source.sideVectors)) };
            //box.sideVectors = Quaternion::SLERP(sourceRotation, targetRotation, alpha).ToAxes();

            // Lerp side sizes
            for (int i = 0; i < 3; ++i) {
                box.sideVectors[i] *= ::ProperRT::LERP(source.sideVectors[i].Norm(), target.sideVectors[i].Norm(), alpha);
            }

            return box;
        }

        /// @brief Converting ctor.
        /// @param alignedBoundingBox Axis aligned bounding box to construct from
        OrientedBoundingBox(AABB const& alignedBoundingBox) {
            if (!alignedBoundingBox.IsValid()) {
                throw std::runtime_error("The input bounding box must be valid when creating an oriented bounding box");//TODO
            }
            
            corner = alignedBoundingBox.minBounds;
            for (int axis = 0; axis < 3; ++axis) {
                auto point = corner;
                point[axis] = alignedBoundingBox.maxBounds[axis];
                sideVectors[axis] = point - corner;
            }
        }

        /// @brief Returns all corner points of the bounding box.
        CUDA_FUNCTION
        Vector<8, VertexType> AllPoints() const noexcept {
            auto cornerUp = corner + sideVectors.y();
            return {
                corner, corner + sideVectors.z(), corner + sideVectors.z() + sideVectors.x(), corner + sideVectors.x(),
                cornerUp, cornerUp + sideVectors.z(), cornerUp + sideVectors.z() + sideVectors.x(), cornerUp + sideVectors.x()
            };
        }

        /// @brief Transforms the bounding box using @p matrix.
        /// @param matrix Transformation matrix to transform the box with
        /// @return Transformed bounding box according to @p matrix
        CUDA_FUNCTION
        OrientedBoundingBox Transform(Matrix4x4cf const& matrix) const noexcept {
            return OrientedBoundingBox{
                VertexType{ matrix * Vector4cf{ static_cast<Vector3cf>(corner), 1._cf } },
                {
                    VertexType{ matrix * Vector4cf{ static_cast<Vector3cf>(sideVectors.x()), 0._cf } },
                    VertexType{ matrix * Vector4cf{ static_cast<Vector3cf>(sideVectors.y()), 0._cf } },
                    VertexType{ matrix * Vector4cf{ static_cast<Vector3cf>(sideVectors.z()), 0._cf } }
                }
            };
        }

        /// @brief Returns the axis-aligned bounding box of the cuboid.
        CUDA_FUNCTION
        AABB GetAABB() const noexcept {
            auto points = AllPoints();
            AABB bb = AABB::InfinitelySmall();
            for (int i = 0; i < 8; ++i) {
                bb.Include(points[i]);
            }
            return bb;
        }

        /// @brief Unimplemented
        CUDA_FUNCTION
        CuSTD::pair<AABB, AABB> SplitClipBB(AABB const& clippingBox, int splitAxis, cfloat splitCoord) const {
            //TODO
            return CuSTD::pair<AABB, AABB>{ AABB::InfinitelySmall(), AABB::InfinitelySmall() };
        }

        /// @brief Calculates intersection of @p ray with this box, setting @p t if intersects.
		/// @param ray Ray to intersect with
		/// @param[out] t Distance of the intersection from ray origin
		/// @return True if @p ray intersects, false otherwise
        CUDA_FUNCTION
        bool Intersect(Ray const& ray, cfloat& t, IntersectionCoordinates& ic) const {
            // Transform ray and bounding box so that the box becomes an AABB
            //TODO
            //Mat3x3 rotation = Mat3x3::Rows({ sideVectors.x().Normalized(), sideVectors.y().Normalized(), sideVectors.z().Normalized() })
            AABB aabb{ corner, corner + VertexType{ 1, 1, 1 } * sideVectors.CudaTransform([] (VertexType const& v) { return v.Norm(); }) };
            // Intersect transformed ray with AABB
            return aabb.Intersect(ray, t, ic);
        }

        /// @brief Returns the normal of this box.
        CUDA_FUNCTION
        Vector3cf GetNormal(IntersectionCoordinates const& ic) const {
            //TODO signs might be wrong
            if (ic.side < 2) {
                return ((1 - ic.side) * -1._cf) * Vector3cf{ sideVectors[0] }.NormalizedSafe();
            }
            else if (ic.side < 4) {
                return ((1 - (ic.side - 2)) * -1._cf) * Vector3cf{ sideVectors[1] }.NormalizedSafe();
            }
            else {
                return ((1 - (ic.side - 4)) * -1._cf) * Vector3cf{ sideVectors[2] }.NormalizedSafe();
            }
        }
    };
}
