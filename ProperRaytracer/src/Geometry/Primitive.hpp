/**
 * File name: Primitive.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <variant>
#include <cuda/std/variant>

#include "IndexedTriangle.hpp"
#include "OrientedBoundingBox.hpp"
#include "Ray.hpp"
#include "Utilities/TypeList.hpp"

namespace ProperRT
{
    /// @brief Type list with primitive types.
    using PrimitiveTypes = TypeList<Triangle, OrientedBoundingBox>;

    /// @brief Type list with indexed primitive types.
    using IndexedPrimitiveTypes = TypeList<IndexedTriangle>;

    /// @brief Variant for abstracting different primitives.
    using Primitive = PassTypeList_t<CuSTD::variant, PrimitiveTypes>;

    namespace Detail
    {
        template<class... T>
        struct ToIntersectionCoordinatesTypes
        {
            using IntersectionCoordinatesTypeList = TypeList<typename T::IntersectionCoordinates ...>;
        };
    }

    /// Intersection coordinates variant for all primitive types.
    using IntersectionCoordinates = PassTypeList_t<CuSTD::variant, PassTypeList_t<Detail::ToIntersectionCoordinatesTypes, PrimitiveTypes>::IntersectionCoordinatesTypeList>;

    /// @brief Intersects @p ray with @p primitive and saves the intersection distance to @p t and coordinates to @p ic.
    /// @param ray Ray to intersect with
    /// @param primitive Primitive to intersect the ray with
    /// @param t Intersection distance
    /// @param ic Intersection coordinates
    /// @return True if the ray intersects with the primitive, false otherwise
    CUDA_FUNCTION inline
    bool Intersect(Ray const& ray, Primitive const& primitive, cfloat& t, IntersectionCoordinates& ic) {
        return CuSTD::visit([&ray, &t, &ic] (auto const& p) {
            using TPrimitive = std::remove_cvref_t<decltype(p)>;
            typename TPrimitive::IntersectionCoordinates ic_;
            bool r = p.Intersect(ray, t, ic_);
            ic = ic_;
            return r;
        }, primitive);
    }

    /// @brief Returns the the geometric normal of @p primitive at @p ic.
    /// @param primitive Primitive
    /// @param ic Intersection coordinates
    CUDA_FUNCTION inline
    Vector3cf GetNormal(Primitive const& primitive, IntersectionCoordinates const& ic) {
        return CuSTD::visit([&ic] (auto const& p) {
            using TPrimitive = std::remove_cvref_t<decltype(p)>;
            return p.GetNormal(CuSTD::get<typename TPrimitive::IntersectionCoordinates>(ic));
        }, primitive);
    }

    /// @brief Returns the axis-aligned bounding box of @p primitive.
    /// @param primitive Primitive
    CUDA_FUNCTION inline
    AABB GetAABB(Primitive const& primitive) {
        return CuSTD::visit([] (auto const& p) {
            return p.GetAABB();
        }, primitive);
    }
    
    /// @brief Splits @p primitive into 2 parts located inside left and right parts of @p clippingBox, split
    /// by @p splitCoord on @p splitAxis, and computes their tight bounding boxes.
    /// @param primitive Primitive to split
    /// @param clippingBox Bounding box to clip the primitive with, that is split by @p splitCoord
    /// @param splitAxis The axis of the split
    /// @param splitCoord The coordinate of the split
    /// @return Left and right tight bounding boxes of the split and clipped primitive
    CUDA_FUNCTION inline
    CuSTD::pair<AABB, AABB> SplitClipBB(Primitive const& primitive, AABB const& clippingBox, int splitAxis, cfloat splitCoord) {
        return CuSTD::visit([&clippingBox, splitAxis, splitCoord] (auto const& p) {
            return p.SplitClipBB(clippingBox, splitAxis, splitCoord);
        }, primitive);
    }
}