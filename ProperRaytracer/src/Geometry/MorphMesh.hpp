/**
 * File name: MorphMesh.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <span>

#include "Matrix.hpp"
#include "GenericMesh.hpp"
#include "Primitive.hpp"

namespace ProperRT
{
    struct RasterizationMatrices;
    
    class MorphMesh : public GenericMesh
    {
    public:
        /// @brief Class for returning info on blending of two meshes.
        struct MeshBlendInfo
        {
            /// @brief Interpolation alpha between the two meshes, lies between 0 and 1.
            float blendAlpha;
            /// @brief Index of the first mesh to be interpolated, second index is this plus one.
            int32 firstIndex;

            CUDA_FUNCTION
            int32 FirstIndex() const noexcept { return firstIndex; }

            CUDA_FUNCTION
            int32 SecondIndex() const noexcept { return firstIndex + 1; }
        };

    public:
        /// @brief Returns the number of key frame meshes.
        virtual
        int32 MeshCount() const = 0;

        /// @brief Fetches a primitive from the mesh.
        /// @param submeshIdx Index of the submesh
        /// @param primitiveIdx Index of the primitive in a submesh
        /// @param blendInfo Current blending phase info
        virtual
        Primitive GetPrimitive(int32 submeshIdx, int32 primitiveIdx, MeshBlendInfo blendInfo) const = 0;

        /// @brief Fetches a primitive from the mesh and transforms it to world space.
        /// @param submeshIdx Index of the submesh
        /// @param primitiveIdx Index of the primitive in a submesh
        /// @param blendInfo Current blending phase info
        virtual
        Primitive GetPrimitive(int32 submeshIdx, int32 primitiveIdx, MeshBlendInfo blendInfo, Matrix4x4cf const& transform) const = 0;

        /// @brief Fills a GPU mesh manager instance with GPU data.
        /// @param instance Instance to fill
        /// @param blendInfo Current blend info
        /// @param submeshIdx Index of the submesh to fill instance with
        virtual
        void FillSubmeshInstance(GPUMeshManagement::Instance& instance, MeshBlendInfo blendInfo, int32 submeshIdx) = 0;

        /// @brief Rasterizes the mesh.
        /// @details Do not call when in Raytracing rendering mode.
        /// @param modelMat Matrix for transforming the mesh into world space
        /// @param matrices Matrices used for projection
        /// @param blendInfo Current blending phase info
        /// @param bind Set to false if you are rendering this mesh multiple times and have called @ref GenericMesh::GLBind() yourself.
        virtual
        void Rasterize(Matrix4x4f const& modelMat, RasterizationMatrices const& matrices, MeshBlendInfo blendInfo, bool bind = true) = 0;

        /// @brief Rasterizes submeshes according to @p submeshIndeces.
        /// @details Do not call when in Raytracing rendering mode.
        /// @param modelMat Matrix for transforming the mesh into world space
        /// @param matrices Matrices used for projection
        /// @param submeshIndeces Indeces of submeshes to be rendered
        /// @param blendInfo Current blending phase info
        /// @param bind Set to false if you are rendering this mesh multiple times and have called @ref GenericMesh::GLBind() yourself.
        virtual
        void Rasterize(Matrix4x4f const& modelMat, RasterizationMatrices const& matrices, std::span<int32 const> submeshIndeces, MeshBlendInfo blendInfo, bool bind = true) = 0;

        virtual
        ~MorphMesh() = default;
    };
}
