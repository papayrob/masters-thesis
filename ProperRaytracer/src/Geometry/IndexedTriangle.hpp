/**
 * File name: IndexedTriangle.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <span>

#include "Triangle.hpp"

namespace ProperRT
{
	/// @brief Class for representing indexed triangles.
	struct IndexedTriangle
	{
        using VertexType = Triangle::VertexType;
        
		/// @brief Vertex of the triangle
		int32_t v0{}, v1{}, v2{};

        /// @brief Offset the indeces with @p offset.
        /// @param offset Integer to offset the indeces with
        /// @return Indexed triangle with offset indeces
        CUDA_FUNCTION
        IndexedTriangle Offset(int32_t offset) const {
            return IndexedTriangle{ v0 + offset, v1 + offset, v2 + offset };
        }

        /// @brief Replaces the indeces with actual vertices.
        /// @param vertices Array of vertices belonging to the indexed mesh
        /// @return Triangle with actual vertices instead of indeces
        Triangle ToTriangle(std::span<VertexType const> vertices) const {
            return DeIndex(vertices.data());
        }

        /// @brief Replaces the indeces with actual vertices.
        /// @param vertices Array of vertices belonging to the indexed mesh
        /// @return Triangle with actual vertices instead of indeces
        Triangle DeIndex(std::span<VertexType const> vertices) const {
            return DeIndex(vertices.data());
        }

        /// @brief Replaces the indeces with actual vertices.
        /// @param vertices Array of vertices belonging to the indexed mesh
        /// @return Triangle with actual vertices instead of indeces
        CUDA_FUNCTION
        Triangle DeIndex(VertexType const* vertices) const {
            return Triangle{{ vertices[v0], vertices[v1], vertices[v2] }};
        }
	};
}
