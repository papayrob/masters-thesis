/**
 * File name: Matrix.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "Vector.hpp"
#include <cuda/std/utility>

namespace ProperRT
{
    /// @brief Concept for square matrices (row count equals column count).
    template<class TMatrix>
    concept SquareMatrix = (TMatrix::ROW_COUNT == TMatrix::COLUMN_COUNT);

    /// @brief Class for representing mathematical matrices.
    /// @tparam NRows Number of rows of the matrix
    /// @tparam NCols Number of columns of the matrix
    /// @tparam TData Data type
    template<int NRows, int NCols, class TData>
	struct Matrix
	{
    public:
        template<int NRowsOther, int NColsOther, class TDataOther>
        friend struct Matrix;

        /// @brief Number of rows of the matrix
        inline static constexpr
        int ROW_COUNT = NRows;

        /// @brief Number of columns of the matrix
        inline static constexpr
        int COLUMN_COUNT = NCols;

        using DataType = TData;

        /// @brief Vector with NCols elements
        using ColumnVector = Vector<NRows, TData>;
        /// @brief Vector with NRows elements
        using RowVector = Vector<NCols, TData>;

        /// @brief Class containing an element value an its index.
        struct Element
        {
            /// @brief Element value.
            TData value;
            /// @brief Element index.
            int idx;
        };
    
    public:
        /// @brief Creates a matrix from column vectors and returns it.
        /// @param columns Input columns
        static constexpr
        Matrix FromColumns(Vector<NCols, ColumnVector> const& columns) {
            return Matrix{ columns };
        }

        /// @brief Creates a matrix from row vectors and returns it.
        /// @param rows Input rows
        static constexpr
        Matrix FromRows(Vector<NRows, RowVector> const& rows) {
            if constexpr (SquareMatrix<Matrix>) {
                Matrix m{ rows };
                m.Transpose();
                return m;
            }
            else {
                return Matrix<NCols, NRows, TData>{ rows }.Transposed();
            }
        }

        /// @brief Creates an identity matrix and returns it if the matrix is square.
        static constexpr
        Matrix Identity() requires SquareMatrix<Matrix> {
            Matrix m{};
            for (int i = 0; i < NCols; ++i) {
                m[i][i] = TData{ 1 };
            }
            return m;
        }

        /// @brief Creates a matrix with @p diagonalElement on its diagonal and returns it if the matrix is square.
        /// @param diagonalElement Element to put on the diagonal
        static constexpr
        Matrix Diagonal(TData const& diagonalElement) requires SquareMatrix<Matrix> {
            Matrix m{};
            for (int i = 0; i < NCols; ++i) {
                m[i][i] = diagonalElement;
            }
            return m;
        }

        /// @brief Creates a matrix with elements from @p diagonal on its diagonal and returns it if the matrix is square.
        /// @param diagonal Elements to put on the diagonal
        static constexpr
        Matrix Diagonal(ColumnVector const& diagonal) requires SquareMatrix<Matrix> {
            Matrix m{};
            for (int i = 0; i < NCols; ++i) {
                m[i][i] = diagonal[i];
            }
            return m;
        }

    public:
        Vector<NCols, ColumnVector> columns{};

        /// @brief Default ctor.
        constexpr
        Matrix() noexcept = default;

        /// @brief Copy ctor.
        constexpr
        Matrix(Matrix const&) noexcept = default;

        /// @brief Copy assignment operator.
        constexpr
        Matrix& operator=(Matrix const&) noexcept = default;

        /// @brief Convert ctor with different element type.
        template<class TDataOther>
        constexpr
        Matrix(Matrix<NRows, NCols, TDataOther> const& other) noexcept {
            for (int i = 0; i < NCols; ++i) {
                columns[i] = static_cast<ColumnVector>(other.columns[i]);
            }
        }

        /// @brief Column access operator.
        /// @param columnIdx Column index to retrieve
        CUDA_FUNCTION constexpr
        ColumnVector& operator[](auto columnIdx) {
            return columns[columnIdx];
        }

        /// @brief Column access operator.
        /// @param columnIdx Column index to retrieve
        CUDA_FUNCTION constexpr
        ColumnVector const& operator[](auto columnIdx) const {
            return columns[columnIdx];
        }

        /// @brief Row access operator.
        /// @param rowIdx Row index to retrieve
        CUDA_FUNCTION constexpr
        Vector<NCols, TData&> Row(auto rowIdx) {
            Vector<NCols, TData&> row;
            for (int c = 0; c < NCols; ++c) {
                row[rowIdx] = columns[c][rowIdx];
            }
            return row;
        }

        /// @brief Row access operator.
        /// @param rowIdx Row index to retrieve
        CUDA_FUNCTION constexpr
        RowVector Row(auto rowIdx) const {
            RowVector row;
            for (int c = 0; c < NCols; ++c) {
                row[rowIdx] = columns[c][rowIdx];
            }
            return row;
        }

        friend CUDA_FUNCTION constexpr
        Matrix operator+(Matrix lhs, Matrix const& rhs) {
            lhs += rhs;
            return lhs;
        }

        CUDA_FUNCTION constexpr
        Matrix& operator+=(Matrix const& other) {
            for (int c = 0; c < NCols; ++c) {
                for (int r = 0; r < NRows; ++r) {
                    columns[c][r] += other.columns[c][r];
                }
            }
            return *this;
        }

        template<int NColsOther>
        friend CUDA_FUNCTION constexpr
        Matrix<NRows, NColsOther, TData> operator*(Matrix const& lhs, Matrix<NCols, NColsOther, TData> const& rhs) {
            Matrix<NRows, NColsOther, TData> result;

            for (int columnRes = 0; columnRes < NColsOther; ++columnRes) {
                result[columnRes] = operator*(lhs, rhs[columnRes]);
            }

            return result;
        }

        CUDA_FUNCTION constexpr
        Matrix& operator*=(Matrix const& other) requires SquareMatrix<Matrix> {
            *this = operator*(*this, other);
            return *this;
        }

        CUDA_FUNCTION static constexpr
        Matrix ElementProduct(Matrix lhs, Matrix const& rhs) {
            for (int c = 0; c < NCols; ++c) {
                for (int r = 0; r < NRows; ++r) {
                    lhs.columns[c][r] *= rhs.columns[c][r];
                }
            }
            return lhs;
        }

        friend CUDA_FUNCTION constexpr
        ColumnVector operator*(Matrix const& matrix, RowVector const& vector) {
            ColumnVector result{};
            for (int c = 0; c < NCols; ++c) {
                result += matrix[c] * vector[c];
            }
            return result;
        }

        /// @brief Multiplies matrix @p m by scalar @p s.
        /// @param s Input scalar
        /// @param m Input matrix
        /// @return Resulting matrix
        friend CUDA_FUNCTION constexpr
        Matrix operator*(TData const& s, Matrix m) {
            m *= s;
            return m;
        }

        /// @brief Multiplies matrix @p m by scalar @p s.
        /// @param m Input matrix
        /// @param s Input scalar
        /// @return Resulting matrix
        friend CUDA_FUNCTION constexpr
        Matrix operator*(Matrix m, TData const& s) {
            m *= s;
            return m;
        }

        /// @brief Multiplies this matrix by scalar @p s.
        /// @param s Input scalar
        /// @return Reference to this for chaining
        CUDA_FUNCTION constexpr
        Matrix& operator*=(TData const& s) {
            for (int i = 0; i < NCols; ++i) {
                columns[i] *= s;
            }
            return *this;
        }

        static
        Matrix Inverse(Matrix const& m) requires SquareMatrix<Matrix>;

        Matrix& Inverse() requires SquareMatrix<Matrix> {
            *this = Matrix::Inverse(*this);
            return *this;
        }

        Matrix Inversed() const requires SquareMatrix<Matrix> {
            return Matrix::Inverse(*this);
        }

        CUDA_FUNCTION constexpr
        Matrix Transposed() const {
            Matrix<NCols, NRows, TData> m;
            for (int c = 0; c < NCols; ++c) {
                for (int r = 0; r < NRows; ++r) {
                    m.columns[r][c] = columns[c][r];
                }
            }
            return m;
        }

        CUDA_FUNCTION constexpr
        Matrix& Transpose() {
            for (int c = 0; c < NCols; ++c) {
                for (int r = 0; r < c; ++r) {
                    CuSTD::swap(columns[r][c], columns[c][r]);
                }
            }
            return *this;
        }

    private:
        constexpr
        Matrix(Vector<NCols, ColumnVector> const& columns_) noexcept
            : columns{ columns_ }
        {}
    };

    template<int NRows, int NCols, class TData>
    Matrix<NRows,NCols,TData> Matrix<NRows,NCols,TData>::Inverse(Matrix const& m) requires SquareMatrix<Matrix> {
        using Matrix = Matrix<NRows,NCols,TData>;
        if constexpr (NRows == 3) {
            // Source: https://stackoverflow.com/questions/983999/simple-3x3-matrix-inverse-code-c
            auto det = m[0][0] * (m[1][1] * m[2][2] - m[2][1] * m[1][2]) -
                       m[0][1] * (m[1][0] * m[2][2] - m[1][2] * m[2][0]) +
                       m[0][2] * (m[1][0] * m[2][1] - m[1][1] * m[2][0]);

            auto invdet = TData{ 1 } / det;

            Matrix minv;
            minv[0][0] = (m[1][1] * m[2][2] - m[2][1] * m[1][2]) * invdet;
            minv[0][1] = (m[0][2] * m[2][1] - m[0][1] * m[2][2]) * invdet;
            minv[0][2] = (m[0][1] * m[1][2] - m[0][2] * m[1][1]) * invdet;
            minv[1][0] = (m[1][2] * m[2][0] - m[1][0] * m[2][2]) * invdet;
            minv[1][1] = (m[0][0] * m[2][2] - m[0][2] * m[2][0]) * invdet;
            minv[1][2] = (m[1][0] * m[0][2] - m[0][0] * m[1][2]) * invdet;
            minv[2][0] = (m[1][0] * m[2][1] - m[2][0] * m[1][1]) * invdet;
            minv[2][1] = (m[2][0] * m[0][1] - m[0][0] * m[2][1]) * invdet;
            minv[2][2] = (m[0][0] * m[1][1] - m[1][0] * m[0][1]) * invdet;
            return minv;
        }
        else if constexpr (NRows == 4) {
            // Source: https://stackoverflow.com/questions/1148309/inverting-a-4x4-matrix
            Matrix inv;
            inv[0][0] = m[1][1] * m[2][2] * m[3][3] - 
                        m[1][1] * m[2][3] * m[3][2] - 
                        m[2][1] * m[1][2] * m[3][3] + 
                        m[2][1] * m[1][3] * m[3][2] +
                        m[3][1] * m[1][2] * m[2][3] - 
                        m[3][1] * m[1][3] * m[2][2];

            inv[1][0] = -m[1][0] * m[2][2] * m[3][3] + 
                        m[1][0] * m[2][3] * m[3][2] + 
                        m[2][0] * m[1][2] * m[3][3] - 
                        m[2][0] * m[1][3] * m[3][2] - 
                        m[3][0] * m[1][2] * m[2][3] + 
                        m[3][0] * m[1][3] * m[2][2];

            inv[2][0] = m[1][0] * m[2][1] * m[3][3] - 
                        m[1][0] * m[2][3] * m[3][1] - 
                        m[2][0] * m[1][1] * m[3][3] + 
                        m[2][0] * m[1][3] * m[3][1] + 
                        m[3][0] * m[1][1] * m[2][3] - 
                        m[3][0] * m[1][3] * m[2][1];

            inv[3][0] = -m[1][0] * m[2][1] * m[3][2] + 
                        m[1][0] * m[2][2] * m[3][1] +
                        m[2][0] * m[1][1] * m[3][2] - 
                        m[2][0] * m[1][2] * m[3][1] - 
                        m[3][0] * m[1][1] * m[2][2] + 
                        m[3][0] * m[1][2] * m[2][1];

            inv[0][1] = -m[0][1] * m[2][2] * m[3][3] + 
                        m[0][1] * m[2][3] * m[3][2] + 
                        m[2][1] * m[0][2] * m[3][3] - 
                        m[2][1] * m[0][3] * m[3][2] - 
                        m[3][1] * m[0][2] * m[2][3] + 
                        m[3][1] * m[0][3] * m[2][2];

            inv[1][1] = m[0][0] * m[2][2] * m[3][3] - 
                        m[0][0] * m[2][3] * m[3][2] - 
                        m[2][0] * m[0][2] * m[3][3] + 
                        m[2][0] * m[0][3] * m[3][2] + 
                        m[3][0] * m[0][2] * m[2][3] - 
                        m[3][0] * m[0][3] * m[2][2];

            inv[2][1] = -m[0][0] * m[2][1] * m[3][3] + 
                        m[0][0] * m[2][3] * m[3][1] + 
                        m[2][0] * m[0][1] * m[3][3] - 
                        m[2][0] * m[0][3] * m[3][1] - 
                        m[3][0] * m[0][1] * m[2][3] + 
                        m[3][0] * m[0][3] * m[2][1];

            inv[3][1] = m[0][0] * m[2][1] * m[3][2] - 
                        m[0][0] * m[2][2] * m[3][1] - 
                        m[2][0] * m[0][1] * m[3][2] + 
                        m[2][0] * m[0][2] * m[3][1] + 
                        m[3][0] * m[0][1] * m[2][2] - 
                        m[3][0] * m[0][2] * m[2][1];

            inv[0][2] = m[0][1] * m[1][2] * m[3][3] - 
                        m[0][1] * m[1][3] * m[3][2] - 
                        m[1][1] * m[0][2] * m[3][3] + 
                        m[1][1] * m[0][3] * m[3][2] + 
                        m[3][1] * m[0][2] * m[1][3] - 
                        m[3][1] * m[0][3] * m[1][2];

            inv[1][2] = -m[0][0] * m[1][2] * m[3][3] + 
                        m[0][0] * m[1][3] * m[3][2] + 
                        m[1][0] * m[0][2] * m[3][3] - 
                        m[1][0] * m[0][3] * m[3][2] - 
                        m[3][0] * m[0][2] * m[1][3] + 
                        m[3][0] * m[0][3] * m[1][2];

            inv[2][2] = m[0][0] * m[1][1] * m[3][3] - 
                        m[0][0] * m[1][3] * m[3][1] - 
                        m[1][0] * m[0][1] * m[3][3] + 
                        m[1][0] * m[0][3] * m[3][1] + 
                        m[3][0] * m[0][1] * m[1][3] - 
                        m[3][0] * m[0][3] * m[1][1];

            inv[3][2] = -m[0][0] * m[1][1] * m[3][2] + 
                        m[0][0] * m[1][2] * m[3][1] + 
                        m[1][0] * m[0][1] * m[3][2] - 
                        m[1][0] * m[0][2] * m[3][1] - 
                        m[3][0] * m[0][1] * m[1][2] + 
                        m[3][0] * m[0][2] * m[1][1];

            inv[0][3] = -m[0][1] * m[1][2] * m[2][3] + 
                        m[0][1] * m[1][3] * m[2][2] + 
                        m[1][1] * m[0][2] * m[2][3] - 
                        m[1][1] * m[0][3] * m[2][2] - 
                        m[2][1] * m[0][2] * m[1][3] + 
                        m[2][1] * m[0][3] * m[1][2];

            inv[1][3] = m[0][0] * m[1][2] * m[2][3] - 
                        m[0][0] * m[1][3] * m[2][2] - 
                        m[1][0] * m[0][2] * m[2][3] + 
                        m[1][0] * m[0][3] * m[2][2] + 
                        m[2][0] * m[0][2] * m[1][3] - 
                        m[2][0] * m[0][3] * m[1][2];

            inv[2][3] = -m[0][0] * m[1][1] * m[2][3] + 
                        m[0][0] * m[1][3] * m[2][1] + 
                        m[1][0] * m[0][1] * m[2][3] - 
                        m[1][0] * m[0][3] * m[2][1] - 
                        m[2][0] * m[0][1] * m[1][3] + 
                        m[2][0] * m[0][3] * m[1][1];

            inv[3][3] = m[0][0] * m[1][1] * m[2][2] - 
                        m[0][0] * m[1][2] * m[2][1] - 
                        m[1][0] * m[0][1] * m[2][2] + 
                        m[1][0] * m[0][2] * m[2][1] + 
                        m[2][0] * m[0][1] * m[1][2] - 
                        m[2][0] * m[0][2] * m[1][1];

            auto det = m[0][0] * inv[0][0] + m[0][1] * inv[1][0] + m[0][2] * inv[2][0] + m[0][3] * inv[3][0];

            if (det == 0) {
                throw MathException{ "Cannot invert a singular matrix" };
            }

            det = TData{ 1 } / det;

            inv *= det;
            return inv;
        }
        else {
            throw std::runtime_error{ "Unimplemented "};
        }
    }

    using Matrix4x4sf = Matrix<4, 4, sfloat>;
    using Matrix4x4cf = Matrix<4, 4, cfloat>;
    using Matrix4x4f = Matrix<4, 4, float>;

    template<class TData>
    inline constexpr
    Matrix<4, 4, TData> TranslationMatrix(Vector<3, TData> const& translation) {
        auto m = Matrix<4, 4, TData>::Identity();
        m[3][0] = translation.x();
        m[3][1] = translation.y();
        m[3][2] = translation.z();
        return m;
    }

    template<class TData>
    inline constexpr
    Matrix<4, 4, TData> ScaleMatrix(Vector<3, TData> const& scale) {
        Matrix<4, 4, TData> m{};
        m[0][0] = scale.x();
        m[1][1] = scale.y();
        m[2][2] = scale.z();
        m[3][3] = TData{ 1 };
        return m;
    }

    template<class TData>
    inline constexpr
    Matrix<4, 4, TData> RotationMatrixX(TData angleRad) {
        Matrix<4, 4, TData> m{};
        m[0][0] = TData{ 1 };
        m[1][1] = std::cos(angleRad);
        m[2][2] = std::cos(angleRad);
        m[1][2] = std::sin(angleRad);
        m[2][1] = -std::sin(angleRad);
        m[3][3] = TData{ 1 };
        return m;
    }

    template<class TData>
    inline constexpr
    Matrix<4, 4, TData> RotationMatrixY(TData angleRad) {
        Matrix<4, 4, TData> m{};
        m[0][0] = std::cos(angleRad);
        m[1][1] = TData{ 1 };
        m[2][2] = std::cos(angleRad);
        m[0][2] = -std::sin(angleRad);
        m[2][0] = std::sin(angleRad);
        m[3][3] = TData{ 1 };
        return m;
    }

    template<class TData>
    inline constexpr
    Matrix<4, 4, TData> RotationMatrixZ(TData angleRad) {
        Matrix<4, 4, TData> m{};
        m[0][0] = std::cos(angleRad);
        m[1][1] = std::cos(angleRad);
        m[2][2] = TData{ 1 };
        m[0][1] = std::sin(angleRad);
        m[1][0] = -std::sin(angleRad);
        m[3][3] = TData{ 1 };
        return m;
    }

    template<class TData>
    inline constexpr
    Matrix<4, 4, TData> PerspectiveMatrix(TData near, TData far, TData verticalFOV, TData aspectRatio) {
        TData fovTerm = std::tan(verticalFOV / TData{ 2 });
        Matrix<4, 4, TData> m;
        m[0][0] = TData{ 1 } / (fovTerm * aspectRatio);
        m[1][1] = TData{ 1 } / fovTerm;
        m[2][2] = -(far + near) / (far - near);
        m[2][3] = TData{ -1 };
        m[3][2] = (TData{ -2 } * far * near) / (far - near);
        return m;
    }
}
