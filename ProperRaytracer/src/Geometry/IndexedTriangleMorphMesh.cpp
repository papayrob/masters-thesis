/**
 * File name: IndexedTriangleMorphMesh.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "IndexedTriangleMorphMesh.hpp"

#include "glbinding/gl/gl.h"

#include "Graphics/OpenGL/OGLUtilities.hpp"
#include "Utilities/Utilities.hpp"
#include "SceneDefinition/Components/Camera.hpp"
#include "SceneDefinition/Components/Transform.hpp"
#include "Graphics/OpenGL/Shader.hpp"
#include "Graphics/Raytracing/GPUMeshManager.cuh"
#include "RTApp.hpp"

namespace
{

ProperRT::OpenGL::Shader shader;

}

namespace ProperRT
{
    struct IndexedTriangleMorphMesh::ComputeStorage
    {
        DeviceUniquePointer<Vector3sf[]> vertices;
        DeviceUniquePointer<IndexedTriangle[]> indeces;
    };

    IndexedTriangleMorphMesh::IndexedTriangleMorphMesh(std::vector<std::shared_ptr<IndexedTriangleMesh>> meshes) {
        if (RTApp::GetRenderingMode() == RTApp::RenderingMode::OpenGL && shader.Program() == 0) {
            shader = OpenGL::Shader{OpenGL::Shader::Sources{
                .vertexSource{ OpenGL::Shader::LoadShaderSource(AssetID{"Shaders/TriangleMorphMesh.vs"}) },
                .geometrySource{ OpenGL::Shader::LoadShaderSource(AssetID{"Shaders/TriangleMesh.gs"}) },
                .fragmentSource{ OpenGL::Shader::LoadShaderSource(AssetID{"Shaders/TriangleMesh.fs"}) }
            }};
        }

        if (meshes.size() == 0) {
            throw std::runtime_error("Cannot create a morph target mesh from zero meshes");
        }
        meshN = StdSize<int32>(meshes);

        // Copy indeces and submeshes from the first mesh
        indeces = static_cast<IndexedTriangleMesh const&>(*meshes.front()).Indeces();
        auto const& singleSubmeshes = meshes.front()->Submeshes();
        for (std::vector_span<IndexedTriangle const> submesh : singleSubmeshes) {
            submeshes.push_back(std::vector_span<IndexedTriangle const>(indeces).subspan(submesh.offset(), submesh.size()));
        }
        singleMeshVertexN = StdSize<int32>(meshes.front()->Vertices());

        materials = { meshes.front()->Materials().begin(), meshes.front()->Materials().end() };

        // Copy vertices and vertex attributes from all meshes into a single array
        vertices.reserve(singleMeshVertexN * meshes.size());
        for (auto const& mesh : meshes) {
            if (mesh->Vertices().size() != meshes.front()->Vertices().size() || mesh->Indeces().size() != meshes.front()->Indeces().size()) {
                throw std::runtime_error("Different mesh attribute counts when creating a morph target mesh");
            }
            std::copy(mesh->Vertices().begin(), mesh->Vertices().end(), std::back_inserter(vertices));
        }
    }

    IndexedTriangleMorphMesh::~IndexedTriangleMorphMesh() {
        ReleaseGPUResources();
    }

    IndexedTriangleMorphMesh::IndexedTriangleMorphMesh(IndexedTriangleMorphMesh&&) = default;

    IndexedTriangleMorphMesh& IndexedTriangleMorphMesh::operator=(IndexedTriangleMorphMesh&&) = default;

    Primitive IndexedTriangleMorphMesh::GetPrimitive(int32 submeshIdx, int32 primitiveIdx, MeshBlendInfo blendInfo) const {
        return Triangle::LERP(
            submeshes.at(submeshIdx)[primitiveIdx].Offset(blendInfo.FirstIndex() * singleMeshVertexN).ToTriangle(vertices),
            submeshes.at(submeshIdx)[primitiveIdx].Offset(blendInfo.SecondIndex() * singleMeshVertexN).ToTriangle(vertices),
            blendInfo.blendAlpha
        );
    }

    Primitive IndexedTriangleMorphMesh::GetPrimitive(int32 submeshIdx, int32 primitiveIdx, MeshBlendInfo blendInfo, Matrix4x4cf const& transform) const {
        return Triangle::LERP(
            submeshes.at(submeshIdx)[primitiveIdx].Offset(blendInfo.FirstIndex() * singleMeshVertexN).ToTriangle(vertices),
            submeshes.at(submeshIdx)[primitiveIdx].Offset(blendInfo.SecondIndex() * singleMeshVertexN).ToTriangle(vertices),
            blendInfo.blendAlpha
        ).Transform(transform);
    }

    std::vector<int32> IndexedTriangleMorphMesh::GetPrimitiveCounts() const noexcept {
        std::vector<int32> out(submeshes.size());
        std::transform(submeshes.begin(), submeshes.end(), out.begin(), [](auto submesh) { return StdSize<int32>(submesh); });
        return out;
    }

    void IndexedTriangleMorphMesh::SyncToGPU() {
        if (VAO == 0) {
            if (RTApp::GetRenderingMode() == RTApp::RenderingMode::OpenGL) {
                // Create VAO
                gl::glCreateVertexArrays(1, &VAO);

                // Create buffers
                vertexBuffer = CreateOGLBuffer(std::span(vertices));
                indexBuffer = CreateOGLBuffer(std::span(indeces));

                // Enable attributes
                gl::glEnableVertexArrayAttrib(VAO, 0);
                gl::glEnableVertexArrayAttrib(VAO, 1);
                
                auto constexpr bufferStride = sizeof(decltype(vertices)::value_type);
                gl::glVertexArrayVertexBuffer(VAO, 0, vertexBuffer, 0, bufferStride);
                gl::glVertexArrayVertexBuffer(VAO, 1, vertexBuffer, singleMeshVertexN * bufferStride, bufferStride);
                
                gl::glVertexArrayAttribFormat(VAO, 0, 3, gl::GLenum::GL_FLOAT, gl::GL_FALSE, 0);
                gl::glVertexArrayAttribFormat(VAO, 1, 3, gl::GLenum::GL_FLOAT, gl::GL_FALSE, 0);
                
                gl::glVertexArrayAttribBinding(VAO, 0, 0);
                gl::glVertexArrayAttribBinding(VAO, 1, 1);

                // Attach element buffer
                gl::glVertexArrayElementBuffer(VAO, indexBuffer);
            }
            else {
                VAO = 1;
            }

            // Create compute storage
            computeStorage = std::make_unique<ComputeStorage>();
            computeStorage->vertices = DeviceUniquePointer<Vector3sf[]>::AllocateAndUpload(vertices);
            computeStorage->indeces = DeviceUniquePointer<IndexedTriangle[]>::AllocateAndUpload(indeces);
        }
        else {
            if (RTApp::GetRenderingMode() == RTApp::RenderingMode::OpenGL) {
                gl::glNamedBufferSubData(vertexBuffer, 0, std::span(vertices).size_bytes(), vertices.data());
                gl::glNamedBufferSubData(indexBuffer, 0, std::span(indeces).size_bytes(), indeces.data());
            }

            computeStorage->vertices.Upload(vertices);
            computeStorage->indeces.Upload(indeces);
        }
    }

    void IndexedTriangleMorphMesh::ReleaseCPUResources() {
        vertices.clear();
        indeces.clear();
    }

    void IndexedTriangleMorphMesh::ReleaseGPUResources() {
        if (VAO != 0) {
            if (RTApp::GetRenderingMode() == RTApp::RenderingMode::OpenGL) {
                gl::glDeleteBuffers(1, &vertexBuffer);
                gl::glDeleteBuffers(1, &indexBuffer);
                gl::glDeleteVertexArrays(1, &VAO);
            }
            computeStorage = nullptr;
            VAO = 0;
        }
    }

    void IndexedTriangleMorphMesh::GLBind() const {
        if (VAO == 0) {
            throw std::runtime_error("Cannot bind mesh without GPU resources");//TODO
        }

        gl::glBindVertexArray(VAO);
        shader.Use();
    }

    void IndexedTriangleMorphMesh::FillSubmeshInstance(GPUMeshManagement::Instance &instance, MeshBlendInfo blendInfo, int32 submeshIdx) {
        auto& submesh = submeshes[submeshIdx];
        instance.meshData = GPUMeshManagement::IndexedMorphMeshData<IndexedTriangle>{
            computeStorage->vertices.Get(),
            DevicePointer<IndexedTriangle[]>{ computeStorage->indeces.Get().data + submesh.offset(), StdSize<DevicePointer<IndexedTriangle[]>::SizeType>(submesh) },
            blendInfo,
            singleMeshVertexN
        };
    }

    void IndexedTriangleMorphMesh::Rasterize(Matrix4x4f const& modelMat, RasterizationMatrices const &matrices, MeshBlendInfo blendInfo, bool bind) {
        if (VAO == 0) {
            SyncToGPU();
        }
        if (bind) {
            GLBind();
        }

        Matrix4x4f PVM = matrices.PV * modelMat;
        Matrix4x4f VM = matrices.view * modelMat;
        gl::glUniformMatrix4fv(0, 1, gl::GL_FALSE, &PVM[0][0]);
        gl::glUniformMatrix4fv(1, 1, gl::GL_FALSE, &matrices.view[0][0]);
        gl::glUniformMatrix4fv(2, 1, gl::GL_FALSE, &VM[0][0]);

        gl::glUniform4fv(3, 1, Components::Light::ambientColor.elements);
        gl::glUniform1f(10, blendInfo.blendAlpha);

        int submeshIdx = 0;
        for (auto const& submesh : submeshes) {
            gl::GLint materialLoc = 4;
            gl::glUniform4fv(materialLoc++, 1, materials.at(submeshIdx).colorDiffuse.elements);
            //gl::glUniform4fv(materialLoc++, 1, materials.at(submeshIdx).colorSpecular.elements);

            gl::glDrawElementsBaseVertex(
                gl::GLenum::GL_TRIANGLES,
                submesh.size() * 3,
                gl::GLenum::GL_UNSIGNED_INT,
                (void const*)(submesh.offset() * sizeof(decltype(indeces)::value_type)),
                singleMeshVertexN * blendInfo.firstIndex
            );
            ++submeshIdx;
        }
    }

    void IndexedTriangleMorphMesh::Rasterize(Matrix4x4f const& modelMat, RasterizationMatrices const &matrices, std::span<int32 const> submeshIndeces, MeshBlendInfo blendInfo, bool bind) {
        if (VAO == 0) {
            SyncToGPU();
        }
        if (bind) {
            GLBind();
        }

        Matrix4x4f PVM = matrices.PV * modelMat;
        Matrix4x4f VM = matrices.view * modelMat;
        gl::glUniformMatrix4fv(0, 1, gl::GL_FALSE, &PVM[0][0]);
        gl::glUniformMatrix4fv(1, 1, gl::GL_FALSE, &matrices.view[0][0]);
        gl::glUniformMatrix4fv(2, 1, gl::GL_FALSE, &VM[0][0]);

        gl::glUniform4fv(3, 1, Components::Light::ambientColor.elements);
        gl::glUniform1f(10, blendInfo.blendAlpha);
        
        for (auto submeshIdx : submeshIndeces) {
            gl::GLint materialLoc = 4;
            gl::glUniform4fv(materialLoc++, 1, materials.at(submeshIdx).colorDiffuse.elements);
            //gl::glUniform4fv(materialLoc++, 1, materials.at(submeshIdx).colorSpecular.elements);
            
            auto const& submesh = submeshes[submeshIdx];
            
            gl::glDrawElementsBaseVertex(
                gl::GLenum::GL_TRIANGLES,
                submesh.size() * 3,
                gl::GLenum::GL_UNSIGNED_INT,
                (void const*)(submesh.offset() * sizeof(decltype(indeces)::value_type)),
                singleMeshVertexN * blendInfo.firstIndex
            );
        }
    }
}
