/**
 * File name: AABB.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2022 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <limits>
#include <cuda/std/limits>

#include "Ray.hpp"
#include "Matrix.hpp"

namespace ProperRT
{
    /// @brief Struct for representing Axis-Aligned Bounding Boxes.
    struct AABB
    {
        using VertexType = Vector3sf;

        /// @brief Class for storing intersection coordinates as part of Primitive variant.
        struct IntersectionCoordinates
        {
            int side;
            Vector2cf uv;
        };

        /// @brief Bounding boxes are expanded by this value when they have zero volume.
        inline static constexpr
        sfloat MINIMAL_NON_ZERO_LENGTH = 0.000'001_sf;

        /// @brief Bounding box bounds.
        VertexType minBounds, maxBounds;

        /// @brief Default ctor.
        CUDA_FUNCTION
        AABB() : minBounds(VertexType{}), maxBounds(VertexType{}) {}

        /// @brief Ctor with bounds input as vectors.
        /// @param min Coordinates of the min bounds
        /// @param max Coordinates of the max bounds
        CUDA_FUNCTION
        AABB(VertexType min, VertexType max)
            : minBounds(min), maxBounds(max) {}

        /// @brief Returns an AABB with aMin = inf and aMax = -inf for a = x,y,z.
        CUDA_FUNCTION static
        AABB InfinitelySmall() {
            constexpr auto inf = CuSTD::numeric_limits<VertexType::DataType>::infinity();
            return AABB{ { inf, inf, inf }, { -inf, -inf, -inf } };
        }

        /// @brief Returns the side length on @p axis.
        CUDA_FUNCTION
        cfloat SideLength(Axis3D axis) const {
            return static_cast<cfloat>(maxBounds[ToUnderlying(axis)]) - static_cast<cfloat>(minBounds[ToUnderlying(axis)]);
        }

        /// @brief Returns the side lengths on all axes.
        CUDA_FUNCTION
        Vector3cf SideLengths() const {
            return static_cast<Vector3cf>(maxBounds) - static_cast<Vector3cf>(minBounds);
        }

        /// @brief Expands the current AABB so that it contains @p include.
        /// @param include Bounding box with which to expand the current one
        CUDA_FUNCTION
        void Include(VertexType const& include);

        /// @brief Returns true if @p point is inside this bounding box.
        /// @param point Point to test
        [[nodiscard]]
        CUDA_FUNCTION
        bool Includes(VertexType const& point) const;

        /// @brief Expands the current AABB so that it contains @p include.
        /// @param include Bounding box with which to expand the current one
        CUDA_FUNCTION
        void Include(AABB const& include);

        /// @brief Calculates intersection of @p ray with this bounding box, setting @p tMin and @p tMax if it intersects.
        /// @details Source: https://tavianator.com/2015/ray_box_nan.html. Inconsistent but faster method.
        /// @param ray Ray to intersect with
        /// @param[out] tMin Distance from ray origin to first intersection point
        /// @param[out] tMax Distance from ray origin to second intersection point
        /// @return True if @p ray intersects, false otherwise
        CUDA_FUNCTION
        bool Intersect(Ray const& ray, cfloat& tMin, cfloat& tMax) const;

        /// @brief Calculates intersection of @p ray with this bounding box, setting intersection distance @p t and coordinates @p ic.
        /// @param ray Ray to intersect with
        /// @param[out] t Distance from ray origin to the intersection point
        /// @param[out] t Coordinates on the box where the ray intersected
        /// @return True if @p ray intersects, false otherwise
        CUDA_FUNCTION
        bool Intersect(Ray const& ray, cfloat& t, IntersectionCoordinates& ic) {
            cfloat tMax;
            if (Intersect(ray, t, tMax)) {
                auto point = ray.origin + t * ray.direction;
                auto minOfMin = (point - Vector3cf{ minBounds }).CudaTransform([] (cfloat v) { return CuSTD::abs(v); }).MinElement();
                auto minOfMax = (point - Vector3cf{ maxBounds }).CudaTransform([] (cfloat v) { return CuSTD::abs(v); }).MinElement();
                std::size_t intersectionAxis;
                Vector3cf bounds;
                if (minOfMin.value < minOfMax.value) {
                    intersectionAxis = minOfMin.idx;
                    ic.side = intersectionAxis * 2;
                    bounds = Vector3cf{ minBounds };
                }
                else {
                    intersectionAxis = minOfMax.idx;
                    ic.side = intersectionAxis * 2 + 1;
                    bounds = Vector3cf{ maxBounds };
                }

                std::size_t otherAxis1{ (intersectionAxis + 1) % 3 }, otherAxis2{ (intersectionAxis + 2) % 3 };
                if (otherAxis1 < otherAxis2) {
                    otherAxis1 = 0;
                    otherAxis2 = 2;
                }
                ic.uv.u() = (point[otherAxis1] - bounds[otherAxis1]) / (bounds[otherAxis1] - bounds[otherAxis1]);
                ic.uv.v() = (point[otherAxis2] - bounds[otherAxis2]) / (bounds[otherAxis2] - bounds[otherAxis2]);
                return true;
            }
            else {
                return false;
            }
        }

        /// @brief Itersects this box with @p other and saves the intersection into @p result.
        /// @param other Bounding box to intersect with
        /// @param[out] result Result of the intersection, undefined value when there is no intersection
        /// @return True if this and @p other intersect, false otherwise
        CUDA_FUNCTION
        bool Intersect(AABB other, AABB& result) const;

        /// @brief Calculates if @p other intersects this bounding box.
        /// @param other Other bounding box to intersect with
        /// @return True if @p other intersects, false otherwise
        CUDA_FUNCTION
        bool Intersects(AABB const& other) const;

        /// @brief Returns true if the box is valid geometrically, i.e. if min bounds are smaller than max bounds.
        CUDA_FUNCTION
        bool IsValid() const {
            return (
                minBounds.x() <= maxBounds.x() &&
                minBounds.y() <= maxBounds.y() &&
                minBounds.z() <= maxBounds.z()
            );
        }

        /// @brief Splits this bounding box into @p left and @p right at a specific coordinate based on @p coord.
        /// @param axis Axis in which to split
        /// @param coord Coordinate where to split this bounding box, must be between min and max bounds
        /// @param[out] left Left bounding box after the split
        /// @param[out] right Right bounding box after the split
        /// @throws std::invalid_argument Thrown when @p coord is outside of the min and max bounds
        CUDA_FUNCTION
        void SplitCoord(Axis3D axis, VertexType::DataType coord, AABB& left, AABB& right) const;

        /// @brief Splits this bounding box into @p left and @p right at a coordinate linearly interpolated from min and max bounds based on @p t.
        /// @param axis Axis in which to split
        /// @param t LERP parameter determining where to split this bounding box, must be between 0 and 1
        /// @param[out] left Left bounding box after the split
        /// @param[out] right Right bounding box after the split
        /// @throws std::invalid_argument Thrown when @p t is outside of (0,1)
        CUDA_FUNCTION
        void SplitLERP(Axis3D axis, cfloat t, AABB& left, AABB& right) const;

        /// @brief Computes and returns the surface area of this bounding box.
        CUDA_FUNCTION
        cfloat SurfaceArea() const {
            Vector3cf sides = SideLengths();
            return 2._cf * sides[0] * sides[1] + 2._cf * sides[0] * sides[2] + 2._cf * sides[1] * sides[2];
        }

        /// @brief Expands axes that are near zero with MINIMAL_NON_ZERO_LENGTH.
        CUDA_FUNCTION
        void ExpandToNonZeroVolume() {
            for (int axis = 0; axis < 3; ++axis) {
                if (SideLength(static_cast<Axis3D>(axis)) < ZERO_COMP_EPSILON) {
                    maxBounds[axis] = maxBounds[axis] * (1._sf + cuda::std::copysign(MINIMAL_NON_ZERO_LENGTH, maxBounds[axis])) + MINIMAL_NON_ZERO_LENGTH;
                }
            }
        }

        /// @brief Expands the bounding box in all axes on min and max bounds by values in @p size.
        /// @param size Size to expand by in the x, y and z axes
        /// @return Reference for chaining
        CUDA_FUNCTION
        AABB& ExpandBy(VertexType size) {
            minBounds -= size;
            maxBounds += size;
            return *this;
        }

        /// @brief Expands the bounding box in all axes on min and max bounds using @p epsilon.
        /// @details Multplies min bounds coordinates by 1-epsilon, max bounds by 1+epsilon with correct signs.
        /// @param epsilon Multiplier
        /// @return Reference for chaining
        CUDA_FUNCTION
        AABB& ExpandProportional(VertexType::DataType epsilon) {
            for (int axis = 0; axis < 3; ++axis) {
                minBounds[axis] = minBounds[axis] * (VertexType::DataType{1} - cuda::std::copysign(epsilon, minBounds[axis]));
                maxBounds[axis] = maxBounds[axis] * (VertexType::DataType{1} + cuda::std::copysign(epsilon, maxBounds[axis]));
            }
            return *this;
        }

        /// @brief Returns true if this and @p other are equal (within a small epsilon).
        /// @param other Bounding box to check equality against
        CUDA_FUNCTION
        bool IsTightlyBounding(AABB const& other) {
            VertexType::DataType error{};
            for (int axis = 0; axis < 3; ++axis) {
                error += CuSTD::abs(minBounds[axis] - other.minBounds[axis]);
                error += CuSTD::abs(maxBounds[axis] - other.maxBounds[axis]);
            }

            return (error < static_cast<float>(ZERO_COMP_EPSILON));
        }

        /// @brief Returns true if this and @p primitive's bounding box are equal (within a small epsilon).
        /// @tparam Primitive type, must have GetAABB method that returns its AABB
        /// @param primitive Checks if this bounding box is tightly bounding this primitive
        template<class TPrimitive>
            requires requires (TPrimitive p) { { p.GetAABB() } -> std::convertible_to<AABB const&>; }
        CUDA_FUNCTION
        bool IsTightlyBounding(TPrimitive const& primitive) {
            return IsTightlyBounding(primitive.GetAABB());
        }

        /// @brief Member pointers for helping indexed access.
        inline static
        VertexType AABB::* const bounds[2] { &AABB::minBounds, &AABB::maxBounds };

        /// @brief Enum for indexing bounds.
        enum class BoundsEnum {
            MinBounds = 0, MaxBounds
        };

        /// @brief Returns min or max bounds based on indexing enum.
        CUDA_FUNCTION
        VertexType& GetBounds(BoundsEnum bound) {
            #ifdef __CUDA_ARCH__

            switch (bound)
            {
            case BoundsEnum::MinBounds:
                return minBounds;
            case BoundsEnum::MaxBounds:
            default:
                return maxBounds;
            }
            
            #else
            return (this->*bounds[ToUnderlying(bound)]);
            #endif
        }

        /// @brief Returns min or max bounds based on indexing enum.
        CUDA_FUNCTION
        VertexType const& GetBounds(BoundsEnum bound) const {
            #ifdef __CUDA_ARCH__
            
            switch (bound)
            {
            case BoundsEnum::MinBounds:
                return minBounds;
            case BoundsEnum::MaxBounds:
            default:
                return maxBounds;
            }
            
            #else
            return (this->*bounds[ToUnderlying(bound)]);
            #endif
        }
    };
}
