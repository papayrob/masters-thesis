/**
 * File name: Ray.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2022 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "Vector.hpp"

namespace ProperRT
{
	/// @brief Struct for representing rays.
	struct Ray
	{
		/// @brief Ray origin
		Vector3cf origin;
		/// @brief Ray direction, should awlays be normalized
		Vector3cf direction;
	};
}
