/**
 * File name: TriangleMesh.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <vector>
#include "glbinding/gl/types.h"

#include "Mesh.hpp"
#include "Triangle.hpp"
#include "Graphics/Color.hpp"

namespace ProperRT
{
    /// @brief Class for representing triangle meshes.
    class TriangleMesh : public Mesh
    {
    public:
        /// @brief Default ctor.
        TriangleMesh() noexcept;

        virtual
        ~TriangleMesh();

        TriangleMesh(TriangleMesh const&) = delete;
        TriangleMesh& operator=(TriangleMesh const&) = delete;

        TriangleMesh(TriangleMesh&&);
        TriangleMesh& operator=(TriangleMesh&&);

        /// @brief Sets the triangle array.
        /// @details Cannot set vertices after allocating resources on the GPU.
        /// To change values use the @ref Triangles() method, or release GPU resources
        /// to make this method available again.
        void SetTriangles(std::vector<Triangle> newValue) {
            if (VAO != 0) {
                throw std::runtime_error("Cannot modify mesh after creating GPU resources");
            }
            triangles = std::move(newValue);
        }

        /// @brief Sets the mesh material.
        /// @param newValue New material
        void SetMaterial(Material const& newValue) {
            if (materials.empty()) {
                materials.push_back(newValue);
            }
            else {
                materials[0] = newValue;
            }
        }

        /// @brief Mesh triangles.
        std::span<Triangle> Triangles() { return triangles; }

        /// @brief Mesh triangles.
        std::vector<Triangle> const& Triangles() const { return triangles; }

        virtual
        Primitive GetPrimitive(int32 submeshIdx, int32 primitiveIdx) const override;

        virtual
        Primitive GetPrimitive(int32 submeshIdx, int32 primitiveIdx, Matrix4x4cf const& transform) const override;

        virtual
        int32 GetPrimitiveCount(int32 submeshIdx) const noexcept override { return StdSize<int32>(triangles); }

        virtual
        std::vector<int32> GetPrimitiveCounts() const noexcept override { return std::vector<int32>{ StdSize<int32>(triangles) }; }

        virtual
        void FillSubmeshInstance(GPUMeshManagement::Instance& instance, int32 submeshIdx) override;

        virtual
        void SyncToGPU() override;

        virtual
        void ReleaseCPUResources() override;

        virtual
        void ReleaseGPUResources() override;

        virtual
        void GLBind() const override;

        virtual
        void Rasterize(Matrix4x4f const& modelMat, RasterizationMatrices const& matrices, bool bind = true) override;

        virtual
        void Rasterize(Matrix4x4f const& modelMat, RasterizationMatrices const& matrices, std::span<int32 const> submeshIndeces, bool bind = true) override;

    private:
        // Pimpl
        struct ComputeStorage;

    private:
        std::vector<Triangle> triangles;

        gl::GLuint VAO{};

        gl::GLuint vertexBuffer{};

        std::unique_ptr<ComputeStorage> computeStorage;
    };
}
