/**
 * File name: Vector.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2022 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <array>
#include <concepts>
#include <limits>
#include <iostream>

#include <cuda/std/array>
#include <cuda/std/concepts>
#include <cuda/std/limits>
#include <cuda/std/cmath>

#include "imgui.h"

#include "Utilities/Utilities.hpp"
#include "Utilities/MathException.hpp"

namespace ProperRT
{
    /**********************************************************
    ***********************************************************
    ******************** General vector ***********************
    ***********************************************************
    **********************************************************/

	/// @brief Class for representing n-dimensional vectors with a static array as storage.
    /// @tparam NDim Dimension of the vector
	/// @tparam TData Data type of vector elements
	template<int NDim, class TData>
	struct Vector
	{
    public:
        template<int NDimOther, class TDataOther>
        friend struct Vector;

        inline static constexpr
        int DIMENSION = NDim;
        
        using DataType = TData;

        struct Element
        {
            TData value;
            int idx;
        };

    public:
        TData elements[NDim];

        /**********************************************************
        ***********************************************************
        ********************** Constructors ***********************
        ***********************************************************
        **********************************************************/
    public:

        /// @brief Default ctor, intializes to zero.
        CUDA_FUNCTION constexpr
        Vector() {
            for (int i = 0; i < NDim; ++i) {
                elements[i] = TData{};
            }
        };

        /// @brief Copy ctor.
        /// @param other Vector to copy from
        CUDA_FUNCTION constexpr
        Vector(Vector const& other) {
            for (int i = 0; i < NDim; ++i) {
                elements[i] = other.elements[i];
            }
        }

        /// @brief Copies values while converting from @p TDataOther to @p TData.
        /// @tparam NDimOther Dimension of the other vector
        /// @tparam TDataOther Data type of the other vector
        /// @param other Vector to convert from
        template<class TDataOther>
        CUDA_FUNCTION explicit constexpr
        Vector(Vector<NDim, TDataOther> const& other) {
            // Copy data
            for (int i = 0; i < NDim; ++i) {
                elements[i] = static_cast<TData>(other.elements[i]);
            }
        }

        /// @brief Convert ctor, copies min(NDim, NDimOther) elements from @p other to this and initializes rest to zero.
        /// @tparam NDimOther Dimension of the other vector
        /// @tparam TDataOther Data type of the other vector
        /// @param other Vector to convert from
        template<int NDimOther, class TDataOther>
        CUDA_FUNCTION explicit constexpr
        Vector(Vector<NDimOther, TDataOther> const& other) {
            // Copy data
            constexpr auto minDim = (NDim < NDimOther ? NDim : NDimOther);
            for (int i = 0; i < minDim; ++i) {
                elements[i] = static_cast<TData>(other.elements[i]);
            }

            // Initialize rest to default
            if constexpr (NDimOther < NDim) {
                for (int i = NDimOther; i < NDim; ++i) {
                    elements[i] = TData{};
                }
            }
        }

        template<class TFirst, class TSecond, class... TRest>
            requires (NDim > 1 && AllSameAsFirst<TData, TFirst, TSecond, TRest...>)
        CUDA_FUNCTION constexpr
        Vector(TFirst first, TSecond second, TRest... rest) {
            elements[0] = first;
            elements[1] = second;
            int i = 2;
            ((elements[i++] = rest), ...);
        }

        /// @brief Constructs this vector from lower dimension vectors and scalars, e.g. Vector4 from float, Vector2, float.
        /// @details The sum of dimensions of component vectors must match the dimension of this vector. The allowed
        /// types are only @p TData and @p Vector<N,TData>.
        template<class TFirst, class TSecond, class... TRest>
            requires (NDim > 1 && !AllSameAsFirst<TData, TFirst, TSecond, TRest...>)
        CUDA_FUNCTION constexpr
        Vector(TFirst const& first, TSecond const& second, TRest const&... rest) {
            //constexpr bool correctDimSum = (DimensionManager<TFirst, TSecond, TRest...>::SumDimensions() == NDim);
            constexpr bool correctDimSum = (SumDimensions<TFirst, TSecond, TRest...>() == NDim);
            static_assert(correctDimSum, "Dimensions of vector constructor elements must add up to the vector dimension");

            if constexpr (correctDimSum) {
                //RecursiveInit(elements.begin(), first, second, rest...);
                SequentialInit(first, second, rest...);
            }
        }

        /// @brief Copy ctor from std::array.
        /// @param src Source array to copy from
        CUDA_FUNCTION constexpr
        Vector(CuSTD::array<TData, NDim> const& src) {
            for (int i = 0; i < NDim; ++i) {
                elements[i] = src[i];
            }
        }

        /// @brief Copy ctor from C array.
        /// @param src Source array to copy from
        CUDA_FUNCTION constexpr
        Vector(const TData(&src)[NDim]) {
            for (int i = 0; i < NDim; ++i) {
                elements[i] = src[i];
            }
        }

        /// @brief Init ctor, initializes all elements to @p init.
        /// @param init Element to be initialized with
        CUDA_FUNCTION constexpr
        Vector(TData const& init) {
            for (int i = 0; i < NDim; ++i) {
                elements[i] = init;
            }
        }

        operator ImVec2() const requires (NDim == 2) { return ImVec2{ static_cast<float>(x()), static_cast<float>(y()) }; }

        Vector(ImVec2 const& vec) requires (NDim == 2) : Vector(static_cast<TData>(vec.x), static_cast<TData>(vec.y)) {}

        /**********************************************************
        ***********************************************************
        ******************* Data accessors ************************
        ***********************************************************
        **********************************************************/

        /// @brief Returns reference to coordinate at index @p i.
        /// @param i Index of the desired coordinate
        /// @return Reference to coordinate at index @p i
        CUDA_FUNCTION constexpr
        TData& operator[](auto i) {
            return elements[i];
        }

        /// @brief Returns const reference to coordinate at index @p i.
        /// @param i Index of the desired coordinate
        /// @return Const reference to coordinate at index @p i
        CUDA_FUNCTION constexpr
        TData const& operator[](auto i) const {
            return elements[i];
        }

        /// @brief Returns reference to coordinate at index @p I with compile time bounds checking.
        template<int I>
        CUDA_FUNCTION constexpr
        TData& At() {
            static_assert(I < NDim, "Index must be a positive integer");
            return elements[I];
        }

        /// @brief Returns const reference to coordinate at index @p I with compile time bounds checking.
        template<int I>
        CUDA_FUNCTION constexpr
        TData const& At() const {
            static_assert(I < NDim, "Index must be a positive integer");
            return elements[I];
        }

        /// @brief Returns reference to first coordinate.
        /// @return Reference to first coordinate
        CUDA_FUNCTION constexpr
        TData& x() { return elements[0]; }

        /// @brief Returns const reference to first coordinate.
        /// @return Const reference to first coordinate
        CUDA_FUNCTION constexpr
        TData const& x() const { return elements[0]; }

        /// @brief Returns reference to second coordinate.
        /// @return Reference to second coordinate
        CUDA_FUNCTION constexpr
        TData& y() requires (NDim > 1) { return elements[1]; }

        /// @brief Returns const reference to second coordinate.
        /// @return Const reference to second coordinate
        CUDA_FUNCTION constexpr
        TData const& y() const requires (NDim > 1) { return elements[1]; }

        /// @brief Returns reference to third coordinate.
        /// @return Reference to third coordinate
        CUDA_FUNCTION constexpr
        TData& z() requires (NDim > 2) { return elements[2]; }

        /// @brief Returns const reference to third coordinate.
        /// @return Const reference to third coordinate
        CUDA_FUNCTION constexpr
        TData const& z() const requires (NDim > 2) { return elements[2]; }

        /// @brief Returns reference to fourth coordinate.
        /// @return Reference to fourth coordinate
        CUDA_FUNCTION constexpr
        TData& w() requires (NDim > 3) { return elements[3]; }

        /// @brief Returns reference to first coordinate.
        /// @return Reference to first coordinate
        CUDA_FUNCTION constexpr
        TData& u() { return elements[0]; }

        /// @brief Returns const reference to first coordinate.
        /// @return Const reference to first coordinate
        CUDA_FUNCTION constexpr
        TData const& u() const { return elements[0]; }

        /// @brief Returns reference to second coordinate.
        /// @return Reference to second coordinate
        CUDA_FUNCTION constexpr
        TData& v() requires (NDim > 1) { return elements[1]; }

        /// @brief Returns const reference to second coordinate.
        /// @return Const reference to second coordinate
        CUDA_FUNCTION constexpr
        TData const& v() const requires (NDim > 1) { return elements[1]; }

        /// @brief Returns const reference to fourth coordinate.
        /// @return Const reference to fourth coordinate
        CUDA_FUNCTION constexpr
        TData const& w() const requires (NDim > 3) { return elements[3]; }

        /// @brief Returns reference to first element.
        /// @return Reference to first element
        CUDA_FUNCTION constexpr
        TData& r() { return elements[0]; }

        /// @brief Returns const reference to first element.
        /// @return Const reference to first element
        CUDA_FUNCTION constexpr
        TData const& r() const { return elements[0]; }

        /// @brief Returns reference to second element.
        /// @return Reference to second element
        CUDA_FUNCTION constexpr
        TData& g() requires (NDim > 1) { return elements[1]; }

        /// @brief Returns const reference to second element.
        /// @return Const reference to second element
        CUDA_FUNCTION constexpr
        TData const& g() const requires (NDim > 1) { return elements[1]; }

        /// @brief Returns reference to third element.
        /// @return Reference to third element
        CUDA_FUNCTION constexpr
        TData& b() requires (NDim > 2) { return elements[2]; }

        /// @brief Returns const reference to third element.
        /// @return Const reference to third element
        CUDA_FUNCTION constexpr
        TData const& b() const requires (NDim > 2) { return elements[2]; }

        /// @brief Returns reference to fourth element.
        /// @return Reference to fourth element
        CUDA_FUNCTION constexpr
        TData& a() requires (NDim > 3) { return elements[3]; }

        /// @brief Returns const reference to fourth element.
        /// @return Const reference to fourth element
        CUDA_FUNCTION constexpr
        TData const& a() const requires (NDim > 3) { return elements[3]; }

        /**********************************************************
        ***********************************************************
        ******************* General operators *********************
        ***********************************************************
        **********************************************************/

        friend
        std::ostream& operator<<(std::ostream& ostream, Vector const& v) {
            // Vector always has at least one element
            ostream << '(' << v[0];
            for (int i = 1; i < NDim; ++i) {
                ostream << ", " << v[i];
            }
            ostream << ')';
            return ostream;
        }

        /**********************************************************
        ***********************************************************
        ***************** Comparison operators ********************
        ***********************************************************
        **********************************************************/

        /// @brief Comparison op., compares elements.
        friend CUDA_FUNCTION constexpr
        bool operator==(Vector const& lhs, Vector const& rhs) {
            return [&lhs, &rhs]<int... I>(std::integer_sequence<int, I...>) {
                return ((lhs.elements[I] == rhs.elements[I]) && ...);
            }(std::make_integer_sequence<int, Vector::DIMENSION>{});
        }

        /// @brief Comparison op., compares elements.
        friend CUDA_FUNCTION constexpr
        Vector<NDim, bool> operator<(Vector const& lhs, Vector const& rhs) {
            Vector<NDim, bool> res;
            for (int i = 0; i < NDim; ++i) {
                res.elements[i] = (lhs.elements[i] < rhs.elements[i]);
            }
            return res;
        }

        /// @brief Comparison op., compares elements.
        friend CUDA_FUNCTION constexpr
        Vector<NDim, bool> operator<=(Vector const& lhs, Vector const& rhs) {
            Vector<NDim, bool> res;
            for (int i = 0; i < NDim; ++i) {
                res.elements[i] = (lhs.elements[i] <= rhs.elements[i]);
            }
            return res;
        }

        /// @brief Comparison op., compares elements.
        friend CUDA_FUNCTION constexpr
        Vector<NDim, bool> operator>(Vector const& lhs, Vector const& rhs) {
            Vector<NDim, bool> res;
            for (int i = 0; i < NDim; ++i) {
                res.elements[i] = (lhs.elements[i] > rhs.elements[i]);
            }
            return res;
        }

        /// @brief Comparison op., compares elements.
        friend CUDA_FUNCTION constexpr
        Vector<NDim, bool> operator>=(Vector const& lhs, Vector const& rhs) {
            Vector<NDim, bool> res;
            for (int i = 0; i < NDim; ++i) {
                res.elements[i] = (lhs.elements[i] >= rhs.elements[i]);
            }
            return res;
        }

        /**********************************************************
        ***********************************************************
        ********************* Math operators **********************
        ***********************************************************
        **********************************************************/

        /// @brief Adds vectors @p lhs and @p rhs.
        /// @param lhs Input vector
        /// @param rhs Input vector
        /// @return Resulting vector
        friend CUDA_FUNCTION constexpr
        Vector operator+(Vector lhs, Vector const& rhs) {
            lhs += rhs;
            return lhs;
        }

        /// @brief Adds vector @p other to this vector.
        /// @param other Vector to be added
        /// @return Reference to this for chaining
        CUDA_FUNCTION constexpr
        Vector& operator+=(Vector const& other) {
            for (int i = 0; i < NDim; ++i) {
                elements[i] += other.elements[i];
            }
            return *this;
        }

        /// @brief Adds @p rhs to all elements of @p lhs.
        /// @param lhs Input vector
        /// @param rhs Input scalar
        /// @return Resulting vector
        friend CUDA_FUNCTION constexpr
        Vector operator+(Vector lhs, TData rhs) {
            lhs += rhs;
            return lhs;
        }

        /// @brief Adds @p other to all elements of this vector.
        /// @param other Input scalar
        /// @return Reference for chaining
        CUDA_FUNCTION constexpr
        Vector& operator+=(TData other) {
            for (int i = 0; i < NDim; ++i) {
                elements[i] += other;
            }
            return *this;
        }

        /// @brief Subtracts vector @p rhs from @p lhs.
        /// @param lhs Input vector
        /// @param rhs Input vector
        /// @return Resulting vector
        friend CUDA_FUNCTION constexpr
        Vector operator-(Vector lhs, Vector const& rhs) {
            lhs -= rhs;
            return lhs;
        }

        /// @brief Subtracts vector @p other from this vector.
        /// @param other Vector to be subtracted
        /// @return Reference to this for chaining
        CUDA_FUNCTION constexpr
        Vector& operator-=(Vector const& other) {
            for (int i = 0; i < NDim; ++i) {
                elements[i] -= other.elements[i];
            }
            return *this;
        }

        /// @brief Subtracts @p rhs from all elements of @p lhs.
        /// @param lhs Input vector
        /// @param rhs Input scalar
        /// @return Resulting vector
        friend CUDA_FUNCTION constexpr
        Vector operator-(Vector lhs, TData rhs) {
            lhs -= rhs;
            return lhs;
        }

        /// @brief Subtracts @p other from all elements of this vector.
        /// @param other Input scalar
        /// @return Reference for chaining
        CUDA_FUNCTION constexpr
        Vector& operator-=(TData other) {
            for (int i = 0; i < NDim; ++i) {
                elements[i] -= other;
            }
            return *this;
        }

        /// @brief Returns vector with negated elements.
        /// @return Vector with negated elements
        CUDA_FUNCTION constexpr
        Vector operator-() const {
            Vector res;
            for (int i = 0; i < NDim; ++i) {
                res.elements[i] = -elements[i];
            }
            return res;
        }

        /// @brief Multiplies vector @p v by scalar @p s.
        /// @param s Input scalar
        /// @param v Input vector
        /// @return Resulting vector
        friend CUDA_FUNCTION constexpr
        Vector operator*(TData const& s, Vector v) {
            v *= s;
            return v;
        }

        /// @brief Multiplies vector @p v by scalar @p s.
        /// @param v Input vector
        /// @param s Input scalar
        /// @return Resulting vector
        friend CUDA_FUNCTION constexpr
        Vector operator*(Vector v, TData const& s) {
            v *= s;
            return v;
        }

        /// @brief Multiplies this vector by scalar @p s.
        /// @param s Input scalar
        /// @return Reference to this for chaining
        CUDA_FUNCTION constexpr
        Vector& operator*=(TData const& s) {
            for (int i = 0; i < NDim; ++i) {
                elements[i] *= s;
            }
            return *this;
        }

        /// @brief Divides elements of vector @p v by scalar @p s.
        /// @param v Input vector
        /// @param s Input scalar
        /// @return Resulting vector
        friend CUDA_FUNCTION constexpr
        Vector operator/(Vector v, TData const& s) {
            v /= s;
            return v;
        }

        /// @brief Divides scalar @p s by elements of vector @p v.
        /// @param s Input scalar
        /// @param v Input vector
        /// @return Resulting vector
        friend CUDA_FUNCTION constexpr
        Vector operator/(TData const& s, Vector v) {
            for (int i = 0; i < Vector::DIMENSION; ++i) {
                v.elements[i] = s / v.elements[i];
            }
            return v;
        }

        /// @brief Multiplies this vector by scalar @p s.
        /// @param s Input scalar
        /// @return Reference to this for chaining
        CUDA_FUNCTION constexpr
        Vector& operator/=(TData const& s) {
            if constexpr (std::is_floating_point_v<TData>) {
                TData oneOverS = TData{ 1 } / s;
                return operator*=(oneOverS);
            }
            else {
                for (int i = 0; i < NDim; ++i) {
                    elements[i] /= s;
                }
                return *this;
            }
        }

        /// @brief Multiplies elements of vector @p rhs by elements of vector @p lhs.
        /// @param lhs Input vector
        /// @param rhs Input vector
        /// @return Resulting vector
        friend CUDA_FUNCTION constexpr
        Vector operator*(Vector lhs, Vector const& rhs) {
            lhs *= rhs;
            return lhs;
        }

        /// @brief Multiplies elements of this vector by elements of vector @p other.
        /// @param other Input vector
        /// @return Reference to this for chaining
        CUDA_FUNCTION constexpr
        Vector& operator*=(Vector const& other) {
            for (int i = 0; i < NDim; ++i) {
                elements[i] *= other.elements[i];
            }
            return *this;
        }

        /// @brief Divides elements of vector @p rhs by elements from vector @p lhs.
        /// @param lhs Input vector
        /// @param rhs Input vector
        /// @return Resulting vector
        friend CUDA_FUNCTION constexpr
        Vector operator/(Vector lhs, Vector const& rhs) {
            lhs /= rhs;
            return lhs;
        }

        /// @brief Divides elements of this vector by elements of vector @p other.
        /// @param other Input vector
        /// @return Reference to this for chaining
        CUDA_FUNCTION constexpr
        Vector& operator/=(Vector const& other) {
            for (int i = 0; i < NDim; ++i) {
                elements[i] /= other.elements[i];
            }
            return *this;
        }

        /**********************************************************
        ***********************************************************
        ******************* More vector math **********************
        ***********************************************************
        **********************************************************/

        /// @brief Computes the dot product of vectors @p lhs and @p rhs and returns it.
        /// @param lhs Input vector
        /// @param rhs Input vector
        /// @return Dot product of the input vectors
        static CUDA_FUNCTION constexpr
        TData Dot(Vector const& lhs, Vector const& rhs) {
            return (lhs * rhs).ElementSum();
        }

        /// @brief Computes the dot product of this vector and @p other and returns it.
        /// @param other Input vector
        /// @return Dot product of the input vectors
        CUDA_FUNCTION constexpr
        TData Dot(Vector const& other) {
            return Dot(*this, other);
        }

        /// @brief Computes the magnitude of this vector and returns it.
        /// @tparam TPrecision Precision of the result, main use intended for integer vectors
        /// @return Magnitude of this vector
        template<std::floating_point TPrecision = TData>
        CUDA_FUNCTION
        TPrecision Magnitude() const {
            return CuSTD::sqrt(static_cast<TPrecision>(Vector::Dot(*this, *this)));
        }

        /// @copydoc Vector::Magnitude()
        template<std::floating_point TPrecision = TData>
        CUDA_FUNCTION
        TPrecision Norm() const {
            return Magnitude<TPrecision>();
        }

        
        /// @brief Computes the p-norm of this vector and returns it.
        /// @details Computed value is the result of a p-th root of the sum of absolute value of elements to the p-th power.
        /// See method definition for a latex comment describing the equation. Works with p = infinity (max-norm).
        /// @tparam P Determines the power and root used by the norm
        /// @tparam TPrecision Precision of the result, main use intended for integer vectors
        /// @return p-norm of this vector
        template<double P, std::floating_point TPrecision = TData>
            requires (P >= 1.0)
        TPrecision pNorm() const {
            //tex:
            //$$\sqrt[\leftroot{2} \uproot{0} p]{\sum\nolimits_{i}^{NDim} \left | x_i \right | ^p \vphantom{i}} $$
            
            // Infinity norm
            if constexpr (P == std::numeric_limits<decltype(P)>::infinity()) {
                TPrecision max{ 0 };
                Unroll<NDim>(
                    [&](auto i) {
                        max = std::max(max, std::abs(static_cast<TPrecision>(elements[i])));
                    }
                );
                return max;
            }
            // Normal p-norm
            else {
                TPrecision sum{ 0 };
                TPrecision p{ P };
                Unroll<NDim>(
                    [&](auto i) {
                        sum += std::pow(std::abs(static_cast<TPrecision>(elements[i])), p);
                    }
                );
                return std::pow(sum, TPrecision{ 1 } / p);
            }
        }

        /// @brief Returns normalized version of this vector.
        /// @return Normalized version of this vector
        CUDA_FUNCTION
        Vector Normalized() const
            requires std::is_floating_point_v<TData>
        {
            Vector res = *this;
            res.Normalize();
            return res;
        }

        /// @brief Normalizes this vector.
        CUDA_FUNCTION
        void Normalize()
            requires std::is_floating_point_v<TData>
        {
            *this /= Magnitude();
        }

        /// @brief Returns normalized version of this vector, or zero vector if the norm is zero.
        /// @return Normalized version of this vector
        CUDA_FUNCTION
        Vector NormalizedSafe() const
            requires std::is_floating_point_v<TData>
        {
            auto mag = this->Magnitude();
            if (mag <= 0.0000001) {
                return Vector(0.0);
            }
            
            return (*this) / mag;
        }

        /// @brief Normalizes this vector, or stays a zero vector if the norm is zero.
        CUDA_FUNCTION
        void NormalizeSafe()
            requires std::is_floating_point_v<TData>
        {
            *this = this->NormalizedSafe();
        }

        /// @brief Returns the sum of all elements.
        /// @return Sum of all elements
        CUDA_FUNCTION constexpr
        TData ElementSum() const {
            return [this]<int... I>(std::integer_sequence<int, I...>) {
                return (elements[I] + ...);
            }(std::make_integer_sequence<int, NDim>{});
        }

        /// @brief Returns the product of all elements.
        /// @return Product of all elements
        CUDA_FUNCTION constexpr
        TData ElementProduct() const {
            return [this]<int... I>(std::integer_sequence<int, I...>) {
                return (elements[I] * ...);
            }(std::make_integer_sequence<int, NDim>{});
        }

        /// @brief Returns the max of all elements.
        CUDA_FUNCTION constexpr
        TData ElementMax() const {
            TData max{ elements[0] };
            for (int i = 1; i < NDim; ++i) {
                max = max > elements[i] ? max : elements[i];
            }
            return max;
        }

        /// @brief Returns the min of all elements.
        CUDA_FUNCTION constexpr
        TData ElementMin() const {
            TData min{ elements[0] };
            for (int i = 1; i < NDim; ++i) {
                min = elements[i] < min ? elements[i] : min;
            }
            return min;
        }

        /// @brief Returns the max of all elements with its index.
        CUDA_FUNCTION constexpr
        Element MaxElement() const {
            Element max{ elements[0], 0 };
            for (int i = 1; i < NDim; ++i) {
                if (elements[i] > max.value) {
                    max.value = elements[i];
                    max.idx = i;
                }
            }
            return max;
        }

        /// @brief Returns the min of all elements with its index.
        CUDA_FUNCTION constexpr
        Element MinElement() const {
            Element min{ elements[0], 0 };
            for (int i = 1; i < NDim; ++i) {
                if (elements[i] < min.value) {
                    min.value = elements[i];
                    min.idx = i;
                }
            }
            return min;
        }

        /// @brief Returns the per element max of @p lhs and @p rhs.
        /// @param lhs Input vector
        /// @param rhs Input vector
        CUDA_FUNCTION static constexpr
        Vector Max(Vector lhs, Vector const& rhs) {
            for (int i = 0; i < Vector::DIMENSION; ++i) {
                lhs.elements[i] = lhs.elements[i] > rhs.elements[i] ? lhs.elements[i] : rhs.elements[i];
            }
            return lhs;
        }

        /// @brief Returns the per element min of @p lhs and @p rhs.
        /// @param lhs Input vector
        /// @param rhs Input vector
        CUDA_FUNCTION static constexpr
        Vector Min(Vector lhs, Vector const& rhs) {
            for (int i = 0; i < Vector::DIMENSION; ++i) {
                lhs.elements[i] = rhs.elements[i] < lhs.elements[i] ? rhs.elements[i] : lhs.elements[i];
            }
            return lhs;
        }

        /// @brief Returns the per element max of this and @p rhs.
        /// @param other Input vector
        CUDA_FUNCTION constexpr
        Vector Max(Vector other) const {
            return Max(other, *this);
        }

        /// @brief Returns the per element min of this and @p rhs.
        /// @param other Input vector
        CUDA_FUNCTION constexpr
        Vector Min(Vector other) const {
            return Min(other, *this);
        }

        /// @brief Returns true if all elements, converted to bool, are true.
        CUDA_FUNCTION constexpr
        bool All() const
            requires (std::is_convertible_v<TData, bool>)
        {
            return [this]<int... I>(std::integer_sequence<int, I...>) {
                return (static_cast<bool>(elements[I]) && ...);
            }(std::make_integer_sequence<int, NDim>{});
        }

        /// @brief Returns true if any element, converted to bool, is true.
        CUDA_FUNCTION constexpr
        bool Any() const
            requires (std::is_convertible_v<TData, bool>)
        {
            return [this]<int... I>(std::integer_sequence<int, I...>) {
                return (static_cast<bool>(elements[I]) || ...);
            }(std::make_integer_sequence<int, NDim>{});
        }

        /// @brief Floors all elements of this vector.
        CUDA_FUNCTION
        Vector& Floor() {
            for (int i = 0; i < NDim; ++i) {
                elements[i] = CuSTD::floor(elements[i]);
            }
            return *this;
        }

        /// @brief Returns a copy of this vector with all elements floored.
        CUDA_FUNCTION
        Vector Floored() const {
            Vector res = *this;
            res.Floor();
            return res;
        }

        /// @brief Floors all elements of this vector.
        CUDA_FUNCTION
        Vector& Ceil() {
            for (int i = 0; i < NDim; ++i) {
                elements[i] = CuSTD::ceil(elements[i]);
            }
            return *this;
        }

        /// @brief Returns a copy of this vector with all elements floored.
        CUDA_FUNCTION
        Vector Ceiled() const {
            Vector res = *this;
            res.Ceil();
            return res;
        }

        /// @brief Clamps all elements of this vector betweem @p minValue and @p maxValue.
        /// @param minValue Left clamp value
        /// @param maxValue Right clamp value
        CUDA_FUNCTION constexpr
        Vector& Clamp(TData const& minValue, TData const& maxValue) {
            for (int i = 0; i < NDim; ++i) {
                elements[i] = (elements[i] < minValue ? minValue : (elements[i] > maxValue ? maxValue : elements[i]));
            }
            return *this;
        }

        /// @brief Returns a copy of this vector with all elements clamped betweem @p minValue and @p maxValue.
        /// @param minValue Left clamp value
        /// @param maxValue Right clamp value
        CUDA_FUNCTION constexpr
        Vector Clamped(TData const& minValue, TData const& maxValue) const {
            Vector res;
            for (int i = 0; i < NDim; ++i) {
                res.elements[i] = (elements[i] < minValue ? minValue : (elements[i] > maxValue ? maxValue : elements[i]));
            }
            return res;
        }

        /// @brief Linearly interpolates between vectors @p a and @p b using @p alpha.
        /// @param a First vector
        /// @param b Second vector
        /// @param alpha Interpolant
        /// @return (1 - alpha) * a + alpha * b
        CUDA_FUNCTION static constexpr
        Vector LERP(Vector const& a, Vector const& b, TData alpha)
            requires std::is_floating_point_v<TData>
        {
            return (TData{ 1 } - alpha) * a + alpha * b;
        }

        /// @copydoc LERP(Vector const&, Vector const&, TData)
        CUDA_FUNCTION static constexpr
        Vector Mix(Vector const& a, Vector const& b, TData alpha) {
            return LERP(a, b, alpha);
        }

        /// @brief Computes the cross product of input vectors.
        /// @tparam N Size of the input array, must be equal to NDim - 1
        /// @param vectors Input vectors
        /// @return Cross product of input vectors
        template<int N>
            requires (N == (NDim - 1))
        CUDA_FUNCTION static constexpr
        Vector Cross(const Vector(&vectors)[N]) {
            if constexpr (NDim == 2) {
                Vector const& v0 = vectors[0];
                return Vector{ -v0.elements[1], v0.elements[0] };
            }
            else if constexpr (NDim == 3) {
                Vector const& v0 = vectors[0];
                Vector const& v1 = vectors[1];
                return Vector{
                    v0.elements[1] * v1.elements[2] - v0.elements[2] * v1.elements[1],
                    v0.elements[2] * v1.elements[0] - v0.elements[0] * v1.elements[2],
                    v0.elements[0] * v1.elements[1] - v0.elements[1] * v1.elements[0]
                };
            }
            else if constexpr (NDim == 4) {
                Vector const& v0 = vectors[0];
                Vector const& v1 = vectors[1];
                Vector const& v2 = vectors[2];
                return Vector{
                    v0.elements[1] * v1.elements[2] * v2.elements[3] + v1.elements[1] * v2.elements[2] * v0.elements[3] + v2.elements[1] * v0.elements[2] * v1.elements[3]
                    - v2.elements[1] * v1.elements[2] * v0.elements[3] - v1.elements[1] * v0.elements[2] * v2.elements[3] - v0.elements[1] * v2.elements[2] * v1.elements[3],

                    -v0.elements[0] * v1.elements[2] * v2.elements[3] - v1.elements[0] * v2.elements[2] * v0.elements[3] - v2.elements[0] * v0.elements[2] * v1.elements[3]
                    + v2.elements[0] * v1.elements[2] * v0.elements[3] + v1.elements[0] * v0.elements[2] * v2.elements[3] + v0.elements[0] * v2.elements[2] * v1.elements[3],

                    v0.elements[0] * v1.elements[1] * v2.elements[3] + v1.elements[0] * v2.elements[1] * v0.elements[3] + v2.elements[0] * v0.elements[1] * v1.elements[3]
                    - v2.elements[0] * v1.elements[1] * v0.elements[3] - v1.elements[0] * v0.elements[1] * v2.elements[3] - v0.elements[0] * v2.elements[1] * v1.elements[3],

                    -v0.elements[0] * v1.elements[1] * v2.elements[2] - v1.elements[0] * v2.elements[1] * v0.elements[2] - v2.elements[0] * v0.elements[1] * v1.elements[2]
                    + v2.elements[0] * v1.elements[1] * v0.elements[2] + v1.elements[0] * v0.elements[1] * v2.elements[2] + v0.elements[0] * v2.elements[1] * v1.elements[2]
                };
            }
            else {
                #ifndef __CUDA_ARCH__
                throw std::runtime_error("General dimension cross product unimplemented");
                #endif
            }
        }

        /// @brief Computes the reflection vector of @p direction according to @p normal.
        /// @param direction In direction to reflect
        /// @param normal Normal of the surface
        CUDA_FUNCTION static constexpr
        Vector Reflect(Vector direction, Vector normal) {
            return TData{ 2 } * Dot(direction, normal) * normal - direction;
        }

        /**********************************************************
        ***********************************************************
        ************************ Utility **************************
        ***********************************************************
        **********************************************************/

        /// @brief Transforms all elements according to @p transformer and returns the result.
        /// @tparam F Transformer functor type
        /// @param transformer Transformer functor instance
        template<class F>
        auto Transform(F&& transformer) const -> Vector<NDim, std::invoke_result_t<F, TData>> {
            using TTarget = std::invoke_result_t<F, TData>;
            Vector<NDim, TTarget> transformed;
            for (int i = 0; i < NDim; ++i) {
                transformed.elements[i] = transformer(elements[i]);
            }
            return transformed;
        }

        /// @brief Transforms all elements according to @p transformer and returns the result.
        /// @tparam F Transformer functor type
        /// @param transformer Transformer functor instance
        template<class F>
        CUDA_FUNCTION
        auto CudaTransform(F&& transformer) const -> Vector<NDim, std::invoke_result_t<F, TData>> {
            using TTarget = std::invoke_result_t<F, TData>;
            Vector<NDim, TTarget> transformed;
            for (int i = 0; i < NDim; ++i) {
                transformed.elements[i] = transformer(transformed.elements[i]);
            }
            return transformed;
        }

        /// @brief Returns a vector with NaN in all its elements.
        /// @return Vector with NaN in all its elements
        static constexpr
        Vector Poisoned()
            requires std::numeric_limits<TData>::has_quiet_NaN
        {
            return Vector(std::numeric_limits<TData>::quiet_NaN());
        }

        /// @brief Returns true if this vector is poisoned, i.e. has NaN in all its elements.
        /// @return True if this vector is poisoned, false otherwise
        bool IsPoisoned()
            requires std::numeric_limits<TData>::has_quiet_NaN
        {
            return [this]<int... I>(std::integer_sequence<int, I...>) {
                return (std::isnan(elements[I]) || ...);
            }(std::make_integer_sequence<int, NDim>{});
        }

        ~Vector() = default;

	protected:
        // Contains the dimension of the inserted type and an inserter method
        // that inserts DIMENSION values at iterator and returns iterator after last insterted
        template<class T, class Dummy = void>
        struct Inserter
        {
            inline static constexpr
            int DIMENSION = 1;

            CUDA_FUNCTION static constexpr
            TData* Insert(TData* it, T const& elements) {
                *it = static_cast<TData>(elements);
                return ++it;
            }
        };

        template<class Dummy>
        struct Inserter<TData, Dummy>
        {
            inline static constexpr
            int DIMENSION = 1;

            CUDA_FUNCTION static constexpr
            TData* Insert(TData* it, TData const& elements) {
                *it = elements;
                return ++it;
            }
        };

        template<int NDimOther>
        struct Inserter<Vector<NDimOther, TData>, void>
        {
            inline static constexpr
            int DIMENSION = NDimOther;

            CUDA_FUNCTION static constexpr
            TData* Insert(TData* it, const Vector<NDimOther, TData>& elements) {
                for (int i = 0U; i < DIMENSION; ++i) {
                    *it = elements[i];
                    ++it;
                }
                return it;
            }
        };

        // Checks if all dimensions of input types are more than zero
        template<class... Ts>
        CUDA_FUNCTION static consteval
        bool CheckCorrectDimensions() {
            bool correctDims = true;
            ((correctDims = correctDims && Inserter<Ts>::DIMENSION > 0), ...);
            return correctDims;
        }

        // Sum dimensions of input types
        template<class... Ts>
        CUDA_FUNCTION static consteval
        int SumDimensions() {
            int dimSum = 0;
            ((dimSum += Inserter<Ts>::DIMENSION), ...);
            return dimSum;
        }

        // template<class TCurrent, class TNext, class... TRest>
        // CUDA_FUNCTION constexpr
        // void RecursiveInit(CuSTD::array<TData, NDim>::iterator it, TCurrent const& current, TNext const& next, TRest const&... rest) {
        //     RecursiveInit(Inserter<TCurrent>::Insert(it, current), next, rest...);
        // }

        // template<class TCurrent>
        // CUDA_FUNCTION constexpr
        // void RecursiveInit(CuSTD::array<TData, NDim>::iterator it, TCurrent const& current) {
        //     Inserter<TCurrent>::Insert(it, current);
        // }

        template<class... Ts>
        CUDA_FUNCTION constexpr
        void SequentialInit(Ts const&... values) {
            TData* it = elements;
            ((it = Inserter<Ts>::Insert(it, values)), ...);
        }
	};

    /**********************************************************
    ***********************************************************
    ********************* Type defines ************************
    ***********************************************************
    **********************************************************/

    using Vector2i = Vector<2, int32_t>;
    using Vector3i = Vector<3, int32_t>;
    using Vector4i = Vector<4, int32_t>;

    using Vector2ui = Vector<2, uint32_t>;
    using Vector3ui = Vector<3, uint32_t>;
    using Vector4ui = Vector<4, uint32_t>;

    using Vector2sf = Vector<2, sfloat>;
    using Vector3sf = Vector<3, sfloat>;
    using Vector4sf = Vector<4, sfloat>;

    using Vector2cf = Vector<2, cfloat>;
    using Vector3cf = Vector<3, cfloat>;
    using Vector4cf = Vector<4, cfloat>;
}
