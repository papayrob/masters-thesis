/**
 * File name: Triangle.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2022 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "AABB.hpp"

namespace ProperRT
{
	/// @brief Struct for representing triangles.
	struct Triangle
	{
        using VertexType = Vector3sf;

        struct IntersectionCoordinates
        {
            Vector2cf barycentric;
        };
        
		/// @brief Vertices of the triangle.
        Vector<3, VertexType> vertices{};

        /// @brief Linearly interpolates between @p triA and @p triB using @p alpha.
        /// @param source Source triangle
        /// @param target Target triangle
        /// @param alpha Interpolant
        /// @return Interpolated triangle
        CUDA_FUNCTION static
        Triangle LERP(Triangle const& source, Triangle const& target, sfloat alpha) {
            return Triangle{{
                VertexType::LERP(source.vertices[0], target.vertices[0], alpha),
                VertexType::LERP(source.vertices[1], target.vertices[1], alpha),
                VertexType::LERP(source.vertices[2], target.vertices[2], alpha)
            }};
        }

        /// @brief Returns the normal of this triangle.
        /// @return Normal of this triangle
        CUDA_FUNCTION
        Vector3cf GetNormal() const {
            return Vector3cf::Cross({ Vector3cf{ vertices[1] - vertices[0] }, Vector3cf{ vertices[2] - vertices[0] } }).Normalized();
        }

        /// @brief Returns the normal of this triangle.
        /// @return Normal of this triangle
        CUDA_FUNCTION
        Vector3cf GetNormal(IntersectionCoordinates const&) const {
            return GetNormal();
        }

        /// @brief Transforms the triangle using @p matrix.
        /// @param matrix Transformation matrix
        /// @return Transformed triangle
        CUDA_FUNCTION
        Triangle Transform(Matrix4x4cf const& matrix) const {
            return Triangle{{
                VertexType{ matrix * Vector4cf{ static_cast<Vector3cf>(vertices[0]), 1._cf } },
                VertexType{ matrix * Vector4cf{ static_cast<Vector3cf>(vertices[1]), 1._cf } },
                VertexType{ matrix * Vector4cf{ static_cast<Vector3cf>(vertices[2]), 1._cf } }
            }};
        }

		/// @brief Calculates intersection of @p ray with this triangle, setting @p t if intersects.
        /// @details Source: https://dl.acm.org/doi/10.1080/10867651.1997.10487468
		/// @param ray Ray to intersect with
		/// @param[out] t Distance of the intersection from ray origin
		/// @return True if @p ray intersects, false otherwise
        CUDA_FUNCTION
        bool Intersect(Ray const& ray, cfloat& t, IntersectionCoordinates& ic) const;

        /// @brief Splits this triangle into 2 parts located inside left and right parts of @p clippingBox, split
        /// by @p splitCoord on @p splitAxis, and computes their tight bounding boxes.
        /// @param clippingBox Bounding box to clip with, that is split by @p splitCoord
        /// @param splitAxis The axis of the split
        /// @param splitCoord The coordinate of the split
        /// @return Left and right tight bounding boxes of the split and clipped triangle
        CUDA_FUNCTION
        CuSTD::pair<AABB, AABB> SplitClipBB(AABB const& clippingBox, int splitAxis, cfloat splitCoord) const;

        /// @brief Returns the axis-aligned bounding box of this triangle.
        /// @return Axis-aligned bounding box of this triangle
        CUDA_FUNCTION
        AABB GetAABB() const;

    private:
        // Helper method for GetABBB
        CUDA_FUNCTION static
        void FindAndSetMinMax(sfloat x0, sfloat x1, sfloat x2, sfloat& xMin, sfloat& xMax);
	};
}
