/**
 * File name: TriangleMesh.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "TriangleMesh.hpp"

#include "glbinding/gl/gl.h"

#include "Graphics/OpenGL/OGLUtilities.hpp"
#include "SceneDefinition/Components/Camera.hpp"
#include "SceneDefinition/Components/Transform.hpp"
#include "Graphics/OpenGL/Shader.hpp"
#include "Graphics/Raytracing/GPUMeshManager.cuh"
#include "RTApp.hpp"

namespace
{

ProperRT::OpenGL::Shader shader;

}

namespace ProperRT
{
    struct TriangleMesh::ComputeStorage
    {
        DeviceUniquePointer<Triangle[]> triangles;

        ~ComputeStorage() = default;
    };

    TriangleMesh::TriangleMesh() noexcept {
        if (RTApp::GetRenderingMode() == RTApp::RenderingMode::OpenGL && shader.Program() == 0) {
            shader = OpenGL::Shader{OpenGL::Shader::Sources{
                .vertexSource{ OpenGL::Shader::LoadShaderSource(AssetID{"Shaders/TriangleMesh.vs"}) },
                .geometrySource{ OpenGL::Shader::LoadShaderSource(AssetID{"Shaders/TriangleMesh.gs"}) },
                .fragmentSource{ OpenGL::Shader::LoadShaderSource(AssetID{"Shaders/TriangleMesh.fs"}) }
            }};
        }
    }

    TriangleMesh::~TriangleMesh() {
        ReleaseGPUResources();
    }

    TriangleMesh::TriangleMesh(TriangleMesh&&) = default;

    TriangleMesh& TriangleMesh::operator=(TriangleMesh&&) = default;

    Primitive TriangleMesh::GetPrimitive(int32, int32 primitiveIdx) const {
        return triangles.at(primitiveIdx);
    }

    Primitive TriangleMesh::GetPrimitive(int32, int32 primitiveIdx, Matrix4x4cf const& transform) const {
        return triangles.at(primitiveIdx).Transform(transform);
    }

    void TriangleMesh::FillSubmeshInstance(GPUMeshManagement::Instance& instance, int32_t submeshIdx) {
        instance.meshData = GPUMeshManagement::MeshData<Triangle>{
            computeStorage->triangles.Get()
        };
    }

    void TriangleMesh::SyncToGPU() {
        if (VAO == 0) {
            if (RTApp::GetRenderingMode() == RTApp::RenderingMode::OpenGL) {
                // Create VAO
                gl::glCreateVertexArrays(1, &VAO);

                // Create buffers
                vertexBuffer = CreateOGLBuffer(std::span(triangles));

                // Enable attribute
                gl::glEnableVertexArrayAttrib(VAO, 0);
                // Attach buffer object (bind VAO to vertexBuffer at index 0 with 0 offset and vector size stride)
                gl::glVertexArrayVertexBuffer(VAO, 0, vertexBuffer, 0, sizeof(Triangle::VertexType));
                // Setup attribute format (stride and relative offset)
                gl::glVertexArrayAttribFormat(VAO, 0, 3, gl::GLenum::GL_FLOAT, gl::GL_FALSE, 0);
                // Associate attribute at 0 with buffer object at 0
                gl::glVertexArrayAttribBinding(VAO, 0, 0);
            }

            computeStorage = std::make_unique<ComputeStorage>();
            computeStorage->triangles = DeviceUniquePointer<Triangle[]>::AllocateAndUpload(triangles);
        }
        else {
            if (RTApp::GetRenderingMode() == RTApp::RenderingMode::OpenGL) {
                gl::glNamedBufferSubData(vertexBuffer, 0, std::span(triangles).size_bytes(), triangles.data());
            }
            computeStorage->triangles.Upload(triangles);
        }
    }

    void TriangleMesh::ReleaseCPUResources() {
        triangles.clear();
    }

    void TriangleMesh::ReleaseGPUResources() {
        if (VAO != 0) {
            if (RTApp::GetRenderingMode() == RTApp::RenderingMode::OpenGL) {
                gl::glDeleteBuffers(1, &vertexBuffer);
                gl::glDeleteVertexArrays(1, &VAO);
            }
            computeStorage = nullptr;
            VAO = 0;
        }
    }

    void TriangleMesh::GLBind() const {
        gl::glBindVertexArray(VAO);
        shader.Use();
    }

    void TriangleMesh::Rasterize(Matrix4x4f const& modelMat, RasterizationMatrices const &matrices, bool bind) {
        if (VAO == 0) {
            SyncToGPU();
        }
        if (bind) {
            GLBind();
        }

        Matrix4x4f PVM = matrices.PV * modelMat;
        Matrix4x4f VM = matrices.view * modelMat;
        gl::glUniformMatrix4fv(0, 1, gl::GL_FALSE, &PVM[0][0]);
        gl::glUniformMatrix4fv(1, 1, gl::GL_FALSE, &matrices.view[0][0]);
        gl::glUniformMatrix4fv(2, 1, gl::GL_FALSE, &VM[0][0]);

        gl::glUniform4fv(3, 1, Components::Light::ambientColor.elements);

        gl::GLint materialLoc = 4;
        gl::glUniform4fv(materialLoc++, 1, materials.at(0).colorDiffuse.elements);
        //gl::glUniform4fv(materialLoc++, 1, materials.at(0).colorSpecular.elements);

        gl::glDrawArrays(
            gl::GLenum::GL_TRIANGLES,
            0,
            triangles.size() * 3
        );
    }

    void TriangleMesh::Rasterize(Matrix4x4f const& modelMat, RasterizationMatrices const &matrices, std::span<int32 const>, bool bind) {
        Rasterize(modelMat, matrices, bind);
    }
}
