/**
 * File name: Triangle.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2022 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */

#include "Triangle.hpp"

#include "Utilities/Utilities.hpp"

namespace ProperRT
{
    CUDA_FUNCTION
    bool Triangle::Intersect(Ray const& ray, cfloat& t, IntersectionCoordinates& ic) const {
        // Edges
        const Vector3cf e1 = Vector3cf{ vertices[1] } - Vector3cf{ vertices[0] };
        const Vector3cf e2 = Vector3cf{ vertices[2] } - Vector3cf{ vertices[0] };

        // Determinant
        const Vector3cf pvec = Vector3cf::Cross({ ray.direction, e2 });
        const cfloat det = Vector3cf::Dot(e1, pvec);
        if (cuda::std::abs(det) < .000'001_cf) {
            return false;
        }

        const cfloat detInv = 1.0_cf / det;

        // U parameter and test bounds
        const Vector3cf tvec = ray.origin - Vector3cf{ vertices[0] };
        Vector2cf uv;
        uv.x() = Vector3cf::Dot(tvec, pvec) * detInv;
        if (uv.x() < 0.0_cf || 1.0_cf < uv.x()) {
            return false;
        }

        // V parameter and test bounds
        const Vector3cf qvec = Vector3cf::Cross({ tvec, e1 });
        uv.y() = Vector3cf::Dot(ray.direction, qvec) * detInv;
        if (uv.y() < 0.0_cf || 1.0_cf < uv.x() + uv.y()) {
            return false;
        }

        // Ray intersects triangle
        t = Vector3cf::Dot(e2, qvec) * detInv;
        ic.barycentric = uv;
        return t > 0._cf;
    }

    CUDA_FUNCTION
    CuSTD::pair<AABB, AABB> Triangle::SplitClipBB(AABB const& clippingBox, int splitAxis, cfloat splitCoord) const {
        CuSTD::pair<AABB, AABB> splitBB{ AABB::InfinitelySmall(), AABB::InfinitelySmall() };

        // #ifdef __CUDA_ARCH__
        // printf("Split clipping (%.5f,%.5f,%.5f),(%.5f,%.5f,%.5f) on %d %.5f\n",
        // clippingBox.minBounds.x(), clippingBox.minBounds.y(), clippingBox.minBounds.z(),
        // clippingBox.maxBounds.x(), clippingBox.maxBounds.y(), clippingBox.maxBounds.z(),
        // splitAxis, splitCoord);
        // #endif

        auto sideLengths = clippingBox.SideLengths();
        bool enabledAxes[3];
        for (int axis = 0; axis < 3; ++axis) {
            enabledAxes[axis] = (sideLengths[axis] > ZERO_COMP_EPSILON);
        }

        Vector3cf c_vertices[3] { Vector3cf{ vertices[0] }, Vector3cf{ vertices[1] }, Vector3cf{ vertices[2] } };

        // Intersect triangle edges with bounding box faces and splitting plane
        for (int vertex = 0; vertex < 3; ++vertex) {
            int nextVertex = (vertex + 1) % 3;
            for (int bound = 0; bound < 2; ++bound) {
                for (int axis = 0; axis < 3; ++axis) {
                    // Intersect triangle edge with bounding box plane
                    cfloat denom = c_vertices[nextVertex][axis] - c_vertices[vertex][axis];
                    // exclude edges parallel to the plane, if inside the box, the vertices will be included later
                    if (CuSTD::abs(denom) > ZERO_COMP_EPSILON) {

                        cfloat d = (clippingBox.GetBounds(AABB::BoundsEnum{ bound })[axis] - c_vertices[vertex][axis]) / denom;

                        // Intersection on triangle edge line segment
                        if (0._cf <= d && d <= 1._cf) {
                            Vector3cf intersection;
                            if (d == 0._cf) {
                                intersection = c_vertices[vertex];
                            }
                            else if (d == 1._cf) {
                                intersection = c_vertices[nextVertex];
                            }
                            else {
                                intersection = c_vertices[vertex] + d * (c_vertices[nextVertex] - c_vertices[vertex]);
                            }

                            auto otherAxis1{ (axis + 1) % 3 }, otherAxis2{ (axis + 2) % 3 };
                            // Intersection on box side
                            if (
                                clippingBox.minBounds[otherAxis1] <= intersection[otherAxis1] &&
                                intersection[otherAxis1] <= clippingBox.maxBounds[otherAxis1]
                                &&
                                clippingBox.minBounds[otherAxis2] <= intersection[otherAxis2] &&
                                intersection[otherAxis2] <= clippingBox.maxBounds[otherAxis2]
                            ) {
                                intersection[axis] = clippingBox.GetBounds(AABB::BoundsEnum{ bound })[axis];
                                // Expand the corresponding bounding boxes
                                if (intersection[splitAxis] <= splitCoord) {
                                    splitBB.first.Include(AABB::VertexType{ intersection });
                                    // #ifdef __CUDA_ARCH__
                                    // printf("Left intersection (%.5f,%.5f,%.5f) tri edge %d with bb face %d %d\n", intersection.x(), intersection.y(), intersection.z(), vertex, axis, bound);
                                    // #endif
                                }
                                if (splitCoord <= intersection[splitAxis]) {
                                    splitBB.second.Include(AABB::VertexType{ intersection });
                                    // #ifdef __CUDA_ARCH__
                                    // printf("Right intersection (%.5f,%.5f,%.5f) tri edge %d with bb face %d %d\n", intersection.x(), intersection.y(), intersection.z(), vertex, axis, bound);
                                    // #endif
                                }
                            }
                        }
                    }
                }
            }

            // Intersect triangle edge with splitting plane
            cfloat denom = (c_vertices[nextVertex][splitAxis] - c_vertices[vertex][splitAxis]);
            if (CuSTD::abs(denom) > ZERO_COMP_EPSILON) {
                cfloat d = (splitCoord - c_vertices[vertex][splitAxis]) / denom;
                
                // Intersection on triangle edge line segment
                if (0._cf <= d && d <= 1._cf) {
                    Vector3cf intersection;
                    if (d == 0._cf) {
                        intersection = c_vertices[vertex];
                    }
                    else if (d == 1._cf) {
                        intersection = c_vertices[nextVertex];
                    }
                    else {
                        intersection = c_vertices[vertex] + d * (c_vertices[nextVertex] - c_vertices[vertex]);
                    }

                    auto otherAxis1{ (splitAxis + 1) % 3 }, otherAxis2{ (splitAxis + 2) % 3 };
                    // Intersection on box side
                    if (
                        clippingBox.minBounds[otherAxis1] <= intersection[otherAxis1] &&
                        intersection[otherAxis1] <= clippingBox.maxBounds[otherAxis1]
                        &&
                        clippingBox.minBounds[otherAxis2] <= intersection[otherAxis2] &&
                        intersection[otherAxis2] <= clippingBox.maxBounds[otherAxis2]
                    ) {
                        intersection[splitAxis] = splitCoord;
                        // Expand the corresponding bounding boxes
                        splitBB.first.Include(AABB::VertexType{ intersection });
                        // #ifdef __CUDA_ARCH__
                        // printf("Left intersection (%.5f,%.5f,%.5f) tri edge %d with split\n", intersection.x(), intersection.y(), intersection.z(), vertex);
                        // #endif
                        splitBB.second.Include(AABB::VertexType{ intersection });
                        // #ifdef __CUDA_ARCH__
                        // printf("Right intersection (%.5f,%.5f,%.5f) tri edge %d with split\n", intersection.x(), intersection.y(), intersection.z(), vertex);
                        // #endif
                    }
                }
            }

            // Include vertices inside the bounding box
            if (clippingBox.Includes(vertices[vertex])) {
                if (c_vertices[vertex][splitAxis] <= splitCoord) {
                    splitBB.first.Include(vertices[vertex]);
                    // #ifdef __CUDA_ARCH__
                    // printf("Left inside vertex %d\n", vertex);
                    // #endif
                }
                if (splitCoord <= c_vertices[vertex][splitAxis]) {
                    splitBB.second.Include(vertices[vertex]);
                    // #ifdef __CUDA_ARCH__
                    // printf("Right inside vertex %d\n", vertex);
                    // #endif
                }
            }
        }


        auto triangleNormal = GetNormal();

        auto IntersectTriangleWithBoxEdge = [&c_vertices, &sideLengths, &triangleNormal, this, &clippingBox, &splitBB, splitAxis, splitCoord] (Vector3cf point, cfloat side, int axis)
        {
            if (CuSTD::abs(triangleNormal[axis]) <= ZERO_COMP_EPSILON) {
                return;
            }

            cfloat d = (c_vertices[0] - point).Dot(triangleNormal) / (side * triangleNormal[axis]);
            Vector3cf intersection = point;
            intersection[axis] += d * side;

            if (clippingBox.minBounds[axis] <= intersection[axis] &&
                intersection[axis] <= clippingBox.maxBounds[axis]
                &&
                Vector3cf::Cross({c_vertices[1] - c_vertices[0], intersection - c_vertices[1]}).Dot(triangleNormal) >= 0._cf &&
                Vector3cf::Cross({c_vertices[2] - c_vertices[1], intersection - c_vertices[2]}).Dot(triangleNormal) >= 0._cf &&
                Vector3cf::Cross({c_vertices[0] - c_vertices[2], intersection - c_vertices[0]}).Dot(triangleNormal) >= 0._cf
            ) {
                // Expand the corresponding bounding boxes
                if (intersection[splitAxis] <= splitCoord) {
                    splitBB.first.Include(AABB::VertexType{ intersection });
                    // #ifdef __CUDA_ARCH__
                    // printf("Left intersection (%.5f,%.5f,%.5f) box edge %d (%.5f,%.5f,%.5f) with tri face\n", intersection.x(), intersection.y(), intersection.z(), axis, point.x(), point.y(), point.z());
                    // #endif
                }
                if (splitCoord <= intersection[splitAxis]) {
                    splitBB.second.Include(AABB::VertexType{ intersection });
                    // #ifdef __CUDA_ARCH__
                    // printf("Right intersection (%.5f,%.5f,%.5f) box edge %d (%.5f,%.5f,%.5f) with tri face\n", intersection.x(), intersection.y(), intersection.z(), axis, point.x(), point.y(), point.z());
                    // #endif
                }
            }
        };

        // Intersect bounding box edges with the triangle face
        for (int firstAxis = 0; firstAxis < 3; ++firstAxis) {
            if (enabledAxes[firstAxis]) {
                auto secondPoint = clippingBox.minBounds;
                secondPoint[firstAxis] = clippingBox.maxBounds[firstAxis];
                
                IntersectTriangleWithBoxEdge(Vector3cf{ clippingBox.minBounds }, sideLengths[firstAxis], firstAxis);
                
                for (int secondAxis = 0; secondAxis < 3; ++secondAxis) {
                    if (secondAxis != firstAxis) {
                        IntersectTriangleWithBoxEdge(Vector3cf{ secondPoint }, sideLengths[secondAxis], secondAxis);
                    }
                }

                IntersectTriangleWithBoxEdge(Vector3cf{ clippingBox.maxBounds }, sideLengths[firstAxis], firstAxis);
            }
        }

        // Intersect split plane edges with triangle face
        {
            auto otherAxis1{ (splitAxis + 1) % 3 }, otherAxis2{ (splitAxis + 2) % 3 };

            Vector3cf point = Vector3cf{ clippingBox.minBounds };
            point[splitAxis] = splitCoord;
            if (enabledAxes[otherAxis1]) {
                IntersectTriangleWithBoxEdge(point, sideLengths[otherAxis1], otherAxis1);
            }

            point = Vector3cf{ clippingBox.minBounds }; // CUDA release bugs here if this line is not here
            point[splitAxis] = splitCoord;
            if (enabledAxes[otherAxis2]) {
                IntersectTriangleWithBoxEdge(point, sideLengths[otherAxis2], otherAxis2);
            }

            point = Vector3cf{ clippingBox.maxBounds };
            point[splitAxis] = splitCoord;
            if (enabledAxes[otherAxis1]) {
                IntersectTriangleWithBoxEdge(point, sideLengths[otherAxis1], otherAxis1);
            }

            point = Vector3cf{ clippingBox.maxBounds };
            point[splitAxis] = splitCoord;
            if (enabledAxes[otherAxis2]) {
                IntersectTriangleWithBoxEdge(point, sideLengths[otherAxis2], otherAxis2);
            }
        }

        if (splitBB.first.IsValid()) {
            splitBB.first.ExpandToNonZeroVolume();
        }
        else {
            splitBB.first = AABB::InfinitelySmall();
        }

        if (splitBB.second.IsValid()) {
            splitBB.second.ExpandToNonZeroVolume();
        }
        else {
            splitBB.second = AABB::InfinitelySmall();
        }

        return splitBB;
    }

    CUDA_FUNCTION
    AABB Triangle::GetAABB() const {
        AABB boundingBox;
        // Find and set min and max bounds of the bounding box for all axes
        for (int i = 0; i < 3; ++i) {
            FindAndSetMinMax(vertices[0][i], vertices[1][i], vertices[2][i], boundingBox.minBounds[i], boundingBox.maxBounds[i]);
        }
        boundingBox.ExpandToNonZeroVolume();
        return boundingBox;
    }

    CUDA_FUNCTION
    void Triangle::FindAndSetMinMax(sfloat x0, sfloat x1, sfloat x2, sfloat& xMin, sfloat& xMax) {
        // x0, x1 and x2 are coordinates of the three triangle vertices,
        // xMin and xMax are output variables, representing the bounds of the bounding box

        // Find which coordinate is minimal and which maximal and set them to relevant outputs

        // x0 candidate for min, x1 for max
        if (x0 < x1) {
            // x0 is min, x2 is second candidate for max
            if (x0 < x2) {
                xMin = x0;
                xMax = x1 > x2 ? x1 : x2;
            }
            // x2 is min, x1 is max (x2 < x0 < x1)
            else {
                xMin = x2;
                xMax = x1;
            }
        }
        // x1 is candidate for min, x0 for max
        else {
            // x1 is min, x2 is second cadidate for max
            if (x1 < x2) {
                xMin = x1;
                xMax = x0 > x2 ? x0 : x2;
            }
            // x2 is min, x0 is max (x2 < x1 < x0)
            else {
                xMin = x2;
                xMax = x0;
            }
        }
    }
}
