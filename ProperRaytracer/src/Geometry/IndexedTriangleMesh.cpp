/**
 * File name: IndexedTriangleMesh.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "IndexedTriangleMesh.hpp"

#include <algorithm>
#include "glbinding/gl/gl.h"

#include "Graphics/OpenGL/OGLUtilities.hpp"
#include "SceneDefinition/Components/Camera.hpp"
#include "SceneDefinition/Components/Transform.hpp"
#include "SceneDefinition/Components/Renderer.hpp"
#include "Graphics/OpenGL/Shader.hpp"
#include "Graphics/Raytracing/GPUMeshManager.cuh"
#include "RTApp.hpp"

namespace
{

ProperRT::OpenGL::Shader shader;

}

namespace ProperRT
{
    struct IndexedTriangleMesh::ComputeStorage
    {
        DeviceUniquePointer<Vector3sf[]> vertices;
        DeviceUniquePointer<IndexedTriangle[]> indeces;
    };

    IndexedTriangleMesh::IndexedTriangleMesh() noexcept {
        if (RTApp::GetRenderingMode() == RTApp::RenderingMode::OpenGL && shader.Program() == 0) {
            shader = OpenGL::Shader{OpenGL::Shader::Sources{
                .vertexSource{ OpenGL::Shader::LoadShaderSource(AssetID{"Shaders/TriangleMesh.vs"}) },
                .geometrySource{ OpenGL::Shader::LoadShaderSource(AssetID{"Shaders/TriangleMesh.gs"}) },
                .fragmentSource{ OpenGL::Shader::LoadShaderSource(AssetID{"Shaders/TriangleMesh.fs"}) }
            }};
        }
    }

    IndexedTriangleMesh::~IndexedTriangleMesh() {
        ReleaseGPUResources();
    }

    IndexedTriangleMesh::IndexedTriangleMesh(IndexedTriangleMesh&&) = default;

    IndexedTriangleMesh& IndexedTriangleMesh::operator=(IndexedTriangleMesh&&) = default;


    std::size_t IndexedTriangleMesh::AddSubmesh(std::span<int32_t const> subIndeces, Material material) {
        if (VAO != 0) {
            throw std::runtime_error("Cannot add submeshes after creating GPU resources");
        }

        auto offset = indeces.size();
        auto size = subIndeces.size();
        if (size % 3 != 0) {
            throw std::runtime_error("Number of submesh indeces must be a multiple of 3");//TODO
        }

        //TODO reserve
        for (std::size_t i = 0; i < size; i += 3) {
            indeces.push_back(IndexedTriangle{ subIndeces[i], subIndeces[i + 1], subIndeces[i + 2] });
        }

        submeshes.push_back(std::vector_span(indeces).subspan(offset, size / 3));
        materials.push_back(material);
        
        return submeshes.size() - 1;
    }

    std::size_t IndexedTriangleMesh::AddSubmesh(std::span<IndexedTriangle const> subIndeces, Material material) {
        if (VAO != 0) {
            throw std::runtime_error("Cannot add submeshes after creating GPU resources");
        }

        auto offset = indeces.size();
        //std::copy(subIndeces.begin(), subIndeces.end(), std::back_inserter(indeces));
        indeces.insert(indeces.end(), subIndeces.begin(), subIndeces.end());

        submeshes.push_back(std::vector_span(indeces).subspan(offset, subIndeces.size()));//TODO last
        materials.push_back(material);

        return submeshes.size() - 1;
    }

    std::size_t IndexedTriangleMesh::AddSubmesh(std::vector_span<IndexedTriangle const> subIndeces, Material material) {
        if (VAO != 0) {
            throw std::runtime_error("Cannot add submeshes after creating GPU resources");
        }

        if (!subIndeces.references(indeces)) {
            throw std::runtime_error("Submesh indeces reference different indeces vector");//TODO
        }

        submeshes.push_back(subIndeces);
        materials.push_back(material);

        return submeshes.size() - 1;
    }

    Primitive IndexedTriangleMesh::GetPrimitive(int32_t submeshIdx, int32_t primitiveIdx) const {
        return submeshes.at(submeshIdx)[primitiveIdx].ToTriangle(vertices);
    }

    Primitive IndexedTriangleMesh::GetPrimitive(int32_t submeshIdx, int32_t primitiveIdx, Matrix4x4cf const& transform) const {
        return submeshes.at(submeshIdx)[primitiveIdx].ToTriangle(vertices).Transform(transform);
    }

    std::vector<int32_t> IndexedTriangleMesh::GetPrimitiveCounts() const noexcept {
        std::vector<int32_t> out(submeshes.size());
        std::transform(submeshes.begin(), submeshes.end(), out.begin(), [](auto submesh) { return StdSize<int32>(submesh); });
        return out;
    }

    void IndexedTriangleMesh::FillSubmeshInstance(GPUMeshManagement::Instance &instance, int32_t submeshIdx) {
        auto& submesh = submeshes[submeshIdx];
        instance.meshData = GPUMeshManagement::IndexedMeshData<IndexedTriangle>{
            computeStorage->vertices.Get(),
            DevicePointer<IndexedTriangle[]>{ computeStorage->indeces.Get().data + submesh.offset(), StdSize<DevicePointer<IndexedTriangle[]>::SizeType>(submesh) }
        };
    }

    void IndexedTriangleMesh::SyncToGPU() {
        if (VAO == 0) {
            if (RTApp::GetRenderingMode() == RTApp::RenderingMode::OpenGL) {
                // Create VAO
                gl::glCreateVertexArrays(1, &VAO);

                // Create buffers
                vertexBuffer = CreateOGLBuffer(std::span(vertices));
                indexBuffer = CreateOGLBuffer(std::span(indeces));

                // Enable attribute
                gl::glEnableVertexArrayAttrib(VAO, 0);
                // Attach buffer object (bind VAO to vertexBuffer at index 0 with 0 offset and vector size stride)
                gl::glVertexArrayVertexBuffer(VAO, 0, vertexBuffer, 0, sizeof(decltype(vertices)::value_type));
                // Setup attribute format (stride and relative offset)
                gl::glVertexArrayAttribFormat(VAO, 0, 3, gl::GLenum::GL_FLOAT, gl::GL_FALSE, 0);
                // Associate attribute at 0 with buffer object at 0
                gl::glVertexArrayAttribBinding(VAO, 0, 0);

                // Attach element buffer
                gl::glVertexArrayElementBuffer(VAO, indexBuffer);
            }
            else {
                VAO = 1;
            }

            // Create compute storage
            computeStorage = std::make_unique<ComputeStorage>();
            computeStorage->vertices = DeviceUniquePointer<Vector3sf[]>::AllocateAndUpload(vertices);
            computeStorage->indeces = DeviceUniquePointer<IndexedTriangle[]>::AllocateAndUpload(indeces);
        }
        else {
            if (RTApp::GetRenderingMode() == RTApp::RenderingMode::OpenGL) {
                gl::glNamedBufferSubData(vertexBuffer, 0, std::span(vertices).size_bytes(), vertices.data());
                gl::glNamedBufferSubData(indexBuffer, 0, std::span(indeces).size_bytes(), indeces.data());
            }

            computeStorage->vertices.Upload(vertices);
            computeStorage->indeces.Upload(indeces);
        }
    }

    void IndexedTriangleMesh::ReleaseCPUResources() {
        vertices.clear();
        indeces.clear();
    }

    void IndexedTriangleMesh::ReleaseGPUResources() {
        if (VAO != 0) {
            if (RTApp::GetRenderingMode() == RTApp::RenderingMode::OpenGL) {
                gl::glDeleteBuffers(1, &vertexBuffer);
                gl::glDeleteBuffers(1, &indexBuffer);
                gl::glDeleteBuffers(1, &colorBuffer);
                gl::glDeleteVertexArrays(1, &VAO);
            }
            computeStorage = nullptr;
            VAO = 0;
        }
    }

    void IndexedTriangleMesh::GLBind() const {
        if (VAO == 0) {
            throw std::runtime_error("Cannot bind mesh without GPU resources");//TODO
        }

        gl::glBindVertexArray(VAO);
        shader.Use();
    }

    void IndexedTriangleMesh::Rasterize(Matrix4x4f const& modelMat, RasterizationMatrices const &matrices, bool bind) {
        if (VAO == 0) {
            SyncToGPU();
        }
        if (bind) {
            GLBind();
        }

        Matrix4x4f PVM = matrices.PV * modelMat;
        Matrix4x4f VM = matrices.view * modelMat;
        gl::glUniformMatrix4fv(0, 1, gl::GL_FALSE, &PVM[0][0]);
        gl::glUniformMatrix4fv(1, 1, gl::GL_FALSE, &matrices.view[0][0]);
        gl::glUniformMatrix4fv(2, 1, gl::GL_FALSE, &VM[0][0]);

        gl::glUniform4fv(3, 1, Components::Light::ambientColor.elements);

        int submeshIdx = 0;
        for (auto const& submesh : submeshes) {
            gl::GLint materialLoc = 4;
            gl::glUniform4fv(materialLoc++, 1, materials.at(submeshIdx).colorDiffuse.elements);
            //gl::glUniform4fv(materialLoc++, 1, materials.at(submeshIdx).colorSpecular.elements);

            gl::glDrawElements(
                gl::GLenum::GL_TRIANGLES,
                submesh.size() * 3,
                gl::GLenum::GL_UNSIGNED_INT,
                (void const*)(submesh.offset() * sizeof(decltype(indeces)::value_type))
            );
            ++submeshIdx;
        }
    }

    void IndexedTriangleMesh::Rasterize(Matrix4x4f const& modelMat, RasterizationMatrices const &matrices, std::span<int32_t const> submeshIndeces, bool bind) {
        if (VAO == 0) {
            SyncToGPU();
        }
        if (bind) {
            GLBind();
        }

        Matrix4x4f PVM = matrices.PV * modelMat;
        Matrix4x4f VM = matrices.view * modelMat;
        gl::glUniformMatrix4fv(0, 1, gl::GL_FALSE, &PVM[0][0]);
        gl::glUniformMatrix4fv(1, 1, gl::GL_FALSE, &matrices.view[0][0]);
        gl::glUniformMatrix4fv(2, 1, gl::GL_FALSE, &VM[0][0]);

        gl::glUniform4fv(3, 1, Components::Light::ambientColor.elements);

        for (auto submeshIdx : submeshIndeces) {
            gl::GLint materialLoc = 4;
            gl::glUniform4fv(materialLoc++, 1, materials.at(submeshIdx).colorDiffuse.elements);
            //gl::glUniform4fv(materialLoc++, 1, materials.at(submeshIdx).colorSpecular.elements);

            auto const& submesh = submeshes.at(submeshIdx);
            
            gl::glDrawElements(
                gl::GLenum::GL_TRIANGLES,
                submesh.size() * 3,
                gl::GLenum::GL_UNSIGNED_INT,
                (void const*)(submesh.offset() * sizeof(decltype(indeces)::value_type))
            );
        }
    }
}
