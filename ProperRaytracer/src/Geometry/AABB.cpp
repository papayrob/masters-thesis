/**
 * File name: AABB.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2022 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */

#include "AABB.hpp"

#include "Utilities/Utilities.hpp"

namespace ProperRT
{
    void AABB::Include(VertexType const& include) {
        minBounds = VertexType::Min(minBounds, include);
        maxBounds = VertexType::Max(maxBounds, include);
    }

    bool AABB::Includes(VertexType const& point) const {
        return (minBounds <= point).All() && (point <= maxBounds).All();
    }

    void AABB::Include(AABB const& include) {
        minBounds = VertexType::Min(minBounds, include.minBounds);
        maxBounds = VertexType::Max(maxBounds, include.maxBounds);
    }

    bool AABB::Intersect(Ray const& ray, cfloat& tMin, cfloat& tMax) const {
        // Division by zero should result in inf
        Vector3cf dirInv = 1._cf / ray.direction;

        auto min = [](cfloat a, cfloat b) {
            return a < b ? a : b;
        };
        auto max = [](cfloat a, cfloat b) {
            return a > b ? a : b;
        };

        // Compute the entry and exit intersection distance with extended sides
        // and keep the one that actually intersects the box
        cfloat t1 = (minBounds[0] - ray.origin[0]) * dirInv[0];
        cfloat t2 = (maxBounds[0] - ray.origin[0]) * dirInv[0];

        tMin = min(t1, t2);
        tMax = max(t1, t2);

        for (int i = 1; i < 3; ++i) {
            t1 = (minBounds[i] - ray.origin[i]) * dirInv[i];
            t2 = (maxBounds[i] - ray.origin[i]) * dirInv[i];

            tMin = max(tMin, min(t1, t2));
            tMax = min(tMax, max(t1, t2));
        }

        return tMax > max(tMin, 0.0_cf);
    }

    bool AABB::Intersect(AABB other, AABB& result) const {
        result.maxBounds = VertexType::Min(maxBounds, other.maxBounds);
        result.minBounds = VertexType::Max(minBounds, other.minBounds);
        return result.IsValid();
    }

    bool AABB::Intersects(AABB const& other) const {
        return (minBounds <= other.maxBounds).All() && (other.minBounds <= maxBounds).All();
    }

    void AABB::SplitCoord(Axis3D axis, VertexType::DataType coord, AABB& left, AABB& right) const {
        auto ax = ToUnderlying(axis);
        if (coord < minBounds[ax] || maxBounds[ax] < coord) {
            #ifndef __CUDA_ARCH__
            throw std::invalid_argument("Split coordinate must be between min and max bounds");
            #else
            printf("Split coordinate must be between min and max bounds");
            __trap();
            #endif
        }

        // Copy coords
        left = right = *this;

        // Set relevant coords to split value in left and right bounding boxes
        left.maxBounds[ax] = right.minBounds[ax] = coord;
    }

    void AABB::SplitLERP(Axis3D axis, cfloat t, AABB& left, AABB& right) const {
        if (t < 0.0_cf || 1.0_cf < t) {
            #ifndef __CUDA_ARCH__
            throw std::invalid_argument("Split parameter must be between 0 and 1"s);
            #else
            __trap();
            #endif
        }

        auto ax = ToUnderlying(axis);
        SplitCoord(axis, LERP(minBounds[ax], maxBounds[ax], t), left, right);
    }
}
