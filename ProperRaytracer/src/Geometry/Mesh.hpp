/**
 * File name: Mesh.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <span>

#include "Matrix.hpp"
#include "GenericMesh.hpp"
#include "Primitive.hpp"

namespace ProperRT
{
    struct RasterizationMatrices;
    
    /// @brief Class for representing a standard mesh.
    class Mesh : public GenericMesh
    {
    public:
        /// @brief Fetches a primitive from the mesh.
        /// @param submeshIdx Index of the submesh
        /// @param primitiveIdx Index of the primitive in a submesh
        virtual
        Primitive GetPrimitive(int32 submeshIdx, int32 primitiveIdx) const = 0;

        /// @brief Fetches a primitive from the mesh and transforms it to world space.
        /// @param submeshIdx Index of the submesh
        /// @param primitiveIdx Index of the primitive in a submesh
        virtual
        Primitive GetPrimitive(int32 submeshIdx, int32 primitiveIdx, Matrix4x4cf const& transform) const = 0;

        /// @brief Fills a GPU mesh manager instance with GPU data.
        /// @param instance Instance to fill
        /// @param submeshIdx Index of the submesh to fill instance with
        virtual
        void FillSubmeshInstance(GPUMeshManagement::Instance& instance, int32 submeshIdx) = 0;

        /// @brief Rasterizes the mesh.
        /// @details Do not call when in Raytracing rendering mode.
        /// @param modelMat Matrix for transforming the mesh into world space
        /// @param matrices Matrices used for projection
        /// @param bind Set to false if you are rendering this mesh multiple times and have called @ref GenericMesh::GLBind() yourself.
        virtual
        void Rasterize(Matrix4x4f const& modelMat, RasterizationMatrices const& matrices, bool bind = true) = 0;

        /// @brief Rasterizes submeshes according to @p submeshIndeces.
        /// @details Do not call when in Raytracing rendering mode.
        /// @param modelMat Matrix for transforming the mesh into world space
        /// @param matrices Matrices used for projection
        /// @param submeshIndeces Indeces of submeshes to be rendered
        /// @param bind Set to false if you are rendering this mesh multiple times and have called @ref GenericMesh::GLBind() yourself.
        virtual
        void Rasterize(Matrix4x4f const& modelMat, RasterizationMatrices const& matrices, std::span<int32 const> submeshIndeces, bool bind = true) = 0;
    };
}
