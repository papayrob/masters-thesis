/**
 * File name: Input.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2023 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <vector>
#include <bitset>

#include "Graphics/glfwInclude.hpp"
#include "Geometry/Vector.hpp"
#include "Utilities/Utilities.hpp"

namespace ProperRT
{
    /// @brief Class (static) for handling user input.
    class Input
    {
    public:
        /// @brief Struct for representing a key state.
        struct KeyState
        {
            /// @brief Bitset with boolean values for all states.
            std::bitset<3> states{};

            /// @brief Returns true if the key is currently pressed, false otherwise.
            /// @return True if the key is currently pressed, false otherwise
            std::bitset<3>::reference IsPressed() { return states[ToUnderlying(Action::Pressed)]; }

            /// @brief Returns true if the key was released last frame, but is pressed now, false otherwise.
            /// @return True if the key was released last frame, but is pressed now, false otherwise
            std::bitset<3>::reference HasPressed() { return states[ToUnderlying(Action::HasPressed)]; }

            /// @brief Returns true if the key was pressed last frame, but is released now, false otherwise.
            /// @return True if the key was pressed last frame, but is released now, false otherwise
            std::bitset<3>::reference HasReleased() { return states[ToUnderlying(Action::HasReleased)]; }


            /// @brief Returns true if the key is currently pressed, false otherwise.
            /// @return True if the key is currently pressed, false otherwise
            bool IsPressed() const { return states[ToUnderlying(Action::Pressed)]; }

            /// @brief Returns true if the key was released last frame, but is pressed now, false otherwise.
            /// @return True if the key was released last frame, but is pressed now, false otherwise
            bool HasPressed() const { return states[ToUnderlying(Action::HasPressed)]; }

            /// @brief Returns true if the key was pressed last frame, but is released now, false otherwise.
            /// @return True if the key was pressed last frame, but is released now, false otherwise
            bool HasReleased() const { return states[ToUnderlying(Action::HasReleased)]; }
        };

    public:
        /// @brief Callback function 
        static
        void KeyCallback(vkfw::Window const&, vkfw::Key key, int32_t, vkfw::KeyAction action, vkfw::ModifierKeyFlags);

        /// @brief Callback function
        static
        void MouseButtonCallback(vkfw::Window const&, vkfw::MouseButton key, vkfw::MouseButtonAction action, vkfw::ModifierKeyFlags);

        /// @brief Callback function
        static
        void MousePositionCallback(vkfw::Window const&, double x, double y);

        /// @brief Gets the key state of a specific key.
        /// @param key Requested key
        /// @return Key state of the requested key
        static
        KeyState const& GetKeyState(vkfw::Key key) { return keyStates[ToUnderlying(key)]; }

        /// @brief Gets the key state of a specific key.
        /// @param key Requested key
        /// @return Key state of the requested key
        static
        KeyState const& GetKeyState(vkfw::MouseButton key) { return mouseStates[ToUnderlying(key)]; }

        /// @brief Hides the cursor.
        static
        void HideCursor();

        /// @brief Shows the cursor.
        static
        void ShowCursor();

        /// @brief Returns the position of the cursor in pixels relative to the window.
        /// @return Position of the cursor in pixels relative to the window
        static
        Vector2cf GetCursorPosition();

        /// @brief Call after the end of a frame, resets single-frame key states to false.
        static
        void ResetKeyStates() {
            for (auto& state : keyStates) {
                state.HasPressed() = false;
                state.HasReleased() = false;
            }
            for (auto& state : mouseStates) {
                state.HasPressed() = false;
                state.HasReleased() = false;
            }
        }

        /// @brief Enables callbacks.
        static
        void Enable() {
            enabled = true;
        }

        /// @brief Disables callbacks.
        static
        void Disable() {
            enabled = false;
            ResetKeyStates();
        }

        /// @brief Returns true if input is enabled, false otherwise.
        static
        bool IsEnabled() { return enabled; }

    private:
        inline static
        bool enabled{ true };

        inline static
        std::vector<KeyState> keyStates{ ToUnderlying(vkfw::Key::eLAST) };

        inline static
        std::vector<KeyState> mouseStates{ ToUnderlying(vkfw::MouseButton::eLAST) };

        inline static
        Vector2cf mousePos;

    private:
        enum class Action : int8
        {
            Pressed = 0, HasPressed, HasReleased
        };
    };

}
