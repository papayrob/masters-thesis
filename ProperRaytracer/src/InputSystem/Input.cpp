/**
 * File name: Input.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2023 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "Input.hpp"

#include "RTApp.hpp"
#include "Graphics/glfwInclude.hpp"

namespace ProperRT
{
    void Input::KeyCallback(vkfw::Window const&, vkfw::Key key, int32_t, vkfw::KeyAction action, vkfw::ModifierKeyFlags) {
        if (!enabled || key == vkfw::Key::eUnknown) {
            return;
        }

        switch (action)
        {
        case vkfw::KeyAction::eRelease:
            if (keyStates[ToUnderlying(key)].IsPressed()) {
                keyStates[ToUnderlying(key)].IsPressed() = false;
                keyStates[ToUnderlying(key)].HasReleased() = true;
            }
            break;
        case vkfw::KeyAction::ePress:
            if (!keyStates[ToUnderlying(key)].IsPressed()) {
                keyStates[ToUnderlying(key)].IsPressed() = true;
                keyStates[ToUnderlying(key)].HasPressed() = true;
            }
            break;
        case vkfw::KeyAction::eRepeat:
            break;
        default:
            break;
        }
    }

    void Input::MouseButtonCallback(vkfw::Window const&, vkfw::MouseButton key, vkfw::MouseButtonAction action, vkfw::ModifierKeyFlags) {
        if (!enabled) {
            return;
        }

        switch (action)
        {
        case vkfw::MouseButtonAction::eRelease:
            if (mouseStates[ToUnderlying(key)].IsPressed()) {
                mouseStates[ToUnderlying(key)].IsPressed() = false;
                mouseStates[ToUnderlying(key)].HasReleased() = true;
            }
            break;
        case vkfw::MouseButtonAction::ePress:
            if (!mouseStates[ToUnderlying(key)].IsPressed()) {
                mouseStates[ToUnderlying(key)].IsPressed() = true;
                mouseStates[ToUnderlying(key)].HasPressed() = true;
            }
            break;
        default:
            break;
        }
    }

    void Input::MousePositionCallback(vkfw::Window const&, double x, double y) {
        mousePos = Vector2cf{ cfloat(x), cfloat(y) };
    }

    void Input::HideCursor() {
        auto& window = RTApp::Instance()->GetMainWindow();
        window->set<vkfw::InputMode::eCursor>(vkfw::CursorMode::eDisabled);
        window->set<vkfw::InputMode::eRawMouseMotion>(vkfw::eTrue);
    }

    void Input::ShowCursor() {
        auto& window = RTApp::Instance()->GetMainWindow();
        window->set<vkfw::InputMode::eRawMouseMotion>(vkfw::eFalse);
        window->set<vkfw::InputMode::eCursor>(vkfw::CursorMode::eNormal);
    }

    Vector2cf Input::GetCursorPosition() {
        return mousePos;
    }
}
