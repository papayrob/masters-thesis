/**
 * File name: AssetID.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2023 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "AssetID.hpp"

#include "AssetManager.hpp"
#include "InvalidAsset.hpp"

namespace ProperRT
{
    AssetID::AssetID(std::filesystem::path pathToAsset) {
        // Make absolute
        if (pathToAsset.is_relative()) {
            pathToAsset = AssetManager::assetRootPath / pathToAsset;
        }

        // Normalize without checking if it exists //(and check if it exists)
        try
        {
            pathToAsset = std::filesystem::weakly_canonical(pathToAsset);
        }
        catch (const std::exception& ex)
        {
            path = pathToAsset.string();
            throw InvalidAsset(*this, ex.what());
        }

        // Check if input path is inside the Assets folder
        auto [rootEnd, _] = std::mismatch(AssetManager::assetRootPath.begin(), AssetManager::assetRootPath.end(), pathToAsset.begin(), pathToAsset.end());

        if (rootEnd != AssetManager::assetRootPath.end()) {
            path = pathToAsset.string();
            throw InvalidAsset(*this, "Path not inside Assets");
        }

        // Take only path relative to Assets folder
        path = pathToAsset.lexically_relative(AssetManager::assetRootPath).string();
    }

    bool AssetID::IsValid() const {
        return std::filesystem::exists(GetSystemPath());
    }

    std::filesystem::path AssetID::GetAssetPath() const {
        return std::filesystem::path(path);
    }

    std::filesystem::path AssetID::GetSystemPath() const {
        return AssetManager::GetAssetRoot() / path;
    }

    std::string AssetID::ToString() const {
        return "Asset at " + path;
    }

    void to_json(nlohmann::json& j, AssetID const& value) {
        j = { value.path };
    }

    void from_json(nlohmann::json const& j, AssetID& value) {
        j.get_to(value.path);
    }
}
