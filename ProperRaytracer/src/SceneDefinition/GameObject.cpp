/**
 * File name: GameObject.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2023 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "GameObject.hpp"

#include "Utilities/Serializers.hpp"
#include "Scene.hpp"
#include "Utilities/ObjectFactory.hpp"
#include "Components/Transform.hpp"

namespace ProperRT
{
    GameObject::CompOwner::CompOwner(Component* obj) noexcept
        : data(obj), control(new ControlBlock)
    {}

    GameObject::CompOwner::CompOwner(CompOwner&& other) noexcept
        : data(other.data), control(other.control)
    {
        other.data = nullptr;
        other.control = nullptr;
    }

    GameObject::CompOwner::~CompOwner() noexcept {
        if (data && control) {
            control->Kill();
            if (!control->DecreaseRefCount()) {
                delete control;
            }
            delete data;
            data = nullptr;
            control = nullptr;
        }
    }

    void GameObject::CompOwner::operator=(CompOwner&& other) noexcept {
        if (data && control) {
            control->Kill();
            if (!control->DecreaseRefCount()) {
                delete control;
            }
            delete data;
        }

        data = other.data;
        control = other.control;
        other.data = nullptr;
        other.control = nullptr;
    }

    Component* GameObject::CompOwner::operator->() const noexcept { return data; }

    void from_json(nlohmann::json const& j, GameObject::CompOwner& value) {
        value = std::move(GameObject::CompOwner(
            ObjectFactory<Component::CompID, Component>::Create(
                j.at("type").get<Component::CompID>()
            )
        ));
    }

    GameObject::GameObject(Scene* createdBy, std::string_view name, GameObject* parent)
        : owningScene(createdBy), parent(parent), transform(CompOwner::Create<Components::Transform>()), name(name)
    {
        transform->_gameObject = this;
    }

    GameObject::~GameObject() {
        for (auto const& component : components) {
            component->_gameObject = nullptr;
        }
    }

    std::unique_ptr<GameObject> GameObject::Deserialize(nlohmann::json const& j) {
        std::unique_ptr<GameObject> value{ new GameObject(Scene::GetActiveScene(), j.at("name").get<std::string>()) };

        j.at("children").get_to(value->children);
        for (auto const& child : value->children) {
            child->parent = value.get();
        }

        j.at("transform").get_to(value->transform);
        value->transform->_gameObject = value.get();
        value->transform->from_json(j.at("transform"));

        j.at("components").get_to(value->components);
        int componentN = value->components.size();
        auto const& jcomps = j.at("components");
        for (int i = 0; i < componentN; ++i) {
            value->components[i]->_gameObject = value.get();
            value->components[i]->from_json(jcomps[i]);
            value->owningScene->uninitialized.push_back(value->components[i].operator->());
        }

        return value;
    }

    GameObject* GameObject::GetChild(int32_t childIndex) {
        return (childIndex < children.size()) ? children[childIndex].get() : nullptr;
    }

    CompPtr<Components::Transform> GameObject::Transform() {
        return CompPtr<Components::Transform>(transform);
    }

    CompPtr<const Components::Transform> GameObject::Transform() const {
        return CompPtr<const Components::Transform>(transform);
    }
}
