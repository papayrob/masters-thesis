/**
 * File name: Component.hpp
 * Description: Header for intuitive include of Component base class,
 *              the actual definition is located in Scene.hpp.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2023 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "nlohmann/json.hpp"

#include "SceneDefinition/Scene.hpp"

#define PRT_COMPONENT(NAME) \
private: inline static Component::Registerer<NAME> registerer{ #NAME }; \
public: inline static constexpr bool IS_COMPONENT{true}; \
public: void from_json(nlohmann::json const& j) override; \
public: NAME() : Component{} {} \
private:

#define PRT_COMPONENT_DERIVED(NAME, BASE) \
private: inline static Component::Registerer<NAME> registerer{ #NAME }; \
public: inline static constexpr bool IS_COMPONENT{true}; \
public: void from_json(nlohmann::json const& j) override; \
public: NAME() : BASE{} {} \
private:

#define PRT_COMPONENT_ABSTRACT(NAME, BASE) \
public: inline static constexpr bool IS_COMPONENT{true}; \
public: NAME() : BASE{} {} \
private:

