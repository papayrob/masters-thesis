/**
 * File name: GameObject.hpp
 * Description: Header for intuitive include of the GameObject class,
 *              the actual definition is located in Scene.hpp.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2023 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */

#include "Scene.hpp"
