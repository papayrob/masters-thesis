/**
 * File name: AssetManager.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2023 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <unordered_map>

#include "AssetID.hpp"
#include "ProperRT.hpp"

namespace ProperRT
{
    class Asset;
    class Mesh;
    class MorphMesh;
}

namespace Assimp
{
    class Importer;
}

namespace ProperRT
{
    /// @brief Class for managing assets using asset ids.
    class AssetManager
    {
    public:
        static const
        std::filesystem::path executablePath;

        static const
        std::filesystem::path assetRootPath;

    public:
        /// @brief Returns the system path to the assets folder.
        /// @return System path to the assets folder
        static
        std::filesystem::path GetAssetRoot() { return executablePath.parent_path() / "Assets"; };

        /// @brief Reads a mesh from @p assetID and loads it, unless it has already been loaded,
        /// in which case it returns the loaded mesh instead.
        /// @param assetID ID of the asset to load
        /// @param flattenGeometry Set to true if geometry should be flattened into only one submesh per material
        /// @return Loaded mesh
        static
        std::shared_ptr<Mesh> LoadMesh(AssetID const& assetID, bool flattenGeometry = false);

        /// @brief Reads a mesh from @p assetID and loads it, unless it has already been loaded,
        /// in which case it returns the loaded mesh instead.
        /// @param assetID ID of the asset to load
        /// @param importer Importer to use
        /// @param flattenGeometry Set to true if geometry should be flattened into only one submesh per material
        /// @return Loaded mesh
        static
        std::shared_ptr<Mesh> LoadMesh(AssetID const& assetID, Assimp::Importer& importer, bool flattenGeometry = false);

        /// @brief Reads an animation file from @p assetID and loads listed meshes.
        /// If some have already been loaded, it does not load them again and returns the loaded ones.
        /// @param assetID ID of the asset to load
        /// @param flattenGeometry Set to true if geometry should be flattened into only one submesh per material
        /// @return Loaded mesh
        static
        std::shared_ptr<MorphMesh> LoadMorphAnim(AssetID const& assetID, bool flattenGeometry = false);

        /// @brief Reads an animation file from @p assetID and loads listed meshes.
        /// If some have already been loaded, it does not load them again and returns the loaded ones.
        /// @param assetID ID of the asset to load
        /// @param importer Importer to use
        /// @param flattenGeometry Set to true if geometry should be flattened into only one submesh per material
        /// @return Loaded mesh
        static
        std::shared_ptr<MorphMesh> LoadMorphAnim(AssetID const& assetID, Assimp::Importer& importer, bool flattenGeometry = false);

        /// @brief Returns a loaded asset or nullptr if there is no asset loaded corresponding to @p assetID.
        /// @param assetID ID of the asset to retrieve
        /// @return Loaded asset corresponding to assetID or nullptr
        static
        std::shared_ptr<Asset> GetAsset(AssetID const& assetID);

    private:
		static inline
        std::unordered_map<AssetID, std::weak_ptr<Asset>, AssetID::Hash> assets{};
    };

}
