/**
 * File name: InvalidAsset.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2023 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <exception>
#include <sstream>

#include "AssetID.hpp"

namespace ProperRT
{
    /// @brief Exception class, signals trying to load an invalid asset (invalid path or the asset itself).
    class InvalidAsset : public std::runtime_error
    {
    public:
        InvalidAsset(AssetID const& assetID, std::string_view message)
            : std::runtime_error(Format(assetID, message)), cause(assetID)
        {}

        /// @brief Asset id that caused this exception.
        AssetID cause;

    private:
        static std::string Format(AssetID const& assetID, std::string_view message) {
            std::stringstream ss;
            ss << "<Asset " << assetID.ToString() << ">::<" << message << ">";
            return ss.str();
        }
    };
}
