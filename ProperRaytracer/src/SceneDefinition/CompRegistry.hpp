/**
 * File name: CompRegistry.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2023 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <unordered_set>
#include <ranges>

#include "SceneDefinition/Scene.hpp"
#include "Component.hpp"

namespace ProperRT
{
    /// @brief Class for storing and retrieving a list of component instances.
    /// @tparam TComponent Type of the component to store.
    template<ComponentType TComponent>
    class CompRegistry
    {
    public:

        /// @brief Registers the component instance.
        /// @param comp Component instance to register
        void Register(CompPtr<TComponent> comp) {
            items.insert(comp);
        }

        /// @brief Removes the component instance from the registry.
        /// @param comp Component instance to remove
        void UnRegister(CompPtr<TComponent> const& comp) {
            items.erase(comp);
        }

        /// @brief Returns a filtered view of instances belonging to the currently active scene.
        /// @return List of instances belonging to the currently active scene
        auto GetItems() {
            return std::ranges::filter_view(items, [](CompPtr<TComponent> const& comp) { return comp->GetGameObject()->GetScene() == Scene::GetActiveScene(); });
        }

        /// @brief Removes all instances from the registry.
        void UnRegisterAll() {
            items.clear();
        }

    private:
        std::unordered_set<CompPtr<TComponent>, typename CompPtr<TComponent>::Hash> items{};
    };
}
