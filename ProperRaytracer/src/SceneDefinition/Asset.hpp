/**
 * File name: Asset.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

namespace ProperRT
{
    /// @brief Base class for engine assets.
    class Asset
    {
    public:
        virtual
        ~Asset() = default;
    
    protected:
        Asset() noexcept = default;
    };
}