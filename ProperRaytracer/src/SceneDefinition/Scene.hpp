/**
 * File name: Scene.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2022 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <vector>
#include <memory>
#include <filesystem>
#include <unordered_set>
#include <type_traits>
#include <stack>
#include <string_view>
#include "nlohmann/json.hpp"

#include "AssetManager.hpp"
#include "Geometry/Triangle.hpp"
#include "Geometry/Mesh.hpp"
#include "Utilities/ObjectFactory.hpp"
#include "Utilities/NullReferenceException.hpp"
#include "Utilities/Logger.hpp"
#include "Utilities/IsCVQualified.hpp"

namespace ProperRT
{
	class RTApp;
}

namespace ProperRT
{
    class Scene;
    class GameObject;
    class Component;

    namespace Components
    {
        class Transform;
    }

    template<class T>
    concept ComponentType = T::IS_COMPONENT || std::same_as<std::remove_cvref_t<T>, Components::Transform>;

    template<class T>
    concept AddComponentType = (ComponentType<T> && !std::same_as<T, Components::Transform> && !CVQualifiedType<T>);

    template<ComponentType TComponent>
    class CompPtr;

    /// @brief Base class for components.
    class Component
    {
        friend class GameObject;

    public:
        inline static constexpr bool IS_COMPONENT{true};
        
        /// @brief Type with which component types are referenced in serialization.
        using CompID = std::string;

        /// @brief Default ctor.
        Component() noexcept;

        Component(Component const& other) = delete;
        Component& operator=(Component const& other) = delete;

        Component(Component&& other) = delete;
        Component& operator=(Component&& other) = delete;

        /// @brief Default virtual dtor.
        virtual
        ~Component() = default;

        /// @brief Deserializes this component from @p j.
        /// @param j JSON to deserialize from
        virtual
        void from_json(nlohmann::json const& j) = 0;

        /// @brief Awake method, called when the component is created.
        virtual
        void Awake() {}

        /// @brief Start method, called at the start of the next frame after being created.
        virtual
        void OnStart() {}

        /// @brief Update method, called every frame.
        virtual
        void Update() {}

        /// @brief UI draw method, called when UI is being drawn (ImGui functions can be used here).
        virtual
        void OnUIDraw() {}

        /// @brief Post render method, called after rendering each frame.
        virtual
        void OnPostRender() {}

        virtual
        void OnDestroy() {}

        /// @brief The returned value determines if this component gets updated during a snapshot.
        /// @details Override this method and return true if you want to allow this component to be updated
        /// during a snapshot. A snapshot happens when the frame tracer is running.
        virtual
        bool AllowDuringSnapshot() { return false; }

        /// @brief Returns the game object this component is attached to, or nullptr if the attached object was destroyed.
        inline
        GameObject* GetGameObject() { return _gameObject; }

        /// @brief Returns the game object this component is attached to, or nullptr if the attached object was destroyed.
        inline
        GameObject const* GetGameObject() const { return _gameObject; }

        template<AddComponentType TComponent>
        CompPtr<TComponent> AddComponent();

        template<ComponentType TComponent>
        CompPtr<TComponent> GetComponent();

        template<ComponentType TComponent>
        CompPtr<TComponent const> GetComponent() const;

    protected:
        /// @brief Class for static registration of component types.
        /// @tparam TComponent Component type to register
        template<class TComponent>
        struct Registerer
        {
            /// @brief Registers component of type @p TComponent with @p key.
            /// @param key Type id of the component
            Registerer(CompID const& key) {
                ObjectFactory<CompID, Component>::Register<TComponent>(key);
            }
        };

    private:
        GameObject* _gameObject{ nullptr };

    };

    /// @brief Class for representing scene objects in hierarchy, serves as a container for components. Always has a transform component.
    class GameObject
    {
        friend class Component;
        template<ComponentType TComponent>
        friend class CompPtr;
        friend class Scene;

    private:
        class CompOwner;

    public:

        GameObject(GameObject const& other) = delete;
        GameObject(GameObject&& other) = delete;

        ~GameObject();

        static
        std::unique_ptr<GameObject> Deserialize(nlohmann::json const& j);

        /// @brief Adds a component of type @p T and returns a pointer to it.
        /// @tparam T Type of the component to add
        /// @return Ptr to added component
        template<AddComponentType TComponent>
        CompPtr<TComponent> AddComponent();

        template<AddComponentType TComponent>
        CompPtr<TComponent> AddComponentNoAwake();

        /// @brief Returns a ptr to the first component of type @p T from this game object.
        /// @tparam T Type of the component to add
        /// @return Ptr to component of type @p T
        template<ComponentType TComponent>
        CompPtr<TComponent> GetComponent();

        /// @brief Returns a const ptr to the first component of type @p T from this game object.
        /// @tparam T Type of the component to add
        /// @return Const ptr to component of type @p T
        template<ComponentType TComponent>
        CompPtr<const TComponent> GetComponent() const;

        Scene* GetScene() const { return owningScene; }

        std::string_view Name() const { return name; }

        /// @brief Returns a ptr to the parent game object, or nullptr if the current GO is a root GO.
        /// @return Ptr to the parent game object or nullptr if the current GO is a root GO
        GameObject* GetParent() { return parent; }

        /// @brief Returns a const ptr to the parent game object, or nullptr if the current GO is a root GO.
        /// @return Const ptr to the parent game object or nullptr if the current GO is a root GO
        GameObject const* GetParent() const { return parent; }

        GameObject* GetChild(int32_t childIndex);

        int32_t GetChildCount() const { return children.size(); }

        /// @brief Returns a ptr to the transform component of this game object, which is always present.
        /// @return Ptr to the transform component of this game object, which is always present.
        CompPtr<Components::Transform> Transform();

        /// @brief Returns a const ptr to the transform component of this game object, which is always present.
        /// @return Const ptr to the transform component of this game object, which is always present.
        CompPtr<Components::Transform const> Transform() const;

    private:
        // Control block for component pointers.
        struct ControlBlock;

        // Component pointer that owns the allocated object, analogous to std::unique_ptr<Component>
        class CompOwner
        {
            template<ComponentType TComponent>
            friend class CompPtr;
        public:

            CompOwner() = default;

            CompOwner(Component* obj) noexcept;

            CompOwner(CompOwner const&) = delete;

            CompOwner(CompOwner&& other) noexcept;

            ~CompOwner() noexcept;

            // Allocates a new component and returns its owner pointer
            template<ComponentType TComponent>
            static
            CompOwner Create();

            void operator=(CompOwner const&) = delete;

            void operator=(CompOwner&& other) noexcept;

            Component* operator->() const noexcept;

            friend
            void from_json(nlohmann::json const& j, CompOwner& value);

        private:
            Component* data{ nullptr };
            ControlBlock* control{ nullptr };
        };

        friend
        void from_json(nlohmann::json const& j, CompOwner& value);

    private:
        Scene* owningScene;
        GameObject* parent;
        std::vector<std::unique_ptr<GameObject>> children{};

        CompOwner transform;
        std::vector<CompOwner> components{};

        std::string name;

        GameObject(Scene* createdBy, std::string_view name, GameObject* parent = nullptr);
    };

    /// @brief Class for representing (weak) pointers to components, similar to std::weak_ptr, but without thread-safety and no need to lock the resource.
    /// @detail The pointer can be safely stored and after the pointed to
    /// object is destroyed, the class returns nullptr/throws an exception when dereferencing.
    /// @tparam TComponent The type of the component the pointer is pointing to
    template<ComponentType TComponent>
    class CompPtr
    {
        template<ComponentType TOther>
        friend class CompPtr;

    public:
        /// @brief Constructs a weak component pointer from the component owner.
        /// @detail If the component is not of the correct type or the owner is empty, the constructed
        /// weak pointer will be empty.
        /// @param owner Component owner
        CompPtr(GameObject::CompOwner const& owner) noexcept;

        /// @brief Constructs an empty pointer.
        CompPtr() noexcept;

        /// @brief Constructs an empty pointer.
        CompPtr(std::nullptr_t) noexcept;

        CompPtr(TComponent* component);

        /// @brief Copy ctor.
        CompPtr(CompPtr const& other) noexcept;

        /// @brief Copy upcast ctor.
        template <ComponentType TCastFrom>
        CompPtr(CompPtr<TCastFrom> const& other) noexcept;

        /// @brief Move ctor.
        CompPtr(CompPtr&& other) noexcept;

        /// @brief Move upcast ctor.
        template <ComponentType TCastFrom>
        CompPtr(CompPtr<TCastFrom>&& other) noexcept;

        ~CompPtr();

        /// @brief Checks if component in @p owner is of the same type as the type of this class.
        /// @param owner Component owner to check against
        static inline bool DoesTypeMatch(GameObject::CompOwner const& owner) { return dynamic_cast<TComponent*>(owner.data) != nullptr; }

        template<ComponentType TCastTo>
        CompPtr<TCastTo> DynamicCast() const;

        template <ComponentType TCastTo>
            requires std::derived_from<TComponent, TCastTo>
        CompPtr<TCastTo> UpCast() const;

        CompPtr& operator=(CompPtr const& other);

        CompPtr& operator=(CompPtr&& other);

        TComponent& operator*() const { return (control != nullptr && control->IsAlive()) ? (*data) : (throw NullReferenceException("Dereferencing null component pointer")); }

        TComponent* operator->() const { return (control != nullptr && control->IsAlive()) ? data : nullptr; }

        friend
        bool operator==(CompPtr<TComponent> const& lhs, CompPtr<TComponent> const& rhs) { return lhs.data == rhs.data; }

        bool operator==(std::nullptr_t) { return control == nullptr || !control->IsAlive(); }

    private:
        TComponent* data;
        GameObject::ControlBlock* control;

        GameObject::CompOwner const& FindOwner(TComponent* component);

    public:
        struct Hash
        {
            std::size_t operator()(CompPtr const& compPtr) const {
                return std::hash<TComponent*>{}(compPtr.data);
            }
        };

    };

    class SceneConfig
    {
    public:
        SceneConfig() noexcept = default;

        explicit
        SceneConfig(nlohmann::json&& configJson, SceneConfig const* backupConfig_ = nullptr)
            : config(std::move(configJson)) /*must be () braces, otherwise interpreted as initializer list*/, backupConfig{ backupConfig_ }
        {}

        template<class T>
        T GetValue(std::string const& key, T defaultValue = T{}) const {
            TryGetValue(key, defaultValue);
            return defaultValue;
        }

        template<class T>
        bool TryGetValue(std::string const& key, T& value) const {
            if (config.contains(key)) {
                try {
                    config.at(key).get_to(value);
                }
                catch(const std::exception& e) {
                    //TODO logerror
                    return false;
                }
                
                return true;
            }
            else if (backupConfig != nullptr) {
                return backupConfig->TryGetValue(key, value);
            }
            
            Logger::LogInfo("Failed to retrieve config value " + key);
            return false;
        }
    private:
        nlohmann::json config{};
        SceneConfig const* backupConfig{};
    };

	/// @brief Class for representing a scene with objects and lights.
	class Scene
	{
		friend Component;
		friend GameObject;

	public:
        inline static
        std::string startingSceneName{ "Default.scene" };

		/// @brief Returns a pointer to the currently active scene.
		static
        Scene* GetActiveScene() {
			return activeScene;
		}

		/// @brief Changes the active scene based on scene name.
        /// @param name Name of the scene to activate
        /// @returns True if the scene was loaded and activated, false otherwise
        static
        bool SetActiveScene(std::string const& name);

        /// @brief Loads a scene from assets.
        /// @param assetID ID of the scene asset to load
        /// @param setActive If set to true, the scene will also be set to active after being loaded
        /// @param unloadActiveScene If set to true, the currently active scene will be unloaded
        static
        void LoadScene(AssetID assetID, bool setActive, bool unloadActiveScene = true);

        /// @brief Deserializes a scene from a json.
        static
        std::unique_ptr<Scene> Deserialize(nlohmann::json const& j);

	private:
        inline static
        std::unordered_map<std::string, std::unique_ptr<Scene>> loadedScenes{};

		inline static
        Scene* activeScene{ nullptr };

		inline static
        Scene* nextActiveScene{ nullptr };

		inline static
        bool unloadActive{ false };

	public:

		/// @brief Default ctor.
		Scene() = default;

		Scene(Scene const&) = delete;
        Scene& operator=(Scene const&) = delete;
        
		Scene(Scene&&) = delete;
		Scene& operator=(Scene&&) = delete;

        /// @brief Creates a game object named @p name as a child of @p parent.
        /// If @p parent is null, the game object will be added to root GOs.
        /// @param name Name of the GO to be created
        /// @param parent Parent GO under which to create the new GO
        /// @return 
        GameObject& CreateGameObject(std::string_view name, GameObject* parent = nullptr);

        /// @brief Executes @p f for each game object in this scene.
        /// @tparam F Functor type
        /// @param f Functor to execute on each game object
        template<class F>
        void ForEachGameObject(F&& f) {
            std::stack<GameObject*> stack;
            for (auto const& go : rootGameObjects) {
                stack.push(go.get());
            }

            while (!stack.empty()) {
                GameObject* go = stack.top();
                stack.pop();

                f(go);

                for (auto const& child : go->children) {
                    stack.push(child.get());
                }
            }
        }

        /// @brief Executes @p f for each component in this scene.
        /// @tparam F Functor type
        /// @param f Functor to execute on each component
        template<class F>
        void ForEachComponent(F&& f) {
            ForEachGameObject([&f](GameObject* go) {
                for (auto& comp : go->components) {
                    f(CompPtr<Component>(comp));
                }
            });
        }

        /// @brief Executes @p f for each component of type @p TComponent in this scene.
        /// @tparam TComponent Type of the component to filter
        /// @tparam F Functor type
        /// @param f Functor to execute on each component
        template<class TComponent, class F>
        void ForEachComponentOfType(F&& f) {
            ForEachComponent([&f](CompPtr<Component> comp) {
                if (auto cast = comp.DynamicCast<TComponent>(); cast != nullptr) {
                    f(cast);
                }
            });
        }

        GameObject* FindGameObject(std::string const& name) {
            GameObject* found{};
            ForEachGameObject([&name, &found](GameObject* go) { if (go->Name() == name) { found = go; } });
            return found;
        }

        std::string const& Name() { return name; }

        SceneConfig const& Config() const { return sceneConfig; }

		/// @brief Default dtor.
		~Scene() {
			if (activeScene == this) {
				activeScene = nullptr;
			}
		}


	private:

		std::string name{};
		std::vector<std::unique_ptr<GameObject>> rootGameObjects{};

        SceneConfig sceneConfig;

		std::vector<Component*> uninitialized{};

        void OnLoad();

        void OnSwitch();

	public:
        /// @brief Class for updating the scene.
		struct Updater
		{
			friend RTApp;

		public:
            bool snapshotted{ false };
            
			Updater(Scene* scene) : owner(scene) {}

		private:
			Scene* owner;
			std::vector<GameObject*> gos;

			static void SwitchScenes();

			void PreRenderUpdate();

            void UIUpdate();

			void PostRenderUpdate();
		};
	};

    struct GameObject::ControlBlock
    {
        static inline constexpr
        uint32 ALIVE_MASK = 1U << 31;

        static inline constexpr
        uint32 COUNT_MASK = ~(1U << 31);

        
        std::atomic<uint32> data{ ALIVE_MASK + 1 };

        uint32 RefCount() const noexcept { return data.load() & COUNT_MASK; }

        void IncreaseRefCount() noexcept { ++data; }

        bool DecreaseRefCount() noexcept { --data; return RefCount() > 0U; }

        bool IsAlive() const noexcept { return static_cast<bool>(data.load() & ALIVE_MASK); }

        void Kill() noexcept { data &= COUNT_MASK; }
    };

    template<ComponentType TComponent>
    GameObject::CompOwner GameObject::CompOwner::Create() {
        CompOwner co{};
        co.data = dynamic_cast<Component*>(new TComponent);
        co.control = new ControlBlock;
        return co;
    }
    
    template<AddComponentType TComponent>
    CompPtr<TComponent> GameObject::AddComponentNoAwake() {
        components.push_back(CompOwner::Create<TComponent>());
        components.back()->_gameObject = this;
        owningScene->uninitialized.push_back(components.back().operator->());
        return CompPtr<TComponent>(components.back());
    }

    template<AddComponentType TComponent>
    CompPtr<TComponent> GameObject::AddComponent() {
        auto comp = AddComponentNoAwake<TComponent>();
        comp->Awake();
        return comp;
    }

    template<AddComponentType TComponent>
    CompPtr<TComponent> Component::AddComponent() {
        return GetGameObject()->AddComponent<TComponent>();
    }

    template<ComponentType TComponent>
    CompPtr<TComponent> GameObject::GetComponent() {
        using CompTypeNoConst = std::remove_const_t<TComponent>;

        if constexpr (std::is_same_v<CompTypeNoConst, Components::Transform>) {
            return CompPtr<TComponent>(transform);
        }
        else {
            for (auto& component : components) {
                if (CompPtr<CompTypeNoConst>::DoesTypeMatch(component)) {
                    return CompPtr<TComponent>(component);
                }
            }

            return nullptr;
        }
    }

    template<ComponentType TComponent>
    CompPtr<TComponent const> GameObject::GetComponent() const {
        using CompTypeNoConst = std::remove_const_t<TComponent>;

        if constexpr (std::is_same_v<CompTypeNoConst, Components::Transform>) {
            return CompPtr<TComponent const>(transform);
        }
        else {
            for (auto& component : components) {
                if (CompPtr<CompTypeNoConst>::DoesTypeMatch(component)) {
                    return CompPtr<TComponent const>(component);
                }
            }
            return nullptr;
        }
    }

    template<ComponentType TComponent>
    CompPtr<TComponent> Component::GetComponent() {
        return GetGameObject()->GetComponent<TComponent>();
    }

    template<ComponentType TComponent>
    CompPtr<TComponent const> Component::GetComponent() const {
        return GetGameObject()->GetComponent<TComponent const>();
    }

    template<ComponentType TComponent>
    CompPtr<TComponent>::CompPtr(GameObject::CompOwner const& owner) noexcept
        : data(dynamic_cast<TComponent*>(owner.data)), control(owner.control)
    {
        if (data == nullptr) {
            control = nullptr;
        }
        else {
            control->IncreaseRefCount();
        }
    }

    template<ComponentType TComponent>
    CompPtr<TComponent>::CompPtr() noexcept
        : data(nullptr), control(nullptr)
    {}

    template<ComponentType TComponent>
    CompPtr<TComponent>::CompPtr(std::nullptr_t) noexcept
        : data(nullptr), control(nullptr)
    {}

    template<ComponentType TComponent>
    CompPtr<TComponent>::CompPtr(TComponent* component)
        : CompPtr(FindOwner(component))
    {}

    template<ComponentType TComponent>
    CompPtr<TComponent>::CompPtr(CompPtr const& other) noexcept
        : data(other.data), control(other.control)
    {
        if (control != nullptr) {
            control->IncreaseRefCount();
        }
    }

    template<ComponentType TComponent>
    template<ComponentType TCastFrom>
    CompPtr<TComponent>::CompPtr(CompPtr<TCastFrom> const& other) noexcept
        : data(static_cast<TComponent*>(other.data)), control{ other.control }
    {
        if (control != nullptr) {
            control->IncreaseRefCount();
        }
    }

    template<ComponentType TComponent>
    CompPtr<TComponent>::CompPtr(CompPtr&& other) noexcept
        : data(other.data), control(other.control)
    {
        other.data = nullptr;
        other.control = nullptr;
    }

    template<ComponentType TComponent>
    template<ComponentType TCastFrom>
    CompPtr<TComponent>::CompPtr(CompPtr<TCastFrom> && other) noexcept
        : data(static_cast<TComponent*>(other.data)), control{ other.control }
    {
        other.data = nullptr;
        other.control = nullptr;
    }

    template<ComponentType TComponent>
    CompPtr<TComponent>::~CompPtr() {
        if (control != nullptr && !control->DecreaseRefCount()) {
            delete control;
        }

        data = nullptr;
        control = nullptr;
    }

    template <ComponentType TComponent>
    template <ComponentType TCastTo>
    CompPtr<TCastTo> CompPtr<TComponent>::DynamicCast() const {
        CompPtr<TCastTo> ptr{};
        if (TCastTo* castData = dynamic_cast<TCastTo*>(data); castData != nullptr) {
            ptr.data = castData;
            ptr.control = control;
            control->IncreaseRefCount();
        }
        return ptr;
    }

    template <ComponentType TComponent>
    template <ComponentType TCastTo>
        requires std::derived_from<TComponent, TCastTo>
    CompPtr<TCastTo> CompPtr<TComponent>::UpCast() const {
        CompPtr<TCastTo> ptr;
        ptr.data = static_cast<TCastTo*>(data);
        ptr.control = control;
        if (control != nullptr) {
            control->IncreaseRefCount();
        }
        return ptr;
    }

    template<ComponentType TComponent>
    CompPtr<TComponent>& CompPtr<TComponent>::operator=(CompPtr const& other) {
        if (control != nullptr && !control->DecreaseRefCount()) {
            delete control;
        }

        data = other.data;
        control = other.control;

        if (control != nullptr) {
            control->IncreaseRefCount();
        }

        return *this;
    }

    template<ComponentType TComponent>
    CompPtr<TComponent>& CompPtr<TComponent>::operator=(CompPtr&& other) {
        if (control != nullptr && !control->DecreaseRefCount()) {
            delete control;
        }

        data = other.data;
        control = other.control;

        other.data = nullptr;
        other.control = nullptr;

        return *this;
    }

    template<ComponentType TComponent>
    GameObject::CompOwner const& CompPtr<TComponent>::FindOwner(TComponent* component) {
        GameObject* go = component->GetGameObject();
        //TODO throw if go is nullptr
        for (auto const& compOwner : go->components) {
            if (compOwner.data == component) {
                return compOwner;
            }
        }
        //TODO throw not found
        throw std::exception();
    }
}
