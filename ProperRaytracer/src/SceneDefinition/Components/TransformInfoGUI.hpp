/**
 * File name: TransformInfoGUI.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "SceneDefinition/Component.hpp"

namespace ProperRT::Components
{
    /// @brief Component for displaying infromation about transfrom position in UI.
    class TransformInfoGUI : public Component
    {
        PRT_COMPONENT(TransformInfoGUI)

    public:
        virtual
        void OnStart() override;

        virtual
        void OnUIDraw() override;

        virtual
        bool AllowDuringSnapshot() override { return true; }

    private:
        std::string windowName;
    };
}
