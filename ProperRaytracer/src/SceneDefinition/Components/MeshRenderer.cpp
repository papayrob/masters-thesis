/**
 * File name: MeshRenderer.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2023 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "MeshRenderer.hpp"

#include <algorithm>
#include <numeric>

#include "RTApp.hpp"
#include "Transform.hpp"
#include "Graphics/Raytracing/GPUMeshManager.cuh"

namespace ProperRT::Components
{
	void MeshRenderer::from_json(nlohmann::json const& j) {
		// Load asset
		j.at("assetID").get_to(assetID);
		mesh = AssetManager::LoadMesh(assetID, true);
		submeshIndeces.resize(mesh->GetPrimitiveCounts().size());//TODO
		std::iota(submeshIndeces.begin(), submeshIndeces.end(), 0);
		mesh->SyncToGPU();
	}

    void MeshRenderer::Awake() {
        auto cp = CompPtr(this);
		if (RTApp::GetRenderingMode() == RTApp::RenderingMode::OpenGL) {
			RTApp::GetRasterizer()->RegisterRenderer(cp);
		}
		RTApp::GetRaytracer()->RegisterRenderer(cp);
    }

	Primitive MeshRenderer::GetPrimitiveLocal(int32 submeshIdxInRenderer, int32 primitiveIdx) {
		return mesh->GetPrimitive(submeshIndeces[submeshIdxInRenderer], primitiveIdx);
    }

	Primitive MeshRenderer::GetPrimitive(int32 submeshIdxInRenderer, int32 primitiveIdx) {
		return mesh->GetPrimitive(submeshIndeces[submeshIdxInRenderer], primitiveIdx, GetComponent<Transform>()->ObjectToWorldMatrix());
    }

    std::vector<Renderer::SubmeshInfo> MeshRenderer::GetSubmeshesInfo() {
        std::vector<Renderer::SubmeshInfo> out(submeshIndeces.size());
		std::transform(submeshIndeces.begin(), submeshIndeces.end(), out.begin(),
			[this](int32 submeshIdx) { return Renderer::SubmeshInfo{ submeshIdx, mesh->GetPrimitiveCount(submeshIdx) }; }
		);
		return out;
    }

    void MeshRenderer::CreateSubmeshInstances(std::span<GPUMeshManagement::Instance> instances) {
		auto submeshN = std::min(StdSize<int32>(instances), StdSize<int32>(submeshIndeces));
		for (int32 i = 0; i < submeshN; ++i) {
			mesh->FillSubmeshInstance(instances[i], submeshIndeces[i]);
			instances[i].transform = GetComponent<Transform>()->ObjectToWorldMatrix();
			instances[i].material = mesh->Materials()[submeshIndeces[i]];
			instances[i].submeshIdx = submeshIndeces[i];
		}
    }

    void MeshRenderer::Rasterize(RasterizationMatrices const &matrices, bool bind)
    {
        mesh->Rasterize(GetComponent<Transform>()->ObjectToWorldMatrixf(), matrices, submeshIndeces, bind);
    }
}
