/**
 * File name: RaycastMove.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "SceneDefinition/Component.hpp"

namespace ProperRT::Components
{
    /// @brief Component that moves the an object called "HitPrefab" to the hit of a raycast after pressing the preiod key.
    class RaycastMove : public Component
    {
        PRT_COMPONENT(RaycastMove)

    public:
        void Update() override;
    };
}
