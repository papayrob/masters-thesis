/**
 * File name: MorphRenderer.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2023 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "SceneDefinition/Components/Renderer.hpp"
#include "Geometry/MorphMesh.hpp"
#include "MorphController.hpp"

namespace ProperRT::Components
{
    /// @brief Component for referencing and rendering a list of meshes used for morph animation.
    class MorphRenderer : public Renderer
    {
        PRT_COMPONENT_DERIVED(MorphRenderer, Renderer)

    public:
        /// @brief Rendered morph mesh.
        std::shared_ptr<MorphMesh> mesh;
        /// @brief List of submesh indeces the renderer renders.
        std::vector<int32_t> submeshIndeces;
        /// @brief Animation controller component
        CompPtr<MorphController> controller;

        void Awake() override;

        void Update() override;

        /// @brief Returns the info on which meshes to blend and how much to blend them.
        /// @details Updated once per frame.
        MorphMesh::MeshBlendInfo GetMeshBlendInfo() const { return cachedInfo; }

        virtual
        Primitive GetPrimitiveLocal(int32_t submeshIdxInRenderer, int32_t primitiveIdx) override;

        virtual
        Primitive GetPrimitive(int32_t submeshIdxInRenderer, int32_t primitiveIdx) override;

        virtual
        int32_t GetSubmeshCount() override { return StdSize<int32>(submeshIndeces); }

        virtual
        SubmeshInfo GetSubmeshInfo(int32_t idx) override { return SubmeshInfo{ submeshIndeces.at(idx), mesh->GetPrimitiveCount(submeshIndeces.at(idx)) }; }

        virtual
        std::vector<Renderer::SubmeshInfo> GetSubmeshesInfo() override;

        virtual
        std::shared_ptr<GenericMesh> GetMesh() override { return mesh; }

        virtual
        void CreateSubmeshInstances(std::span<GPUMeshManagement::Instance> instances) override;

        virtual
        void Rasterize(RasterizationMatrices const& matrices, bool bind = true) override;

    private:
        MorphMesh::MeshBlendInfo cachedInfo;

        MorphMesh::MeshBlendInfo ComputeMeshBlendInfo() const;
    };
}
