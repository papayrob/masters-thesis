/**
 * File name: Camera.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2023 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "Camera.hpp"

#include "glbinding/gl/functions.h"
#include "Graphics/glfwInclude.hpp"

#include "Utilities/Serializers.hpp"
#include "Transform.hpp"

namespace ProperRT::Components
{
    void Camera::from_json(nlohmann::json const& j) {
        j.at("verticalFOV").get_to(verticalFOV);
        verticalFOV = ToRadians(verticalFOV);
        j.at("nearPlane").get_to(nearPlane);
        j.at("farPlane").get_to(farPlane);
        j.at("viewport").get_to(viewport);
        j.at("background").get_to(backgroundColor);

        //TODO: throw on invalid values
    }

    Vector2i Camera::Viewport() const {
        return viewport;
    }

    RasterizationMatrices Camera::GetCameraMatrices() const {
        auto transform = GetComponent<Transform>();
        auto const& cameraMat = transform->ObjectToWorldMatrix();

        RasterizationMatrices matrices{
            Matrix4x4f::FromColumns({
                Matrix4x4f::ColumnVector{ static_cast<Vector<3,float>>(cameraMat[0].Normalized()), 0.f },
                Matrix4x4f::ColumnVector{ static_cast<Vector<3,float>>(cameraMat[1].Normalized()), 0.f },
                Matrix4x4f::ColumnVector{ static_cast<Vector<3,float>>(cameraMat[2].Normalized()), 0.f },
                Matrix4x4f::ColumnVector{ 0.f, 0.f, 0.f, 1.f },
            }).Transposed()
            *
            TranslationMatrix(static_cast<Vector<3,float>>(-transform->GetPositionWorld())),
            PerspectiveMatrix(nearPlane, farPlane, verticalFOV, static_cast<float>(GetAspectRatio()))
        };

        return matrices;
    }

    void Camera::OnStart() {
        SetMainCamera(CompPtr{this});
    }

    void Camera::OnDestroy() {
        SetMainCamera(nullptr);
    }

    void Camera::SetWindowSize(Vector2i size) {
        if (RTApp::ShowGUI()) {
            RTApp::Instance()->GetMainWindow()->setSize(static_cast<size_t>(size.x()), static_cast<size_t>(size.y()));
        }
        //mainCamera->viewport = size;
    }

    CompPtr<Camera const> Camera::MainCamera() {
        return mainCamera;
    }

    void Camera::SetMainCamera(CompPtr<Camera> camera) {
        mainCamera = camera;
        if (camera != nullptr) {
            SetWindowSize(camera->Viewport());
        }
    }

    void Camera::WindowSizeChangeCallback(vkfw::Window const &, size_t width, size_t height) {
        gl::glViewport(0, 0, width, height);
        mainCamera->viewport = Vector2i{ static_cast<int32>(width), static_cast<int32>(height) };
    }
}
