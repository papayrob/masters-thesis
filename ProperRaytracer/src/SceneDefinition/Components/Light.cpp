/**
 * File name: Light.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "Light.hpp"

#include "Utilities/Serializers.hpp"
#include "Transform.hpp"

namespace ProperRT::Components
{
    void Light::from_json(nlohmann::json const& j) {
        auto storedColor = j.at("color").get<Vector3sf>();
        auto intensity = j.at("intensity").get<sfloat>();

        color = Color32f{ storedColor, intensity };

        lightType = ToLightType(j.at("lightType").get<std::string>());
        switch (lightType)
        {
        case Type::Directional:
            break;
        case Type::Point: {
            j.at("range").get_to(range);
            break;
        }
        case Type::Spotlight: {
            j.at("range").get_to(range);
            j.at("angle").get_to(angle);
            break;
        }
        default:
            break;
        }

        //TODO: throw on invalid values
    }

    void Light::Awake() {
        lightRegistry.Register(CompPtr<Light>(this));
    }
    
    Light::DirectionData Light::DirectToLight(Vector3cf const &source) const {
        return GetGPUData().DirectToLight(source);
    }

    Light::GPUData Light::GetGPUData() const {
        auto transform = GetComponent<Transform>();
        return GPUData{
            lightType,
            Vector3sf{ transform->GetPositionWorld() },
            Vector3sf{ transform->ForwardWorld() },
            range,
            angle,
            color
        };
    }
}
