/**
 * File name: Model.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2023 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "SceneDefinition/AssetID.hpp"
#include "SceneDefinition/Component.hpp"

namespace ProperRT::Components
{
    /// @brief Component representing an unserializable model (hierarchy of meshes).
    class Model : public Component
    {
        PRT_COMPONENT(Model)

    protected:
        AssetID assetID;
    };
}
