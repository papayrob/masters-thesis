/**
 * File name: BasicFlight.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2023 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "BasicFlight.hpp"

#include "RTApp.hpp"
#include "Transform.hpp"
#include "InputSystem/Input.hpp"

namespace ProperRT::Components
{
    void BasicFlight::from_json(nlohmann::json const& j) {
        j.at("speed").get_to(speed);
        j.at("sensitivity").get_to(sensitivity);
    }

    void BasicFlight::Update() {
        // Hide or show the cursor based on if the user is moving the camera (=holding right click)
        if (Input::GetKeyState(vkfw::MouseButton::eRight).HasPressed()) {
            Input::HideCursor();
            // Save current cursor position
            lastCursorPos = Input::GetCursorPosition();
        }
        else if (Input::GetKeyState(vkfw::MouseButton::eRight).HasReleased()) {
            Input::ShowCursor();
        }

        // Move only when the user is holding the right click
        if (Input::GetKeyState(vkfw::MouseButton::eRight).IsPressed()) {
            auto transform = GetGameObject()->GetComponent<Transform>();

            // Rotate the camera based on mouse movement
            auto currCursorPos = Input::GetCursorPosition();
            auto change = currCursorPos - lastCursorPos;
            lastCursorPos = currCursorPos;

            auto rot = transform->GetRotationLocal();
            rot.y() += -sensitivity * change.x();
            rot.x() += -sensitivity * change.y();
            transform->ChangeRotationLocal(rot);

            // Add to flight direction based on pressed keys
            Vector3cf dir{ 0._cf };

            if (Input::GetKeyState(vkfw::Key::eW).IsPressed()) {
                dir += transform->ForwardWorld();
            }

            if (Input::GetKeyState(vkfw::Key::eS).IsPressed()) {
                dir -= transform->ForwardWorld();
            }

            if (Input::GetKeyState(vkfw::Key::eA).IsPressed()) {
                dir -= transform->RightWorld();
            }

            if (Input::GetKeyState(vkfw::Key::eD).IsPressed()) {
                dir += transform->RightWorld();
            }

            if (Input::GetKeyState(vkfw::Key::eSpace).IsPressed()) {
                dir += transform->UpWorld();
            }

            if (Input::GetKeyState(vkfw::Key::eLeftControl).IsPressed()) {
                dir -= transform->UpWorld();
            }

            // Normalize the flight direction and change the camera transform position based on it
            dir.NormalizeSafe();
            if (Input::GetKeyState(vkfw::Key::eLeftShift).IsPressed()) {
                dir *= 2;
            }
            transform->ChangePositionWorld(transform->GetPositionWorld() + RTApp::GetTimeDelta() * speed * dir);

        }

    }

}
