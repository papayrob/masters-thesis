/**
 * File name: Model.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2023 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "Model.hpp"

#include "assimp/scene.h"
#include "assimp/postprocess.h"
#include "assimp/Importer.hpp"

#include "SceneDefinition/InvalidAsset.hpp"
#include "SceneDefinition/GameObject.hpp"
#include "Geometry/Mesh.hpp"
#include "SceneDefinition/Scene.hpp"
#include "MeshRenderer.hpp"
#include "Transform.hpp"

namespace ProperRT::Components
{
	namespace
	{
		void ProcessNode(std::shared_ptr<Mesh> const& mesh, aiNode const* modelNode, GameObject& current) {
			// Initialize mesh renderer for all node submeshes
			if (modelNode->mNumMeshes > 0) {
				auto renderer = current.AddComponentNoAwake<MeshRenderer>();
				renderer->mesh = mesh;
				renderer->submeshIndeces.reserve(modelNode->mNumMeshes);
				for (unsigned int meshI = 0U; meshI < modelNode->mNumMeshes; ++meshI) {
					renderer->submeshIndeces.push_back(modelNode->mMeshes[meshI]);
				}
				renderer->Awake();
			}

			// Set transform
			aiVector3D position, rotation, scale;
			modelNode->mTransformation.Decompose(scale, rotation, position);
			auto transform = current.GetComponent<Transform>();

			auto convert = [](aiVector3D const& vec) -> Transform::Vector3 {
				return Transform::Vector3{ vec.x, vec.y, vec.z };
			};
			transform->ChangeAllLocal(convert(position), convert(rotation), convert(scale));

			// Process children nodes
			for (unsigned int childI = 0; childI < modelNode->mNumChildren; ++childI) {
				auto& child = Scene::GetActiveScene()->CreateGameObject(modelNode->mName.C_Str(), &current);
				ProcessNode(mesh, modelNode->mChildren[childI], child);
			}
		}
	}

	void Model::from_json(nlohmann::json const& j) {
		j.at("assetID").get_to(assetID);
		
		// Process model hierarchy
		Assimp::Importer importer{};
		auto mesh = AssetManager::LoadMesh(assetID, importer);
		mesh->SyncToGPU();

		auto& root = Scene::GetActiveScene()->CreateGameObject(importer.GetScene()->mRootNode->mName.C_Str(), GetGameObject());
		ProcessNode(mesh, importer.GetScene()->mRootNode, root);
	}
}
