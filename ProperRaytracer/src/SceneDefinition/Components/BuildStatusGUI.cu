/**
 * File name: BuildStatusGUI.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "BuildStatusGUI.hpp"

#include "imgui.h"

#include "RTApp.hpp"
#include "Graphics/Raytracing/IAccelerationStructure.hpp"
#include "Graphics/Raytracing/KDTree/KDTBinsGPUBuilder.cuh"

namespace ProperRT::Components
{
    void BuildStatusGUI::OnStart() {
    }

    void BuildStatusGUI::Update() {
        #if PRT_BUILD_STATUS
        elapsedTime += RTApp::Instance()->GetTimeDelta();
        if (elapsedTime > 1.0) {
            if (KDTreeGPU* kdgpu = dynamic_cast<KDTreeGPU*>(RTApp::GetRaytracer()->GetAccelerationStructure()); kdgpu != nullptr) {
                if (KDTBinsGPUBuilder const* builder = dynamic_cast<KDTBinsGPUBuilder const*>(kdgpu->GetDynamicBuilder()); builder != nullptr) {
                    cudaEventQuery(builder->stop);
                    tasks = builder->buildStatusHost->tasks;
                    workingSubgroups = builder->buildStatusHost->workingSubgroups;
                    depth = builder->buildStatusHost->depth;
                    nodes = builder->buildStatusHost->nodes;
                }
            }
            elapsedTime = 0.0;
        }
        #endif
    }

    void BuildStatusGUI::OnUIDraw() {
        #if PRT_BUILD_STATUS
        ImGui::Begin("Build Status");

        ImGui::LabelText("Tasks", "%d", tasks);
        ImGui::LabelText("Working Subgroups", "%d", workingSubgroups);
        ImGui::LabelText("Depth", "%d", depth);
        ImGui::LabelText("Nodes", "%d", nodes);

        ImGui::End();
        #endif
    }

    void BuildStatusGUI::from_json(nlohmann::json const& j) {}
}
