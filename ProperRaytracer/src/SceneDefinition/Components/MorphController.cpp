/**
 * File name: MorphController.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "MorphController.hpp"

#include "RTApp.hpp"

namespace ProperRT::Components
{
	void MorphController::from_json(nlohmann::json const& j) {
		j.at("animLength").get_to(animLength);
	}

	void MorphController::Update() {
		//animTime = 5.9296965999999962;
		animTime = std::fmod(animTime + RTApp::GetTimeDelta(), animLength);
	}
}
