/**
 * File name: Light.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "SceneDefinition/Component.hpp"
#include "Graphics/Color.hpp"
#include "SceneDefinition/CompRegistry.hpp"

namespace ProperRT::Components
{

    /// @brief Component representing a light.
    class Light : public Component
    {
        PRT_COMPONENT(Light)

    public:
        /// @brief Enum for light types.
        /// @details Spotlight is not implemented.
        enum class Type : int
        {
            Directional = 0, Point, Spotlight
        };

        /// @brief Returns light Type enum from a string name.
        /// @param str Name
        static
        Type ToLightType(std::string_view str) {
            if (str == "Directional") {
                return Type::Directional;
            }
            else if (str == "Point") {
                return Type::Point;
            }
            else if (str == "Spotlight") {
                return Type::Spotlight;
            }
            //TODO throw
            return Type::Directional;
        }

        /// @brief Class for storing direction data from DirectToLight method.
        struct DirectionData
        {
            /// @brief Direction pointing at the light.
            Vector3cf direction;
            /// @brief Distance to the light.
            cfloat distance;
            /// @brief Itensity calculated from the distance.
            cfloat distanceIntensity;

            /// @brief Returns true if the light is out of range.
            CUDA_FUNCTION
            bool IsOutOfRange() const { return distanceIntensity < 0.0_cf; }
        };

        /// @brief Class for storing data used on the GPU.
        struct GPUData
        {
            Type type;
            Vector3sf position;
            Vector3sf direction;
            sfloat range;
            sfloat angle;
            Color32f color;

            /// @brief Directs a vector to the light from @p source.
            /// @param source Point to direct from
            /// @return Data related to the light direction and distance
            CUDA_FUNCTION
            DirectionData DirectToLight(Vector3cf const& source) const;
        };

	public:
		/// @brief Registry with all lights.
		inline static
        CompRegistry<Light> lightRegistry;

        /// @brief Ambient color.
        inline static
        Color32f ambientColor{0.0f};

    public:
        /// @brief Light color.
        Color32f color;

        /// @brief Type of the light.
        Type lightType;

        void Awake() override;

        /// @brief Directs a vector to the light from @p source.
        /// @param source Point to direct from
        /// @return Data related to the light direction and distance
        DirectionData DirectToLight(Vector3cf const& source) const;

        /// @brief Creates and returns GPUData from this light.
        GPUData GetGPUData() const;

    private:
        sfloat range;
        sfloat angle;
    };

    CUDA_FUNCTION inline
    Light::DirectionData Light::GPUData::DirectToLight(Vector3cf const& source) const {
        switch (type)
        {
        case Type::Directional: {
            return DirectionData{
                -Vector3cf{ direction },
                CuSTD::numeric_limits<cfloat>::infinity(),
                1.0_cf
            };
        }
        case Type::Point: {
            Vector3cf dir = static_cast<Vector3cf>(position) - source;
            cfloat dist = dir.Magnitude();
            // Check if source point is not too far away or too close
            if (ZERO_COMP_EPSILON < dist && dist <= range) {
                return DirectionData{
                    dir / dist,
                    dist,
                    1.0_cf - dist / range
                };
            }
            break;
        }
        case Type::Spotlight: {
            //TODO
            break;
        }
        }

        return DirectionData{
            Vector3cf{},
            -1.0_cf,
            -1.0_cf
        };
    }
}
