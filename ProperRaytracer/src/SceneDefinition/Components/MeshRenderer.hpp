/**
 * File name: MeshRenderer.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2023 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "SceneDefinition/Components/Renderer.hpp"
#include "Geometry/Mesh.hpp"
#include "SceneDefinition/CompRegistry.hpp"

namespace ProperRT::Components
{
    /// @brief Component for referencing and rendering a mesh.
    class MeshRenderer : public Renderer
    {
        PRT_COMPONENT_DERIVED(MeshRenderer, Renderer)

    public:
        /// @brief Rendered mesh.
        std::shared_ptr<Mesh> mesh;
        /// @brief List of submesh indeces the renderer renders.
        std::vector<int32> submeshIndeces;

        void Awake() override;

        virtual
        Primitive GetPrimitiveLocal(int32 submeshIdxInRenderer, int32 primitiveIdx) override;

        virtual
        Primitive GetPrimitive(int32 submeshIdxInRenderer, int32 primitiveIdx) override;

        virtual
        int32 GetSubmeshCount() override { return StdSize<int32>(submeshIndeces); }

        virtual
        SubmeshInfo GetSubmeshInfo(int32 idx) override { return SubmeshInfo{ submeshIndeces.at(idx), mesh->GetPrimitiveCount(submeshIndeces.at(idx)) }; }

        virtual
        std::vector<Renderer::SubmeshInfo> GetSubmeshesInfo() override;

        virtual
        std::shared_ptr<GenericMesh> GetMesh() override { return mesh; }

        virtual
        void CreateSubmeshInstances(std::span<GPUMeshManagement::Instance> instances) override;

        virtual
        void Rasterize(RasterizationMatrices const& matrices, bool bind = true) override;
    };
}
