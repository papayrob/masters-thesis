/**
 * File name: StructureNavigator.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <stack>

#include "Renderer.hpp"
#include "Graphics/Raytracing/INavigator.hpp"

namespace ProperRT::Components
{
    class Renderer;

    /// @brief Component for displaying UI for navigating data structures.
    class StructureNavigator : public Component
    {
        PRT_COMPONENT(StructureNavigator)
    
    public:
        virtual
        void OnUIDraw() override;

        virtual
        void OnPostRender() override;

        virtual
        bool AllowDuringSnapshot() override { return true; }

    private:
        std::stack<INavigator*> navigatorStack;
        std::vector<CompPtr<Renderer>> enabledComponents;

        INavigator* TopNavigator();

        void Clear();
    };
}