/**
 * File name: RaycastMove.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "RaycastMove.hpp"

#include "RTApp.hpp"
#include "Graphics/Raytracing/PrimaryRayGenerator.hpp"
#include "Transform.hpp"
#include "InputSystem/Input.hpp"

namespace ProperRT::Components
{
    void RaycastMove::from_json(nlohmann::json const& j) {
    }

    void RaycastMove::Update() {
        if (Input::GetKeyState(vkfw::Key::ePeriod).HasReleased()) {
            PrimaryRayGenerator rayGen{ Camera::MainCamera() };
            auto hitOptional = RTApp::GetRaytracer()->CastRay(rayGen.GetRay(Camera::MainCamera()->Viewport().x() / 2, Camera::MainCamera()->Viewport().y() / 2));
            if (hitOptional.has_value()) {
                if (GameObject* hitPrefab = Scene::GetActiveScene()->FindGameObject("HitPrefab"); hitPrefab != nullptr) {
                    hitPrefab->GetComponent<Transform>()->ChangePositionWorld(hitOptional->point);
                }
            }
        }
    }

}
