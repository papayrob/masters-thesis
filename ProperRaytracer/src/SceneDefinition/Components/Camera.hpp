/**
 * File name: Camera.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2023 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <numbers>

#include "SceneDefinition/Component.hpp"
#include "Geometry/Matrix.hpp"
#include "Graphics/Color.hpp"

namespace vkfw
{
	class Window;
}

namespace ProperRT
{
	struct RasterizationMatrices
	{
		Matrix4x4f view;
		Matrix4x4f projection;
		Matrix4x4f PV;

		RasterizationMatrices(Matrix4x4f const& view_, Matrix4x4f const& projection_)
			: view{ view_ }, projection{ projection_ }, PV{ projection_ * view_ }
		{}
	};
}

namespace ProperRT::Components
{

    /// @brief Component representing the camera. Only the currently active camera will render the scene.
    class Camera : public Component
    {
        PRT_COMPONENT(Camera)

    public:
		static
		CompPtr<Camera const> MainCamera();

		static
		void SetMainCamera(CompPtr<Camera> camera);

		static
		void SetWindowSize(Vector2i size);

		static
		void WindowSizeChangeCallback(vkfw::Window const&, size_t width, size_t height);

	public:
		/// @brief Color of the background, i.e. color of pixels where nothing is rendered.
		Color32f backgroundColor;

		sfloat GetFOV() const { return verticalFOV; }

		Vector2sf GetPlanes() const { return { nearPlane, farPlane }; }

		// /// @brief Sets new fov, must be in (0.0, pi>.
		// /// @param newFOV New fov
		// /// @throws std::invalid_argument Thrown when FOV is outside of allowed interval
		// void SetFOV(cfloat newFOV) {
		// 	if (newFOV <= 0.0_cf || std::numbers::pi_v<cfloat> < newFOV) {
		// 		throw std::invalid_argument("Field of view must be in interval (0.0, pi>");
		// 	}

		// 	fov = newFOV;
		// }

		cfloat GetAspectRatio() const noexcept { return static_cast<cfloat>(viewport.x()) / static_cast<cfloat>(viewport.y()); }

		/// @brief Returns matrices necessary for rasterization.
		RasterizationMatrices GetCameraMatrices() const;

		void OnStart() override;

		void OnDestroy() override;

		Vector2i Viewport() const;

	private:
		/// @brief Camera that will render the scene.
		inline static
		CompPtr<Camera> mainCamera; //TODO add retrieval of first active camera from a vector of main cameras

		// inline static constexpr
		// cfloat MIN_FOV{ 0._cf };

		// inline static constexpr
		// cfloat MAX_FOV{ std::numbers::pi_v<cfloat> };

	private:
		Vector2i viewport;
		sfloat verticalFOV;
		sfloat nearPlane;
		sfloat farPlane;
    };
}
