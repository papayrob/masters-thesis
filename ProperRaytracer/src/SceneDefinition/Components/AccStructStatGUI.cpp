/**
 * File name: AccStructStatGUI.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "AccStructStatGUI.hpp"

#include "imgui.h"

#include "RTApp.hpp"
#include "Graphics/Raytracing/IAccelerationStructure.hpp"

namespace ProperRT::Components
{
    void AccStructStatGUI::OnUIDraw() {
        IAccelerationStructure* accStruct = RTApp::GetRaytracer()->GetAccelerationStructure();
        if (RTApp::GetRenderingMode() != RTApp::RenderingMode::OpenGL || accStruct == nullptr) {
            return;
        }

        ImGui::Begin("Acceleration structure statistics");

        // Table 1
        auto stats = accStruct->GetStatistics();
        ImGui::Text("Performance stats");
        ImGui::BeginTable("AccStructStats1", 2);

        auto prefixed = ClosestBytesPrefix(stats.memoryConsumption);
        ImGui::TableNextColumn();
        ImGui::Text("Memory:");
        ImGui::TableNextColumn();
        ImGui::Text("%.2f %sB", prefixed.value, ToString(prefixed.prefix, true).c_str());

        ImGui::TableNextColumn();
        ImGui::Text("Performance:");
        ImGui::TableNextColumn();
        ImGui::Text("%7f", stats.performance);

        ImGui::TableNextColumn();
        ImGui::Text("Cost:");
        ImGui::TableNextColumn();
        ImGui::Text("%7f", stats.cost);

        ImGui::TableNextColumn();
        ImGui::Text("Structure build time:");
        ImGui::TableNextColumn();
        ImGui::Text("%s", Timer::ToString(stats.structureBuildTime, Timer::Precision::Closest).c_str());

        ImGui::TableNextColumn();
        ImGui::Text("Structure query time:");
        ImGui::TableNextColumn();
        ImGui::Text("%s", Timer::ToString(stats.averageQueryTime, Timer::Precision::Closest).c_str());

        ImGui::TableNextColumn();
        ImGui::Text("Average intersections:");
        ImGui::TableNextColumn();
        ImGui::Text("%.2f", stats.avgIntersectionsPerQuery);

        ImGui::TableNextColumn();
        ImGui::Text("Average traversal steps:");
        ImGui::TableNextColumn();
        ImGui::Text("%.2f", stats.avgTraversalStepsPerQuery);

        ImGui::TableNextColumn();
        ImGui::Text("Number of queries");
        ImGui::TableNextColumn();
        ImGui::Text("%d", stats.queryCount);

        ImGui::EndTable();


        // Table 2
        ImGui::Text("Structure stats");
        ImGui::BeginTable("AccStructStats2", 2);

        ImGui::TableNextColumn();
        ImGui::Text("Nodes:");
        ImGui::TableNextColumn();
        ImGui::Text("%d", stats.innerNodes + stats.leafNodes);

        ImGui::TableNextColumn();
        ImGui::Text("Inner nodes ratio:");
        ImGui::TableNextColumn();
        ImGui::Text("%.4f", static_cast<double>(stats.innerNodes) / static_cast<double>(stats.innerNodes + stats.leafNodes));

        ImGui::TableNextColumn();
        ImGui::Text("Leaf nodes ratio:");
        ImGui::TableNextColumn();
        ImGui::Text("%.4f", static_cast<double>(stats.leafNodes) / static_cast<double>(stats.innerNodes + stats.leafNodes));

        ImGui::TableNextColumn();
        ImGui::Text("Inner to leaf ratio");
        ImGui::TableNextColumn();
        ImGui::Text("%.4f", static_cast<double>(stats.innerNodes) / static_cast<double>(stats.leafNodes));

        ImGui::TableNextColumn();
        ImGui::Text("Empty nodes ratio");
        ImGui::TableNextColumn();
        ImGui::Text("%.4f", static_cast<double>(stats.emptyNodes) / static_cast<double>(stats.leafNodes));

        ImGui::TableNextColumn();
        ImGui::Text("Average primitives:");
        ImGui::TableNextColumn();
        ImGui::Text("%.2f", stats.avgPrimitivesInLeaf);

        ImGui::TableNextColumn();
        ImGui::Text("Max primitives:");
        ImGui::TableNextColumn();
        ImGui::Text("%d", stats.maxPrimitivesInLeaf);

        ImGui::TableNextColumn();
        ImGui::Text("Average depth:");
        ImGui::TableNextColumn();
        ImGui::Text("%.2f", stats.avgDepth);

        ImGui::TableNextColumn();
        ImGui::Text("Max depth:");
        ImGui::TableNextColumn();
        ImGui::Text("%d", stats.maxDepth);

        ImGui::EndTable();

        ImGui::End();
    }

    void AccStructStatGUI::from_json(nlohmann::json const& j) {}
}
