/**
 * File name: MorphRenderer.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2023 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "MorphRenderer.hpp"

#include <fstream>

#include "RTApp.hpp"
#include "Transform.hpp"
#include "Utilities/Utilities.hpp"
#include "Graphics/Raytracing/GPUMeshManager.cuh"

namespace ProperRT::Components
{
	void MorphRenderer::from_json(nlohmann::json const& j) {
		// Load asset
		j.at("assetID").get_to(assetID);
		mesh = AssetManager::LoadMorphAnim(assetID, true);
		submeshIndeces.resize(mesh->GetPrimitiveCounts().size());//TODO
		std::iota(submeshIndeces.begin(), submeshIndeces.end(), 0);
		mesh->SyncToGPU();
	}

    void MorphRenderer::Awake() {
        auto cp = CompPtr(this);
		if (RTApp::GetRenderingMode() == RTApp::RenderingMode::OpenGL) {
			RTApp::GetRasterizer()->RegisterRenderer(cp);
		}
		RTApp::GetRaytracer()->RegisterRenderer(cp);

		if (controller == nullptr) {
			controller = GetGameObject()->GetComponent<MorphController>();
			if (controller == nullptr) {
				throw;//TODO
			}
		}

		cachedInfo = ComputeMeshBlendInfo();
    }

    void MorphRenderer::Update() {
		cachedInfo = ComputeMeshBlendInfo();
    }

	MorphMesh::MeshBlendInfo MorphRenderer::ComputeMeshBlendInfo() const {
		// Compute the time interval between each blend
		auto meshN = mesh->MeshCount();
		cfloat interval = controller->animLength / (meshN - 1);

		// computes the index in the integer part and the blend alpha in the decimal part
		cfloat interTime = controller->animTime / interval;

		// Compute index of the first mesh
		auto idx = static_cast<int32>(std::floor(interTime));
		if (idx >= meshN - 1) {
			// Last mesh, there would be no second mesh, so instead return the second to last mesh and last mesh with blend alpha = 1
			return MorphMesh::MeshBlendInfo{ .blendAlpha{ 1._cf }, .firstIndex{ meshN - 2 } };
		}

		return MorphMesh::MeshBlendInfo{ .blendAlpha{ static_cast<float>(interTime - static_cast<cfloat>(idx)) }, .firstIndex{ idx } };
	}

	Primitive MorphRenderer::GetPrimitiveLocal(int32 submeshIdxInRenderer, int32 primitiveIdx) {
		return mesh->GetPrimitive(submeshIndeces[submeshIdxInRenderer], primitiveIdx, cachedInfo);
	}

    Primitive MorphRenderer::GetPrimitive(int32 submeshIdxInRenderer, int32 primitiveIdx) {
        return mesh->GetPrimitive(submeshIndeces[submeshIdxInRenderer], primitiveIdx, cachedInfo, GetComponent<Transform>()->ObjectToWorldMatrix());
    }

    std::vector<Renderer::SubmeshInfo> MorphRenderer::GetSubmeshesInfo() {
        std::vector<Renderer::SubmeshInfo> out(submeshIndeces.size());
		std::transform(submeshIndeces.begin(), submeshIndeces.end(), out.begin(),
			[this](int32_t submeshIdx) { return Renderer::SubmeshInfo{ submeshIdx, mesh->GetPrimitiveCount(submeshIdx) }; }
		);
		return out;
    }

    void MorphRenderer::CreateSubmeshInstances(std::span<GPUMeshManagement::Instance> instances) {
		int32_t submeshN = std::min(StdSize<int32>(instances), StdSize<int32>(submeshIndeces));
		for (int32_t i = 0; i < submeshN; ++i) {
			mesh->FillSubmeshInstance(instances[i], cachedInfo, submeshIndeces[i]);
			instances[i].transform = GetComponent<Transform>()->ObjectToWorldMatrix();
			instances[i].material = mesh->Materials()[submeshIndeces[i]];
			instances[i].submeshIdx = submeshIndeces[i];
		}
    }

    void MorphRenderer::Rasterize(RasterizationMatrices const &matrices, bool bind)
    {
        mesh->Rasterize(GetComponent<Transform>()->ObjectToWorldMatrixf(), matrices, submeshIndeces, cachedInfo, bind);
    }
}
