/**
 * File name: InterpolateTransform.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "SceneDefinition/Component.hpp"
#include "Geometry/Vector.hpp"
#include "Transform.hpp"

namespace ProperRT::Components
{
    /// @brief Component for interpolating between the start transform state and target transform state.
    class InterpolateTransform : public Component
    {
        PRT_COMPONENT(InterpolateTransform)

    public:

        void OnStart() override;

        void Update() override;

    private:
        Vector3cf sourcePosition;
        Vector3cf sourceRotation;
        Vector3cf sourceScale;

        Vector3cf targetPosition;
        Vector3cf targetRotation;
        Vector3cf targetScale;

        cfloat length;
        cfloat time{ 0.0_cf };

        CompPtr<Transform> transform;
    };
}
