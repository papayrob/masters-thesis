/**
 * File name: MorphModel.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "SceneDefinition/AssetID.hpp"
#include "SceneDefinition/Component.hpp"
#include "MorphController.hpp"
#include "MorphRenderer.hpp"

namespace ProperRT::Components
{
    /// @brief Component representing an unserializable animated model (hierarchy of animated meshes).
    class MorphModel : public Component
    {
        PRT_COMPONENT(MorphModel)

    protected:
        AssetID assetID;
    };
}
