/**
 * File name: MorphController.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2023 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <utility>

#include "SceneDefinition/Component.hpp"
#include "Geometry/Mesh.hpp"

namespace ProperRT::Components
{
    /// @brief Component for controlling animation time.
    /// @details Adds time delta to the local animation time each frame and clips it to animation length.
    class MorphController : public Component
    {
        PRT_COMPONENT(MorphController)

    public:

        void Update() override;

        cfloat animLength{ 1.0_cf };
        cfloat animTime{ 0.0_cf };
    };
}
