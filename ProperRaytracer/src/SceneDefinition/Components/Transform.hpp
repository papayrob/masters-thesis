/**
 * File name: Transform.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2023 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "Geometry/Vector.hpp"
#include "SceneDefinition/Component.hpp"

namespace ProperRT::Components
{
    /// @brief Transform component for storing and working with game object's transformations. Game objects always have a transform component.
    class Transform : public Component
    {
        PRT_COMPONENT(Transform)

    public:
        using Vector3 = Vector3cf;
        using Vector4 = Vector4cf;
        using Matrix4x4 = Matrix4x4cf;

        /// @brief World forward vector.
        inline static constexpr
        Vector3 ForwardLocal{ 0._cf, 0._cf, -1._cf };

        /// @brief World right vector.
        inline static constexpr
        Vector3 RightLocal{ 1._cf, 0._cf, 0._cf };

        /// @brief World up vector.
        inline static constexpr
        Vector3 UpLocal{ 0._cf, 1._cf, 0._cf };
    
    public:

        /// @brief Transforms @p point from local to world coordinates.
        /// @param point Input interpreted as a point
        /// @return Point vector transformed to world coordinates
        Vector3 TransformToWorld(Vector3 const& point) const {
            return Vector3{ static_cast<Vector4>(objectToWorld * Vector4(point, 1._cf)) };
        }

        /// @brief Transforms @p triangle to world coordinates according to this transform.
        /// @param triangle Triangle to transform
        /// @return Triangle transformed to world coordinates
        Triangle TransformToWorld(Triangle const& triangle) const {
            return Triangle{{
                Triangle::VertexType{ TransformToWorld(Vector3{ triangle.vertices[0] }) },
                Triangle::VertexType{ TransformToWorld(Vector3{ triangle.vertices[1] }) },
                Triangle::VertexType{ TransformToWorld(Vector3{ triangle.vertices[2] }) }
            }};
        }

        /// @brief Transforms @p dir to world coordinates according to this transform.
        /// @param dir Direction to transform
        Vector3 TransformToWorldDirection(Vector3 const& dir) const;

        /// @brief Returns the matrix transforming points from local object space to the world space.
        /// @return Object to world matrix
        Matrix4x4 ObjectToWorldMatrix() const { return objectToWorld; }

        /// @brief Returns the matrix transforming points from local object space to the world space.
        /// @return Object to world matrix
        Matrix4x4 ObjectToWorldMatrixf() const { return static_cast<Matrix4x4f>(objectToWorld); }

        /// @brief Returns the matrix transforming points from world space to the local object space.
        /// @details Has to compute the inverse of the object to world matrix.
        /// @return World to object matrix
        Matrix4x4 WorldToObjectMatrix() const { return ObjectToWorldMatrix().Inversed(); }

        /// @brief Change the world position of this transform.
        /// @details Must propagate the changes to child transforms.
        /// @param newPos New position
        void ChangePositionWorld(Vector3 const& newPos);

        //void Rotate(Vector3sf const& rotation);

        /// @brief Change the local position of this transform.
        /// @details Must propagate the changes to child transforms.
        /// @param newPos New position
        void ChangePositionLocal(Vector3 const& newPos) {
            if (IsStatic()) {
                return;
            }
            localPosition = newPos;
            Recalculate();
        }

        /// @brief Change the local rotation of this transform.
        /// @details Must propagate the changes to child transforms.
        /// @param newRot New rotation
        void ChangeRotationLocal(Vector3 const& newRot) {
            if (IsStatic()) {
                return;
            }
            localRotation = newRot;
            Recalculate();
        }

        //TODO throw on zero scale
        /// @brief Change the local scale of this transform.
        /// @details Must propagate the changes to child transforms.
        /// @param newScale New scale
        void ChangeScaleLocal(Vector3cf const& newScale) {
            if (IsStatic()) {
                return;
            }
            localScale = newScale;
            Recalculate();
        }

        /// @brief Changes all local properties (position, rotation, scale) at once, doing only one transform recalculation.
        /// @param newPos New local position
        /// @param newRot New local rotation
        /// @param newScale New local scale
        void ChangeAllLocal(Vector3 const& newPos, Vector3 const& newRot, Vector3 const& newScale) {
            if (IsStatic()) {
                return;
            }
            localPosition = newPos;
            localRotation = newRot;
            localScale = newScale;
            Recalculate();
        }

        /// @brief Recalculates local matrix and propagates changes down to child transforms.
        bool Recalculate();

        /// @brief Returns the local position of this transform.
        /// @return Local position of this transform
        Vector3 GetPositionLocal() const { return localPosition; }

        /// @brief Returns the local rotation of this transform.
        /// @return Local rotation of this transform
        Vector3 GetRotationLocal() const { return localRotation; }

        /// @brief Returns the local scale of this transform.
        Vector3 GetScaleLocal() const { return localScale; }

        /// @brief Returns the world position of this transform.
        /// @return World position of this transform
        Vector3 GetPositionWorld() const { return Vector3{ ObjectToWorldMatrix()[3] }; }

        /// @brief Returns the forward vector of this transform in world coordinates.
        /// @return Forward vector of this transform in world coordinates
        Vector3 ForwardWorld() const { return -Vector3{ ObjectToWorldMatrix()[2] }; }

        /// @brief Returns the right vector of this transform in world coordinates.
        /// @return Right vector of this transform in world coordinates
        Vector3 RightWorld() const { return Vector3{ ObjectToWorldMatrix()[0] }; }

        /// @brief Returns the up vector of this transform in world coordinates.
        /// @return Up vector of this transform in world coordinates
        Vector3 UpWorld() const { return Vector3{ ObjectToWorldMatrix()[1] }; }

        /// @brief Returns true if the transform is static. Static transforms cannot be moved.
        bool IsStatic() const { return (staticStatus == StaticStatus::StaticInitialized); }

    private:
        enum class StaticStatus : int8 {
            StaticUninitialized = -1, DynamicUninitialized = -2, None = 0, StaticInitialized = 1, DynamicInitialized = 2
        };

        constexpr
        bool IsInitialized() const {
            return (ToUnderlying(staticStatus) > 0);
        }

        constexpr
        bool IsUninitialized() const {
            return (ToUnderlying(staticStatus) < 0);
        }

        StaticStatus Initialize() {
            staticStatus = static_cast<StaticStatus>(std::abs(ToUnderlying(staticStatus)));
            return staticStatus;
        }

    private:
        Vector3 localPosition{ 0._cf, 0._cf, 0._cf };
        Vector3 localRotation{ 0._cf, 0._cf, 0._cf };
        Vector3 localScale{ 1._cf, 1._cf, 1._cf };

        StaticStatus staticStatus;

        Matrix4x4 objectToWorld{ Matrix4x4::Identity() };
    };
}
