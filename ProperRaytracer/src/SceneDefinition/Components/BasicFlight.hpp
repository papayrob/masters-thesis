/**
 * File name: BasicFlight.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2023 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "SceneDefinition/Component.hpp"
#include "Geometry/Vector.hpp"

namespace ProperRT::Components
{
    /// @brief Component implementing basic flight with WASD, CTRL, Space and Shift.
    class BasicFlight : public Component
    {
        PRT_COMPONENT(BasicFlight)

    public:

        void Update() override;

        virtual
        bool AllowDuringSnapshot() override { return true; }

    private:
        cfloat speed{ 1._cf };
        cfloat sensitivity{ 1._cf };
        Vector2cf lastCursorPos{ 0._cf };
    };
}
