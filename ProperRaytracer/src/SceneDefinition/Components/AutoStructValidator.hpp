/**
 * File name: AutoStructValidator.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "SceneDefinition/Component.hpp"

namespace ProperRT::Components
{
    /// @brief Component for automatically validating an active data structure either once at the start, or every n frames.
    /// @details Useful mainly for testing, otherwise could significantly lag the application.
    class AutoStructValidator : public Component
    {
        PRT_COMPONENT(AutoStructValidator)
    
    public:
        virtual
        void OnPostRender();
    
    private:
        int skipFrames;
    };
}
