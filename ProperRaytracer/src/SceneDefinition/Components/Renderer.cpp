/**
 * File name: Renderer.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "Renderer.hpp"

#include "Graphics/Raytracing/GPUMeshManager.cuh"

namespace ProperRT::Components
{
    std::vector<GPUMeshManagement::Instance> Renderer::CreateSubmeshInstances() {
        std::vector<GPUMeshManagement::Instance> out(GetSubmeshCount());
        CreateSubmeshInstances(out);
        return out;
    }
}

