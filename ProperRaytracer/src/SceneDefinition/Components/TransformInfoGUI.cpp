/**
 * File name: TransformInfoGUI.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "TransformInfoGUI.hpp"

#include "imgui.h"
#include "Transform.hpp"

namespace ProperRT::Components
{
    void TransformInfoGUI::from_json(nlohmann::json const& j) {
    }

    void TransformInfoGUI::OnStart() {
        windowName = std::string{ GetGameObject()->Name() } + " transform info";
    }

    void TransformInfoGUI::OnUIDraw() {
        ImGui::Begin(windowName.c_str());
        auto transform = GetComponent<Transform>();
        auto pos = transform->GetPositionLocal();
        auto rot = transform->GetRotationLocal();
        ImGui::LabelText("Position", "[%.4f,%.4f,%.4f]", pos.x(), pos.y(), pos.z());
        ImGui::SameLine();
        if (ImGui::Button("Copy##Position")) {
            ImGui::SetClipboardText(std::format("[{:.4f},{:.4f},{:.4f}]", pos.x(), pos.y(), pos.z()).c_str());
        }
        ImGui::LabelText("Rotation", "[%.4f,%.4f,%.4f]", rot.x(), rot.y(), rot.z());
        ImGui::SameLine();
        if (ImGui::Button("Copy##Rotation")) {
            ImGui::SetClipboardText(std::format("[{:.4f},{:.4f},{:.4f}]", rot.x(), rot.y(), rot.z()).c_str());
        }
        ImGui::End();
    }
}
