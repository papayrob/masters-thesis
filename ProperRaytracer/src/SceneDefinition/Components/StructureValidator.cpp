/**
 * File name: StructureValidator.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "StructureValidator.hpp"

#include "imgui.h"

#include "RTApp.hpp"
#include "Renderer.hpp"

namespace ProperRT::Components
{
    void StructureValidator::OnUIDraw() {
        if (RTApp::GetRenderingMode() != RTApp::RenderingMode::OpenGL) {
            return;
        }

        ImGui::Begin("Structure validation");

        // Disable if there is no active navigator or the frame tracer is running
        if (Validator() == nullptr || RTApp::IsFrameTracerRunning()) {
            //ImGui::Text("Current raytracer has no navigator available");
            ImGui::BeginDisabled();
            ImGui::Button("Validate");
            ImGui::EndDisabled();
        }
        // Validate of button clicked
        else if (ImGui::Button("Validate")) {
            error = Validator()->Validate();
        }

        // Display error or success
        if (error.has_value()) {
            ImGui::Text("%s", error.value().c_str());
        }
        else {
            ImGui::Text("Structure successfully validated without errors");
        }

        ImGui::End();
    }

    IStructureValidator * StructureValidator::Validator() {
        return ((RTApp::GetRaytracer() == nullptr) ? nullptr : RTApp::GetRaytracer()->GetValidator());
    }


    void StructureValidator::from_json(nlohmann::json const& j) {}
}
