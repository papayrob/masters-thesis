/**
 * File name: Renderer.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "SceneDefinition/Component.hpp"
#include "Geometry/GenericMesh.hpp"

namespace ProperRT
{
    namespace GPUMeshManagement
    {
        struct Instance;
    }
}

namespace ProperRT::Components
{
    /// @brief Base class for components that render meshes.
    class Renderer : public Component
    {
        PRT_COMPONENT_ABSTRACT(Renderer, Component)
    public:
        /// @brief Class with the index and size of a submesh.
        struct SubmeshInfo
        {
            int32 submeshIdx;
            int32 submeshSize;
        };

    public:
        /// @brief If true, the mesh is rendered, otherwise it should not be visible.
        bool doRender{ true };
        
        /// @brief Returns the primitive at @p primitiveIdx from submesh at @p submeshIdxInRenderer.
        /// @param submeshIdxInRenderer Index of the submesh in the renderer (not in the mesh)
        /// @param primitiveIdx Offset of the primitive in the submesh
        virtual
        Primitive GetPrimitiveLocal(int32 submeshIdxInRenderer, int32 primitiveIdx) = 0;

        /// @brief Transforms and returns the primitive at @p primitiveIdx from submesh at @p submeshIdxInRenderer.
        /// @param submeshIdxInRenderer Index of the submesh in the renderer (not in the mesh)
        /// @param primitiveIdx Offset of the primitive in the submesh
        virtual
        Primitive GetPrimitive(int32 submeshIdxInRenderer, int32 primitiveIdx) = 0;

        /// @brief Returns the number of submeshes this renderer is displaying.
        virtual
        int32 GetSubmeshCount() = 0;

        /// @brief Returns info about the submesh the renderer displays at @p submeshIdxInRenderer.
        virtual
        SubmeshInfo GetSubmeshInfo(int32 submeshIdxInRenderer) = 0;

        /// @brief Returns info on all submeshes the renderer displays.
        virtual
        std::vector<SubmeshInfo> GetSubmeshesInfo() = 0;

        /// @brief Creates compute instances and places them into @p instances.
        /// @param instances Span of instances to create into, must be at least as large as submesh count
        virtual
        void CreateSubmeshInstances(std::span<GPUMeshManagement::Instance> instances) = 0;

        /// @brief Creates compute instances and returns them.
        std::vector<GPUMeshManagement::Instance> CreateSubmeshInstances();

        /// @brief Returns the rendered mesh.
        virtual
        std::shared_ptr<GenericMesh> GetMesh() = 0;

        /// @brief Rasterizes the referenced submeshes of the referenced mesh with OpenGL.
        /// @param matrices Required rasterization matrices used for rendering
        /// @param bind If set to true, the renderer will bind resources itself, otherwise the responsibility falls onto the caller
        virtual
        void Rasterize(RasterizationMatrices const& matrices, bool bind = true) = 0;

    protected:
        AssetID assetID;
    };
}
