/**
 * File name: InterpolateTransform.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "InterpolateTransform.hpp"

#include "RTApp.hpp"
#include "Utilities/Serializers.hpp"

namespace ProperRT::Components
{
    void InterpolateTransform::from_json(nlohmann::json const& j) {
        j.at("targetPosition").get_to(targetPosition);
        j.at("targetRotation").get_to(targetRotation);
        j.at("targetScale").get_to(targetScale);
        j.at("length").get_to(length);
    }

    void InterpolateTransform::OnStart() {
        transform = GetComponent<Transform>();
        sourcePosition = transform->GetPositionLocal();
        sourceRotation = transform->GetRotationLocal();
        sourceScale = transform->GetScaleLocal();
    }

    void InterpolateTransform::Update() {
        time = std::fmod(time + RTApp::GetTimeDelta(), length);
        cfloat alpha = time / length;
        transform->ChangePositionLocal(Vector3cf::LERP(sourcePosition, targetPosition, alpha));
        transform->ChangeRotationLocal(Vector3cf::LERP(sourceRotation, targetRotation, alpha));
        transform->ChangeScaleLocal(Vector3cf::LERP(sourceScale, targetScale, alpha));
    }
}
