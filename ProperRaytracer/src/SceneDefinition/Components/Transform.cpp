/**
 * File name: Transform.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2023 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "Transform.hpp"

#include "Utilities/Serializers.hpp"

namespace ProperRT::Components
{
    void Transform::from_json(nlohmann::json const& j) {
        j.at("localPosition").get_to(localPosition);
        j.at("localRotation").get_to(localRotation);
        j.at("localScale").get_to(localScale);
        if (j.at("static").get<bool>()) {
            staticStatus = StaticStatus::StaticUninitialized;
        }
        else {
            staticStatus = StaticStatus::DynamicUninitialized;
        }
    }

    Transform::Vector3 Transform::TransformToWorldDirection(Transform::Vector3 const &dir) const {
        auto rotX = RotationMatrixX(ToRadians(localRotation.x()));
        auto rotY = RotationMatrixY(ToRadians(localRotation.y()));
        auto rotZ = RotationMatrixZ(ToRadians(localRotation.z()));
        return Vector3{ (rotY * rotX * rotZ) * Vector4{ dir, 1.0 } };
    }

    void Transform::ChangePositionWorld(Vector3 const &newPos) {
        if (IsStatic()) {
            return;
        }
        
        if (GetGameObject()->GetParent() != nullptr) {
            localPosition = Vector3{ GetGameObject()->GetParent()->GetComponent<Transform>()->WorldToObjectMatrix() * Vector4{ newPos, Vector4::DataType{ 1 } } };
        }
        else {
            localPosition = newPos;
        }
        
        Recalculate();
    }

    bool Transform::Recalculate() {
        if (IsStatic()) {
            return true;
        }

        auto scale = ScaleMatrix(localScale);
        auto rotX = RotationMatrixX(ToRadians(localRotation.x()));
        auto rotY = RotationMatrixY(ToRadians(localRotation.y()));
        auto rotZ = RotationMatrixZ(ToRadians(localRotation.z()));
        auto translate = TranslationMatrix(localPosition);
        objectToWorld = translate * rotY * rotX * rotZ * scale;
        if (GetGameObject()->GetParent() != nullptr) {
            objectToWorld = GetGameObject()->GetParent()->GetComponent<Transform>()->ObjectToWorldMatrix() * objectToWorld;
        }

        // Propagate transform changes down the graph
        int32 childN = GetGameObject()->GetChildCount();
        bool hasStaticChild = false;
        for (int32 childI = 0; childI < childN; ++childI) {
            hasStaticChild = hasStaticChild || GetGameObject()->GetChild(childI)->GetComponent<Transform>()->Recalculate();
        }

        if (IsUninitialized()) {
            if (Initialize() == StaticStatus::DynamicInitialized && hasStaticChild) {
                staticStatus = StaticStatus::StaticInitialized;
            }
        }
        assert(!(IsInitialized() && staticStatus == StaticStatus::DynamicInitialized && hasStaticChild));

        return IsStatic();
    }
}
