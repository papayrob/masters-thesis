/**
 * File name: StructureValidator.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "SceneDefinition/Component.hpp"
#include "Graphics/Raytracing/IStructureValidator.hpp"

namespace ProperRT::Components
{
    class Renderer;

    /// @brief Component for displaying UI for validating data structures.
    class StructureValidator : public Component
    {
        PRT_COMPONENT(StructureValidator)
    
    public:
        virtual
        void OnUIDraw() override;

        virtual
        bool AllowDuringSnapshot() override { return true; }
    
    private:
        std::optional<std::string> error{ "Not validated yet" };

        IStructureValidator* Validator();
    };
}
