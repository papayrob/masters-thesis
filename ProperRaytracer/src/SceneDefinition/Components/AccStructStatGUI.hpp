/**
 * File name: AccStructStatGUI.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "SceneDefinition/Component.hpp"

namespace ProperRT::Components
{
    /// @brief Component for displaying acceleration data statistics, usually computed when validating.
    class AccStructStatGUI : public Component
    {
        PRT_COMPONENT(AccStructStatGUI)
    
    public:
        virtual
        void OnUIDraw() override;

        virtual
        bool AllowDuringSnapshot() override { return true; }
    };
}
