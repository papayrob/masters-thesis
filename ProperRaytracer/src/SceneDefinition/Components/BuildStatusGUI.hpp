/**
 * File name: BuildStatusGUI.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "SceneDefinition/Component.hpp"

namespace ProperRT::Components
{
    /// @brief Component for displaying the status of the build algorithm. Experimental.
    class BuildStatusGUI : public Component
    {
        PRT_COMPONENT(BuildStatusGUI)
    
    public:
        virtual
        void OnStart() override;
        
        virtual
        void Update() override;

        virtual
        void OnUIDraw() override;

        virtual
        bool AllowDuringSnapshot() override { return true; }
    
    private:
        double elapsedTime{0.0_cf};

        int32 tasks{-1};
        int32 workingSubgroups{-1};
        int32 depth{-1};
        int32 nodes{-1};
    };
}
