/**
 * File name: AutoStructValidator.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "AutoStructValidator.hpp"

#include "RTApp.hpp"
#include "Graphics/Raytracing/IStructureValidator.hpp"

namespace ProperRT::Components
{
    void AutoStructValidator::from_json(nlohmann::json const& j) {
        j.at("skipFrames").get_to(skipFrames);
        if (skipFrames == 0) skipFrames = 1;
        else if (skipFrames < 0) skipFrames = -1;
    }

    void AutoStructValidator::OnPostRender() {
        // Validate only first frame
        if (skipFrames < 0) {
            if (skipFrames == -1 && RTApp::GetRaytracer()->GetValidator() != nullptr) {
                auto err = RTApp::GetRaytracer()->GetValidator()->Validate();
                if (err.has_value()) {
                    throw std::runtime_error(err.value()); // TODO
                }
                skipFrames = -2;
            }
        }
        // Validate every skipFrames-th frame
        else {
            if (RTApp::GetRaytracer()->GetValidator() != nullptr && RTApp::FramesSinceStart() % skipFrames == 0) {
                auto err = RTApp::GetRaytracer()->GetValidator()->Validate();
                if (err.has_value()) {
                    throw std::runtime_error(err.value()); // TODO
                }
            }
        }
    }
}
