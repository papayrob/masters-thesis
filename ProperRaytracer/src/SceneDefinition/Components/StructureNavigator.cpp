/**
 * File name: StructureNavigator.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "StructureNavigator.hpp"

#include "imgui.h"

#include "RTApp.hpp"

namespace ProperRT::Components
{
    void StructureNavigator::OnUIDraw() {
        if (RTApp::GetRenderingMode() != RTApp::RenderingMode::OpenGL) {
            return;
        }

        ImGui::Begin("Structure navigation");

        // Disable if there is no active navigator or the frame tracer is running
        if (TopNavigator() == nullptr || RTApp::IsFrameTracerRunning()) {
            //ImGui::Text("Current raytracer has no navigator available");
            ImGui::BeginDisabled();
            ImGui::Button("Start Navigation");
            ImGui::EndDisabled();
        }
        else if (ImGui::Button("Start Navigation")) {
            // Reset navigation
            Clear();
            auto navigator = TopNavigator();

            navigatorStack.push(navigator);
            navigatorStack.top()->StartNavigating();

            // Disable rendering of all active renderers so that the navigators can use custom rendering, and save them for reactivation later
            Scene::GetActiveScene()->ForEachComponentOfType<Renderer>([this](CompPtr<Renderer> rend) {
                if (rend->doRender) {
                    enabledComponents.push_back(rend);
                    rend->doRender = false;
                }
            });

            RTApp::SetFrameTracerEnabled(false);
        }

        if (navigatorStack.empty()) {
            // No active navigator
            ImGui::BeginDisabled();
            ImGui::Button("Into");
            ImGui::SameLine();
            ImGui::Button("Out");
            ImGui::EndDisabled();
        }
        else {
            // General navigation
            ImGui::BeginDisabled(!navigatorStack.top()->CanNavigateInto());
            bool navigateInto = ImGui::Button("Into");
            ImGui::EndDisabled();
            ImGui::SameLine();
            bool navigateOut = ImGui::Button("Out");

            ImGui::Separator();

            // Navigator navigation
            INavigator* navigator = navigatorStack.top();
            navigator->Navigate();

            if (navigateOut) {
                navigator->NavigateOut();
                navigatorStack.pop();

                if (navigatorStack.empty()) {
                    // Enable disabled renderers
                    for (CompPtr<Renderer> rend : enabledComponents) {
                        if (rend != nullptr) {
                            rend->doRender = true;
                        }
                    }
                    enabledComponents.clear();
                    RTApp::SetFrameTracerEnabled(true);
                }
            }
            else if (navigateInto) {
                navigatorStack.push(navigator->NavigateInto());
            }
        }

        ImGui::End();
    }

    void StructureNavigator::OnPostRender() {
        if (RTApp::GetRenderingMode() != RTApp::RenderingMode::OpenGL) {
            return;
        }
        
        // Render top-level navigator
        if (!navigatorStack.empty()) {
            navigatorStack.top()->Render();
        }
    }

    INavigator* StructureNavigator::TopNavigator() {
        return RTApp::GetRaytracer()->GetNavigator();
    }

    void StructureNavigator::Clear() {
        while (!navigatorStack.empty()) {
            navigatorStack.top()->NavigateOut();
            navigatorStack.pop();
        }
        RTApp::SetFrameTracerEnabled(true);
    }

    void StructureNavigator::from_json(nlohmann::json const& j) {}
}
