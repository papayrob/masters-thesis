/**
 * File name: AssetID.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2023 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <filesystem>

#include "nlohmann/json.hpp"

namespace ProperRT
{
    /// @brief Class for abstracting asset paths.
    struct AssetID
    {
    public:
        /// @brief Hashing class for AssetID.
        struct Hash
        {
            std::size_t operator()(AssetID const& assetID) const {
                return std::hash<std::string>{}(assetID.path);
            }
        };

        /// @brief Default ctor.
        AssetID() noexcept = default;

        /// @brief Creates an asset id from a relative or absolute system path.
        /// @details The path has to be inside the Assets folder.
        /// @param pathToAsset Path to the asset
        AssetID(std::filesystem::path pathToAsset);

        /// @brief Returns true if the id points to a valid existing asset.
        /// @return True if the id points to a valid asset.
        bool IsValid() const;

        /// @brief Returns the path to the asset from asset root.
        /// @return Path to the asset.
        std::filesystem::path GetAssetPath() const;

        /// @brief Returns the actual path to the asset from drive root.
        /// @return System path to the asset.
        std::filesystem::path GetSystemPath() const;

        /// @brief Returns the asset id as a string.
        std::string ToString() const;

        friend
        void to_json(nlohmann::json&, AssetID const& value);

        friend
        void from_json(nlohmann::json const& j, AssetID& value);

        friend
        bool operator==(AssetID const&, AssetID const&) = default;

    private:
        std::string path{ "" };
    };
}
