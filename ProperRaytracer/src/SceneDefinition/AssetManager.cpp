/**
 * File name: AssetManager.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2023 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "AssetManager.hpp"

#include <stdexcept>
#include <fstream>
#include "assimp/scene.h"
#include "assimp/Importer.hpp"
#include "assimp/postprocess.h"

#include "InvalidAsset.hpp"
#include "Geometry/IndexedTriangleMesh.hpp"
#include "Geometry/IndexedTriangleMorphMesh.hpp"
#include "Utilities/Utilities.hpp"
#include "Utilities/Logger.hpp"

#if defined(PRT_WINDOWS)

#include <windows.h>

namespace
{
	std::filesystem::path GetExecutablePath() {
		DWORD bufferSize = 256;
		std::vector<WCHAR> pathBuffer(bufferSize, 0);

		bool hasPath = false;
		bool shouldContinue = true;

		DWORD result;
		DWORD lastError;
		do {
			result = GetModuleFileNameW(nullptr, pathBuffer.data(), bufferSize);
			lastError = GetLastError();

			if (result == 0) {
				shouldContinue = false;
			}
			else if (result < bufferSize) {
				hasPath = true;
				shouldContinue = false;
			}
			else if (result == bufferSize && (lastError == ERROR_INSUFFICIENT_BUFFER || lastError == ERROR_SUCCESS)) {
				bufferSize *= 2;
				pathBuffer.resize(bufferSize);
			}
			else {
				shouldContinue = false;
			}
		} while (shouldContinue);

		if (hasPath) {
			return std::filesystem::canonical(std::filesystem::path(std::wstring(pathBuffer.data(), result)));
		}

		throw std::runtime_error("Could not locate executable: " + std::to_string(lastError));
	}
}

#elif defined(PRT_LINUX)

#include <unistd.h>
#include <errno.h>

namespace
{
	std::filesystem::path GetExecutablePath() {
		ssize_t bufferSize = 256;
		std::vector<char> pathBuffer(bufferSize, 0);

		bool hasPath = false;
		bool shouldContinue = true;

		ssize_t result;
		int errnoSaved;
		while (shouldContinue) {
			result = readlink("/proc/self/exe", pathBuffer.data(), bufferSize);

			if (result < 0) {
				shouldContinue = false;
				errnoSaved = errno;
			}
			else if (result < bufferSize) {
				hasPath = true;
				shouldContinue = false;
			}
			else {
				bufferSize *= 2;
				pathBuffer.resize(bufferSize);
			}
		}

		if (hasPath) {
			try
			{
				return std::filesystem::canonical(
					std::filesystem::path(
						std::string(pathBuffer.data(), result)
					)
				);
			}
			catch (const std::exception& ex)
			{
				throw std::runtime_error("Could not locate executable: " + std::string(ex.what()));
			}
		}

		throw std::runtime_error("Could not locate executable: " + std::to_string(errnoSaved));
	}
}

#elif defined(PRT_APPLE)

#include <mach-o/dyld.h>

namespace
{
	std::filesystem::path GetExecutablePath() {
		uint32_t bufferSize = 256;
		std::vector<char> pathBuffer(bufferSize, 0);

		int result = _NSGetExecutablePath(pathBuffer.data(), &bufferSize);
		if (result == -1) {
			pathBuffer.resize(bufferSize + 1);
			pathBuffer[0] = 0;
			result = _NSGetExecutablePath(pathBuffer.data(), &bufferSize);
			if (result != 0) {
				result = -1;
			}
		}
		else if (result != 0) {
			result = -1;
		}

		if (result == 0 && pathBuffer[0] != 0) {
			try
			{
				return std::filesystem::canonical(
					std::filesystem::path(
						std::string(pathBuffer.data(), result)
					)
				);
			}
			catch (const std::exception& ex)
			{
				throw std::runtime_error("Could not locate executable: " + std::string(ex.what()));
			}
		}

		throw std::runtime_error("Could not locate executable");
	}
}

#else

#error "Compilation for current platform is not supported"

#endif

namespace ProperRT
{
	std::filesystem::path const AssetManager::executablePath = GetExecutablePath();

	std::filesystem::path const AssetManager::assetRootPath = executablePath.parent_path() / "Assets";

	std::shared_ptr<Mesh> AssetManager::LoadMesh(AssetID const& assetID, bool flattenGeometry) {
		Assimp::Importer importer{};
		return AssetManager::LoadMesh(assetID, importer, flattenGeometry);
	}

	std::shared_ptr<Mesh> AssetManager::LoadMesh(AssetID const& assetID, Assimp::Importer& importer, bool flattenGeometry) {
		// Try to find already allocated mesh
		auto asset = GetAsset(assetID);
		auto mesh = std::dynamic_pointer_cast<IndexedTriangleMesh>(asset);

		// Load assimp scene
		auto filePath = assetID.GetSystemPath();

		importer.SetPropertyInteger(AI_CONFIG_PP_SBP_REMOVE, aiPrimitiveType_POINT | aiPrimitiveType_LINE);

		unsigned int flags = aiProcessPreset_TargetRealtime_Fast;
		flags &= ~aiProcess_JoinIdenticalVertices;
		flags |= (flattenGeometry ? aiPostProcessSteps::aiProcess_PreTransformVertices : 0);
		aiScene const* meshScene = importer.ReadFile(filePath.string(), flags);
		if (meshScene == nullptr) {
			throw InvalidAsset(assetID, std::string("Bad mesh: ") + importer.GetErrorString());
		}

		if (mesh != nullptr) {
			return mesh;
		}
		else if (asset != nullptr) {
			throw InvalidAsset(assetID, std::string("Another asset loaded under this asset ID that is not a mesh"));
		}

		auto const convert = [](aiVector3D const& vec) -> Vector3sf {
			return Vector3sf{ vec.x, vec.y, vec.z };
		};

		// All submeshes
		mesh = std::make_shared<IndexedTriangleMesh>();

		std::vector<Vector3sf> vertices{};
		for (unsigned int meshI = 0, offset{ 0 }; meshI < meshScene->mNumMeshes; ++meshI) {
			aiMesh const* assMesh = meshScene->mMeshes[meshI];
			vertices.reserve(vertices.size() + assMesh->mNumVertices);

			for (unsigned int vertexI = 0; vertexI < assMesh->mNumVertices; ++vertexI) {
				vertices.push_back(convert(assMesh->mVertices[vertexI]));
			}

			// Indexed faces
			std::vector<IndexedTriangle> indeces;

			for (unsigned faceI = 0U; faceI < assMesh->mNumFaces; ++faceI) {
				aiFace const& face = assMesh->mFaces[faceI];
				indeces.push_back(IndexedTriangle{ static_cast<int32>(offset + face.mIndices[0]), static_cast<int32>(offset + face.mIndices[1]), static_cast<int32>(offset + face.mIndices[2]) });
			}

			aiMaterial const* assMat = meshScene->mMaterials[assMesh->mMaterialIndex];
			aiColor3D color;

			Material mat;

			if (assMat->Get(AI_MATKEY_COLOR_DIFFUSE, color) != AI_SUCCESS) {
				color = aiColor3D(1);
			}
			mat.colorDiffuse = Color32f{color.r, color.g, color.b, 1.0f};

			// if (assMat->Get(AI_MATKEY_COLOR_SPECULAR, color) != AI_SUCCESS) {
			// 	color = aiColor3D(0);
			// }
			// mat.colorSpecular = Color32f{color.r, color.g, color.b, 1.0f};

			// ai_real shininess;
			// ai_real strength;

			// if (assMat->Get(AI_MATKEY_SHININESS, shininess) != AI_SUCCESS) {
			// 	shininess = ai_real{0};
			// }
			// if (assMat->Get(AI_MATKEY_SHININESS_STRENGTH, strength) != AI_SUCCESS) {
			// 	strength = ai_real{0};
			// }
			// mat.colorSpecular.a() = shininess * strength;
			
			mesh->AddSubmesh(std::span<IndexedTriangle const>{ indeces }, mat);

			offset += meshScene->mMeshes[meshI]->mNumVertices;
		}

		mesh->SetVertices(vertices);

		// Insert
		assets.insert(std::make_pair(assetID, std::weak_ptr<Asset>(mesh)));
		return mesh;
	}

	std::shared_ptr<MorphMesh> AssetManager::LoadMorphAnim(AssetID const& assetID, bool flattenGeometry) {
		// Try to find already allocated animation
		{
			auto asset = GetAsset(assetID);
			auto morphMesh = std::dynamic_pointer_cast<MorphMesh>(asset);
			if (morphMesh != nullptr) {
				return morphMesh;
			}
			else if (asset != nullptr) {
				throw InvalidAsset(assetID, std::string("Another asset loaded under this asset ID that is not a mesh"));
			}
		}
		
		Assimp::Importer importer{};
		return LoadMorphAnim(assetID, importer, flattenGeometry);
	}

	std::shared_ptr<MorphMesh> AssetManager::LoadMorphAnim(AssetID const& assetID, Assimp::Importer& importer, bool flattenGeometry) {
		// Load animation asset
		auto animFile = std::ifstream(assetID.GetSystemPath());

		// Load all meshes in the animation
		auto dir = assetID.GetAssetPath().parent_path();
		std::string meshFileName;
		std::vector<std::shared_ptr<Mesh>> meshes;
		while (std::getline(animFile, meshFileName)) {
			meshes.push_back(AssetManager::LoadMesh(AssetID(dir / meshFileName), importer, flattenGeometry));
		}

		if (meshes.size() == 0) {
			throw InvalidAsset(assetID, "Morph animation does not contain any meshes");
		}

		std::vector<std::shared_ptr<IndexedTriangleMesh>> triMeshes;
		triMeshes.reserve(meshes.size());
		for (auto const& mesh : meshes) {
			if (auto triMesh = std::dynamic_pointer_cast<IndexedTriangleMesh>(mesh); triMesh != nullptr) {
				triMeshes.push_back(triMesh);
			}
			else {
				throw InvalidAsset(assetID, "Morph anim meshes do not have the same size");
			}
		}

		auto mesh = std::make_shared<IndexedTriangleMorphMesh>(triMeshes);
		
		// Insert
		assets.insert(std::make_pair(assetID, std::weak_ptr<Asset>(mesh)));
		return mesh;
	}

	std::shared_ptr<Asset> AssetManager::GetAsset(AssetID const& assetID) {
		auto meshIt = assets.find(assetID);
		if (meshIt != assets.end()) {
			auto locked = meshIt->second.lock();
			if (locked != nullptr) {
				return locked;
			}
			else {
				assets.erase(meshIt);
			}
		}

		return nullptr;
	}

}
