/**
 * File name: Scene.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2022 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "Scene.hpp"

#include <fstream>
#include "nlohmann/json.hpp"

#include "Utilities/Serializers.hpp"
#include "InvalidAsset.hpp"
#include "Components/Transform.hpp"
#include "Components/Light.hpp"

namespace ProperRT
{
	bool Scene::SetActiveScene(std::string const& name) {
		auto it = loadedScenes.find(name);
		if (it == loadedScenes.end()) {
			return false;
		}
		nextActiveScene = it->second.get();
		return true;
	}

	void Scene::LoadScene(AssetID assetID, bool setActive, bool unloadActiveScene) {
		if (!assetID.IsValid()) {
			throw InvalidAsset(assetID, "Invalid scene file path");
		}

		// Load file
		auto fileStream = std::ifstream(assetID.GetSystemPath());
		if (!fileStream.is_open()) {
			throw InvalidAsset(assetID, "Could not open scene file");
		}

		// Load scene from json and add to loaded scenes
		Scene* scene;
		{
			//TODO add try catch
			auto sceneUnique = nlohmann::json::parse(fileStream, nullptr, true, true).get<std::unique_ptr<Scene>>();
			scene = sceneUnique.get();
			loadedScenes.insert(std::make_pair(scene->name, std::move(sceneUnique)));
		}

		assetID = AssetID{ assetID.GetAssetPath().replace_extension(".config") };
		if (assetID.IsValid()) {
			// Load file
			fileStream = std::ifstream(assetID.GetSystemPath());
			if (fileStream.is_open()) {
				scene->sceneConfig = SceneConfig{ nlohmann::json::parse(fileStream, nullptr, true, true), &RTApp::AppConfig() };
			}
			else {
				Logger::LogWarning("Could not open " + assetID.GetAssetPath().string());
				scene->sceneConfig = SceneConfig{ nlohmann::json{}, &RTApp::AppConfig() };
			}
			//TODO try catch
		}
		else {
			Logger::LogInfo("Could not find scene config " + assetID.GetAssetPath().string());
			scene->sceneConfig = SceneConfig{ nlohmann::json{}, &RTApp::AppConfig() };
		}

		scene->OnLoad();

		// Process other parameters
		Scene::unloadActive = unloadActiveScene;

		if (setActive) {
			if (activeScene == nullptr) {
				activeScene = scene;
			}
			nextActiveScene = scene;
		}
	}

	std::unique_ptr<Scene> Scene::Deserialize(nlohmann::json const& j) {
		auto value = std::make_unique<Scene>();

		auto oldActive = Scene::activeScene;
		Scene::activeScene = value.get();

		j.at("name").get_to(value->name);
		j.at("ambient").get_to(Components::Light::ambientColor);
		j.at("rootGameObjects").get_to(value->rootGameObjects);

		for (auto const& rootGO : value->rootGameObjects) {
			rootGO->GetComponent<Components::Transform>()->Recalculate();
		}

		Scene::activeScene = oldActive;
		return value;
	}

	GameObject& Scene::CreateGameObject(std::string_view name, GameObject* parent) {
		if (parent == nullptr) {
			rootGameObjects.push_back(std::unique_ptr<GameObject>(new GameObject(this, name, nullptr)));
			return *rootGameObjects.back();
		}

		if (parent->owningScene != this) {
			//TODO
			throw;
		}

		parent->children.push_back(std::unique_ptr<GameObject>(new GameObject(this, name, parent)));
		return *(parent->children.back());
	}

	void Scene::OnLoad() {

		std::stack<GameObject*> stack;
		for (auto const& go : rootGameObjects) {
			stack.push(go.get());
		}

		while (!stack.empty()) {
			GameObject* go = stack.top();
			stack.pop();

			for (auto const& comp : go->components) {
				comp->Awake();
			}

			for (auto const& child : go->children) {
				stack.push(child.get());
			}
		}
	}

    void Scene::OnSwitch() {
		if (RTApp::GetRenderingMode()== RTApp::RenderingMode::Raytracing) {
			RTApp::GetRaytracer()->BuildStatic();
		}
    }

    void Scene::Updater::SwitchScenes() {
		if (unloadActive && activeScene != nullptr) {
			loadedScenes.erase(activeScene->name);
		}

		if (nextActiveScene != nullptr) {
			activeScene = nextActiveScene;
			activeScene->OnSwitch();
			nextActiveScene = nullptr;
		}
	}

	void Scene::Updater::PreRenderUpdate() {
		for (auto const& comp : owner->uninitialized) {
			comp->OnStart();
		}

		owner->uninitialized.clear();

		owner->ForEachGameObject([this](GameObject* go) { gos.push_back(go); });
		
		if (snapshotted) {
			for (auto* go : gos) {
				for (auto const& comp : go->components) {
					if (comp->AllowDuringSnapshot()) {
						comp->Update();
					}
				}
			}
		}
		else {
			for (auto* go : gos) {
				for (auto const& comp : go->components) {
					comp->Update();
				}
			}
		}
	}

	void Scene::Updater::UIUpdate() {
		if (!RTApp::ShowGUI()) {
			return;
		}
		
		if (snapshotted) {
			for (auto* go : gos) {
				for (auto const& comp : go->components) {
					if (comp->AllowDuringSnapshot()) {
						comp->OnUIDraw();
					}
				}
			}
		}
		else {
			for (auto* go : gos) {
				for (auto const& comp : go->components) {
					comp->OnUIDraw();
				}
			}
		}
	}

	void Scene::Updater::PostRenderUpdate() {
		if (snapshotted) {
			for (auto* go : gos) {
				for (auto const& comp : go->components) {
					if (comp->AllowDuringSnapshot()) {
						comp->OnPostRender();
					}
				}
			}
		}
		else {
			for (auto* go : gos) {
				for (auto const& comp : go->components) {
					comp->OnPostRender();
				}
			}
		}
	}

}
