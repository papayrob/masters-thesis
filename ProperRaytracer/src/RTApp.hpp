/**
 * File name: RTApp.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2022 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <string>
#include <memory>
#include <chrono>
#include <atomic>
#include <thread>

#include "SceneDefinition/Scene.hpp"
#include "Utilities/ObjectFactory.hpp"
#include "Graphics/OpenGL/Rasterizer.hpp"
#include "Graphics/OpenGL/Texture.hpp"
#include "Graphics/Raytracing/IRaytracer.hpp"
#include "Graphics/Raytracing/INavigator.hpp"
#include "Utilities/TableStatistics.hpp"

namespace vkfw
{
    template<class T>
    class UniqueHandle;
    class Window;
}

namespace ProperRT
{
    /// @brief Singleton class for represening the main application.
    class RTApp
    {
    public:
        /// @brief Enum for determining the current rendering mode.
        /// @details In OpenGL mode, the scene gets rendered with OpenGL every frame
        /// and a single frame can be raytraced using special UI. This mode
        /// is suitable for testing and slower raytracer implementation.
        /// In Raytracing mode, every frame gets raytraced and no additional OpenGL resources
        /// besides the window buffers get created.
        enum class RenderingMode
        {
            OpenGL, Raytracing
        };

        /// @brief Enum for controlling the current stage of the frame tracer, run in another thread.
        enum class FrameTracerStage : int8 {
            Default, Constructed, Ready, StaticBuilt, DynamicBuilt, Traced
        };

    public:
        inline static constexpr
        char const* APP_NAME = "ProperRaytracer";

        /// @brief Singleton ctor/getter.
        /// @return RTApp instance pointer
        static
        RTApp* Instance() { return (instance == nullptr) ? (instance = std::unique_ptr<RTApp>(new RTApp)).get() : instance.get(); }

        static
        void DestroyInstance() { if (instance != nullptr && !instance->inMainLoop) instance = nullptr; }

        /// @brief Returns the rasterizer instance, available only in OpenGL rendering mode.
        static
        Rasterizer* GetRasterizer() { return Instance()->rasterizer.get(); }

        /// @brief Returns the raytracer instance, always availabe.
        static
        IRaytracer* GetRaytracer() { return Instance()->raytracer.get(); }

        /// @brief Returns the current rendering mode.
        /// @see RTApp::RenderingMode
        static
        RenderingMode GetRenderingMode() { return renderingMode; }

        /// @brief Returns the time in seconds since the last frame.
        static
        double GetTimeDelta() { return Instance()->timeDelta; }

        /// @brief Returns true if the frame tracer is currently running.
        static
        bool IsFrameTracerRunning() { return (Instance()->frameTracer != nullptr && Instance()->frameTracer->running.load()); }

        /// @brief If set to true, the frame tracer can be run.
        static
        void SetFrameTracerEnabled(bool newValue);

        static
        SceneConfig const& AppConfig() { return appConfig; }

        static
        bool ShowGUI() { return showGUI; }

        static
        void InitializeStatic(RenderingMode renderingMode_, bool showGUI_, int stopFrame_, int saveFrame_, AssetID&& appConfigPath);

        static
        int64 FramesSinceStart() { return Instance()->frame; }

    public:
        bool runFrameTracerInSeparateThread{ true };

        TableStatistics statistics;

        /// @brief Enters the main loop
        void EnterMainLoop();

        /// @brief Returns the main window.
        vkfw::UniqueHandle<vkfw::Window> const& GetMainWindow();

        ~RTApp();

    private:
        /// @brief Class for running a raytracer to raytrace a single frame in another thread.
        class FrameTracer
        {
            using Stage = FrameTracerStage;
        public:
            /// @brief Constructs the frame tracer from a raytracer, which it will use.
            /// @param raytracer Raytracer that will be used to trace the frame
            FrameTracer(IRaytracer* raytracer, bool runFrameTracerInSeparateThread);

            /// @brief Raytracer used to trace the frame.
            IRaytracer* raytracer;
            /// @brief Run progress
            std::atomic<Stage> stage{ Stage::Default };
            /// @brief True when the raytracing is running, false when it is finished.
            std::atomic<bool> running{ false };
            /// @brief True if the frame tracer can be run, false otherwise.
            bool enabled{ true };
            /// @brief True if the frame tracer should run in a separate thread.
            bool runFrameTracerInSeparateThread;
            /// @brief Thread used to run this frame tracer.
            std::jthread thread{};

            bool renderOpen{ false };
            
            /// @brief Runs the frame tracer in another thread.
            /// @param stopStage Stage at which the frame tracer should stop running.
            void Run(Stage stopStage);
        };

    private:
        inline static
        RenderingMode renderingMode{ RenderingMode::OpenGL };

        inline static
        bool showGUI{true};

        inline static
        int64 stopFrame{-1};

        inline static
        int64 saveFrame{-1};

        inline static
        SceneConfig appConfig{};

        inline static
        std::unique_ptr<RTApp> instance{};

    private:
        struct GLFWData;

        std::unique_ptr<GLFWData> glfw;

        std::unique_ptr<Rasterizer> rasterizer{ nullptr };
        std::unique_ptr<IRaytracer> raytracer{ nullptr };

        std::chrono::steady_clock::time_point lastTimePoint{};
        cfloat timeDelta{ 0.0 };

        std::unique_ptr<FrameTracer> frameTracer{ nullptr };

        int64 frame{};

        bool inMainLoop{false};

        RTApp();

        void OpenGLMainLoop();

        void RaytracingMainLoop();

        void RaytraceFrame();

        bool ShouldExit();

        void OnExit();
    };
}
