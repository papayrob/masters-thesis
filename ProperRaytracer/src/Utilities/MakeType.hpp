/**
 * File name: MakeType.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

namespace ProperRT
{
    template<class TSource, class TTarget>
    struct MakeType
    {
        using Type = TTarget;
    };

    template<class TSource, class TTarget>
    using MakeType_t = MakeType<TSource, TTarget>::Type;
}
