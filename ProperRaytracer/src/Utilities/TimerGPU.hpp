/**
 * File name: TimerGPU.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2022 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <cuda_runtime_api.h>
#include "Timer.hpp"

namespace ProperRT
{
    /// @brief Struct representing a timer for GPU kernels.
    struct TimerGPU : public Timer
    {
        using Base = Timer;
    public:
        TimerGPU();

        ~TimerGPU();

        void Start();

        void Stop(int32_t runs = 1);

        void Reset();

        void Restart();

        void Pause();

        void Continue();

        Clock::duration Elapsed();

        DurationNanoReal Average();

        /// @brief Returns the elapsed time as a string.
        /// @param outputPrecision Desired precision of the output
        /// @return Elapsed time as a string
        std::string GetElapsedString(Precision outputPrecision = Precision::Closest);

        /// @brief Returns the average time over all runs of the timer as a string.
        /// @details Run of the timer is defined as the time elapsed between a call to Start() and Stop(), including pauses.
        /// @param outputPrecision Desired precision of the output
        /// @return Average time as a string
        std::string GetAverageTimeString(Precision outputPrecision = Precision::Closest);

    protected:
        cudaEvent_t startEvent;
        cudaEvent_t stopEvent;

        bool synced{ true };

        DurationMilliReal SyncEvent(cudaEvent_t start, cudaEvent_t stop);

        void Sync();
    };
}
