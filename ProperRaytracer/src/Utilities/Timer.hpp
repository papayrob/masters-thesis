/**
 * File name: Timer.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2023 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <chrono>
#include <iostream>

namespace ProperRT
{
    /// @brief Struct representing a timer.
    struct Timer
    {
    public:
        /// @brief Enum for differentiating desired output precision.
        enum class Precision
        {
            Closest, Seconds, Milliseconds, Microseconds, Nanoseconds
        };

        /// @brief Seconds represented with a double
        using DurationReal = std::chrono::duration<double>;
        /// @brief Milliseconds represented with a double
        using DurationMilliReal = std::chrono::duration<double, std::milli>;
        /// @brief Microseconds represented with a double
        using DurationMicroReal = std::chrono::duration<double, std::micro>;
        /// @brief Nanoseconds represented with a double
        using DurationNanoReal = std::chrono::duration<double, std::nano>;

        using Clock = std::chrono::steady_clock;

        /// @brief Starts this timer.
        void Start() {
            if (!started) {
                started = true;
                elapsedTime = Clock::duration::zero();
                timePoint = Now();
            }
        }

        /// @brief Stops this timer.
        /// @param runs Pass a value higher than 1 if the timed algorithm was run more than 1 time between calling Start() and Stop()
        void Stop(int32_t runs = 1) {
            if (started) {
                if (!paused) {
                    elapsedTime += Now() - timePoint;
                }
                accumulatedTime += elapsedTime;
                runN += runs;
                started = false;
                paused = false;
            }
        }

        /// @brief Resets this timer.
        void Reset() {
            elapsedTime = Clock::duration::zero();
            accumulatedTime = Clock::duration::zero();
            runN = 0;
            started = false;
            paused = false;
        }

        /// @brief Restarts this timer (resetting and starting it).
        void Restart() {
            Reset();
            Start();
        }

        /// @brief Pauses the timer.
        void Pause() {
            if (started && !paused) {
                elapsedTime += Now() - timePoint;
                paused = true;
            }
        }

        /// @brief Continues the timer.
        void Continue() {
            if (started && paused) {
                timePoint = Now();
                paused = false;
            }
        }

        Clock::duration Elapsed() const {
            return (started ? Clock::duration{-1} : elapsedTime);
        }

        DurationNanoReal Average() const {
            return std::chrono::duration_cast<DurationNanoReal>(accumulatedTime) / static_cast<double>(runN);
        }

        /// @brief Returns the elapsed time as a string.
        /// @param outputPrecision Desired precision of the output
        /// @return Elapsed time as a string
        std::string GetElapsedString(Precision outputPrecision = Precision::Closest) const {
            if (started) {
                return "Timer is still running!";
            }
            return ToString(elapsedTime, outputPrecision);
        }

        /// @brief Returns the average time over all runs of the timer as a string.
        /// @details Run of the timer is defined as the time elapsed between a call to Start() and Stop(), including pauses.
        /// @param outputPrecision Desired precision of the output
        /// @return Average time as a string
        std::string GetAverageTimeString(Precision outputPrecision = Precision::Closest) const {
            if (started) {
                return "Timer is still running!";
            }
            return ToString(std::chrono::duration_cast<DurationNanoReal>(accumulatedTime) / static_cast<double>(runN), outputPrecision);
        }

        static
        std::string ToString(auto duration, Precision outputPrecision) {
            std::stringstream ss{};
            using namespace std::chrono_literals;

            if (outputPrecision == Precision::Closest) {
                if (duration > 0.1s) {
                    outputPrecision = Precision::Seconds;
                }
                else if (duration > 0.1ms) {
                    outputPrecision = Precision::Milliseconds;
                }
                else if (duration > 0.1us) {
                    outputPrecision = Precision::Microseconds;
                }
                else {
                    outputPrecision = Precision::Nanoseconds;
                }
            }

            switch (outputPrecision)
            {
            case Precision::Seconds: {
                ss << std::chrono::duration_cast<DurationReal>(duration);
                break;
            }
            case Precision::Milliseconds: {
                ss << std::chrono::duration_cast<DurationMilliReal>(duration);
                break;
            }
            case Precision::Microseconds: {
                ss << std::chrono::duration_cast<DurationMicroReal>(duration);
                break;
            }
            case Precision::Nanoseconds:
                ss << std::chrono::duration_cast<DurationNanoReal>(duration);
                break;
            }

            return ss.str();
        }

    protected:
        std::chrono::time_point<Clock> timePoint;
        Clock::duration elapsedTime{ Clock::duration::zero() };
        Clock::duration accumulatedTime{ Clock::duration::zero() };
        int32_t runN{ 0 };
        bool started{ false };
        bool paused{ false };

        static 
        std::chrono::time_point<Clock> Now() {
            return Clock::now();
        }

        Clock::duration ToTimerClockDuration(auto duration) const {
            return std::chrono::duration_cast<Clock::duration>(duration);
        }
    };

}
