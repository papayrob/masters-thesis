/**
 * File name: Serializers.hpp
 * Description: This file contains custom serializers for nlohmann::json.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2022 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "nlohmann/json.hpp"

#include "RTApp.hpp"
#include "Geometry/Vector.hpp"
#include "SceneDefinition/Scene.hpp"

namespace ProperRT
{
    template<int NDim, class T>
    void to_json(nlohmann::json& json, const Vector<NDim, T>& value) {
        json = nlohmann::json::array();
        for (int i = 0; i < NDim; ++i) {
            json.push_back(value[i]);
        }
    }

    template<int NDim, class T>
    void from_json(nlohmann::json const& json, Vector<NDim, T>& value) {
        for (int i = 0; i < NDim; ++i) {
            json.at(i).get_to(value[i]);
        }
    }
}

NLOHMANN_JSON_NAMESPACE_BEGIN

template <typename T>
struct adl_serializer<std::unique_ptr<T>> {
    static void from_json(json const& j, std::unique_ptr<T>& value) {
        value = T::Deserialize(j);
    }
};

template <typename T>
struct adl_serializer<std::shared_ptr<T>> {
    static void from_json(json const& j, std::shared_ptr<T>& value) {
        value = T::Deserialize(j);
    }
};

template<class... T>
struct adl_serializer<std::variant<T...>> {
    static
    void to_json(nlohmann::json& json, std::variant<T...> const& value) {
        std::visit([&json] (auto const& v) { json = v; }, value);
    }
};

NLOHMANN_JSON_NAMESPACE_END
