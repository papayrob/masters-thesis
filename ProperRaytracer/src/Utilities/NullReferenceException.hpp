/**
 * File name: NullReferenceException.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2023 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <exception>
#include <string>
#include <sstream>

namespace ProperRT
{
    /// @brief Exception class, thrown when trying to dereference nullptr.
    class NullReferenceException : public std::runtime_error
    {
    public:
        using std::runtime_error::runtime_error;
    };
}
