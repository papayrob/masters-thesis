/**
 * File name: TimerGPU.cu
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "TimerGPU.hpp"

namespace ProperRT
{
    TimerGPU::TimerGPU() {
        cudaEventCreate(&startEvent);
        cudaEventCreate(&stopEvent);
    }

    TimerGPU::~TimerGPU() {
        cudaEventDestroy(startEvent);
        cudaEventDestroy(stopEvent);
    }

    void TimerGPU::Start() {
        if (!started) {
            // Sync if called after stop
            if (!synced) {
                Sync();
            }

            started = true;
            elapsedTime = std::chrono::steady_clock::duration::zero();
            cudaEventRecord(startEvent);
        }
    }

    void TimerGPU::Stop(int32_t runs) {
        if (started) {
            // Called after start or continue
            if (synced) {
                cudaEventRecord(stopEvent);
                synced = false;
            }
            // if called after pause, stop is already recorded

            runN += runs;
            started = false;
            paused = false;
        }
    }
    
    void TimerGPU::Reset() {
        synced = true;
        Base::Reset();
    }

    void TimerGPU::Restart() {
        Base::Restart();
    }

    void TimerGPU::Pause() {
        if (started && !paused) {
            cudaEventRecord(stopEvent);
            synced = false;
            paused = true;
        }
    }

    void TimerGPU::Continue() {
        if (started && paused) {
            // Sync after pause
            elapsedTime += ToTimerClockDuration(SyncEvent(startEvent, stopEvent));
            synced = true;

            cudaEventRecord(startEvent);
            paused = false;
        }
    }

    TimerGPU::Clock::duration TimerGPU::Elapsed() {
        if (started) {
            return Clock::duration{-1};
        }

        if (!synced) {
            Sync();
        }

        return elapsedTime;
    }

    TimerGPU::DurationNanoReal TimerGPU::Average() {
        if (started) {
            return Clock::duration{-1};
        }

        if (!synced) {
            Sync();
        }

        return Base::Average();
    }

    std::string TimerGPU::GetElapsedString(Precision outputPrecision) {
        if (started) {
            return "Timer is still running!";
        }

        if (!synced) {
            Sync();
        }

        return Base::GetElapsedString(outputPrecision);
    }

    std::string TimerGPU::GetAverageTimeString(Precision outputPrecision) {
        if (started) {
            return "Timer is still running!";
        }

        if (!synced) {
            Sync();
        }

        return Base::GetAverageTimeString(outputPrecision);
    }

    Timer::DurationMilliReal TimerGPU::SyncEvent(cudaEvent_t start, cudaEvent_t stop) {
        cudaEventSynchronize(stop);
        float milliseconds;
        cudaEventElapsedTime(&milliseconds, start, stop);
        return static_cast<DurationMilliReal>(milliseconds);
    }
    
    void TimerGPU::Sync() {
        elapsedTime += ToTimerClockDuration(SyncEvent(startEvent, stopEvent));
        accumulatedTime += elapsedTime;
        synced = true;
    }
}
