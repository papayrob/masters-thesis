/**
 * File name: TableStatistics.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <map>
#include <variant>
#include <vector>
#include <span>
#include <string>
#include <chrono>
#include <concepts>

namespace ProperRT
{
    /// @brief Class for storing statistics and saving them to a JSON file.
    class TableStatistics
    {
    public:
        void AddInfo(std::string const& infoName, int64_t value);

        void AddInfo(std::string const& infoName, int32_t value) {
            AddInfo(infoName, int64_t{value});
        }

        void AddInfo(std::string const& infoName, double value);

        void AddInfo(std::string const& infoName, float value) {
            AddInfo(infoName, double{value});
        }

        void AddInfo(std::string const& infoName, std::string const& value);

        void AddData(std::string const& statisticName, int64_t data);

        void AddData(std::string const& statisticName, int32_t data) {
            AddData(statisticName, int64_t{data});
        }

        void AddData(std::string const& statisticName, double data);

        void AddData(std::string const& statisticName, float data) {
            AddData(statisticName, double{data});
        }

        template<std::integral TRep, class TPeriod>
        void AddData(std::string const& statisticName, std::chrono::duration<TRep, TPeriod> data) {
            AddData(statisticName, std::chrono::duration_cast<std::chrono::duration<int64_t, std::nano>>(data).count());
        }

        template<std::floating_point TRep, class TPeriod>
        void AddData(std::string const& statisticName, std::chrono::duration<TRep, TPeriod> data) {
            AddData(statisticName, std::chrono::duration_cast<std::chrono::duration<double, std::nano>>(data).count());
        }

        void AddData(std::string const& statisticName, std::vector<int64_t>&& data);

        void AddData(std::string const& statisticName, std::vector<double>&& data);

        void SaveToDisk(std::string const& fileName);

    private:
        using InfoVariant = std::variant<int64_t, double, std::string>;

        using DataVariant = std::variant<
            std::vector<int64_t>,
            std::vector<double>,
            std::vector<std::vector<int64_t>>,
            std::vector<std::vector<double>>
        >;

    private:
        std::vector<std::pair<std::string, InfoVariant>> info;
        std::map<std::string, DataVariant> statistics;

        template<class T>
        void AddInfoInternal(std::string const& infoName, T value);

        template<class T>
        void AddDataInternal(std::string const& statisticName, T data);

        template<class T>
        void AddDataInternal(std::string const& statisticName, std::vector<T>&& data);
    };
}
