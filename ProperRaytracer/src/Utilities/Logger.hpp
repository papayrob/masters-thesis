/**
 * File name: Logger.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2022 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */

#pragma once

#include <iostream>
#include <fstream>
#include <source_location>
#include <unordered_map>
#include <format>

namespace ProperRT
{
    /// @brief Class for logging.
    class Logger
    {
    public:
        /// @brief Different levels of output, each one with its setting of when it is enabled and its output stream.
        enum class Level
        {
            Debug, Info, Warning, Error, PerfTest
        };

    public:
        static inline constexpr
        bool ENABLE_LOGGING = 
#ifdef PRT_LOG_ENABLED
            true;
#else
            false;
#endif

        /// @brief True if building in debug config, false otherwise.
        static inline constexpr
        bool LOG_DEBUG =
#ifdef PRT_DEBUG
            true;
#else
            false;
#endif
        
        /// @brief True if Info level should be logged.
        static inline
        bool logInfo = true;

        /// @brief True if tests are running, false otherwise.
        static inline
        bool logPerformance = false;

        static inline
        std::ostream* debugOutput{};

        static inline
        std::ostream* infoOutput{};

        static inline
        std::ostream* warningOutput{};

        static inline
        std::ostream* errorOutput{};

        static inline
        std::ostream* testOutput{};

    public:

        static
        void LogDebug(std::string const& message, bool printIdentifier = true) {
            if constexpr (LOG_DEBUG) {
                if (debugOutput != nullptr) {
                    (*debugOutput) << (printIdentifier ? "[Debug] " : "") << message << std::endl;
                }
            }
        }

        static
        void LogInfo(std::string const& message, bool printIdentifier = true) {
            if (logInfo && infoOutput != nullptr) {
                (*infoOutput) << (printIdentifier ? "[Info] " : "") << message << std::endl;
            }
        }

        static
        void LogWarning(std::string const& message) {
            if (warningOutput != nullptr) {
                (*warningOutput) << "[Warning] !! " << message << std::endl;
            }
        }

        static
        void LogError(std::string const& message, std::source_location const& location = std::source_location::current()) {
            if (errorOutput != nullptr) {
                (*errorOutput) << "At ";
                PrintSourceLocation(*errorOutput, location) << ":\n[Error] !!!! " << message << std::endl;
            }
        }

        static
        void LogTest(std::string const& message, bool printIdentifier = true) {
            if (logPerformance && testOutput != nullptr) {
                (*testOutput) << (printIdentifier ? "[Test] " : "") << message << std::endl;
            }
        }

        /// @brief Closes all logging streams and open files. Should be called before forcefully exitting to guarantee write-through.
        static
        void CloseStreams() {
            if constexpr (LOG_DEBUG) {
                (*debugOutput) << std::flush;
            }
            if (logInfo) {
                (*infoOutput) << std::flush;
            }
            (*warningOutput) << std::flush;
            (*errorOutput) << std::flush;
            if (logPerformance) {
                (*testOutput) << std::flush;
            }

            openFiles.clear();
        }

        static
        std::ostream& PrintSourceLocation(std::ostream& ostream, std::source_location const& location) {
            ostream << location.file_name() << "(" << location.line() << ") `" << location.function_name() << "`";
            return ostream;
        }

        static
        void SetAllOutputs(std::ostream* output) {
            debugOutput = infoOutput = warningOutput = errorOutput = testOutput = output;
        }

        static
        void OutputAllToFile(std::string const& fileName = "prt.log") {
            debugOutput = infoOutput = warningOutput = errorOutput = testOutput = CreateLogFile(fileName);
        }

        static
        void OutputDebugToFile(std::string const& fileName = "debug.log") {
            debugOutput = CreateLogFile(fileName);
        }

        static
        void OutputInfoToFile(std::string const& fileName = "info.log") {
            infoOutput = CreateLogFile(fileName);
        }

        static
        void OutputWarningToFile(std::string const& fileName = "warning.log") {
            warningOutput = CreateLogFile(fileName);
        }

        static
        void OutputErrorToFile(std::string const& fileName = "error.log") {
            errorOutput = CreateLogFile(fileName);
        }

        static
        void OutputTestToFile(std::string const& fileName = "test.log") {
            testOutput = CreateLogFile(fileName);
        }

        static
        std::ostream* CreateLogFile(std::string const& fileName);

    private:
        static inline
        std::unordered_map<std::string, std::ofstream> openFiles;
    };
}
