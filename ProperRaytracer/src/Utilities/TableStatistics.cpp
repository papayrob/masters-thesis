/**
 * File name: TableStatistics.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "TableStatistics.hpp"

#include <fstream>

#include "Serializers.hpp"
#include "SceneDefinition/AssetManager.hpp"

namespace ProperRT
{
    namespace {
        struct DataVisitor
        {
            nlohmann::json& array;

            template<class T>
            void operator()(std::vector<T> const& vec) {
                for (T const& val : vec) {
                    array += val;
                }
            }
        };
    }

    void TableStatistics::AddInfo(std::string const& infoName, int64_t value) {
        AddInfoInternal(infoName, value);
    }

    void TableStatistics::AddInfo(std::string const& infoName, double value) {
        AddInfoInternal(infoName, value);
    }

    void TableStatistics::AddInfo(std::string const& infoName, std::string const& value) {
        AddInfoInternal(infoName, value);
    }

    void TableStatistics::AddData(std::string const& statisticName, int64_t data) {
        AddDataInternal(statisticName, data);
    }

    void TableStatistics::AddData(std::string const& statisticName, double data) {
        AddDataInternal(statisticName, data);
    }

    void TableStatistics::AddData(std::string const& statisticName, std::vector<int64_t>&& data) {
        AddDataInternal(statisticName, std::move(data));
    }

    void TableStatistics::AddData(std::string const& statisticName, std::vector<double>&& data) {
        AddDataInternal(statisticName, std::move(data));
    }

    void TableStatistics::SaveToDisk(std::string const &fileName) {
        auto path = AssetManager::executablePath.parent_path() / "Tables";
        std::filesystem::create_directory(path);
        path /= fileName;

        // Load config file
        nlohmann::json j;

        if (std::filesystem::exists(path)) {
            std::ifstream fileStream{path};
            if (!fileStream.is_open()) {
                //TODO log error "Could not open scene config file"
            }
            //TODO try catch
            j = { nlohmann::json::parse(fileStream, nullptr, true, true) };
        }
        else {
            j = {
                { "info", {} },
                { "data", {} }
            };
        }

        // Info
        auto& infoJson = j.at("info");
        for (auto const& [infoName, infoValue] : info) {
            infoJson.emplace(infoName, infoValue);
        }

        // Values
        auto& dataJson = j.at("data");
        for (auto const& [statName, statValue] : statistics) {
            if (!dataJson.contains(statName)) {
                dataJson.emplace(statName, nlohmann::json::array());
            }

            auto& statArrayJson = dataJson[statName];
            nlohmann::json newJson{ statValue };
            statArrayJson.insert(statArrayJson.end(), newJson.at(0).begin(), newJson.at(0).end());
            //std::visit(DataVisitor{ statArrayJson }, statValue);
        }

        {
            std::ofstream fileStream{path};
            fileStream << j;
        }
    }

    template <class T>
    void TableStatistics::AddInfoInternal(std::string const& infoName, T value) {
        info.push_back(std::make_pair(infoName, value));
    }

    template <class T>
    void TableStatistics::AddDataInternal(std::string const& statisticName, T data) {
        if (!statistics.contains(statisticName)) {
            statistics.insert({ statisticName, std::vector<T>{} });
        }
        std::get<std::vector<T>>(statistics[statisticName]).push_back(data);
    }

    template<class T>
    void TableStatistics::AddDataInternal(std::string const& statisticName, std::vector<T>&& data) {
        if (!statistics.contains(statisticName)) {
            statistics.insert({ statisticName, std::vector<std::vector<T>>{} });
        }

        std::get<std::vector<std::vector<T>>>(statistics[statisticName]).push_back(std::move(data));
    }
}
