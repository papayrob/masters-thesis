/**
 * File name: MathException.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <stdexcept>

namespace ProperRT
{
    /// @brief Exception class, thrown when a math computation error happens.
    class MathException : public std::runtime_error
    {
    public:
        using std::runtime_error::runtime_error;
    };
}
