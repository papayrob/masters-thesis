/**
 * File name: IsCVQualified.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <type_traits>

namespace ProperRT
{
    /// @brief Class for checking if \p T is qualified with const and/or volatile.
    /// @tparam T Type to check
    template<class T>
    struct IsCVQualified : std::false_type {};

    /// @brief Specialization of IsCVQualified for volatile types.
    template<class T>
    struct IsCVQualified<T volatile> : std::true_type {};

    /// @brief Specialization of IsCVQualified for const types.
    template<class T>
    struct IsCVQualified<T const> : std::true_type {};

    /// @brief Specialization of IsCVQualified for const volatile types.
    template<class T>
    struct IsCVQualified<T const volatile> : std::true_type {};

    /// @brief Shortcut to the boolean value of IsCVQualified.
    /// @tparam T Type to check
    template<class T>
    inline constexpr bool IsCVQualified_v = IsCVQualified<T>::value;

    template<class T>
    concept CVQualifiedType = IsCVQualified_v<T>;
}