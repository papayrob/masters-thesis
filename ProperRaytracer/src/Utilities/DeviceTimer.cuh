/**
 * File name: DeviceTimer.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <cuda_runtime.h>
#include <sstream>
#include <vector>

#include "Compute/DevicePointer.cuh"

namespace ProperRT
{
    /// @brief Struct representing a timer to be used in device code.
    struct DeviceTimer
    {
    public:
        using ClockType = long long;

        DevicePointer<ClockType[]> timePoints;
        DevicePointer<ClockType[]> elapsedTimes;
        DevicePointer<ClockType[]> accumulatedTimes;
        DevicePointer<int32[]> runsN;

        /// @brief Starts this timer.
        __device__
        void Start(int threadID) {
            elapsedTimes[threadID] = 0;
            timePoints[threadID] = Now();
        }

        /// @brief Stops this timer.
        /// @param runs Pass a value higher than 1 if the timed algorithm was run more than 1 time between calling Start() and Stop()
        __device__
        void Stop(int threadID, int32 runs = 1) {
            elapsedTimes[threadID] += Now() - timePoints[threadID];
            accumulatedTimes[threadID] += elapsedTimes[threadID];
            runsN[threadID] += runs;
        }

        /// @brief Resets this timer.
        __device__
        void Reset(int threadID) {
            elapsedTimes[threadID] = 0;
            accumulatedTimes[threadID] = 0;
            runsN[threadID] = 0;
        }

        /// @brief Restarts this timer (resetting and starting it).
        __device__
        void Restart(int threadID) {
            Reset(threadID);
            Start(threadID);
        }

        __device__
        ClockType Now() {
            return clock64();
        }
    };

    class DeviceUniqueTimer
    {
    public:
        using ClockType = DeviceTimer::ClockType;

        DeviceUniqueTimer() = default;

        DeviceUniqueTimer(int threadN_)
        {
            SetThreadCount(threadN);
        }

        void SetThreadCount(int threadCount) {
            if (threadCount <= 0) {
                threadN = threadCount;
                timePoints = nullptr;
                elapsedTimes = nullptr;
                accumulatedTimes = nullptr;
                runsN = nullptr;
            }
            else if (threadN != threadCount) {
                threadN = threadCount;
                timePoints = DeviceUniquePointer<ClockType[]>::AllocateAndSet(threadN, 0);
                elapsedTimes = DeviceUniquePointer<ClockType[]>::AllocateAndSet(threadN, 0);
                accumulatedTimes = DeviceUniquePointer<ClockType[]>::AllocateAndSet(threadN, 0);
                runsN = DeviceUniquePointer<int32[]>::AllocateAndSet(threadN, 0);
            }
        }

        DeviceTimer Get() const {
            return DeviceTimer{
                timePoints.Get(),
                elapsedTimes.Get(),
                accumulatedTimes.Get(),
                runsN.Get()
            };
        }

        std::string GetElapsedString() {
            if (threadN <= 0) {
                return "";
            }

            std::vector<ClockType> elapsed = elapsedTimes.Download();

            std::stringstream ss;
            ss << '[';
            ss << std::to_string(elapsed[0]);
            for (int i = 1; i < threadN; ++i) {
                ss << ", " << std::to_string(elapsed[i]);
            }
            ss << ']';
            return ss.str();
        }

        std::string GetAccumulatedString() {
            if (threadN <= 0) {
                return "";
            }

            std::vector<ClockType> accumulated = accumulatedTimes.Download();

            std::stringstream ss;
            ss << '[';
            ss << std::to_string(accumulated[0]);
            for (int i = 1; i < threadN; ++i) {
                ss << ", " << std::to_string(accumulated[i]);
            }
            ss << ']';
            return ss.str();
        }

        std::string GetAverageTimeString() {
            if (threadN <= 0) {
                return "";
            }

            std::vector<ClockType> accumulated = accumulatedTimes.Download();
            std::vector<int32> runs = runsN.Download();

            std::stringstream ss;
            ss << '[';
            ss << std::to_string(GetAverage(accumulated[0], runs[0]));
            for (int i = 1; i < threadN; ++i) {
                ss << ", " << std::to_string(GetAverage(accumulated[i], runs[i]));
            }
            ss << ']';
            return ss.str();
        }
    private:
        DeviceUniquePointer<ClockType[]> timePoints{};
        DeviceUniquePointer<ClockType[]> elapsedTimes{};
        DeviceUniquePointer<ClockType[]> accumulatedTimes{};
        DeviceUniquePointer<int32[]> runsN{};
        int threadN{ 0 };

        double GetAverage(ClockType accumulated, ClockType runs) {
            return ((runs == 0) ? (0.0) : (static_cast<double>(accumulated) / static_cast<double>(runs)));
        }
    };

}
