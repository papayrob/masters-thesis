/**
 * File name: AdvancedUtilities.hpp
 * Description: Utility functions and variables.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "Utilities.hpp"
#include "Geometry/Vector.hpp"

namespace ProperRT
{
    constexpr inline
	Vector2i FitInsideKeepRatio(Vector2i const& imageSize, Vector2i const& windowSize) {
        double ratio = static_cast<double>(imageSize.x()) / static_cast<double>(imageSize.y());
        if (int width = static_cast<int>(ratio * static_cast<double>(windowSize.y())); width <= windowSize.x()) {
            return Vector2i{ width, windowSize.y() };
        }
        else {
            return Vector2i{ windowSize.x(), static_cast<int>(1.0 / ratio * static_cast<double>(windowSize.x())) };
        }
    }
}
