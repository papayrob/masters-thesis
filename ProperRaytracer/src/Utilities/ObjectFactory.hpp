/**
 * File name: ObjectFactory.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2023 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <unordered_map>
#include <new>
#include <type_traits>

namespace ProperRT
{
    /// @brief Factory class for creating objects derived from @p TBase based on a key of type @p TKey.
    /// @tparam TKey Type of the key
    /// @tparam TBase Type of the base class
    template<typename TKey, typename TBase>
    class ObjectFactory {
    public:
        /// @brief Registers the class of type @tpatam T with @p key. The last registration is the valid one.
        /// @tparam T Type of the class to register
        /// @param key Key used when creating the class
        template<class T>
        static
        void Register(TKey const& key) {
            ObjectCreatorMap().insert(std::make_pair(key, &Create<T>));
        }

        /// @brief Creates an object based on @p key, returned as the type of the base class.
        /// @param key Key of the class that should be created
        /// @return Pointer to the allocated object
        static
        TBase* Create(TKey const& key) {
            auto it = ObjectCreatorMap().find(key);
            return (it == ObjectCreatorMap().end()) ? nullptr : (it->second)();
        }

    private:
        using ObjectCreator = TBase * (*)();
        using MapType = std::unordered_map<TKey, ObjectCreator>;

        inline static
        typename std::aligned_storage<sizeof(MapType), alignof(MapType)>::type objCreatorMapStorage;

        static
        MapType& ObjectCreatorMap() {
            static bool mapCreated = false;
            if (!mapCreated) {
                new (&objCreatorMapStorage) MapType();
                mapCreated = true;
            }
            return reinterpret_cast<MapType&>(objCreatorMapStorage);
        }

        template<class T>
        static
        TBase* Create() { return static_cast<TBase*>(new T); }
    };
}
