/**
 * File name: Logger.cpp
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "Logger.hpp"

#include "SceneDefinition/AssetManager.hpp"

namespace ProperRT
{
    std::ostream* Logger::CreateLogFile(std::string const& fileName) {
        if (auto it = openFiles.find(fileName); it != openFiles.end()) {
            return &(it->second);
        }

        //TODO add try catch
        try
        {
            auto logDir = AssetManager::executablePath.parent_path() / "Logs";
            std::filesystem::create_directory(logDir);
            auto& logFile = openFiles[fileName];
            logFile = std::ofstream{ logDir / fileName };
            return &logFile;
        }
        catch(std::filesystem::filesystem_error const& fse)
        {
            LogError(std::format("Could not create Logs directory: {}", fse.what()));
            throw;
        }
    }
}
