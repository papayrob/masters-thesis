/**
 * File name: stb_impl.cpp
 * Description: Implementation file for stb library.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2023 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"
