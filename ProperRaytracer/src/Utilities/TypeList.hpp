/**
 * File name: TypeList.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

namespace ProperRT
{
    /// @brief Template metaprogramming class for storing a list of types.
    /// Main use intended for use together with @ref PassTypeList and similar classes.
    template<class... Ts>
    struct TypeList{};

    /// @brief Template metaprogramming class for passing a type list to another class.
    /// @details The first type argument must be a templated type that takes the rest
    /// of the type arguments as its own argument. Then must follow a list of types,
    /// or the intended use for this class, a TypeList that contains the list of types
    /// meant to be passed to the first type argument. The resulting type is aliased
    /// as Type inside the class.
    template<template<class> class TPassTo, class... Ts>
    struct PassTypeList
    {
        using Type = TPassTo<Ts...>;
    };

    /// @brief Template metaprogramming class for passing a type list to another class.
    /// @details The first type argument must be a templated type that takes the rest
    /// of the type arguments as its own argument. Then must follow a list of types,
    /// or the intended use for this class, a TypeList that contains the list of types
    /// meant to be passed to the first type argument. The resulting type is aliased
    /// as Type inside the class.
    template<template<class> class TPassTo, class... Ts>
    struct PassTypeList<TPassTo, TypeList<Ts...>>
    {
        using Type = TPassTo<Ts...>;
    };

    /// @brief Type alias for PassTypeList.
    template<template<class> class TPassTo, class T>
    using PassTypeList_t = PassTypeList<TPassTo, T>::Type;
}
