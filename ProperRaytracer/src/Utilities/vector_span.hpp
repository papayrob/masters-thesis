/**
 * File name: vector_span.hpp
 * Description: Non-owning view into std::vector specifically.
 * Imitates std::span and can be easily converted into one, but
 * has some additional functionality coming from knowing
 * the underlying vector, like staying valid after vector reallocation
 * and knowing the offset from the start of the underlying vector.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <vector>
#include <span>

namespace std
{
	template <typename T>
	class vector_span
	{
	public:
		vector_span(vector<T>& vector) noexcept
			: ref{ vector }, m_offset{ 0 }, m_count{ vector.size() }
		{
		}

		vector_span subspan(size_t offset_, size_t count_) const noexcept {
			vector_span s{ ref };
			s.m_offset = offset_;
			s.m_count = count_;
			return s;
		}

		vector_span first(size_t count_) const noexcept {
			return vector_span{ ref }.subspan(m_offset, count_);
		}

		vector_span last(size_t count_) const noexcept {
			return vector_span{ ref }.subspan(m_offset + m_count - count_, count_);
		}

		operator vector_span<T const>() const noexcept {
			return vector_span<T const>{ ref }.subspan(m_offset, m_count);
		}

		operator span<T>() const noexcept {
			return span<T>{ ref.begin() + m_offset, m_count };
		}

		span<T> as_span() const noexcept {
			return span<T>{ ref.begin() + m_offset, m_count };
		}

		bool references(vector<T> const& vector) const noexcept {
			return ref.data() == vector.data();
		}

		size_t offset() const noexcept {
			return m_offset;
		}

		size_t size() const noexcept {
			return m_count;
		}

		T& operator[](size_t index) const noexcept {
			return span<T>(ref.begin() + m_offset, m_count).operator[](index);
		}

		auto begin() const noexcept {
			return ref.begin() + m_offset;
		}

		auto end() const noexcept {
			return ref.begin() + m_offset + m_count;
		}

		auto cbegin() const noexcept {
			return ref.cbegin() + m_offset;
		}

		auto cend() const noexcept {
			return ref.cbegin() + m_offset + m_count;
		}

	private:
		vector<T>& ref;
		size_t m_offset;
		size_t m_count;
	};

	template <typename T>
	class vector_span<T const>
	{
	public:
		vector_span(vector<T> const& vector) noexcept
			: ref{ vector }, m_offset{ 0 }, m_count{ vector.size() }
		{
		}

		vector_span subspan(size_t offset_, size_t count_) const noexcept {
			vector_span s{ ref };
			s.m_offset = offset_;
			s.m_count = count_;
			return s;
		}

		vector_span first(size_t count_) const noexcept {
			return vector_span{ ref }.subspan(m_offset, count_);
		}

		vector_span last(size_t count_) const noexcept {
			return vector_span{ ref }.subspan(m_offset + m_count - count_, count_);
		}

		operator span<T const>() const noexcept {
			return span<T const>{ ref.cbegin() + m_offset, m_count };
		}

		span<T const> as_span() const noexcept {
			return span<T>{ ref.cbegin() + m_offset, m_count };
		}

		bool references(vector<T> const& vector) const noexcept {
			return ref.data() == vector.data();
		}

		size_t offset() const noexcept {
			return m_offset;
		}

		size_t size() const noexcept {
			return m_count;
		}

		T const& operator[](size_t index) const noexcept {
			return span<T const>(ref.begin() + m_offset, m_count).operator[](index);
		}

		auto begin() const noexcept {
			return ref.cbegin() + m_offset;
		}

		auto end() const noexcept {
			return ref.cbegin() + m_offset + m_count;
		}

	private:
		vector<T> const& ref;
		size_t m_offset;
		size_t m_count;
	};
}
