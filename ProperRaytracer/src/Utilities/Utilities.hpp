/**
 * File name: Utilities.hpp
 * Description: Utility functions and variables.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <bit>
#include <concepts>
#include <numbers>
#include <type_traits>
#include <cuda/std/bit>
#include <cuda/std/concepts>

#include "ProperRT.hpp"

namespace ProperRT
{
    /// @brief Computes ceil(x / y) for unsigned integers.
    /// @tparam T Unsigned integer type
    /// @param x Dividend
    /// @param y Divisor
    /// @return ceil(x / y)
    template<CuSTD::unsigned_integral T>
	CUDA_FUNCTION constexpr inline
	T IntDivRoundUp(T x, T y) noexcept {
		return (x + y - 1) / y;
	}

	/// @brief Computes the floor of base-2 logarithm of an unsigned integer.
	/// @param x Unsigned integer value
    /// @return floor(log2(x))
	template<CuSTD::unsigned_integral T>
	CUDA_FUNCTION constexpr inline
	T IntLog2(T x) noexcept {
		return CuSTD::bit_width(x) - 1;
	}

	/// @brief Computes the ceiling of base-2 logarithm of an unsigned integer.
	/// @param x Unsigned integer value
    /// @return ceil(log2(x))
	template<CuSTD::unsigned_integral T>
	CUDA_FUNCTION constexpr inline
	T IntLog2RoundUp(T x) noexcept {
		return CuSTD::bit_width(x - 1);
	}

	/// @brief Computes the ceiling of base-2^p logarithm of an unsigned integer.
	/// @param x Unsigned integer value
    /// @param p Power to which the base (=2) is raised
    /// @return ceil(log_{2^p}(x))
	template<CuSTD::unsigned_integral T>
	CUDA_FUNCTION constexpr inline
	T IntLog2PowpRoundUp(T x, T p) noexcept {
		return IntDivRoundUp(IntLog2RoundUp(x), p);
	}

	/// @brief Computes the closest power of two above @p x and returns it.
	/// @tparam T Unsigned integral type
	/// @param x Value
	template<CuSTD::unsigned_integral T>
	CUDA_FUNCTION constexpr inline
	T RoundUpToNearestPowerOf2(T x) noexcept {
		return T{ 1 } << IntLog2RoundUp(x);
	}

    namespace Compute
	{
		/// @brief Size of the GPU device subgroup for which the program will be compiled.
		inline static constexpr
		int32 SUBGROUP_SIZE = 32;

		/// @brief Mask for subgroup synchronization with all bits set to 1.
		inline static constexpr
		auto FULL_MASK = ~uint32_t{ 0 };
	}

	/// @brief Returns v.size() cast to T.
	template<class T>
	inline
	T StdSize(auto const& v) {
		return static_cast<T>(v.size());
	}

	/// @brief Converts a floating point number in the range <0,1> into an integer of range <0,int_max>.
	/// @tparam TTarget Integral type to convert to
	/// @param source Floating point number in the range <0,1>
	/// @return Floating point converted from <0,1> to an integer in <0,int_max>
	template<std::integral TTarget>
	inline
	TTarget RealToIntegerNormalized(std::floating_point auto source) {
		constexpr double max = static_cast<double>(std::numeric_limits<TTarget>::max());
		double value = std::round(static_cast<double>(source) * max);
		return static_cast<TTarget>(value);
	}

	/// @brief Converts an integer in the range <0,int_max> into a floating point number of range <0,1>.
	/// @tparam TTarget Floating point type to convert to
	/// @param source Integer in the range <0,int_max>
	/// @return Integer converted from <0,int_max> to a floating point number in <0,1>
	template<std::floating_point TTarget, std::integral TSource>
	inline
	TTarget IntegerToRealNormalized(TSource source) {
		constexpr double max = static_cast<double>(std::numeric_limits<TSource>::max());
		double value = static_cast<double>(source) / max;
		return static_cast<TTarget>(value);
	}

	/// @brief Calls @p F @p NCount times.
	/// @tparam NCount Number of interations
	/// @param f Function to call
	template<std::size_t NCount, class F>
	CUDA_FUNCTION inline
	void Unroll(F&& f) {
		[] <std::size_t... I> (std::index_sequence<I...>, F&& f) {
			(f(I), ...);
		}(std::make_index_sequence<NCount>{}, CuSTD::forward<F>(f));
	}

	/// @brief Calls @p F with a template index parameter with indeces @p ...I.
	/// @tparam ...I Indeces to call @p f with
	/// @param f Function to call
	template<std::size_t... I, class F>
	CUDA_FUNCTION inline
	void UnrollTemplated(std::index_sequence<I...>, F&& f) {
		(f.template operator()<I>(), ...);
	}

	/// @brief Calls @p f with a template index parameter @p NCount times.
	/// @tparam NCount Number of interations
	/// @param f Function to call
	template<std::size_t NCount, class F>
	CUDA_FUNCTION inline
	void UnrollTemplated(F&& f) {
		UnrollTemplated(std::make_index_sequence<NCount>{}, CuSTD::forward<F>(f));
	}

	/// @brief Converts (casts) an enum value to the underlying type.
	/// @tparam TEnum Type of the enum
	/// @param enumValue Enum value to convert
	/// @return @p enumValue cast to underlying type of @p TEnum
	template<class TEnum>
	CUDA_FUNCTION constexpr inline
	std::underlying_type_t<TEnum> ToUnderlying(TEnum enumValue) {
		return static_cast<std::underlying_type_t<TEnum>>(enumValue);
	}

	/// @brief Linearly interpolates between @p source and @p target according to @p alpha.
	/// @details Uses T::LERP if available, otherwise falls back on the default lerp.
	/// @tparam T Type to linearly interpolate
	/// @param source Value to interpolate from
	/// @param target Value to interpolate to
	/// @param alpha Interpolant
	/// @return Interpolated value
	template<class T>
	CUDA_FUNCTION constexpr inline
	T LERP(T const& source, T const& target, std::floating_point auto alpha) {
		if constexpr (requires { { T::LERP(source, target, alpha) } -> std::same_as<T>; }) {
			return T::LERP(source, target, alpha);
		}
		else {
			return (1 - alpha) * source + alpha * target;
		}
	}

	/// @brief Computes radians from @p degrees.
	/// @param degrees Input degrees
	CUDA_FUNCTION constexpr inline
	auto ToRadians(std::floating_point auto degrees) {
		return degrees * static_cast<decltype(degrees)>(std::numbers::pi / 180.0);
	}

	/// @brief Enum for metric system prefixes.
	enum class MetricPrefix {
		None, Kilo, Mega, Giga, Tera
	};

	/// @brief Returns a string with the metric prefix.
	/// @param prefix Prefix to convert to string
	/// @param capitalized If true, the result will be capitlized
	constexpr inline
	std::string ToString(MetricPrefix prefix, bool capitalized = false) {
		switch (prefix)
		{
		case MetricPrefix::Kilo:
			return (capitalized ? "K" : "k");
		case MetricPrefix::Mega:
			return (capitalized ? "M" : "m");
		case MetricPrefix::Giga:
			return (capitalized ? "G" : "g");
		case MetricPrefix::Tera:
			return (capitalized ? "T" : "t");
		default:
			return "";
		}
	}

	/// @brief Ckass for representing a value with a metric unit prefix.
	struct PrefixedValue
	{
		/// @brief Prefix.
		MetricPrefix prefix;
		/// @brief Value.
		double value;
	};

	/// @brief Returns @p sourceValue with a prefix that is best for the value.
	/// @param sourceValue Input value
	constexpr inline
	PrefixedValue ClosestPrefix(PrefixedValue sourceValue) {
		auto prefix = ToUnderlying(sourceValue.prefix);
		while (sourceValue.value >= 1000.0) {
			sourceValue.value /= 1000.0;
			++prefix;
		}

		while (sourceValue.value < 1.0 && sourceValue.value > 0.0) {
			sourceValue.value *= 1000.0;
			--prefix;
		}

		sourceValue.prefix = static_cast<MetricPrefix>(prefix);
		return sourceValue;
	}

	/// @brief Returns @p sourceValue with @p targetPrefix.
	/// @param sourceValue Input value
	/// @param targetPrefix Target prefix
	constexpr inline
	PrefixedValue ToPrefix(PrefixedValue sourceValue, MetricPrefix targetPrefix) {
		auto source = ToUnderlying(sourceValue.prefix);
		auto target = ToUnderlying(targetPrefix);
		if (target < source) {
			while (source != target) {
				sourceValue.value *= 1000.0;
				--source;
			}
		}
		else {
			while (source != target) {
				sourceValue.value /= 1000.0;
				++source;
			}
		}

		sourceValue.prefix = targetPrefix;
		return sourceValue;
	}

	/// @brief Returns @p sourceValue with @p targetPrefix cast to T.
	/// @param sourceValue Input value
	/// @param targetPrefix Target prefix
	template<class T>
	constexpr inline
	T ToPrefixAs(PrefixedValue sourceValue, MetricPrefix targetPrefix) {
		return static_cast<T>(ToPrefix(sourceValue, targetPrefix).value);
	}

	constexpr inline
	PrefixedValue ClosestBytesPrefix(int64 bytes) {
		double prefixed = static_cast<double>(bytes);
		auto prefix = ToUnderlying(MetricPrefix::None);
		while (prefixed >= 1024.0) {
			prefixed /= 1024.0;
			++prefix;
		}

		return PrefixedValue{ static_cast<MetricPrefix>(prefix), prefixed };
	}

	constexpr inline
	PrefixedValue ToBytesPrefix(PrefixedValue sourceValue, MetricPrefix targetPrefix) {
		auto source = ToUnderlying(sourceValue.prefix);
		auto target = ToUnderlying(targetPrefix);
		if (target < source) {
			while (source != target) {
				sourceValue.value *= 1024.0;
				--source;
			}
		}
		else {
			while (source != target) {
				sourceValue.value /= 1024.0;
				++source;
			}
		}

		sourceValue.prefix = targetPrefix;
		return sourceValue;
	}

	template<class T>
	constexpr inline
	T ToBytesPrefixAs(PrefixedValue sourceValue, MetricPrefix targetPrefix) {
		return static_cast<T>(ToBytesPrefix(sourceValue, targetPrefix).value);
	}

	/// @brief Concept that returns true if TMain is any of the TOther types.
	template<class TMain, class... TOther>
	concept IsAnyOf = (std::same_as<TMain, TOther> || ...);

	/// @brief Concept that returns true if TOther types are all same as TMain.
	template<typename TMain, typename ... TOther>
	concept AllSameAsFirst = (std::same_as<TMain, TOther> && ...);

	/// @brief Class for retrieving the first type from a parameter pack.
	template<class T, class... Ts>
	struct GetFirstType
	{
		using Type = T;
	};

	/// @brief Alias for GetFirstType<Ts...>::Type.
	template<class... Ts>
	using GetFirstType_t = GetFirstType<Ts...>::Type;

	/// @brief Retrieves the first value from a parameter pack.
	template<class T, class... Ts>
	CUDA_FUNCTION inline constexpr
	decltype(auto) GetFirstValue(T&& value, Ts&&... values) {
		return std::forward<T>(value);
	}
}
