/**
 * File name: StrippedType.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "IsCVQualified.hpp"

namespace ProperRT
{
    /// @brief Class for stripping any qualifiers (const, pointer, reference...) from \p T.
    /// @tparam T Type to strip
    template<class T>
    struct StripType
    {
        /// @brief Stripped type.
        using Type = T;
    };

    /// @brief Specialization of StripType for removing pointers.
    template<class T>
    struct StripType<T*>
    {
        /// @brief Recursively strip type after removing pointer.
        using Type = typename StripType<T>::Type;
    };

    /// @brief Specialization of StripType for removing references.
    template<class T>
    struct StripType<T&>
    {
        using Type = typename StripType<T>::Type;
    };

    /// @brief Specialization of StripType for removing rvalue references.
    template<class T>
    struct StripType<T&&>
    {
        using Type = typename StripType<T>::Type;
    };

    /// @brief Specialization of StripType for removing arrays.
    template<class T>
        requires (!IsCVQualified_v<T>)
    struct StripType<T[]>
    {
        using Type = typename StripType<T>::Type;
    };

    /// @brief Specialization of StripType for removing static arrays.
    template<class T, std::size_t N>
        requires (!IsCVQualified_v<T>)
    struct StripType<T[N]>
    {
        using Type = typename StripType<T>::Type;
    };

    /// @brief Specialization of StripType for removing const qualifiers.
    template<class T>
    struct StripType<T const>
    {
        using Type = typename StripType<T>::Type;
    };

    /// @brief Specialization of StripType for removing volatile qualifiers.
    template<class T>
    struct StripType<T volatile>
    {
        using Type = typename StripType<T>::Type;
    };

    /// @brief Specialization of StripType for removing const volatile qualifiers.
    template<class T>
    struct StripType<T const volatile>
    {
        using Type = typename StripType<T>::Type;
    };

    /// @brief Shortcut to the StripType type.
    /// @tparam T Type to strip
    template<class T>
    using StripType_t = typename StripType<T>::Type;

    /// @brief Concept for checking if a type is stripped, i.e. has no qualifiers like const, pointers, arrays...
    /// @tparam T Type to check
    template<class T>
    concept StrippedType = std::is_same_v<T, StripType_t<T>>;

    /// @brief Concept for checking if a type is an array of a stripped type.
    /// @tparam T Type to check
    /// @see StrippedType
    template<class T>
    concept ArrayOfStrippedType = std::is_array_v<T> && StrippedType<std::remove_extent_t<T>>;

    /// @brief Concept for checking if a type is a pointer to a stripped type.
    /// @tparam T Type to check
    /// @see StrippedType
    template<class T>
    concept PointerToStrippedType = std::is_pointer_v<T> && StrippedType<std::remove_pointer_t<T>>;
}