/**
 * File name: Macros.hpp
 * Description: File containing compilation controlling macros.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */

#pragma once

// Adds an atomic add to every memory pool allocation and split, increasing time footprint
#ifndef PRT_COUNT_MEMPOOL_ALLOCATIONS
#define PRT_COUNT_MEMPOOL_ALLOCATIONS 0
#endif

// Saves statistics for each thread separately, significantly increasing size of statistics file
#ifndef PRT_SAVE_PER_THREAD_STATS
#define PRT_SAVE_PER_THREAD_STATS 0
#endif

// Adds timers for each thread, increasing the memory footprint
#ifndef PRT_TIME_KERNELS
#define PRT_TIME_KERNELS 0
#endif

// Specifies the number of bins used for binning when building kd trees
#ifndef PRT_KDTREE_BINS
#define PRT_KDTREE_BINS 32
#endif

// If true, KDTreeGPU builder bins locally before adding atomically
#ifndef PRT_BIN_LOCALLY
#define PRT_BIN_LOCALLY 0
#endif

// If true, split clipping is applied when building k-d trees
#ifndef PRT_SPLIT_CLIP
#define PRT_SPLIT_CLIP 1
#endif

// Display build status in GUI
#ifndef PRT_BUILD_STATUS
#define PRT_BUILD_STATUS 0
#endif
