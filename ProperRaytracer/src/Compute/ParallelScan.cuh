/**
 * File name: ParallelScan.cuh
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <array>

#include "TaskPool.cuh"
#include "Compute/ComputeUtilities.cuh"

namespace ProperRT
{
    /// @brief Class for handling GPU powered computation of a parallel scan using a task pool.
    /// @details Made for testing the task pool and mainly used for its public methods for scan phases, not optimal.
    /// @tparam T Type to scan
    /// @tparam VIdentity Identity value for which "value + VIdentity = value" holds
    /// @tparam FOperator Operator functor to scan with
    template<class T, T VIdentity, class FOperator>
    class ParallelScan
    {
    public:
        /// @brief Enum for scan task phases
        enum class ScanPhases : int8
        {
            UpSweep, DownSweep
        };

        /// @brief Class with custom scan task data
        struct ScanTaskData
        {
            int32 arrayStride;
            int32 stepN;
            int32 dataN;
        };

    public:
        /// @brief Does the processing for the up sweep phase
        /// @param data Data array
        /// @param dataN Data number
        /// @param workChunkFirst First work chunk idx
        /// @param workChunkLast Last work chunk idx
        /// @param arrayStride Current stride
        /// @param laneIdx Lane index
        __device__ static
        void ProcessUpSweep(T* data, int32 dataN, int workChunkFirst, int workChunkLast, int32 arrayStride, int32 laneIdx);

        /// @brief Does the processing for the down sweep phase
        /// @param data Data array
        /// @param dataN Data number
        /// @param workChunkFirst First work chunk idx
        /// @param workChunkLast Last work chunk idx
        /// @param arrayStride Current stride
        /// @param laneIdx Lane index
        __device__ static
        void ProcessDownSweep(T* data, int32 dataN, int workChunkFirst, int workChunkLast, int32 arrayStride, int32 laneIdx);

        /// @brief Calculates the global array index for a @p laneIdx and @p workChunkIdx based on @p arrayStride.
        __device__ static
        int32 GlobalArrayIndex(int32 arrayStride, int32 laneIdx, int32 workChunkIdx) {
            return arrayStride - 1 + workChunkIdx * arrayStride * Compute::SUBGROUP_SIZE + laneIdx * arrayStride;
        }

        /// @brief Calculates the total step count needed for a given array size
        __host__ __device__ static
        int32 StepCount(int32 arraySize) {
            return static_cast<int32>(IntLog2PowpRoundUp(static_cast<uint32>(arraySize), IntLog2(static_cast<uint32>(Compute::SUBGROUP_SIZE))));
        }

        /// @brief Calculates the number of work chunks needed to process an array with @p arraySize and current @p arrayStride.
        __host__ __device__ static
        int32 WorkChunkCount(int32 arraySize, int32 arrayStride) {
            return static_cast<int32>(IntDivRoundUp(static_cast<uint32>(arraySize), static_cast<uint32>(arrayStride * Compute::SUBGROUP_SIZE)));
        }
    public:
        /// @brief Input ctor.
        /// @param input Input to scan
        ParallelScan(std::span<T const> input) {
            dataGPU = DeviceUniquePointer<T[]>::AllocateAndUpload(input);
            taskManager.data = dataGPU.Get();
        }

        /// @brief Runs the scan asynchronously.
        void Run() {
            taskManager.RunKernel(state);
        }

        /// @brief Returns the result, has to wait for completion.
        /// @return Scanned result
        std::vector<T> GetResult() {
            return taskManager.data.Download();
        }

    public:
        /// @brief Task processor for parallel scan
        class ParallelScanProcessor : public GPUTaskPoolProcessor<ScanPhases, ScanTaskData, NoData, ParallelScanProcessor>
        {
        public:
            using Base = GPUTaskPoolProcessor<ScanPhases, ScanTaskData, NoData, ParallelScanProcessor>;
            using SharedParameters = Base::SharedParameters;
            using GroupTask = Base::GroupTask;

        public:
            DevicePointer<T[]> data;

            __host__
            void RunKernel(Base::PersistentState<>& allocator);

            __device__
            void ProcessTask();

            __device__
            void TaskPostProcess();

            __device__
            void Start() {}

            __device__
            void Finish() {}
        };

    private:
        ParallelScanProcessor taskManager{};
        DeviceUniquePointer<T[]> dataGPU{};
        ParallelScanProcessor::Base::PersistentState<> state{};
    };

    template<class T, T VIdentity, class FOperator>
    __host__
    void ParallelScan<T, VIdentity, FOperator>::ParallelScanProcessor::RunKernel(Base::PersistentState<>& state) {
        // Init the single task needed to process the array
        int32_t constexpr initStride = 1;
        int32_t dataN = static_cast<int32_t>(data.Bytes() / sizeof(T));
        std::array<GroupTask const, 1> const initialTasks{
            GroupTask{
                ScanTaskData{
                    initStride,
                    StepCount(dataN),
                    dataN
                },
                ScanPhases::UpSweep,
                0,
                static_cast<int32_t>(WorkChunkCount(dataN, initStride))
            }
        };

        std::array<int32_t const, 1> const taskHeader{ initialTasks[0].workChunksLeft.load() };

        // Get the number of streaming multiprocessors to determine the number of groups to launch
        int sm;
        int devID;
        Compute::CudaCheckError(cudaGetDevice(&devID));
        Compute::CudaCheckError(cudaDeviceGetAttribute(&sm, cudaDeviceAttr::cudaDevAttrMultiProcessorCount, devID));
        
        Base::RunKernel(state, initialTasks, taskHeader, Compute::TaskPoolProcessorParams{ 32, 1, 1, 8 });
    }

    template<class T, T VIdentity, class FOperator>
    __device__
    void ParallelScan<T, VIdentity, FOperator>::ParallelScanProcessor::ProcessTask() {
        // Process based on phase
        switch (Base::sharedParam->taskInfo.selectedTask->phase)
        {
        case ScanPhases::UpSweep: {
            ProcessUpSweep(
                data,
                Base::SelectedTask()->customData.dataN,
                Base::sharedParam->taskInfo.workChunkFirst,
                Base::sharedParam->taskInfo.workChunkFirst + Base::sharedParam->taskInfo.workChunkN,
                Base::SelectedTask()->customData.arrayStride,
                Compute::Grid1DHelpers::LaneIdx()
            );
            break;
        }
        case ScanPhases::DownSweep: {
            ProcessDownSweep(
                data,
                Base::SelectedTask()->customData.dataN,
                Base::sharedParam->taskInfo.workChunkFirst,
                Base::sharedParam->taskInfo.workChunkFirst + Base::sharedParam->taskInfo.workChunkN,
                Base::SelectedTask()->customData.arrayStride,
                Compute::Grid1DHelpers::LaneIdx()
            );
            break;
        }
        default:
            break;
        }
    }

    template<class T, T VIdentity, class FOperator>
    __device__
    void ParallelScan<T, VIdentity, FOperator>::ParallelScanProcessor::TaskPostProcess() {
        using namespace Compute;

        // Single thread
        if (Grid1DHelpers::LaneIdx() == 0) {
            // Load locally
            GroupTask task = *Base::SelectedTask();

            // Based on task phase
            switch (task.phase)
            {
            case ScanPhases::UpSweep: {
                // Increase step
                ++task.step;
                if (task.step < task.customData.stepN){
                    task.customData.arrayStride *= SUBGROUP_SIZE;
                }
                else {
                    // Move onto down sweep phase
                    task.phase = ScanPhases::DownSweep;
                    task.step -= 2;
                    task.customData.arrayStride /= SUBGROUP_SIZE;
                }
                break;
            }
            case ScanPhases::DownSweep: {
                // Decrease step
                --task.step;
                if (task.step >= 0) {
                    task.customData.arrayStride /= SUBGROUP_SIZE;
                }
                break;
            }
            default:
                break;
            }

            if (task.step >= 0) {
                // Write task to global memory
                *Base::SelectedTask() = task;
                // Compute and store new work chunk count for the next step
                int32_t newWorkChunkCount = static_cast<int32_t>(WorkChunkCount(task.customData.dataN, task.customData.arrayStride));
                Base::SelectedTask()->workChunksLeft.store(newWorkChunkCount);
                Base::sharedParam->taskInfo.selectedTaskPool->taskHeaders[Base::SelectedTaskIdx()].store(newWorkChunkCount);
            }
            else {
                Base::UnlockTask();
            }
        }
    }

    template<class T, T VIdentity, class FOperator>
    __device__
    void ParallelScan<T, VIdentity, FOperator>::ProcessUpSweep(T* data, int32 dataN, int workChunkFirst, int workChunkLast, int32 arrayStride, int32 laneIdx) {
        // For each work chunk
        FOperator op{};
        for (int workChunkIdx = workChunkFirst; workChunkIdx < workChunkLast; ++workChunkIdx) {
            int32_t gArrayIdx = GlobalArrayIndex(arrayStride, laneIdx, workChunkIdx);
            T value = Compute::SubgroupScanLocal((gArrayIdx < dataN ? data[gArrayIdx] : VIdentity), laneIdx, op);
            if (gArrayIdx < dataN) {
                data[gArrayIdx] = value;
            }
        }
    }

    template<class T, T VIdentity, class FOperator>
    __device__
    void ParallelScan<T, VIdentity, FOperator>::ProcessDownSweep(T* data, int32 dataN, int workChunkFirst, int workChunkLast, int32 arrayStride, int32 laneIdx) {
        // For each work chunk
        for (int workChunkIdx = workChunkFirst; workChunkIdx < workChunkLast; ++workChunkIdx) {
            // Propagate sum from upsweep to lower level
            int32 gArrayIdx = GlobalArrayIndex(arrayStride, laneIdx, workChunkIdx + 1);
            if (gArrayIdx < dataN && laneIdx < Compute::SUBGROUP_SIZE - 1) {
                data[gArrayIdx] = data[gArrayIdx] + data[(workChunkIdx + 1) * arrayStride * Compute::SUBGROUP_SIZE - 1];
            }
        }
    }
}
