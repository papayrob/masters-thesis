/**
 * File name: OpenCLInclude.hpp
 * Description: Customized OpenCL include file. Contains the required macros in addition to the header file.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2023 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#define CL_HPP_MINIMUM_OPENCL_VERSION 120
#define CL_HPP_TARGET_OPENCL_VERSION 120
#define CL_HPP_ENABLE_EXCEPTIONS
#pragma warning(push, 0)
//#include <CL/opencl.hpp>
#pragma warning(pop)
