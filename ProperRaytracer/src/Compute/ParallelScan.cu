/**
 * File name: ParallelScan.cu
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "ParallelScan.hpp"
#include "ParallelScan.cuh"

#include <cuda/std/functional>

#include "DeviceAllocator.cuh"

namespace ProperRT
{
    std::vector<int> PrefixScan(std::span<int const> input)
    {
        ParallelScan<int, 0, cuda::std::plus<int>> scan{ input };
        scan.Run();
#ifdef PRT_DEBUG
        Compute::CudaCheckError(cudaDeviceSynchronize());
#endif
        return scan.GetResult();
    }
}
