/**
 * File name: InteropTexture.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include "Graphics/OpenGL/Texture.hpp"

namespace ProperRT
{
    /// @brief Class for managing a texture that can be used by both OpenCL and CUDA.
    class InteropTexture
    {
    public:
        /// @brief Default ctor.
        InteropTexture();

        /// @brief Constructs the texture with given @p dimensions.
        InteropTexture(Vector2i const& dimensions);

        /// @brief Dtor.
        ~InteropTexture();

        /// @brief Resizes the texture to @p newSize.
        /// @param newSize New size
        void Resize(Vector2i const& newSize);

        /// @brief Maps the texture, CUDA can only write to mapped textures.
        void Map();

        /// @brief Umaps the texture, OpenGL can only use unmapped textures.
        void Unmap();

        /// @brief Returns true if the texture is mapped, false otherwise.
        bool IsMapped() const { return mapped; }

        /// @brief Returns the cuda surface object (texture representation in CUDA).
        cudaSurfaceObject_t GetSurfaceObject() const { return cudaSurface; }

        /// @brief Downloads the texture from the GPU to an ImageBuffer and returns it.
        ImageBuffer<Color32f> Download() const;

        /// @brief Downloads the texture from the GPU to @p buffer.
        void Download(ImageBuffer<Color32f>& buffer) const;

        /// @brief Returns the OpenGL texture object.
        OpenGL::Texture const& GetTexture() const { return texture; }

    private:
        Vector2i dimensions{0,0};
        OpenGL::Texture texture{};
        cudaGraphicsResource_t cudaResource{};
        cudaArray_t cudaArray{};
        cudaSurfaceObject_t cudaSurface{};
        bool mapped{ false };
    };
}
