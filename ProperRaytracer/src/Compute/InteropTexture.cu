/**
 * File name: InteropTexture.cu
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "InteropTexture.hpp"

#include <GLFW/glfw3.h>
#include <cuda_gl_interop.h>
#include "RTApp.hpp"
#include "Compute/CudaHelpers.cuh"

namespace ProperRT
{
    InteropTexture::InteropTexture() {
        
    }

    InteropTexture::InteropTexture(Vector2i const& dimensions_)
        : dimensions{ dimensions_ }
    {
        Resize(dimensions);
    }

    InteropTexture::~InteropTexture() {
        Unmap();
        if (cudaSurface != cudaSurfaceObject_t{}) {
            Compute::CudaCheckError(cudaDestroySurfaceObject(cudaSurface));
            cudaSurface = cudaSurfaceObject_t{};
        }
        if (cudaResource != cudaGraphicsResource_t{}) {
            Compute::CudaCheckError(cudaGraphicsUnregisterResource(cudaResource));
            cudaResource = cudaGraphicsResource_t{};
        }
        if (!RTApp::ShowGUI() && cudaArray != cudaArray_t{}) {
            cudaFreeArray(cudaArray);
            cudaArray = cudaArray_t{};
        }
    }

    void InteropTexture::Resize(Vector2i const& newSize) {
        if (newSize == dimensions) {
            return;
        }
        dimensions = newSize;

        Unmap();

        // Destroy old resource
        if (cudaResource != cudaGraphicsResource_t{}) {
            Compute::CudaCheckError(cudaGraphicsUnregisterResource(cudaResource));
        }

        // Recreate texture and resource
        if (RTApp::ShowGUI()) {
            texture = OpenGL::Texture{ newSize, gl::GLenum::GL_RGBA32F };
            Compute::CudaCheckError(
                cudaGraphicsGLRegisterImage(&cudaResource, texture.Handle(), GL_TEXTURE_2D, cudaGraphicsRegisterFlagsWriteDiscard)
            );
        }
        else {
            if (cudaArray != cudaArray_t{}) {
                cudaFreeArray(cudaArray);
            }

            cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc(32, 32, 32, 32, cudaChannelFormatKindFloat);
            Compute::CudaCheckError(cudaMallocArray(&cudaArray, &channelDesc, static_cast<size_t>(newSize.x()), static_cast<size_t>(newSize.y()), cudaArraySurfaceLoadStore));

            // Create and link cudaSurface
            cudaResourceDesc surfaceDesc{ .resType{ cudaResourceTypeArray }, .res{ .array{ cudaArray } }};
            Compute::CudaCheckError(cudaCreateSurfaceObject(&cudaSurface, &surfaceDesc));
        }
    }
    
    void InteropTexture::Map() {
        if (!mapped && RTApp::ShowGUI()) {
            // Map resource
            Compute::CudaCheckError(cudaGraphicsMapResources(1, &cudaResource));

            // Get cudaArray
            Compute::CudaCheckError(cudaGraphicsSubResourceGetMappedArray(&cudaArray, cudaResource, 0, 0));

            // Create and link cudaSurface
            cudaResourceDesc surfaceDesc{ .resType{ cudaResourceTypeArray }, .res{ .array{ cudaArray } }};
            Compute::CudaCheckError(cudaCreateSurfaceObject(&cudaSurface, &surfaceDesc));

        }
        mapped = true;
    }
    
    void InteropTexture::Unmap() {
        if (mapped && RTApp::ShowGUI()) {
            Compute::CudaCheckError(cudaDestroySurfaceObject(cudaSurface));
            Compute::CudaCheckError(cudaGraphicsUnmapResources(1, &cudaResource));
        }
        mapped = false;
    }
    
    ImageBuffer<Color32f> InteropTexture::Download() const {
        if (RTApp::ShowGUI()) {
            return texture.Download<float>();
        }
        else {
            ImageBuffer<Color32f> buffer{ dimensions };
            Download(buffer);
            return buffer;
        }
    }
    
    void InteropTexture::Download(ImageBuffer<Color32f>& buffer) const {
        if (RTApp::ShowGUI()) {
            texture.Download(buffer);
        }
        else {
            cudaMemcpy2DFromArray(buffer.buffer.data(), sizeof(Color32f) * dimensions.x(), cudaArray, 0, 0, sizeof(Color32f) * dimensions.x(), dimensions.y(), cudaMemcpyDeviceToHost);
        }
    }
}
