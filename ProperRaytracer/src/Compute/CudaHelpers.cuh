/**
 * File name: CudaHelpers.hpp
 * Description: File containing helper functions for CUDA device code.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <cuda_runtime.h>
#include <device_launch_parameters.h>

#include "Utilities/Utilities.hpp"
#include "Utilities/Logger.hpp"
#include "Geometry/Vector.hpp"

namespace ProperRT::Compute
{
    namespace Grid1DHelpers
    {
        /// @brief Returns the local lane index (thread index local to the subgroup, 0 to SUBGROUP_SIZE).
        template<class T = int>
        __device__ __forceinline__
        auto LaneIdx() {
            return static_cast<T>(threadIdx.x) % static_cast<T>(Compute::SUBGROUP_SIZE);
        }

        /// @brief Returns the number of launched subgroups per group.
        __device__ __forceinline__
        auto SubgroupsPerGroup() {
            return static_cast<int32>(blockDim.x) / Compute::SUBGROUP_SIZE;
        }

        /// @brief Returns the number of launched subgroups across all groups.
        __device__ __forceinline__
        auto TotalSubgroupCount() {
            return static_cast<int32>(gridDim.x) * SubgroupsPerGroup();
        }

        /// @brief Returns the local index of the subgroup local to the group.
        __device__ __forceinline__
        auto LocalSubgroupIdx() {
            return static_cast<int32>(threadIdx.x) / Compute::SUBGROUP_SIZE;
        }

        /// @brief Returns the global index of the subgroup across the whole grid.
        __device__ __forceinline__
        auto GlobalSubgroupIdx() {
            return static_cast<int32>(blockIdx.x) * (static_cast<int32>(blockDim.x) / Compute::SUBGROUP_SIZE) + LocalSubgroupIdx();
        }

        /// @brief Returns the local index of the thread inside its group.
        __device__ __forceinline__
        auto LocalThreadIdx() {
            return static_cast<int32>(threadIdx.x);
        }

        /// @brief Returns the global index of the thread across the whole grid.
        __device__ __forceinline__
        auto GlobalThreadIdx() {
            return static_cast<int32>(blockIdx.x) * static_cast<int32>(blockDim.x) + static_cast<int32>(threadIdx.x);
        }
    }

    namespace Grid2DHelpers
    {
        /// @brief Returns the local 2D index of the thread inside its group.
        __device__ __forceinline__
        auto LocalThreadIdx2D() {
            return Vector2i{
                Grid1DHelpers::LocalThreadIdx(),
                static_cast<int32>(threadIdx.y),
            };
        }

        /// @brief Returns the local 1D index of the thread inside its group.
        __device__ __forceinline__
        auto LocalThreadIdx1D() {
            return static_cast<int32>(threadIdx.y) * static_cast<int32>(blockDim.x) + Grid1DHelpers::LocalThreadIdx();
        }

        /// @brief Returns the global 2D index of the thread across the whole grid.
        __device__ __forceinline__
        auto GlobalThreadIdx2D() {
            return Vector2i{
                Grid1DHelpers::GlobalThreadIdx(),
                static_cast<int32>(blockIdx.y) * static_cast<int32>(blockDim.y) + static_cast<int32>(threadIdx.y),
            };
        }

        /// @brief Returns the global 1D index of the thread across the whole grid.
        __device__ __forceinline__
        auto GlobalThreadIdx1D() {
            return (static_cast<int32>(blockIdx.y) * static_cast<int32>(blockDim.y) + static_cast<int32>(threadIdx.y)) * static_cast<int32>(gridDim.x) * static_cast<int32>(blockDim.x)
            + Grid1DHelpers::GlobalThreadIdx();
        }

        /// @brief Returns the total number of threads in the grid.
        __device__ __forceinline__
        auto ThreadsInGrid() {
            return Vector2i{
                static_cast<int32>(gridDim.x) * static_cast<int32>(blockDim.x),
                static_cast<int32>(gridDim.y) * static_cast<int32>(blockDim.y)
            };
        }

    }

    /// @brief Checks the return of a cuda function, and logs and exits on any errors.
    inline
    void CudaCheckError(cudaError_t result, std::source_location const& location = std::source_location::current()) {
        if (result != cudaError_t::cudaSuccess) {
            Logger::LogError(std::string{ cudaGetErrorName(result) } + " (" + std::to_string(ToUnderlying(result)) + ")", location);
            exit(EXIT_FAILURE);
        }
    }
}
