/**
 * File name: TaskPool.cuh
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <limits>
#include <cuda/atomic>

#include "Macros.hpp"
#include "Utilities/Utilities.hpp"
#include "Utilities/DeviceTimer.cuh"
#include "Utilities/TimerGPU.hpp"
#include "Compute/MemoryPool.cuh"

namespace ProperRT
{
    namespace Kernels
    {
        /// @brief Kernel that handles the processing of tasks from the task pool.
        /// @tparam TGPUTaskPoolProcessor Class derived from GPUTaskPoolProcessor
        /// @param taskManager Instance of a class derived from GPUTaskPoolProcessor
        template<class TGPUTaskPoolProcessor>
        __global__
        #if PRT_TIME_KERNELS
        void ProcessTasks(TGPUTaskPoolProcessor taskManager, DeviceTimer retrievalTimer, DeviceTimer processTimer, DeviceTimer postProcessTimer)
        #else
        void ProcessTasks(TGPUTaskPoolProcessor taskManager)
        #endif
        {
            extern __shared__ typename TGPUTaskPoolProcessor::SharedParameters sharedParam[];

            taskManager.sharedParam = &sharedParam[Compute::Grid1DHelpers::LocalSubgroupIdx()];
            if (Compute::Grid1DHelpers::LaneIdx() == 0) {
                taskManager.sharedParam->skipRetrieval = false;
            }
            __syncwarp();

            taskManager.StartBase();

            #if PRT_TIME_KERNELS
            auto tid = Compute::Grid1DHelpers::GlobalThreadIdx();
            processTimer.Reset(tid);
            postProcessTimer.Reset(tid);
            retrievalTimer.Reset(tid);
            retrievalTimer.Start(tid);
            #endif

            // Retrieve tasks while there are tasks left
            while (taskManager.RetrieveTask()) {
                #if PRT_TIME_KERNELS
                __threadfence_block();
                retrievalTimer.Stop(tid);
                #endif
                // if (Compute::Grid1DHelpers::LaneIdx() == 0) {
                //     printf("Subgroup %d retrieved task\n", Compute::Grid1DHelpers::GlobalSubgroupIdx());
                // }
                __syncwarp();

                // Process the retrieved work chunks of the retrieved task
                #if PRT_TIME_KERNELS
                processTimer.Start(tid);
                __threadfence_block();
                #endif

                taskManager.ProcessTask();
                __syncwarp();

                #if PRT_TIME_KERNELS
                __threadfence_block();
                processTimer.Stop(tid);
                #endif

                cuda::atomic_thread_fence(cuda::memory_order_release, cuda::thread_scope_device);

                // Check if current subgroup isn't last subgroup
                auto chunks = taskManager.sharedParam->taskInfo.workChunkN;
                if (Compute::Grid1DHelpers::LaneIdx() == 0) {
                    if (taskManager.sharedParam->skipRetrieval && taskManager.sharedParam->taskInfo.workChunkFirst == 0) {
                        chunks = 0;
                    }
                    else {
                        chunks = taskManager.sharedParam->taskInfo.selectedTask->workChunksLeft.fetch_sub(chunks, cuda::memory_order_relaxed) - chunks;
                    }
                }

                if (Compute::Grid1DHelpers::LaneIdx() == 0) {
                    taskManager.sharedParam->skipRetrieval = false;
                }
                __syncwarp();

                // if last, do post processing (i.e. allocating, freeing, creating new tasks)
                if (__shfl_sync(Compute::FULL_MASK, (chunks == 0), 0)) {
                    #if PRT_TIME_KERNELS
                    postProcessTimer.Start(tid);
                    __threadfence_block();
                    #endif

                    cuda::atomic_thread_fence(cuda::memory_order_acquire, cuda::thread_scope_device);
                    taskManager.TaskPostProcess();
                    __syncwarp();

                    #if PRT_TIME_KERNELS
                    __threadfence_block();
                    postProcessTimer.Stop(tid);
                    #endif
                }
                #if PRT_TIME_KERNELS
                retrievalTimer.Start(tid);
                __threadfence_block();
                #endif
            }

            #if PRT_TIME_KERNELS
            __threadfence_block();
            retrievalTimer.Stop(tid);
            #endif

            taskManager.FinishBase();
        }

        /// @brief Computes and saves the number of allocated task pools into @p allocatedPoolN.
        template<class TTaskPool>
        __global__
        void GetTaskPoolCount(TTaskPool* taskPool, DevicePointer<int> allocatedPoolN) {
            int allocated = 1;
            taskPool = taskPool->next;
            while (taskPool != nullptr) {
                ++allocated;
                taskPool = taskPool->next;
            }
            *allocatedPoolN = allocated;
        }

        /// @brief Deletes allocated task pools, also computes and saves the number of allocated task pools into @p allocatedPoolN.
        /// @details Assumes the first task pool was allocated from the host, the rest from the device.
        template<class TTaskPool>
        __global__
        void DeleteTaskPool(TTaskPool* taskPool, DevicePointer<int> allocatedPoolN) {
            int allocated = 1;
            taskPool = taskPool->next;
            while (taskPool != nullptr) {
                ++allocated;
                delete[] taskPool->tasks;
                delete[] taskPool->taskHeaders;
                TTaskPool* next = taskPool->next;
                delete taskPool;
                taskPool = next;
            }
            *allocatedPoolN = allocated;
        }
    }

    namespace Compute
    {

        /// @brief Concept that checks if the input type has the required functions for being a task pool processor.
        template<class TTaskProcessor>
        concept TaskProcessorType = requires (TTaskProcessor processor)
        {
            { processor.ProcessTask() };
            { processor.TaskPostProcess() };
            { processor.Start() };
            { processor.Finish() };
        };

        /// @brief Class for grouping task pool processor parameters.
        struct TaskPoolProcessorParams
        {
            /// @brief Number of work chunks each thread will retrieve.
            int32 chunksToRetrieve;
            /// @brief Multiplier with which the task pool size gets multiplied (taskPoolSize = groupN * subgroupsPerGroupN * SUBGROUP_SIZE * taskPoolMult).
            int32 taskPoolMult;
            /// @brief Number of groups to be launched on the GPU.
            int32 groupN;
            /// @brief Number of subgroups per group to be launched.
            int32 subgroupsPerGroupN;
        };
    }

    /// @brief Class for solving problems by processing tasks in a task pool on the GPU.
    /// @details To create a solver, inherit after this class and fill the template parameters
    /// with the GroupTask type with filled template parameters, class for storing data in shared parameters
    /// and the derived type itself (look up CRTP for more info). Custom global parameters can be added as
    /// member variables of the class, but all data must be copyable to the GPU (the whole instance gets memcopied).
    /// Then implement 2 device methods, ProcessTask that gets called after a task has been retrieved
    /// (with retrieved task info saved in sharedParam), and TaskPostProcess, that gets called by the last subgroup
    /// working on the task, and a host method for initializing and filling custom data and call RunKernel method from it.
    /// Start and Finish device methods must also be implemented (may be empty) and get called once per processing at the start and the end.
    /// @tparam TTaskPhase Enum type for the phases of the task
    /// @tparam TCustomTaskData Additional user provided task data (for no data, use NoData struct)
    /// @tparam TCustomSharedData Custom data for shared parameters (for no data, use NoData struct)
    /// @tparam TTaskProcessor Inherited type
    template<class TTaskPhase, class TCustomTaskData, class TCustomSharedData, class TTaskProcessor>
        requires (std::is_enum_v<TTaskPhase>)
    class GPUTaskPoolProcessor
    {
    public:
        using AtomicInt = cuda::atomic<int32, cuda::thread_scope_device>;
        
        template<class T>
        using AtomicPtr = cuda::atomic<T*, cuda::thread_scope_device>;

        using AtomicBool = cuda::atomic<bool, cuda::thread_scope_device>;

        /// @brief Class for representing tasks partially processable by subgroups.
        /// @details The task consists of a phase enum variable signifying the task purpose,
        /// a step variable used for global synchronization when the phase needs multiple steps,
        /// the storage for 
        struct GroupTask
        {
            /// @brief User custom data.
            TCustomTaskData customData;

            /// @brief Current phase of work.
            /// @details The task pool consists of only a single task type, so this
            /// determines what the task processor should be doing with its work chunks.
            TTaskPhase phase;

            /// @brief Current step of a phase.
            /// @details Used when the phase needs multiple steps. Also acts as
            /// global synchronization.
            int32 step;

            /// @brief The number of work chunks left for the current step of the current phase.
            /// @details Used to determine which subgroup should do post processing (when fetch_sub returns zero).
            AtomicInt workChunksLeft;


            /// @brief Default ctor.
            GroupTask() = default;

            /// @brief All members ctor.
            GroupTask(TCustomTaskData const& customData_, TTaskPhase phase_, int32 step_, int32 workChunksLeft_)
                requires std::copyable<TCustomTaskData>
                : customData{ customData_ }, phase{ phase_ }, step{ step_ }, workChunksLeft{ workChunksLeft_ }
            {}
            
            /// @brief Copy ctor.
            __host__ __device__
            GroupTask(GroupTask const& other)
                requires std::copyable<TCustomTaskData>
                : customData{ other.customData }, phase{ other.phase }, step{ other.step }, workChunksLeft{ other.workChunksLeft.load(cuda::memory_order_seq_cst) }
            {}

            /// @brief Copy operator.
            __host__ __device__
            GroupTask& operator=(GroupTask const& other)
                requires std::copyable<TCustomTaskData>
            {
                customData = other.customData;
                phase = other.phase;
                step = other.step;
                workChunksLeft.store(other.workChunksLeft.load(cuda::memory_order_seq_cst));
                return *this;
            }
        };

        /// @brief Class representing a task pool which can be chained into a linked list.
        struct TaskPool
        {
            /// @brief Task array.
            GroupTask* tasks;
            /// @brief Task header array, signifying the state of the corresponding task.
            AtomicInt* taskHeaders;
            /// @brief Number of active tasks in this task pool.
            AtomicInt taskN;
            /// @brief Link to the next pool, allocated whenever the previous ones fill up.
            AtomicPtr<TaskPool> next;
            AtomicBool modifyingNext;
        };

        /// @brief Class for storing info about a retrieved task.
        struct SelectedTaskInfo
        {
            /// @brief Retrieved task.
            GroupTask* selectedTask;
            /// @brief Task pool the selected task is from.
            TaskPool* selectedTaskPool;
            /// @brief The first work chunk index that the subgroup retrieved.
            int32 workChunkFirst;
            /// @brief Number of work chunks the subgroup retrieved.
            int32 workChunkN;
        };

        /// @brief Class for storing info shared by a subgroup.
        struct SharedParameters
        {
            SelectedTaskInfo taskInfo;
            bool skipRetrieval;
            TCustomSharedData customData;
        };

        /// @brief Class for deleting the task pool, assuming all task pool besides the first one were allocated on the device.
        struct TaskPoolDeleter
        {
            std::shared_ptr<int> allocatedPoolN{ std::make_shared<int>(0) };

            constexpr
            TaskPoolDeleter() noexcept = default;

            void operator()(TaskPool* pointer) const {
                if (pointer != nullptr) {
                    auto allocated = DeviceUniquePointer<int>::Allocate();
                    Kernels::DeleteTaskPool<<<1,1>>>(pointer, allocated.Get());
                    #ifdef PRT_DEBUG
                    Compute::CudaCheckError(cudaDeviceSynchronize());
                    #endif
                    Compute::CudaCheckError(cudaFree(pointer));
                    *allocatedPoolN = allocated.Download();
                }
            }
        };

        /// @brief Class for storing the persistent state of the task pool, must be stored outside the processor class.
        /// @tparam TTaskPoolDeleter Task pool deleter type used to delete the task pool(s)
        template<class TTaskPoolDeleter = TaskPoolDeleter>
        struct PersistentState
        {
            /// @brief Task pool deleter instance.
            TTaskPoolDeleter taskPoolDeleter{};
            /// @brief Task pool storage.
            DeviceUniquePointer<TaskPool, TTaskPoolDeleter> taskPool{};
            /// @brief Tasks storage.
            DeviceUniquePointer<GroupTask[]> tasks{};
            /// @brief Task headers storage.
            DeviceUniquePointer<AtomicInt[]> taskHeaders{};
            /// @brief Working subgroups count storage.
            DeviceUniquePointer<AtomicInt> workingSubgroups{};
            /// @brief Total task retrieval count storage.
            DeviceUniquePointer<AtomicInt> totalRetrievals{};
            /// @brief Total skipped task retrieval count storage.
            DeviceUniquePointer<AtomicInt> totalSkippedRetrievals{};

            /// @brief Task pool processor timer.
            TimerGPU runTimer{};

            #if PRT_TIME_KERNELS
            DeviceUniqueTimer retrievalTimer{};
            DeviceUniqueTimer processTimer{};
            DeviceUniqueTimer postProcessTimer{};
            #endif

            /// @brief Computes the total memory consumed by the task pool processor.
            __host__
            int64 AllocatedMemory(int32 taskPoolSize) const {
                if (taskPool != nullptr) {
                    auto allocated = DeviceUniquePointer<int>::Allocate();
                    Kernels::GetTaskPoolCount<<<1,1>>>(taskPool.Get().data, allocated.Get());
                    #ifdef PRT_DEBUG
                    Compute::CudaCheckError(cudaDeviceSynchronize());
                    #endif
                    *taskPoolDeleter.allocatedPoolN = allocated.Download();
                }
                else {
                    *taskPoolDeleter.allocatedPoolN = 0;
                }
                
                return (*taskPoolDeleter.allocatedPoolN) * taskPoolSize * sizeof(GroupTask) + tasks.Bytes() + taskHeaders.Bytes();
            }
        };

    public:
        /// @brief Pointer to the subgroup's shared memory class.
        SharedParameters* sharedParam;
        /// @brief Task retrieval counter.
        int retrievalN;
        /// @brief Task retrieval skip counter.
        int skippedRetrievalN;

    public:
        /// @brief Retrieves task from the task pool. Must be entered with the whole subgroup.
        __device__
        bool RetrieveTask() {
            using namespace Compute;
            using namespace Grid1DHelpers;

            //printf("%d skip %d\n", LaneIdx(), (int)sharedParam->skipRetrieval);
            if (sharedParam->skipRetrieval) {
                return true;
            }

            int32 laneIdx = LaneIdx();
            if (laneIdx == 0) {
                sharedParam->taskInfo = SelectedTaskInfo{ nullptr, nullptr, 0, 0 };
            }

            bool failedOnce = false;

            // Iterate until an active task is found
            while (true) {
                // Check if there are any working subgroups left
                if (__ballot_sync(FULL_MASK, (workingSubgroups->load(cuda::memory_order_relaxed) == 0)) == FULL_MASK) {
                    return false;
                }

                // Go over all task pools (chained in a linked list) and check for active tasks in each
                TaskPool* currentTaskPool = taskPool;
                while (true) {
                    // start at an offset based on total subgroup number and current subgroup index
                    int32 startIdx = (currentTaskPool->taskN.load(cuda::memory_order_relaxed) / TotalSubgroupCount()) * GlobalSubgroupIdx();
                    // align to subgroup size
                    startIdx = startIdx - (startIdx % SUBGROUP_SIZE);
                    // also offset starting lane
                    int32 startLane = GlobalSubgroupIdx() % SUBGROUP_SIZE;

                    int32 poolIdx = startIdx;
                    // Task pool range
                    do {
                        if (int32 header = currentTaskPool->taskHeaders[poolIdx + laneIdx].load(cuda::memory_order_relaxed); __any_sync(FULL_MASK, header > 0)) {
                            // Subgroup range
                            for (int32 i = 0; i < SUBGROUP_SIZE; ++i) {
                                auto lane = (i + startLane) % SUBGROUP_SIZE;
                                // Single thread
                                if (lane == laneIdx && header > 0) {
                                    // Retrieve chunks atomically and check if chunks are still available
                                    if (header = currentTaskPool->taskHeaders[poolIdx + lane].fetch_sub(chunksToRetrieve, cuda::memory_order_acquire); header > 0) {
                                        header -= chunksToRetrieve;
                                        if (header < 0) {
                                            // Align retrieved chunks to zero
                                            chunksToRetrieve += header;
                                            header = 0;
                                        }
                                        sharedParam->taskInfo = SelectedTaskInfo{ &(currentTaskPool->tasks[poolIdx + lane]), currentTaskPool, header, chunksToRetrieve };
                                    }
                                }

                                // Active task found, exit
                                __syncwarp();
                                if (sharedParam->taskInfo.selectedTask != nullptr) {
                                    ++retrievalN;
                                    if (failedOnce && laneIdx == 0) {
                                        workingSubgroups->fetch_add(1, cuda::memory_order_relaxed);
                                    }
                                    cuda::atomic_thread_fence(cuda::memory_order_acquire, cuda::thread_scope_device);
                                    return true;
                                }
                            }
                        }
                        
                        poolIdx += SUBGROUP_SIZE;
                        if (poolIdx == taskPoolSize) {
                            poolIdx = 0;
                        }
                    } while (poolIdx != startIdx);

                    // No task found in current task pool, try next
                    if (__ballot_sync(FULL_MASK, (currentTaskPool->next.load(cuda::memory_order_seq_cst) != nullptr)) == FULL_MASK) {
                        currentTaskPool = currentTaskPool->next.load(cuda::memory_order_seq_cst);
                    }
                    else {
                        break;
                    }
                }

                if (!failedOnce) {
                    failedOnce = true;
                    if (laneIdx == 0) {
                        workingSubgroups->fetch_sub(1, cuda::memory_order_relaxed);
                    }
                }
            }
        }

        /// @brief Unlocks the currently selected task and subtracts from the number of tasks in the current task pool.
        /// Must be entered with a single thread.
        __device__
        void UnlockTask() {
            sharedParam->taskInfo.selectedTaskPool->taskHeaders[SelectedTaskIdx()].store(TASK_EMPTY, cuda::memory_order_release);
            sharedParam->taskInfo.selectedTaskPool->taskN.fetch_sub(1, cuda::memory_order_relaxed);
        }

        /// @brief Locks an empty task slot and returns the owning task pool and task index.
        /// Subtracts from the number of task pool tasks, must be entered with the whole subgroup.
        __device__
        cuda::std::pair<TaskPool*, int32> LockTask(DevicePointer<MemoryPool> memoryPool = {nullptr}) {
            using namespace Compute;
            using namespace Grid1DHelpers;

            int32 laneIdx = LaneIdx();
            
            // Go over all task pools (chained in a linked list) and check for empty tasks in each
            TaskPool* currentTaskPool = taskPool;
            while (true) {
                // start at an offset based on total subgroup number and current subgroup index
                int32 startIdx = (currentTaskPool->taskN.load(cuda::memory_order_relaxed) / TotalSubgroupCount()) * GlobalSubgroupIdx();
                // align to subgroup size
                startIdx = startIdx - startIdx % SUBGROUP_SIZE;
                // also offset starting lane
                int32 startLane = GlobalSubgroupIdx() % SUBGROUP_SIZE;

                int32 poolIdx = startIdx;
                // Task pool range
                do {
                    if (int32 header = currentTaskPool->taskHeaders[poolIdx + laneIdx].load(cuda::memory_order_relaxed); __any_sync(FULL_MASK, header == TASK_EMPTY)) {
                        // Subgroup range
                        for (int32 i = 0; i < SUBGROUP_SIZE; ++i) {
                            auto lane = (i + startLane) % SUBGROUP_SIZE;
                            bool locked = false;
                            if (lane == laneIdx && header == TASK_EMPTY) {
                                // Try locking
                                locked = currentTaskPool->taskHeaders[poolIdx + laneIdx].compare_exchange_strong(header, TASK_LOCKED, cuda::memory_order_acquire, cuda::memory_order_relaxed);
                            }

                            // Lock successful, so return storage
                            if (__shfl_sync(FULL_MASK, locked, lane)) {
                                if (laneIdx == 0) {
                                    currentTaskPool->taskN.fetch_add(1, cuda::memory_order_relaxed);
                                }
                                return { currentTaskPool, __shfl_sync(FULL_MASK, poolIdx + laneIdx, lane) };
                            }
                        }
                    }
                    
                    poolIdx += SUBGROUP_SIZE;
                    if (poolIdx == taskPoolSize) {
                        poolIdx = 0;
                    }
                } while (poolIdx != startIdx);

                TaskPool* next;
                bool initNext = false;
                if (laneIdx == 0) {
                    // No empty slot found in current task pool, try next
                    if (currentTaskPool->next.load(cuda::memory_order_seq_cst) != nullptr) {
                        currentTaskPool = currentTaskPool->next.load(cuda::memory_order_seq_cst);
                    }
                    else if (!currentTaskPool->modifyingNext.load(cuda::memory_order_seq_cst)) {
                        // No next task pool, so try allocating a new one
                        bool allocatingNext{ false };
                        // Allocate next
                        if (currentTaskPool->modifyingNext.compare_exchange_strong(allocatingNext, true)) {
                            if (memoryPool == nullptr) {
                                next = new TaskPool{
                                    new GroupTask[taskPoolSize],
                                    new AtomicInt[taskPoolSize],
                                    0,
                                    nullptr,
                                    false
                                };
                            }
                            else {
                                auto allocated = memoryPool->Allocate<TaskPool, GroupTask, AtomicInt>(1, 1, taskPoolSize, taskPoolSize).data;
                                next = cuda::std::get<0>(allocated);
                                next->tasks = cuda::std::get<1>(allocated);
                                next->taskHeaders = cuda::std::get<2>(allocated);
                                next->taskN = 0;
                                next->next = nullptr;
                                next->modifyingNext = false;
                            }
                            
                            initNext = true;
                        }
                        // Reset search from beginning
                        else {
                            currentTaskPool = taskPool;
                        }
                    }
                }

                // Init the new task pool if one was allocated
                initNext = __shfl_sync(FULL_MASK, initNext, 0);
                if (initNext) {
                    next = (TaskPool*)__shfl_sync(FULL_MASK, (cuda::std::intptr_t)next, 0);
                    for (int i = 0; i < taskPoolSize; i += SUBGROUP_SIZE) {
                        next->taskHeaders[i + laneIdx] = TASK_EMPTY;
                    }

                    if (laneIdx == 0) {
                        //cuda::atomic_thread_fence(cuda::memory_order_seq_cst, cuda::thread_scope_device);
                        currentTaskPool->next.store(next);
                        currentTaskPool->modifyingNext.store(false);
                        currentTaskPool = next;
                    }
                }

                // Send the new current task pool to all lanes
                currentTaskPool = (TaskPool*)__shfl_sync(FULL_MASK, (cuda::std::intptr_t)currentTaskPool, 0);
            }
        }

        /// @brief Updates the current task with a new work chunk count. Must be entered only by one thread.
        /// @param newWorkChunkCount Value to update the work chunk count with.
        __device__
        void UpdateTask(int32 newWorkChunkCount) {
            assert(newWorkChunkCount > 0);

            sharedParam->skipRetrieval = true;
            ++skippedRetrievalN;

            // Skip updating the global memory too
            if (newWorkChunkCount < 2 * chunksToRetrieve) {
                sharedParam->taskInfo.workChunkFirst = 0;
                sharedParam->taskInfo.workChunkN = newWorkChunkCount;
            }
            // Skip retrieval, but update in global memory
            else {
                sharedParam->taskInfo.workChunkFirst = newWorkChunkCount - chunksToRetrieve;
                sharedParam->taskInfo.workChunkN = chunksToRetrieve;

                SelectedTask()->workChunksLeft.store(newWorkChunkCount, cuda::memory_order_relaxed);
                sharedParam->taskInfo.selectedTaskPool->taskHeaders[SelectedTaskIdx()].store(newWorkChunkCount - chunksToRetrieve, cuda::memory_order_release);
            }
        }

        /// @brief Calls implementation's Start method.
        __device__
        void StartBase() {
            Implementation().Start();
        }

        /// @brief Calls implementation's Finish method and adds retrieval counts to global counters.
        __device__
        void FinishBase() {
            totalSkippedRetrievals->fetch_add(skippedRetrievalN);
            if (Compute::Grid1DHelpers::LaneIdx() == 0) {
                totalRetrievals->fetch_add(retrievalN);
            }
            Implementation().Finish();
        }

    protected:
        /// @brief Constant signifying the task is empty.
        inline static constexpr
        int32 TASK_EMPTY{ std::numeric_limits<int32>::min() / 2 };

        /// @brief Constant signifying the task is locked.
        inline static constexpr
        int32 TASK_LOCKED{ 0 };

        /// @brief Primary task pool.
        DevicePointer<TaskPool> taskPool;
        /// @brief Size of all task pools.
        int32 taskPoolSize;
        /// @brief Number of working subgroups.
        /// @details Decreases after retrieval restarts for the first time, until a task is found.
        /// When zero, the kernel will end.
        DevicePointer<AtomicInt> workingSubgroups;
        /// @brief Number of work chunks each thread will retrieve.
        int32 chunksToRetrieve;

        /// @brief Counter for total task retrieval count.
        DevicePointer<AtomicInt> totalRetrievals;
        /// @brief Counter for total task retrieval skip count.
        DevicePointer<AtomicInt> totalSkippedRetrievals;

    protected:
        /// @brief Shortcut to the selected task variable.
        __device__
        GroupTask* SelectedTask() { return sharedParam->taskInfo.selectedTask; }

        /// @brief Computes the selected task idx in the current task pool.
        __device__
        ptrdiff_t SelectedTaskIdx() const { return sharedParam->taskInfo.selectedTask - sharedParam->taskInfo.selectedTaskPool->tasks; }

        /// @brief Computes the task pool size.
        __host__ static
        int32 GetTaskPoolSize(Compute::TaskPoolProcessorParams const& params) {
            return params.groupN * params.subgroupsPerGroupN * Compute::SUBGROUP_SIZE * params.taskPoolMult;
        }

        /// @brief Sets up required variables before launching the task pool processing kernel.
        /// @param state Processor state, needed to be saved outside the processor class
        /// @param initialTasks Array of initial tasks to be copied to the GPU
        /// @param taskHeaders Array of task headers corresponding to the initial tasks
        /// @param params Other parameters
        template<class TTaskPoolDeleter>
        __host__
        void RunKernel(PersistentState<TTaskPoolDeleter>& state, std::span<GroupTask const> initialTasks, std::span<int32 const> taskHeaders, Compute::TaskPoolProcessorParams params) {
            using namespace Compute;

            // Compute the size of each task pool, guarantees that the size will be divisible by the subgroup size and the total number of subgroups
            taskPoolSize = GetTaskPoolSize(params);

            if (taskPoolSize < initialTasks.size()) {
                throw std::invalid_argument("Task pool size must be larger than initial number of tasks!");
            }

            #if PRT_TIME_KERNELS
            state.retrievalTimer.SetThreadCount(params.groupN * params.subgroupsPerGroupN * SUBGROUP_SIZE);
            state.processTimer.SetThreadCount(params.groupN * params.subgroupsPerGroupN * SUBGROUP_SIZE);
            state.postProcessTimer.SetThreadCount(params.groupN * params.subgroupsPerGroupN * SUBGROUP_SIZE);
            #endif

            if (state.taskPool == nullptr) {
                // Init the task pool
                TaskPool taskPool_;
                
                // upload tasks
                state.tasks = DeviceUniquePointer<GroupTask[]>::Allocate(taskPoolSize);
                state.tasks.Upload(initialTasks);
                taskPool_.tasks = state.tasks.Get();

                // upload task headers
                std::vector<AtomicInt> taskHeadersFull(taskPoolSize);
                std::fill(std::copy(taskHeaders.begin(), taskHeaders.end(), taskHeadersFull.begin()), taskHeadersFull.end(), TASK_EMPTY);
                state.taskHeaders = DeviceUniquePointer<AtomicInt[]>::AllocateAndUpload(taskHeadersFull);
                taskPool_.taskHeaders = state.taskHeaders.Get();

                taskPool_.taskN = StdSize<int32>(initialTasks);
                taskPool_.next = nullptr;
                taskPool_.modifyingNext = false;

                // Upload the task pool struct
                state.taskPool = DeviceUniquePointer<TaskPool, TTaskPoolDeleter>::AllocateAndUpload(taskPool_, state.taskPoolDeleter);
                taskPool = state.taskPool.Get();
            }
            else {
                std::vector<AtomicInt> taskHeadersAtomic(taskHeaders.begin(), taskHeaders.end());

                state.tasks.Upload(initialTasks);
                state.taskHeaders.Upload(taskHeadersAtomic);
                TaskPool taskPool_;
                state.taskPool.Download(&taskPool_);
                taskPool_.taskN = StdSize<int32>(initialTasks);
                state.taskPool.Upload(taskPool_);
            }


            if (workingSubgroups == nullptr) {
                state.workingSubgroups = DeviceUniquePointer<AtomicInt>::AllocateAndUpload(params.groupN * params.subgroupsPerGroupN);
                workingSubgroups = state.workingSubgroups.Get();
            }
            else {
                state.workingSubgroups.Upload(params.groupN * params.subgroupsPerGroupN);
            }

            if (totalRetrievals == nullptr) {
                state.totalRetrievals = DeviceUniquePointer<AtomicInt>::AllocateAndUpload(0);
                totalRetrievals = state.totalRetrievals.Get();
            }
            else {
                state.totalRetrievals.Upload(0);
            }

            if (totalSkippedRetrievals == nullptr) {
                state.totalSkippedRetrievals = DeviceUniquePointer<AtomicInt>::AllocateAndUpload(0);
                totalSkippedRetrievals = state.totalSkippedRetrievals.Get();
            }
            else {
                state.totalSkippedRetrievals.Upload(0);
            }

            chunksToRetrieve = params.chunksToRetrieve;

            // Run the kernel
            auto const groupSize = params.subgroupsPerGroupN * SUBGROUP_SIZE;
            auto const sharedMem = params.subgroupsPerGroupN * sizeof(SharedParameters);
            state.runTimer.Start();
            #if PRT_TIME_KERNELS
            Kernels::ProcessTasks<TTaskProcessor><<<params.groupN, groupSize, sharedMem>>>(Implementation(), state.retrievalTimer.Get(), state.processTimer.Get(), state.postProcessTimer.Get());
            #else
            Kernels::ProcessTasks<TTaskProcessor><<<params.groupN, groupSize, sharedMem>>>(Implementation());
            #endif
            state.runTimer.Stop();
#ifdef PRT_DEBUG
            Compute::CudaCheckError(cudaDeviceSynchronize());
#endif
        }

    private:
        /// @brief Returns the instance cast to the derived type.
        __host__ __device__
        TTaskProcessor& Implementation() requires Compute::TaskProcessorType<TTaskProcessor> { return static_cast<TTaskProcessor&>(*this); }
    };
}
