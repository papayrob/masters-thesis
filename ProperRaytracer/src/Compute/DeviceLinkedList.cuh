/**
 * File name: DeviceLinkedList.cuh
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <cuda/std/utility>

#include "DevicePointer.cuh"
#include "ComputeUtilities.cuh"
#include "MemoryPool.cuh"

namespace ProperRT
{
    /// @brief Class representing a psuedo-linked list on the device.
    /// @details The list consists of smaller lists (=arrays) linked together.
    /// This means that memory can be allocated and deallocated without changing
    /// the data location, meaning that pointer to data remain valid.
    /// Due to not having correct copy and move semantics,
    /// this class should have only one copy in device memory and be copied
    /// to host memory only for uploading and downloading while not being changed
    /// on the device. Otherwise, the host or device copies could be accessing
    /// invalid deallocated memory.
    /// @tparam T Type of data
    template<class T>
    struct DeviceLinkedList
    {
        using SizeType = int64_t;

        /// @brief Pointer to data allocated on the device, inaccessible on the host through dereferencing.
        T* data;
        /// @brief Pointer to the next list, allocated on the device and thus inaccessible on the host.
        DeviceLinkedList* next;
        /// @brief Number of stored elements.
        SizeType count;
        /// @brief Allocated capacity, corresponding to the maximum number of elements this list can hold.
        /// @details When the capacity is exceeded, a new list is allocated and linked to this one.
        SizeType capacity;

        /// @brief Default ctor.
        __host__ __device__
        DeviceLinkedList() noexcept {
            // Init only on host
            #ifndef __CUDA_ARCH__
            data = nullptr;
            next = nullptr;
            count = 0;
            capacity = 0;
            #endif
        }

        /// @brief All member ctor.
        __device__
        DeviceLinkedList(T* data_, DeviceLinkedList* next_, SizeType count_, SizeType capacity_) noexcept
            : data{ data_ }, next{ next_ }, count{ count_ }, capacity{ capacity_ }
        {}

        /// @brief Constructs the list and allocates memory with an initial capacity.
        /// @param initialCapacity Initial size of the array
        __host__ explicit
        DeviceLinkedList(SizeType initialCapacity) noexcept
            : next{ nullptr }, count{ 0 }, capacity{ initialCapacity }
        {
            Compute::CudaCheckError(cudaMalloc(&data, initialCapacity * sizeof(T)));
        }

        /// @brief Returns element at @p idx starting from this list,
        /// possibly delegating the operation to the next list recursively.
        /// @param idx Index of the element to return
        __device__
        T& operator[](auto idx) {
            if (idx < capacity) {
                return data[idx];
            }
            else {
                return (*next)[idx - capacity];
            }
        }

        /// @brief Pushes back @p value, possibly allocating and linking a new list if size exceeds capacity.
        /// @details Only call with one thread at a time if the object is in shared or global memory.
        /// @param value Value to push back
        /// @return Pointer to the pushed back value
        __device__
        T* PushBack(T&& value) {
            T* location = ReserveBack();
            *location = cuda::std::forward<T>(value);
            return location;
        }

        /// @brief Pushes back @p value, possibly allocating and linking a new list if size exceeds capacity.
        /// @details Only call with one thread at a time if the object is in shared or global memory.
        /// @param value Value to push back
        /// @param memPool Memory pool used for possible allocation
        /// @return Pointer to the pushed back value
        __device__
        T* PushBack(T&& value, DevicePointer<MemoryPool> memPool) {
            T* location = ReserveBack(memPool);
            *location = cuda::std::forward<T>(value);
            return location;
        }

        /// @brief Reserves a slot at the back of the list, possibly allocating and linking a new list if size exceeds capacity.
        /// @details Only call with one thread at a time if the object is in shared or global memory.
        /// @return Pointer to the reserved value
        __device__
        T* ReserveBack() {
            if (count == capacity) {
                if (next == nullptr) {
                    T* nextPointer = new T[capacity * 2];
                    next = new DeviceLinkedList{
                        nextPointer,
                        nullptr,
                        1,
                        capacity * 2
                    };

                    if (nextPointer == nullptr || next == nullptr) {
                        printf("Error: Out of dynamic heap memory\n");
                        __trap();
                    }

                    return nextPointer;
                }
                else {
                    return next->ReserveBack();
                }
            }
            else {
                ++count;
                return data + count - 1;
            }
        }

        /// @brief Reserves a slot at the back of the list, possibly allocating and linking a new list if size exceeds capacity.
        /// @details Only call with one thread at a time if the object is in shared or global memory.
        /// @param memPool Memory pool used for possible allocation
        /// @return Pointer to the reserved value
        __device__
        T* ReserveBack(DevicePointer<MemoryPool> memPool) {
            if (count == capacity) {
                if (next == nullptr) {
                    T* nextPointer = memPool->Allocate<T>(1, capacity * 2).data;
                    next = memPool->Allocate<DeviceLinkedList>(1, 1).data;
                    *next = DeviceLinkedList{
                        nextPointer,
                        nullptr,
                        1,
                        capacity * 2
                    };
                    
                    return nextPointer;
                }
                else {
                    return next->ReserveBack();
                }
            }
            else {
                ++count;
                return data + count - 1;
            }
        }

        /// @brief Returns the size of bytes the list currently holds.
        __host__ __device__
        SizeType Bytes() { return count * sizeof(T); }
    };

    /// @brief Class for storing the beginning and end of DeviceLinkedList and providing methods for manipulation.
    /// @tparam T Type of data
    template<class T>
    struct DeviceLinkedListHead
    {
        using SizeType = typename DeviceLinkedList<T>::SizeType;

        /// @brief The first list.
        DeviceLinkedList<T>* first;
        /// @brief The last list for faster inserting.
        DeviceLinkedList<T>* last;

        /// @brief Returns the total size of the lists.
        __host__
        SizeType TotalSize() const;

        /// @brief Downloads the data into a single vector and returns it.
        __host__
        std::vector<T> Download() const;

        /// @brief Calls PushBack on the last list with @p value and returns the result.
        __device__
        T* PushBack(T&& value) {
            T* location = last->PushBack(cuda::std::forward<T>(value));
            if (last->next != nullptr) {
                last = last->next;
            }

            return location;
        }

        /// @brief Calls PushBack on the last list with @p value and @p memPool and returns the result.
        __device__
        T* PushBack(T&& value, DevicePointer<MemoryPool> memPool) {
            T* location = last->PushBack(cuda::std::forward<T>(value), memPool);
            if (last->next != nullptr) {
                last = last->next;
            }

            return location;
        }

        /// @brief Calls ReserveBack on the last list and returns the result.
        __device__
        T* ReserveBack() {
            T* location = last->ReserveBack();
            if (last->next != nullptr) {
                last = last->next;
            }

            return location;
        }

        /// @brief Calls ReserveBack on the last list with @p memPool and returns the result.
        __device__
        T* ReserveBack(DevicePointer<MemoryPool> memPool) {
            T* location = last->ReserveBack(memPool);
            if (last->next != nullptr) {
                last = last->next;
            }

            return location;
        }

        /// @brief Calls the indexing operator on the first list with @p idx and returns the result.
        __device__
        T& operator[](auto idx) {
            return (*first)[idx];
        }
    };

    namespace Kernels
    {
        /// @brief Computes the total size of the linked lists starting with @p head and assigns the result to @p sizeOut.
        template<class T>
        __global__
        void GetDeviceLinkedListSize(DeviceLinkedListHead<T> head, DevicePointer<typename DeviceLinkedList<T>::SizeType> sizeOut) {
            typename DeviceLinkedList<T>::SizeType size{ 0 };
            
            DeviceLinkedList<T>* list = head.first;
            // Go through all lists
            while (list != nullptr) {
                size += list->count;
                list = list->next;
            }

            *sizeOut = size;
        }

        /// @brief Simple copy kernel that assumes an item per thread, copies items from linked lists starting at @p first to @p out.
        template<class T>
        __global__
        void DownloadDeviceLinkedList(DeviceLinkedList<T> first, DevicePointer<T> out) {
            int threadID = blockIdx.x * blockDim.x + threadIdx.x;
            out[threadID] = first[threadID];
        }

        /// @brief Deallocates linked lists starting with first->next, assumes allocation with device new.
        template<class T>
        __global__
        void DeallocateDeviceLinkedList(DeviceLinkedList<T>* first) {
            DeviceLinkedList<T>* list = first->next;
            while (list != nullptr) {
                delete list->data;
                DeviceLinkedList<T>* next = list->next;
                delete list;
                list = next;
            }
        }
    }

    template<class T>
    __host__ inline
    typename DeviceLinkedListHead<T>::SizeType DeviceLinkedListHead<T>::TotalSize() const {
        auto sizeGPU = DeviceUniquePointer<SizeType>::Allocate();
        Kernels::GetDeviceLinkedListSize<<<1, 1>>>(*this, sizeGPU.Get());
        #ifdef PRT_DEBUG
        Compute::CudaCheckError(cudaDeviceSynchronize());
        #endif
        return sizeGPU.Download();
    }

    template<class T>
    __host__ inline
    std::vector<T> DeviceLinkedListHead<T>::Download() const {
        SizeType size = TotalSize();
        auto outGPU = DeviceUniquePointer<T[]>::Allocate(size);
        SizeType blockDim = 256;
        SizeType gridDim = IntDivRoundUp(size, blockDim);
        Kernels::DownloadDeviceLinkedList<<<gridDim, blockDim>>>(*first, outGPU.Get());
        #ifdef PRT_DEBUG
        Compute::CudaCheckError(cudaDeviceSynchronize());
        #endif
        return outGPU.Download();
    }

    /// @brief DeviceLinkedListHead with RAII, intended to be used only on host.
    /// @tparam T Data type
    /// @tparam TDeleter Deleter functor type
    template<class T, class TDeleter>
    class DeviceLinkedListUniqueHead : public DeviceLinkedListHead<T>
    {
    public:
        using Base = DeviceLinkedListHead<T>;
        using Base::first;
        using Base::last;

        using Base::TotalSize;
        using Base::Download;

        using SizeType = typename DeviceLinkedList<T>::SizeType;

        /// @brief Default ctor.
        DeviceLinkedListUniqueHead() noexcept
            : Base{ nullptr, nullptr }
        {}
        
        /// @brief Creates the first list and initializes it with @p initialCapacity.
        /// @param initialCapacity Initial size of the first list
        explicit
        DeviceLinkedListUniqueHead(SizeType initialCapacity) {
            // Create first list on cpu
            DeviceLinkedList<T> list{ initialCapacity };
            // Allocate first list
            Compute::CudaCheckError(cudaMalloc(&first, sizeof(decltype(list))));
            last = first;
            // Copy it to the gpu
            Compute::CudaCheckError(cudaMemcpy(first, &list, sizeof(decltype(list)), cudaMemcpyKind::cudaMemcpyHostToDevice));
        }

        /// @brief Creates the first list and initializes it with @p input.
        /// @param input Initial data
        explicit
        DeviceLinkedListUniqueHead(std::span<T const> input) {
            // Get power of two size capable of holding all data
            SizeType initialSize = std::max(SizeType{ Compute::SUBGROUP_SIZE }, static_cast<SizeType>(RoundUpToNearestPowerOf2(input.size())));

            // Create first list on cpu
            DeviceLinkedList<T> list{ initialSize };
            // Allocate first list
            Compute::CudaCheckError(cudaMalloc(&first, sizeof(decltype(list))));
            last = first;
            // Copy initial data to gpu
            Compute::CudaCheckError(cudaMemcpy(list.data, input.data(), input.size_bytes(), cudaMemcpyKind::cudaMemcpyHostToDevice));
            // Copy list to the gpu
            Compute::CudaCheckError(cudaMemcpy(first, &list, sizeof(decltype(list)), cudaMemcpyKind::cudaMemcpyHostToDevice));
        }

        DeviceLinkedListUniqueHead(DeviceLinkedListUniqueHead const& other) = delete;
        DeviceLinkedListUniqueHead& operator=(DeviceLinkedListUniqueHead const& other) = delete;

        /// @brief Move ctor.
        DeviceLinkedListUniqueHead(DeviceLinkedListUniqueHead&& other)
            : Base{ other.first, other.last }
        {
            other.first = nullptr;
            other.last = nullptr;
        }

        /// @brief Move assignment.
        DeviceLinkedListUniqueHead& operator=(DeviceLinkedListUniqueHead&& other) {
            if (this != &other) {
                TDeleter{}(*this);
                first = other.first;
                last = other.last;
            }
            return *this;
        }

        /// @brief Dtor, calls TDeleter{}.
        ~DeviceLinkedListUniqueHead() noexcept {
            TDeleter{}(*this);
            first = nullptr;
            last = nullptr;
        }
    };
    
    namespace DeviceLinkedListDeleters
    {
        /// @brief Deleter class for DeviceLinkedList allocated with device new.
        /// @tparam T Data type
        template<class T>
        struct DeviceNewDeleter
        {
            /// @brief Default ctor.
            constexpr
            DeviceNewDeleter() noexcept = default;

            /// @brief Default ctor.
            template<class TOther>
                requires std::is_convertible_v<TOther*, T*>
            constexpr
            DeviceNewDeleter(DeviceNewDeleter<TOther> const&) noexcept {}

            /// @brief Deletes all lists using device delete.
            void operator()(DeviceLinkedListUniqueHead<T, DeviceNewDeleter<T>>& head) const {
                static_assert(0 < sizeof(T), "Cannot delete an incomplete type");
                if (head.first != nullptr) {
                    // Copy list to cpu
                    DeviceLinkedList<T> list{};
                    Compute::CudaCheckError(cudaMemcpy(&list, head.first, sizeof(decltype(list)), cudaMemcpyKind::cudaMemcpyDeviceToHost));

                    // Free gpu data allocated by device
                    if (list.next != nullptr) {
                        Kernels::DeallocateDeviceLinkedList<<<1, 1>>>(head.first);
                        #ifdef PRT_DEBUG
                        Compute::CudaCheckError(cudaDeviceSynchronize());
                        #endif
                    }

                    // Free gpu data allocated by host
                    Compute::CudaCheckError(cudaFree(list.data));
                    Compute::CudaCheckError(cudaFree(head.first));
                }
            }
        };

        /// @brief Deleter class for DeviceLinkedList allocated using the MemoryPool.
        /// @tparam T Data type
        template<class T>
        struct MemPoolDeleter
        {
            /// @brief Default ctor.
            constexpr
            MemPoolDeleter() noexcept = default;

            /// @brief Default ctor.
            template<class TOther>
                requires std::is_convertible_v<TOther*, T*>
            constexpr
            MemPoolDeleter(MemPoolDeleter<TOther> const&) noexcept {}

            /// @brief Deletes all memory allocated by the host, the rest is assumed to be deallocated with memory pool.
            void operator()(DeviceLinkedListUniqueHead<T, MemPoolDeleter<T>>& head) const {
                static_assert(0 < sizeof(T), "Cannot delete an incomplete type");
                if (head.first != nullptr) {
                    // Copy list to cpu
                    DeviceLinkedList<T> list{};
                    Compute::CudaCheckError(cudaMemcpy(&list, head.first, sizeof(decltype(list)), cudaMemcpyKind::cudaMemcpyDeviceToHost));

                    // Free gpu data allocated by host
                    Compute::CudaCheckError(cudaFree(list.data));
                    Compute::CudaCheckError(cudaFree(head.first));
                }
            }
        };
    }
}