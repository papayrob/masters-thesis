/**
 * File name: DevicePointer.cuh
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <span>

#include "CudaHelpers.cuh"
#include "Utilities/StrippedType.hpp"
#include "Utilities/NullReferenceException.hpp"

namespace ProperRT
{
    /// @brief Class for storing a pointer and a deleter instance, without wasting space for an empty deleter.
    /// @tparam TPointer Pointer type
    /// @tparam TDeleter Deleter type
    template<class TPointer, class TDeleter>
    class PointerDeleterPair final : private TDeleter
    {
    public:
        /// @brief Data pointer.
        TPointer data;

        using Base = TDeleter;

        /// @brief Constructs the class from a pointer and a deleter
        /// @param ptr Data pointer
        /// @param del Deleter instance
        constexpr
        PointerDeleterPair(TPointer ptr, TDeleter const& del)
            : data{ ptr }, TDeleter{ del }
        {}

        /// @brief Constructs the class from a pointer and a deleter
        /// @param ptr Data pointer
        /// @param del Deleter instance
        constexpr
        PointerDeleterPair(TPointer ptr, TDeleter&& del)
            : data{ ptr }, TDeleter{ std::move(del) }
        {}

        /// @brief Returns the deleter instance.
        constexpr
        TDeleter& GetDeleter() noexcept {
            return *this;
        }

        /// @brief Returns the deleter instance.
        constexpr
        TDeleter const& GetDeleter() const noexcept {
            return *this;
        }
    };

    /// @brief Deleter for CUDA memory allocated on the host.
    /// @tparam T Data type
    template<class T>
    struct CudaHostDelete
    {
        /// @brief Default ctor.
        constexpr
        CudaHostDelete() noexcept = default;

        /// @brief Copy ctor.
        template<class TOther>
            requires std::is_convertible_v<TOther*, T*>
        constexpr
        CudaHostDelete(CudaHostDelete<TOther> const&) noexcept {}

        /// @brief Deleter functor.
        /// @param pointer Pointer to delete using cudaFree
        void operator()(T* pointer) const {
            static_assert(0 < sizeof(T), "Cannot delete an incomplete type");
            if (pointer != nullptr) {
                Compute::CudaCheckError(cudaFree(pointer));
            }
        }
    };

    /// @brief Deleter for CUDA memory allocated on the host.
    /// @tparam T Data type
    template<class T>
    struct CudaHostDelete<T[]>
    {
        /// @brief Default ctor.
        constexpr
        CudaHostDelete() noexcept = default;

        /// @brief Copy ctor.
        template<class TOther>
            requires std::is_convertible_v<TOther (*)[], T (*)[]>
        constexpr
        CudaHostDelete(CudaHostDelete<TOther> const&) noexcept {}

        /// @brief Deleter functor.
        /// @param pointer Pointer to delete using cudaFree
        void operator()(T* pointer) const {
            static_assert(0 < sizeof(T), "Cannot delete an incomplete type");
            if (pointer != nullptr) {
                Compute::CudaCheckError(cudaFree(pointer));
            }
        }
    };

    namespace Kernels
    {
        /// @brief Calls delete on @p pointer.
        template<class T>
        __global__
        void DeletePointer(T* pointer) {
            delete pointer;
        }

        /// @brief Calls delete[] on @p pointer.
        template<class T>
        __global__
        void DeleteArray(T* pointer) {
            delete[] pointer;
        }
    }

    /// @brief Deleter for CUDA memory allocated on the device.
    /// @tparam T Data type
    template<class T>
    struct CudaDeviceDelete
    {
        /// @brief Default ctor.
        constexpr
        CudaDeviceDelete() noexcept = default;

        /// @brief Copy ctor.
        template<class TOther>
            requires std::is_convertible_v<TOther*, T*>
        constexpr
        CudaDeviceDelete(CudaDeviceDelete<TOther> const&) noexcept {}

        /// @brief Deleter functor.
        /// @param pointer Pointer to delete using device delete
        void operator()(T* pointer) const {
            static_assert(0 < sizeof(T), "Cannot delete an incomplete type");
            if (pointer != nullptr) {
                Kernels::DeletePointer<<<1,1>>>(pointer);
                #ifdef PRT_DEBUG
                Compute::CudaCheckError(cudaDeviceSynchronize());
                #endif
            }
        }
    };

    /// @brief Deleter for CUDA memory allocated on the device.
    /// @tparam T Data type
    template<class T>
    struct CudaDeviceDelete<T[]>
    {
        /// @brief Default ctor.
        constexpr
        CudaDeviceDelete() noexcept = default;

        /// @brief Copy ctor.
        template<class TOther>
            requires std::is_convertible_v<TOther (*)[], T (*)[]>
        constexpr
        CudaDeviceDelete(CudaDeviceDelete<TOther> const&) noexcept {}

        /// @brief Deleter functor.
        /// @param pointer Pointer to delete using device delete
        void operator()(T* pointer) const {
            static_assert(0 < sizeof(T), "Cannot delete an incomplete type");
            if (pointer != nullptr) {
                Kernels::DeleteArray<<<1,1>>>(pointer);
                #ifdef PRT_DEBUG
                Compute::CudaCheckError(cudaDeviceSynchronize());
                #endif
            }
        }
    };

    /// @brief Class for representing a raw device pointer accessible only on the GPU.
    /// @tparam T Data type
    template<class T>
    struct DevicePointer
    {
        /// @brief Pointer allocated on the device, inaccessible on the host through dereferencing.
        T* data{ nullptr };

        /// @brief Implicit conversion to a raw pointer.
        __host__ __device__
        operator T*() const { return data; }

        /// @brief Implicit conversion to a void pointer.
        __host__ __device__
        operator void*() const { return data; }

        /// @brief Default comparison.
        bool operator==(DevicePointer const&) const = default;

        /// @brief Comparison with nullptr.
        __host__ __device__
        bool operator==(cuda::std::nullptr_t) const { return (data == nullptr); }

        /// @brief Default comparison.
        bool operator!=(DevicePointer const&) const = default;

        /// @brief Comparison with nullptr.
        __host__ __device__
        bool operator!=(cuda::std::nullptr_t) const { return (data != nullptr); }

        /// @brief Access pointer, throws when accessing on host.
        __host__ __device__
        T* operator->() {
            #ifdef __CUDA_ARCH__
            return data;
            #else
            throw NullReferenceException("Cannot access device pointer on host");
            #endif
        }

        /// @brief Access pointer, throws when accessing on host.
        __host__ __device__
        T& operator*() {
            #ifdef __CUDA_ARCH__
            return *data;
            #else
            throw NullReferenceException("Cannot access device pointer on host");
            #endif
        }

        /// @brief Uploads the value to the GPU.
        /// @param init Value to be uploaded
        __host__
        void Upload(T const& init) const {
            Compute::CudaCheckError(cudaMemcpy(data, &init, sizeof(T), cudaMemcpyKind::cudaMemcpyHostToDevice));
        }

        /// @brief Downloads the value from the GPU and reutrns it.
        __host__
        T Download() const {
            T value;
            Compute::CudaCheckError(cudaMemcpy(&value, data, sizeof(T), cudaMemcpyKind::cudaMemcpyDeviceToHost));
            return value;
        }

        /// @brief Downloads the value from the GPU to @p value.
        /// @param[out] value Pointer to which the value will be downloaded
        __host__
        void Download(T* value) const {
            Compute::CudaCheckError(cudaMemcpy(value, data, sizeof(T), cudaMemcpyKind::cudaMemcpyDeviceToHost));
        }

        /// @brief Uses the host cudaMemcpy function to copy data from this pointer to @p other.
        /// @param other Pointer to copy to
        __host__
        void HostMemCopyTo(DevicePointer other) const {
            Compute::CudaCheckError(cudaMemcpy(other.data, data, sizeof(T), cudaMemcpyKind::cudaMemcpyDeviceToDevice));
        }
    };

    /// @brief Class for representing device arrays accessible only on the GPU.
    /// @tparam T Data type
    template<class T>
    struct DevicePointer<T[]>
    {
        using SizeType = int64;

        /// @brief Pointer allocated on the device, inaccessible on the host through dereferencing.
        T* data{ nullptr };
        /// @brief Number of stored elements.
        SizeType count{ 0 };

        /// @brief Implicit conversion to a raw pointer.
        __host__ __device__
        operator T*() const { return data; }

        /// @brief Implicit conversion to a void pointer.
        __host__ __device__
        operator void*() const { return data; }

        /// @brief Default comparison.
        bool operator==(DevicePointer const&) const = default;

        /// @brief Comparison with nullptr.
        __host__ __device__
        bool operator==(cuda::std::nullptr_t) const { return (data == nullptr); }

        /// @brief Default comparison.
        bool operator!=(DevicePointer const&) const = default;

        /// @brief Comparison with nullptr.
        __host__ __device__
        bool operator!=(cuda::std::nullptr_t) const { return (data != nullptr); }

        /// @brief Indexing operator, throws when accessed on the host.
        __device__
        T& operator[](auto idx) {
            #ifdef __CUDA_ARCH__
            return data[idx];
            #else
            throw NullReferenceException("Cannot access device pointer on host");
            #endif
        }

        /// @brief Returns the number of bytes represented by this pointer/array.
        __host__ __device__
        SizeType Bytes() const { return count * sizeof(T); }

        /// @brief Uploads @p init to the GPU.
        /// @param init Array to be uploaded
        __host__
        void Upload(std::span<T const> init) const {
            if (static_cast<uint64>(init.size()) > static_cast<uint64>(count)) {
                throw std::runtime_error("Uploading more bytes than allocated");
            }
            Compute::CudaCheckError(cudaMemcpy(data, init.data(), init.size_bytes(), cudaMemcpyKind::cudaMemcpyHostToDevice));
        }

        /// @brief Downloads the array from the GPU and returns it.
        __host__
        std::vector<T> Download() const {
            std::vector<T> out(count);
            if (count > 0) {
                Compute::CudaCheckError(cudaMemcpy(out.data(), data, Bytes(), cudaMemcpyKind::cudaMemcpyDeviceToHost));
            }
            return out;
        }
        
        /// @brief Downloads the array from the GPU to @p out.
        /// @param[out] out Output array to which the array will be downloaded
        __host__
        void Download(std::span<T> out) const {
            if (static_cast<uint64>(out.size()) != static_cast<uint64>(count)) {
                throw std::runtime_error("Downloading different amount of bytes than allocated");
            }
            if (count > 0) {
                Compute::CudaCheckError(cudaMemcpy(out.data(), data, Bytes(), cudaMemcpyKind::cudaMemcpyDeviceToHost));
            }
        }

        /// @brief Uses the host cudaMemcpy function to copy data from this pointer to @p other.
        /// @param other Pointer to copy to
        __host__
        void HostMemCopyTo(DevicePointer other) {
            if (other.count < count) {
                //TODO custom exception
                throw std::runtime_error("Cannot copy to an array of smaller size");
            }

            if (count > 0) {
                Compute::CudaCheckError(cudaMemcpy(other.data, data, Bytes(), cudaMemcpyKind::cudaMemcpyDeviceToDevice));
            }
        }
    };

    namespace Kernels
    {
        /// @brief Calles new on the device and assigns the result to @p pointerToPointer.
        template<class T>
        __global__
        void Allocate(DevicePointer<T*> pointerToPointer) {
            *pointerToPointer = new T;
        }

        /// @brief Calles new[] on the device with @p size and assigns the result to @p pointerToPointer.
        template<class T>
            requires (!std::is_array_v<T>)
        __global__
        void Allocate(DevicePointer<T*> pointerToPointer, int64 size) {
            *pointerToPointer = new T[size];
        }

        /// @brief Calles new[] on the device with @p size and assigns the result to @p pointerToPointer.
        template<class TArray>
            requires std::is_array_v<TArray>
        __global__
        void Allocate(DevicePointer<std::remove_extent_t<TArray>*> pointerToPointer, int64 size) {
            using T = std::remove_extent_t<TArray>;
            *pointerToPointer = new T[size];
        }
    }

    /// @brief RAII wrapper for DevicePointer with custom deleter support.
    /// @tparam T Data type
    /// @tparam TDeleter Deleter functor type
    template<class T, class TDeleter = CudaHostDelete<T>>
    class DeviceUniquePointer
    {
        template<class S, class SDeleter>
        friend class DeviceUniquePointer;

    public:
        using Pointer = T*;
        using Deleter = TDeleter;

    public:
        /// @brief Allocates memory using cudaMalloc and returns a unique pointer owning it.
        /// @param deleter Optional deleter instance
        static
        DeviceUniquePointer Allocate(Deleter const& deleter = Deleter{}) {
            DeviceUniquePointer ptr{ deleter };
            Compute::CudaCheckError(cudaMalloc(&ptr.dataPair.data, sizeof(T)));
            return ptr;
        }

        /// @brief Allocates memory using cudaMalloc and returns a unique pointer owning it.
        /// @param deleter Deleter instance
        static
        DeviceUniquePointer Allocate(Deleter&& deleter) {
            DeviceUniquePointer ptr{ std::move(deleter) };
            Compute::CudaCheckError(cudaMalloc(&ptr.dataPair.data, sizeof(T)));
            return ptr;
        }

        /// @brief Allocates memory using cudaMalloc, initializes it with @p init and returns a unique pointer owning it.
        /// @param init Initial value
        /// @param deleter Optional deleter instance
        static
        DeviceUniquePointer AllocateAndUpload(T const& init, Deleter const& deleter = Deleter{}) {
            DeviceUniquePointer ptr = Allocate(deleter);
            ptr.Upload(init);
            return ptr;
        }

        /// @brief Allocates memory using cudaMalloc, initializes it with @p init and returns a unique pointer owning it.
        /// @param init Initial value
        /// @param deleter Deleter instance
        static
        DeviceUniquePointer AllocateAndUpload(T const& init, Deleter&& deleter) {
            DeviceUniquePointer ptr = Allocate(std::move(deleter));
            ptr.Upload(init);
            return ptr;
        }

        /// @brief Allocates memory using device new and returns a unique pointer owning it.
        static
        DeviceUniquePointer<T, CudaDeviceDelete<T>> AllocateWithDeviceNew() {
            return DeviceUniquePointer<T, CudaDeviceDelete<T>>::AllocateWithDeviceNew(CudaDeviceDelete<T>{});
        }

        /// @brief Allocates memory using device new and returns a unique pointer owning it.
        /// @param deleter Deleter instance (must use device delete)
        static
        DeviceUniquePointer AllocateWithDeviceNew(Deleter const& deleter) {
            auto pointerGPU = DeviceUniquePointer<Pointer>::Allocate();
            Kernels::Allocate<<<1,1>>>(pointerGPU.Get());
            #ifdef PRT_DEBUG
            Compute::CudaCheckError(cudaDeviceSynchronize());
            #endif
            DeviceUniquePointer ptr{ deleter };
            ptr.dataPair.data = pointerGPU.Download();
            return ptr;
        }

        /// @brief Allocates memory using device new and returns a unique pointer owning it.
        /// @param deleter Deleter instance (must use device delete)
        static
        DeviceUniquePointer AllocateWithDeviceNew(Deleter&& deleter) {
            auto pointerGPU = DeviceUniquePointer<Pointer>::Allocate();
            Kernels::Allocate<<<1,1>>>(pointerGPU.Get());
            #ifdef PRT_DEBUG
            Compute::CudaCheckError(cudaDeviceSynchronize());
            #endif
            DeviceUniquePointer ptr{ std::move(deleter) };
            ptr.dataPair.data = pointerGPU.Download();
            return ptr;
        }

    public:
        /// @brief Default ctor.
        /// @param deleter Optional deleter instance
        DeviceUniquePointer(Deleter const& deleter = Deleter{})
            : dataPair{ nullptr, deleter }
        {}

        /// @brief Default ctor.
        /// @param deleter Deleter instance
        DeviceUniquePointer(Deleter&& deleter)
            : dataPair{ nullptr, std::move(deleter) }
        {}

        /// @brief Acquires ownership of @p pointer.
        /// @param pointer Pointer to allocated memory
        /// @param deleter Optional deleter instance
        DeviceUniquePointer(Pointer pointer, Deleter const& deleter = Deleter{})
            : dataPair{ pointer, deleter }
        {}

        /// @brief Acquires ownership of @p pointer.
        /// @param pointer Pointer to allocated memory
        /// @param deleter Deleter instance
        DeviceUniquePointer(Pointer pointer, Deleter&& deleter)
            : dataPair{ pointer, std::move(deleter) }
        {}

        /// @brief Nullptr ctor.
        /// @param deleter Optional deleter instance
        DeviceUniquePointer(std::nullptr_t, Deleter const& deleter = Deleter{})
            : dataPair{ nullptr, deleter }
        {}

        /// @brief Nullptr ctor.
        /// @param deleter Deleter instance
        DeviceUniquePointer(std::nullptr_t, Deleter&& deleter)
            : dataPair{ nullptr, std::move(deleter) }
        {}

        /// @brief Dtor, calles deleter functor.
        ~DeviceUniquePointer() {
            dataPair.GetDeleter()(dataPair.data);
        }

        DeviceUniquePointer(DeviceUniquePointer const&) = delete;
        DeviceUniquePointer& operator=(DeviceUniquePointer const&) = delete;

        /// @brief Nullptr assignment.
        DeviceUniquePointer& operator=(std::nullptr_t) {
            if (dataPair.data != nullptr) {
                dataPair.GetDeleter()(dataPair.data);
            }
            dataPair.data = nullptr;
            return *this;
        }

        /// @brief Move ctor.
        DeviceUniquePointer(DeviceUniquePointer&& other)
            : dataPair{ other.dataPair }
        {
            other.dataPair.data = nullptr;
        }

        /// @brief Move assignment.
        DeviceUniquePointer& operator=(DeviceUniquePointer&& other) {
            if (this != &other) {
                if (dataPair.data != nullptr) {
                    dataPair.GetDeleter()(dataPair.data);
                }
                dataPair = std::move(other.dataPair);
                other.dataPair.data = nullptr;
            }

            return *this;
        }

        /// @brief Comparison operator.
        bool operator==(DeviceUniquePointer const&) const = default;

        /// @brief Nullptr comparison operator.
        bool operator==(cuda::std::nullptr_t) const { return (dataPair.data == nullptr); }

        /// @brief Comparison operator.
        bool operator!=(DeviceUniquePointer const&) const = default;

        /// @brief Nullptr comparison operator.
        bool operator!=(cuda::std::nullptr_t) const { return (dataPair.data != nullptr); }

        /// @brief Uploads @p init to the GPU.
        /// @param init Value to be uploaded
        __host__
        void Upload(T const& init) const {
            Compute::CudaCheckError(cudaMemcpy(dataPair.data, &init, sizeof(T), cudaMemcpyKind::cudaMemcpyHostToDevice));
        }

        /// @brief Downloads the value from the GPU and returns it.
        __host__
        T Download() const {
            T value;
            Compute::CudaCheckError(cudaMemcpy(&value, dataPair.data, sizeof(T), cudaMemcpyKind::cudaMemcpyDeviceToHost));
            return value;
        }

        /// @brief Downloads the value from the GPU to @p value.
        /// @param[out] value Pointer to which the value will be downloaded
        __host__
        void Download(T* value) const {
            Compute::CudaCheckError(cudaMemcpy(value, dataPair.data, sizeof(T), cudaMemcpyKind::cudaMemcpyDeviceToHost));
        }

        /// @brief Returns a non-owning DevicePointer to the owned data.
        DevicePointer<T> Get() const { return DevicePointer<T>{ dataPair.data }; }

        /// @brief Releases the ownership of owned data and returns a non-owning DevicePointer with it.
        DevicePointer<T> Release() {
            DevicePointer<T> ptr = Get();
            dataPair.data = nullptr;
            return ptr;
        }

    private:
        PointerDeleterPair<Pointer, Deleter> dataPair{ nullptr, Deleter{} };
    };

    /// @brief RAII wrapper for DevicePointer with custom deleter support.
    /// @tparam T Data type
    /// @tparam TDeleter Deleter functor type
    template<class T, class TDeleter>
    class DeviceUniquePointer<T[], TDeleter>
    {
        template<class S, class SDeleter>
        friend class DeviceUniquePointer;

    public:
        using Pointer = T*;
        using Deleter = TDeleter;
        using DevicePointer = DevicePointer<T[]>;
        using SizeType = DevicePointer::SizeType;

    public:
        /// @brief Allocates memory using cudaMalloc and returns a unique pointer owning it.
        /// @param count Number of elements to allocate memory for
        /// @param deleter Optional deleter instance
        static
        DeviceUniquePointer Allocate(SizeType count, Deleter const& deleter = Deleter{}) {
            DeviceUniquePointer ptr{ deleter };
            ptr.count = count;
            Compute::CudaCheckError(cudaMalloc(&ptr.dataPair.data, ptr.Bytes()));
            return ptr;
        }

        /// @brief Allocates memory using cudaMalloc and returns a unique pointer owning it.
        /// @param count Number of elements to allocate memory for
        /// @param deleter Deleter instance
        static
        DeviceUniquePointer Allocate(SizeType count, Deleter&& deleter) {
            DeviceUniquePointer ptr{ std::move(deleter) };
            ptr.count = count;
            Compute::CudaCheckError(cudaMalloc(&ptr.dataPair.data, ptr.Bytes()));
            return ptr;
        }

        /// @brief Allocates memory using cudaMalloc, memsets it with @p value and returns a unique pointer owning it.
        /// @param count Number of elements to allocate memory for
        /// @param value Value to memset with
        /// @param deleter Optional deleter instance
        static
        DeviceUniquePointer AllocateAndSet(SizeType count, int value, Deleter const& deleter = Deleter{}) {
            DeviceUniquePointer ptr = Allocate(count, deleter);
            Compute::CudaCheckError(cudaMemset(ptr.dataPair.data, value, ptr.Bytes()));
            return ptr;
        }

        /// @brief Allocates memory using cudaMalloc, memsets it with @p value and returns a unique pointer owning it.
        /// @param count Number of elements to allocate memory for
        /// @param value Value to memset with
        /// @param deleter Deleter instance
        static
        DeviceUniquePointer AllocateAndSet(SizeType count, int value, Deleter&& deleter) {
            DeviceUniquePointer ptr = Allocate(count, std::move(deleter));
            Compute::CudaCheckError(cudaMemset(ptr.dataPair.data, value, ptr.Bytes()));
            return ptr;
        }

        /// @brief Allocates memory using cudaMalloc, initializes it with @p init and returns a unique pointer owning it.
        /// @param init Initial values, count deduced from init.size()
        /// @param deleter Optional deleter instance
        static
        DeviceUniquePointer AllocateAndUpload(std::span<T const> init, Deleter const& deleter = Deleter{}) {
            DeviceUniquePointer ptr = Allocate(static_cast<SizeType>(init.size()), deleter);
            ptr.Upload(init);
            return ptr;
        }

        /// @brief Allocates memory using cudaMalloc, initializes it with @p init and returns a unique pointer owning it.
        /// @param init Initial values, count deduced from init.size()
        /// @param deleter Deleter instance
        static
        DeviceUniquePointer AllocateAndUpload(std::span<T const> init, Deleter&& deleter) {
            DeviceUniquePointer ptr = Allocate(static_cast<SizeType>(init.size()), std::move(deleter));
            ptr.Upload(init);
            return ptr;
        }

        /// @brief Allocates memory using device new[] and returns a unique pointer owning it.
        /// @param count Number of elements to allocate memory for
        static
        DeviceUniquePointer<T[], CudaDeviceDelete<T>> AllocateWithDeviceNew(SizeType count) {
            return DeviceUniquePointer<T[], CudaDeviceDelete<T>>::AllocateWithDeviceNew(count, CudaDeviceDelete<T>{});
        }

        /// @brief Allocates memory using device new[] and returns a unique pointer owning it.
        /// @param count Number of elements to allocate memory for
        /// @param deleter Deleter instance (must use device delete[])
        static
        DeviceUniquePointer AllocateWithDeviceNew(SizeType count, Deleter const& deleter) {
            auto pointerGPU = DeviceUniquePointer<Pointer>::Allocate();
            Kernels::Allocate<<<1,1>>>(pointerGPU.Get(), count);
            #ifdef PRT_DEBUG
            Compute::CudaCheckError(cudaDeviceSynchronize());
            #endif
            DeviceUniquePointer ptr{ deleter };
            ptr.dataPair.data = pointerGPU.Download();
            ptr.count = count;
            return ptr;
        }

        /// @brief Allocates memory using device new[] and returns a unique pointer owning it.
        /// @param count Number of elements to allocate memory for
        /// @param deleter Deleter instance (must use device delete[])
        static
        DeviceUniquePointer AllocateWithDeviceNew(SizeType count, Deleter&& deleter) {
            auto pointerGPU = DeviceUniquePointer<Pointer>::Allocate();
            Kernels::Allocate<<<1,1>>>(pointerGPU.Get(), count);
            #ifdef PRT_DEBUG
            Compute::CudaCheckError(cudaDeviceSynchronize());
            #endif
            DeviceUniquePointer ptr{ std::move(deleter) };
            ptr.dataPair.data = pointerGPU.Download();
            ptr.count = count;
            return ptr;
        }

    public:
        /// @brief Default ctor.
        /// @param deleter Optional deleter instance
        DeviceUniquePointer(Deleter const& deleter = Deleter{})
            : dataPair{ nullptr, deleter }
        {}

        /// @brief Default ctor.
        /// @param deleter Deleter instance
        DeviceUniquePointer(Deleter&& deleter)
            : dataPair{ nullptr, std::move(deleter) }
        {}

        /// @brief Acquires ownership of @p pointer.
        /// @param pointer Pointer to allocated memory
        /// @param count Number of elements to allocate memory for
        /// @param deleter Optional deleter instance
        DeviceUniquePointer(Pointer pointer, SizeType count_, Deleter const& deleter = Deleter{})
            : dataPair{ pointer, deleter }, count{ count_ }
        {}

        /// @brief Acquires ownership of @p pointer.
        /// @param pointer Pointer to allocated memory
        /// @param count Number of elements to allocate memory for
        /// @param deleter Deleter instance
        DeviceUniquePointer(Pointer pointer, SizeType count, Deleter&& deleter)
            : dataPair{ pointer, std::move(deleter) }
        {}

        /// @brief Nullptr ctor.
        /// @param deleter Optional deleter instance
        DeviceUniquePointer(std::nullptr_t, Deleter const& deleter = Deleter{})
            : dataPair{ nullptr, deleter }
        {}

        /// @brief Nullptr ctor.
        /// @param deleter Deleter instance
        DeviceUniquePointer(std::nullptr_t, Deleter&& deleter)
            : dataPair{ nullptr, std::move(deleter) }
        {}

        /// @brief Dtor, calles deleter functor.
        ~DeviceUniquePointer() {
            dataPair.GetDeleter()(dataPair.data);
        }

        DeviceUniquePointer(DeviceUniquePointer const&) = delete;
        DeviceUniquePointer& operator=(DeviceUniquePointer const&) = delete;

        /// @brief Nullptr assignment.
        DeviceUniquePointer& operator=(std::nullptr_t) {
            if (dataPair.data != nullptr) {
                dataPair.GetDeleter()(dataPair.data);
            }
            dataPair.data = nullptr;
            count = 0;
            return *this;
        }

        /// @brief Move ctor.
        DeviceUniquePointer(DeviceUniquePointer&& other)
            : dataPair{ other.dataPair }, count{ other.count }
        {
            other.dataPair.data = nullptr;
            other.count = 0;
        }

        /// @brief Move assignment.
        DeviceUniquePointer& operator=(DeviceUniquePointer&& other) {
            if (this != &other) {
                if (dataPair.data != nullptr) {
                    dataPair.GetDeleter()(dataPair.data);
                }
                dataPair = std::move(other.dataPair);
                count = other.count;
                other.dataPair.data = nullptr;
                other.count = 0;
            }

            return *this;
        }

        /// @brief Comparison operator.
        bool operator==(DeviceUniquePointer const&) const = default;

        /// @brief Nullptr comparison operator.
        bool operator==(cuda::std::nullptr_t) const { return (dataPair.data == nullptr); }

        /// @brief Comparison operator.
        bool operator!=(DeviceUniquePointer const&) const = default;

        /// @brief Nullptr comparison operator.
        bool operator!=(cuda::std::nullptr_t) const { return (dataPair.data != nullptr); }

        /// @brief Uploads @p init to the GPU.
        /// @param init Array to be uploaded
        __host__
        void Upload(std::span<T const> init) const {
            Get().Upload(init);
        }

        /// @brief Downloads the array from the GPU and returns it.
        __host__
        std::vector<T> Download() const {
            return Get().Download();
        }
        
        /// @brief Downloads the array from the GPU to @p out.
        /// @param[out] out Output array to which the array will be downloaded
        __host__
        void Download(std::span<T> out) const {
            Get().Download(out);
        }

        /// @brief Returns a non-owning DevicePointer to the owned data.
        DevicePointer Get() const { return DevicePointer{ dataPair.data, count }; }

        /// @brief Releases the ownership of owned data and returns a non-owning DevicePointer with it.
        DevicePointer Release() {
            DevicePointer ptr = Get();
            dataPair.data = nullptr;
            count = 0;
            return ptr;
        }

        /// @brief Returns the number of allocated elements.
        SizeType Count() const { return count; }

        /// @brief Returns the number of allocated elements.
        SizeType Size() const { return count; }

        /// @brief Returns the size of allocated memory.
        __host__ __device__
        SizeType Bytes() const { return count * sizeof(T); }

    private:
        PointerDeleterPair<Pointer, Deleter> dataPair{ nullptr, Deleter{} };
        SizeType count{ 0 };
    };
}
