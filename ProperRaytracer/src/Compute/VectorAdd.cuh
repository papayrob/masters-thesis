/**
 * File name: VectorAdd.cuh
 * Description: Test implementation of adding vectors on the GPU.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <span>

#include "DeviceAllocator.cuh"
#include "CudaHelpers.cuh"

namespace ProperRT
{
    namespace Kernels
    {
        template<class T>
        __global__
        void VectorAdd(T const* a, T const* b, T* out) {
            int i = blockIdx.x * blockDim.x + threadIdx.x;
            out[i] = a[i] + b[i];
        }
    }

    namespace Detail
    {
        template<class T, std::size_t NExtent>
        inline
        std::vector<T> VectorAddImpl(std::span<T const, NExtent> a, std::span<T const, NExtent> b) {
            DeviceAllocator allocator{};
            auto devA = DeviceUniquePointer<T[]>::AllocateAndUpload(a);
            auto devB = DeviceUniquePointer<T[]>::AllocateAndUpload(b);
            auto devC = DeviceUniquePointer<T[]>::Allocate(a.size());
            
            dim3 threads { 512U };
            dim3 blocks { static_cast<unsigned>(std::ceil(double(a.size()) / double(threads.x))) };
            Kernels::VectorAdd<<<blocks, threads>>>(static_cast<T const*>(devA.Get()), static_cast<T const*>(devB.Get()), static_cast<T*>(devC.Get()));
            #ifdef PRT_DEBUG
            Compute::CudaCheckError(cudaDeviceSynchronize());
            #endif

            return devC.Download();
        }
    }

    template<class T, std::size_t NExtent = std::dynamic_extent>
    std::vector<T> VectorAdd(std::span<T const, NExtent> a, std::span<T const, NExtent> b) {
        return Detail::VectorAddImpl(a, b);
    }

    template<class T>
    std::optional<std::vector<T>> VectorAdd(std::span<T const, std::dynamic_extent> a, std::span<T const, std::dynamic_extent> b)
    {
        if (a.size() != b.size()) {
            return {};
        }

        return Detail::VectorAddImpl(a, b);
    }
}
