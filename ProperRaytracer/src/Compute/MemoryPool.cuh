/**
 * File name: MemoryPool.cuh
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <cuda/std/tuple>
#include <cuda/std/numeric>
#include <cuda/atomic>

#include "Macros.hpp"
#include "Utilities/MakeType.hpp"
#include "Utilities/Utilities.hpp"

namespace ProperRT
{
    /// @brief Class for simple memory allocation on the GPU with splitting capabilities.
    class MemoryPool
    {
    public:
        using SizeType = int64;

        /// @brief Enum for the selecting the split strategy in split methods.
        enum class SplitStrategy {
            Stack, GiveToLarger, Even, Proportional
        };

        /// @brief Computes max alignment from input types.
        /// @tparam ...Ts Input types
        template<class... Ts>
        __host__ __device__ static constexpr
        SizeType MaxAlignment() {
            std::size_t align = 0;
            ((align = (alignof(Ts) > align ? alignof(Ts) : align)), ...);
            return static_cast<SizeType>(align);
        }

        /// @brief Max alignment for the whole task pool.
        constexpr static inline
        SizeType MAX_ALIGNMENT = 16;

        /// @brief Class for storing memory allocated with MemoryPool class.
        /// @tparam ...Ts Allocated types
        template<class... Ts>
        struct AllocatedMemory
        {
            /// @brief Pointer to the allocated memory for each type.
            cuda::std::tuple<Ts* ...> data;
            /// @brief Total size of allocated memory, may be much larger than needed to allow for splitting.
            SizeType size;

            /// @brief Splits the pointers in data to an AllocatedMemory class instance per type.
            /// @return 
            __host__ __device__
            cuda::std::tuple<AllocatedMemory<Ts>...> Split();
        };

        /// @brief Specialization of AllocatedMemory for a single type, with the tuple removed.
        /// @tparam T Allocated type
        template<class T>
        struct AllocatedMemory<T>
        {
            /// @brief Pointer to the allocated memory.
            T* data;
            /// @brief Size of the allocated memory.
            SizeType size;
        };

    public:
        /// @brief Pointer to the start of the pool.
        void* poolStart{nullptr};
        /// @brief Pointer to the top of the pool.
        cuda::atomic<uint8*, cuda::thread_scope_device> poolTop{nullptr};

        /// @brief Capacity of the memory pool, poolTop - poolStart must not exceed this.
        SizeType capacity{0};

        #if PRT_COUNT_MEMPOOL_ALLOCATIONS
        cuda::atomic<int32, cuda::thread_scope_device> allocations;
        cuda::atomic<int32, cuda::thread_scope_device> splits;
        cuda::atomic<SizeType, cuda::thread_scope_device> usedSpace;
        #endif

        /// @brief Default ctor.
        MemoryPool() noexcept = default;

        /// @brief Constructs the pool from allocated memory at @p storage with @p capacity_.
        /// @param storage Pointer to allocated storage
        /// @param capacity_ Capacity of the allocated storage
        __host__
        MemoryPool(uint8* storage, SizeType capacity_) noexcept
            : poolStart{ storage }, poolTop{ storage }, capacity{ capacity_ }
            #if PRT_COUNT_MEMPOOL_ALLOCATIONS
            , allocations{ 0 }, splits{ 0 }, usedSpace{ 0 }
            #endif
        {}

        /// @brief Move ctor.
        __host__
        MemoryPool(MemoryPool&& other) noexcept
            : poolStart{ other.poolStart }, poolTop{ other.poolTop.load(cuda::memory_order_relaxed) }, capacity{ other.capacity }
            #if PRT_COUNT_MEMPOOL_ALLOCATIONS
            , allocations{ other.allocations.load(cuda::memory_order_relaxed) }, splits{ other.splits.load(cuda::memory_order_relaxed) },
            usedSpace{ other.usedSpace.load(cuda::memory_order_relaxed) }
            #endif
        {}

        /// @brief Copy ctor.
        __host__
        MemoryPool(MemoryPool const& other) noexcept
            : poolStart{ other.poolStart }, poolTop{ other.poolTop.load(cuda::memory_order_relaxed) }
            #if PRT_COUNT_MEMPOOL_ALLOCATIONS
            , allocations{ other.allocations.load(cuda::memory_order_relaxed) }, splits{ other.splits.load(cuda::memory_order_relaxed) },
            usedSpace{ other.usedSpace.load(cuda::memory_order_relaxed) }
            #endif
        {}

        /// @brief Move operator.
        __host__
        MemoryPool& operator=(MemoryPool&& other) noexcept {
            if (&other != this) {
                poolStart = other.poolStart;
                poolTop.store(other.poolTop.load(cuda::memory_order_relaxed), cuda::memory_order_relaxed);
                capacity = other.capacity;

                #if PRT_COUNT_MEMPOOL_ALLOCATIONS
                allocations.store(other.allocations.load(cuda::memory_order_relaxed), cuda::memory_order_relaxed);
                splits.store(other.splits.load(cuda::memory_order_relaxed), cuda::memory_order_relaxed);
                usedSpace.store(other.usedSpace.load(cuda::memory_order_relaxed));
                #endif
            }

            return *this;
        }

        /// @brief Copy operator.
        __host__
        MemoryPool& operator=(MemoryPool const& other) noexcept {
            if (&other != this) {
                poolStart = other.poolStart;
                poolTop.store(other.poolTop.load(cuda::memory_order_relaxed), cuda::memory_order_relaxed);

                #if PRT_COUNT_MEMPOOL_ALLOCATIONS
                allocations.store(other.allocations.load(cuda::memory_order_relaxed), cuda::memory_order_relaxed);
                splits.store(other.splits.load(cuda::memory_order_relaxed), cuda::memory_order_relaxed);
                usedSpace.store(other.usedSpace.load(cuda::memory_order_relaxed));
                #endif
            }

            return *this;
        }

        /// @brief Resets poolTop to poolStart, essentially "deallocated" allocated memory.
        __host__ __device__
        void Reset() {
            poolTop.store(reinterpret_cast<uint8*>(poolStart), cuda::memory_order_relaxed);
        }

        /// @brief Allocates room for input types and input sizes. The allocated space is equal to the needed space times @p allocationMultiplier.
        /// @tparam Ts Types to allocate memory for
        /// @param allocationMultiplier Multiplier to multiply the needed memory with for the actual allocation size
        /// @param counts For how many instances of the n-th type to allocate memory
        template<class... Ts>
        __host__ __device__
        AllocatedMemory<Ts...> Allocate(SizeType allocationMultiplier, MakeType_t<Ts, SizeType>... counts) {
            constexpr auto typesMaxAlignment = MaxAlignment<Ts...>();
            static_assert(typesMaxAlignment <= MAX_ALIGNMENT);

            AllocatedMemory<Ts...> allocated;

            // Sum up total number of required bytes
            allocated.size = (Align(counts * sizeof(Ts)) + ...);

            #if PRT_COUNT_MEMPOOL_ALLOCATIONS
            allocations.fetch_add(1, cuda::memory_order_relaxed);
            #endif

            AllocateInternal(allocated, allocationMultiplier, counts...);

            return allocated;
        }

        /// @brief If the requested memory size fits into @p previouslyAllocated, split it, otherwise allocate new with @p allocationMultiplier.
        /// @tparam splitStrategy Split strategy to use for splitting
        /// @tparam Ts Types to allocate memory for
        /// @param previouslyAllocated Already allocated memory to split if possible
        /// @param allocationMultiplier Multiplier used when allocation of new memory is needed
        /// @param counts1 Counts of instances for the "left" split
        /// @param counts2 Counts of instances for the "right" split
        template<SplitStrategy splitStrategy, class... Ts>
        __device__
        AllocatedMemory<Ts...> SplitOrAllocateNew(AllocatedMemory<Ts...>& previouslyAllocated, SizeType allocationMultiplier, MakeType_t<Ts, SizeType>... counts1, MakeType_t<Ts, SizeType>... counts2) {
            constexpr auto typesMaxAlignment = MaxAlignment<Ts...>();
            static_assert(typesMaxAlignment <= MAX_ALIGNMENT);

            AllocatedMemory<Ts...> allocated2;
            SizeType size1 = (Align(counts1 * sizeof(Ts)) + ...);
            allocated2.size = (Align(counts2 * sizeof(Ts)) + ...);

            // Can split
            if (size1 + allocated2.size <= previouslyAllocated.size) {
                constexpr bool singleType = (sizeof...(Ts) == 1);
                using TFirst = GetFirstType_t<Ts...>;

                // Get start of old block
                uint8* poolTop_;
                if constexpr (singleType) {
                    poolTop_ = reinterpret_cast<uint8*>(previouslyAllocated.data);
                }
                else {
                    poolTop_ = reinterpret_cast<uint8*>(cuda::std::get<0>(previouslyAllocated.data));
                }

                #ifndef NDEBUG
                uint8* poolTopDebug = poolTop_;
                SizeType oldSizeDebug = previouslyAllocated.size;
                #endif

                #if PRT_COUNT_MEMPOOL_ALLOCATIONS
                splits.fetch_add(1, cuda::memory_order_relaxed);
                #endif

                // Initialize left split
                if constexpr (singleType) {
                    previouslyAllocated.data = reinterpret_cast<TFirst*>(poolTop_);
                    poolTop_ += Align(GetFirstValue(counts1...) * sizeof(TFirst));
                }
                else {
                    cuda::std::apply(GetInitAllocatedDataPtrsLambda<Ts...>(poolTop_, counts1...), previouslyAllocated.data);
                    //((cuda::std::get<Ts*>(previouslyAllocated.data) = reinterpret_cast<Ts*>(poolTop_), poolTop_ += Align(counts1 * sizeof(Ts))), ...);
                }

                if constexpr (splitStrategy == SplitStrategy::Stack) {
                    // Initialize right split
                    if constexpr (singleType) {
                        allocated2.data = reinterpret_cast<TFirst*>(poolTop_);
                        poolTop_ += Align(GetFirstValue(counts2...) * sizeof(TFirst));
                    }
                    else {
                        cuda::std::apply(GetInitAllocatedDataPtrsLambda<Ts...>(poolTop_, counts2...), allocated2.data);
                        //((cuda::std::get<Ts*>(allocated2.data) = reinterpret_cast<Ts*>(poolTop_), poolTop_ += Align(counts2 * sizeof(Ts))), ...);
                    }

                    // Set right block size to requested size plus remaining size of old block
                    allocated2.size = previouslyAllocated.size - size1;
                    // Set left block size to requested size
                    previouslyAllocated.size = size1;
                }
                else if constexpr (splitStrategy == SplitStrategy::GiveToLarger) {
                    // Give all to left
                    if (size1 > allocated2.size) {
                        // Shift top by size of empty space
                        poolTop_ += previouslyAllocated.size - (size1 + allocated2.size);

                        // Initialize right split
                        if constexpr (singleType) {
                            allocated2.data = reinterpret_cast<TFirst*>(poolTop_);
                            poolTop_ += Align(GetFirstValue(counts2...) * sizeof(TFirst));
                        }
                        else {
                            cuda::std::apply(GetInitAllocatedDataPtrsLambda<Ts...>(poolTop_, counts2...), allocated2.data);
                            //((cuda::std::get<Ts*>(allocated2.data) = reinterpret_cast<Ts*>(poolTop_), poolTop_ += Align(counts2 * sizeof(Ts))), ...);
                        }

                        // Set left block size to requested size plus remaining size of old block
                        previouslyAllocated.size = previouslyAllocated.size - allocated2.size;
                        // Right block size stays the same
                        //allocated2.size = allocated2.size;
                    }
                    // Give all to right
                    else {
                        // Initialize right split
                        if constexpr (singleType) {
                            allocated2.data = reinterpret_cast<TFirst*>(poolTop_);
                            poolTop_ += Align(GetFirstValue(counts2...) * sizeof(TFirst));
                        }
                        else {
                            cuda::std::apply(GetInitAllocatedDataPtrsLambda<Ts...>(poolTop_, counts2...), allocated2.data);
                            //((cuda::std::get<Ts*>(allocated2.data) = reinterpret_cast<Ts*>(poolTop_), poolTop_ += Align(counts2 * sizeof(Ts))), ...);
                        }

                        // Set right block size to requested size plus remaining size of old block
                        allocated2.size = previouslyAllocated.size - size1;
                        // Set left block size to requested size
                        previouslyAllocated.size = size1;
                    }
                }
                else if constexpr (splitStrategy == SplitStrategy::Even) {
                    SizeType alignedHalf = Align((previouslyAllocated.size - (size1 + allocated2.size)) / 2);
                    // Shift top by aligned half of empty space
                    poolTop_ += alignedHalf;
                    // Initialize right split
                    if constexpr (singleType) {
                        allocated2.data = reinterpret_cast<TFirst*>(poolTop_);
                        poolTop_ += Align(GetFirstValue(counts2...) * sizeof(TFirst));
                    }
                    else {
                        cuda::std::apply(GetInitAllocatedDataPtrsLambda<Ts...>(poolTop_, counts2...), allocated2.data);
                        //((cuda::std::get<Ts*>(allocated2.data) = reinterpret_cast<Ts*>(poolTop_), poolTop_ += Align(counts2 * sizeof(Ts))), ...);
                    }

                    // Set left and right sizes
                    allocated2.size = previouslyAllocated.size - (size1 + alignedHalf);
                    previouslyAllocated.size = size1 + alignedHalf;
                }
                else if constexpr (splitStrategy == SplitStrategy::Proportional) {
                    SizeType empty = previouslyAllocated.size - (size1 + allocated2.size);
                    SizeType alignedLeft = Align(static_cast<SizeType>(static_cast<float>(empty) / static_cast<float>(size1 + allocated2.size) * static_cast<float>(size1)));
                    // Shift top by aligned half of empty space
                    poolTop_ += alignedLeft;
                    // Initialize right split
                    if constexpr (singleType) {
                        allocated2.data = reinterpret_cast<TFirst*>(poolTop_);
                        poolTop_ += Align(GetFirstValue(counts2...) * sizeof(TFirst));
                    }
                    else {
                        cuda::std::apply(GetInitAllocatedDataPtrsLambda<Ts...>(poolTop_, counts2...), allocated2.data);
                        //((cuda::std::get<Ts*>(allocated2.data) = reinterpret_cast<Ts*>(poolTop_), poolTop_ += Align(counts2 * sizeof(Ts))), ...);
                    }

                    // Set left and right sizes
                    allocated2.size = previouslyAllocated.size - (size1 + alignedLeft);
                    previouslyAllocated.size = size1 + alignedLeft;
                }
                else {
                    static_assert(false, "Incorrect enum value");
                }

                #ifndef NDEBUG
                assert(poolTopDebug + oldSizeDebug >= poolTop_);
                #endif
            }
            // Cannot split, allocated new
            else {
                #if PRT_COUNT_MEMPOOL_ALLOCATIONS
                allocations.fetch_add(1, cuda::memory_order_relaxed);
                #endif
                AllocateInternal(allocated2, allocationMultiplier, counts2...);
            }

            return allocated2;
        }

        /// @brief Returns the allocated size.
        __host__ __device__
        cuda::std::ptrdiff_t AllocatedSize() const {
            return poolTop.load(cuda::memory_order_relaxed) - reinterpret_cast<uint8*>(poolStart);
        }

    private:

        static __host__ __device__
        SizeType Align(SizeType size) {
            return ((size - 1 + MAX_ALIGNMENT) & -MAX_ALIGNMENT);
        }

        template<class... Ts>
        __host__ __device__
        decltype(auto) GetInitAllocatedDataPtrsLambda(uint8*& poolTop_, MakeType_t<Ts, SizeType>... counts) {
            return [&poolTop_ = poolTop_, counts...] (Ts*& ... data) {
                ((data = reinterpret_cast<Ts*>(poolTop_), poolTop_ += Align(counts * sizeof(Ts))), ...);
            };
        }

        template<class... Ts>
        __host__ __device__
        void AllocateInternal(AllocatedMemory<Ts...>& allocated, SizeType allocationMultiplier, MakeType_t<Ts, SizeType>... counts) {
            // Increase pool's allocated size
            allocated.size *= allocationMultiplier;
            uint8* poolTop_ = poolTop.fetch_add(allocated.size, cuda::memory_order_relaxed);

            #ifndef NDEBUG
            uint8* poolTopDebug = poolTop_;
            #endif

            // Initialize allocated memory pointers
            if constexpr (sizeof...(Ts) == 1) {
                using TFirst = GetFirstType_t<Ts...>;
                allocated.data = reinterpret_cast<TFirst*>(poolTop_);
                poolTop_ += Align(GetFirstValue(counts...) * sizeof(TFirst));
            }
            else {
                cuda::std::apply(GetInitAllocatedDataPtrsLambda<Ts...>(poolTop_, counts...), allocated.data);
            }

            #ifndef NDEBUG
            assert(poolTopDebug + allocated.size / allocationMultiplier == poolTop_);
            #endif

            CheckCapacity(poolTop_);
        }

        __host__ __device__
        void CheckCapacity(uint8* newPoolTop) {
            if ((reinterpret_cast<uint8*>(poolStart) + capacity) - (newPoolTop) < 0) {
                #ifdef __CUDA_ARCH__
                printf("Error: Not enough memory in memory pool\n");
                __trap();
                #else
                throw std::runtime_error("Not enough memory in memory pool");
                #endif
            }
        }
    };
    
    template <class... Ts>
    inline __host__ __device__
    cuda::std::tuple<MemoryPool::AllocatedMemory<Ts>...> MemoryPool::AllocatedMemory<Ts...>::Split() {
        cuda::std::tuple<AllocatedMemory<Ts>...> split;

        constexpr std::size_t typeN = sizeof...(Ts);
        UnrollTemplated<typeN>([&split, this] <std::size_t I> () {
            if constexpr (I == typeN - 1) {
                cuda::std::get<I>(split) = AllocatedMemory<cuda::std::tuple_element_t<I, cuda::std::tuple<Ts...>>>{
                    cuda::std::get<I>(data),
                    size - static_cast<MemoryPool::SizeType>(reinterpret_cast<uint8*>(cuda::std::get<I>(data)) - reinterpret_cast<uint8*>(cuda::std::get<0>(data)))
                };
            }
            else {
                cuda::std::get<I>(split) = AllocatedMemory<cuda::std::tuple_element_t<I, cuda::std::tuple<Ts...>>>{
                    cuda::std::get<I>(data),
                    static_cast<MemoryPool::SizeType>(reinterpret_cast<uint8*>(cuda::std::get<I+1>(data)) - reinterpret_cast<uint8*>(cuda::std::get<I>(data)))
                };
            }
        });

        return split;
    }

    namespace Kernels
    {
        /// @brief Allocates memory in @p pool on the device according to @p counts and @p allocationMultiplier, and assigns it to @p memory.
        /// @tparam Ts Types to allocate memory for
        /// @param pool Pointer to the memory pool to allocate memory in
        /// @param memory Pointer to the output
        /// @param allocationMultplier Allocation multiplier for allocation
        /// @param counts Counts for allocation
        template<class... Ts>
        __global__
        void MemoryPoolAllocate(DevicePointer<MemoryPool> pool, DevicePointer<MemoryPool::AllocatedMemory<Ts...>> memory, MemoryPool::SizeType allocationMultiplier, MakeType_t<Ts, MemoryPool::SizeType>... counts) {
            *memory = pool->Allocate<Ts...>(allocationMultiplier, counts...);
        }
    }

    /// @brief Memory pool wrapper with RAII.
    class UniqueMemoryPool
    {
    public:
        /// @brief Default ctor.
        UniqueMemoryPool() noexcept = default;

        /// @brief Constructs a memory pool with @p capacity, including allocating the needed device memory.
        explicit
        UniqueMemoryPool(MemoryPool::SizeType capacity)
            : storage{ DeviceUniquePointer<uint8[]>::Allocate(capacity) }, poolCPU{ storage.Get().data, capacity }, poolGPU{ DeviceUniquePointer<MemoryPool>::Allocate() }
        {}

        /// @brief Returns the memory pool instance on the CPU.
        MemoryPool& GetCPU() { return poolCPU; }

        /// @brief Returns the pointer to the memory pool instance on the GPU.
        DevicePointer<MemoryPool> GetGPU() const { return poolGPU.Get(); }

        /// @brief Copies the current value of the memory pool instance from the CPU to the GPU.
        void SyncCPU2GPU() {
            poolGPU.Upload(poolCPU);
        }

        /// @brief Copies the current value of the memory pool instance from the GPU to the CPU.
        void SyncGPU2CPU() {
            poolGPU.Download(&poolCPU);
        }

        /// @brief Allocates memory in the memory pool on the device.
        /// @details Useful when allocating memory while the device is using the pool.
        /// @tparam Ts Types to allocate memory for
        /// @param allocationMultiplier Allocation multiplier for allocation
        /// @param counts Counts for allocation
        template<class... Ts>
        MemoryPool::AllocatedMemory<Ts...> AllocateOnDevice(MemoryPool::SizeType allocationMultiplier, MakeType_t<Ts, MemoryPool::SizeType>... counts) {
            auto allocated = DeviceUniquePointer<MemoryPool::AllocatedMemory<Ts...>>::Allocate();
            Kernels::MemoryPoolAllocate<Ts...><<<1,1>>>(GetGPU(), allocated.Get(), allocationMultiplier, counts...);
            return allocated.Download();
        }

    private:
        DeviceUniquePointer<uint8[]> storage{};
        MemoryPool poolCPU{};
        DeviceUniquePointer<MemoryPool> poolGPU{};
    };
}
