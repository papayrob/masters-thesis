/**
 * File name: VectorAdd.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <span>
#include <vector>
#include <optional>

namespace ProperRT
{
    /// @brief Adds 2 vectors on the GPU and returns the result.
    /// @return Result of @p a + @p b, empty when the inputs are of different sizes
    std::optional<std::vector<int>> AddVectors(std::span<int const> a, std::span<int const> b);
}
