/**
 * File name: DeviceAllocator.cuh
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <vector>

#include "DevicePointer.cuh"

namespace ProperRT
{
    /// @brief Class for allocating and releasing device memory.
    /// @details Memory gets released when the instance gets destroyed,
    /// so the instance should always outlive the use of allocated memory.
    class DeviceAllocator
    {
    public:
        /// @brief Dtor, frees all allocated memory.
        ~DeviceAllocator() {
            Reset();
        }

        __host__
        void Reset() {
            for (void* p : devicePointers) {
                Compute::CudaCheckError(cudaFree(p));
            }
            devicePointers.clear();
        }

        /// @brief Allocate memory for type T on the device.
        /// @return Pointer class to allocated device memory
        template<class T>
        __host__ [[nodiscard]]
        DevicePointer<T> Allocate() {
            DevicePointer<T> devPtr = DeviceUniquePointer<T>::Allocate().Release();
            devicePointers.push_back(devPtr);
            return devPtr;
        }

        /// @brief Allocate memory for an array of type T on the device.
        /// @param size Number of elements in the array
        /// @return Pointer class to allocated device memory
        template<class TArray>
            requires std::is_unbounded_array_v<TArray>
        __host__ [[nodiscard]]
        DevicePointer<TArray> Allocate(typename DevicePointer<TArray>::SizeType size) {
            DevicePointer<TArray> devPtr = DeviceUniquePointer<TArray>::Allocate(size).Release();
            devicePointers.push_back(devPtr);
            return devPtr;
        }

        /// @brief Allocate memory for type T on the device and initialize it with @p init.
        /// @param init Value to initialize with (copied to device immediately after allocating)
        /// @return Pointer class to allocated device memory
        template<class T>
        __host__ [[nodiscard]]
        DevicePointer<T> AllocateAndUpload(T const& init) {
            DevicePointer<T> devPtr = DeviceUniquePointer<T>::AllocateAndUpload(init).Release();
            devicePointers.push_back(devPtr);
            return devPtr;
        }

        /// @brief Allocate memory for an array of type T on the device and initialize it with @p init.
        /// @param init Array to initialize with (copied to device immediately after allocating)
        /// @return Pointer class to allocated device memory
        template<class TArray>
            requires std::is_unbounded_array_v<TArray>
        __host__ [[nodiscard]]
        DevicePointer<TArray> AllocateAndUpload(std::span<std::remove_extent_t<TArray> const> init) {
            DevicePointer<TArray> devPtr = DeviceUniquePointer<TArray>::AllocateAndUpload(init).Release();
            devicePointers.push_back(devPtr);
            return devPtr;
        }

    private:
        std::vector<void*> devicePointers{};
    };
}