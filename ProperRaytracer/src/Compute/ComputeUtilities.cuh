/**
 * File name: ComputeUtilities.hpp
 * Description: File containing utility functions for device related code.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <cuda_runtime_api.h>

#include "Utilities/Utilities.hpp"
#include "DevicePointer.cuh"

namespace ProperRT
{
    namespace Kernels
    {
        
        /// @brief A simple copy kernel that assumes a thread per item in a 1D grid. Copies @p src to @p dst.
        /// @tparam T Data pointer type (or any other type that supports indexing)
        /// @param src Source data
        /// @param dst Destination data
        /// @param size Total count of data
        template<class T>
        __global__
        void SimpleCopy(T src, T dst, int64 size) {
            auto gid = Compute::Grid1DHelpers::GlobalThreadIdx();
            if (gid < size) {
                dst[gid] = src[gid];
            }
        }

        /// @brief A simple kernel that assumes a thread per item in a 1D grid. Initializes @p data with @p init.
        /// @tparam T Data pointer type (or any other type that supports indexing)
        /// @tparam S Data type to init with
        /// @param data Data to initialize
        /// @param init Init value
        /// @param size Total count of data
        template<class T, class S>
        __global__
        void SimpleValueInit(T data, S init, int64 size) {
            auto gid = Compute::Grid1DHelpers::GlobalThreadIdx();
            if (gid < size) {
                data[gid] = init;
            }
        }
    }

    /// @brief Copies value from @p src to @p dst on the GPU.
    /// @tparam T Data type
    /// @param src Source data
    /// @param dst Destination data
    template<class T>
    inline
    void KernelCopy(DevicePointer<T> src, DevicePointer<T> dst) {
        Kernels::SimpleCopy<<<1,1>>>(src, dst, 1);
        #ifdef PRT_DEBUG
        Compute::CudaCheckError(cudaDeviceSynchronize());
        #endif
    }

    /// @brief Copies values from @p src to @p dst on the GPU.
    /// @tparam T Data type
    /// @param src Source data
    /// @param dst Destination data
    template<class T>
    inline
    void KernelCopy<T[]>(DevicePointer<T[]> src, DevicePointer<T[]> dst) {
        auto size = std::min(src.count, dst.count);
        int blockDim = 256;
        int gridDim = static_cast<int>(IntDivRoundUp(static_cast<uint64>(size), static_cast<uint64>(blockDim)));
        Kernels::SimpleCopy<<<gridDim, blockDim>>>(src, dst, size);
        #ifdef PRT_DEBUG
        Compute::CudaCheckError(cudaDeviceSynchronize());
        #endif
    }

    /// @brief Initializes @p data with @p init on the GPU.
    /// @tparam T Data pointer type
    /// @tparam S Data type
    /// @param data Pointer to data to be initalized
    /// @param init Init value
    template<class T, class S>
    inline
    void KernelInitValues(T data, S init, int64 size) {
        int blockDim = 256;
        int gridDim = static_cast<int>(IntDivRoundUp(static_cast<uint64>(size), static_cast<uint64>(blockDim)));
        Kernels::SimpleValueInit<<<gridDim, blockDim>>>(data, init, size);
        #ifdef PRT_DEBUG
        Compute::CudaCheckError(cudaDeviceSynchronize());
        #endif
    }

namespace Compute
{
    /// @brief Retrieves the largest copy type from struct alignment.
    /// @details Retrieves an integral type that can be used to copy a struct of type T
    /// by reintepreting it as an array of the retrieved type. Used for custom memcpy
    /// operations. Generally unsafe, because it bypasses copy constructors etc.
    /// @tparam T 
    template<class T>
	using CopyTypeFromAlignment =
        std::conditional_t<
            alignof(T) == 1,
            uint8,
            std::conditional_t<
                alignof(T) == 2,
                uint16,
                std::conditional_t<
                    alignof(T) == 4,
                    uint32,
                    std::conditional_t<
                        alignof(T) == 8,
                        uint64,
                        std::conditional_t<
                            alignof(T) == 16,
                            uint4,
                            uint4
                        >
                    >
                >
            >
        >;

    /// @brief Performs an inclusive subgroup wide scan.
    /// @tparam T Value type
    /// @tparam F Functor object type
    /// @param laneValue Lane specific value
    /// @param laneIdx Index of the lane
    /// @param op Functor object that determines the operation performed on values
    /// @return Scanned value belonging to @p laneIdx
    template<class T, class F>
    __device__ inline
    T SubgroupScanLocal(T laneValue, int laneIdx, F&& op) {
        for (int delta = 1; delta < Compute::SUBGROUP_SIZE; delta *= 2) {
            T add = __shfl_up_sync(Compute::FULL_MASK, laneValue, delta);
            if (delta <= laneIdx) {
                laneValue = op(laneValue, add);
            }
        }

        return laneValue;
    }

    /// @brief Unimplemented
    template<class TArray, class T, class F>
    __device__ inline
    T SubgroupScanShared(TArray* data, T identity, int64 dataN, int32 laneIdx, F&& op) {
        // // Up sweep
        // for (int64_t delta = 1; delta < ; delta *= 2) {
        //     for (int64_t dataI = delta - (delta % Compute::SUBGROUP_SIZE); dataI < dataN; dataI += Compute::SUBGROUP_SIZE) {
        //         T add;
        //         if (0 < dataI - delta + laneIdx) {
        //             add = data[dataI - delta + laneIdx];
        //         }
        //         __syncwarp();
        //         if (0 < dataI - delta + laneIdx && dataI + laneIdx < dataN) {
        //             data[dataI + laneIdx] = op(add, data[dataI + laneIdx]);
        //         }
        //         __syncwarp();
        //     }
        // }

        return data[dataN - 1];
    }

    /// @brief Performs an inclusive scan on an array @p data of size @p dataN.
    /// @tparam T Value type
    /// @tparam F Functor object type
    /// @param data Pointer to the data array
    /// @param init Initial value
    /// @param dataN Number of data
    /// @param laneIdx Index of the lane
    /// @param op Functor object that determines the operation performed on values
    /// @return Final scanned value
    template<class T, class F>
    __device__ inline
    T SubgroupScanGlobal(T* data, T init, int64 dataN, int laneIdx, F&& op) {
        int64 dataI{};

        while (dataI + Compute::SUBGROUP_SIZE <= dataN) {
            // Perform a subgroup wide scan, then add the accumulated total sum and save the new one
            T value = SubgroupScanLocal(data[dataI + laneIdx], laneIdx, Compute::FULL_MASK);
            value = op(value, init);
            data[dataI + laneIdx] = value;
            init = __shfl_sync(Compute::FULL_MASK, value, Compute::SUBGROUP_SIZE - 1);
            dataI += Compute::SUBGROUP_SIZE;
        }

        // Perform a partial subgroup scan for the remaining data
        if (dataI != dataN) {
            uint32 mask = __ballot_sync(Compute::FULL_MASK, dataI + laneIdx < dataN);
            T value;
            if (dataI + laneIdx < dataN) {
                value = SubgroupScanLocal(data[dataI + laneIdx], laneIdx, mask);
                value = op(value, init);
                data[dataI + laneIdx] = value;
            }

            init = __shfl_sync(Compute::FULL_MASK, value, dataN - dataI - 1);
        }

        return init;
    }

    /// @brief Performs a subgroup wide reduction.
    /// @tparam T Value type
    /// @tparam F Functor object type
    /// @param laneValue Lane specific value
    /// @param laneIdx Index of the lane
    /// @param op Functor object that determines the operation performed on values
    /// @return Final reduced value
    template<class T, class F>
    __device__ inline
    T SubgroupReduceLocal(T laneValue, int laneIdx, F&& op) {
        for (int delta = Compute::SUBGROUP_SIZE / 2; delta > 0; delta /= 2) {
            laneValue = op(laneValue, __shfl_down_sync(Compute::FULL_MASK, laneValue, delta));
        }

        return __shfl_sync(Compute::FULL_MASK, laneValue, 0);
    }

    /// @brief Performs a reduction on an array @p data of size @p dataN.
    /// @tparam T Value type
    /// @tparam F Functor object type
    /// @param data Pointer to the data array
    /// @param identity Value for which op(x, identity) = x holds
    /// @param dataN Number of data
    /// @param laneIdx Index of the lane
    /// @param op Functor object that determines the operation performed on values
    /// @return Final reduced value
    template<class T, class F>
    __device__ inline
    T SubgroupReduceArray(T const* data, T identity, int64 dataN, int32 laneIdx, F&& op) {
        int64 dataI{ laneIdx };
        T value{ (dataI < dataN) ? data[dataI] : identity };
        dataI += Compute::SUBGROUP_SIZE;

        for (; dataI < dataN; dataI += Compute::SUBGROUP_SIZE) {
            // Reduce each lane value with each subgroup-sized block
            value = op(value, data[dataI]);
        }

        return SubgroupReduceLocal(value, laneIdx, op);
    }

    /// @brief Copies data between two structs using the whole subgroup as if using memcpy.
    /// @tparam TSource Source struct type
    /// @tparam TTarget Target struct type, must have same size and alignment as @p TSource
    /// @param source Pointer to the source struct data
    /// @param target Pointer to the target struct data
    template<class TSource, class TTarget>
    __device__
    void SubgroupMemCopyStruct(TSource* source, TTarget* target) {
        static_assert(sizeof(TSource) == sizeof(TTarget) && alignof(TSource) == alignof(TTarget));
        assert(sizeof(TSource) == sizeof(TTarget) && alignof(TSource) == alignof(TTarget));

        using CopyType = CopyTypeFromAlignment<TSource>;

        auto laneIdx = Compute::Grid1DHelpers::LaneIdx();
        constexpr auto copySize = Compute::SUBGROUP_SIZE * sizeof(CopyType);

        // Can copy without looping
        if constexpr (sizeof(TSource) <= copySize) {
            if (laneIdx * sizeof(CopyType) < sizeof(TSource)) {
                *(reinterpret_cast<CopyType*>(target) + laneIdx) = *(reinterpret_cast<CopyType*>(source) + laneIdx);
            }
        }
        // Must loop
        else {
            for (; laneIdx * sizeof(CopyType) < sizeof(TSource); laneIdx += Compute::SUBGROUP_SIZE) {
                *(reinterpret_cast<CopyType*>(target) + laneIdx) = *(reinterpret_cast<CopyType*>(source) + laneIdx);
            }
        }
    }

}

}
