/**
 * File name: ParallelScan.hpp
 * Description: See comments for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#pragma once

#include <vector>
#include <span>

namespace ProperRT
{
    /// @brief Computes a prefix scan on @p input and returns it.
    std::vector<int> PrefixScan(std::span<int const> input);
}
