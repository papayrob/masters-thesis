/**
 * File name: VectorAdd.cu
 * Description: See header file for description.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2024 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */
#include "VectorAdd.hpp"
#include "VectorAdd.cuh"

namespace ProperRT
{
    std::optional<std::vector<int>> AddVectors(std::span<int const> a, std::span<int const> b)
    {
        return VectorAdd(a, b);
    }
}
