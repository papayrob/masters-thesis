﻿/**
 * File name: ProperRaytracer.cpp
 * Description: This file is the entry point to the application.
 *				After input parameteres are processed, an app instance
 *              is created and the main loop is entered.
 *
 * This file is part of ProperRaytracer.
 *
 * Written by Robert Papay.
 *
 * Copyright (c) 2022 Robert Papay.
 * This code is licensed under MIT license (see LICENSE for details).
 */

#include <bit>
#include <cxxopts.hpp>

#include "Utilities/Utilities.hpp"
#include "Utilities/Logger.hpp"
#include "RTApp.hpp"
#include "Macros.hpp"

#include "Compute/ParallelScan.hpp"

// Code currently built only for CUDA
//static_assert(std::has_single_bit(static_cast<uint32_t>(ProperRT::Compute::SUBGROUP_SIZE)));
static_assert(ProperRT::Compute::SUBGROUP_SIZE == 32);
static_assert(std::popcount(ProperRT::Compute::FULL_MASK) == ProperRT::Compute::SUBGROUP_SIZE);

static_assert(std::has_single_bit(static_cast<uint32_t>(PRT_KDTREE_BINS)));
// Min subgroup size for easy calculation
static_assert(ProperRT::Compute::SUBGROUP_SIZE <= PRT_KDTREE_BINS && PRT_KDTREE_BINS <= 256);

namespace
{
void PrintHelpAndExit(cxxopts::Options const& options) {
	std::cout << options.help() << std::endl;
	exit(0);
}
}

int main(int argc, char* argv[])
{
	using namespace ProperRT;

	// Init
	cxxopts::Options options(RTApp::APP_NAME, "Description");

	options.add_options()
		("scene", "Starting scene", cxxopts::value<std::string>())
		("config", "App config", cxxopts::value<std::string>())
		("m,mode", "Rendering mode (ogl/rt)", cxxopts::value<std::string>()->default_value("ogl"))
		("no-gui", "If specified, the application will be launched without GUI, usefull for benchmarking. Available only with rt mode", cxxopts::value<bool>()->default_value("false"))
		("t,test", "Enable performance testing", cxxopts::value<bool>()->default_value("false"))
		//("stats", "Name of the statistics file to which stats will be saved", cxxopts::value<std::string>())
		("s,stop-frame", "If specified, the application will close after rendering the specified number of frames (negative number = inf)", cxxopts::value<int>()->default_value("-1"))
		("save-frame", "If specified, the rendered image will be saved to Assets each n-th frame", cxxopts::value<int>()->default_value("-1"))
		("l,log", "Specifies the name of the log file, empty/default means no logging, std means std output", cxxopts::value<std::string>()->default_value("")->implicit_value(""))
		("e,log-errors", "Specifies the name of the log file for warnings and errors, empty/default means same value as --log", cxxopts::value<std::string>()->default_value("")->implicit_value(""))
		("h,help", "Print usage")
	;

	options.parse_positional({ "scene", "config" });

	auto parsedOpts = options.parse(argc, argv); //TODO add try catch -> print help on catch

	if (parsedOpts.count("help") > 0 || parsedOpts.count("scene") == 0 || parsedOpts.count("config") == 0) {
		PrintHelpAndExit(options);
	}

	Scene::startingSceneName = parsedOpts["scene"].as<std::string>();

	Logger::logPerformance = parsedOpts["test"].as<bool>();

	auto log = parsedOpts["log"].as<std::string>();
	if (log == "") {
		Logger::SetAllOutputs(nullptr);
	}
	else if (log == "std") {
		Logger::SetAllOutputs(&std::cout);
		Logger::warningOutput = Logger::errorOutput = &std::cerr;
	}
	else {
		Logger::OutputAllToFile(log);
	}

	log = parsedOpts["log-errors"].as<std::string>();
	if (log == "") {
		// same as log, do nothing
	}
	else if (log == "std") {
		Logger::warningOutput = Logger::errorOutput = &std::cerr;
	}
	else {
		Logger::OutputErrorToFile(log);
		Logger::warningOutput = Logger::errorOutput;
	}

	{
		AssetID configID{ parsedOpts["config"].as<std::string>() };

		RTApp::RenderingMode rmode = RTApp::RenderingMode::OpenGL;
		if (parsedOpts["mode"].as<std::string>() == "rt") {
			rmode = RTApp::RenderingMode::Raytracing;
		}

		bool nogui = parsedOpts["no-gui"].as<bool>();

		int stopFrame = parsedOpts["stop-frame"].as<int>();
		int saveFrame = parsedOpts["save-frame"].as<int>();

		if (nogui && rmode != RTApp::RenderingMode::Raytracing) {
			PrintHelpAndExit(options);
		}

		RTApp::InitializeStatic(rmode, !nogui, stopFrame, saveFrame, std::move(configID));
	}

	auto app = RTApp::Instance();
	int mainRet = 0;

	#ifdef PRT_DEBUG
	// Don't catch exceptions so that the debugger catches them and they can be debugged
	app->EnterMainLoop();
	#else
	try {
		app->EnterMainLoop();
	}
	catch(std::exception const& e)
	{
		Logger::LogError(std::string{ "Uncaught exception " } + e.what() + " raised, exitting program");
		mainRet = 1;
	}
	#endif

	RTApp::DestroyInstance();

	return mainRet;
}
