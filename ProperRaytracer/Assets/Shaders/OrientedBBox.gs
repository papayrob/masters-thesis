#version 460 core

layout (lines_adjacency) in;
layout (triangle_strip, max_vertices = 24) out;

in GeometryInfo {
    vec4 posBot;
    vec4 posTop;
} gs_in[];

void EmitFace(vec4 p1, vec4 p2, vec4 p3, vec4 p4) {
    gl_Position = p1;
    EmitVertex();
    gl_Position = p2;
    EmitVertex();
    gl_Position = p3;
    EmitVertex();
    gl_Position = p4;
    EmitVertex();

    EndPrimitive();
}

void main() {
    // Top and bottom
    EmitFace(gs_in[0].posTop, gs_in[1].posTop, gs_in[3].posTop, gs_in[2].posTop);
    EmitFace(gs_in[0].posBot, gs_in[3].posBot, gs_in[1].posBot, gs_in[2].posBot);

    // Front and back
    EmitFace(gs_in[1].posBot, gs_in[2].posBot, gs_in[1].posTop, gs_in[2].posTop);
    EmitFace(gs_in[0].posBot, gs_in[0].posTop, gs_in[3].posBot, gs_in[3].posTop);

    // Left and right
    EmitFace(gs_in[1].posBot, gs_in[1].posTop, gs_in[0].posBot, gs_in[0].posTop);
    EmitFace(gs_in[2].posBot, gs_in[3].posBot, gs_in[2].posTop, gs_in[3].posTop);
}
