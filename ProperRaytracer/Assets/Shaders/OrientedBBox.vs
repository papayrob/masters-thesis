#version 460 core

layout(location = 0)
uniform mat4 _PVMmatrix;

layout(location = 0)
in vec3 posBot;
layout(location = 1)
in vec3 posTop;

out GeometryInfo {
    vec4 posBot;
    vec4 posTop;
} vs_out;

void main() {
    vs_out.posBot = _PVMmatrix * vec4(posBot, 1.0);
    vs_out.posTop = _PVMmatrix * vec4(posTop, 1.0);
}
