#version 460 core

struct Material
{
    vec4 diffuseColor;
    //vec4 specularColor;
};

layout(location = 0)
uniform mat4 _PVMmatrix;
layout(location = 1)
uniform mat4 _Vmatrix;

layout(location = 3)
uniform vec4 _AmbientColor;
layout(location = 4)
uniform Material _Material;

struct Light {
    int type;
    float position[3];
    float direction[3];
    float range;
    float angle;
    float color[4];
};

layout(std430, binding = 0) readonly buffer Lights
{
    Light lights[];
};

in VertexInfo {
    vec3 v_position;
    vec3 normal;
} fs_in;

out vec4 outputColor;


vec3 ComputeLitSurfaceColor(vec3 lightDir, vec3 normal, vec3 viewerDir, vec4 light) {
    vec3 reflection = reflect(-lightDir, normal);

    return
        // diffuse
        max(0.0, dot(lightDir, normal)) * light.a * _Material.diffuseColor.a * _Material.diffuseColor.rgb * light.rgb;
        // +
        // // specular
        // pow(max(0.0, dot(reflection, viewerDir)), Material.specularColor.a + 0.0001) * Material.specularColor.rgb;
}

vec3 ApplyPoint(vec3 color, int i) {
    vec3 surfaceColor = vec3(0.0);
    vec3 lightPos = (_Vmatrix * vec4(lights[i].position[0], lights[i].position[1], lights[i].position[2], 1.0)).xyz;
    float dist = length(lightPos - fs_in.v_position);

    // Only light the object if it's in range
    if (dist < lights[i].range) {
        surfaceColor = ComputeLitSurfaceColor(
            normalize(lightPos - fs_in.v_position),
            fs_in.normal,
            normalize(-fs_in.v_position),
            vec4(lights[i].color[0], lights[i].color[1], lights[i].color[2], lights[i].color[3])
        );
    }

    return (color + surfaceColor);
}

vec3 ApplyDirectional(vec3 color, int i) {
    vec3 lightDir = normalize((_Vmatrix * vec4(lights[i].direction[0], lights[i].direction[1], lights[i].direction[2], 0.0)).xyz);

    vec3 surfaceColor = ComputeLitSurfaceColor(
        -lightDir,
        fs_in.normal,
        normalize(-fs_in.v_position),
        vec4(lights[i].color[0], lights[i].color[1], lights[i].color[2], lights[i].color[3])
    );

    return (color + surfaceColor);
}

void main() {
    vec3 color = vec3(0.0);
    for (int i = 0; i < lights.length(); ++i) {
        switch (lights[i].type) {
        case 0:
            color = ApplyDirectional(color, i);
            break;
        case 1:
            color = ApplyPoint(color, i);
            break;
        }
    }

    color += _AmbientColor.a * _AmbientColor.rgb * _Material.diffuseColor.rgb;
    outputColor = vec4(color, _Material.diffuseColor.a);
}
