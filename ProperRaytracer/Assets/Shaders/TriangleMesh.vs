#version 460 core

layout(location = 0)
uniform mat4 _PVMmatrix;
layout(location = 2)
uniform mat4 _VMmatrix;

layout(location = 0)
in vec3 position;

out VertexInfo {
    smooth vec3 v_position;
} vs_out;

void main() {
    vs_out.v_position = (_VMmatrix * vec4(position, 1.0)).xyz;
    gl_Position = _PVMmatrix * vec4(position, 1.0);
}
