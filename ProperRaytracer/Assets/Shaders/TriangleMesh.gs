#version 460 core

layout (triangles) in;
layout (triangle_strip, max_vertices=3) out;

in VertexInfo {
    vec3 v_position;
} gs_in[];

out VertexInfo {
    smooth vec3 v_position;
    flat vec3 normal;
} gs_out;

vec3 GetNormal() {
    vec3 a = gs_in[1].v_position - gs_in[0].v_position;
    vec3 b = gs_in[2].v_position - gs_in[0].v_position;
    return normalize(cross(a, b));
}

void main() {
    gs_out.normal = GetNormal();
    for (int i = 0; i < 3; ++i) {
        gl_Position = gl_in[i].gl_Position;
        gs_out.v_position = gs_in[i].v_position;
        EmitVertex();
    }
    EndPrimitive();
}
