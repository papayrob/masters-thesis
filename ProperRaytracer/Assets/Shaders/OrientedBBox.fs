#version 460 core

layout(location = 1)
uniform vec4 _Color;

out vec4 outputColor;

void main() {
    outputColor = _Color;
}
