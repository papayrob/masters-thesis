#version 460 core

layout(location = 0)
uniform mat4 _PVMmatrix;
layout(location = 2)
uniform mat4 _VMmatrix;
layout(location = 10)
uniform float _AnimTime;

layout(location = 0)
in vec3 position0;
layout(location = 1)
in vec3 position1;

out VertexInfo {
    smooth vec3 v_position;
} vs_out;

void main() {
    vec3 position = mix(position0, position1, _AnimTime);
    vs_out.v_position = (_VMmatrix * vec4(position, 1.0)).xyz;
    gl_Position = _PVMmatrix * vec4(position, 1.0);
}
